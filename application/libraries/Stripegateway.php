<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include("./vendor/autoload.php");
class Stripegateway {
    
    public function __construct(){
        $stripe = array(
            'secret_key'      => 'sk_live_FbWnEfCFgCQkFrHJSR5xLWaO',
            'publishable_key' => 'pk_live_fEHEWhOB8hlOt5r9uJSLvXd7'
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
    }
    
    public function create_customer($data){
        $customer_id = '';
        $customer = array(
          'email' => $data['email'],
          'source' => $data['token']
        );
        $stripe_customer = \Stripe\Customer::create(array(
                                                        'email' => $data['email'],
                                                        'source' => $data['token']));
        $customer_id = $stripe_customer->id;
        
        return $customer_id;
    }
    
    public function create_charges($data){
        $message = '';
        try{
            $charge = \Stripe\Charge::create(array('customer' => $data['customer_id'],
                                                   'amount' => $data['amount'],
                                                   'currency' => $data['currency'],
                                                   'source' => $data['token']));
            $message = $charge->status;
        }catch(Exception $e){
            $message = $e->getMessage();
        }
        return $message;
    }
    
}