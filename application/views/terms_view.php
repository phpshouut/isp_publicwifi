<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" href="assets/images/shouut-icons.png" type="image/x-icon"/>
    <title>SHOUUT OS</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url()?>assets/css/mui.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    
</head>

<body>

    <!-- Start your project here-->
	<div class="container-fluid" style="background-color:#fff">
		<div class="login_container" id="main_div">
			<div class="shouut_os_login_logo">
			   <?php
			      $isplogo = $this->permission_model->get_isplogo();
			      if($isplogo != '0'){
				echo '<img src="'.$isplogo.'" class="img-responsive"/>';
			     }else{
			     
				echo '<img src="'.base_url().'assets/images/isp_logo.png" class="img-responsive"/>';
			     }
			   ?>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-md-offset-3">
				<div class="row">
				<div class="shouut_os_input_container">
					<div class="row">
					
					  <h4>Welcome to Shouut OS</h4>
					  <h5>Please read through and <strong>Reseller Agreement</strong>.</h5>
					  <div class="mui-textfield mui-textfield--float-label" style="margin-top:0px">
						  <div class="shouut_os_form">
							  <div class="shouut_os_form-control"> 
								 <h4>SECTION B: GENERAL RESELLER AGREEMENT TERMS</h4> 
								   
								<p><strong>This Agreement (&#39;&#39; Agreement &#39;&#39;) is a legally binding agreement between:</strong></p>
								<p>YOU, being any person or company distributing the Cloud Services under the terms of this Agreement (&#39;&#39; You &#39;&#39; or &#39;&#39; Reseller &#39;&#39;); and</p>
								<p>Shouut Technologies Inc. (&#39;&#39; Company &#39;&#39;) a company incorporated under the federal laws of Canada and the provincial laws of Ontario, with address at: 24 Routliffe Lane, North York, Ontario, Canada.</p>
								<p>This Agreement sets forth the terms and conditions applicable to the reseller relationship between You and the Company regarding Your distribution of Cloud Services.</p>
								 <p>
                     BY SIGNING A RESELLER AGREEMENT (INCLUDING BUT NOT LIMITED TO SUBMITTING YOUR ELECTRONIC ACCEPTANCE OF THIS AGREEMENT BY CLICKING ON &#39;&#39; ACCEPT &#39;&#39; OR OTHER BUTTONS PROMPTING YOU TO ACCEPT THIS AGREEMENT ON THE RELEVANT COMPANY ACCOUNT REGISTRATION PAGE), YOU:
                  </p>
                  <p>(i) REPRESENT AND WARRANT THAT THE INFORMATION PROVIDED BY YOU IS TRUE AND ACCURATE; YOU HAVE THE FULL LEGAL CAPACITY NECESSARY TO ENTER INTO THIS AGREEMENT UNDER ANY LAW THAT MAY BE APPLICABLE TO YOU, AND THAT YOU ARE, IN ANY CASE, AT LEAST EIGHTEEN (18) YEARS OLD; YOU ARE EITHER (A) A SERVICE PROVIDER OR AN AUTHORIZED OR LEGAL REPRESENTATIVE OF THE SERVICE PROVIDER, AND HAVE THE POWER AND AUTHORITY TO BIND THE SERVICE PROVIDER TO THIS AGREEMENT AND ENTER INTO THIS AGREEMENT ON ITS BEHALF, OR (B) A PRODUCT DISTRIBUTOR OR AN AUTHORIZED AGENT OR LEGAL REPRESENTATIVE OF THE PRODUCT DISTRIBUTOR, AND HAVE THE POWER AND AUTHORITY TO BIND THE PRODUCT DISTRIBUTOR TO THIS AGREEMENT AND ENTER INTO THIS AGREEMENT ON ITS BEHALF; AND</p>
                  <p>(ii) ACKNOWLEDGE THAT YOU HAVE READ THIS AGREEMENT (INCLUDING ALL APPLICABLE DOCUMENTATION THERETO), UNDERSTAND IT, AND AGREE TO BE BOUND BY ITS TERMS AND CONDITIONS AND/OR HAVE CAUSED THE SERVICE PROVIDER/THE PRODUCT DISTRIBUTOR TO DO SO.</p>
				  <p><strong>1. Definitions</strong></p>
                  <p><strong>(a)</strong> <strong>&#39;&#39; Authorized Representative &#39;&#39;</strong> means any person who is authorized by the Board through a resolution to represent and sign on behalf of the entity for the purpose of the Agreement.</p>
                  <p><strong>(b)</strong> <strong>&#39;&#39; Addendum &#39;&#39;</strong> means any service-specific addendum applicable to the Cloud Services to be sold by the Reseller to End Users in the Territory, which are each made a part of this Agreement.</p>
                  <p><strong>(c)</strong> <strong>&#39;&#39; Calendar Quarter &#39;&#39;</strong> means any of the three (3) month periods commencing January 1, April 1, July 1, and October 1 of any given year during the Term of this Agreement.</p>
								
				  <p><strong>(d)</strong> <strong>&#39;&#39; Cloud Service &#39;&#39; or &#39;&#39; Cloud Services &#39;&#39;</strong> means any resource that is provided over the internet by the Company and offered for sale to the Reseller from time to time, including any additional Support Services. Cloud Services include, but are not limited to, Email as a Service (&#39;&#39;EaaS&#39;&#39;), Software as a Service (&#39;&#39;SaaS&#39;&#39;), Platform as a Service (&#39;&#39;PaaS&#39;&#39;) and Infrastructure as a Service (&#39;&#39;IaaS&#39;&#39;). Additional information regarding each Cloud Service may be found in the applicable Documentation.</p>
								
				<p><strong>(e)</strong> <strong>&#39;&#39; Confidential Information &#39;&#39;</strong> means any and all information, in any medium, which is provided by one party to this Agreement (&#39;&#39;Discloser&#39;&#39;) to the other party (&#39;&#39;Recipient&#39;&#39;) including, without limitation, information that is either </p>
				<p>
					<ul style="list-style: none"> 
						<li><strong>i.</strong> Related to business practices, financial statements, financial information, pricing, vendors, resellers, Company, End Users, customers, Cloud Services, methods, techniques, processes, apparatuses, and employee data; </li>
						<li><strong>ii.</strong> Marked using a legend such as &#39;&#39;confidential,&#39;&#39; &#39;&#39;proprietary&#39;&#39; or similar words or if disclosed orally must be confirmed as such by the Discloser; or </li>
						<li><strong>iii.</strong> Any information which Recipient should have reasonably considered to be confidential under the circumstances surrounding disclosure.</li>
					</ul>
			  </p>					
				<p><strong>(f)</strong> <strong> &#39;&#39; Contract &#39;&#39;</strong> means any agreement entered into by the Company and the Reseller under the Agreement including, but not limited to, Purchase Orders issued by the Reseller and accepted by the Company for Reseller&#39;s purchase of Cloud Services from the Company.</p>
				<p><strong>(g)</strong> <strong> &#39;&#39; Contract Date &#39;&#39;</strong> means the date upon which a Reseller Purchase Order is accepted by the Company, as set forth in Section 4.</p>
				<p><strong>(h)</strong> <strong> &#39;&#39; Company &#39;&#39;</strong> means Giant tech Labs Private Limited and its subsidiaries and affiliates.</p>
				<p><strong>(i)</strong> <strong> &#39;&#39; Documentation &#39;&#39;</strong> means any and all additional documents, policies, procedures, programs, requirements, criteria and/or information relating to the sale and usage of the Cloud Services, including but not limited to the Authorized Use Policy, Statements of Work, and/or Service Level Agreements (&#39;&#39; <strong>SLAs</strong> &#39;&#39;). Reseller may access the Documentation online at www.shouut.com. Company may update or modify the Documentation from time to time; provided that changes to the Documentation will not result in a material reduction in the level of performance or availability of the applicable Cloud Services provided to the Reseller for the duration of Term hereof. Reseller must accept and comply, and shall notify its End Users to accept and comply with the terms of any applicable Documentation.</p>
				<p><strong>(j)</strong> <strong> &#39;&#39; End User &#39;&#39;</strong> means a user of a Cloud Service.</p>
				<p><strong>(k)</strong> <strong> &#39;&#39; Other Agreements &#39;&#39;</strong> means any other terms and conditions of sale, contracts, agreements or arrangements between the Reseller and Company for the purchase by the Reseller of any products and services other than Cloud Services for End Users, whether executed before or after the Effective Date.</p>
				<p><strong>(l) </strong><strong> &#39;&#39; Support Services &#39;&#39;</strong> means any additional support services available from Company for the applicable Cloud Services.</p>
				<p><strong>(m)</strong> <strong> &#39;&#39; Territory &#39;&#39;</strong> means the geographic regions or markets in which the Reseller is authorized to sell a particular Cloud Service, as set forth in Section A of the Agreement.</p>
				<p><strong>(n)</strong>	<strong> &#39;&#39; Usage Report &#39;&#39;</strong> means a periodic report generated by theCompany for a specific End User. The Usage Report may be sent or made accessible to the Reseller for forwarding to the End User, indicating the actual level of Cloud Services usage during a given time period and may serve as a basis for the invoicing and payment of any Overage Fees or usage based Fees in accordance with any applicable, additional requirements or Documentation.</p>			
            <hr>
            <h5><strong>(2) Appointment</strong> </h5>
			<p>
				<ul style="list-style:none">
					<li>
						<p>a.<strong> Non-Exclusive Appointment</strong></p>
						<p>Subject to compliance with the terms of this Agreement, Company hereby grants the Reseller the non-exclusive right and authority to purchase from the Company and subsequently market and sell Cloud Services to End Users in the Territory. </p>
					</li>
					<li>
						<p>b.<strong> Cloud Services Responsibility Matrix</strong></p>
						<p>Reseller and Company agree that in order to meet their respective obligations effectively with regard to the delivery of the Cloud Services to End Users hereunder, prior or subsequent to execution of this Agreement, the parties may work together to develop a responsibility matrix (<strong> &#39;&#39; Cloud Services Responsibility Matrix &#39;&#39;</strong>) or similar technical document, which shall allocate the respective roles and responsibilities of each party with regard to the provision of Cloud Services to End Users. Notwithstanding the foregoing, such technical document shall not supersede, and shall, at all times, remain subject to, the terms of the Agreement. </p>
					</li>
					<li>
						<p>c.<strong> No License of Trademarks</strong></p>
						<p>Nothing contained herein shall be construed as granting to the Reseller any right or license to use any trade names, service marks, trademarks, logos and other marks (collectively, &#39;&#39;Trademarks&#39;&#39;), other than that specified in Section A of the Agreement. Any other trademarks, if applicable, shall be subject to a separate agreement, including any current published requirements or guidelines (<strong> &#39;&#39; Trademark Guidelines &#39;&#39;</strong>).</p>
					</li>
					<li>
						<p>d. <strong>Title to Trademarks</strong></p>
						<p>Nothing contained herein shall be construed as granting the Reseller any title or ownership interest in or to the Trademarks and the Reseller makes no claim of title or ownership interest in and/or to the Trademarks. The Reseller agrees not to register, nor attempt to register, any Trademark which may be confusingly similar to the Trademarks in any jurisdiction.</p>
					</li>
					<li>
						<p>e. <strong>Hardware and Infrastructure Purchases</strong></p>
						<p>The Reseller agrees that any and all additional hardware infrastructure products associated with the delivery of the Cloud Services that can be provided by the Company in a reasonable timeframe,and at a cost acceptable to the reseller, will be purchased through the Company, subject to the Company&#39;s standard pricing and terms and conditions of sale for such associative hardware infrastructure products.</p>
					</li>
					<li>
						<p>f. <strong>End User Agreements</strong></p>
						<p>Reseller may not distribute any Cloud Services to any End Users unless an End User enters into an agreement with the Reseller (the <strong> &#39;&#39;End User Agreement &#39;&#39;</strong>) that at a minimum: </p>
						<p>
						   <ul style="list-style: none">
							   <li><p>
								   <strong>i.</strong> Completely disclaims Company&#39;s liability for all matters arising out of or related to this Agreement or the Cloud Services to be provided hereunder to the extent permissible by law and requires the End User to look solely to the Reseller with respect to such matters, unless otherwise agreed by writing by the Company; 
								   </p>
							   </li>
							   <li>
								   <p>
								   <strong>ii.</strong> Requires the End User to agree that all End User use of the Cloud Services shall be lawful and to ensure that each End User complies fully with the applicable terms of this Agreement, the acceptable use policy for the Cloud Services (if any), and all applicable laws and regulations in any of its dealings with respect to the Cloud Services; 
								   </p>
							   </li>
							    <li>
								   <p>
								   <strong>iii.</strong> Prohibits the End User from reselling or distributing the Cloud Services;
								   </p>
							   </li>
							   <li>
								   <p>
								   <strong>iv.</strong> Indemnifies, defends and holds the Company, and their respective affiliates, officers, directors, employees and suppliers harmless from and against any third-party claims arising out of or relating to the End User&#39;s (or its authorized users&#39;) use of the Cloud Services;
								   </p>
							   </li>
							   <li>
								   <p>
								   <strong>v.</strong> Protects the Reseller&#39;s proprietary rights in the Cloud Services to at least the same degree as the terms and conditions of this Agreement;
								   </p>
							   </li>
							   <li>
								   <p>
								   <strong>vi.</strong> Makes no representations or warranties on behalf of the Company, except to the extent permitted in Section 9(a);
								   </p>
							   </li>
							   <li>
								   <p>
								   <strong>vii.</strong> Specifies Company as express intended third party beneficiary of the provisions in the End User Agreement relating this Section 2(f), to the extent permitted by applicable law; and
								   </p>
							   </li>
							   <li>
								   <p>
								   <strong>viii.</strong> Does not grant any rights to the End User beyond the scope of this Agreement.
								   </p>
							   </li>
						   </ul>
						</p>
					</li>
				</ul>
			</p>
            <hr>
            <h5><strong>(3) Term and Termination</strong></h5>

              <ul style="list-style: none">
				  <li>
					  <p>a. <strong>Term.</strong></p>
						  <p>The term of this Agreement shall commence upon the Start Date and continue for a period of twenty four (24) months (<strong> &#39;&#39; Initial Term &#39;&#39;</strong>) and shall automatically renew for additional periods of twelve (12) months each (<strong> &#39;&#39; Renewal Term &#39;&#39;</strong>), unless either party provides the other party at least sixty (60) days&#39; prior written notice of its intent not to renew. The Initial Term and each Renewal Term shall be collectively referred to as the <strong> &#39;&#39; Term &#39;&#39;</strong>.</p></li>
				  <li><p>b.	<strong>Termination</strong></p>
					  <p>Either party may terminate this Agreement without cause and without penalty by providing the other thirty (30) days advance written notice of termination. Notwithstanding the foregoing, Reseller shall not be entitled to terminate without cause during the Initial Term. Either party may terminate this Agreement if at any time </p>
					  <p>
					  <ul style="list-style: none">
						  <li><p>i. The other party breaches the Agreement, </p></li>
						  <li><p>ii. That breach has a material adverse impact on the non-breaching party, and</p></li>
						  <li><p>iii. The breaching party fails to cure such breach within thirty (30) days following receipt of written notice describing the breach.
							Further, either party may terminate this Agreement if the other party becomes insolvent, makes a general assignment for the benefit of creditors, suffers or permits the appointment of a receiver for its business or assets, or avails itself or becomes subject to any proceeding under any applicable bankruptcy laws of any other country, federal, state or local statutes relating to insolvency or the protection of rights of creditors. The Reseller&#39;s right and authority to purchase, market and sell the Company&#39;s Cloud Services to End Users in the Territory will immediately terminate in the event that the Agreement is terminated or expires. The Company will use commercially reasonable efforts to notify the reseller in advance of any such termination or expiration.
							</p></li>
					  </ul>
				  </p>
				  </li>
			     <li><p>c. <strong>Subscription Term</strong></p>
				 <p>The Cloud Service shall be sold by the Company to the Reseller for usage by End Users, the term for which shall be designated on the Purchase Order between the Reseller and the Company (<strong> &#39;&#39; Subscription Term &#39;&#39;</strong>). Each Subscription Term shall begin on the effective date set forth on the Purchase Order and shall run for the designated term, unless otherwise terminated in accordance with the Agreement, including any applicable Documentation. The applicable Addendum may specify a minimum Subscription Term and may provide for auto-renewal of such Subscription Terms. The Reseller shall have the right to terminate any Purchase Order or its obligations to provide Cloud Services to a particular End User during a Subscription Term by providing the company with 30 days  notice of its intent for the same.</p></li>
				  
					<li><p>d. <strong>Transition Period</strong></p>
					  <p>Following expiration or termination of the Agreement, unless otherwise set forth in the applicable Addendum, there shall begin a transition period, not exceeding 30 days, to allow End Users to transition off of the Cloud Services (<strong> &#39;&#39; Transition Period &#39;&#39;</strong>). The parties shall continue to be bound by this Agreement during the Transition Period with respect to any Purchase Orders submitted prior to the effective date of expiration or notice of termination, as the case may be, for the duration of any active Subscription Terms (<strong> &#39;&#39; Surviving Subscription Terms &#39;&#39;</strong>). During the Transition Period, the Reseller shall not enter into any new Purchase Orders, nor shall the Reseller renew or extend the Subscription Term for any Surviving Subscription Terms.</p></li>  
					<li><p>e. <strong>Termination Assistance. </strong></p>
					  <p>Except as otherwise set forth in the Addendum, Reseller will, at least thirty (30) days prior to the effective date of termination or expiration of each End User&#39;s Subscription Term during the Transition Period, or as promptly as possible if less time is available, notify the End Users of the impending termination. The parties will cooperate in good faith to provide such End Users with instructions regarding how such End Users may continue to receive the applicable or comparable Cloud Services and to timely transition End Users seeking to maintain continuity of such Cloud Services, including, but not limited to, assignment or transfer of End User Cloud Services subscriptions. The Agreement shall fully and finally terminate upon expiration of the final Transition Period for the final Surviving Subscription Term.</p></li>
               </ul>
			   <hr>
              <h5><strong>(4) Purchase Ordering</strong></h5>
								<ul style="list-style:none">
									<li><p>a. <strong>Purchase Orders</strong></p>
										<p>Reseller may submit a purchase order to the Company for Cloud Services hereunder (<strong> &#39;&#39; Purchase Order &#39;&#39;</strong>) which must contain the following information:
										</p>
										<p>
										<ul style="list-style:none">
											<li>
												<p>i. Reseller&#39;s corporate name;</p>
											</li>
											<li>
												<p>ii. End User&#39;s corporate name;</p>
											</li>
											<li>
												<p>iii. The specific Cloud Services ordered;</p>
											</li>
											<li>
												<p>iv. The initial Cloud Services quantity ordered;</p>
											</li>
											<li>
												<p>v. The committed Subscription Term length for each of the Cloud Services ordered; and</p>
											</li>
											<li>
												<p>vi. Any additional information required or set forth in any Documentation (<strong> &#39;&#39; Purchase Order Requirements &#39;&#39;</strong>).
The Company&#39;s quotes shall not constitute an offer. Only a Purchase Order submitted by the Reseller shall constitute an offer to contract subject to this Agreement. However a Purchase Order shall not be deemed a Contract unless and until the earlier date upon which: 
</p>
												<p>
												<ul style="list-style: disc">
													<li><p>Written acceptance is provided by the Company, or</p></li>
													<li><p>Company proceeds with the fulfillment of the Purchase Order.</p></li>
												</ul>
											   </p>
										<p>No additional or alternative terms of agreement or any alteration to this Agreement proposed by the Reseller contained or referred to in a Purchase Order or other form submitted to the Company shall be deemed to apply unless they are expressly accepted in writing by an Authorized Representative of the Company with respect to that Purchase Order. The Reseller shall be solely responsible for the accuracy of any Purchase Order, including, but not limited to, the specification, configuration or other details of Cloud Services and their functionality, compatibility and interoperability with other products or services, as well as their fitness for particular use.</p>
											</li>
										</ul>
										</p>
								   </li>
								<li><p>b.<strong> No Cancelation</strong></p>
								
								<p>Notwithstanding the provisions mentioned in Clause 3 (c) herein, Reseller may not cancel nor reschedule any Purchase Order to the Company at any time.</p>
							</li>
								<li><p>c. <strong>Acceptance of Documentation</strong></p>
								<p>Reseller&#39;s submission of any Purchase Order for Cloud Services hereunder shall constitute the Reseller&#39;s acknowledgement and acceptance of any applicable Documentation for such Cloud Services. The Reseller agrees that it shall forward on any applicable Documentation to the End User.</p></li>
								</ul>
               
              
				<hr>
            <h5><strong>(5) Fees and Payment Terms </strong></h5>
            <p>
            <ul style="list-style:none">
				<li>
					<p>a. <strong>Fees</strong></p>
					<p>The fee charged to the Reseller for Cloud Services purchased hereunder on the Contract Date shall be:</p>
					<p>
						<ul style="list-style: none">
							<li><p>i. As listed above in Section A of the Agreement, where no price has been quoted or a quoted price has expired, or </p></li>
							<li><p>ii. The quoted price (which shall be given formally in writing by the Company and be valid for seven (7) days following the date of quotation).</p></li>
					    </ul>
					</p>
					<p>In the event the Company makes a material error or omission when quoting a price, the Company shall be entitled, for a period of seven (7) days following the Contract Date, to terminate the Cloud Services with immediate effect and intimate the same to the Reseller. Subsequent to such action, the Reseller may request the company for a fresh quote for the said Cloud Services.</p>
				
				</li>
				<li><p>b. <strong>Taxes</strong></p>
					<p>All prices and charges are exclusive of the cost of interest, insurance, configuration, fulfillment, cancellation, or rescheduling charges, and other services, as well as applicable Harmonized Sales Tax (<strong> &#39;&#39; HST &#39;&#39;</strong>), sales, use, consumption, privilege, and other taxes (other than taxes based upon the Company&#39;s net income), duties or customs fees for which the Reseller shall be additionally liable for paying to the Company, but Company shall, upon request, quote the additional costs of such items to the Reseller. In addition, prices exclude any copyright levies, waste and environment fees and similar charges that the Company, by law or statute, may charge or collect upon in accordance with such laws or statutes. The Reseller will be responsible for all taxes and duties payable on any Cloud Services purchased by the Reseller, where the tax is imposed on the Reseller&#39;s acquisition or use of such Cloud Services and the amount of tax is measured by the Reseller&#39;s costs in acquiring such Cloud Services and shall make all payments of any such taxes without reduction for any withholding taxes, which shall be the Reseller&#39;s sole responsibility. All taxes shall be paid by the Reseller to the Company, unless the Reseller provides the Company with a valid certificate of exemption acceptable to the appropriate taxation authority.</p>
				</li>  
				<li>
					<p>c. <strong>Usage Reports</strong></p>
					<p>Periodically, a report may be generated by the Company, which may be sent or made accessible to the Reseller, indicating the actual level of Cloud Services usage by End Users during a given time period and which may serve as a basis for the invoicing and payment of any Overage Fees or usage based Fees in accordance with any additional requirements or Documentation.</p>
				</li>
				<li>
					<p>d. <strong>Payment Terms</strong></p>
					<p>Payment terms for the initial Cloud Services order, and any subsequent Cloud Services orders, are set forth in Section 5(d)(i) (<strong> &#39;&#39; Fees &#39;&#39;</strong>). If applicable, payment terms for any additional fees generated by Overage (as defined below and as may be further described in any applicable Documentation) incurred during the Subscription Term shall be set forth in Section 5(d)(ii) below (<strong> &#39;&#39; Overage Fees &#39;&#39;</strong>). The Reseller guarantees full and prompt payment to the Company of any sums as they become due for any Purchase Orders placed hereunder by the Reseller or its subsidiaries and affiliates. For purposes of this Section, a Reseller subsidiary or affiliate shall be defined as any entity where the Reseller owns or controls more than 30% of its shares or other ownership interests. If payment is made by credit or debit card, then the Reseller agrees to pay all fees and service charges incurred by the Company in handling such transactions.</p>
					<p>i. <strong>Fees -</strong> Upon acceptance of a Purchase Order from the Reseller, the Company shall invoice the Reseller the Fees for the initial Cloud Services quantity set forth on the Purchase Order. Unless otherwise agreed to in writing by the parties:</p>
					<p>
						<ul style="list-style:none">
							<li>1.	The Fees shall be as set forth in Section A of the Agreement</li>
							<li>2.	All Fees shall be paid monthly in advance;</li>
							<li>3.	Fees are based on the quantity of Cloud Services purchased, and not actual usage by the End User, which may be less.
The committed quantity of purchased Cloud Services cannot be decreased during the Subscription Term, unless otherwise agreed by the parties in writingand subject to the provisions of Clause 3 (c).
</li>
					    </ul>
					</p>
<p>ii. Overage Fees - In the event actual usage of Cloud Services exceeds the initial quantity ordered on the Purchase Order (<strong>&#39;&#39; Overage &#39;&#39;</strong>), the Reseller may be billed for any applicable Overage charges on a monthly basis in accordance with any applicable terms or Documentation (<strong>&#39;&#39; Overage Fees &#39;&#39;</strong>) provided by the Company. All undisputed portions of the Company&#39;s invoices for Overage Fees will be paid by the Reseller within thirty (30) days of the Reseller&#39;s receipt of invoice for such.</p>
				</li>
               <li><p>e. <strong>Suspension of Access</strong></p>
				   <p>In the event that any fees owed to the Company by the Reseller, including but not limited to monthly Fees or Overage Fees, are thirty (30) days or more overdue, the Company may suspend End User&#39;s access to the Cloud Service(s) associated with such delinquent payment (<strong>&#39;&#39; Suspension of Access &#39;&#39;</strong>) without penalty to the Company. In the event of a Suspension of Access, End User and/or Reseller may be subject to termination and liable to pay any applicable early termination fees (<strong>&#39;&#39; Early Termination Fees &#39;&#39;</strong>) set forth in any applicable Documentation or otherwise imposed by the Company. Notwithstanding the foregoing, in the event the Company elects for a Suspension of Access, the Reseller may be liable for any fees, including but not limited to monthly Fees or Overage Fees, associated with such delinquent payment through the end of the current Subscription Term. Upon or prior to the expiration of the current Subscription Term, the Company shall have the option to reinstate, or request to reinstate End User's access to the Cloud Services, elect not to renew the Subscription Term or terminate the End User account, without any further penalty to the Company. In the event of reinstatement of Cloud Services, Company may request payment of the monthly Fees due from the date of the Suspension of Access through the date of reinstatement of the Cloud Services. The Reseller acknowledges that a Suspension of Access may result in liability to the Company and agrees to fully indemnify, defend and hold harmless the Company and its affiliates from and against all claims, losses, liabilities, damages, costs and expenses (including reasonable attorneys&#39; fees), judgments or settlement amounts arising out of or in connection with any Suspension of Access.</p>
				</li>
				<li><p>f. <strong>Default and Insolvency</strong></p>
					<p>If</p>
					<p>
					   <ul style="list-style:none">
						   <li>
							   <p>i. Reseller fails to make any payment under any Contract and/or Purchase Order when due; </p>
						   </li>
						   <li>
							   <p>ii. Reseller fails to make any payment under any Other Agreements when due;</p>
						   </li>
						   <li><p>iii. Any distress or execution is levied upon Reseller&#39;s property or assets;</p></li>
						   <li><p>iv. Reseller makes or offers any arrangement or composition with its creditors;</p></li>
						   <li><p>v. Reseller is a body corporate and any resolution or petition to wind up Reseller&#39;s business (other than for the purpose of amalgamation or reconstruction) is passed or presented;</p></li>
						   <li><p>vi. A receiver, administrator, manager or analogous person is appointed in respect of the undertaking, property or assets of Reseller or any part thereof; or</p></li>
						   <li><p>vii. Reseller exceeds its credit limit,
								then, without prejudice to any other right or remedy available to the Company, the full price of all Cloud Services delivered to Reseller under any Contract, but not paid, shall become immediately due (notwithstanding any previously agreed credit terms) and the Company may take any or all of the following courses of action: 
								</p>
							   <ul style="list-style:none">
								   <li><p>I. By notice, suspend or terminate any Contract and/or Purchase Order or any part thereof, without liability, suspend or terminate any Cloud Services;</p></li>
								   <li><p>II. Charge Reseller interest, both pre- and post-judgment, on any unpaid amount past due, at the rate of 2.5% (or the highest rate allowed by law) per month until full payment is made (for clarity, a part of a month shall be treated as a full month for the purpose of calculating interest); </p></li>
								   <li><p>III. Set-off any amounts due against any credit note, balance or other liability issued by Company to Reseller;</p></li>
								   <li><p>IV. Appropriate any payment made by Reseller to such Cloud Services (including Cloud Services supplied under any other contract between Reseller and Company or any Company branch or subsidiary) as the Company may deem fit (notwithstanding any purported appropriation by Reseller); and/or</p></li>
								   <li><p>V. Alter Reseller&#39;s payment terms, which may include withdrawing or altering any credit limit previously granted, requiring prepayment, and demanding adequate assurance of due performance by Reseller through the provision of a bank guarantee.</p></li>
							   </ul>
						   </li>
					   </ul>
					</p>
				</li>
            </ul>
            </p>
            <hr>
            <h5><strong>(6) Electronic Delivery and Hosting and Data Center Facilities</strong></h5>
            <ul style="list-style: none">
               <li>
				   <p>
				   		<ul style="list-style: none">
							<li>
								<p>a. <strong>Electronic Delivery</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>The Company shall electronically deliver the Cloud Services (subject to a valid, accepted Purchase Order) directly to End Userson behalf of the Reseller, including any additionally required Documentation, for the purposes of this Agreement. Such Cloud Services shall be provided by the Company in accordance with any additional terms and conditions of use provided in any Documentation or URL Link, including, without limitation, the applicable authorized use policy for the Cloud Services (<strong>&#39;&#39; Authorized Use Policy &#39;&#39;</strong>), incorporated herein by reference. Reseller and/or End User&#39;s usage of the Cloud Services remains subject, at all times, to the terms and conditions of the Company&#39;s applicable Authorized Use Policy. The Company shall not be liable for any losses, damages, claims or liabilities arising out of or in connection with an alleged or actual breach of the Company&#39;s Authorized Use Policy by any Reseller or End User. Company shall not be liable with respect to any breach or error in delivery, loss, damage or interruption to the Cloud Services during the Subscription Term.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
                 <p>
				   		<ul style="list-style: none">
							<li>
								<p>b. <strong>Hosting and Data Center Facilities. </strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>The hosting and data center facilities supporting the Cloud Services delivered by the Company for usage by the End User shall be provided for and managed by a third party to this Agreement on behalf of the Company. Company shall not be liable in respect of any breach or error in delivery, loss, damage or interruption to the Cloud Services during the Subscription Term. Reseller shall immediately notify the Company, in writing, of any such error, loss, breach, damage or interruption. Company shall not be liable for any loss, damage or expense whatsoever and howsoever arising from any breach or error, loss, damage, defect or interruption to the Cloud Services. Any error, loss, damage or interruption of Cloud Services discovered by Reseller and/or End User after delivery shall not entitle Reseller and/or End User to rescind the Purchase Order or the remainder of a Contract.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
               </li>
            </ul>
            <hr>
            <h5><strong>7.Support and Service Level Agreements</strong></h5>
            <ul style="list-style: none">
               <li>
				   <p>
				   		<ul style="list-style: none">
							<li>
								<p>a. <strong>Support</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>If applicable, the Company will provide a reasonable level of technical and customer support for the Cloud Services in accordance with the Company&#39;s then-current support policy for the Cloud Services (<strong>&#39;&#39; Support Terms &#39;&#39;</strong>), as set forth in the Documentation. Support Terms may include, among other things, the levels of support available to Resellers and/or End-Users, a description of support offerings, applicable hours of operation, number of available skilled resources, languages supported and scheduled maintenance windows.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
                 <p>
				   		<ul style="list-style: none">
							<li>
								<p>b. <strong>Service Level Agreements</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>To the extent that the Company provides quality and performance standards in connection with its provision of any Cloud Services purchased hereunder, as set forth in the applicable Documentation, the Company shall be fully responsible for delivery of the Cloud Services in accordance with the terms of such SLAs, including payment of any penalties or return credits in the event of disruption or outages. Unless otherwise agreed by the parties in writing, Company shall not offer any SLAs in connection with the provision of Cloud Services hereunder.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
               </li>
            </ul>
            <hr>
            <h5><strong>(8) Services Promotion and Training</strong></h5>
             <ul style="list-style: none">
               <li>
				   <p>
				   		<ul style="list-style: none">
							<li>
								<p>a. <strong>Marketing and Promotion</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Reseller shall use commercially reasonable efforts at its own cost to market, promote, and sell the Cloud Services in accordance with the terms of this Agreement.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
                  <p>
				   		<ul style="list-style: none">
							<li>
								<p>b. <strong>Support Service</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Company shall provide commercially reasonable amount of sales and marketing support to assist Reseller in the sale of the Cloud Services.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
 					<p>
				   		<ul style="list-style: none">
							<li>
								<p>c. <strong>Training</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Reseller and its employees and agents shall comply with all basic and certificate training, if applicable, at its own cost, required to perform the sales, marketing and technical support of the Cloud Services. Company may make such training, and any training materials, available for the training of Resellers to the extent made available by Company. Reseller shall provide Company the necessary and reasonable space, equipment and on-site facilities for the sole purposes of Company&#39;s provision of the basic and certification training.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
						<p>
				   		<ul style="list-style: none">
							<li>
								<p>d. <strong>Support Staff</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Reseller shall maintain, at its own cost, a staff of sales, marketing and technical personnel familiar with the applications, features, benefits, operations and configuration of the Cloud Services.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
					<p>
				   		<ul style="list-style: none">
							<li>
								<p>e. <strong>Publications & Specifications</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Any and all specifications, descriptions, photographs, measurements, capacities or illustrations contained in any catalogues, price lists, brochures, leaflets, proposals, advertising matter, publications of the Company are intended to be illustrative and approximate only and shall not form part of a Contract or constitute a representation, warranty or condition regarding any Cloud Services, unless specifically agreed by written agreement between Reseller and Company or constituting Documentation hereunder. No employee or agent of the Company has any authority to make any representation regarding the Cloud Services. Reseller acknowledges that it has not been induced to accept this Agreement or a Purchase Order hereunder by any representations or statement, oral or written, not expressly contained herein.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
               </li>
            </ul>
            <hr>
            <h5><strong>(9) Warranties and Disclaimers </strong></h5>
            <ul style="list-style: none">
               <li>
				   <p>
				   		<ul style="list-style: none">
							<li>
								<p>a. <strong>Services Warranty</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Reseller understands and accepts all Cloud Services are sold subject to the express warranty terms, if any, specified by the Company or set forth in any applicable Documentation.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
                  <p>
				   		<ul style="list-style: none">
							<li>
								<p>b. <strong>No Representations</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Reseller may not make or pass on any warranty terms or representations on behalf of the Company other than the warranty provided by the Company and shall take all measures necessary to ensure that neither it nor any of its agents or employees shall make or pass on, any such warranty or representation relating to any Cloud Services provided by Company. Reseller shall disclaim all other warranties, whether express or implied, other than any warranty terms that the Reseller is permitted to make or pass on pursuant to Section 9(a).</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
 					<p>
				   		<ul style="list-style: none">
							<li>
								<p>c. <strong>Disclaimer of Warranties. </strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>COMPANY AND RESELLER DISCLAIM ALL OTHER WARRANTIES, EXPRESS AND IMPLIEDUNDER THE RELEVANT CANADIAN LEGISLATION, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT AND QUALITY. NO ORAL OR WRITTEN INFORMATION OR ADVICE GIVEN BY COMPANY OR ITS AUTHORIZED REPRESENTATIVES WILL CREATE ANY OTHER WARRANTIES OR IN ANY WAY INCREASE THE SCOPE OF COMPANY&#39;S OBLIGATIONS HEREUNDER.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
						<p>
				   		<ul style="list-style: none">
							<li>
								<p>d. <strong>Warranty Assistance</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Reseller shall immediately notify the Company if any Cloud Services supplied to Reseller prove to be defective in quality, availability or condition under Company&#39;s warranty for the Cloud Services (the <strong>&#39;&#39; Claim &#39;&#39;</strong>). Upon receipt of notification of such Claim from Reseller, Company shall notify Reseller whether, as a matter of the Company&#39;s policy, a remedy is available. Such Claims will be addressed in accordance with the applicable Company policy as set forth in the applicable Documentation. </p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
					<p>
				   		<ul style="list-style: none">
							<li>
								<p>e. <strong>Sole Remedy</strong></p>
								<p>
								<ul style="list-style:none">
									<li><p>Reseller agrees that the Company&#39;s sole and exclusive liability to Reseller and End Users regarding any Cloud Services defect claims is limited to the administration of such claims to process a refund, credit or replacement Cloud Services. The Company has no obligation to accept a return credit of Cloud Services where the Reseller fails to comply with any Documentation or the Company&#39;s policy on Cloud Services returns. The Company shall not be liable or responsible for administering any defect or other claim which arises from normal usage, misuse, negligence, accident, abuse, use not in accordance with Company&#39;s Documentation, modification or alteration not authorized by the Company, or use in conjunction with a third party product. The Company reserves the right to determine whether any Cloud Services are defective. All charges incurred in returning or replacing Cloud Services are the responsibility of Reseller.</p></li>
								</ul>
							   </p>
							</li>
				        </ul>
				   </p>
               </li>
            </ul>
            <hr>
            <h5><strong>(10) Indemnification </strong></h5>
			<ul style="list-style:none">
				<li>
					<p>a. <strong>General Indemnity</strong></p>
					<p>
						<ul style="list-style:none">
							<li><p>Reseller will indemnify, defend and hold harmless the Company and the Company&#39;s affiliates, directors, officers, employees, agents, contractors and End Users from and against all claims, lawsuits, losses, liabilities, damages, costs and expenses (including reasonable attorneys&#39; fees), judgments or settlement amounts arising out of or in connection with </p></li>
							<li><p>i. The Cloud Services or this Agreement, </p></li>
							<li><p>ii. Any breach of Section 2(f) or where an End User Agreement otherwise fails to protect the Company in the manner described in Section 2(f); </p></li>
							<li><p>iii. Any termination, for whatever reason, of Cloud Services by Company,</p></li>
							<li><p>iv. Any injury or damage to persons (including death) or tangible or intangible property, </p></li>
							<li><p>v. A claim that any Cloud Service was defective or failed to perform as represented, </p></li>
							<li><p>vi. Reseller&#39;s failure to perform any of its obligations under this Agreement, or </p></li>
							<li><p>vii. A breach or inaccuracy in any material respect of any representation or warranty made by Reseller in this Agreement.</p></li>
						</ul>
				   </p>
				</li>
              <li><p>b. <strong>Intellectual Property Rights</strong></p>
				       <p><ul style="list-style:none">
							<li><p>Reseller understands and agrees that the Company will not, and has no duty to indemnify, defend or hold Reseller or a third party harmless from or against any claims, losses, liabilities, damages, costs and expenses, judgments or settlement amounts arising out of or in connection with the actual or alleged infringement of a third party's intellectual property rights, including, but not limited to, the Company, except and only to the extent that the Company or the third party has expressly agreed in writing to offer such indemnification and defense to the Reseller on a pass through basis. Reseller acknowledges that the Cloud Services are the intellectual property of the Company or the applicable Third Party. Nothing contained herein shall be deemed to grant any right or title to such intellectual property to the Reseller. Reseller further agrees not to translate, reverse compile or disassemble any object code or software associated with the delivery or usage of the Cloud Services. Reseller will not remove, alter or destroy any form of copyright notice, proprietary markings, serial numbers, or confidential legends associated with the Cloud Services, unless otherwise agreed in writing.</p></li>
				  			</ul>
						</p>

				</li>
				<li><p>c. <strong>Company&#39;s Obligations</strong></p>
				       <p>
							<ul style="list-style:none">
							    <li>
								 <p>In connection with the indemnities provided hereunder, the Company shall: </p>
									<p>
									<ul style="list-style: none">
										<li>
											<p>i. Promptly notify Reseller of any claim that is subject to Reseller&#39;s indemnification obligations hereunder. But the Company&#39;s failure to promptly notify Reseller shall not discharge the Reseller of its obligation to indemnify the Company unless and only to the extent that such failure is held to prejudice the Reseller&#39;s defense of such claim;</p>
									    </li>
										<li><p>ii. Reasonably cooperate with Reseller in the performance of its obligations hereunder, provided any related costs or expenses incurred by the Company shall be covered by Reseller; and</p></li>
										<li><p>iii. Grant Reseller the right to control the defense and settlement of any claim which is subject to indemnification, provided Reseller pays in full any monetary component of such settlement and further provided that such settlement contains a full and unconditional release of Company and no admission of liability on behalf of Company.</p></li>
										<li><p>Notwithstanding the foregoing, </p></li>
										<li>
											<p>
												<ul style="list-style: none">
													<li>
														<p>I.	Company shall have the right to employ separate counsel and participate in the defense of such action, at Company&#39;s expense, and </p>
													</li>
														<li>
														<p>II. if </p>
															<p>
																<ul style="list-style: none">
																	<li><p>a. The Reseller does not promptly assume the defense of any such claim following notice of its election to do so, or </p></li>
																	<li><p>b. The Company reasonably concludes that there may be defenses available to it which are different from or additional to those available to Reseller and which could reasonably be expected to result in a conflict of interest or prejudice to Company if both parties were represented by the same counsel, </p></li>
																</ul>
															</p>
											<p>then the Company has the right to undertake the defense of such claim with counsel of its own choosing, with the reasonable costs thereof to be borne by Reseller.</p>
													</li>
												</ul>
											</p>
										</li>
									</ul>
								   </p>
								</li>
				  			</ul>
						</p>
				</li>
			</ul>
            <hr>
            <h5><strong>(11) Limitation of Liability</strong></h5>
			<ul style="list-style: none"><li><p>COMPANY&#39;S LIABILITY FOR ANY DIRECT LOSS OR DAMAGE ARISING OUT OF THIS AGREEMENT AND ANY CONTRACT SHALL BE LIMITED TO, AND SHALL UNDER NO CIRCUMSTANCES EXCEED, THE ACTUAL AMOUNTS PAID BY THE RESELLER TO THE COMPANY FOR THE CLOUD SERVICES GIVING RISE TO THE CLAIM DURING THE PREVIOUS SIX (6) MONTHS, EXCLUDING ANY TAXES AND LESS ANY REFUNDS OR CREDITS RECEIVED BY THE RESELLER FROM THE COMPANY. EXCEPT AS EXPRESSLY PROVIDED IN THIS AGREEMENT, THE COMPANY SHALL NOT BE LIABLE TO THE RESELLER FOR ANY FINANCIAL, CONSEQUENTIAL OR OTHER INDIRECT LOSS OR DAMAGE CAUSED TO THE RESELLER BY REASON OF ANY REPRESENTATION, WARRANTY (EITHER EXPRESS OR IMPLIED), CONDITION OR OTHER TERM, OR ANY DUTY AT COMMON LAW; OR FOR ANY SPECIAL, INDIRECT, INCIDENTAL, PUNITIVE, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING LOSS OF PROFITS, REVENUE, RECORDS OR DATA, COSTS OF PROCUREMENT OF SUBSTITUTE CLOUD SERVICES, OR DAMAGE TO REPUTATION OR GOODWILL) OR FOR ANY OTHER CLAIMS FOR COMPENSATION HOWEVER CAUSED (WHETHER CAUSED BY THE NEGLIGENCE OF THE COMPANY, ITS EMPLOYEES, AGENTS, OR OTHERWISE) WHICH ARISE OUT OF OR IN CONNECTION WITH THIS AGREEMENT OR A CONTRACT HEREUNDER, EVEN IF THE COMPANY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS, LIABILITY OR DAMAGES. NOTHING CONTAINED HEREIN SHALL BE CONSTRUED AS EXCLUDING OR LIMITING THE COMPANY&#39;S LIABILITY FOR DEATH OR PERSONAL INJURY CAUSED BY THE COMPANY&#39;S NEGLIGENCE. THE RESELLER ACKNOWLEDGES THAT THESE ALLOCATIONS OF LIABILITY WERE AN ESSENTIAL ELEMENT IN COMPANY&#39;S ENTERING INTO THIS AGREEMENT AND AGREES THAT SUCH ALLOCATION OF LIABILITY ARE REASONABLE AND APPROPRIATE GIVEN THE NATURE OF THIS AGREEMENT.</p></li></ul>
            <hr>
            <h5><strong>(12) Insurance</strong> </h5>
            <ul style="list-style: none">
                <li>
                   <p>a. Except as otherwise agreed by Company in writing, Reseller will maintain, at its sole expense, during the Term of this Agreement, adequate insurance coverage as follows, underwritten by an insurance company :</p>
					<p>
					<ul style="list-style: none">
					<li><p>i. Comprehensive General Liability Insurance including Services Liability/Completed Operations Insurance; and</p></li>
					<li><p>ii. Professional Liability (also known as Errors and Omissions Liability) and Cyber Risk Insurance covering acts, errors and omissions arising out of Reseller&#39;s operations.</p></li>
					</ul>
					</p>
                </li>
                <li>
		   			<p>b. Reseller&#39;s insurance policies as required herein shall name Company, and Company&#39;s affiliates and their respective officers, directors and employees as additional insured for any and all liability arising at any time in connection with Reseller&#39;s or Reseller Personnel&#39;s performance under this Agreement. Each policy shall provide that it may not be canceled or materially altered except after thirty (30) days&#39; advance written notice to the Company. Reseller shall obtain such endorsements to its policy or policies of insurance as are necessary to cause the policy or policies to comply with the requirements stated herein. Reseller shall provide the Company with certificates of insurance evidencing compliance with this Section (including evidence of renewal of insurance) signed by authorized representatives of the respective carriers within thirty (30) days of the Effective Date and periodically upon Company&#39;s reasonable request. Each certificate of insurance shall provide that the issuing company shall not cancel, reduce, or otherwise materially alter the insurance afforded under the above policies unless notice of such cancellation, reduction or material alteration has been provided at least thirty (30) days in advance. With respect to insurance coverage to be provided by the Reseller pursuant to this Section, the insurance policies shall provide that the insurance companies waive all rights of subrogation against the Reseller and the Company and their respective Affiliates, officers, directors and employees. The Reseller waives its rights to recover against the Company and the Company&#39;s affiliates and their respective officers, directors, and employees in subrogation or as subrogee for another party. </p>
		</li>
		<li>
		   <p>c. The obligation of the Reseller to provide the insurance specified herein shall not limit in any way any obligation or liability of the Reseller provided elsewhere in this Agreement. The rights of the Company and its subsidiaries and affiliates to insurance coverage under policies issued to or for the benefit of one or more of them are independent of this Agreement and shall not be limited by this Agreement.</p>
		</li>
		</ul>
		<hr>
            
            <h5><strong>(13) Confidential and Proprietary Information</strong></h5>
			<ul style="list-style: none">
				<li><p>Each party acknowledges that during the course of performing its obligations hereunder it may receive Confidential Information. Each party will employ the same degree of care to protect the secrecy and confidentiality of the Confidential Information of the other party as it uses to protect its own Confidential Information of a similar nature, but in no event less than a reasonable degree of care. Each party will restrict the release, access and use of Confidential Information to those of its affiliates, employees, officers, directors, consultants and agents who must have access to the Confidential Information in order to perform its obligations under this Agreement, provided such affiliates, employees, officers, directors, consultants and agents are subject to written agreements which contain confidentiality obligations in substance, at least as strict as those set forth herein, in order to enable each party to comply with the provisions of this Agreement or provided they are otherwise bound. The receiving party is responsible for any breaches of confidentiality by such affiliates, employees, officers, directors, consultants and agents. Confidential Information herein shall not include information that </p>
					<p>
					<ul style="list-style: none">
					<li><p>i. The Recipient can demonstrate by its written records to have had in its possession prior to disclosure to the Recipient by the Discloser;</p></li>
					<li><p>ii. Was part of the public knowledge or literature, not as a result of any action or inaction of the Recipient;</p></li>
						<li><p>iii. Was subsequently disclosed to the Recipient from a source other than the Discloser who was not bound by an obligation of confidentiality to the Discloser;</p></li>
						<li><p>iv. The Recipient can demonstrate by its written records to have been independently developed by the Recipient without the use, directly or indirectly, of any Confidential Information; or</p></li>
						<li><p>v. The Recipient is required to disclose pursuant to a court order or as otherwise required by law; provided, however, that Recipient notifies the Discloser within sufficient time to give the Discloser a reasonable period to contest such order.</p></li>
					</ul>
				  </p>
<p>All Confidential Information and any Documentation is provided &#39;&#39;AS IS&#39;&#39; without any representation or warranty, either expressed or implied, as to accuracy or completeness. </p>
				</li>
				</ul>
	    <hr>
            <h5><strong>(14) Compliance With Laws</strong></h5>
            <ul style="list-style:none">
               <li>
                  <p>a.	<strong>Compliance With Laws</strong></p>
				   <p><ul style="list-style:none"><li><p>In connection with this Agreement and the delivery of Cloud Services hereunder, the Reseller shall comply with any and all applicable country, federal, provincial, and local laws, rules, regulations and codes, both domestic and foreign, including, but not limited to, any applicable import, re-import, export and re- export laws and regulations, including the Export Control Regulations by the Exports Control Division of Foreign Affairs, Trade and Development (DFATD), the International Traffic in Arms Regulations (ITAR) and Controlled Goods Program(CGP) implemented under the Defence Production Act and Controlled Goods Regulations. Reseller is solely responsible for compliance related to the manner in which Reseller and/or End Users choose to use the Cloud Services, including, without limitation, any transfer and processing of End User content, the provision of End User content to third parties and the region in which any of the foregoing occur.</p></li></ul></p>
               </li>
               <li>
                  <p>b. <strong>Export Compliance</strong></p>
				   <p><ul style="list-style:none"><li><p>To the extent any import, re-import, export and re-export control laws or regulations are applicable to the Cloud Services (<strong>&#39;&#39; Export Control Laws &#39;&#39;</strong>), Reseller hereby agrees not to export, re-export or otherwise distribute Cloud Services, or direct products thereof, in violation of any Export Control Laws. Reseller warrants that it will not export or re-export any Cloud Services with knowledge that they will be used in the design, development, production, or use of chemical, biological, nuclear, or ballistic weapons, or in a facility engaged in such activities, unless the Reseller has obtained prior written approval from the appropriate department of the Canadian Government or any other government with jurisdiction. The Reseller further warrants that it will not export or re-export, directly or indirectly, any Cloud Services to embargoed countries or sell Cloud Services to companies or individuals listed on the Sanctions List under the United Nations Act (UNA) or the Special Economic Measures Act (SEMA) published by the Canadian Global AffairsDepartment. It is Reseller&#39;s sole and exclusive responsibility to obtain any and all appropriate approvals from the applicable government entities, which may include the Canadian government or any other government with jurisdiction, prior to exporting such Cloud Services, or any technical data related thereto. The Company shall not be responsible for any costs, liabilities or damages resulting from the Reseller&#39;s failure to obtain any such required authorization and the Reseller shall indemnify the Company against the same. The Reseller understands that the Export Control Laws and economic sanctions programs may change from time to time. It is the Reseller&#39;s sole and exclusive responsibility to obtain guidance of counsel or other appropriate channels to ensure its compliance with these laws.</p></li></ul></p>
               </li>
                <li>
                  <p>c. <strong>Anti-Bribery</strong></p>
				   <p><ul style="list-style:none"><li><p>Each Party (including its officers, directors, employees, agents and any person under its control) shall comply with, and shall require its contractors, subcontractors and any contingent workers to comply with, any and all applicable anti-corruption laws and regulations, including, but not limited to, the Corruption of Foreign Public Officials Act (CFPOA) and the U.S. Foreign Corrupt Practices Act. Without limiting the foregoing, each party, (including its officers, directors, employees, agents and any person under its control) shall not, directly or indirectly, make, promise to make, or accept any payment, offer or transfer of anything of value in connection with this Agreement or any other business transaction, to: </p></li>
				<li><p>i. Anyone working in an official capacity for a government, government entity (including employees of government owned or controlled corporations), or public international organization; </p></li>
				<li><p>ii. Any political party, party official, or candidate for political office;</p></li>
				<li><p>iii. An intermediary for payment to any of the foregoing;</p></li>
				<li><p>iv. Any officer, director, employee of any actual or potential customer of either party;</p></li>
				<li><p>v. Any officer, director, employee of any commercial company;</p></li>
				<li><p>vi. Any officer, director or employee of either party or any of its affiliates; or</p></li>
				<li><p>vii. Any other person or entity if such payment, offer or transfer would violate the laws of the country in which made, or the laws of Canada or the United States of America.</p></li>
				<li><p>It is the intent of the parties hereto that no payments, offers or transfers of value shall be made or received which have the purpose or effect of public or commercial bribery, acceptance or acquiescence in extortion, kickbacks or other unlawful or improper means of obtaining or retaining business or directing business to any person or entity. In addition, each party warrants to the other that none of its officers, directors, employees, agents, or representatives is an official or employee of the government of the Territory or of any department or instrumentality of such government, nor is any of them an officer of a political party or candidate for political office who will share, directly or indirectly, any part of the sums due hereunder. The Company represents and warrants that it will conduct its business operations hereunder in accordance with all applicable Canadian and foreign laws and regulations and will not attempt to directly or indirectly improperly influence the sale of Cloud Services by payments or other actions contrary to law or regulation.</p></li></ul></p>
               </li>
				<li>
                  <p>d. <strong>Personal Data Protection</strong></p>
				   <p><ul style="list-style:none"><li><p>During the Term of this Agreement in connection with any processing of personal data which it receives under this Agreement, each party shall :</p></li>
				<li><p>i. Comply with all applicable laws, rules, regulations, regulatory requirements and codes of practice including, but not limited to, laws and regulations implementing the Privacy Act, the Digital Privacy Act, Personal Information Protection and Electronic Documents Act (PIPEDA), Health Information Protection Act and the Access to Information Act, (collectively, the <strong>&#39;&#39; Data Protection Laws &#39;&#39;</strong>) and </p></li>
				<li><p>ii. Implement commercially reasonable technical and organizational security procedures and measures to preserve the security and confidentiality of the personal data received under this Agreement.</p></li>
				<li><p>Neither party shall do any act that puts the other party in breach of its obligations under the Data Protection Laws. Nothing in this Agreement shall be deemed to prevent any party from taking the steps it reasonably deems necessary to comply with the Data Protection Laws including requiring signature of the each party on additional terms and conditions related to the Data Protection Laws prior to providing any personal information, and neither party shall take any action which a reasonable person knowledgeable in the Data Protection Laws should know may cause or otherwise result in a violation of the Data Protection Laws. Each party agrees to obtain all necessary consents under the Data Protection Laws and will not pass personal data to third parties without prior notification to the data subject. The Company may use, store or otherwise process and may transfer or disclose any personal data provided by Reseller to any member of the Company wherever located in the world for the purpose of administration of this Agreement and relationship management on an ongoing basis, and Reseller agrees to inform its employees of the same. Reseller will have the obligation to take necessary steps to provide prior notice to the data subject that their information may be used, stored or otherwise processed by the Company wherever located in the world. Reseller may use, store or otherwise process personal data provided by Company for relationship management purposes, but shall not pass any personal data to third parties without prior notification to the data subject.</p></li>
				</ul></p>
               </li>
				<li>
                  <p>e. <strong>Security Policy and Information Security Management Program</strong></p>
				   <p><ul style="list-style:none"><li><p>Without limiting the foregoing, and upon request by the Company, theReseller agrees to disclose in writing to the Company a detailed description of Reseller&#39;s information and data security controls and policies (<strong>&#39;&#39; Information Security Management Program &#39;&#39;</strong>). Upon reasonable demand, the Company shall have the right to examine the Reseller&#39;s ongoing compliance with its stated written information controls and security policies. </p></li></ul></p>
               </li>
				<li>
                  <p>f. <strong>Data Processing Indemnification</strong></p>
				   <p><ul style="list-style:none"><li><p>Reseller shall defend, indemnify and hold harmless the Company from and against any and all claims, actions, liabilities, losses, damages and expenses (including reasonable legal expenses), which arise from third party claims and/or government agency actions, arising directly or indirectly out of or in connection with Reseller&#39;s data processing activities under or in connection with this Agreement, including without limitation to, those arising out of any third party demand, claim or action, or any breach of contract, negligence, fraud, willful misconduct, breach of statutory duty or non-compliance with any part of the Data Protection Laws. Reseller will obtain adequate cyber insurance to cover the costs of potential data breaches and subsequent related litigation. </p></li></ul></p>
               </li>
				<li>
                  <p>g. <strong>Marketing Data</strong></p>
				   <p><ul style="list-style:none"><li><p>Reseller agrees that Company may collect, store and use Reseller data, for the purpose of facilitating its marketing and sale of the Cloud Services, and the Reseller hereby consents to such collection, storage and use of Reseller data by the Company for these purposes. Reseller further consents to the use of such data for communicating the Cloud Services and promotional information to the Reseller via email or other electronic means unless Reseller notifies Company in writing that it does not wish to receive such promotional information. </p></li></ul></p>
               </li>
            </ul>
            <hr>
            <h5><strong>(15) General Terms </strong></h5>
            <ul style="list-style:none">
               <li>
                  <p>a.	<strong>Force Majeure</strong></p>
				   <p><ul style="list-style: none">
				   <li><p>For purposes of this Agreement, <strong>&#39;&#39; Force Majeure &#39;&#39;</strong> means, without limitation, any acts of God, government, war, terrorism, riot, fire, floods, earthquakes, explosions, strikes, lockouts, cessation of labor, trade disputes, breakdowns, accidents of any kind or any other causes which are beyond the reasonable control of a party (including delay, interruptions, shortages or outages by the Company). Neither party shall be liable to the other party or be deemed in breach of this Agreement or any Contract by reason of delay or failure to perform if such delay or failure to perform was caused by Force Majeure. In the event of a Force Majeure event: </p></li>
				   <li><p>i. The party claiming Force Majeure shall, as soon as commercially practicable, notify the other party of such Force Majeure event provided the notifying party shall incur no liability for its failure to give such notice; </p></li>
				   <li><p>ii. The notifying party&#39;s duty to perform shall be suspended for the duration of the Force Majeure event; and</p></li>
				   <li><p>iii. The time of performance for the party impacted by the Force Majeure event shall be extended by a period equal to the duration of said Force Majeure event.</p></li>
				   </ul></p>
			<p>In the event a Force Majeure event should continue for more than ninety (90) days, either party may, by written notice to the other, cancel a Contract insofar as Cloud Services remain undelivered under the said Contract. Upon such cancellation, the Company shall have no obligation to deliver, and Reseller will have no obligation to accept delivery of or pay for, the undelivered Cloud Services, but the Contract shall remain in full force and effect regarding all Cloud Services delivered prior to the date of cancellation.</p>
						   </li>
				  <li>
                  <p>b.	<strong>Assignment</strong></p>
				   <p><ul style="list-style: none">
				   <li><p>Reseller may not transfer or assign this Agreement, in whole or in part, or delegate any of its duties hereunder, to a third party by change in control, operation of law or otherwise, without the prior written consent of the Company. The Company may assign this Agreement and/or any Purchase Order in whole or in part without the consent of Reseller. Company shall endeavor to provide prompt notice of any assignment to the Reseller. No assignment shall be effective unless</p></li>
				   <li><p>i. The assignor notifies the other party of the assignment in writing, and </p></li>
				   <li><p>ii. The assignee agrees in writing to abide by the terms of this Agreement.</p></li>
				   <li><p>Any assignment in violation of the foregoing shall be void. This Agreement shall inure to the benefit of the parties, their successors and permitted assignees. Reseller understands and agrees that, regardless of any such assignment, the rights and obligations of Company in this Agreement may accrue to, or be fulfilled by, any affiliate, as well as by Company and/or its subcontractors. </p></li>
	         </ul></p></li>
			<li><p>c. <strong>Waiver</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>No delay or failure of either party to enforce any provision of this Agreement will operate as a waiver of the right to enforce that or any other provision of this Agreement, nor will any single or partial exercise of any such rights preclude any other or further exercise thereof. To be effective, any waiver must be in writing, signed by the party providing the waiver. </p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>d. <strong>Modifications</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>Any amendment or modification of this Agreement must be made in writing and signed by authorized representatives of both of the parties hereto. </p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>e. <strong>Relationship of the Parties</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>In all matters relating to this Agreement, the relationship of the parties shall be that of independent contractors. Neither party will represent that it has any authority to assume or create any obligation, expressed or implied, on behalf of the other party. Nothing stated in this Agreement shall be construed as constituting Company and Reseller as partners or joint ventures, or as creating the relationship of employer and employee, principal and agent, or master and servant.</p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>f. <strong>Governing Law</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>This Agreement shall be governed by and construed in accordance with the laws of province of Ontario without regard to its conflicts of law principles. The United Nation&#39;s Convention on Contracts for the International Sale of Goods is specifically excluded from application to this Agreement. The parties agree that the courts ofthe province of Ontario, Canada, shall have sole and exclusive jurisdiction and venue over any matter arising out of this Agreement and each party hereby submits itself and its property to the venue and jurisdiction of such courts. Each party irrevocably waives any objection that it may now or hereafter have to the laying of venue of any such proceeding in such court, including any claim that such proceeding has been brought in an inappropriate or inconvenient forum. </p>
				</li>
				</ul>
			  </p>
			</li>
				<li><p>g. <strong>Severability</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>In the event that any provision of this Agreement is held by a court of competent jurisdiction to be invalid or unenforceable, the remaining provisions of the Agreement will remain in full force and effect, and shall be construed so as to best effectuate the intention of the parties in executing it. </p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>h. <strong>Survival</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>Any obligations which either expressly or by their nature are to continue after the termination or expiration of this Agreement shall survive and remain in effect. Without limiting the foregoing, the following sections shall survive and remain in effect: Sections 5(d), 9, 10 11, and 15. </p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>i. <strong>Notices</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>Any notices provided under this Agreement shall be deemed given three (3) days after sent by a national overnight delivery service or express mail, and immediately after personal delivery to the other party at the addresses shown above. Notice to Company shall be made to the attention of: Legal Department. </p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>j. <strong>Audit Rights</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>Reseller shall keep and maintain true and complete records pertaining to its performance of this Agreement or any Contract hereunder in sufficient detail to permit the Company to accurately determine whether Reseller has fully complied with their terms. Reseller shall make such records available upon reasonable notice, during regular business hours, for inspection and copying by Company and its representatives. Reseller shall maintain such records for at least two (2) years after the end of the calendar year to which they pertain. Reseller shall maintain such records for a period of two (2) years from the date of termination of Cloud Services. </p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>k. <strong>Construction</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>Neither party hereto shall be considered the drafter of this Agreement or any provision hereof for the purpose of any statute, case law, rule of interpretation or construction that would or might cause any provision or ambiguity to be construed against the drafter hereof. As used in these Agreement,</p>
					<p><ul style="list-style: none"><li><p>i. Any reference to a statute shall be construed as a reference to that statute as amended, re-enacted or otherwise modified from time to time, </p></li>
					<li><p>ii. The term &#39;&#39; including &#39;&#39; will always be deemed to mean &#39;&#39; including, without limitation &#39;&#39;,</p></li>
					<li><p>iii. A definition is equally applicable to the singular and plural forms of the feminine, masculine and neuter forms of the term defined, and</p></li>
					<li><p>iv. Any headings in the Agreement are for convenience only and shall not affect the interpretation of any terms.</p></li></ul></p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>l. <strong>Language</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>The parties agree that the English language shall be the controlling language of this Agreement. Each party hereby declares that it</p>
					<p><ul style="list-style: none"><li><p>i. Has had an opportunity to review and discuss the Agreement with legal counsel,</p></li>
					<li><p>ii. Has read and understood the entire text of the Agreement, and</p></li>
					<li><p>iii. Has a clear understanding of each and every of its terms and conditions.</p></li>
					</ul></p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>m. <strong>No Conflicts and Entire Agreement</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>This Agreement (together with all Contracts) constitutes the entire agreement between the parties with respect to the purchase of Cloud Services and supersedes any and all written or oral agreements previously existing between the parties and/or their affiliates with respect to the purchase of Cloud Services from Company. Any Other Agreements between the Reseller and theCompany shall not be affected by the execution of this Agreement and the terms of such Other Agreements shall remain in full force and effect with respect to the subject matter set forth in such Other Agreements. The Reseller acknowledges that it is not entering this Agreement on the basis of any representations not expressly contained herein. Every Contract between the Company and the Reseller or any of its subsidiaries shall be subject to the Agreement. Reseller may not purchase Cloud Services from the Company until completion of Company&#39;s credit application. In the event of any conflict between the terms and conditions of this Agreement and any applicable Addendum, the terms and conditions of the applicable Addendum shall prevail. </p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>n. <strong>Execution</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>This Agreement may be executed contemporaneously in one or more counterparts, each of which shall be deemed an original, but which together shall constitute one instrument. The parties may rely on a facsimile or scanned signature to bind the other party and may deliver such signatures electronically. </p>
				</li>
				</ul>
			  </p>
			</li>
			<li><p>o. <strong>Non-Solicitation</strong></p>
				<p>
				<ul style="list-style: none">
				<li><p>Except as otherwise agreed by the Company in writing, during the Term of this Agreement and for a period of twelve (12) months following the date of expiry/termination of this agreement hereunder, the Reseller hereby agrees not to solicit, induce or hire any employee of the Company involved in the marketing, promotion, sale or distribution of Cloud Services to Reseller to leave their employment or terminate or breach their contract for services with the Company, as the case may be. Notwithstanding the foregoing, solicitation of either party&#39;s current employees or independent contractors who are not involved in the performance of this Agreement by means of a general media solicitation or trade publication or advertisement shall not constitute a breach of this provision. </p>
				</li>
				</ul>
			  </p>
			</li>
            </ul>
            <hr>
            				
				
							  </div>
						  </div>  
					  </div>
					   <div class="mui-row">
						<div class="col-sm-8">
							<h5><a href="<?php echo base_url()?>login/logout">I do not agree to these terms.</a></h5>
						</div>
						<div class="col-sm-4">
						   <form action="<?php echo base_url()?>terms/check_terms_condition_update" method="post">
						      <button type="submit" class="mui-btn mui-btn--accent btn-sm btn-block pull-right">ACCEPT</button>
						   </form>
					    
					    </div>
					   </div>
					
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/mui.min.js"></script>
    
    <script>
         $(document).ready(function() {
           var height = $(window).height();
            $('#main_div').css('height', height);
         });
      </script>
</body>

</html>
