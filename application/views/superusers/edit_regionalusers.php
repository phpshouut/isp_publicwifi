

<!-- Start your project here-->
<?php $this->load->view('includes/header');?>
<div class="loading" style="display:none">
   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <?php
               $data['navperm']=$this->plan_model->leftnav_permission();
               $this->load->view('includes/left_nav');?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">Manage SuperUsers</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <?php $this->load->view('includes/global_setting',$data);?>
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid" id="right-container-fluid">
               <div class="add_user" style="padding-top:0px;">
                 <form action="<?php echo base_url()?>manageUsers/add_regional_data_todb" method="post" autocomplete="off">
                     <input type="hidden" name="user_id" value="<?php echo $edit_userid; ?>" />
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <br>
                       <div class="row">
                        <?php echo validation_errors(); ?>
                          <h2>Edit Regional User</h2>
                          <div class="form-group">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="mui-textfield mui-textfield--float-label">
                                         <input type="email" name="username" value="<?php echo $email;?>" required disabled>
                                         <label>Enter Email as Username<sup>*</sup></label>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="mui-textfield mui-textfield--float-label">
                                         <input type="text" name="firstname" value="<?php echo $name;?>" required>
                                         <label>First Name<sup>*</sup></label>
                                      </div>
                                   </div>
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="mui-textfield mui-textfield--float-label">
                                         <input type="text" name="lastname" value="<?php echo $lastname;?>" required>
                                         <label>Last Name<sup>*</sup></label>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="mui-textfield mui-textfield--float-label">
                                         <input type="number" name="phone" value="<?php echo $phone;?>" required>
                                         <label>Mobile Number <sup>*</sup></label>
                                      </div>
                                      <span style="display: inline-block;color:#a1a1a1">This will be treated as password to login</span>
                                   </div>
                                </div>
                                
                                   <?php
                                   if(strpos($assigned_region , ',') > 0){
                                       $statearr = explode(',' , $assigned_region);
                                       $i = 0;
                                       foreach($statearr as $sid){
                                          if($i == 0){
                                             echo '<div class="row">
                                                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                         <div class="mui-select">
                                                             <select name="regional_state[]" required>
                                                                <option value="">Select State*</option>'.
                                                                 $this->superusers_model->get_allstates($sid)
                                                             .'</select>
                                                          </div>
                                                      </div>
                                                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                         <span class="mui-btn mui-btn--small mui-btn--accent" onclick="addmorestate_toisp()">ADD MORE STATE</span>
                                                      </div>
                                                   </div>';
                                          }else{
                                             echo '<div class="row">
                                                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                         <div class="mui-select">
                                                             <select name="regional_state[]" required>
                                                                <option value="">Select State*</option>'.
                                                                 $this->superusers_model->get_allstates($sid)
                                                             .'</select>
                                                          </div>
                                                      </div>
                                                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                         <span class="mui-btn mui-btn--small mui-btn--accent" onclick="remove_stateoption(this)"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                      </div>
                                                   </div>';
                                          }
                                          $i++;
                                       }
                                   }
                                   ?>
                                <div class="morestate"></div>
                             </div>
                          </div>
                          <div class="form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                               <div class="row">
                                  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                    <input type="submit" class="mui-btn mui-btn--accent" value="SAVE">
                                  </div>
                                  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                    <a href="<?php echo base_url().'manageUsers' ?>" class="mui-btn mui-btn--accent" style="background-color:#222;">CANCEL</a>
                                  </div>
                               </div>
                            </div>
                         </div>
                       </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){   
        var height = $(window).height();
        //alert(height);
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);
        setTimeout(function (){
           $('.loading').css('display', 'none');
        },1000);
    });
</script>
<script>
    if ($(window)) {
        $(function () {
            $('.menu').crbnMenu({
                hideActive: true
            });
        });
    }
   //setting_menu_ul
    $(document).ready(function(){
        //class="active" style="display: block;"
        $("#setting_menu_ul").css("display","block");
        $("#team_menu").addClass("menu_active");
    });
    
   function addmorestate_toisp() {
      $.ajax({
         url: base_url+'manageUsers/getallstatelist',
         type: 'POST',
         success: function(data){
             $('.morestate').append('<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><div class="mui-select"><select name="regional_state[]" required><option value="">Select State *</option>'+data+'</select><label>Select STATE</label></div></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="mui-btn mui-btn--small mui-btn--accent" onclick="remove_stateoption(this)"><i class="fa fa-times" aria-hidden="true"></i></span></div></div>');
         }
      });    
   }
   
   function remove_stateoption(obj) {
      $(obj).parent().parent().remove();
   }
</script>
</body>
</html>

