
<!-- Start your project here-->
<?php $this->load->view('includes/header');?>
<div class="loading" style="display:none">
   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <?php
               $data['navperm']=$this->plan_model->leftnav_permission();
               $this->load->view('includes/left_nav');?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">Manage SuperUsers</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <?php $this->load->view('includes/global_setting',$data);?>
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid" id="right-container-fluid">
               <div class="add_user" style="padding-top:0px;">
                 <form action="<?php echo base_url()?>manageUsers/add_team_data_todb" method="post" autocomplete="off">
                     <input type="hidden" name="user_id" value="<?php echo $edit_userid; ?>" />
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <br>
                       <div class="row">
                        <?php echo validation_errors(); ?>
                          <h2>Edit SuperAdmin User</h2>
                          <div class="form-group">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="mui-textfield mui-textfield--float-label">
                                         <input type="email" name="username" value="<?php echo $email; ?>" required disabled>
                                         <label>Enter Email as Username<sup>*</sup></label>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="mui-textfield mui-textfield--float-label">
                                         <input type="text" name="firstname" value="<?php echo $name; ?>" required>
                                         <label>First Name<sup>*</sup></label>
                                      </div>
                                   </div>
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="mui-textfield mui-textfield--float-label">
                                         <input type="text" name="lastname" value="<?php echo $lastname; ?>" required>
                                         <label>Last Name<sup>*</sup></label>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                      <div class="mui-textfield mui-textfield--float-label">
                                         <input type="number" name="phone" value="<?php echo $phone; ?>" required>
                                         <label>Mobile Number <sup>*</sup></label>
                                      </div>
                                      <span style="display: inline-block;color:#a1a1a1">This will be treated as password to login</span>
                                   </div>
                                </div>
                             </div>
                          </div>
                          <div class="form-group">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:25px">
                               <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                     <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE">
                                  </div>
                               </div>
                            </div>
                         </div>
                       </div>
                    </div>
                 </form>
                </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){   
        var height = $(window).height();
        //alert(height);
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);
        setTimeout(function (){
           $('.loading').css('display', 'none');
        },1000);
    });
</script>
<script>
    if ($(window)) {
        $(function () {
            $('.menu').crbnMenu({
                hideActive: true
            });
        });
    }
   //setting_menu_ul
    $(document).ready(function(){
        //class="active" style="display: block;"
        $("#setting_menu_ul").css("display","block");
        $("#team_menu").addClass("menu_active");
    });
</script>
</body>
</html>

