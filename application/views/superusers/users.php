

<!-- Start your project here-->
<?php $this->load->view('includes/header');?>
<div class="loading" style="display:none">
   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <?php
               $data['navperm']=$this->plan_model->leftnav_permission();
               $this->load->view('includes/left_nav');?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">Manage SuperUsers</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <?php $this->load->view('includes/global_setting',$data);?>
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid" id="right-container-fluid">
               <div class="add_user" style="padding-top:0px;">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="mui--appbar-height"></div>
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                    <li class="mui--is-active Plan-details limiui">
                                       <a data-mui-toggle="tab" data-mui-controls="superusers-details">
                                          Manage Superadmin Users
                                       </a>
                                    </li>
                                    <li class="Plan-settings limiui">
                                       <a data-mui-toggle="tab" data-mui-controls="regionalusers-details" id="_regionaltabdetails" >
                                          Manage Regional Users
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <div class="mui--appbar-height"></div>
                                 <div class="mui-tabs__pane mui--is-active " id="superusers-details">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <a href="<?php echo base_url()?>manageUsers/add_users"><button class="mui-btn mui-btn--small mui-btn--accent  pull-right">+ ADD SuperAdmin User</button></a>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <div class="table-responsive">
                                             <table class="table table-striped">
                                                <thead>
                                                   <tr class="active">
                                                      <th>#</th>
                                                      <th>NAME</th>
                                                      <th>EMAIL</th>
                                                      <th>PHONE NO.</th>
                                                      <th>Password.</th>
                                                      <th  class="mui--text-center" colspan="3">ACTIONS</th>
                                                   </tr>
                                                </thead>
                                                <tbody id='search_gridview'>
                                                   <?php echo $list_data; ?>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="mui-tabs__pane " id="regionalusers-details">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <a href="<?php echo base_url()?>manageUsers/add_regionalusers"><button class="mui-btn mui-btn--small mui-btn--accent  pull-right">+ ADD Regional User</button></a>
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row">
                                          <div class="table-responsive">
                                             <table class="table table-striped">
                                                <thead>
                                                   <tr class="active">
                                                      <th>#</th>
                                                      <th>NAME</th>
                                                      <th>EMAIL</th>
                                                      <th>PHONE NO.</th>
                                                      <th>PASSWORD</th>
                                                      <th>REGION</th>
                                                      <th  class="mui--text-center" colspan="3">ACTIONS</th>
                                                   </tr>
                                                </thead>
                                                <tbody id='regional_gridview'>
                                                   <?php //echo $list_data; ?>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="change_pwdconfirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
               <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
            </h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p id="erromsg">Are you sure you want to Change password ?</p>
         </div>
         <form action="" method="post" id="update_pwd" autocomplete="off"  onsubmit="update_pwd(); return false;">
            <input type="hidden" id="teamid" name="teamid" value="">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mui-textfield mui-textfield--float-label">
                  <input type="text" name="changepwd" class="city_text" required>
                  <label>New Password</label>
              </div>
            </div>
            <div class="modal-footer" style="text-align: right">
               <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
               <input type="submit" class="mui-btn  mui-btn--small mui-btn--accent" value="Update">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="Deleteuser_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
   <input type="hidden" id="deluserid" value="">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
               <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
            </h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p>Are you sure you want to Delete User ?</p>
         </div>
         <div class="modal-footer" style="text-align: right">
            <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
         <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyesuser" >YES</button>
         </div>
      </div>
   </div>
</div>
 
 



<script type="text/javascript">
   $(document).ready(function(){   
      var height = $(window).height();
      //alert(height);
      $('#main_div').css('height', height);
      $('#right-container-fluid').css('height', height);
      setTimeout(function (){
         $('.loading').css('display', 'none');
      },1000);
        
      $(document).on('click','.changepwd',function(){
	 var id=$(this).attr('rel');
	 $('#teamid').val(id);
	 $('#change_pwdconfirmation_alert').modal('show');
      });
      $(document).on('click','.deletuser',function(){
         var deluserid=$(this).attr('rel');
         $('#deluserid').val(deluserid);
         $('#Deleteuser_confirmation_alert').modal('show');
      });
      $(document).on('click','.delyesuser',function(){
         var userid=$('#deluserid').val();
         $.ajax({
            url: base_url+'manageUsers/delete_user',
            type: 'POST',
            dataType: 'json',
            data: {userid:userid},
            success: function(data){
               $('#Deleteuser_confirmation_alert').modal('hide');
	       location.reload();
            }
         });         
      });
   });
</script>
<script>
    if ($(window)) {
        $(function () {
            $('.menu').crbnMenu({
                hideActive: true
            });
        });
    }
   //setting_menu_ul
    $(document).ready(function(){
        //class="active" style="display: block;"
        $("#setting_menu_ul").css("display","block");
        $("#team_menu").addClass("menu_active");
    });
    
   function update_pwd(){
      $('.loading').removeClass('hide');
      var teamid = $('#teamid').val();
      var formdata = $("#update_pwd").serialize();
        
      $.ajax({
         url: base_url+'manageUsers/update_userpwd',
         type: 'POST',
         dataType: 'json',
         data: formdata,
         success: function(data){
            if (data.resultcode==1) {
               $('.loading').addClass('hide');
               $('#change_pwdconfirmation_alert').modal('hide');
               $('.pwdupd'+teamid).html(data.origpwd);
            }
         }
      });  
   }
   
   $('body').on('click', '#_regionaltabdetails', function(){
      $('.loading').removeClass('hide');        
      $.ajax({
         url: base_url+'manageUsers/regionalteam_data',
         type: 'POST',
         dataType: 'json',
         success: function(data){
            $('.loading').addClass('hide');
            $('#regional_gridview').html(data);
         }
      });  
   });
   
</script>
</body>
</html>

