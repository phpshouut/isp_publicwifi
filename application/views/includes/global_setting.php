

<?php
   $user_active = ''; $plan_active = '';$topup_active='';$nas_active='';$report_active="";
   $setting_open = '';
   $setting_mgnt_team_user_active = '';$setting_biling_active = '';$locations='';$department='';$team=''; $myacct_active='';$isp_active="";$promotion_open = '';
   $home_promotion = '';
   $marketing_promotion = '';$sms_setting_active='';
   $listyle="";
   $pstyle="padding:2px 10px;";
   $license_active=''; $superusers_active = '';
   $controller = $this->router->fetch_class();
   $method=$this->router->fetch_method();
   //$navperm=$this->plan_model->leftnav_permission();
   //echo "<pre>"; print_R($navperm); die;
   if($controller == 'user'){
       $user_active = 'class="active"';
   	if($method=="edit")
   	{
   		$listyle="height:37px";
   		$pstyle="padding:7px 10px;";
   	}
   }elseif($controller == 'plan'){
       if($method=="topup" || $method=="add_topup" || $method=="edit_topup" || $method=="topupuser_stat" || $method=="topup_stat")
       {
       $topup_active = 'class="active"';
       }
       else
       {
            $plan_active = 'class="active"';
       }
   }
   else if($controller == 'nas')
   {
       $nas_active='class="active"';
   }
   else if($controller == 'manageLicense')
   {
       $license_active='class="active"';
   }
   else if($controller == 'report')
   {
      $report_active='class="active"'; 
   }
   else if($controller == 'setting'){
       $setting_open = 'open';
       if($method == 'index' || $method == ''){
           $setting_biling_active='class="active"';
       }
       else if($method=='smssetup')
       {
             $sms_setting_active='class="active"';                   
       }
       elseif($method == 'user_management'){
           $setting_mgnt_team_user_active='class="active"';
       }
   	 elseif($method == 'locations'){
           $locations = 'class="active"';
       }
       
        elseif($method == 'isp_detail'){
           $isp_active = 'class="active"';
       }
   
   
   }
   else if($controller == 'mgmt')
   {
   	$setting_open = 'open';
       $department='class="active"';
   }
   
   else if($controller == 'team')
   {
   	$setting_open = 'open';
      // $team='class="active"';
        if($method=="my_account")
       {
       $myacct_active = 'class="active"';
       }
       else
       {
            $team = 'class="active"';
       }
   }
   else if($controller == 'promotion'){
       $promotion_open = 'open';
       if($method == 'index' || $method == ''){
           $home_promotion='class="active"';
       }
       elseif($method == 'marketing_promotion'){
    $marketing_promotion='class="active"';
       }
       
   
   }else if($controller == 'manageUsers')
   {
       $superusers_active='class="active"';
   }
   $super_admin = $this->session->userdata['isp_session']['super_admin'];
   ?>
<li class="dropdown mui-btn--small mui-btn--accent" style="border-radius: 2px; margin-top: 10px; <?php echo $listyle;?>;">
   <a class="dropdown-toggle" data-toggle="dropdown" href="#" 
      style="background-color:none;color: #fff; font-weight: 400; <?php echo $pstyle;?>"><i class="fa fa-cogs" aria-hidden="true"></i>
   <span class="caret"></span></a>
   <ul class="dropdown-menu">
      <?php if(isset($navperm['MYACCOUNT']) && $navperm['MYACCOUNT']==0){?>
      <li id="myaccount_menu">
         <a href="<?php echo base_url().'team/my_account'?>" <?php echo $myacct_active ?>>
         <i class="fa fa-cog" aria-hidden="true"></i>
         My Account 
         </a>
      </li>
      <?php } ?>
      <?php if($super_admin == 1){ ?>
      <li><a href="<?php echo base_url().'manageUsers' ?> " <?php echo $superusers_active;?> >
         <i class="fa fa-users" aria-hidden="true"></i> Manage Users</a>
      </li>
      <?php } ?>
      <?php if($super_admin == 1){ ?>
      <li><a href="<?php echo base_url().'manageLicense' ?> " <?php echo $license_active;?> >
         <i class="fa fa-calculator" aria-hidden="true"></i> Manage Licences</a>
      </li>
      <?php } ?>
      <?php if(isset($navperm['BILLINGCYCLE']) && $navperm['BILLINGCYCLE']==0){?>
      <li id="billing_menu">
         <a href="<?php echo base_url().'setting'?>" <?php echo $setting_biling_active?>>
         <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
         Billing
         </a>
      </li>
      <li id="smssetup_menu">
         <a href="<?php echo base_url().'setting/smssetup'?>" <?php echo $sms_setting_active; ?>>
         <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
         Sms Gateway 
         </a>
      </li>
      <?php } ?>
      <?php if(isset($navperm['ISPSETUP']) && $navperm['ISPSETUP']==0){?>
      <li id="ispdetail_menu">
         <a href="<?php echo base_url().'setting/isp_detail'?>" <?php echo $isp_active ?>>
         <i class="fa fa-wrench" aria-hidden="true"></i>
         ISP Setup
         </a>
      </li>
      <?php } ?>
      <?php if(isset($navperm['SUPPORT']) && $navperm['SUPPORT']==0){?>
      <li id="ispdetail_menu">
         <a href="https://www.shouut.com/ticketsystem/index.php" target="_blank" >
         <i class="fa fa-ticket" aria-hidden="true"></i>
         SUPPORT
         </a>
      </li>
      <?php } ?>
      <li id="ispdetail_menu">
         <a href="<?php echo base_url().'login/logout'?>"  >
         <i class="fa fa-sign-out" aria-hidden="true"></i>
         Logout
         </a>
      </li>
   </ul>
</li>

