<div id="sidedrawer-brand" class="mui--appbar-line-height">
                      <?php
			$isplogo = $this->permission_model->get_isplogo();
			if($isplogo != '0'){
			   echo '<img src="'.$isplogo.'" class="img-responsive"/>';
			}else{
			
			   echo '<img src="'.base_url().'assets/images/isp_logo.png" class="img-responsive"/>';
			}
		     ?>
                  </div>
<?php
$plan_active='';$nas_active='';$data_use_active = '';$traffic_active = '';$legal_intervention_active = '';
$sessionReport_active = '';$wifi_session_failure_report = '';
$lms_active = '';
$traffic_report = '';
$channel_active = '';
$location_active='active';
$controller = $this->router->fetch_class();
$method=$this->router->fetch_method();
if($controller == 'plan'){
    $plan_active = 'active';
    $location_active='';
    
}
else if($controller == 'nas')
{
   $nas_active ='active';
           $location_active='';
}
elseif($controller == 'data_use'){
    $data_use_active ='active';
    $location_active='';
}
elseif($controller == 'traffic'){
    if($method == 'traffic_report'){
	$traffic_report ='active';
    }else{
	$traffic_active ='active';
    }
    
    $location_active='';
}
elseif($controller == 'legal_intervention'){
    $legal_intervention_active ='active';
    $location_active='';
}
elseif($controller == 'channel'){
    $channel_active ='active';
    $location_active='';
}
elseif($controller == 'sessionReport'){
 if($method == 'wifi_session_failure_report'){
	$wifi_session_failure_report ='active';
    }else{
	$sessionReport_active ='active';
    }
    
    $location_active='';
    
}
elseif($controller == 'location'){
 if($method == 'lms'){
	$lms_active ='active';
	 $location_active='';
    }
    
   
    
}



$sessiondata = $this->session->userdata('isp_session');
   $publicuser = isset($sessiondata['publicuser']) ? $sessiondata['publicuser'] : '0';
?>
<?php
if($publicuser == '1'){
	?>
	<aside class="main-sidebar">
   <section class="sidebar">
      <ul class="sidebar-menu">
         <li class="treeview <?php echo $location_active;?>">
            <a href="<?php echo base_url()?>location">
            <i class="fa fa-map-marker" aria-hidden="true"></i> Locations
            </a>
         </li>
      </ul>
   </section>
</aside>
	<?php 
}
else
{
		      ?>
		    
    <aside class="main-sidebar">
	<section class="sidebar">
	    <ul class="sidebar-menu">
		<li class="treeview <?php echo $location_active;?>">
		    <a href="<?php echo base_url()?>location">
			<i class="fa fa-map-marker" aria-hidden="true"></i> Locations
		    </a>
		</li>
		<li class="treeview <?php echo $plan_active?>">
		    <a href="<?php echo base_url().'plan'?>" >
			<i class="fa fa-wifi" aria-hidden="true"></i> Plans
		    </a>
		</li>
		<li class="treeview <?php echo $nas_active?>">
		    <a href="<?php echo base_url().'nas'?>" >
			<i class="fa fa-database" aria-hidden="true"></i> Nas
		    </a>
		</li>
		<!--<li class="treeview <?php echo $lms_active?>">
		    <a href="<?php echo base_url().'location/lms'?>" >
			<i class="fa fa-database" aria-hidden="true"></i> LMS
		    </a>
		</li>-->
		<li class="treeview <?php echo $data_use_active?>">
		    <a   href="<?php echo base_url().'data_use'?>" >
			<i class="fa fa-database" aria-hidden="true"></i> Data Use
		    </a>
		</li>
		<li class="treeview <?php echo $traffic_active?>">
		    <a   href="<?php echo base_url().'traffic'?>" >
			<i class="fa fa-database" aria-hidden="true"></i>User Report
		    </a>
		</li>
		<li class="treeview <?php echo $traffic_report?>">
		    <a   href="<?php echo base_url().'traffic/traffic_report'?>" >
			<i class="fa fa-database" aria-hidden="true"></i>Traffic Report
		    </a>
		</li>
		<li class="treeview <?php echo $legal_intervention_active?>">
		    <a href="<?php echo base_url()?>legal_intervention" >
			<i class="fa fa-file-text" aria-hidden="true"></i> Legal
		    </a>
		</li>
		<li class="treeview <?php echo $sessionReport_active?>">
		    <a href="<?php echo base_url()?>sessionReport" >
			<i class="fa fa-file-text" aria-hidden="true"></i> Wifi Session Report
		    </a>
		</li>
		
		<li class="treeview  <?php echo $wifi_session_failure_report?>">
		    <a href="<?php echo base_url()?>sessionReport/wifi_session_failure_report">
			<i class="fa fa-file-text" aria-hidden="true"></i> Wifi Session Failure <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report
		    </a>
		</li>
		<?php
		$permission = $this->permission_model->isp_portal_permission();
		$isp_uid = ISPID;
		$channel_name = "Channel";
		if($isp_uid == '193'){
		  $channel_name = "360 Channel";
		}
		if($isp_uid != '176' && $isp_uid != '178' && $isp_uid != '179' && $isp_uid != '180'){
		    if($permission['isp_channel_permission'] == '1'){
			?>
			<li class="treeview <?php echo $channel_active?>">
			    <a href="<?php echo base_url()?>channel" > 
				<i class="fa fa-file-text" aria-hidden="true"></i> <?php echo $channel_name?>
			    </a>
			</li>
			<?php
		    }
		}
		?>
		
		<!--li class="treeview">
		    <a href="#">
			<i class="fa fa-file-text" aria-hidden="true"></i> Settings
		    </a>
		</li-->
		<?php
		if($permission['isp_portal_permission'] == '1'){
		    ?>
		<li>
		    <a href="<?php echo HOMEWIFIPORTAL?>" class="def_btn" style="border-bottom:none">
			<i class="fa fa-exchange" aria-hidden="true"></i>
			Home Wi-Fi <i class="fa fa-angle-right pull-right" aria-hidden="true"></i>
		    </a>
		</li>
		    <?php
		}
		?>
		
		<!--<li><a href="<?php echo base_url()?>login/logout" class="def_btn"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>-->
				  </ul>
				  </section>
				  </aside>



  <?php 
}
?>
 <div class="footer">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <h5><?php
			    if(isset($this->session->userdata['isp_session']['isp_name'])){
			     echo $this->session->userdata['isp_session']['isp_name'];
			    }
				
				?>  <i class="fa fa-angle-right" aria-hidden="true"></i></h5>
                           <h5><small>198.168.0.1</small></h5>
                           <h5><small><?php echo date('d.m.y h:i') ?></small></h5>
                           <!--h5 style="font-size: 13px">Powered By:</h5-->
                        </div>
                     </div>
                     <!--div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                          <img src="<?php echo base_url()?>assets/images/shouut_logo.png" class="img-responsive"/>
                        </div>
                     </div-->
                  </div>