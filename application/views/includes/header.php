<?php

$fevicon = 'shouut-icons.png';
if(isset($this->session->userdata['isp_session']['fevicon_icon'])){
   $fevicon = $this->session->userdata['isp_session']['fevicon_icon'];
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="icon" href="<?php echo base_url()?>assets/images/<?php echo $fevicon?>" type="image/x-icon"/>
   <title><?php echo $this->session->userdata['isp_session']['isp_name']; ?> </title>
   <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css">
   <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php echo base_url()?>assets/css/mui.min.css" rel="stylesheet">
   <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
   <link rel="stylesheet" href="<?php echo base_url()?>assets/css/skins.min.css">
   <link href="<?php echo base_url()?>assets/css/jquery.multiselect.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/daterangepicker.css" />
   <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-material-datetimepicker.css" />
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/css/demo.css">
   
   <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-3.2.1.min.js"></script>
   <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
   <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url()?>assets/js/mui.min.js"></script>
   
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/moment-with-locales.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-material-datetimepicker.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.multiselect.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/daterangepicker.js"></script>
   
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/plan_misc.js"></script>
    <script>
      var base_url = "<?php echo base_url()?>";
    </script>
    <script type="text/javascript">
		   $(document).ready(function(){
			  $('#button-os').click(function(){
				 if($('#sidedrawer').hasClass('os-container')){
					 $('#sidedrawer').removeClass('os-container');
					 $('#sidedrawer').css("transform","translate(0px,0)");
					$('#content-wrapper').css('margin-left','0px');
				 }else{
					 $('#sidedrawer').addClass('os-container');
					 $('#sidedrawer').css("transform","translate(200px)");
					$('#content-wrapper').css('margin-left','160px');
				 }
			  });
			});
	   </script>
</head>

<body>