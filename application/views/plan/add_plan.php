<?php $this->load->view('includes/header'); ?>
<div class="loading hide" >
    <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
                
                 <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
               //  $this->view('left_nav',$data); ?>
 <?php $this->load->view('includes/left_nav');?>
            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Plans</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <?php $this->load->view('plan/plan_headerview',$data);?>
                        </div>
                    </div>
                </nav>
            </header>
            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="add_user">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                            <li class="mui--is-active Plan-details limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="Plan-details">
                                                    Plan Details
                                                </a>
                                            </li>
                                            <li class="Plan-settings limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="Plan-settings" class="" >
                                                    Plan Settings
                                                </a>
                                            </li>
                                           <!-- <li class="  Pricing limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="Pricing" class="" >
                                                    Pricing
                                                </a>
                                            </li>
                                            
                                             <li class="  Pricing limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="add-region" class="" >
                                                    Region
                                                </a>
                                            </li>
                                            
                                            <li class="Usage-stats limiui">
                                                <a data-mui-toggle="tab" data-mui-controls="Usage-stats" >
                                                    Usage Stats
                                                </a>
                                            </li>-->
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="mui--appbar-height"></div>
                                        <div class="mui-tabs__pane mui--is-active " id="Plan-details">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <form action="" method="post" id="add_plan" autocomplete="off" onsubmit="add_plan(); return false;">
                                                        <h2>Plan Details</h2>
                                                        <input type="hidden" id="plan_id" class="plan_id" name="plan_id" value="">
                                                           <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="0">

                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="text" name="plan_name" maxlength="20" required>
                                                                            <label>Plan Name<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="text" name="plan_desc" >
                                                                            <label>Plan Description</label>
                                                                        </div>
                                                                    </div>

                                                                    <!--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="row" style="margin-top:24px">
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                                <h4 class="toggle_heading"> Enable for Use</h4>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                                <label class="switch">
                                                                                    <input type="checkbox" >
                                                                                    <div class="slider round slide_usable"></div>
                                                                                </label>
                                                                            </div>
                                                                            <input type="hidden" name="is_usable" id="is_usable" value="0">
                                                                        </div>
                                                                    </div>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:5px;">
                                                                <h4>TYPE OF PLAN</h4>
                                                                 <div class="row" style="margin-top: 10px;">
                                                                 
                                                                 <label class="radio-inline">
                                                                        <input type="radio" required name="plan_type_radio"  value="TP" > Time Plan 
                                                                    </label>
                                                               
                                                                 <label class="radio-inline">
                                                                        <input type="radio" required name="plan_type_radio"  value="DP">  Data Plan 
                                                                    </label>
                                                                      <label class="radio-inline">
                                                                        <input type="radio" required name="plan_type_radio"  value="HP">Hybrid Plan 
                                                                    </label>
                                                               
                                                                  
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" id="user_type_ful_plan_enquiry" style="display: block">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:20px;">
                                                                <div class="row">
                                                                    
                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" name="dwnld_rate" required onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" />
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Download Rate<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" name="upld_rate" required>
                                                                            <span class="title_box">Kbps</span>
                                                                            <label>Upload Rate<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class ="row timeplan" style="margin-top:15px; display:none;">
                                                                    
                                                                 
                                                                    
                                                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" name="timelimit"  class="timelimit" id="state"  required>
                                                                             <span class="title_box">Minute</span>
                                                                            <label>Time Limit Minutes<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    
                                                              
                                                                </div>
                                                                <div class="row dataplan" style="margin-top: 15px; display:none;">
                                                                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number"  class="data_limit" min="0"  name="data_limit" placeholder="" >
                                                                            <span class="title_box">Mb</span>
                                                                            <label>Data Limit<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                     
                                                                   
                                                                </div>
                                                                
                                                                 <div class="row fupclass" style="display:none;">
                                                                     
                                                                     
                                                                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number"  class="data_limith" min="0"  name="data_limith" placeholder="" >
                                                                            <span class="title_box">Mb</span>
                                                                            <label>Data Limit<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                     
                                                                       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nopadding-left">
                                                                           <div class="mui-textfield mui-textfield--float-label">
							<input type="number" min="0" name="timelimith" class="timelimith" id=""   required>
							<span class="title_box">Minute</span>
                                                         <label>Time Limit Minutes<sup>*</sup></label>
                                                      </div>
                                                                    </div>
                                                                     
                                                                    
                                                                     
                                                                   
                                                                </div>
                                                                    
                                                                  
                                                                   
                                                                </div>
                                                            </div>
                                                           
                                                           
                                                           
                                                           
                                                           
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px;">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                         <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_submit"  value="NEXT" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                     
                                        <div class="mui-tabs__pane " id="Plan-settings">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                      <form action="" method="post" id="add_plan_setting" autocomplete="off" onsubmit="add_plan_setting(); return false;">
                                                        <input type="hidden"  class="plan_id" name="plan_id" value="1">
                                                         <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="0">
                                                          <h2>Plan Settings</h2>
                                                        <div class="form-group">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                        <div class="row">
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:5px">
                                                                                <h4 class="toggle_heading"> Enable Burst Mode</h4>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                                <label class="switch">
                                                                                    <input type="checkbox">
                                                                                    <div class="slider round sliderburst"></div>
                                                                                </label>
                                                                            </div>
                                                                            <input type="hidden" name="is_burst_enable" id="is_burst_enable" value="0">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <h5>Burst speed and burst threshhold should be greater than the plan main speed</h5>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left" style="padding-left:0px">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" class="dwnld_burst" name="dwnld_burst" >
                                                                             <span class="title_box">Kbps</span>
                                                                            <label>Download Burst Speed<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" class="upld_burst" name="upld_burst" >
                                                                             <span class="title_box">Kbps</span>
                                                                            <label>Upload Burst Speed<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" class="dwnld_threshold" name="dwnld_threshold" >
                                                                             <span class="title_box">Kbps</span>
                                                                            <label>Download Burst Threshold<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0" class="upld_threshold" name="upld_threshold" >
                                                                             <span class="title_box">Kbps</span>
                                                                            <label>Upload Burst Threshold*<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left" style="padding-left:0px">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="text" class="burst_time" pattern="([01]?[0-9]{2})(:[0-5][0-9]){1}" name="burst_time" >
                                                                             <span class="title_box">HH:mm</span>
                                                                            <label>Burst Time<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                   
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" min="0"  class="burst_priority" name="burst_priority">
                                                                            <label>Burst Priority<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                           
                                                        
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px;">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_setting" disabled value="NEXT" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mui-tabs__pane " id="Pricing">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <form action="" method="post" id="add_pricing" autocomplete="off" onsubmit="add_pricing(); return false;">
                                                         <input type="hidden"  class="plan_id" name="plan_id" value="1">
                                                          <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="0">
                                                        <h2>Plan Pricing</h2>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="gross_amt" class="gross_amt" name="gross_amt" required>
                                                                           <span class="title_box_left">
                                                            ₹
                                                            </span
                                                                        <label> Gross Amount<sup>*</sup></label>
                                                                        <span class="amterror"></span>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="tax" class="tax" value="<?php echo $tax->tax?>">
                                                               <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="number" class="tax" name="tax" required>
                                                                         <span class="title_box_left">
                                                            %
                                                            </span>
                                                                        <label> Tax<sup>*</sup></label>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>

                                          

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0px">
                                                                    <div class="mui-pricing">
                                                                        <h3>TOTAL AMOUNT</h3>
                                                                        <h2><small>₹</small><span id="net_amt"> 00</span>.00</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <input type="hidden" name="net_amount" id="net_amount" value="">

                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_pricing" disabled value="NEXT" >
                                                                </div>
                                                            </div>
                                                        </div>      
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mui-tabs__pane" id="Usage-stats">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  
                                                <div class="row">
                                                    Usage Stats
                                                </div>
                                            </div>
                                        </div>
                                        
                                         <div class="mui-tabs__pane" id="add-region">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <form action="" method="post" id="add_region" autocomplete="off" onsubmit="add_region(); return false;">
                                                  <input type="hidden"  class="plan_id" name="plan_id" value="1">
                                                   <input type="hidden" id="" class="is_frmedit" name="is_frmedit" value="0">
                                                    <div class="row">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                     <h4>Region</h4>
                     <!--<label class="radio-inline">
                     <input type="radio" required name="region" <?php echo ($region_type=="allindia")?"":"disabled";?> value="allindia" > All india
                     </label>-->
                     <label class="radio-inline">
                     <input type="radio" required name="region"   value="region" > Region wise
                     </label>
                  </div>
                                                </div>
                                                <div class='row '>
                                                    <input type="hidden" name="regiondat" id="regiondat" value="">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hide regionappend">
                     <div class="row xxxx">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                               
                              <!-- onchange="search_filter_city(this.value)"-->
                              <select name="state" class="statelist form-control" id="statelist"    required>
                                 <option value="">Select State</option>
                                 <?php echo $state_list; ?>
                              </select>
                              <!--<label>State</label>-->
                              <input type="hidden" class="statesel" name="statesel" value="">
                          
                        </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                              <select name="city" class="search_citylist form-control" required>
                                 <option value="all">All Cities</option>
                                 
                              </select>
                               <input type="hidden" class="citysel" name="citysel" value="">
                              <!--<label>City</label>-->
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                           <div class="">
                              <select name="zone" class="zone_list form-control" required>
                                 <option value="all">All Zones</option>
                                
                              </select>
                              <!--<label>Zone</label>-->
                           </div>
                        </div>
                          <div class="col-lg-3 col-md-3 col-sm-3 btnreg">
                           <button class="mui-btn mui-btn--small mui-btn--accent addregion">
                                 + ADD Region
                                 </button>
                              <span class="rem"></span>
                            
                        </div>
                         
                        <input type="hidden" class="valregion" rel="" data-stateid="all" data-cityid="all" data-zoneid="all" data-mapid="">
                     </div>
                                                       
                                                       
                  </div>  
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px; margin-top: 20px;">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_region" disabled value="SAVE" >
                                                                </div>
                                                            </div>
                                                        </div>   
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
   
    $(document).ready(function () {
   
       $('.tab_menu').css({'cursor' : 'not-allowed'});
               $('.tab_menu').attr('disabled','disabled');
        
        var height = $(window).height();
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);

        // on user type change change page(form)
        $('input[type=radio][name=plan_type_radio]').change(function () {
            if (this.value == 'FUP_Plan') {
                $("#user_type_ful_plan_enquiry").show();
            } else if (this.value == 'Unlimited') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Data_Usage') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Time_Usage') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Others') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Others') {
                $("#user_type_ful_plan_enquiry").hide();
            } else if (this.value == 'Offer_Plan') {
                $("#user_type_ful_plan_enquiry").hide();
            }
        });
    
        
        //zone script
        
     
        
        $(document).on('click','.addregion',function(){
            
         // $(this).closest('.row').clone().appendTo('.regionappend').find('.rem').html("<button class='mui-btn mui-btn--small mui-btn--accent remregion'>-</button>");  
            var $block = $(this).closest('.row');

            var $clone = $block.clone();

       
    $clone.find('option:selected').prop("selected", false);
    $clone.find('.search_citylist').html('').append('<option  value="all">All Cities</option>');
    $clone.find('.zone_list').html('').append('<option  value="all">All Zones</option>');
    $clone.find('.valregion').attr('data-zoneid','');
    $clone.find('.valregion').attr('data-mapid','');
    $clone.find('.valregion').attr('data-cityid','');
    $clone.find('.valregion').attr('data-stateid','');
    $clone.appendTo('.regionappend').find('.rem').html("<button class='mui-btn mui-btn--small mui-btn--accent remregion'>-</button>");  
          return false;
        });
        
        $(document).on('click','.remregion',function(){
            
            $(this).closest('.row').remove();
        });
        
         $(document).on('click','input:radio[name="region"]',function(){
         
         if($(this).val()==="region")
         {
             $('.regionappend').removeClass('hide');
              $('.statelist').attr('required',true);
              $('.search_citylist').attr('required',true);
              $('.zone_list').attr('required',true);
         }
         else
         {
              $('.regionappend').addClass('hide');
               $('.statelist').attr('required',false);
               $('.search_citylist').attr('required',false);
              $('.zone_list').attr('required',false);
         }
         
        });
        
      $(document).on('change','.statelist',function(){
          var stateid=$(this).val();
          var $this=$(this);
         //  $('.statesel').val(stateid);
           $(this).closest('.row').find('.statesel').val(stateid);
          $this.closest('.row').find('.valregion').data('stateid',stateid);
           $this.closest('.row').find('.valregion').data('cityid','all');
            $this.closest('.row').find('.valregion').data('zoneid','all');
          $.ajax({
        url: base_url+'plan/getcitylist',
        type: 'POST',
        dataType: 'text',
        data: 'stateid='+stateid,
        success: function(data){
        
           $this.closest('.row').find('.search_citylist').empty();
             $this.closest('.row').find('.search_citylist').append(data);
        }
        
        
    });
         
      });
      
       $(document).on('change','.search_citylist',function(){
          var cityid=$(this).val();
          if($(this).val()=="addc")
          {
            $('.city_text').val('');
            var state_id=$(this).closest('.row').find('.statelist').val();
            $('.state_id').val(state_id);
            $('#add_city').modal('show'); 
            return false;
          }
          var stateid=$(this).closest('.row').find('.statelist').val();
          var $this=$(this);
          $(this).closest('.row').find('.citysel').val(cityid);
       
       
          $this.closest('.row').find('.valregion').data('cityid',cityid);
          $.ajax({
        url: base_url+'plan/getzonelist',
        type: 'POST',
        dataType: 'text',
        data: 'cityid='+cityid+'&stateid='+stateid,
        success: function(data){
         
           $this.closest('.row').find('.zone_list').empty();
             $this.closest('.row').find('.zone_list').append(data);
        }
        
        
    });
         
      });
      
      
       $(document).on('change','.zone_list',function(){
           if($(this).val()=="addz"){
                        $('.zone_text').val('');
                 var state_id=$(this).closest('.row').find('.search_citylist').val();
                 var city_id=$(this).closest('.row').find('.search_citylist').val();
                 $('.state_id').val(state_id);
                 $('.city_id').val(city_id);
                $('#add_zone').modal('show'); 
                return false;
           }
           
          var zoneid=$(this).val();
          var $this=$(this);
            $this.closest('.row').find('.valregion').data('zoneid',zoneid);
         
      });
        
     /*  $(document).on('change','.search_citylist',function(){
        
    // $('.zncity').change(function(){
            alert('xxxxxx');
           var cityid=$(this).val(); 
         getZonelist(cityid) ;
        });*/
        
        
        
      
         
         
        
        

    });
    
function add_region()
{
    	var regionarr=[];
        $( ".valregion" ).each(function() {
            var stateid=$(this).data('stateid');
            var cityid=$(this).data('cityid');
            var zoneid=$(this).data('zoneid');
             var mapid=$(this).data('mapid');
            var fd=stateid+"::"+cityid+"::"+zoneid+"::"+mapid;
            	regionarr.push(fd);
});
$('#regiondat').val(regionarr);
      var formdata = $("#add_region").serialize();
      
      
      
   // alert(data);
    $('.loading').removeClass('hide');
     $.ajax({
        url: base_url+'plan/add_region',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          //  alert(data);
               window.location = base_url+"plan";
           $('.loading').addClass('hide');
          // mui.tabs.activate('Plan-settings');
             
         
        }
  });
}
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            time: false,
            clearButton: true
        });
        
         $('.time').bootstrapMaterialDatePicker
   ({
    date: false,
    shortTime: false,
    format: 'HH:mm:ss'
   });

        $('.date-start').bootstrapMaterialDatePicker({
            weekStart: 0, format: 'DD-MM-YYYY HH:mm', shortTime: true
        })
                .on('change', function (e, date) {
                    $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
                });

        $('.min-date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY HH:mm', minDate: new Date()});

       // $.material.init()
        
      
    });
</script>
<script type="text/javascript">
    function toggleChevron(e) {
        $(e.target)
                .prev('.panel-heading')
                .find("i.")
                .toggleClass('fa fa-caret-down fa fa-caret-right');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);
    
    
   
    
</script>
<script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
        

      </script>
       <?php $this->load->view('plan/Addzone_view')?>
 </body>
</html>