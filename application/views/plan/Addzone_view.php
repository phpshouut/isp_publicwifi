<?php 

//$state_list = $this->setting_model->state_list_dropdown(); ?>
<div class="modal fade" id="add_zone" role="dialog"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="resetzonevalue();">&times;</button>
                <h4 class="modal-title"><strong>ADD ZONE</strong></h4>
            </div>
            <form action="" method="post" id="addzone_fly" autocomplete="off" onsubmit="add_zone(); return false;">
                <div class="modal-body" style="padding-bottom:5px">
                    <p>Add a new zone to your location listing.</p>
                    <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" name="zone_name" class="zone_text" required>
                        <label>Zone Name</label>
                    </div>
                    <input type="hidden" class="state_id" name="state_id" value="">
                     <input type="hidden" class="city_id" name="add_zone_city" value="">
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" onclick="resetzonevalue();" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                      <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="ADD ZONE">
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="add_city" role="dialog"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" onclick="resetcityvalue();" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>ADD CITY</strong></h4>
            </div>
            <form action="" method="post" id="addcity_fly" autocomplete="off"  onsubmit="add_city(); return false;">
                <div class="modal-body" style="padding-bottom:5px">
                    <p>Add a new City to your location listing.</p>
                    <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" name="city_add" class="city_text" required>
                        <label>City Name</label>
                    </div>
                    <input type="hidden" class="state_id" name="add_city_state" value="">
                  
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button"  onclick="resetcityvalue();" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                      <input type="submit" class="mui-btn mui-btn--large mui-btn--accent" value="ADD CITY">
                </div>
            </form>

        </div>
    </div>
</div>



<div class="modal fade" id="taxerr" role="dialog"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            
            
                <div class="modal-body" style="padding-bottom:5px">
                    <p id="taxmsg">Please enter Tax before.</p>
                   
                  
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button"   class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                     
                </div>
           

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        
    
   /* $(document).on('click','.addzonefly',function(){
        // alert($(this).prevAll('.addzonefly').length);
        $('.zone_text').val('');
        var state_id=$(this).closest('.row').find('.search_citylist').val();
        var city_id=$(this).closest('.row').find('.search_citylist').val();
        $('.state_id').val(state_id);
        $('.city_id').val(city_id);
       // alert($(this).parents().find('.xxxx').prevAll('.xxxx').html());
       $('#add_zone').modal('show'); 
    });*/
    
  $('#add_zone_state').change(function(){
   
        $('.loading').show();
        var state_id = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>setting/state_city_list",
            cache: false,
            data: "state_id="+state_id,
            success: function(data){
                $('.loading').hide();
                $('#add_zone_city').html(data);
                $('#add_franchisee_city').html(data);
            }
        });
    });
    
 
    
    
    
    
    });
    
    
      function add_zone()
     {
         $('.loading').removeClass('hide');
         //alert('zzzzzzzz');
        var formdata = $("#addzone_fly").serialize();
        // alert(formdata);
         $.ajax({
        url: base_url+'nas/add_zonedata',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
           alert(data.html);
                      $("input[class='citysel'][value='"+data.city_id+"']").closest('.row').find('.zone_list').append(data.html);
                      $('.loading').addClass('hide');
                        $('#add_zone').modal('hide'); 
              // alert($("input[class='citysel'][value='"+data.city_id+"']").closest('.row').html());
          
        }
    });
         //alert(formdata);
     }
    
      function add_city()
     {
         $('.loading').removeClass('hide');
         //alert('zzzzzzzz');
        var formdata = $("#addcity_fly").serialize();
        // alert(formdata);
         $.ajax({
        url: base_url+'nas/add_citydata',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          //  alert(data.html);
                      $("input[class='statesel'][value='"+data.state_id+"']").closest('.row').find('.search_citylist').append(data.html);
                      $('.loading').addClass('hide');
                        $('#add_city').modal('hide'); 
              // alert($("input[class='citysel'][value='"+data.city_id+"']").closest('.row').html());
          
        }
    });
         //alert(formdata);
     }
     
     
     function resetcityvalue() {
    $('.search_citylist').val('');
}
function resetzonevalue() {
    $('.zonelist').val('');
}
    </script>