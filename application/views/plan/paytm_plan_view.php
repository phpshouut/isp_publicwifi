<!-- Start your project here-->
<?php $this->load->view('includes/header');

//echo "<pre>"; print_R($plan_detail); die;
//echo "<pre>"; print_R($loc_list); 
 //die;
?>
<div class="loading hide" >
    <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
                

               <?php
	       $data['navperm']=$this->plan_model->leftnav_permission();
	       $this->load->view('includes/left_nav');?>
            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Plans</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <?php $this->load->view('plan/plan_headerview',$data); ?>
                        </div>
                    </div>
                </nav>
            </header>


            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="right_side" style="height:auto; padding-bottom: 0px;">
                        
                    </div>
                    <div class="add_user" style="padding-top:0px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <div class="row">

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                      <form action="<?php echo base_url()."plan/add_paytm_plan"?>" method="post" id="add_paytm_plan" >
                                                        
                                                          <h2>Paytm Plan Mapping</h2>
                                                        
                                                        <div class="form-group">
                                                            
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left" style="padding-left:0px">
                                                                        <select name="plan" class="plan form-control" id="plan"    required>
																			 <option value="">Select Plan</option>
																			 <?php if(count($plan_list)>0){
																				 foreach($plan_list as $planval){
																					 $selected=(isset($plan_detail['srvid']) && $planval['srvid']==$plan_detail['srvid'])?"selected":"";
																					 
																					 ?>
																					  <option value="<?php echo $planval['srvid']?>" <?php echo $selected;?>><?php echo $planval['srvname']?></option>
																				<?php }
																				 
																			 }?>
																			
																		  </select>
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <select name="location" class="loclist form-control" id="loclist"    required>
																			 <option value="">Select Location</option>
																			 <?php if(count($loc_list)>0){
																				 foreach($loc_list as $locval){
																					 $selected=($locval['is_selected']==1)?"selected":"";
																					 ?>
																					  <option value="<?php echo $locval['location_uid']?>" <?php echo $selected;?>><?php echo $locval['location_name']?></option>
																				<?php }
																				 
																			 }?>
																		  </select>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" required min="0"  value="<?php echo (isset($plan_detail['price']))?$plan_detail['price']:0; ?>" class="price" name="price" >
                                                                             <span class="title_box"></span>
                                                                            <label>Price<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <div class="mui-textfield mui-textfield--float-label">
                                                                            <input type="number" required min="0" value="<?php echo (isset($plan_detail['beans']))?$plan_detail['beans']:0; ?>" class="beans" name="beans" >
                                                                             <span class="title_box"></span>
                                                                            <label>Beans<sup>*</sup></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                           
                                                            
                                                           
                                                        
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="padding-left:0px;">
                                                                <div class="row">
                                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                        <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block btn_setting"  value="NEXT" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                       
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="mui--appbar-height"></div>
                                        <div class="mui-tabs__pane mui--is-active" id="pane-default-1">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="row" style="color:#f00; font-size:15px; text-align:center"><?php echo isset($_SESSION['planerror']) ? $_SESSION['planerror'] : ""; ?></div>
                                                <div class="row">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr class="active">
                                                                    <th>&nbsp;</th>
                                                                    <th>PLAN NAME</th>
                                                                     <th>Location Name</th>
																	 <th>Price Type</th>
                                                                    <th>Price </th>
                                                                    <th>Beans</th>
																	<th colspan="2" class="mui--text-right">ACTIONS</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id='search_gridview'>

                                                                <?php
                                                                $i = 1;
                                                                if(count($mapped_list)>0){
                                                                foreach ($mapped_list as $valdata) {

                                                                   ?>
                                                                    <tr>
                                                                        <td><?php echo $i; ?></td>
                                                                        <td><?php echo $valdata['srvname']?></td>
                                                                       <td><?php echo $valdata['location_name']?></td>
                                                                        <td><?php echo ($valdata['price_type']==1)?"Paid":"Free"?></td>
                                                                        <td><?php echo $valdata['price']?></td>
                                                                        <td><?php echo $valdata['beans']?></td>
    
   
                                                                        <td> <a href="<?php echo base_url() . "plan/paytm_plan/".$valdata['srvid']?>?loc_uid=<?php echo $valdata['location_uid']?>">Edit</a></td> 
                                                                        <td><!--<a >Delete</a>--></td> 
                                                                    </tr>
                                                                        <?php
                                                                        $i++;
                                                                    }
                                                                }
                                                                    ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

   

<div class="modal fade" id="Deleteplan_confirmation_alert" role="dialog" data-backdrop="static" data-keyboard="false">
    <input type="hidden" id="delplanid" value="">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                </h4>
            </div>
            <div class="modal-body" style="padding-bottom:5px">
                <p id="erromsg">Are you sure you want to Delete Plan ?</p>
            </div>
            <div class="modal-footer" style="text-align: right">
                <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
                <button type="button" class="mui-btn  mui-btn--small mui-btn--accent delyes" >YES</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('body').on('click', '#searchclear', function () {
            $('#searchtext').val('');
            $('#searchtext').addClass('serch_loading');
            search_filter('', '', '1');

        });

        $('body').on('keyup', '#searchtext', function () {
            $('#searchtext').addClass('serch_loading');
            search_filter('', '', '1');
        });

        $(document).on('click', '.deletplan', function () {
            $('#erromsg').html("Are you sure you want to Delete Plan ?");
            $('.delyes').removeClass('hide');
            $('.loading').removeClass('hide');
            var delplanid = $(this).attr('rel');
            $('#delplanid').val(delplanid);
            $.ajax({
                url: base_url + 'plan/check_plan_deletable',
                type: 'POST',
                dataType: 'json',
                data: {delplanid: delplanid},
                success: function (data) {
                      $('.loading').addClass('hide');
                    if (data == 1)
                    {
                        $('#Deleteplan_confirmation_alert').modal('show');
                    } else
                    {
                        $('#Deleteplan_confirmation_alert').modal('show');
                        $('#erromsg').html("Plan Can't be deleted already assigned to user");
                        $('.delyes').addClass('hide');
                    }

                    // $('#Deletenas_confirmation_alert').modal('hide');

                    //  location.reload();


                }
            });



        });

        $(document).on('click', '.delyes', function () {
            $('.loading').removeClass('hide');
            var planid = $('#delplanid').val();
            $.ajax({
                url: base_url + 'plan/delete_plan',
                type: 'POST',
                dataType: 'json',
                data: {planid: planid},
                success: function (data) {

                    $('#Deleteplan_confirmation_alert').modal('hide');

                    location.reload();


                }
            });

        });

    });


    function getcitylist(stateid) {
        $.ajax({
            url: base_url + 'plan/getcitylist',
            type: 'POST',
            dataType: 'text',
            data: 'stateid=' + stateid + '&is_addable=0',
            success: function (data) {

                $('.search_citylist').empty();
                $('.search_citylist').append(data);
            }
        });
    }

    function getzonelist(cityid) {
        var stateid = $('.statelist').val();
        $.ajax({
            url: base_url + 'plan/getzonelist',
            type: 'POST',
            dataType: 'text',
            data: 'cityid=' + cityid + '&stateid=' + stateid + '&is_addable=0',
            success: function (data) {

                $('.search_zonelist').empty();
                $('.search_zonelist').append(data);
            }
        });
    }

    function search_filter(data = '', filterby = '', keyup = '') {
        var searchtext = $('#searchtext').val();
        if (keyup == "")
        {
            $('.loading').removeClass('hide');
        }
        $('#search_panel').addClass('hide');
        $('#allsearch_results').removeClass('hide');


        var formdata = '';
        //  var city = $('select[name="filter_city"]').val();
        // var zone = $('select[name="filter_zone"]').val();
        if (filterby == 'state') {
            $('.search_citylist').find('option:selected').prop("selected", false);
            $('.search_zonelist').find('option:selected').prop("selected", false);
            getcitylist(data);
            var city = "all";
            var zone = "all";
            formdata += '&state=' + data + '&city=' + city + '&zone=' + zone;
        } else if (filterby == 'city')
        {
            $('.search_zonelist').find('option:selected').prop("selected", false);
            getzonelist(data);
            var state = $('select[name="filter_state"]').val();
            var zone = "all";
            formdata += '&state=' + state + '&city=' + data + '&zone=' + zone;
        } else {
            var state = $('select[name="filter_state"]').val();
            var city = $('select[name="filter_city"]').val();
            var zone = $('select[name="filter_zone"]').val();
            formdata += '&state=' + state + '&city=' + city + '&zone=' + zone;
        }

        //alert(formdata);
        $.ajax({
            url: base_url + 'plan/viewplan_search_results',
            type: 'POST',
            dataType: 'json',
            data: 'search_user=' + searchtext + formdata,
            success: function (data) {
                $('.loading').addClass('hide');
                $('#search_count').html(data.total_results + ' <small>Results</small>');
                $('#search_gridview').html(data.search_results);
                $('#searchtext').removeClass('serch_loading');

            }
        });
    }
</script>
<script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
        

      </script>

 </body>
</html>