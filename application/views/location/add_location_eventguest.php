<?php $this->load->view('includes/header');?>
<style>

.table-fixed-tbody {
    max-height: 350px !important;
    overflow-y: auto;
    width: 100%;
}
.progress_overlay {
position:fixed;
z-index:99999;
width:99%;
overflow:auto;

}

.progress_overlay:before {
content:'';
display:block;
position:fixed;
bottom:0;
left:0;
width:100%;
height:100%;
/*background-color:rgba(0,0,0,0.6)*/
}

.dtp-picker-datetime{
    width:200px;
    height: 200px;
    margin: auto;
}
.modal-dropzone{ background-color: #eeeeee; text-align: center; position: relative; border: 1px solid #dddddd; display: inline-block;  }
.modal-dropzone:after{ content: 'Drag & Drop Image or Click to Upload'; font-size: 12px !important; color: #bbbbbb; position: absolute;  left: 0; width: 100%; text-align: center; z-index:0;padding:10%; }
.modal-dropzone:before{ content: ''; font-family:"Glyphicons Halflings"; font-size:20px; color: #dbdbdb; position: absolute; top: 40%; left: 0; width: 100%; text-align: center; z-index:0;}
</style>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/responsiveCarousel.min.js"></script>
   <div style="display:none" class="wikipedia_temp_data"></div>
   <div class="loading" >
      <img src="<?php echo base_url()?>assets/images/loader.svg"/>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select os-container">
               
               <?php
	       $data['navperm']=$this->plan_model->leftnav_permission();
	       $this->load->view('includes/left_nav');?>
            </div>
            <header id="header">
               <nav class="navbar navbar-default">
		  <div class="container-fluid">
		     <div class="navbar-header">
			<p class="navbar-brand">Locations</p>
                     </div>
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			  
			   <?php $this->load->view('includes/global_setting',$data);?>
                        </ul>
                     </div>
                  </div>
               </nav>
            </header>

            <div id="content-wrapper" >
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="os_right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <div class="col-sm-6 col-xs-6">
                                    <h2>
				       Add New Location (
                                       <?php
                                       $location_type_val = '1';
                                       if($location_type == 'public'){
                                          echo 'Public Space';
                                          $location_type_val = '1';
                                       }
                                       elseif($location_type == 'hotel'){
                                          echo 'Hotel';
                                          $location_type_val = '2';
                                       }
                                       elseif($location_type == 'hospital'){
                                          echo 'Hospitals & Healthcare ';
                                          $location_type_val = '3';
                                       }
                                       elseif($location_type == 'institutional'){
                                          echo 'Institutions & Colleges';
                                          $location_type_val = '4';
                                       }
                                       elseif($location_type == 'enterprise'){
                                          echo 'Enterprise';
                                          $location_type_val = '5';
                                       }
                                       elseif($location_type == 'cafe'){
                                          echo 'Cafe & Restaurants';
                                          $location_type_val = '6';
                                       }
                                       elseif($location_type == 'retail'){
                                          echo 'Retail';
                                          $location_type_val = '7';
                                       }
				       elseif($location_type == 'custom'){
                                          echo 'Custom Location';
                                          $location_type_val = '8';
                                       }
				       elseif($location_type == 'purple'){
                                          echo 'Purple Box';
                                          $location_type_val = '9';
                                       }
				       elseif($location_type == 'offline'){
                                          echo 'Offline';
                                          $location_type_val = '10';
                                       }
				       elseif($location_type == 'visitor'){
                                          echo 'Visitor Management';
                                          $location_type_val = '11';
                                       }
				       elseif($location_type == 'contestifi'){
                                          echo 'Contestifi';
                                          $location_type_val = '12';
                                       }
				       elseif($location_type == 'retail_audit'){
                                          echo 'Retail Audit';
                                          $location_type_val = '13';
                                       }
				       elseif($location_type == 'event_module'){
                                          echo 'Event Module';
                                          $location_type_val = '14';
                                       }
				       elseif($location_type == 'lms'){
                                          echo 'LMS';
                                          $location_type_val = '16';
                                       }elseif($location_type == 'eventguest'){
                                          echo 'Event Guest management';
                                          $location_type_val = '18';
                                       }
                                       ?>
                                       )
				    </h2>
				 </div>
				 <div class="col-sm-6 col-xs-6 text-right">
				    <h2>
				       <small style="font-size: 19px">Location ID: <?php echo $locationid->location_id?> <span>(auto-generated)</span></small>
				       <span class="mui-btn---os">
					  <a href="<?php echo base_url()?>location"><img src="<?php echo base_url()?>assets/images/close.svg""></a>
                                       </span>
				    </h2>
				 </div>
			      </div>
                           </div> 
                           <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="mui---osbar-height"></div>
				 <div class="row">
				    <div class="col-lg-12 col-md-12 col-sm-12">
				       <ul class="mui-tabs__bar">
                                          <li class="mui--is-active">
                                             <a data-mui-toggle="tab" data-mui-controls="Location_details" >
                                             Location Details
                                             </a>
                                          </li>
                                          <li>
                                             <a data-mui-toggle="tab" data-mui-controls="captive_portal" class="tab_menu" onclick="get_location_cp_path()">
                                             Captive Portal
                                             </a>
                                          </li>
					  <li id="content_builder_tab_li" style="display:none">
                                             <a data-mui-toggle="tab" data-mui-controls="content_builder" onclick="content_builder()" class="tab_menu" >
                                             Content Builder
                                             </a>
                                          </li>
					  <li id="contestifi_content_builder_tab_li">
                                             <a data-mui-toggle="tab" data-mui-controls="contestifi_content_builder" onclick="contestifi_content_builder()" class="tab_menu" >
                                             Content Builder
                                             </a>
                                          </li>
                                          <li id="network_tab">
                                             <a data-mui-toggle="tab" data-mui-controls="nas_setup" class="tab_menu" onclick="nas_setup_list()">
                                             Network
                                             </a>
                                          </li>
                                         
                                          <li id="content_builder_tab_coupon">
                                             <a data-mui-toggle="tab" data-mui-controls="Vouchers" class="tab_menu" onclick="voucher_list()">
                                             Coupon
                                             </a>
                                          </li>
                                          <li id="content_builder_tab_termconditon">
                                             <a data-mui-toggle="tab" data-mui-controls="terms_of_use" class="tab_menu" onclick="terms_of_use()">
                                             Terms Of Use
                                             </a>
                                          </li>
					  <li id="content_builder_tab_sms">
                                             <a data-mui-toggle="tab" data-mui-controls="sms" class="tab_menu" onclick="sms_list()">
                                             SMS
                                             </a>
                                          </li>
                                       </ul>
				    </div>
				    <div class="col-lg-12 col-md-12 col-sm-12">
				       <div class="mui--osbar-height"></div>
                                       
                                       
                                       
                                       
                                       <!-- location tab start -->
					  <div class="mui-tabs__pane mui--is-active" id="Location_details">
					     <div class="row">
						<div class="col-lg-7 col-sm-7">
                                                   <div class="row">
						      <div class="col-sm-12 col-xs-12">
							 <h4>LOCATION DETAILS</h4>
						      </div>	
						   </div>
						   <div class="row">
						      <form id="add_location" onsubmit="add_location(); return false;">
                                                         <input type="hidden" name="created_location_id" id="created_location_id">
                                                         <input type="hidden" name="location_name_hidden" id="location_name_hidden">
                                                         <input type="hidden" name="location_id_hidden" id="location_id_hidden">
                                                         <input type="hidden" name="email_hidden" id="email_hidden">
                                                         <input type="hidden" name="mobile_hidden" id="mobile_hidden">
                                                         
                                                         <input type="hidden" name="location_type" id="location_type" value="<?php echo $location_type_val?>"?>
                                                         <input type="hidden" name="location_id" id="location_id" value="<?php echo $locationid->location_id?>"?>
							 <div class="row">
                                                            <div class="col-sm-12 col-xs-12">
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="location_name" id="location_name" required>
								     <label>Location Name<sup>*</sup></label>
								  </div>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" placeholder="" id="geolocation" name="geoaddr" required>
                                                                     <input type="hidden"  id="placeid" name="placeid"  readonly>
                                                                     <input type="hidden"  id="lat" name="lat" readonly>
                                                                     <input type="hidden" id="long" name="long"  readonly>
								     <label>Geo Location Lookup<sup>*</sup></label>
								  </div>
							       </div>
							    </div>
							 </div>
							 <div class="row">
							    <div class="col-sm-12 col-xs-12">
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="address1" id="address1" required>
								     <label>Location Address<sup>*</sup></label>
								  </div>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="address2" id="address2" required>
								     <label>Address Line 2<sup>*</sup></label>
								  </div>
							       </div>
							    </div>
							 </div>
							 <div class="row">
							    <div class="col-sm-12 col-xs-12">
							       <div class="col-sm-6 col-xs-6">
                                                                  <select class="form-control" name="state" id="state" required>
                                                                     <option value="">All States</option>
                                                                        <?php
                                                                      
                                                                           $state_list = $this->location_model->state_list();
                                                                          
                                                                           if($state_list->resultCode == 1){
                                                                              foreach($state_list->state as $state_list_view){
                                                                                 echo '<option value = "'.$state_list_view->state_id.'">'.$state_list_view->state_name.'</option>';
                                                                              }
                                                                           }
                                                                        ?>
                                                                  </select>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <div class="row">
								     <div class="col-sm-7 col-xs-7">
									<select class="form-control" name="city" id="city" required>
									   <option value="">Select City</option>
									</select>
								     </div>
								     <div class="col-sm-5 col-xs-5">
									<h5 class="so-add_headings" onclick="add_city_model()">
									   Add New City
									</h5>
								     </div>
                                                                  
								  </div>
							    </div>
							 </div>
							 </div>
							 <div class="row">
							    <div class="col-sm-12 col-xs-12">
							       <!--div class="col-sm-6 col-xs-6">
								  <div class="row">
								     <div class="col-sm-7 col-xs-7">
									<select class="form-control" id="zone" name="zone" required>
                                                                           <option value="">Select Zone</option>
                                                                        </select>	
								     </div>
								     <div class="col-sm-5 col-xs-5">
									<h5 class="so-add_headings" onclick="add_zone_model()">
									   Add New Zone
									</h5>	
								     </div>
								  </div>
							       </div-->
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="pin" id="pin"  required>
								  <label>PIN Code<sup>*</sup></label>
							       </div>
							    </div>
							       
							      <div class="col-sm-6 col-xs-6">
								    <div id="adv-search">
									<div class="mui-textfield mui-textfield--float-label">
									    <!--input type="text" id="hashtags" name="hashtags" value="<?php echo $edit_location_data->hashtags?>" autocomplete="off"-->
									    <textarea autocomplete="off" id="hashtags" name="hashtags"><?php echo $edit_location_data->hashtags?></textarea>
									    <label>HashTags</label>
									</div>
									<div class="input-group-btn">
									    <div class="btn-group" role="group">
										<div class="dropdown dropdown-lg" style="position: absolute !important;
z-index: 99999;">
										    <div class="dropdown-menu dropdown-menu-right"  id="search_filter_areas" style="border-radius:0px; margin-top: 2px;  position: static">
										    </div>
										</div>
									    </div>
									</div>
								    </div>
								</div> 
							       
							       
							 </div>
						      </div>
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-8 col-xs-8">
							       <h4>CONTACT PERSON DETAILS FOR THIS LOCATION</h4>
							    </div>
                                                         </div>	
						      </div>
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="text" name="contact_person_name" id="contact_person_name" required>
								  <label>Full Name<sup>*</sup></label>
							       </div>
							    </div>
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="email" name="email_id" id="email_id"  required>
                                                                  <label>Email ID<sup>*</sup></label>
							       </div>
							    </div>
							 </div>
						      </div>
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <div class="row">
								  <div class="col-sm-3 col-xs-3">
                                                                     <select class="form-control so-add-zone">
                                                                        <option>+<?php echo $locationid->country_code?></option>
                                                                        
                                                                     </select>	
                                                                  </div>
								  <div class="col-sm-9 col-xs-9">
								     <div class="mui-textfield mui-textfield--float-label">
									<input type="text" name="mobile_no" id="mobile_no" pattern="[0-9]{1,}" required>
                                                                        <label>Mobile No.<sup>*</sup></label>
								     </div>
								  </div>
							       </div>
							    </div>
							    
							    
							    <div class="col-sm-6 col-xs-6" style="margin-top:25px">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h6 class="toggle_heading"> Enable Advertisement Channel</h6>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="enable_channel" value="1" checked="">
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
							      
							    </div>
							    
							    
							    
							 </div>
						      </div>
						      
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6" style="margin-top:25px">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h6 class="toggle_heading"> Enable NAS Down Notification</h6>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="enable_nas_down_notification" value="1" >
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
							       <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
							    
							    
							    
							 </div>
						      </div>

						      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-12 col-xs-12">
                                                               <p id="add_location_error" style="color:red"></p>
							       <div class="row">
								  <div class="col-sm-4 col-xs-4 nopadding-right">
								     <button type="submit" class="mui-btn mui-btn--outline-accent btn-sm btn-block" value="exit">SAVE & EXIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4 nopadding-left">
								     <button type="submit" class="mui-btn mui-btn--accent btn-sm btn-block" value="next">SAVE & PROCEED<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								  </div>	 
							       </div>
							    </div>
							 </div>
						      </div>
						   </form>
						</div>
						   </div>
						
					     <div class="col-lg-5 col-sm-5">
						<div class="row">
                                                   <div class="col-sm-12 col-xs-12">
						      <h4>GEO LOCATION ON MAP</h4>
						   </div>	
						</div>
						<div class="row">
                                                   <div class="col-sm-12 col-xs-12">
						      <div class="os-mag" id="map1">
							 <h2>Google Maps Preview</h2>
						      </div>
						   </div>
						   <div class="col-sm-12 col-xs-12 pull-right text-right">
                                                      PlaceID:<span class="Lat_text" id="placeidview"></span>
						      Lat:<span class="Lat_text" id="latview"></span>
						      Long:<span class="Lat_text" id="longview">  </span>
						   </div>	
						</div>
						<!--div class="row">
						   <div class="col-sm-12 col-xs-12">
						      <h4>LOCATION PORTAL DETAILS</h4>
						   </div>	
						</div>
						<div class="row">
						   <div class="col-sm-12 col-xs-12">
                                                      <div class="os-mag-detail">
                                                         <h6>Portal URL:</h6>
                                                         <h6><small>https://www.shouut.com/ispadmins/star.shouut.com/locationportal</small></h6>
                                                         <h6>Username:</h6>
                                                         <h5>praveer@gmail.com</h5>
                                                         <h6>Password:</h6>
                                                         <h5>123456 <small>Copy to Clipboard</small></h5>
                                                      </div>
						   </div>
						</div-->
					     </div>
					  </div>
				       </div>
				       <!-- location tab End -->
                                       
                                       
                                       <!-- nas/network tab start -->	
                                       <div class="mui-tabs__pane" id="nas_setup">
                                          <div class="col-lg-12 col-sm-12">
                                             <div class="row">
						<div class="col-sm-12 col-xs-12">
						   <h4>SELECT NAS TO CONFIGURE</h4>
						</div>	
					     </div>
                                             <div class="row">
                                                <form id="configure_nas" onsubmit="configure_nas(); return false;">
                                                   <div id="nas_setup_data">
							 
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- nas/network tab end -->
                                       
                                       
                                       
                                        <!-- voucher  tab start -->
				       <div class="mui-tabs__pane" id="Vouchers">
					  <div class="row">
					     <div class="col-sm-6 col-xs-6">
						<h2>Create New Coupon</h2>
					     </div>
					     <div class="col-sm-6 col-xs-6">
						<div class="col-sm-12 col-xs-12 mui--text-right">
						   <p><strong><span class="location_name_view"></span> <span class="location_id_view"></span></strong></p>
						   <p><small style="color:#808080" class="email_view"></small></p>
						   <p><small stylecolor:#808080" class="mobile_number_view"></small></p>
						</div>="
					     </div>
					  </div>
					  <div class="row" >
					     <div class="col-sm-6 col-xs-6">
						<label class="radio-inline">
						   <input type="radio" name="payment_gateway_radio" value="0" checked> Payment Gateway Enable
						   </label>
						   <label class="radio-inline">
						   <input type="radio" name="payment_gateway_radio"  value="1"> Payment Gateway Disable
						   </label>
					      </div>
					    
					  </div>
					  <div class="row" style="margin-top:15px;">
					     <div class="col-sm-6 col-xs-6">
						<label class="radio-inline">
						   <input type="radio" name="voucher_radio" value="1" checked> Voucher Use
						   </label>
						   <label class="radio-inline">
						   <input type="radio" name="voucher_radio"  value="0" > Not use
						   </label>
					     </div>
					     
					  </div>
					  
					  <div class="row">
					     <div class="col-sm-6 col-xs-6">
						
						<div id="voucher_generate_div">
						   <form id="add_location_vouchers" onsubmit="add_location_vouchers(); return false;">
						      <div class="row">
							 <div class="col-sm-5 col-xs-5">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="text" id="voucher_data" name="voucher_data" required>
							       <label>Voucher Value <sup>*</sup></label>
							    </div>
							 </div>
							 <div class="col-sm-2 col-xs-2">
							    <div class="mui-select">
							       <select id="voucher_data_type" name="voucher_data_type" required>
								  <option value="MB">MB</option>
								  <option value="GB">GB</option>
							       </select>
							    </div>
							 </div>
						      </div>
						      <div class="row" style="margin-bottom:15px;">
							 <div class="col-sm-5 col-xs-5">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="text" id="voucher_cost" name="voucher_cost" required>
							       <label>Voucher Cost Price <sup>*</sup></label>
							    </div>
							 </div>
							 <div class="col-sm-5 col-xs-5">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="text" id="voucher_selling_price" name="voucher_selling_price" required>
							       <label>Voucher Selling Price <sup>*</sup></label>
							    </div>
							 </div>
							 <div class="col-sm-3 col-xs-3">
							    <p style="color:red" id="voucher_create_error"></p>
							    <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">ADD</button>
							 </div>
						      </div>
						   </form>
						   <div class="row">
						      <div class="col-sm-12 col-xs-6">
							 <div class="table-responsive">
							    <table class="table table-striped">
							       <thead>
								  <tr class="active">
								     <th>&nbsp;</th>
								     <th>COUPON VALUE</th>
								     <th>COUPON COST</th>
								     <th>COUPON SELLING PRICE</th>
								     <th class="mui--text-right">ACTIONS</th>
								  </tr>
							       </thead>
							       <tbody id="voucher_list">
							       </tbody>
							    </table>
							 </div>
						      </div>
						   </div>
						</div>
						<div class="form-group">
						   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
						      <div class="row">
							 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
							    <button type="button" onclick="exit_voucher_panel()"  class="mui-btn mui-btn--accent btn-lg save_btn">SAVE & EXIT</button>
							 </div>
						      </div>
						   </div>
						</div>
					     </div>
					  </div>
				       </div>
				       <!-- voucher tab END -->
			
				      <!-- content builder tab start -->
				       <div class="mui-tabs__pane offline" id="content_builder">
							 <div class="row">
							     <div class="col-lg-12 col-sm-12">
								 <div class="row">
								     <div class="col-sm-12 col-xs-12">
									 <h4>CONTENT BUILDER</h4>
									 <span class="os-otp">Please create you content structure and upload the relevant media. The CP will allow users to navigate to the content based on the heirarchy built here.</span>	
								     </div>
								 </div>
								 <div class="row">
								     <form id="add_content_builder"  enctype="multipart/form-data" onsubmit="add_content_builder(); return false;">
									 <br/>
									 <div class="row">
									     <div class="col-sm-12 col-xs-12">
										 <div class="col-sm-4 col-xs-4">
										     <label class="so-lable-network">TITLE <sup>*</sup></label>
										     <input type="text" class="form-control" id="offline_content_title" name="offline_content_title" placeholder="Title" required="required">
										 </div>
										 <div class="col-sm-4 col-xs-4">
										     <label class="so-lable-network">SHORT DESCRIPTION <sup>*</sup></label>
										     <input type="text" class="form-control" id="offline_content_desc" name="offline_content_desc" placeholder="Description" required="required">
										 </div>
									     </div>
									 </div>
									 <div class="row" style="margin-top:30px; margin-bottom:20px">
									     <div class="col-sm-12 col-xs-12">
										 <div class="col-sm-12 col-xs-12">
										     <div class="offline_section">
											 <div class="row">
											     <div class="col-sm-12 col-xs-12">
												 <div class="offline_list">
												     <div class="table-responsive">
													 <table class="table offline-table-bordered" style="margin-bottom: 0px;">
													     <tr>
														 <th class="col-sm-4 col-xs-4 offline_br">
														     <h5>Main Section</h5>
														     <h6>Main sections will be displayed as a list.</h6>
														 </th>
														 <th class="col-sm-4 col-xs-4 offline_br">
														     <h5>Sub Section / Content </h5>
														     <h6>You can add either sub sections or content. </h6>
														 </th>
														 <th class="col-sm-4 col-xs-4 offline_b" style="border-right:none">
														     <h5>Content</h5>
														     <h6>Here you can only add content</h6>
														 </th>
													     </tr>
													     <tbody>
														 <tr>
														     <td class="col-sm-4">
															 <div class="col-sm-12 offline-table">
															     <div class="row">
																 <div style="display: none" class="offline_main_section_filled_div">
																    <ul class = "main_section_content_list">
																    </ul>
																    <ul>
																       <li>
																	  <a onclick="open_add_main_category()"> <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Section</a>		
																       </li>
																    </ul>
																 </div>
																 <div style="display: none" class="offline-col offline_main_section_blank_div" onclick="open_add_main_category()">
																     <center>
																	 <div class="col-sm-12">
																	     <span class="mui-btn offline--outline btn-sm">
																	      Add New Section
																	     </span>
																	 </div>
																     </center>
																 </div>
															     </div>
															 </div>
														     </td>
														     <td class="col-sm-4">
															 <div class="col-sm-12">
															     <div class="row">
															      <div style="display: none" class="offline-col offline_sub_section_blank_first_div" onclick="open_add_main_category()">
																     <center>
																       <p>
																	  Add Main Section First
																       </p>
																     </center>
																 </div>
																 <div style="display: none" class="offline_list offline_sub_section_filled_div">
																    <ul class = "sub_section_content_list">
																       
																    </ul>
																    <ul>
																	 
																	 <li>
																	     <a class="sub_category_add_data" data-is_this_is_content="0"> 
																	     <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Sub Section</a>		
																	 </li>
																    </ul>
																 </div>
																 <div style="display: none" class="offline-col offline_sub_section_blank_div">
																     <center>
																	 <div class="col-sm-12 discuss_question_add_option" onclick="open_add_sub_category()">
																	     <span class="mui-btn offline--outline btn-sm">
																	     Add New Section
																	     </span>
																	 </div>
																	 <div class="col-sm-12 discuss_question_add_option">
																	     OR
																	 </div>
																	 <div class="col-sm-12" onclick="choose_content_type(1)">
																	     <span class="mui-btn offline--outline btn-sm" data-toggle="modal" data-target="">
																	     Add Content
																	     </span>
																	 </div>
																     </center>
																 </div>
																 
															     </div>
															 </div>
														     </td>
														     <td class="col-sm-4" style="border-right:none">
															 <div class="col-sm-12">
															     <div class="row">
															      <div style="display: none" class="offline-col offline_content_section_blank_first_div">
																     <center>
																       <p>
																	  Add Sub Section First
																       </p>
																     </center>
															      </div>
															      <div style="display: none" class="offline_list offline_content_section_filled_div">
																 <ul class = "content_section_content_list">
																    
																 </ul>
																 <ul>
																    <li>
																       <a onclick="choose_content_type(0)">
																       <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add More Content</a>        
																    </li>
																 </ul>
															      </div>
															      <div style="display: none" class="offline-col  offline_content_section_blank_div">
																 <center>
																    <div class="col-sm-12" onclick="choose_content_type(0)">
																       <span class="mui-btn offline--outline btn-sm" data-toggle="modal" data-target="#mymodalcontent">
																       Add Content
																       </span>
																    </div>
																 </center>
															      </div>
																 
															     </div>
															 </div>
														     </td>
														 </tr>
													     </tbody>
													 </table>
												     </div>
												 </div>
											     </div>
											 </div>
										     </div>
										 </div>
									     </div>
									 </div>
									 <div class="row">
									     <div class="col-sm-12 col-xs-12">
										 <div class="col-sm-10 col-xs-10">
										    <p id="add_content_builder_error" style="color:red"></p>
										     <div class="row">
											 <div class="col-sm-3 col-xs-3 nopadding-right">
											     <button type="submit" class="mui-btn mui-btn--outline-accent btn-sm btn-block" value="exit">SAVE & EXIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
											 </div>
											 <div class="col-sm-3 col-xs-3 nopadding-left">
											     <button type="submit" class="mui-btn mui-btn--accent btn-sm btn-block" value="save">SAVE & PROCEED<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
											 </div>
										     </div>
										 </div>
									     </div>
									 </div>
								     </form>
								 </div>
							     </div>
							 </div>
						      </div>
	       

				       <!-- content builder tab end -->
                                       
                                       
				       <!-- content builder tab start for contestifi -->
				       
				       <div class="mui-tabs__pane offline" id="contestifi_content_builder">
					  <div class="row">
					     <div class="col-lg-12 col-sm-12">
						<div class="row">
						   <div class="col-sm-12 col-xs-12">
						      <h4>CONTENT BUILDER</h4>
						    </div>
						</div>
						
						<div class="row">
						   <form id="contestifi_add_content_builder"  enctype="multipart/form-data" onsubmit="contestifi_add_content_builder(); return false;">
						      <div class="row" style="margin-top:30px; margin-bottom:20px">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-12 col-xs-12">
							       <div class="offline_section">
								  <div class="row">
								     <div class="col-sm-12 col-xs-12">
									<div class="offline_list">
									   <div class="table-responsive">
									      <table class="table offline-table-bordered" style="margin-bottom: 0px;">
										 <tr>
										    <th class="col-sm-4 col-xs-4 offline_br">
											<h5>Contest</h5>
										    </th>
										    <th class="col-sm-4 col-xs-4 offline_br">
											<h5>Quiz </h5>
										    </th>
										    <th class="col-sm-4 col-xs-4 offline_br">
											<h5>Contest Prize </h5>
										    </th>
										 </tr>
										 <tbody>
										    <tr>
										       <td class="col-sm-4">
											  <div class="col-sm-12 offline-table">
											     <div class="row">
												<div style="display: none" class="contestifi_content_filled_div">
												   <ul class = "contestifi_section_content_list">
												   </ul>
												   <ul>
												      <li>
													 <a onclick="open_add_contestifi_contest()"> <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Contest</a>		
												      </li>
												   </ul>
												</div>
												<div style="display: none" class="offline-col contestifi_content_blank_div" onclick="open_add_contestifi_contest()">
												   <center>
												      <div class="col-sm-12">
													 <span class="mui-btn offline--outline btn-sm">
													       Add New Contest
													      </span>
													  </div>
												      </center>
												</div>
											     </div>
											  </div>
										       </td>
										       <td class="col-sm-4">
											  <div class="col-sm-12">
											     <div class="row">
												<div style="display: none" class="offline-col contestifi_quiz_blank_first_div" onclick="open_add_contestifi_quiz()">
												   <center>
												      <p>
													 Add Contest First
												      </p>
												   </center>
												</div>
												<div style="display: none" class="offline_list contestifi_quiz_filled_div">
												   <ul class = "contestifi_quiz_list"></ul>
												   <ul>
												      <li onclick="open_add_contestifi_quiz()">
													 <a class="contestifi_quiz_add_data"> 
													    <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Quiz
													 </a>		
												      </li>
												   </ul>
												</div>
												<div style="display: none" class="offline-col contestifi_quiz_blank_div">
												   <center>
												      <div class="col-sm-12" onclick="open_add_contestifi_quiz()">
													 <span class="mui-btn offline--outline btn-sm">
													    Add New Quiz
													 </span>
												      </div>
												   </center>
												</div>
											     </div>
											  </div>
										       </td>
										       
										       <td class="col-sm-4">
											  <div class="col-sm-12">
											     <div class="row">
												<div style="display: none" class="offline-col contestifi_prize_blank_first_div" onclick="open_add_contestifi_prize()">
												   <center>
												      <p>
													 Add Contest First
												      </p>
												   </center>
												</div>
												<div style="display: none" class="offline_list contestifi_prize_filled_div">
												   <ul class = "contestifi_prize_list"></ul>
												   <ul>
												      <li onclick="open_add_contestifi_prize()">
													 <a class="contestifi_prize_add_data"> 
													    <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Prize
													 </a>		
												      </li>
												   </ul>
												</div>
												<div style="display: none" class="offline-col contestifi_prize_blank_div">
												   <center>
												      <div class="col-sm-12" onclick="open_add_contestifi_prize()">
													 <span class="mui-btn offline--outline btn-sm">
													    Add New Prize
													 </span>
												      </div>
												   </center>
												</div>
											     </div>
											  </div>
										       </td>
										    </tr>
										 </tbody>
									      </table>
									   </div>
									</div>
								     </div>
								  </div>
							       </div>
							    </div>
							 </div>
						      </div>
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							     <div class="col-sm-10 col-xs-10">
								<p id="add_content_builder_error" style="color:red"></p>
								 <div class="row">
								     <div class="col-sm-3 col-xs-3 nopadding-right">
									 <button type="submit" class="mui-btn mui-btn--outline-accent btn-sm btn-block" value="exit">SAVE & EXIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								     </div>
								     <div class="col-sm-3 col-xs-3 nopadding-left">
									 <button type="submit" class="mui-btn mui-btn--accent btn-sm btn-block" value="save">SAVE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								     </div>
								 </div>
							     </div>
							 </div>
						      </div>
						   </form>
						</div>
						
					     </div>
					  </div>
				       </div>

				       <!-- content builder tab end for contestifi -->
				       
				       
                                       <!-- Captive portal tab start -->
                                       <div class="mui-tabs__pane" id="captive_portal">
                                          <div class="row" id="cp_public_location" style="display: none">
					     <div class="col-lg-7 col-sm-7">
						<div class="row">
						   <div class="col-sm-12 col-xs-12">
						      <h4>SELECT CAPTIVE PORTAL TYPE</h4>
						   </div>	
						</div>
						<div class="row">
                                                   <form id="add_captive_portal" onsubmit="add_captive_portal(); return false;">
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-12 col-xs-12 os_cptb">
							       <label class="radio-inline">
                                                                  <input type="radio" name="captive_potal_type" value="cp1" checked> CP1 (Time Based)
							       </label>
							       <label class="radio-inline">
                                                                  <input type="radio" name="captive_potal_type"  value="cp2"> CP2 (Data Earn & Burn)
							       </label>
							       <label class="radio-inline">
								  <input type="radio" name="captive_potal_type"  value="cp3"> CP3 (Hybrid)
							       </label>
							       
							       <!--label class="radio-inline">
								  <input type="radio" name="captive_potal_type"  value="cp4"> CP4 (Hybrid)
							       </label-->
							    </div>
							 </div>
						      </div>
						      <!--div class="col-sm-12 col-xs-12">
                                                         <h4>SSID DETAILS</h4>
						      </div-->	
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input maxlength="24" type="text" name="public_wifi_ssid">
								  <span class="title_box">(Max 24 Chars.)</span>
								  <label>SSID Name <sup>*</sup></label>
							       </div>
							    </div>
                                                            <div class="col-sm-3 col-xs-3">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="color" style="border: none" name="icon_color" value="#fccf47">
								  <label>Set Theme Color <sup>*</sup></label>
							       </div>
							    </div>
							    <div class="col-sm-3 col-xs-3" id="slider_background_color" style="display:  none">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="color" style="border: none" name="slider_background_color">
								  <label>Set Slider Background Color <sup>*</sup></label>
							       </div>
							    </div>
							 </div>
						      </div>
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <label class="os-lable">LOGO FOR THIS LOCATION</label>
							       <div class="mui-textfield" style="padding-top:5px;">
								  <div class="dropzone" data-width="900" data-ajax="false"  data-originalsize="false" data-height="300" style="width: 250px; height:80px;">
                                                                     <input type="file" name="thumb"  />
                                                                  </div>
							       </div>
							    </div>
							   <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <label class="os-lable">&nbsp;</label>
                                                             <div class="mui-textfield" style="padding-top:5px;width: 250px; height:85px; overflow: hidden;">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="logo_image_preview" class="profile-user-img img-responsive location_logo_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                           </div>
							 </div>
						      </div>
						      
						      
						      
                                                      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">DEFAULT OFFER IMAGE FOR THIS LOCATION</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone" data-width="900" data-ajax="false" data-originalsize="false" data-height="450" style="width: 300px; height:150px;">
                                                                     <input type="file" name="thumb_background"  />
                                                                  </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <label class="os-lable">&nbsp;</label>
                                                             <div class="mui-textfield" style="padding-top:5px;width: 300px; height:150px; overflow: hidden;">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="background_image_preview" class="profile-user-img img-responsive defult_offer_image_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                           </div>
                                                         </div>
                                                      </div>
						      
						      
						      <div id="publicspaces_slider_image_div" style="display: none">
							 
						      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">SLIDER 1 IMAGE FOR THIS LOCATION</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone" data-ajax="false" data-originalsize="false" data-width="520" data-height="750" style="width: 182px; height:262.5px;">
                                                                     <input type="file" name="thumb_slider1"  />
                                                                  </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <div class="slider_img_preview">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="slider1_image_preview" class="profile-user-img img-responsive slider1_image_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                             <div class="slider_img_loader" style="display: none">
                                                                  <img src="<?php echo base_url()?>assets/images/loader.svg"/>
                                                               </div>
                                                           </div>
                                                         </div>
                                                      </div>
						      
						      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">SLIDER 2 IMAGE FOR THIS LOCATION</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone" data-ajax="false" data-originalsize="false" data-width="520" data-height="750" style="width: 182px; height:262.5px;">
                                                                     <input type="file" name="thumb_slider2"  />
                                                                  </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <div class="slider_img_preview">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="slider2_image_preview" class="profile-user-img img-responsive slider2_image_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                             <div class="slider_img_loader" style="display: none">
                                                                  <img src="<?php echo base_url()?>assets/images/loader.svg"/>
                                                               </div>
                                                           </div>
                                                         </div>
                                                      </div>
						      

						      </div>
						      
						      
						      <!--div id="div_for_cp1_plantype">
							 <div class="col-sm-12 col-xs-12">
							    <div class="form-group">
							       <div class="row">
								  <div class="col-sm-7 col-xs-7">
								     <label class="os-lable" style="font-weight:700">PLAN TYPE</label>
								  </div>
							       </div>
							    </div>
							 </div>
							 <div class="col-sm-12 col-xs-12"> 
							    <div class="form-group">
							       <div class="row">
								  <div class="col-sm-7 col-xs-7">
								     <label class="radio-inline" style="margin-right:10px">
									<input class="cp1_plan_type_radio" name="cp1_plan_type_value" value="0"  type="radio" checked> Time Plan
								     </label>
								     <label class="radio-inline" style="margin-right:10px">
									<input class="cp1_plan_type_radio" name="cp1_plan_type_value" value="2" type="radio" > Hybrid Plan
								     
								  </div>
								  
							       </div>
							    </div>
							 </div>
						      </div-->
						
						
                                                      <div class="row">
							 <div class="col-sm-12 col-xs-12 cp1_div cp1_plan_div">
							    <div class="col-sm-12 col-xs-12">
							       <div class="row">
								  <div class="col-sm-6 col-xs-6">
								     <label class="os-lable">PLAN FOR THIS LOCATION</label>
								     <select class="form-control so-add-zone" name="cp1_plan" id="cp1_plan" onchange="change_plan(this)">
									<?php
                                                                           echo $plan_list = $this->location_model->plan_list('6');
                                                                        ?>
								     </select>	
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <button type="button" class="mui-btn mui-btn--outline-plan btn-sm btn-block" onclick="save_time_plan()">Save Plan<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								  </div>
							       </div>
							    </div>
							 </div>
                                                         <div class="col-sm-12 col-xs-12 cp2_div" style="display: none" >
							    <div class="col-sm-12 col-xs-12">
							       <div class="row">
								  <div class="col-sm-6 col-xs-6">
								     <label class="os-lable">PLAN FOR THIS LOCATION</label>
								     <select class="form-control so-add-zone" name="cp2_plan" id="cp2_plan" onchange="change_plan(this)">
									<?php
                                                                           echo $plan_list = $this->location_model->plan_list('4');
                                                                        ?>
								     </select>	
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <button type="button" class="mui-btn mui-btn--outline-plan btn-sm btn-block" onclick="save_data_plan()">Save Plan<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								  </div>
							       </div>
							    </div>
							 </div>
                                                         <!--div class="col-sm-12 col-xs-12 cp3_div" style="display: none" >
							    <div class="col-sm-12 col-xs-12">
							       <div class="row">
								  <div class="col-sm-6 col-xs-6">
								     <label class="os-lable">PLAN FOR THIS LOCATION</label>
								     <select class="form-control so-add-zone" name="cp3_data_plan" id="cp3_data_plan" onchange="change_plan(this)">
									<?php
                                                                           //echo $plan_list = $this->location_model->plan_list('4');
                                                                        ?>
								     </select>	
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <button type="button" class="mui-btn mui-btn--outline-plan btn-sm btn-block" onclick="save_hybrid_data_plan()">Save Plan<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								  </div>
							       </div>
							    </div>
							 </div-->
						      </div>
                                                      <div class="row hide_data_from_cp3">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="download_rate" class="download_rate">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Download Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="upload_rate" class="upload_rate">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Upload Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-sm-12 col-xs-12 hide_data_from_cp3">
                                                         <span class="os-otp">If actual bandwidth is lower than limits set here, user will be limited to actual bandwdith available at the location.</span>
                                                      </div>
                                                      <div class="row data_limit_div hide_data_from_cp3">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-8 col-xs-8">
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4">
                                                                     <div class="form-group os-form-group">
                                                                        <label class="checkbox-inline">
                                                                           <input name="datalimit_checkbox" value="option1" type="checkbox" class="openmodal datalimit_checkbox"> Data Limit
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-sm-6 col-xs-6 nopadding-left">
                                                                     <div class="mui-textfield ">
                                                                        <input type="text" name="datalimit" class="datalimit">
                                                                        <span class="title_box">MB</span>
                                                                        <label>Data Limit<sup>*</sup></label>
                                                                     </div>
                                                                  </div>
                                                               </div>  
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row time_limit_div hide_data_from_cp3">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-8 col-xs-8">
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4">
                                                                     <div class="form-group os-form-group">
                                                                        <label class="checkbox-inline">
                                                                           <input name="timelimit_checkbox" value="option1" type="checkbox" class="openmodal timelimit_checkbox"> Time Limit
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-sm-6 col-xs-6">
                                                                     <div class="os-range">
                                                                        <span class="os-currentVal">5</span>
                                                                        <span class="os-currentVal2">600</span>
                                                                     </div>
                                                                  </div>
                                                               </div>  
                                                            </div>
                                                         </div>
                                                      </div>

                                                      <div class="row cp3_div cp3_plan_div" style="display:none">
							 <div class="col-sm-12 col-xs-12 "  >
							    <div class="col-sm-12 col-xs-12">
							       <div class="row">
								  <div class="col-sm-6 col-xs-6">
								     <label class="os-lable">HYBRID PLAN FOR THIS LOCATION</label>
								     <select class="form-control so-add-zone" name="cp3_hybrid_plan" id="cp3_hybrid_plan" onchange="change_plan(this)">
									<?php
                                                                           echo $plan_list = $this->location_model->plan_list('5');
                                                                        ?>
								     </select>	
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <button type="button" class="mui-btn mui-btn--outline-plan btn-sm btn-block" onclick="save_hybrid_plan()">Save Plan<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								  </div>
							       </div>
							    </div>
							 </div>
                                                                                                                  <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="download_rate_hybrid" class="download_rate_hybrid">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Download Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="upload_rate_hybrid" class="upload_rate_hybrid">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Upload Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="col-sm-12 col-xs-12">
                                                            <span class="os-otp">If actual bandwidth is lower than limits set here, user will be limited to actual bandwdith available at the location.</span>
                                                         </div>
                                                         <div class="col-sm-12 col-xs-12 data_limit_div_hybrid">
                                                            <div class="col-sm-8 col-xs-8">
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4">
                                                                     <div class="form-group os-form-group">
                                                                        <label class="checkbox-inline">
                                                                           <input name="datalimit_checkbox_hybrid" value="option1" type="checkbox" class="openmodal datalimit_checkbox_hybrid"> Data Limit
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-sm-6 col-xs-6 nopadding-left">
                                                                     <div class="mui-textfield ">
                                                                        <input type="text" name="datalimit_hybrid" class="datalimit_hybrid">
                                                                        <span class="title_box">MB</span>
                                                                        <label>Data Limit<sup>*</sup></label>
                                                                     </div>
                                                                  </div>
                                                               </div>  
                                                            </div>
                                                         </div>
                                                         <div class="col-sm-12 col-xs-12 time_limit_div_hybrid">
                                                            <div class="col-sm-8 col-xs-8">
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4">
                                                                     <div class="form-group os-form-group">
                                                                        <label class="checkbox-inline">
                                                                           <input name="timelimit_checkbox_hybrid" value="option1" type="checkbox" class="openmodal timelimit_checkbox_hybrid"> Time Limit
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-sm-6 col-xs-6">
                                                                     <div class="os-range_hybrid">
                                                                        <span class="os-currentVal_hybrid">5</span>
                                                                        <span class="os-currentVal2">600</span>
                                                                     </div>
                                                                  </div>
                                                               </div>  
                                                            </div>
                                                         </div>



                                                      </div>
                                                      <div class="col-sm-12 col-xs-12">
                                                         <h4>DATA LIMIT FOR LOCATION</h4>
						      </div>	
						      <div class="row">
							 <div class="col-sm-12 col-xs-12 cp1_div">
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="text" name="cp1_no_of_daily_session" id="cp1_no_of_daily_session">
								  <label>NO OF DAILY SESSION <sup>*</sup></label>
							       </div>
							    </div>
							 </div>
                                                         <div class="col-sm-12 col-xs-12 cp2_div" style="display:none">
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="text" name="cp2_signip_data" id="cp2_signip_data" >
                                                                  <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
								  <label>Data on Signup<sup>*</sup></label>
							       </div>
							    </div>
                                                            <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="text" name="cp2_daily_data" id="cp2_daily_data">
                                                                  <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                                  <label>Daily Data Quota<sup>*</sup></label>
							       </div>
							    </div>
							 </div>
                                                         <div class="col-sm-12 col-xs-12 cp3_div" style="display:none">
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="text" name="cp3_signip_data" id="cp3_signip_data">
                                                                  <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                                  <label>Data on Signup<sup>*</sup></label>
							       </div>
							    </div>
                                                            <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="text" name="cp3_daily_data" id="cp3_daily_data">
                                                                  <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                                  <label>Daily Data Quota<sup>*</sup></label>
							       </div>
							    </div>
							 </div>
						      </div>
                                                      <div class="col-sm-12 col-xs-12">
                                                         <h4>MANAGE LOGIN CONDITION</h4>
						      </div>
                                                      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h6 class="toggle_heading"> Enable OTP Verification</h6>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" checked="" name="is_otpdisabled_public_location" value="0">
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
							       <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp">OTP Verification maybe a legal requirement in some countries.
											Please check your local regualtions</span>
                                                                  </div>
							       </div>
							    </div>
							    
							    
							    <div class="col-sm-6 col-xs-6" id="publicspace_social_login_div" style="display:none">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h4 class="toggle_heading"> Enable Social Login</h4>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="is_socialloginenable" value="1">
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
							    
							    <div class="col-sm-6 col-xs-6" id="publicspace_loginwith_div" style="display:none">
							       <div class="row">
                                                                  <div class="col-sm-12 col-xs-12"> 
                                                                     <div class="form-group">
                                                                        <label>Login with</label>
                                                                        <label class="radio-inline" style="margin-right:10px">
                                                                           <input name="login_with_mobile_email" value="0"  type="radio" checked="checked"> Mobile
                                                                        </label>
                                                                        <label class="radio-inline" style="margin-right:10px">
                                                                           <input name="login_with_mobile_email" value="1" type="radio"> Email
                                                                        </label>
                                                                     </div>
                                                                  </div>
							       </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
							    
							    <div class="col-sm-6 col-xs-6" id="pin_login_div_publicwifi" style="display:none">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h4 class="toggle_heading"> Enable Pin Login</h4>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="is_pinenable_publicwifi" value="1">
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
							    
							 </div>
						      </div>

                                                      
						      
						      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-12 col-xs-12">
                                                               <p id="add_captive_portal_error" style="color:red"></p>
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4 nopadding-right">
                                                                     <button type="submit" class="mui-btn mui-btn--outline-accent btn-sm btn-block" value="exit">SAVE & EXIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                                                                  </div>
                                                                  <div class="col-sm-4 col-xs-4 nopadding-left">
                                                                     <button type="submit" class="mui-btn mui-btn--accent btn-sm btn-block" value="save">SAVE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                                                                  </div>	 
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
						   </form>
						</div>
					     </div>
                                             <div class="col-lg-5 col-sm-5 nopadding-left">
                                                <div class="row">
                                                   <div class="col-sm-12 col-xs-12">
                                                      <h4>CAPTIVE PORTAL PREVIEW</h4>
						      <h6>Captive Portal URL:
                                                         <small class="cp_path_url_display">-- Add NAS Setup to generate a CP URL --</small>
                                                      </h6>  
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-sm-12 col-xs-12 nopadding-left">
                                                      <div class="col-sm-12 col-xs-12">
                                                         <div class="slider_div pull-right">
                                                            <div class="right_slider">
                                                               <div id="crsl_items_public_location" class="crsl-items" data-navigation="navbtns">
                                                                  <div class="crsl-wrap">
                                                                     <div class="crsl-item">
                                                                        <div class="crsl-item_img">
                                                                           <center   class = "cp1_slider1">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_1_img.jpg" class="img-responsive"/>
                                                                           </center>
                                                                           <center style="display: none" class = "cp2_slider1">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_2_img.jpg" class="img-responsive"/>
                                                                           </center>
                                                                           <center style="display: none" class = "cp3_slider1">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_3_img.jpg" class="img-responsive"/>
                                                                           </center>
                                                                        </div>
                                                                        <div class="crsl-item-logo-a">
                                                                           <div class=" crsl-item-logo">
                                                                              <img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider location_logo_preview_show"/>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="crsl-item">
                                                                        <div class="crsl-item_img">
                                                                           <center  class = "cp1_slider2">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_1_img_2.jpg" class="img-responsive"/>
                                                                           </center>
                                                                           <center style="display: none" class = "cp2_slider2">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_2_img_2.jpg" class="img-responsive"/>
                                                                           </center>
                                                                           <center style="display: none" class = "cp3_slider2">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_3_img_2.jpg" class="img-responsive"/>
                                                                           </center>
                                                                        </div>
                                                                        <div class="crsl-item-logo-a">
                                                                           <div class=" crsl-item-logo">
                                                                              <img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider location_logo_preview_show"/>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="crsl-item">
                                                                        <div class="crsl-item_img">
                                                                           <center class = "cp1_slider3">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_1_img_3.jpg" class="img-responsive"/>
                                                                           </center>
                                                                           <center style="display: none" class = "cp2_slider3">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_2_img_3.jpg" class="img-responsive"/>
                                                                           </center>
                                                                           <center style="display: none" class = "cp3_slider3">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_3_img_3.jpg" class="img-responsive"/>
                                                                           </center>
                                                                        </div>
                                                                        <div class="crsl-item-logo-a">
                                                                           <div class=" crsl-item-logo">
                                                                              <img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider location_logo_preview_show"/>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="crsl-item">
                                                                        <div class="crsl-item_img">
                                                                           <center  class = "cp1_slider4">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_1_img_4.jpg" class="img-responsive"/>
                                                                           </center>
                                                                           <center style="display: none" class = "cp2_slider4">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_2_img_4.jpg" class="img-responsive"/>
                                                                           </center>
                                                                           <center style="display: none" class = "cp3_slider4">
                                                                              <img src="<?php echo base_url()?>assets/images/cp_3_img_4.jpg" class="img-responsive"/>
                                                                           </center>
                                                                        </div>
                                                                        <div class="crsl-item-logo-a">
                                                                           <div class=" crsl-item-logo">
                                                                              <!--img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider"/-->
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <nav class="slidernav">
                                                                  <div id="navbtns" class="clearfix">
                                                                     <a href="#" class="previous pull-left">
                                                                     <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                                     </a>
                                                                     <span class="previous_span"><strong>Setp 01:</strong> User Enter Mobile No.</span>
                                                                     <a href="#" class="next pull-right">
                                                                     <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                     </a>
                                                                  </div>
                                                               </nav>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row" id="cp_hotel_location" style="display: none">
                                             <div class="col-lg-7 col-sm-7">
                                                <div class="row">
                                                   <form id="add_hotel_captive_portal"  enctype="multipart/form-data" onsubmit="add_hotel_captive_portal(); return false;">
                                                      <!--div class="col-sm-12 col-xs-12">
                                                         <h4>SSID DETAILS</h4>
						      </div-->
                                                      	
						      <div class="row">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input maxlength="24" type="text" name="hotel_guest_wifi_ssid">
								  <span class="title_box">(Max 24 Chars.)</span>
								  <label>SSID Name <sup>*</sup></label>
							       </div>
							    </div>
							    <div class="col-sm-3 col-xs-3" id="other_all_location_theme_color_div">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input type="color" style="border: none" name="icon_color" value="#fccf47">
								  <label>Set Theme Color <sup>*</sup></label>
							       </div>
							    </div>
							 </div>
						      </div>
                                                      <div class="row" id="hotel_ssid_div" style="display: none">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="checkbox">
                                                                  <label>
                                                                     <input type="checkbox" name="hotel_guest_wifi" value="1" > <strong style="margin-left:15px">Guest WiFi</strong> 
                                                                  </label>
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="checkbox">
                                                                  <label>
                                                                     <input type="checkbox" name="hotel_visitor_wifi" value="1" > <strong style="margin-left:15px">Visitor WiFi</strong> 
                                                                  </label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
						      <div class="row" id="is_white_theme_div" style="display: none">
                                                        <div class="col-sm-12 col-xs-12">
								<div class="col-sm-6 col-xs-6">
								   <div class="checkbox">
								      <label>
									 <input type="radio" name="is_white_theme" value="0" checked> <strong style="margin-left:15px">Use Black Background</strong> 
								      </label>
								   </div>
								</div>
								<div class="col-sm-6 col-xs-6">
								   <div class="checkbox">
								      <label>
									 <input type="radio" name="is_white_theme" value="1"> <strong style="margin-left:15px">Use White Blackground</strong> 
								      </label>
								   </div>
								</div>
							    </div>
                                                      </div>
                                                      <div class="row" id="hospital_ssid_div" style="display: none">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="checkbox">
                                                                  <label>
                                                                     <input type="checkbox" name="hospital_patient_wifi" value="1" > <strong style="margin-left:15px">Patient WiFi</strong> 
                                                                  </label>
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="checkbox">
                                                                  <label>
                                                                     <input type="checkbox" name="hospital_guest_wifi" value="1" > <strong style="margin-left:15px">Guest WiFi</strong> 
                                                                  </label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row" id="enterprise_ssid_div" style="display: none">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="checkbox">
                                                                  <label>
                                                                     <input type="checkbox" name="enterprise_employee_wifi" value="1" > <strong style="margin-left:15px">Employee WiFi </strong> 
                                                                  </label>
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="checkbox">
                                                                  <label>
                                                                     <input type="checkbox" name="enterprise_guest_wifi" value="1" > <strong style="margin-left:15px">Guest WiFi</strong> 
                                                                  </label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>


                                                      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">LOGO FOR THIS LOCATION</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone" data-width="900" data-ajax="false"  data-originalsize="false" data-height="300" style="width: 250px; height:80px;">
                                                                     <input type="file" name="thumb"  />
                                                                  </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <label class="os-lable">&nbsp;</label>
                                                             <div class="mui-textfield" style="padding-top:5px;width: 250px; height:85px; overflow: hidden;">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="logo_image_preview" class="profile-user-img img-responsive location_logo_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                           </div>
                                                         </div>
                                                      </div>
						      
						      <div class="row" id="location_logo2_div" style="display:none">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <label class="os-lable">LOGO 2 FOR THIS LOCATION</label>
							       <div class="mui-textfield" style="padding-top:5px;">
								  <div class="dropzone" data-width="300" data-ajax="false"  data-originalsize="false" data-height="100" style="width: 250px; height:80px;">
                                                                     <input type="file" name="thumb2"  />
                                                                  </div>
							       </div>
							    </div>
							   <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <label class="os-lable">&nbsp;</label>
                                                             <div class="mui-textfield" style="padding-top:5px;width: 250px; height:85px; overflow: hidden;">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="logo2_image_preview" class="profile-user-img img-responsive location_logo2_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                           </div>
							 </div>
						      </div>
                                                      <!--div class="row" style="display: none"  id="retail_background_image_div"-->
                                                      <div class="row" id="default_offer_image_div">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">DEFAULT OFFER IMAGE FOR THIS LOCATION</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone" data-width="900" data-ajax="false"  data-originalsize="false" data-height="450" style="width: 300px; height:150px;">
                                                                     <input type="file" name="thumb_background"  />
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <label class="os-lable">&nbsp;</label>
                                                             <div class="mui-textfield" style="padding-top:5px;width: 300px; height:150px; overflow: hidden;">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="background_image_preview" class="profile-user-img img-responsive defult_offer_image_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                           </div>
                                                            
                                                         </div>
                                                      </div>
						      <div class="row" id="offline_banner_image_div">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">UPLOAD BACKGROUND / BANNER IMAGE</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone" data-width="900" data-ajax="false"  data-originalsize="false" data-height="450" style="width: 300px; height:150px;">
                                                                     <input type="file" name="thumb_background_offline"  />
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <label class="os-lable">&nbsp;</label>
                                                             <div class="mui-textfield" style="padding-top:5px;width: 300px; height:150px; overflow: hidden;">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="background_image_preview_offline" class="profile-user-img img-responsive defult_offer_image_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                           </div>
                                                            
                                                         </div>
                                                      </div>
						      
						      
						       <div class="row" id="offline_vm_banner_image_div" style="display:none" >
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">UPLOAD BACKGROUND IMAGE</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone"  data-ajax="false" data-originalsize="false" data-width="460" data-height="650" style="width: 184px; height:260px;">
                                                                     <input type="file" name="thumb_background_offline_vm"  />
                                                                  </div>
                                                               </div>
                                                            </div>
							    
							    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							       <label class="os-lable">&nbsp;</label>
                                                             <div class="slider_img_preview">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="background_image_preview_offline_vm" class="profile-user-img img-responsive defult_offer_image_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                             
                                                           </div>
							 </div>
						      </div>
						      
                                                      <div class="col-sm-12 col-xs-12" style="display: none"  id="institutional_user_type_div">
                                                         <div class="col-sm-12 col-xs-12" style="margin-top: 20px;" id="cp_enterprise_user_listing">
                                                            <div class="row">
                                                               <div class="col-sm-4 col-xs-4">
                                                                  <div class="row">
                                                                     <h4 style="margin-top:0px;">Add department type </h4>
                                                                  </div>
                                                               </div>
                                                               <div class="col-sm-3 col-xs-3">
                                                                  <p class="mui-btn mui-btn--small mui-btn--danger institutional_add_user_button" style="margin-top:2px">Add</p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
						      <div id="slider_image_div" style="display: none">
							 
						      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">SLIDER 1 IMAGE FOR THIS LOCATION</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone" data-ajax="false" data-originalsize="false" data-width="460" data-height="650" style="width: 184px; height:260px;">
                                                                     <input type="file" name="thumb_slider1"  />
                                                                  </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <div class="slider_img_preview">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="slider1_image_preview" class="profile-user-img img-responsive slider1_image_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                             <div class="slider_img_loader" style="display: none">
                                                                  <img src="<?php echo base_url()?>assets/images/loader.svg"/>
                                                               </div>
                                                           </div>
                                                         </div>
                                                      </div>
						      
						      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <label class="os-lable">SLIDER 2 IMAGE FOR THIS LOCATION</label>
                                                               <div class="mui-textfield" style="padding-top:5px;">
                                                                  <div class="dropzone" data-ajax="false" data-originalsize="false" data-width="460" data-height="650" style="width: 184px; height:260px;">
                                                                     <input type="file" name="thumb_slider2"  />
                                                                  </div>
                                                               </div>
                                                            </div>
                                                           <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5">
                                                             <div class="slider_img_preview">
                                                
                                                <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="slider2_image_preview" class="profile-user-img img-responsive slider2_image_preview_show" src="" style =" width:100%;">
                                                             </div>
                                                             <div class="slider_img_loader" style="display: none">
                                                                  <img src="<?php echo base_url()?>assets/images/loader.svg"/>
                                                               </div>
                                                           </div>
                                                         </div>
                                                      </div>
						      

						      </div>
							<div id="retail_audit_div" style="display:none">
							    <div class="row">
								<div class="col-sm-12 col-xs-12">
								    <div class="col-sm-6 col-xs-6">
									<label class="os-lable">Select TAT Days</label>
									<select class="form-control so-add-zone"  name="tatdays" id="tatdays" >
									    <option value = '1'>1 Days</option>
									    <option value = '2'>2 Days</option>
									    <option value = '3' selected="selected">3 Days</option>
									    <option value = '4'>4 Days</option>
									    <option value = '5'>5 Days</option>
									    <option value = '6'>6 Days</option>
									    <option value = '7'>7 Days</option>
									    <option value = '8'>8 Days</option>
									    <option value = '9'>9 Days</option>
									    <option value = '10'>10 Days</option>
									</select>	
								    </div>
								</div>
							    </div>
							    <div class="col-sm-12 col-xs-12">
								<div class="form-group">
								    <div class="row">
									<div class="col-sm-6 col-xs-6">
									    <div class="mui-textfield mui-textfield--float-label">
										<input type="email" name="audit_ticket_email_send" id="audit_ticket_email_send" >
										<label>Audit Ticket Email Send </label>
									    </div>
									</div>
									<div class="col-sm-6 col-xs-6" style="margin-top:25px">
									    <div class="row">
									       <div class="col-sm-8 col-xs-8">
										  <h6 class="toggle_heading"> Is Pre audit disable</h6>
									       </div>
									       <div class="col-sm-4 col-xs-4">
										  <label class="switch">
										  <input type="checkbox" name="is_preaudit_disable" value="1">
										     <div class="slider round"></div>
										  </label>
									       </div>
									    </div>
								      
									</div>
								    </div>
								</div>
							    </div>
							    
							    
							    <div class="col-sm-12 col-xs-12">
								<div class="form-group">
								   <div class="row">
								      <div class="col-sm-4 col-xs-4">
									 <label class="os-lable" style="font-weight:700">Add SKU for store</label>
								      </div>
								      <div class="col-sm-4 col-xs-4" >
									 <label class="os-lable" style="font-weight:700">Set login PIN for store</label>
								      </div>
								      <div class="col-sm-4 col-xs-4" >
									 <label class="os-lable" style="font-weight:700">Set Promoter PIN for store</label>
								      </div>
								   </div>
								</div>
							    </div>
							    
							    <div class="col-sm-12 col-xs-12">
								<div class="form-group">
								    <div class="row">
									<div class="col-sm-4 col-xs-4">
									    <div class="row">
									      <div class="col-sm-7 col-xs-7" style="padding-right:0px;">
										<div class="mui-textfield mui-textfield--float-label">
										    <input type="hidden" name="retail_audit_sku_id" id="retail_audit_sku_id">
										  <input class="mui--is-empty mui--is-pristine mui--is-touched" type="text" name="retail_audit_sku_name" id="retail_audit_sku_name">
										  <label>SKU Name</label>
										</div>
									      </div>
										<div class="col-sm-5 col-xs-5 " style="margin-right:-8px;padding:0px">
										    <span class="mui-btn mui-btn--danger" id="add_retail_audit_sku_button" onclick="add_retail_audit_sku()" style="line-height:36px;padding:0px 15px;">Add</span>
										    <span class="mui-btn" style="display: none;line-height:36px;padding:0px 15px;" id="edit_retail_audit_sku_remove_button" onclick="edit_retail_audit_sku_unset()">Unset</span>
										</div>
									    </div>
									    <span id="add_retail_audit_sku_error" style="color:red"></span>
									</div>
									<div class="col-sm-4 col-xs-4">
									    <div class="row">
										<div class="col-sm-8 col-xs-8" style="padding-right:0px;">
										    <div class="mui-textfield mui-textfield--float-label">
											<input type="text" name="retail_audit_login_pin" id="retail_audit_login_pin">
											<label>Enter login PIN</label>
										    </div>
										</div>
										<div class="col-sm-2 col-xs-2" style="margin-right:-8px;padding:0px">
										    <span class="mui-btn mui-btn--danger" onclick="add_retail_audit_pin()" style="line-height:36px">Set</span>
										    
										</div>
									    </div>
									    <span id="add_retail_audit_pin_error" style="color:red"></span>
									    
									</div>
									<div class="col-sm-4 col-xs-4">
									    <div class="row">
										<div class="col-sm-8 col-xs-8" style="padding-right:0px;">
										    <div class="mui-textfield mui-textfield--float-label">
											<input type="text" name="retail_audit_login_pin_promoter" id="retail_audit_login_pin_promoter">
											<label>Enter login PIN</label>
										    </div>
										</div>
										<div class="col-sm-2 col-xs-2">
										    <span class="mui-btn mui-btn--danger" onclick="add_retail_audit_pin_promoter()" style="line-height:36px">Set</span>
										    
										</div>
									    </div>
									    <span id="add_retail_audit_pin_error_promoter" style="color:red"></span>
									    
									</div>
								    </div>
								</div>
							    </div>
							    
							    <div class="col-sm-12 col-xs-12">
								<div class="form-group">
								   <div class="row">
								      <div class="col-sm-4 col-xs-4">
									<div class="row">
									    <div class="table-responsive">
									       <div class="col-sm-12 col-xs-12">
										  <table class="table table-striped" id="retail_audit_sku_table" style="display:none">
										     <thead>
											<tr class="active">
											   <th>SKU</th>
											   <th>Action</th>
											</tr>
										     </thead>
										     <tbody id="retail_audit_sku_table_data">
											
										     </tbody>
										  </table>
									       </div>
									    </div>
									</div>
								      </div>
								      <div class="col-sm-4 col-xs-4" >
									 <label class="os-lable" style="font-weight:700">Login PIN set: <span id="retail_audit_pin_list"></span></label>
								      </div>
								      <div class="col-sm-4 col-xs-4" >
									<label class="os-lable" style="font-weight:700">Promoter PIN set: <span id="retail_audit_pin_list_promoter"></span></label>
								     </div>
								   </div>
								</div>
							    </div>
							</div>
						      <div id="custom_location_div" style="display: none">
							 
							 <div class="col-sm-12 col-xs-12">
							    <h4>CAPTIVE PORTAL URL</h4>
							 </div>
							 <div class="row">
							    <div class="col-sm-12 col-xs-12">
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="custom_captive_url" id="custom_captive_url">
								     <span class="title_box"></span>
								     <label>URL <sup>*</sup></label>
								  </div>
							       </div>
							    </div>
							 </div>
							 <div class="col-sm-12 col-xs-12">
							    <h4 style="text-transform: uppercase; margin-bottom:0px">Select plans to be available</h4>
							    <h4 style="margin-top:0px; font-size:18px; font-weight: 300 "><small>(You can select one plan for the location)</small></h4>
							 </div>
							 <div class="row">
							    <div class="col-sm-12 col-xs-12">
							       <div class="col-sm-6 col-xs-6">
								  <label class="os-lable"></label>
								  <select class="form-control so-add-zone"  name="custom_location_plan" id="custom_location_plan" onchange="change_plan(this)">
									   <?php
									     
										 echo $plan_list = $this->location_model->plan_list('', '0');
									      
									   ?>
								  </select>	
							       </div>
							       <div class="col-sm-3 col-xs-3">
								  <button type="button" class="mui-btn mui-btn--outline-plan btn-sm btn-block" onclick="save_custom_location_plan()">Save Plan<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
							       </div>
							    </div>
							 </div>
							 <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="download_rate" class="download_rate">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Download Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="upload_rate" class="upload_rate">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Upload Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
						      <div class="col-sm-12 col-xs-12">
                                                         <span class="os-otp">If actual bandwidth is lower than limits set here, user will be limited to actual bandwdith available at the location.</span>
                                                      </div>
						      <div class="row data_limit_div">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-8 col-xs-8">
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4">
                                                                     <div class="form-group os-form-group">
                                                                        <label class="checkbox-inline">
                                                                           <input name="datalimit_checkbox" value="option1" type="checkbox" class="openmodal datalimit_checkbox"> Data Limit
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-sm-6 col-xs-6 nopadding-left">
                                                                     <div class="mui-textfield ">
                                                                        <input type="text" id="custom_plan_data_limit" name="datalimit" class="datalimit">
                                                                        <!--span class="title_box">MB</span>
                                                                        <label>Data Limit<sup>*</sup></label-->
                                                                     </div>
                                                                  </div>
                                                               </div>  
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row time_limit_div">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-8 col-xs-8">
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4">
                                                                     <div class="form-group os-form-group">
                                                                        <label class="checkbox-inline">
                                                                           <input name="timelimit_checkbox" value="option1" type="checkbox" class="openmodal timelimit_checkbox"> Time Limit
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-sm-6 col-xs-6">
                                                                     <div class="os-range">
                                                                        <span class="os-currentVal">5</span>
                                                                        <span class="os-currentVal2">600</span>
                                                                     </div>
                                                                  </div>
                                                               </div>  
                                                            </div>
                                                         </div>
                                                      </div>
						      <div class="col-sm-12 col-xs-12">
							 <h4>DATA LIMIT FOR LOCATION</h4>
						      </div>
						      <div id="custom_location_dataplan" style="display:none">
							 <div class="row">
							    <div class="col-sm-12 col-xs-12">
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="custon_cp_signip_data" id="custon_cp_signip_data" >
								     <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
								     <label>Data on Signup<sup>*</sup></label>
								  </div>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="custon_cp_daily_data" id="custon_cp_daily_data" >
								     <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
								     <label>Daily Data Quota<sup>*</sup></label>
								  </div>
							       </div>
							    </div>
							 </div>
						      </div>
						      <div id="custom_location_timepla" style="display: none">
							 <div class="row">
							    <div class="col-sm-12 col-xs-12">
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="custon_cp_no_of_daily_session" id="custon_cp_no_of_daily_session" >
								     <label>NO OF DAILY SESSION <sup>*</sup></label>
								  </div>
							       </div>
							    </div>
							 </div>
						      </div>
							 
							 
						      </div>
						      
						      <div class="row" id="contestifi_contest_header_text_div" style="display: none">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <input maxlength="24" type="text" name="contestifi_header_text">
								  <span class="title_box">(Max 24 Chars.)</span>
								  <label>Header Text <sup>*</sup></label>
							       </div>
							    </div>
							    <div class="col-sm-6 col-xs-6">
							       <div class="mui-textfield mui-textfield--float-label">
								  <a class="mui-url_btn--os" style="cursor:pointer" onclick="contestifi_sign_term_condition()">Signup Term & Condition</a>
							       </div>
							    </div>
							 </div>
						      </div>
						
						      
							<div style="display: none" id="offline_event_module_turn_feature">
							    <div class="col-sm-12 col-xs-12">
								<h4>TURN FEATURE ON OR OFF</h4>
								</div>
								<div class="row">
								    <div class="col-sm-12 col-xs-12">
									<div class="col-sm-6 col-xs-6">
									    <div class="row">
										<div class="col-sm-8 col-xs-8">
										    <h6 class="toggle_heading"> Enable Live Chat</h6>
										 </div>
										<div class="col-sm-4 col-xs-4">
										    <label class="switch">
										    <input type="checkbox" name="is_live_chat_enable" value="1" <?php if($edit_location_data->is_live_chat_enable == '1' )echo "checked"?>>
										    <div class="slider round"></div>
										    </label>
										</div>
									    </div>
									</div>
									<div class="col-sm-6 col-xs-6">
									    <div class="row">
										<div class="col-sm-8 col-xs-8">
										    <h6 class="toggle_heading"> Enable Discuss</h6>
										 </div>
										<div class="col-sm-4 col-xs-4">
										    <label class="switch">
										    <input type="checkbox" name="is_discuss_enable" value="1" <?php if($edit_location_data->is_discuss_enable == '1' )echo "checked"?>>
										    <div class="slider round"></div>
										    </label>
										</div>
									    </div>
									</div>
									<div class="col-sm-6 col-xs-6">
									    <div class="row">
										<div class="col-sm-8 col-xs-8">
										    <h6 class="toggle_heading"> Enable Information</h6>
										 </div>
										<div class="col-sm-4 col-xs-4">
										    <label class="switch">
										    <input type="checkbox" name="is_information_enable" value="1" <?php if($edit_location_data->is_information_enable == '1' )echo "checked"?>>
										    <div class="slider round"></div>
										    </label>
										</div>
									    </div>
									</div>
									<div class="col-sm-6 col-xs-6">
									    <div class="row">
										<div class="col-sm-8 col-xs-8">
										    <h6 class="toggle_heading"> Enable Polls</h6>
										 </div>
										<div class="col-sm-4 col-xs-4">
										    <label class="switch">
										    <input type="checkbox" name="is_poll_enable" value="1" <?php if($edit_location_data->is_poll_enable == '1' )echo "checked"?>>
										    <div class="slider round"></div>
										    </label>
										</div>
									    </div>
									</div>
									<div class="col-sm-6 col-xs-6">
									    <div class="row">
										<div class="col-sm-8 col-xs-8">
										    <h6 class="toggle_heading"> Enable Downloads</h6>
										 </div>
										<div class="col-sm-4 col-xs-4">
										    <label class="switch">
										    <input type="checkbox" name="is_download_enable" value="1" <?php if($edit_location_data->is_download_enable == '1' )echo "checked"?>>
										    <div class="slider round"></div>
										    </label>
										</div>
									    </div>
									</div>
								    </div>
								</div>
							
						    <div class="row"  style=" margin-bottom: 15px;">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <h4>SignUp fields Require</h4>
							    </div>
							 </div>
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-2 col-xs-2">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="event_module_signup_user_name_require" value="1" checked>Name
									   </label>
									</div>
							    </div>
							    <div class="col-sm-2 col-xs-2">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="event_model_signup_user_organisation_require" value="1" checked> Organisation
									   </label>
									</div>
								     
							    </div>
							    <div class="col-sm-3 col-xs-3">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="event_module_signup_nick_name" value="1" checked> Nick Name
									   </label>
									</div>
							    </div>
							    <div class="col-sm-2 col-xs-2">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="event_module_signup_email" value="1" checked> Email
									   </label>
									</div>
							    </div>
							    <div class="col-sm-3 col-xs-3">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="event_module_signup_phone" value="1" checked> Phone No.
									   </label>
									</div>
							    </div>
							 </div>
							<div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
								<div class="row">
								  <div class="col-sm-7 col-xs-7" style="padding-right:0px;">
								    <div class="mui-textfield mui-textfield--float-label">
								      <input class="mui--is-empty mui--is-pristine mui--is-touched" type="text" name="event_module_signup_extra_field" id="event_module_signup_extra_field">
								      <label>Enter Field Name</label>
								    </div>
								  </div>
								    <div class="col-sm-5 col-xs-5 " style="margin-right:-8px;padding:0px">
									<span class="mui-btn mui-btn--danger"  onclick="add_event_module_signup_extra_field()" style="line-height:36px;padding:0px 15px;">Add</span>
								    </div>
								</div>
								<span id="event_module_signup_extra_field_error" style="color:red"></span>
							    </div>
							</div>
							<div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
									<div class="row">
									    <div class="table-responsive">
									       <div class="col-sm-12 col-xs-12" id="event_module_signup_extra_field_table" style="display: none">
										  <table class="table table-striped"  >
										     <thead>
											<tr class="active">
											   <th>Field</th>
											   <th>Action</th>
											</tr>
										     </thead>
										     <tbody id="event_module_signup_extra_field_table_data">
											
										     </tbody>
										  </table>
									       </div>
									    </div>
									</div>
							    </div>
							</div>
						    </div>
						    

							</div>
						      <div id="offline_location_div" style="display: none">
							 <div class="mui--appbar-height"></div>
							 <div class="mui--appbar-height"></div>
							 <div class="col-sm-12 col-xs-12">
							    <div class="form-group">
							       <div class="row">
								  <div class="col-sm-7 col-xs-7">
								     <label class="os-lable" style="font-weight:700">SYNC FREQUENCY</label>
								  </div>
								  <div class="col-sm-5 col-xs-5 offline_not_in_visitor_management">
								     <label class="os-lable" style="font-weight:700">CP TYPE</label>
								  </div>
							       </div>
							    </div>
							 </div>
							 <div class="col-sm-12 col-xs-12"> 
							    <div class="form-group">
							       <div class="row">
								  <div class="col-sm-7 col-xs-7">
								     <label class="radio-inline" style="margin-right:10px">
									<input name="offline_sync_frequency" value="1"  type="radio" checked> Hourly
								     </label>
								     <label class="radio-inline" style="margin-right:10px">
									<input name="offline_sync_frequency" value="2" type="radio" > Daily
								     </label>
								     <label class="radio-inline" style="margin-right:10px">
									<input name="offline_sync_frequency" value="3" type="radio"> Weekly
								     </label>
								     <label class="radio-inline" style="margin-right:10px">
									<input name="offline_sync_frequency" value="4" type="radio"> Live
								     </label>
								  </div>
								  <div class="col-sm-5 col-xs-5 offline_not_in_visitor_management">
								     <label class="radio-inline" style="margin-right:10px">
									<input name="offline_cp_content_type" value="1"  type="radio" checked> Dynamic Content
								     </label>
								     <label class="radio-inline" style="margin-right:10px">
									<input name="offline_cp_content_type" value="2" type="radio"> Static CP
								     </label>
								  </div>
							       </div>
							    </div>
							 </div>
							 <div id="dynamic_login_enable" class="offline_not_in_visitor_management">
							    <div class="col-sm-12 col-xs-12" >
							       <div class="row">
								  <div class="col-sm-6 col-xs-6">
								     <div class="row">
									<div class="col-sm-8 col-xs-8">
									   <h6 class="toggle_heading"> Enable User Registration</h6>
									</div>
									<div class="col-sm-4 col-xs-4">
									   <label class="switch">
									   <input type="checkbox" name="enable_offline_dynamic_registration" value="1">
									      <div class="slider round"></div>
									   </label>
									</div>
								     </div>
								     
								  </div>
							       </div>
							    </div>
							 </div>
							 <div id="offline_cp_type_div_static" style="display: none">
							    <div class="col-sm-12 col-xs-12" >
							       
							      <div class="row">
								 <div class="col-sm-6 col-xs-6" style="padding-right:0px">
								     <div class="mui-textfield mui-textfield--float-label">
									<input class="mui--is-not-empty mui--is-untouched mui--is-pristine" name="cp_source_folder_path" id="cp_source_folder_path" type="text" >
									<label>Cp Source Folder Path <sup>*</sup></label>
								     </div>
								  </div>
								 
								 <div class="col-sm-6 col-xs-6" style="padding-right:0px">
								     <div class="mui-textfield mui-textfield--float-label">
									<input class="mui--is-not-empty mui--is-untouched mui--is-pristine" name="cp_landing_path_path" id="cp_landing_path_path" type="text" >
									<label>CP Landing Page Path <sup>*</sup></label>
								     </div>
								  </div>
								 
							       </div>  
								
							    </div>
							    <!--div class="col-sm-12 col-xs-12" >
							       
							      <div class="row">
								 <div class="col-sm-6 col-xs-6" style="padding-right:0px">
								     <div class="mui-textfield mui-textfield--float-label">
									<input class="mui--is-not-empty mui--is-untouched mui--is-pristine" name="cp_landing_path_path" id="cp_landing_path_path" type="text" >
									<label>CP Landing Page Path <sup>*</sup></label>
								     </div>
								  </div>
							       </div>  
								
							    </div-->
							    
							 </div>



						      </div>
						      
						      <div id="cafe_plan_div" style="display: none">
							 
							
							 
							 
							 <div id="cafe_retail_time_plan_div">
							    <div class="col-sm-12 col-xs-12">
							       <h4 style="text-transform: uppercase; margin-bottom:0px">Select plans to be available</h4>
							       <h4 style="margin-top:0px; font-size:18px; font-weight: 300 "><small>(You can select one plan for the location)</small></h4>
							     </div>
							    <div class="row">
							       <div class="col-sm-12 col-xs-12">
								     <div class="col-sm-6 col-xs-6">
									<label class="os-lable"></label>
									<select class="form-control so-add-zone"  name="cp_cafe_plan" id="cp_cafe_plan" onchange="change_plan(this)">
									      <?php
										 echo $plan_list = $this->location_model->plan_list('6', '0');
									      ?>
									</select>	
								     </div>
								     <div class="col-sm-3 col-xs-3">
									<button type="button" class="mui-btn mui-btn--outline-plan btn-sm btn-block" onclick="save_time_plan_other_location_cafe()">Save Plan<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
								  </div>
							       </div>
							    </div>
							 </div>
							
                                                         
                                                         <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="download_rate" class="download_rate">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Download Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="upload_rate" class="upload_rate">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Upload Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                         <div class="col-sm-12 col-xs-12">
                                                            <span class="os-otp">If actual bandwidth is lower than limits set here, user will be limited to actual bandwdith available at the location.</span>
                                                         </div>
                                                         <div class="row data_limit_div">
                                                            <div class="col-sm-12 col-xs-12">
                                                               <div class="col-sm-8 col-xs-8">
                                                                  <div class="row">
                                                                     <div class="col-sm-4 col-xs-4">
                                                                        <div class="form-group os-form-group">
                                                                           <label class="checkbox-inline">
                                                                              <input name="datalimit_checkbox" value="option1" type="checkbox" class="openmodal datalimit_checkbox"> Data Limit
                                                                           </label>
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-sm-6 col-xs-6 nopadding-left">
                                                                        <div class="mui-textfield ">
                                                                           <input type="text" name="datalimit" class="datalimit">
                                                                           <span class="title_box">MB</span>
                                                                           <label>Data Limit<sup>*</sup></label>
                                                                        </div>
                                                                     </div>
                                                                  </div>  
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="row time_limit_div">
                                                            <div class="col-sm-12 col-xs-12">
                                                               <div class="col-sm-8 col-xs-8">
                                                                  <div class="row">
                                                                     <div class="col-sm-4 col-xs-4">
                                                                        <div class="form-group os-form-group">
                                                                           <label class="checkbox-inline">
                                                                              <input name="timelimit_checkbox" value="option1" type="checkbox" class="openmodal timelimit_checkbox"> Time Limit
                                                                           </label>
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-sm-6 col-xs-6">
                                                                        <div class="os-range">
                                                                           <span class="os-currentVal">5</span>
                                                                           <span class="os-currentVal2">600</span>
                                                                        </div>
                                                                     </div>
                                                                  </div>  
                                                               </div>
                                                            </div>
                                                         </div>
                                                         
                                                         <div class="row">
                                                            <div class="col-sm-12 col-xs-12 cp1_div">
                                                               <div class="col-sm-6 col-xs-6">
                                                                  <div class="mui-textfield mui-textfield--float-label">
                                                                     <input type="text" name="cp1_no_of_daily_session_other" id="cp1_no_of_daily_session_other">
                                                                     <label>NO OF DAILY SESSION <sup>*</sup></label>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="other_location_plan_div" style="display: none">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <h4 style="text-transform: uppercase; margin-bottom:0px">Select plans to be available</h4>
                                                            <h4 style="margin-top:0px; font-size:18px; font-weight: 300 "><small>(You can select multiple plans for the location)</small></h4>
                                                         </div>
                                                         <div class="row">
                                                            <div class="col-sm-12 col-xs-12">
                                                               <div class="col-sm-6 col-xs-6">
                                                                  <label class="os-lable"></label>
                                                                  <select class="form-control so-add-zone"  name="cp_hotel_plan" id="cp_hotel_plan" onchange="change_plan(this)">
                                                                           <?php
									   // plan type 6 means (conbination of time and hybrid)
                                                                              echo $plan_list = $this->location_model->plan_list('6', '0');
                                                                              
                                                                           ?>
                                                                  </select>	
                                                               </div>
                                                               <div class="col-sm-3 col-xs-3">
                                                                     <button type="button" class="mui-btn mui-btn--outline-plan btn-sm btn-block" onclick="save_time_plan_other_location()">Save Plan<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                                                               </div>
                                                                  <div class="col-sm-3 col-xs-3">
                                                                     <p style="color:white" class="mui-btn mui-btn--small mui-btn--danger create_headings" onclick="cp_hotel_add_plan()">
                                                                        Add Plan To List
                                                                     </p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                                                                               <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="download_rate" class="download_rate">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Download Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6">
                                                               <div class="mui-textfield ">
                                                                  <input type="text" name="upload_rate" class="upload_rate">
                                                                  <span class="title_box">Mbps</span>
                                                                  <label>Upload Rate<sup>*</sup></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-sm-12 col-xs-12">
                                                         <span class="os-otp">If actual bandwidth is lower than limits set here, user will be limited to actual bandwdith available at the location.</span>
                                                      </div>
                                                      <div class="row data_limit_div">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-8 col-xs-8">
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4">
                                                                     <div class="form-group os-form-group">
                                                                        <label class="checkbox-inline">
                                                                           <input name="datalimit_checkbox" value="option1" type="checkbox" class="openmodal datalimit_checkbox"> Data Limit
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-sm-6 col-xs-6 nopadding-left">
                                                                     <div class="mui-textfield ">
                                                                        <input type="text" name="datalimit" class="datalimit">
                                                                        <span class="title_box">MB</span>
                                                                        <label>Data Limit<sup>*</sup></label>
                                                                     </div>
                                                                  </div>
                                                               </div>  
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row time_limit_div">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-8 col-xs-8">
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4">
                                                                     <div class="form-group os-form-group">
                                                                        <label class="checkbox-inline">
                                                                           <input name="timelimit_checkbox" value="option1" type="checkbox" class="openmodal timelimit_checkbox"> Time Limit
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="col-sm-6 col-xs-6">
                                                                     <div class="os-range">
                                                                        <span class="os-currentVal">5</span>
                                                                        <span class="os-currentVal2">600</span>
                                                                     </div>
                                                                  </div>
                                                               </div>  
                                                            </div>
                                                         </div>
                                                      </div>




                                                         <div class="row" id="cp_hotel_added_plan_listing">
                                                         </div>
                                                      </div>
                                                      <div class="col-sm-12 col-xs-12 hide_for_custom_location">
                                                         <h4>MANAGE LOGIN DETAIL</h4>
						      </div>
                                                      <div class="row hide_for_custom_location" >
							 <div class="col-sm-12 col-xs-12">
							   
							    <div class="col-sm-6 col-xs-6">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h4 class="toggle_heading"> Enable OTP Verification</h4>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="is_otpdisabled" value="0" checked>
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
							       <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp">OTP Verification maybe a legal requirement in some countries.
											Please check your local regualtions</span>
                                                                  </div>
							       </div>
							    </div>
							    
							    
                                                            
                                                            <div class="col-sm-6 col-xs-6" id="social_login_div" style="display:none">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h4 class="toggle_heading"> Enable Social Login</h4>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="is_socialloginenable" value="1">
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
                                                            <div class="col-sm-6 col-xs-6" id="pin_login_div" style="display:none">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h4 class="toggle_heading"> Enable Pin Login</h4>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="is_pinenable" value="1">
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
                                                            <div class="col-sm-6 col-xs-6" id="disablewifi_div" style="display:none">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h4 class="toggle_heading"> Enable WiFi</h4>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="is_wifidisabled" value="0" checked>
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
                                                            
                                                            <div class="col-sm-6 col-xs-6" id="loginwith_div" style="display:none">
							       <div class="row">
                                                                  <div class="col-sm-12 col-xs-12"> 
                                                                     <div class="form-group">
                                                                        <label>Login with</label>
                                                                        <label class="radio-inline" style="margin-right:10px">
                                                                           <input name="login_with_mobile_email_notpublic" value="0"  type="radio" checked="checked"> Mobile
                                                                        </label>
                                                                        <label class="radio-inline" style="margin-right:10px">
                                                                           <input name="login_with_mobile_email_notpublic" value="1" type="radio"> Email
                                                                        </label>
                                                                     </div>
                                                                  </div>
							       </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
                                                            
                                                            <div class="col-sm-6 col-xs-6" id="offer_redemption_mode_div" style="display:none">
							       <div class="row">
                                                                  <div class="col-sm-12 col-xs-12"> 
                                                                     <div class="form-group">
                                                                        <h6 class="toggle_heading">Offer Redemption Mode</h6>
                                                                        <label class="checkbox-inline" style="margin-right:10px">
                                                                           <input name="offer_redemption_mode" value="0"  type="radio" checked="checked"> SMS
                                                                        </label>
                                                                        <label class="checkbox-inline" style="margin-right:10px">
                                                                           <input name="offer_redemption_mode" value="1"  type="radio" > Email
                                                                        </label>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
                                                            
							    <div class="col-sm-6 col-xs-6" id="is_signup_enable_div" style="display:none">
							       <div class="row">
								  <div class="col-sm-8 col-xs-8">
								     <h4 class="toggle_heading"> Enable Sign Up</h4>
								  </div>
                                                                  <div class="col-sm-4 col-xs-4">
								     <label class="switch">
								     <input type="checkbox" name="is_signup_enable" value="1">
                                                                        <div class="slider round"></div>
								     </label>
								  </div>
							       </div>
                                                               <div class="row">
                                                                  <div class="col-sm-12 col-xs-12">
								     <span class="os-otp"><p><br/><br/><br/></p></span>
                                                                  </div>
							       </div>
							    </div>
							 
							 </div>
						      </div>
						      
						      <div class="row" id="contestifi_signup_fields_div"  style=" margin-bottom: 15px;display:none">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <h4>SignUp fields Require</h4>
							    </div>
							 </div>
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-3 col-xs-3">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="signup_user_name_require" value="1" checked> User Name
									   </label>
									</div>
							    </div>
							    <div class="col-sm-3 col-xs-3">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="signup_user_email_mobile_require" value="1" checked> <span id="signup_require_field_email_mobile">User Email</span>
									   </label>
									</div>
								     
							    </div>
							    <div class="col-sm-3 col-xs-3">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="signup_user_age_require" value="1" checked>  <span>User Age</span>
									   </label>
									</div>
							    </div>
							    <div class="col-sm-3 col-xs-3">
									<div class="checkbox">
									   <label>
									     <input type="checkbox" name="signup_user_gender_require" value="1" checked> <span>User Gender</span>
									   </label>
									</div>
							    </div>
							 </div>
						      </div>
						      
							<!--<div id="lms_vm_cp_org_dpt_emp_main_div" style="display:none">
							     <div class="row">
							<div class="col-sm-12 col-xs-12 mt___10">
							
							    <div class="col-sm-12 col-xs-12">
							       <span id="lms_department_button" class="lms_visitor_list_of_type_odm mui-btn mui-btn--flat mui-btn--danger" style="line-height:36px;"><strong>Table</strong></span>
							       <span id="lms_employee_button" class="lms_visitor_list_of_type_odm mui-btn mui-btn--flat" style="line-height:36px;"><strong>Employees</strong></span>
							    </div>
							 <div class="col-sm-12 col-xs-12">
							    <hr style="margin-top: 0px; margin-bottom: 10px;"/>
							 </div>
							</div>
						    </div>
						     
						    <div class="row" id="lms_department_listing_div">
						       <div class="col-sm-12 col-xs-12">
							  <div class="col-sm-12 col-xs-12">
							    
							     <div class="row">
								<div class="col-sm-6 col-xs-6">
								   <div class="row" style="margin-bottom: 10px;">
								      <div class="col-sm-10 col-xs-10" style="padding-right:0px;">
									 <input type="text" name="lms_vm_dept_name" id="lms_vm_dept_name" class="form-control" placeholder="Department Name"/>
								      </div>
								      <div class="col-sm-2 col-xs-2"><span onclick="lms_add_vm_offline_department()" class="mui-btn mui-btn--danger" style="line-height:36px">Add</span></div>
								   </div>
								</div>
							     </div>
							  </div>
							  <div class="col-sm-12 col-xs-12">
							     <div class="col-sm-6 col-xs-6">
								<p id= 'lms_add_offline_department_error' style="color:red"></p>
							     </div>
							  </div>
 
						       </div>
						       <div class="col-sm-12 col-xs-12">
							  <div class="table-responsive">
							     <div class="col-sm-12 col-xs-12 table-fixed-tbody">
								<table class="table table-striped" id="vm_dept_list_tabel">
								   <thead>
								      <tr class="active">
									 <th>Department Name </th>
									 <th>Action</th>
								      </tr>
								   </thead>
								   <tbody id="lms_vm_dept_list_tabel_data">
								      
								   </tbody>
								</table>
							     </div>
							  </div>
						       </div>
 
						    </div>
						       
						    
						    
						    <div class="row" id="lms_employee_listing_div" style="display:none">
						      <div class="col-sm-12 col-xs-12">  
							 <div class="col-sm-12 col-xs-12">
							    <div class="row">
								<div class="col-sm-6 col-xs-6">
								  <select class="form-control" name="lms_vm_emp_dept_name" id="lms_vm_emp_dept_name">
								     <option>Select Depatment</option>
								  </select>
							       </div>
								<div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="lms_vm_emp_id" id="lms_vm_emp_id">
								     <label>Employee ID</label>
								  </div>
							       </div>
							       
							    </div>
							    <div class="row">
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="lms_vm_emp_name" id="lms_vm_emp_name">
								     <label>Employee First Name</label>
								  </div>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="lms_vm_emp_last_name" id="lms_vm_emp_last_name">
								     <label>Last Name</label>
								  </div>
							       </div>
							    </div>
							    <div class="row">
								<div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="lms_vm_emp_email" id="lms_vm_emp_email">
								     <label>Employee Email</label>
								  </div>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <div class="row" style="margin-bottom: 10px;">
								     <div class="col-sm-10 col-xs-10" style="padding-right:0px;">
									<div class="mui-textfield mui-textfield--float-label">
									   <input type="text" name="lms_vm_emp_mobile" id="lms_vm_emp_mobile">
									   <label>Employee Phone</label>
									</div>
								     </div>
								     <div onclick="lms_add_vm_offline_employee()" class="col-sm-2 col-xs-2"><span class="mui-btn mui-btn--danger" style="line-height:36px">Add</span></div>
								  </div>
							       </div>
							    </div>
							 </div>
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <p id= 'lms_add_offline_employee_error' style="color:red"></p>
							    </div>
							 </div>
						      </div>
						      <div class="col-sm-12 col-xs-12">
							 <div class="table-responsive">
							     <div class="col-sm-12 col-xs-12 table-fixed-tbody">
								<table class="table table-striped" id="vm_emp_list_tabel">
								   <thead>
								      <tr class="active">
									<th>Employee ID</th>
									 <th>First Name</th>
									 <th>Last Name</th>
									 <th>Email</th>
									 <th>Mobile</th>
									 <th>Department</th>
									 
									 <th>Action</th>
								      </tr>
								   </thead>
								   <tbody id="lms_vm_emp_list_tabel_data">
								      
								   </tbody>
								</table>
							     </div>
							  </div>
						      </div>
						   </div>

						    
                                                        </div>-->
                                                        <!--
                                                      <div id="vm_cp_org_dpt_emp_main_div" style="display:none">
							   <div class="row">
						      <div class="col-sm-12 col-xs-12 mt___10">
							
							 <div class="col-sm-12 col-xs-12">
							    <span id="organisation_button"  class="visitor_list_of_type_odm mui-btn mui-btn--flat mui-btn--danger" style="line-height:36px;"><strong>Organisations</strong></span>
							  
							    <span id="employee_button" class="visitor_list_of_type_odm mui-btn mui-btn--flat" style="line-height:36px;"><strong>Employees</strong></span>
							 </div>
							 <div class="col-sm-12 col-xs-12">
							    <hr style="margin-top: 0px; margin-bottom: 10px;"/>
							 </div>
						      </div>
						   </div>
						   
						   
						   <div class="row" id="organisation_listing_div">
						      <div class="col-sm-12 col-xs-12">
							 <div class="col-sm-6 col-xs-6">
							    <div class="row" style="margin-bottom: 10px;">
							       <div class="col-sm-10 col-xs-10"  style="padding-right:0px;">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="offline_organisation_name" id="offline_organisation_name">
								     <label>Table Name</label>
								  </div>
							       </div>
							       
							       <div class="col-sm-2 col-xs-2">
								 
								  <span onclick="add_vm_organisation()" class="mui-btn mui-btn--danger" style="line-height:36px">Add</span>
							       </div>
							    </div>
							 </div>
						      </div>
						      <div class="col-sm-12 col-xs-12">
							 <div class="col-sm-6 col-xs-6">
							    <p id= 'add_offline_organisation_error' style="color:red"></p>
							 </div>
						      </div>
						       
						      <div class="col-sm-6 col-xs-6">
							 <div class="table-responsive">
							    <div class="col-sm-12 col-xs-12 table-fixed-tbody">
							       <table class="table table-striped" id="organisation_list_tabel" style="display:none">
								  <thead>
								     <tr class="active">
									<th>Table List</th>
									<th>Action</th>
								     </tr>
								  </thead>
								  <tbody id="organisation_list_tabel_data">
								     
								  </tbody>
							       </table>
							    </div>
							 </div>
						      </div>

                                                   </div>-->
						   
						  
						      
						   <!-- div for employee -->
						  <!-- <div class="row" id="employee_listing_div" style="display:none">
						      <div class="col-sm-12 col-xs-12">  
							 <div class="col-sm-12 col-xs-12">
							    <div class="row">
							       <div class="col-sm-6 col-xs-6">
								  <select class="form-control" name="vm_emp_org_name" id="vm_emp_org_name">
								     <option>Organisation List</option>
								  </select>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <select class="form-control" name="vm_emp_dept_name" id="vm_emp_dept_name">
								     <option>Department List</option>
								  </select>
							       </div>
							    </div>
							    <div class="row">
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="vm_emp_name" id="vm_emp_name">
								     <label>Employee First Name</label>
								  </div>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="vm_emp_last_name" id="vm_emp_last_name">
								     <label>Last Name</label>
								  </div>
							       </div>
							    </div>
							    <div class="row">
								<div class="col-sm-6 col-xs-6">
								  <div class="mui-textfield mui-textfield--float-label">
								     <input type="text" name="vm_emp_email" id="vm_emp_email">
								     <label>Employee Email</label>
								  </div>
							       </div>
							       <div class="col-sm-6 col-xs-6">
								  <div class="row" style="margin-bottom: 10px;">
								     <div class="col-sm-10 col-xs-10" style="padding-right:0px;">
									<div class="mui-textfield mui-textfield--float-label">
									   <input type="text" name="vm_emp_mobile" id="vm_emp_mobile">
									   <label>Employee Phone</label>
									</div>
								     </div>
								     <div onclick="add_vm_offline_employee()" class="col-sm-2 col-xs-2"><span class="mui-btn mui-btn--danger" style="line-height:36px">Add</span></div>
								  </div>
							       </div>
							    </div>
							 </div>
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-6 col-xs-6">
							       <p id= 'add_offline_employee_error' style="color:red"></p>
							    </div>
							 </div>
						      </div>
						      <div class="col-sm-12 col-xs-12">
							 <div class="table-responsive">
							     <div class="col-sm-12 col-xs-12 table-fixed-tbody">
								<table class="table table-striped" id="vm_emp_list_tabel">
								   <thead>
								      <tr class="active">
									 <th>First Name</th>
									 <th>Last Name</th>
									 <th>Email</th>
									 <th>Mobile</th>
									 <th>Department</th>
									 <th>Organisation</th>
									 <th>Action</th>
								      </tr>
								   </thead>
								   <tbody id="vm_emp_list_tabel_data">
								      
								   </tbody>
								</table>
							     </div>
							  </div>
						      </div>
						   </div>
						     
                                                      </div>-->
						      <div class="row">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="col-sm-12 col-xs-12">
                                                               <p id="add_hotel_captive_portal_error" style="color:red"></p>
                                                               <div class="row">
                                                                  <div class="col-sm-4 col-xs-4 nopadding-right">
                                                                     <button type="submit" class="mui-btn mui-btn--outline-accent btn-sm btn-block" value="exit">SAVE & EXIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                                                                  </div>
                                                                  <div class="col-sm-4 col-xs-4 nopadding-left">
                                                                     <button type="submit" class="mui-btn mui-btn--accent btn-sm btn-block" value="save">SAVE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                                                                  </div>	 
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                             <div class="col-lg-5 col-sm-5 nopadding-left">
                                                <div class="row">
                                                   <div class="col-sm-12 col-xs-12">
                                                      <h4>CAPTIVE PORTAL PREVIEW</h4>
						      <h6>Captive Portal URL:
                                                         <small class="cp_path_url_display">-- Add NAS Setup to generate a CP URL --</small>
                                                      </h6>
                                                      
                                                       <h6> Table Portal URL:
                                                         <small class="cp_path_url_display_table">-- Add NAS Setup to generate a CP URL --</small>
                                                      </h6> 
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-sm-12 col-xs-12 nopadding-left">
                                                                                          <div class="col-sm-12 col-xs-12">
                                       <div class="slider_div pull-right">
                                          <div class="right_slider">
                                             <div id="crsl_items_hotel_location" class="crsl-items" data-navigation="navbtns_cphotel">
                                                <div class="crsl-wrap">
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_1_img.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_hospital_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_institutional_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_cafe_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_retail_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/retail_1.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_purple_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/purple_1.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_offline_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_1.png" class="img-responsive "/>
                                                         </center>
							  <center class="cp_offline_vm_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_vm_1.png" class="img-responsive "/>
                                                         </center>
							  <center class="cp_event_module_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/event_img_1.png" class="img-responsive "/>
                                                         </center>
							  <center class="cp_retail_audit_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/retail_audit_1.png" class="img-responsive "/>
                                                         </center>
							  <center class="cp_contestifi_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_cn_1.png" class="img-responsive "/>
                                                         </center>
                                                      </div>
						      
                                                      <div class="crsl-item-logo-hotel right_side_logo">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo base_url()?>assets/images/radisson_blu_logo.png" class="img-responsive brand_logo_preview_slider location_logo_preview_show" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_2_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_2.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_2.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_2.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_cafe_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_2.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_retail_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/retail_2.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_purple_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/purple_2.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_offline_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_2.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_offline_vm_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_vm_2.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_event_module_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/event_img_2.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_retail_audit_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/retail_audit_2.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_contestifi_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_cn_2.png" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel right_side_logo">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo base_url()?>assets/images/radisson_blu_logo.png" class="img-responsive brand_logo_preview_slider location_logo_preview_show"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_3_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_3.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_3.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_3.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_cafe_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_3.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_retail_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/retail_3.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_purple_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/purple_3.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_offline_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_3.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_offline_vm_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_vm_3.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_event_module_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/event_img_3.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_retail_audit_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/retail_audit_3.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_contestifi_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_cn_3.png" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel slider1_brand_logo right_side_logo" style="display:block">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo base_url()?>assets/images/radisson_blu_logo.png" class="img-responsive brand_logo_preview_slider location_logo_preview_show"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_4_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_4.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_4.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_4.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_cafe_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_4.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_retail_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/retail_4.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_purple_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/purple_4.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_offline_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_4.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_offline_vm_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_vm_4.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_event_module_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/event_img_4.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_retail_audit_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/retail_audit_4.png" class="img-responsive "/>
                                                         </center>
							 <center class="cp_contestifi_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/offline_cn_4.png" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel right_side_logo">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo base_url()?>assets/images/radisson_blu_logo.png" class="img-responsive brand_logo_preview_slider location_logo_preview_show"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <nav class="slidernav">
                                                <div id="navbtns_cphotel" class="clearfix">
                                                   <a href="#" class="previous pull-left">
                                                   <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                   </a>
                                                   <span class="previous_span"><strong>Setp 01:</strong> User Enter Mobile No.</span>
                                                   <a href="#" class="next pull-right">
                                                   <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                   </a>
                                                </div>
                                             </nav>
                                          </div>
                                       </div>
                                    </div>

                                                   </div>
                                                </div>
                                             </div>

                                          </div>
                                       </div>
                                       <!-- Captive portal tab END -->
                                       
                                       
                                       <!-- terms of use start -->	
                                       <div class="mui-tabs__pane" id="terms_of_use">
                                          <div class="col-lg-12 col-sm-12">
                                             <div class="row">
						<div class="col-sm-12 col-xs-12">
						   <h4>TERMS OF USE</h4>
						</div>	
					     </div>
                                             <div class="row">
                                                <form id="configure_nas" onsubmit="update_terms_of_use(); return false;">
                                                <div class="col-sm-12 col-xs-12">
                                                   
                                                      <textarea required id="editor1" name="terms_of_use_content">
                                                      </textarea> 
                                                   
                                                </div>
                                                
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                   <button type="submit"  class="mui-btn mui-btn--accent btn-lg save_btn">SAVE</button>
                                                </div>
                                                </form>
                                             </div>
                                          </div>
                                          
                                       </div>
                                       <!-- terms of use  end -->	
                                       
				       
				       
				        <!-- sms  tab start -->
				       <div class="mui-tabs__pane" id="sms">
					  <div class="row">
					     <div class="col-sm-6 col-xs-6">
						<label class="os-lable">Add OTP SMS</label>
					     </div>
					     
					  </div>
					  <div class="row">
					     <div class="col-sm-6 col-xs-6">
						<form id="add_otp_sms" onsubmit="add_otp_sms(); return false;">
						      <div class="row">
							 <div class="col-sm-5 col-xs-5">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="number" id="otp_sms" name="otp_sms" required>
							       <label>No Of OTP SMS <sup>*</sup></label>
							    </div>
							 </div>
							 <div class="col-sm-3 col-xs-3">
							    <button type="submit" class="btn mui-btn--small mui-btn--accent">ADD</button>
							 </div>
						      </div>
						      
						   </form>

					     </div>
					  </div>
					  <div class="row">
					     <div class="col-sm-6 col-xs-6">
						<label class="os-lable">Add Marking SMS</label>
					     </div>
					     
					  </div>
					  <div class="row">
					     <div class="col-sm-6 col-xs-6">
						<form id="add_otp_sms" onsubmit="add_marketing_sms(); return false;">
						      <div class="row">
							 <div class="col-sm-5 col-xs-5">
							    <div class="mui-textfield mui-textfield--float-label">
							       <input type="number" id="marketing_sms" name="marketing_sms" required>
							       <label>No Of Marketing SMS <sup>*</sup></label>
							    </div>
							 </div>
							 <div class="col-sm-3 col-xs-3">
							    <button type="submit" class="btn mui-btn--small mui-btn--accent">ADD</button>
							 </div>
						      </div>
						      
						   </form>

					     </div>
					  </div>
					  
					 <p style="color:red" id='sms_assing_error'></p>
					  <div class="row" style="margin-top:15px;">
					     <div class="col-sm-6 col-xs-6">
						<div>
						   <div class="row">
						      <div class="col-sm-12 col-xs-6">
							 <div class="table-responsive">
							    <table class="table table-striped">
							       <thead>
								  <tr class="active">
								     <th>&nbsp;</th>
								     <th>SMS TYPE</th>
								     <th>ADDED ON</th>
								     <th>ASSIGN</th>
								     <th>USED</th>
								     <th>BALANCE</th>
								  </tr>
							       </thead>
							       <tbody id="sms_list">
							       </tbody>
							    </table>
							 </div>
						      </div>
						   </div>
						</div>
						
					     </div>
					  </div>
				       </div>
				       <!-- SMS tab END -->
                                       
                                       
                                       
                                    </div>
				    
                                 </div>
                           </div>
                        </div>
		     </div>    
                  </div>
            </div>
         </div>
      </div>
   </div>
      
      
		
		
		
		
		<!---- Save New Plans Modal ----->
				<div class="modal fade" id="save_plan_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
<!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
				<h4 class="os-modal-title">Save New Plan</h4>
			  </div>
                          <form id="save_plan" onsubmit="save_plan(); return false;">
			  <div class="modal-body" style="padding:10px 15px">
				
			       <div class="mui-textfield ">
                                       <input type="hidden" name="edit_plan_id" id="edit_plan_id">
                                          <input type="hidden" name="plan_button_click" id="plan_button_click">
                                          <input type="hidden" name="edit_data_limit" id="edit_data_limit">
                                          <input type="hidden" name="edit_time_limit" id="edit_time_limit">
                                             <input type="hidden" name="edit_plan_downrate" id="edit_plan_downrate">
                                                <input type="hidden" name="edit_plan_uprate" id="edit_plan_uprate">
                                                   <input type="hidden" name="edit_plan_type" id="edit_plan_type">
                                          <input type="hidden" name="add_plan_select_box_id" id="add_plan_select_box_id">
					<input type="text" name="plan_name" id="plan_name" required>
					   <label>Plan Name <sup>*</sup></label>
				  </div>
				  <div class="mui-textfield">
					<textarea name="plan_desc" id="plan_desc"></textarea>
					<label>Plan Description</label>
				  </div>
				
			  </div>
			  <div class="modal-footer" style="text-align: right">
                           <center><p id="save_plan_error" style="color:red"></p></center>
			 <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
			 <button type="submit" class="mui-btn mui-btn--os-accent" >SAVE PLAN<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
                          </form>
			</div>
		  </div>
		</div>

      <!---- Save New Plans Modal ----->
      <!-- add zone modal start -->
      <div class="modal modal-container fade" id="add_zone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>ADD ZONE</strong></h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Add a new zone to your location listing.</p>
                  <div class="mui-textfield mui-textfield--float-label">
                     <input type="text" id="zone_name_popup" name="zone_name_popup">
                     <label>Zone Name</label>
                  </div>
                  <div class="row">
                     <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                           <select class="form-control" id="city_popup">
                           <?php
                              echo $city_list = $this->location_model->city_list($edit_location_data->state_id,$edit_location_data->city_id);
                                  ?>
                           </select>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px ">
                        <!--a class="mui-btn mui-btn--small mui-btn--flat" style="color:#29ABE2 ">Add City</a-->
                     </div>
                  </div>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d ; color:#fff ">CANCEL</button>
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="add_zone()">ADD</button>
               </div>
            </div>
         </div>
      </div>


      <!-- add zone modal End -->
      
      <!-- add city modal start -->
      <div class="modal modal-container fade" id="add_city" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>ADD CITY</strong></h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Add a new city to your location listing.</p>
                  <div class="mui-textfield mui-textfield--float-label">
                     <input type="text" id="city_name_popup" name="city_name_popup">
                     <label>city Name</label>
                  </div>
                  <div class="row">
                     <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                           <select class="form-control" id="state_popup">
                           <?php
                              echo $state_list = $this->location_model->state_list_add_city();
                                  ?>
                           </select>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px ">
                        <!--a class="mui-btn mui-btn--small mui-btn--flat" style="color:#29ABE2 ">Add City</a-->
                     </div>
                  </div>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d ; color:#fff ">CANCEL</button>
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="add_city()">ADD</button>
               </div>
            </div>
         </div>
      </div>


      <!-- add city modal End -->
      
       <!-- modal  for asking user to router username and pass and ip  start-->
      <div class="modal fade" id="add_location_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?php echo base_url()?>assets/images/close_circle.png"/>
				</button>
				<h4 class="os-modal-title">Add new device for Microtik</h4>
			  </div>
                          <form id="add_location" onsubmit="start_configuration(); return false;">
			  <div class="modal-body" style="padding:10px 15px">
				
		          <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield">
                                                    <input type="hidden" id="nas_id_selected" name="nas_id_selected" class="form-control">
                                                   <input type="text" class="form-control"  name="router_ip" id="router_ip" required>
						   <label>Router IP <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				  <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text" class="form-control" name="router_user" id="router_user" required>
						   <label>Router Username <sup>*</sup></label>
					   </div>
					  </div>
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="password" class="form-control"  name="router_password" id="router_password" >
						   <label>Router Password <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				  <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="number" class="form-control" name="router_port" id="router_port" >
					      <span class="title_box">Optional</span>
						   <label>Port</label>
					   </div>
					  </div>
				  </div>
				
			  </div>
			  <div class="modal-footer">
                           <p id="router_connection_error" style="color:red"> </p>
			 <button type="submit" class="mui-btn mui-btn--os-accent" >NEXT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
                          </form>
			</div>
		  </div>
		</div>

      <!-- modal  for asking user to router username and pass and ip  end-->
      
      <!-- modal  for asking user to router type  start-->
      		<div class="modal fade" id="connection_type_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?php echo base_url()?>assets/images/close_circle.png"/>
				</button>
				<h4 class="os-modal-title">Add new device for Microtik</h4>
			  </div>
                          <form id="add_location" onsubmit="start_configuration1(); return false;">
			  <div class="modal-body" style="padding:10px 15px">
				
                           <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Select Router Type <sup>*</sup></label>
							<select class="form-control"  name="router_type_wap" id="router_type_wap" required>
							<option value="">Select Router type</option>
                              <option value="WAP">WAP</option>
                              <option value="WAPAC">WAP AC</option>
                              <option value="Rb850Gx2">Rb850Gx2</option>
                              <option value="Rb941-2nD">Rb941-2nD</option>
                              <option value="2011iL">2011iL-IN / 2011iL-RM</option>
                              <option value="CCR1036-8G-2S">CCR1036-8G-2S+ / CCR1036-8G-2S+-EM</option>
                              <option value="921GS-5HPacD">921GS-5HPacD</option>
			      <option value="750r2-gr3">750r2-Gr3</option>
			      <option value="952-ui-5ac2nd">952-Ui-5ac2nD</option>
			      <option value="450g">450G</option>
							</select>
					  </div>
				  </div>
				
			  </div>
			  <div class="modal-footer">
                           <p id="router_connection_error1" style="color:red"> </p>
			 <button type="submit" class="mui-btn mui-btn--os-accent" >NEXT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
                          </form>
			</div>
		  </div>
		</div>

      <!-- modal  for asking user to router type  end-->
      
      <!-- modal  for asking user to router is reset or not  start-->
      		<div class="modal fade" id="is_router_reset_or_not" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?php echo base_url()?>assets/images/close_circle.png"/>
				</button>
				<h4 class="os-modal-title">Add new device for Microtik</h4>
			  </div>
			  <div class="modal-body" style="padding:10px 15px">
				  <p>Almost done. You can now configure your router. Please ensure the router has been reset once before configuring it.</p>
			  </div>
			  <div class="modal-footer">
                           <p id="is_router_reset_or_not_error" style="color:red"> </p>
			  <button onclick="reset_wap_router()" type="submit" class="mui-btn mui-btn--os-accent-outline" >RESET ROUTER<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
			 <button onclick="check_wap_router_reset_porperly()" type="submit" class="mui-btn mui-btn--os-accent">DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
			</div>
		  </div>
		</div>

<!-- modal  for asking user to router is reset or not  end-->
<!---- configure Modal ----->
		
		<div class="modal fade" id="done_configuration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
<!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
				<h4 class="os-modal-title">Configuration Done!</h4>
			  </div>
			  <div class="modal-body" style="padding:10px 15px">
				<p style="color:#999999; font-style: italic">Router was succesfully configured.</p>
			  </div>
			  <div class="modal-footer" style="text-align: right">
			 <button type="button" class="mui-btn mui-btn--os-accent" data-dismiss="modal" >OK<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
			</div>
		  </div>
		</div>
        <!---- configure Modal ----->
        
        <div class="modal modal-container fade" id="add_ap_location_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><strong>ADD AP LOCATION</strong></h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p>Add a new ap location  to your location listing.</p>
            <div class="mui-textfield mui-textfield--float-label">
               <input type="text" id="ap_location_name_popup" name="ap_location_name_popup">
               <label>AP Location Name</label>
            </div>
           
         </div>
         <div class="modal-footer" style="text-align: right">
	    <p style="color:red" id="add_ap_location_error"></p>
            <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d ; color:#fff ">CANCEL</button>
            <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="add_new_ap_location()">ADD</button>
         </div>
      </div>
   </div>
</div>
        <!-- edit ACCESS point modal -->
<div class="modal modal-container fade"  tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static" id="EditaccesspointModal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title">Edit Access Point</h4>
            </center>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="update_location_vouchers" onsubmit="update_location_access_point(); return false;">
                     <div class="row">
                       
                       <input type="hidden" id="access_point_id_edit" name="access_point_id_edit" required>
                        <div class="col-sm-3 col-xs-3">
                           <div class="mui-textfield ">
                              <input type="text" id="access_point_macid_edit" name="access_point_macid_edit" required>
                              <label>MAC ID  <sup>*</sup></label>
                           </div>
                        </div>
                        <!--div class="col-sm-3 col-xs-3">
                           <div class="mui-textfield ">
                              <input type="text" id="access_point_ip_edit" name="access_point_ip_edit" required>
                              <label>IP <sup>*</sup></label>
                           </div>
                        </div-->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select id="edit_ap_location" name="edit_ap_location" required>
                                                      <option value="">Select AP location</option>
                                                   </select>
                                                   <label>AP Location<sup>*</sup></label>
                                                </div>
                                             </div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h5 class="create_headings" onclick="add_ap_location_model()">
                                                   Add New AP Location
                                                </h5>
                                             </div>
                     </div>
                     <div class="row" style="margin-top:25px;">
                        <center>
                           <p style="color: red" id="edit_access_point_errro"></p>
                           <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">SAVE</button>
                        </center>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- One hop network create start-->
          <div class="modal fade" id="create_onehop_netword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?php echo base_url()?>assets/images/close_circle.png"/>
				</button>
				<h4 class="os-modal-title">Create new network</h4>
			  </div>
                          <form id="add_location" onsubmit="create_onehop_network(); return false;">
			  <div class="modal-body" style="padding:10px 15px">
				
		          <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text"  name="onehop_organization" id="onehop_organization" value="SHOUUT" readonly="readonly" required>
                                                       <input type="hidden" name="onehop_address" id="onehop_address" >
                                                      <input type="hidden" name="onehop_latitude" id="onehop_latitude" >
                                                      <input type="hidden" name="onehop_longitude" id="onehop_longitude" >
                                                      <input type="hidden" name="onehop_is_new_network" id="onehop_is_new_network" >
						   <label>Organization Name <sup>*</sup></label>
					   </div>
					  </div>
					  <div class="col-sm-6 col-xs-6">
					  	  <textarea class="form-control" rows="3" placeholder="Description*" style="padding:5px 10px" name="onehop_description" id="onehop_description"></textarea>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-t">
					  	<div class="mui-textfield">
						<input type="text"  name="onehop_network_name" id="onehop_network_name" readonly="readonly" required>
						   <label>Network Name <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				  <div class="row">
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield">
						<input type="number" name="onehop_maxaps" id="onehop_maxaps"  readonly="readonly" >
						   <label>Max APs <sup>*</sup></label>
					   </div>
					  </div>
					   <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">COA Status <sup>*</sup></label>
							<select class="form-control" name="onehop_coa_status" id="onehop_coa_status">
							<option value='1'>Enable</option>
							</select>
					  </div>
				  </div>
				  <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield">
						<input type="text" name="onehop_coa_ip" id="onehop_coa_ip" readonly="readonly">
							<label>COA IP <sup>*</sup></label>
					   </div>
					  </div>
					  	<div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield">
						<input type="text" name="onehop_coa_secret" id="onehop_coa_secret" readonly="readonly">
						   <label>COA Secret <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				  <div class="row">
				     <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Beacon Status <sup>*</sup></label>
							<select class="form-control" name="onehop_beacon_status" id="onehop_beacon_status">
							<option value='1'>Enable</option>
							<option value='0'>Disable</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield">
						<input type="text" name="onehop_beacon_url" id="onehop_beacon_url"  readonly="readonly" >
						   <label>Beacon URL <sup>*</sup></label>
					   </div>
					  </div>
					  
				  </div>
				
			  </div>
			  <div class="modal-footer">
                           <p id="create_onehop_network_status" style="color:red"> </p>
			 <button type="submit" class="mui-btn mui-btn--os-accent" id="create_network_button">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                         <button type="submit" class="mui-btn mui-btn--os-accent" id="update_network_button">UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
                          </form>
			</div>
		  </div>
		</div>

<!-- One hop network create end-->

<!-- One hop add ssid start-->
<div class="modal fade" id="one_hop_configure_ssid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog"  role="document" style="margin-top:0px;">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?php echo base_url()?>assets/images/close_circle.png"/>
				</button>
				<h4 class="os-modal-title">Configure SSID</h4>
			  </div>
			  <div class="modal-body configure_modal_fixed" style="padding:0px 15px 5px 15px">
				<form class="mui-form">
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">SSID Name <sup>*</sup></label>
							<select class="form-control">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
						   <label>SSID Name <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				   <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Association <sup>*</sup></label>
							<select class="form-control">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text" placeholder="Pre-Shared Key">
					   </div>
					  </div>
				  </div>
				   <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">WPA Mode <sup>*</sup></label>
							<select class="form-control">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Forwarding <sup>*</sup></label>
							<select class="form-control">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							</select>
					  </div>
				  </div>
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">CP Mode <sup>*</sup></label>
							<select class="form-control">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
						   <label>CP URL <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
						  <div class="row">
							  <div class="col-sm-12 col-xs-12">
								  <div class="row">
									  <div class="col-sm-10 col-xs-10 nopadding-right">
										  <div class="mui-textfield mui-textfield--float-label">
											<input type="text">
											    <span class="chip_text">Press Enter to Add</span>
												<label>Walled Garden IP List <sup>*</sup></label>
										   </div>
									  </div>
									  <div class="col-sm-2 col-xs-2 col-pt-30">
										 <i class="fa fa-plus-square-o fa-2x square-o" aria-hidden="true"></i>
									  </div>
								  </div>
							  </div>
							  <div class="col-sm-12 col-xs-12">
								<span class="chip">192.168.0.1 <i class="close fa fa-times" aria-hidden="true"></i></span>
								<span class="chip">192.168.0.2 <i class="close fa fa-times" aria-hidden="true"></i></span>
							  </div>
						  </div>
			     </div>
					  <div class="col-sm-6 col-xs-6">
						  <div class="row">
							  <div class="col-sm-12 col-xs-12">
								 <div class="row">
									  <div class="col-sm-10 col-xs-10 nopadding-right">
										  <div class="mui-textfield mui-textfield--float-label">
											<input type="text">
											    <span class="chip_text">Press Enter to Add</span>
												<label>Walled Garden Domain List <sup>*</sup></label>
										   </div>
									  </div>
									  <div class="col-sm-2 col-xs-2 col-pt-30">
										 <i class="fa fa-plus-square-o fa-2x square-o" aria-hidden="true"></i>
									  </div>
								  </div>  
							  </div>
							  <div class="col-sm-12 col-xs-12">
								 <span class="chip">www.loremipsum.com <i class="close fa fa-times" aria-hidden="true"></i></span>
								  
							  </div>
						  </div>
					  
					  </div>
				  </div>
				  <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
							<label>Auth Server IP <sup>*</sup></label>
					   </div>
					  </div>
					  	<div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
						   <label>Auth Server Port <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				  <div class="row">
				  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
						   <label>CP URL <sup>*</sup></label>
					   </div>
					  </div>
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">CP Mode <sup>*</sup></label>
							<select class="form-control">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							</select>
					  </div>
					  
				  </div>
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
						   <label>Accouting Server IP <sup>*</sup></label>
					   </div>
					  </div>
				    <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
						   <label>Accouting Server Port <sup>*</sup></label>
					   </div>
					  </div>
					  
				  </div>
				  
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
						   <label>Accouting Server Secret <sup>*</sup></label>
					   </div>
					  </div>
				    <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield mui-textfield--float-label">
						<input type="text">
						   <label>Accouting Interval in Seconds <sup>*</sup></label>
					   </div>
					  </div>
					  
				  </div>
				</form>
			  </div>
			  <div class="modal-footer">
			 <button type="button" class="mui-btn mui-btn--os-accent" data-dismiss="modal" data-toggle="modal" data-target="#">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
			</div>
		  </div>
		</div>

<!-- One hop add ssid end-->

<!-- One hop add ssid start-->
<div class="modal fade" id="add_onehop_ssid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog"  role="document" style="margin-top:0px;">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?php echo base_url()?>assets/images/close_circle.png"/>
				</button>
				<h4 class="os-modal-title">Configure SSID</h4>
			  </div>
                          <form id="add_location" onsubmit="add_onehop_ssid(); return false;">
			  <div class="modal-body " style="padding:0px 15px 5px 15px">
				
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">SSID Index <sup>*</sup></label>
							<select class="form-control" name="onehop_ssid_index" id="onehop_ssid_index" required>
							<option value='0'>Index 0</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield">
						<input type="text" name="onehop_ssid_name" id="onehop_ssid_name" readonly="readonly"  required>
						   <label>SSID Name <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				   <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Association <sup>*</sup></label>
							<select class="form-control" name="onehop_association" id="onehop_association">
							<option value='0'>Open(No Encription)</option>
                                                         <option value='1'>PSK</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield ">
						<input type="text" name="onehop_psk" id="onehop_psk">
						   <label>Pre Shared Key</label>
					   </div>
					  </div>
				  </div>
				   <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">WPA Mode <sup>*</sup></label>
							<select class="form-control" name="onehop_wap_mode" id="onehop_wap_mode">
							<option value='3'> WPA and WPA2 </option>
				 <option value='1'> WPA Only </option>
				 <option value='2'> WPA2 Only </option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Forwarding <sup>*</sup></label>
							<select class="form-control" name="onehop_forwarding" id="onehop_forwarding">
							<option value='1'>  NAT Mode  </option>
				 <option value='2'> Bridge Mode  </option>
				 <option value='3'> Tunnel Mode </option>
							</select>
					  </div>
				  </div>
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">CP Mode <sup>*</sup></label>
							<select class="form-control" name="onehop_cp_mode" id="onehop_cp_mode">
							<option value='2'>  External Captive Portal  </option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield">
						<input type="text" placeholder="CP URL" name="onehop_cp_url" id="onehop_cp_url">
						   <label>CP URL <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
						  <div class="row">
							  <div class="col-sm-12 col-xs-12">
								  <div class="row">
									  <div class="col-sm-10 col-xs-10 nopadding-right">
										  <div class="mui-textfield mui-textfield--float-label">
											<input type="text" name="wall_garder_ip_enter" id="wall_garder_ip_enter">
											    <span class="chip_text" >Press Button to Add</span>
												<label>Walled Garden IP List <sup>*</sup></label>
										   </div>
									  </div>
									  <div class="col-sm-2 col-xs-2 col-pt-30">
										 <i class="fa fa-plus-square-o fa-2x square-o" aria-hidden="true" id="add_more_wallagrder"></i>
									  </div>
								  </div>
							  </div>
							  <div class="col-sm-12 col-xs-12" id="wall_garder_ip_div">
								 </div>
						  </div>
			     </div>
					  <div class="col-sm-6 col-xs-6">
						  <div class="row">
							  <div class="col-sm-12 col-xs-12">
								 <div class="row">
									  <div class="col-sm-10 col-xs-10 nopadding-right">
										  <div class="mui-textfield mui-textfield--float-label">
											<input type="text" name="wall_garder_domain_enter" id="wall_garder_domain_enter">
											    <span class="chip_text">Press Button to Add</span>
												<label>Walled Garden Domain List <sup>*</sup></label>
										   </div>
									  </div>
									  <div class="col-sm-2 col-xs-2 col-pt-30">
										 <i class="fa fa-plus-square-o fa-2x square-o" aria-hidden="true" id="add_more_wallagrder_domain"></i>
									  </div>
								  </div>  
							  </div>
							  <div class="col-sm-12 col-xs-12" id="wall_garder_domain_div">
								
							  </div>
						  </div>
					  
					  </div>
				  </div>
				  <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield">
						<input type="text"  name="onehop_auth_server_ip" id="onehop_auth_server_ip" readonly="readonly">
							<label>Auth Server IP <sup>*</sup></label>
					   </div>
					  </div>
					  	<div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield ">
						<input type="number"  name="onehop_auth_server_port" id="onehop_auth_server_port" readonly="readonly">
						   <label>Auth Server Port <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
                                  <div class="row">
					  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield ">
						<input type="text"  name="onehop_auth_server_secret" id="onehop_auth_server_secret" readonly="readonly">
							<label>Auth Server Secret <sup>*</sup></label>
					   </div>
					  </div>
					  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Accounting Status <sup>*</sup></label>
							<select class="form-control" name="onehop_accounting" id="onehop_accounting">
							<option value='1'>  Accounting Enabled    </option>
							</select>
					  </div>
				  </div>
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield ">
						<input type="text"  name="onehop_acc_server_ip" id="onehop_acc_server_ip" readonly="readonly">
						   <label>Accouting Server IP <sup>*</sup></label>
					   </div>
					  </div>
				    <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield">
						<input type="number"  name="onehop_acc_server_port" id="onehop_acc_server_port" readonly="readonly">
						   <label>Accouting Server Port <sup>*</sup></label>
					   </div>
					  </div>
					  
				  </div>
				  
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield">
						<input type="text" name="onehop_acc_server_secret" id="onehop_acc_server_secret" readonly="readonly">
						   <label>Accouting Server Secret <sup>*</sup></label>
					   </div>
					  </div>
				    <div class="col-sm-6 col-xs-6">
					  	<div class="mui-textfield">
						<input type="number"  name="onehop_acc_interval" id="onehop_acc_interval" readonly="readonly">
						   <label>Accouting Interval in Seconds <sup>*</sup></label>
					   </div>
					  </div>
					  
				  </div>
				
			  </div>
			  <div class="modal-footer">
                           <p id="add_onehop_ssid_status" style="color:red"> </p>
			 <button type="submit" class="mui-btn mui-btn--os-accent">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
                          </form>
			</div>
		  </div>
		</div>
<!-- One hop add ssid end-->

<!-- One hop add ssid start-->
<div class="modal fade" id="add_onehop_vip_ssid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog"  role="document" style="margin-top:0px;">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?php echo base_url()?>assets/images/close_circle.png"/>
				</button>
				<h4 class="os-modal-title">Configure VIP SSID</h4>
			  </div>
                          <form id="add_location" onsubmit="add_onehop_ssid_vip(); return false;">
			  <div class="modal-body " style="padding:0px 15px 5px 15px">
				
				  <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">SSID Index <sup>*</sup></label>
							<select class="form-control" name="onehop_ssid_index_vip" id="onehop_ssid_index_vip" required>
							<option value='1'>Index 1</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield">
						<input type="hidden" name="onehop_auth_server_ip_vip" id="onehop_auth_server_ip_vip">
						    <input type="hidden" name="onehop_acc_server_ip_vip" id="onehop_acc_server_ip_vip">
						<input type="text" name="onehop_ssid_name_vip" id="onehop_ssid_name_vip" required>
						   <label>SSID Name <sup>*</sup></label>
					   </div>
					  </div>
				  </div>
				   <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Association <sup>*</sup></label>
							<select class="form-control" name="onehop_association_vip" id="onehop_association_vip" required>
							  <option value='1'>PSK</option>
							</select>
					  </div>
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield ">
						<input type="text" name="onehop_psk_vip" id="onehop_psk_vip" required>
						   <label>Pre Shared Key</label>
					   </div>
					  </div>
				  </div>
				   <div class="row">
				  <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">WPA Mode <sup>*</sup></label>
							<select class="form-control" name="onehop_wap_mode_vip" id="onehop_wap_mode_vip">
							<option value='3'> WPA and WPA2 </option>
				 <option value='1'> WPA Only </option>
				 <option value='2'> WPA2 Only </option>
							</select>
					  </div>
					
				  </div>
				 
				
			  </div>
			  <div class="modal-footer">
                           <p id="add_onehop_ssid_status_vip" style="color:red"> </p>
			 <button type="submit" class="mui-btn mui-btn--os-accent">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
                          </form>
			</div>
		  </div>
		</div>
<!-- One hop add ssid end-->

<!-- edit voucer modal -->
<div class="modal modal-container fade"  tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static" id="EditvoucherModal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title">Edit Coupon</h4>
            </center>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="update_location_vouchers" onsubmit="update_location_vouchers(); return false;">
                     <div class="row">
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="hidden" id="voucher_id_edit" name="voucher_id_edit" required>
                              <input type="text" id="voucher_data_edit" name="voucher_data_edit" required>
                              <label>Coupon Value <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                           <div class="mui-select">
                              <select id="voucher_data_type_edit" name="voucher_data_type_edit" required>
                                 <option value="MB">MB</option>
                                 <option id="GB">GB</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="margin-bottom:15px;">
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="text" id="voucher_cost_edit" name="voucher_cost_edit" required>
                              <label>Coupon Cost price <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="text" id="voucher_selling_price_edit" name="voucher_selling_price_edit" required>
                              <label>Coupon Selling Price <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                           <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">SAVE</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- edit ACCESS point modal -->



<!-- One move ap start-->
          <div class="modal fade" id="move_onehop_ap_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<img src="<?php echo base_url()?>assets/images/close_circle.png"/>
				</button>
				<h4 class="os-modal-title">Move AP</h4>
			  </div>
                          <form id="add_location" onsubmit="move_onehop_ap(); return false;">
			  <div class="modal-body" style="padding:10px 15px">
				
		         
				  <div class="row">
					  <div class="col-sm-6 col-xs-6 mui-textfield-os-Status-t">
					  	<div class="mui-textfield">
						   <input type="hidden" name="move_api_router_type" id="move_api_router_type">
						<input type="text" name="onehop_move_macid" id="onehop_move_macid"  readonly="readonly" >
						   <label>AP <sup>*</sup></label>
					   </div>
					  </div>
					   <div class="col-sm-6 col-xs-6">
					  	<label class="so-lable-network">Location<sup>*</sup></label>
							<select class="form-control" name="onehop_move_location_uid" id="onehop_move_location_uid">
							</select>
					  </div>
				  </div>
				  
				  
				
			  </div>
			  <div class="modal-footer">
                           <p id="move_ap_error" style="color:red"> </p>
			 <button type="submit" class="mui-btn mui-btn--os-accent" >MOVE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                         	
			  </div>
                          </form>
			</div>
		  </div>
		</div>

<!-- One move end-->

<!----Onehop mode done start ----->
		
		<div class="modal fade" id="onehop_ap_mode_done" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
			  <div class="modal-header" style="padding:10px 15px">
<!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
				<h4 class="os-modal-title">Configuration!</h4>
			  </div>
			  <div class="modal-body" style="padding:10px 15px">
				<p style="color:#999999; font-style: italic">AP Successfully Moved.</p>
			  </div>
			  <div class="modal-footer" style="text-align: right">
			 <button type="button" class="mui-btn mui-btn--os-accent" data-dismiss="modal" >OK<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
				
			  </div>
			</div>
		  </div>
		</div>
        <!---- Onehop mode done end----->
	
	<!----  Main Section Modal start ----->	
        <div class="modal fade" id="main_category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">New Main Section</h4>
                    </div>
		    <form  id="add_new_main_section_form" enctype="multipart/form-data" onsubmit="add_main_category(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield mui-textfield--float-label">
			      <input type="hidden" name="icon_validation" id="icon_validation" value='1'>
                                <input type="text" name="main_category_name" id="main_category_name" maxlength="30">
                                <label>Section Name<sup>*</sup></label>
                            </div>
			    <span class="text-right pull-right" ><span class="main_category_name_length">0</span><span>/30</span></span>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="main_category_desc" id="main_category_desc" ></textarea>
                                <label>Section Description</label>
                            </div>
			    
			    
			    
			   <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/x-png" style="cursor:pointer;" type="file" class="col-sm-12 icon_upload_class_image" name="main_category_file" id="main_category_file">
				    <label>Upload Icon (Optional)</label>
				 <span><em>Preffered Size 48 * 48 px, PNG or JPG</em></span>     
			      </div>
			   </div>
			   
			    <div class="row" id="promoter_category_div" style="display:none">
				<div class="col-sm-12 col-xs-12">
				    <select class="form-control" name="compliance" id="compliance">
                                        <option value="">Select Compliance</option>
				    </select>
				</div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group os-form-group">
                                        <label class="checkbox-inline">
                                            <input name="promoter_category" value="1" type="checkbox" class="openmodal"> Promoter Category
                                        </label>
                                    </div>
                                </div>
                            </div>
                    </div>
		    <center><p id="create_main_category_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </form>
                </div>
            </div>
        </div>
        <!----  Main Section Modal end ----->
	
	<!----  Sub Section Modal start ----->	
        <div class="modal fade" id="sub_category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">New Sub Section</h4>
                    </div>
		    <form id="add_new_sub_section_form"  enctype="multipart/form-data" onsubmit="add_sub_category(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="sub_category_name" id="sub_category_name" maxlength="30">
                                <label>Section Name<sup>*</sup></label>
                            </div>
			    <span class="text-right pull-right" ><span class="sub_category_name_length">0</span><span>/30</span></span>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="sub_category_desc" id="sub_category_desc" ></textarea>
                                <label>Section Description</label>
                            </div>
			    
			    
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12 icon_upload_class_image" name="sub_category_file" id="sub_category_file">
				    <label>Upload Icon (Optional)</label>
				 <span><em>Preffered Size 48 * 48 px, PNG or JPG</em></span>     
			      </div>
			   </div>
                    </div>
		    <center><p id="create_sub_category_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </form>
                </div>
            </div>
        </div>
        <!----  Sub Section Modal end ----->
	
	<!----  Upload content model start ----->	
        <div class="modal fade" id="upload_content_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Image, Audio Or Video</h4>
                    </div>
		      <form id = "upload_content_modal_form"  enctype="multipart/form-data" onsubmit="add_content(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category" id = "is_main_category">
                                <input type="text" name="content_title" id="content_title">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield">
				 <input style="cursor:pointer;" type="file" class="col-sm-12" name="content_file" id="content_file">
				 <span><em>Images, Videos, Audio content supported</em></span>     
			      </div>
			   </div>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="content_desc" id="content_desc"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error" style="color:red"></p></center>
		    <div class="mui-textfield" style="padding:0px; margin-bottom: 30px">
			<div class="progress progress_overlay hide" >
			      <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
			</div>
		     </div>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	<!----  Upload content model start ----->	
        <div class="modal fade" id="upload_content_modal_pdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Document To View</h4>
                    </div>
		      <form  id="upload_content_modal_pdf_form"  enctype="multipart/form-data" onsubmit="add_content_pdf(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_pdf" id = "is_main_category_pdf">
                                <input type="text" name="content_title_pdf" id="content_title_pdf">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield">
				 <input style="cursor:pointer;" type="file" class="col-sm-12" name="content_file_pdf" id="content_file_pdf" accept="application/pdf">
				 <span><em>Only PDF docs supported</em></span>     
			      </div>
			   </div>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="content_desc_pdf" id="content_desc_pdf"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error_pdf" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Text Content</h4>
                    </div>
		      <form id="upload_content_modal_text_form"  enctype="multipart/form-data" onsubmit="add_content_text(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_text" id = "is_main_category_text">
                                <input type="text" name="content_title_text" id="content_title_text">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			   
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="content_desc_text" id="content_desc_text"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		     <center><p id="create_content_error_text" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">User Upload Photo Or Video</h4>
                    </div>
		      <form id="upload_content_modal_upload_form"  enctype="multipart/form-data" onsubmit="add_content_upload(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_upload" id = "is_main_category_upload">
                                <input type="text" name="content_title_upload" id="content_title_upload">
                                <label> Title<sup>*</sup></label>
                            </div>
			   
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="content_desc_upload" id="content_desc_upload"></textarea>
                                <label>Instructions for the user (Option)</label>
                            </div>
			    
			    <div class="form-group os-form-group" style="margin-top:10px; color:#000">
			      <label class="checkbox-inline" style="padding-left:25px">
			      <input name="upload_content_user_image_allow" value="1" checked="checked" type="checkbox" class="openmodal"> Allow user to upload Photos
			      </label>
			   </div>
			   <div class="form-group os-form-group" style="margin-top:10px; color:#000">
			      <label class="checkbox-inline" style="padding-left:25px">
			      <input name="upload_content_user_video_allow" value="1" type="checkbox" class="openmodal" checked="checked"> Allow user to upload Videos
			      </label>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_upload" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_poll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Poll</h4>
                    </div>
		      <form  id="add_content_poll"  enctype="multipart/form-data" onsubmit="add_content_poll(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_poll" id = "is_main_category_poll">
                                <input type="text" name="content_title_poll" id="content_title_poll">
                                <label>Poll Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="pole_file" id="pole_file">
				    <label>Poll Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 6 Options)</small></h2>
			   </div>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="poll_question" id="poll_question"></textarea>
                                <label>Poll Question<sup>*</sup></label>
                            </div>
			    
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_one" id="poll_option_one">
                                <label>Option A<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_two" id="poll_option_two">
                                <label>Option B<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_three" id="poll_option_three">
                                <label>Option C</label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_four" id="poll_option_four">
                                <label>Option D</label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_five" id="poll_option_five">
                                <label>Option E</label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_six" id="poll_option_six">
                                <label>Option F</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error_poll" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_poll_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit A Poll</h4>
                    </div>
		      <form id="update_content_poll"  enctype="multipart/form-data" onsubmit="update_content_poll(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				  <input type="hidden" name = "edit_content_id_poll" id = "edit_content_id_poll">
				       <input type="hidden" name = "edit_content_id_main_category_poll" id = "edit_content_id_main_category_poll">
				 <input type="hidden" name = "edit_content_id_sub_category_poll" id = "edit_content_id_sub_category_poll">
                    
                                <input type="text" name="content_title_poll_edit" id="content_title_poll_edit">
                                <label>Poll Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="pole_file_edit" id="pole_file_edit">
				    <label>Poll Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="poll_edit_file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="poll_edit_file_previous_name"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="poll_edit_file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div>
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 6 Options)</small></h2>
			   </div>
                            <div class="mui-textfield ">
                                <textarea name="poll_question_edit" id="poll_question_edit"></textarea>
                                <label>Poll Question<sup>*</sup></label>
                            </div>
			    
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_one_edit" id="poll_option_one_edit">
                                <label>Option A<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_two_edit" id="poll_option_two_edit">
                                <label>Option B<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_three_edit" id="poll_option_three_edit">
                                <label>Option C</label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_four_edit" id="poll_option_four_edit">
                                <label>Option D</label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_five_edit" id="poll_option_five_edit">
                                <label>Option E</label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_six_edit" id="poll_option_six_edit">
                                <label>Option F</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error_poll_edit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_survey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Survey</h4>
                    </div>
		      <form  id="add_content_survey"  enctype="multipart/form-data" onsubmit="add_content_survey(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_survey" id = "is_main_category_survey">
                                <input type="text" name="content_title_survey" id="content_title_survey">
                                <label>Survey Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="survey_file" id="survey_file">
				    <label>Survey Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			   
                           <div id="survey_question_main_div">
			      <input type="hidden" name="total_survey_question_created" id="total_survey_question_created" value = "1">
			      <div id="survey_question_div">
				 <div class="survey_div">
				    <div class="form-group">
				       <label style="color:#f00f64">QUESTION 01</label>
				       <select class="form-control survey_type_class" name="survey_type[]">
					  <option value="1">Text Entry</option>
					  <option value="2">Single Choice</option>
					  <option value="3">Multi Coice</option>
				       </select>
				    </div>
				    <div class="mui-textfield mui-textfield--float-label">
				       <textarea name="survey_question[]" class = "survey_question_class"></textarea>
				       <label>Question<sup>*</sup></label>
				    </div>
				    
				 </div>
				 
			      </div>
			      <a class = "add_new_question" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Question</a>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_survey" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_survey_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit A Survey</h4>
                    </div>
		      <form id="update_content_survey"  enctype="multipart/form-data" onsubmit="update_content_survey(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				  <input type="hidden" name = "edit_content_id_survey" id = "edit_content_id_survey">
				       <input type="hidden" name = "edit_content_id_main_category_survey" id = "edit_content_id_main_category_survey">
				 <input type="hidden" name = "edit_content_id_sub_category_survey" id = "edit_content_id_sub_category_survey">
                    
                                <input type="text" name="content_title_survey_edit" id="content_title_survey_edit">
                                <label>Survey Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="survey_file_edit" id="survey_file_edit">
				    <label>Survey Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:5px" id="survey_edit_file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="survey_edit_file_previous_name"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="survey_edit_file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div>
                           <div id="survey_question_main_div_edit">
			      <input type="hidden" name="total_survey_question_created_edit" id="total_survey_question_created_edit" value = "1">
			      <div id="survey_question_div_edit">
				 
				 
			      </div>
			      <a class = "add_new_question_edit" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Question</a>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_survey_edit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Main Section Modal start ----->	
        <div class="modal fade" id="main_category_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Main Section</h4>
                    </div>
		    <form id="edit_new_main_section_form"  enctype="multipart/form-data" onsubmit="update_main_category(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield">
				 <input type="hidden" name="main_category_id_edit" id="main_category_id_edit">
                                <input type="text" name="main_category_name_edit" id="main_category_name_edit" maxlength="30">
                                <label>Section Name<sup>*</sup></label>
                            </div>
			    <span class="text-right pull-right" ><span class="main_category_name_length">0</span><span>/30</span></span>
                            <div class="mui-textfield">
                                <textarea name="main_category_desc_edit" id="main_category_desc_edit" ></textarea>
                                <label>Section Description</label>
                            </div>
			    
			    
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12 icon_upload_class_image" name="main_category_file_edit" id="main_category_file_edit">
				    <label>Upload Icon (Optional)</label>
				 <span><em>Preffered Size 48 * 48 px, PNG or JPG</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="maine_category_edit_file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="main_category_edit_file_previous_name"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="main_category_edit_file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div>
			     <div class="row" id="promoter_category_div_edit" style="display:none">
				<div class="col-sm-12 col-xs-12">
				    <select class="form-control" name="compliance_edit" id="compliance_edit">
                                        <option value="">Select Compliance</option>
				    </select>
				</div>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group os-form-group">
                                        <label class="checkbox-inline">
                                            <input name="promoter_category_edit" value="1" type="checkbox" class="openmodal"> Promoter Category
                                        </label>
                                    </div>
                                </div>
                            </div>
                    </div>
		    <center><p id="update_main_category_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </form>
                </div>
            </div>
        </div>
        <!----  Main Section Modal end ----->
	<!----  Sub Section Modal start ----->	
        <div class="modal fade" id="sub_category_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Sub Section</h4>
                    </div>
		    <form id="edit_new_sub_section_form"  enctype="multipart/form-data" onsubmit="update_sub_category(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield ">
			      <input type="hidden" name="sub_category_id_edit_main_category" id="sub_category_id_edit_main_category">
			      <input type="hidden" name="sub_category_id_edit" id="sub_category_id_edit">
                                <input type="text" name="sub_category_name_edit" id="sub_category_name_edit" maxlength="30">
                                <label>Section Name<sup>*</sup></label>
                            </div>
			    <span class="text-right pull-right" ><span class="sub_category_name_length">0</span><span>/30</span></span>
                            <div class="mui-textfield ">
                                <textarea name="sub_category_desc_edit" id="sub_category_desc_edit" ></textarea>
                                <label>Section Description</label>
                            </div>
			    
			    
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12 icon_upload_class_image" name="sub_category_file_edit" id="sub_category_file_edit">
				    <label>Upload Icon (Optional)</label>
				 <span><em>Preffered Size 48 * 48 px, PNG or JPG</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="sub_category_edit_file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="sub_category_edit_file_previous_name"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="sub_category_edit_file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div>
			    
                    </div>
		    <center><p id="update_sub_category_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </form>
                </div>
            </div>
        </div>
        <!----  Sub Section Modal end ----->
	<!----  Upload content model start ----->	
        <div class="modal fade" id="upload_content_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Image, Audio Or Video</h4>
                    </div>
		      <form id="edit_image_video_content_form"  enctype="multipart/form-data" onsubmit="update_content(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id" id = "edit_content_id">
				       <input type="hidden" name = "edit_content_id_main_category" id = "edit_content_id_main_category">
				 <input type="hidden" name = "edit_content_id_sub_category" id = "edit_content_id_sub_category">
                                <input type="text" name="content_title_edit" id="content_title_edit">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:10px">
			      <div class="mui-textfield">
				<input style="cursor:pointer;" type="file" class="col-sm-12" name="content_file_edit" id="content_file_edit">
				 <span><em>Images, Videos, Audio content supported</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-9 col-xs-9">    
                                    <label  id="file_previous_name"></label>
                                 </div>
                                    
                                 <!--div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div-->
                                    
                              </div>
                              
                           </div>
                            <div class="mui-textfield ">
                                <textarea name="content_desc_edit" id="content_desc_edit"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <p id="edit_content_error" style="color:red"></p>
		    <div class="mui-textfield" style="padding:0px; margin-bottom: 30px">
			<div class="progress progress_overlay hide" >
			      <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
			</div>
		     </div>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	<!----  Upload content model start ----->	
        <div class="modal fade" id="upload_content_modal_edit_pdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Document To View</h4>
                    </div>
		      <form id="edit_pdf_content_form" enctype="multipart/form-data" onsubmit="update_content_pdf(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id_pdf" id = "edit_content_id_pdf">
				       <input type="hidden" name = "edit_content_id_main_category_pdf" id = "edit_content_id_main_category_pdf">
				 <input type="hidden" name = "edit_content_id_sub_category_pdf" id = "edit_content_id_sub_category_pdf">
                                <input type="text" name="content_title_edit_pdf" id="content_title_edit_pdf">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:10px">
			      <div class="mui-textfield">
				<input accept="application/pdf"  style="cursor:pointer;" type="file" class="col-sm-12" name="content_file_edit_pdf" id="content_file_edit_pdf">
				 <span><em>Only PDF docs supported</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="file_previous_name_div_pdf">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-9 col-xs-9">    
                                    <label  id="file_previous_name_pdf"></label>
                                 </div>
                                    
                                 <!--div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div-->
                                    
                              </div>
                              
                           </div>
                            <div class="mui-textfield ">
                                <textarea name="content_desc_edit_pdf" id="content_desc_edit_pdf"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <p id="edit_content_error_pdf" style="color:red"></p>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	<!----  Upload content model start ----->
	 <div class="modal fade" id="upload_content_modal_edit_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Text Content</h4>
                    </div>
		      <form  enctype="multipart/form-data" onsubmit="update_content_text(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id_text" id = "edit_content_id_text">
				       <input type="hidden" name = "edit_content_id_main_category_text" id = "edit_content_id_main_category_text">
				 <input type="hidden" name = "edit_content_id_sub_category_text" id = "edit_content_id_sub_category_text">
                                <input type="text" name="content_title_edit_text" id="content_title_edit_text">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			   
                            <div class="mui-textfield ">
                                <textarea name="content_desc_edit_text" id="content_desc_edit_text"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		     <center><p id="edit_content_error_text" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>

        <!---- Upload content model end ----->
	<!----  Upload content type model start ----->	
        <div class="modal fade" id="choose_content_type_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Select Content Type To Add</h4>
                    </div>
		      <div class="modal-body" style="padding:5px 15px 10px">
                       
			   <div class="row"> 
			   <div class="col-sm-12 not_retail_audit_content_type text_only_radio" style="margin:5px 0px;">
			      <input type="hidden" name = "choose_content_type_is_main_category" id = "choose_content_type_is_main_category">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="text" checked="checked"> Text Only
			      </label> 
			   </div>
			   
			   <div class="col-sm-12 not_retail_audit_content_type image_video_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="image_audio_video"> Image, Audio or Video With Text
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type document_view_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="pdf"> Document Viewer (PDF Only)
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type upload_photo_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="upload"> Upload Photo or Video by user
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type poll_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="poll"> Create A Poll Question (Single Question)
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type survey_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="survey"> Create A Survey (Multiple Question)
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type wiki_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="wiki"> Wikipedia
			      </label> 
			   </div>
			    <div class="col-sm-12 retail_audit_content_type" style="margin:5px 0px;">
				<label class="radio-inline" style="font-weight:600;color:#808080">
				   <input type="radio" name="content_type_radio"  value="survey_retail_audit"> Add Audit Survey
				</label> 
			    </div>
			</div>
                       
                    </div>
		    
                    <div class="modal-footer" style="text-align: right; padding-top:5px;">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="button" class="mui-btn mui-btn--os-accent" onclick="choose_content_type_next()">NEXT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	
	<!----  Upload content model start ----->
	 <div class="modal fade" id="upload_content_modal_edit_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit User Upload Photo Or video</h4>
                    </div>
		      <form  enctype="multipart/form-data" onsubmit="update_content_upload(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id_upload" id = "edit_content_id_upload">
				       <input type="hidden" name = "edit_content_id_main_category_upload" id = "edit_content_id_main_category_upload">
				 <input type="hidden" name = "edit_content_id_sub_category_upload" id = "edit_content_id_sub_category_upload">
                                <input type="text" name="content_title_edit_upload" id="content_title_edit_upload">
                                <label> Title<sup>*</sup></label>
                            </div>
			   
                            <div class="mui-textfield ">
                                <textarea name="content_desc_edit_upload" id="content_desc_edit_upload"></textarea>
                                <label>Instruction for the user (Option)</label>
                            </div>
			    
			    <div class="form-group os-form-group" style="margin-top:10px; color:#000">
			      <label class="checkbox-inline" style="padding-left:25px">
			      <input name="upload_content_user_image_allow_edit" value="1"  type="checkbox" class="openmodal"> Allow user to upload Photos
			      </label>
			   </div>
			   <div class="form-group os-form-group" style="margin-top:10px; color:#000">
			      <label class="checkbox-inline" style="padding-left:25px">
			      <input name="upload_content_user_video_allow_edit" value="1" type="checkbox" class="openmodal" > Allow user to upload Videos
			      </label>
			   </div>
                       
                    </div>
		     <center><p id="edit_content_error_upload" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>

        <!---- Upload content model end ----->
	
	
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="ask_onehop_network_type_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create Network</h4>
                    </div>
		       <div class="modal-body" style="padding:10px 15px">
			   <div class="col-sm-12 col-xs-12">
							    <div class="form-group">
							       <div class="row">
								  
								  <div class="col-sm-12 col-xs-12">
								     <label class="radio-inline" style="margin-right:10px">
									<input name="is_create_new_network" value="1"  type="radio" > Use Existing
								     </label>
								     <label class="radio-inline" style="margin-right:10px">
									<input name="is_create_new_network" value="2" type="radio" checked> Create New
								     </label>
								  </div>
							       </div>
							    </div>
			      </div>
		     </div>
		    <center><p id="create_content_error_text" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="button" class="mui-btn mui-btn--os-accent" onclick="ask_onehop_network_type_next()">NEXT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	
	
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="select_previous_network_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Select  Network</h4>
                    </div>
		       <div class="modal-body" style="padding:10px 15px">
			   <div class="col-sm-12 col-xs-12">
							    <div class="form-group">
							       <div class="row">
								  
								  <div class="col-sm-12 col-xs-12">
								     <select class="form-control" name="previous_network" id="previous_network">
                                                                     
                                                                  </select>
								  </div>
							       </div>
							    </div>
			      </div>
		     </div>
		    
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="button" class="mui-btn mui-btn--os-accent" onclick="select_previous_network()">DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_wiki" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Wikipedia Content</h4>
                    </div>
		      <form id="upload_content_modal_wiki_form"  enctype="multipart/form-data" onsubmit="add_content_wiki(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_wiki" id = "is_main_category_wiki">
                                <input type="text" name="content_title_wiki" id="content_title_wiki">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			   <div class="mui-textfield mui-textfield--float-label">
				  <input type="text" name="wiki_title_url" id="wiki_title_url">
                                <label>Wikipedia Title/URL<sup>*</sup></label>
                            </div>
                            
                       
                    </div>
		    <center><p id="create_content_error_wiki" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	
	
	<!----  Upload content model start ----->
	 <div class="modal fade" id="upload_content_modal_edit_wiki" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Wikipedia Content</h4>
                    </div>
		      <form  enctype="multipart/form-data" onsubmit="update_content_wiki(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id_wiki" id = "edit_content_id_wiki">
				       <input type="hidden" name = "edit_content_id_main_category_wiki" id = "edit_content_id_main_category_wiki">
				 <input type="hidden" name = "edit_content_id_sub_category_wiki" id = "edit_content_id_sub_category_wiki">
                                <input type="text" name="content_title_edit_wiki" id="content_title_edit_wiki">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield ">
				  <input type="text" name="wiki_title_url_edit" id="wiki_title_url_edit">
                                <label>Wikipedia Title/URL<sup>*</sup></label>
                            </div>
			   
                            
                       
                    </div>
		     <center><p id="edit_content_error_wiki" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>

        <!---- Upload content model end ----->
	
	<!----  Raspberry modal start ----->
	 <div class="modal fade" id="raspberrymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" style="padding-left:0px">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header" style="padding:10px 15px">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <img src="<?php echo base_url()?>assets/images/close_circle.png"/>
                  </button>
                  <h4>Raspberry Logs</h4>
               </div>
               <div class="modal-body modal-body2">
                  <div class="row">
					  <div class="col-sm-12 col-xs-12">
						  <ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#home">Ping Status</a>
							</li>
							<li>
								<a data-toggle="tab" href="#menu1">Sync Status</a>
							</li>
						  </ul>
					  </div>
					  <div class="col-sm-12 col-xs-12">
					   
						  <div class="tab-content">
							<div id="home" class="tab-pane fade in active">
							   <div class="col-sm-12" style="margin: 10px 0px">
                        		<div class="table-responsive">
                           <table class="table table-striped">
							<tbody class="pinglogs"></tbody>
                             
                           </table>
                        </div>
                     			</div>
							</div>
							<div id="menu1" class="tab-pane fade">
								<div class="col-sm-12" style="margin: 10px 0px">
									<div class="table-responsive">
							   <table class="table table-striped ">
								<tbody class="synclogs"></tbody>
							   </table>
							</div>
								</div>
							</div>
						   </div>
					  </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
		
		

        <!----  Raspberry modal ends ----->
	
	
	
	<!----  Upload contestifi_contest model start ----->	
        <div class="modal fade" id="add_contestifi_quiz_qustion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Quiz</h4>
                    </div>
		      <form  id="add_contenstifi_quiz_question"  enctype="multipart/form-data" onsubmit="add_contestifi_question(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   <div id="contestifi_contest_image_contest_div" style="margin-bottom:15px" class="mui-form--inline">
			      <div class="mui-textfield">
				 <input type="file" id="contestifi_content_file" name="contestifi_content_file" class="col-sm-12 mui--is-empty mui--is-untouched mui--is-pristine" style="cursor:pointer;">
				 <span><em>Images, Videos, Audio content supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 4 Options)</small></h2>
			   </div>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="quiz_question" id="quiz_question"></textarea>
                                <label>Quiz Question<sup>*</sup></label>
                            </div>
			    
			   <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="quiz_option_one" id="quiz_option_one">
				       <label>Option A<sup>*</sup></label>
				   </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans" value="1" type="radio" checked> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="quiz_option_two" id="quiz_option_two">
                                <label>Option B<sup>*</sup></label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans" value="2" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				   <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="quiz_option_three" id="quiz_option_three">
                                <label>Option C</label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans" value="3" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				   <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="quiz_option_four" id="quiz_option_four">
                                <label>Option D</label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans" value="4" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    
			    
			    
			   
                       
                    </div>
		    <center><p id="create_contestifi_quiz_error" style="color:red"></p></center>
		    <div class="mui-textfield" style="padding:0px; margin-bottom: 30px">
			<div class="progress progress_overlay hide" >
			      <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
			</div>
		     </div>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest model end ----->
	
	<!----  Upload contestifi_contest model start ----->	
        <div class="modal fade" id="update_contestifi_quiz_qustion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Update A Quiz</h4>
                    </div>
		      <form  id="update_contenstifi_quiz_question"  enctype="multipart/form-data" onsubmit="update_contestifi_question(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   <div id="contestifi_contest_image_contest_div_edit" style="margin-bottom:15px" class="mui-form--inline">
			      <div class="mui-textfield">
				 <input type="file" id="contestifi_content_file_edit" name="contestifi_content_file_edit" class="col-sm-12 mui--is-empty mui--is-untouched mui--is-pristine" style="cursor:pointer;">
				 <span><em>Images, Videos, Audio content supported</em></span>     
			      </div>
			   </div>
			   <div class="row" id="contestifi_content_file_edit_preview_div">
			      <div class="col-sm-8 col-xs-8">
				    <label>Previous File:- <span id="contestifi_content_file_edit_preview"></span></label>
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 4 Options)</small></h2>
			   </div>
                            <div class="mui-textfield ">
			      <input type="hidden" name="edit_contestifi_quiz_question_id" id="edit_contestifi_quiz_question_id">
                                <textarea name="quiz_question_edit" id="quiz_question_edit"></textarea>
                                <label>Quiz Question<sup>*</sup></label>
                            </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield ">
				    <input type="text" name="quiz_option_one_edit" id="quiz_option_one_edit">
				    <label>Option A<sup>*</sup></label>
				</div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans_edit" value="1" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield">
                                <input type="text" name="quiz_option_two_edit" id="quiz_option_two_edit">
                                <label>Option B<sup>*</sup></label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans_edit" value="2" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield">
                                <input type="text" name="quiz_option_three_edit" id="quiz_option_three_edit">
                                <label>Option C</label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans_edit" value="3" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield">
                                <input type="text" name="quiz_option_four_edit" id="quiz_option_four_edit">
                                <label>Option D</label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans_edit" value="4" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    
			    
			    
			   
                       
                    </div>
		    <center><p id="update_contestifi_quiz_error" style="color:red"></p></center>
		      <div class="mui-textfield" style="padding:0px; margin-bottom: 30px">
			<div class="progress progress_overlay hide" >
			      <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
			</div>
		     </div>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest model end ----->
	
	<!----  contestifi contest section start ----->	
        <div class="modal fade" id="contestifi_contest_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">New Contest</h4>
                    </div>
		    <form  id="add_new_contestifi_contest" enctype="multipart/form-data" onsubmit="add_contestifi_contest(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   <div class="col-sm-12 col-xs-12">
			      <div class="mui-select">
				 <select onchange="change_contest_type_drowdown()" name="contestifi_contest_type" id="contestifi_contest_type">
				    <option value="1">Time bound Quiz</option>
				    <option value="2">Image & Video Contest</option>
				    <option value="3">Survey</option>
				 </select>
				 <label>Contest type<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="text" name="contestifi_contest_name" id="contestifi_contest_name">
				  <label>Contest Name<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="col-sm-6 col-xs-6">
							       <label class="os-lable">Contest Logo</label>
							       <div class="mui-textfield" style="padding-top:5px;">
								  <div class=" modal-dropzone dropzone" data-width="900" data-ajax="false"  data-originalsize="false" data-height="300" style="width: 225px; height:75px;">
                                                                     <input type="file" name="contestifi_logo_image"  />
                                                                  </div>
							       </div>
				 </div>
				 
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 
				 <!--div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="color" style="border: none" name="contestifi_contest_bg_color" id="contestifi_contest_bg_color" value="#ffffff">
				       <label>BG Color <sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="color" style="border: none" name="contestifi_contest_text_color" id="contestifi_contest_text_color" value="#000000">
				       <label>Text Color <sup>*</sup></label>
				    </div>
				 </div-->
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="color" style="border: none" name="contestifi_contest_button_color" id="contestifi_contest_button_color" value="#007cb2">
				       <label>Button Color <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="col-sm-6 col-xs-6">
				    <div class="mui-textfield ">
				       <input type="text" class="start_date" name="start_date" >
				       <label>Start Date<sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-sm-6 col-xs-6">
				    <div class="mui-textfield">
				       <input type="text" class="end_date"  name="end_date">
				       <label>End Date <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div class="row only_time_based_quiz">
			      <div class="col-sm-12 col-xs-12">
				 <div class="form-group">
				    <div class="col-sm-12 col-xs-12"><label class="os-lable" style="font-weight:700">Contest Frequency</label></div>
				    <div class="col-sm-12 col-xs-12">
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency" value="1" type="radio" checked> Daily
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency" value="2" type="radio"> Every 2 hours
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency" value="3" type="radio"> Every 4 Hours
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency" value="4" type="radio"> Custom
					   </label>
				    </div>	       
				 </div>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12" style="display:none" id="contestifi_custom_frequecny_div">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_custom_frequecny" id="contestifi_custom_frequecny">
				  <label>Custom Frequency(In hr)<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_no_of_attempt">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_no_of_attemption" id="contestifi_no_of_attemption">
				  <label>Number of attempts<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_point_per_question" id="contestifi_point_per_question">
				  <label>Point Per Question<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_negative_point" id="contestifi_negative_point" value="0">
				  <label>Negative Point<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_point_deduct_every_secod" id="contestifi_point_deduct_every_secod" value="0">
				  <label>Points deducted for every second<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_max_time_allocation" id="contestifi_max_time_allocation" value="0">
				  <label>Maximum time allocation (In second)<sup>*</sup></label>
			      </div>
			   </div>
			   <div id="contestifi_contest_rule_div">
			      <div class="col-sm-12 col-xs-12">
				 <label class="os-lable" style="font-weight:700">Contest Rules</label>
			      </div>
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="contestifi_contet_rule[]">
				       <label>Rule<sup>*</sup></label>
				    </div>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
				 <a class = "contestifi_contest_add_another_rule" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Rule</a>
			   </div>
		    <center><p id="create_contestifi_contest_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </div>
		    </form>
                </div>
            </div>
        </div>
        <!---- contestifi contest section  end ----->
	
	
	
	<!----  contestifi contest update section start ----->	
        <div class="modal fade" id="contestifi_contest_update_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Update Contest</h4>
                    </div>
		    <form  id="update_contestifi_contest" enctype="multipart/form-data" onsubmit="update_contestifi_contest(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   <div class="col-sm-12 col-xs-12">
			      <div class="mui-select">
				 <input type="hidden" name="contestifi_contest_id_edit" id="contestifi_contest_id_edit">
				 <select name="contestifi_contest_type_edit" id="contestifi_contest_type_edit">
				    
				 </select>
				 <label>Contest type<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12">
			      <div class="mui-textfield ">
				  <input type="text" name="contestifi_contest_name_edit" id="contestifi_contest_name_edit">
				  <label>Contest Name<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="col-sm-6 col-xs-6">
							       <label class="os-lable">Contest Logo</label>
							       <div class="mui-textfield" style="padding-top:5px;">
								  <div class="modal-dropzone dropzone" data-width="900" data-ajax="false"  data-originalsize="false" data-height="300" style="width: 225px; height:75px;">
                                                                     <input type="file" name="contestifi_logo_image_edit"  />
                                                                  </div>
							       </div>
				 </div>
				 <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <label class="os-lable">&nbsp;</label>
                                       <div class="mui-textfield" style="padding-top:5px;width: 225px; height:75px; overflow: hidden;">
                                          <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="contest_logo_preview" class="profile-user-img img-responsive" src="" style =" width:100%;">
                                       </div>
                                 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <!--div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield">
				       <input type="color" style="border: none" name="contestifi_contest_bg_color_edit" id="contestifi_contest_bg_color_edit">
				       <label>BG Color <sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield">
				       <input type="color" style="border: none" name="contestifi_contest_text_color_edit" id="contestifi_contest_text_color_edit">
				       <label>Text Color <sup>*</sup></label>
				    </div>
				 </div-->
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield">
				       <input type="color" style="border: none" name="contestifi_contest_button_color_edit" id="contestifi_contest_button_color_edit">
				       <label>Button Color <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="col-sm-6 col-xs-6">
				    <div class="mui-textfield ">
				       <input type="text" class="start_date_edit" name="start_date_edit" >
				       <label>Start Date<sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-sm-6 col-xs-6">
				    <div class="mui-textfield">
				       <input type="text" class="end_date_edit"  name="end_date_edit">
				       <label>End Date <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div class="row only_time_based_quiz_edit">
			      <div class="col-sm-12 col-xs-12">
				 <div class="form-group">
				    <div class="col-sm-12 col-xs-12"><label class="os-lable" style="font-weight:700">Contest Frequency</label></div>
				    <div class="col-sm-12 col-xs-12">
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency_edit" value="1" type="radio"> Daily
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency_edit" value="2" type="radio"> Every 2 hours
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency_edit" value="3" type="radio"> Every 4 Hours
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency_edit" value="4" type="radio"> Custom
					   </label>
				    </div>	       
				 </div>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12" style="display:none" id="contestifi_custom_frequecny_div_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_custom_frequecny" id="contestifi_custom_frequecny_edit">
				  <label>Custom Frequency(In hr)<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit_no_of_attempt">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_no_of_attemption_edit" id="contestifi_no_of_attemption_edit">
				  <label>Number of attempts<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_point_per_question_edit" id="contestifi_point_per_question_edit">
				  <label>Point Per Question<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_negative_point_edit" id="contestifi_negative_point_edit" value="0">
				  <label>Negative Point<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_point_deduct_every_secod_edit" id="contestifi_point_deduct_every_secod_edit" value="0">
				  <label>Points deducted for every second<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_max_time_allocation_edit" id="contestifi_max_time_allocation_edit" value="0">
				  <label>Maximum time allocation (In second)<sup>*</sup></label>
			      </div>
			   </div>
			   <div id="contestifi_contest_rule_div_edit">
			      <div class="col-sm-12 col-xs-12">
				 <label class="os-lable" style="font-weight:700">Contest Rules</label>
			      </div>
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-textfield">
				       <input type="text" class="cotenst_first_rule_edit" name="contestifi_contet_rule_edit[]">
				       <label>Rule<sup>*</sup></label>
				    </div>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
				 <a class = "contestifi_contest_add_another_rule_edit" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Rule</a>
			   </div>
		    <center><p id="create_contestifi_contest_error_edit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </div>
		    </form>
                </div>
            </div>
        </div>
        <!---- contestifi contest update section  end ----->
	
	
	<!----  Upload contestifi_contest prize model start ----->	
        <div class="modal fade" id="add_contestifi_contest_prize_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" >
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Prize</h4>
                    </div>
		      <form  id="add_contenstifi_contest_prize"  enctype="multipart/form-data" onsubmit="add_contestifi_prize(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       			    
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="contestifi_contest_prize" id="contestifi_contest_prize">
                                <label>Contest Prize<sup>*</sup></label>
                            </div>
			    
                    </div>
		    <center><p id="create_contestifi_prize_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest prize model end ----->
	
	<!----  Upload contestifi_contest prize model start ----->	
        <div class="modal fade" id="add_contestifi_contest_prize_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" >
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Update Prize</h4>
                    </div>
		      <form  id="add_contenstifi_contest_prize_edit"  enctype="multipart/form-data" onsubmit="update_contestifi_prize(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       			    
			    <div class="mui-textfield">
			      <input type="hidden" name="contestifi_contest_prize_id_edit" id="contestifi_contest_prize_id_edit">
                                <input type="text" name="contestifi_contest_prize_edit" id="contestifi_contest_prize_edit">
                                <label>Contest Prize<sup>*</sup></label>
                            </div>
			    
                    </div>
		    <center><p id="create_contestifi_prize_error_edit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest prize model end ----->
      
      <!----  Upload contestifi_contest prize model start ----->	
        <div class="modal fade" id="add_contestifi_contest_terms_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog " style="margin-top:5%" role="document" >
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Sign up Term & Condition</h4>
                    </div>
		      <form  id="add_contenstifi_contest_terms_condition"  enctype="multipart/form-data" onsubmit="add_contestifi_terms_conditon(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       			    
			    <div class="mui-textfield">
			        <textarea id="contestifi_term_condition" name="contestifi_term_condition"></textarea>
                                <label>Term Condition<sup>*</sup></label>
                            </div>
			    
                    </div>
		    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest prize model end ----->
	
	
	<!----  Upload contestifi_contest model start ----->	
        <div class="modal fade" id="add_contestifi_survey_qustion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Survey</h4>
                    </div>
		      <form  id="add_contenstifi_survey_question"  enctype="multipart/form-data" onsubmit="add_contestifi_survey(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 4 Options)</small></h2>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-select">
				    <select name="contestifi_contest_survey_type" id="contestifi_contest_survey_type">
				       <option value="1">Single Choice</option>
				       <option value="2">Multi Choice</option>
				    </select>
				    <label>Answer type<sup>*</sup></label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-textfield mui-textfield--float-label">
				     <textarea name="survey_question" id="survey_question"></textarea>
				     <label>Survey Question<sup>*</sup></label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="survey_option_one" id="survey_option_one">
				       <label>Option A<sup>*</sup></label>
				   </div>
				 </div>
			      
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="survey_option_two" id="survey_option_two">
                                <label>Option B<sup>*</sup></label>
                            </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				   <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="survey_option_three" id="survey_option_three">
                                <label>Option C</label>
                            </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				   <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="survey_option_four" id="survey_option_four">
                                <label>Option D</label>
                            </div>
				 </div>
				 
			      
			   </div>
			    
			    
			    
			    
			   
                       
                    </div>
		    <center><p id="create_contestifi_survey_error" style="color:red"></p></center>
		    
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest model end ----->
	
	
	
	<!----  Upload contestifi_contest model start ----->	
        <div class="modal fade" id="update_contestifi_survey_qustion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Survey</h4>
                    </div>
		      <form  id="update_contenstifi_survey_question"  enctype="multipart/form-data" onsubmit="update_contestifi_survey(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 4 Options)</small></h2>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-select">
				    <select name="contestifi_contest_survey_type_edit" id="contestifi_contest_survey_type_edit">
				       <option value="1">Single Choice</option>
				       <option value="2">Multi Choice</option>
				    </select>
				    <label>Answer type<sup>*</sup></label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-textfield ">
				    <input type="hidden" name="contestifi_survey_question_id_hidden" id="contestifi_survey_question_id_hidden">
				     <textarea name="survey_question_edit" id="survey_question_edit"></textarea>
				     <label>Survey Question<sup>*</sup></label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				    <div class="mui-textfield">
				       <input type="text" name="survey_option_one_edit" id="survey_option_one_edit">
				       <label>Option A<sup>*</sup></label>
				   </div>
				 </div>
			      
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				    <div class="mui-textfield">
                                <input type="text" name="survey_option_two_edit" id="survey_option_two_edit">
                                <label>Option B<sup>*</sup></label>
                            </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				   <div class="mui-textfield ">
                                <input type="text" name="survey_option_three_edit" id="survey_option_three_edit">
                                <label>Option C</label>
                            </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				   <div class="mui-textfield ">
                                <input type="text" name="survey_option_four_edit" id="survey_option_four_edit">
                                <label>Option D</label>
                            </div>
				 </div>
				 
			      
			   </div>
			    
			    
			    
			    
			   
                       
                    </div>
		    <center><p id="create_contestifi_survey_error_edit" style="color:red"></p></center>
		    
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest model end ----->
	
	
	
	<!----  Upload audit survey start ----->	
        <div class="modal fade" id="upload_content_modal_survey_retail_audit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Survey</h4>
                    </div>
		      <form  id="add_content_survey_retail_audit"  enctype="multipart/form-data" onsubmit="add_content_survey_retail_audit(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_survey_retail_audit" id = "is_main_category_survey_retail_audit">
                                <input type="text" name="content_title_survey_retail_audit" id="content_title_survey_retail_audit">
                                <label>Add Audit Element Title <sup>*</sup></label>
                            </div>
			    <!--div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="survey_file_retail_audit" id="survey_file_retail_audit">
				    <label>Survey Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div-->
			   
                           <div id="survey_question_main_div_retail_audit">
			      <input type="hidden" name="total_survey_question_created_retail_audit" id="total_survey_question_created_retail_audit" value = "1">
			      <div id="survey_question_div_retail_audit">
				 <div class="survey_div_retail_audit">
				    <div class="form-group">
				       <label style="color:#f00f64">QUESTION 01</label>
				       <label style="padding-left:20%;">Deduct: <input type="text"  class= "health_deduct_retail_audit" name="health_deduct_retail_audit[]" style="width:50px;"> % from store health</label>
				       <!--select class="form-control survey_type_class_retail_audit" name="survey_type_retail_audit[]">
					  <option value="0">Select Survey Type</option>
					  <option value="2">Single Choice</option>
					  <option value="3">Multi Coice</option>
				       </select-->
				    </div>
				    <div class="mui-textfield mui-textfield--float-label">
				       <textarea name="survey_question_retail_audit[]" class = "survey_question_class_retail_audit"></textarea>
				       <label>Question<sup>*</sup></label>
				    </div>
				    <div class="form-group">
					<div class="checkbox" style="margin-top:0px">
					    <label>
					    <input  class= "use_sku_survey_retail_audit" name="use_sku_survey_retail_audit[]" value="1" type="checkbox"> User SKU List
					    </label>
				       </div>
				    </div>
				 </div>
				 
			      </div>
			      <a class = "add_new_question_retail_audit" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Question</a>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_survey_retail_audit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload audit survey end ----->
	
	<!----  edit audit survey  start ----->	
        <div class="modal fade" id="upload_content_modal_survey_edit_retail_audit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit A Survey</h4>
                    </div>
		      <form id="update_content_survey_retail_audit"  enctype="multipart/form-data" onsubmit="update_content_survey_retail_audit(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				  <input type="hidden" name = "edit_content_id_survey_retail_audit" id = "edit_content_id_survey_retail_audit">
				       <input type="hidden" name = "edit_content_id_main_category_survey_retail_audit" id = "edit_content_id_main_category_survey_retail_audit">
				 <input type="hidden" name = "edit_content_id_sub_category_survey_retail_audit" id = "edit_content_id_sub_category_survey_retail_audit">
                    
                                <input type="text" name="content_title_survey_edit_retail_audit" id="content_title_survey_edit_retail_audit">
                                <label>Add Audit Element Title<sup>*</sup></label>
                            </div>
			    <!--div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="survey_file_edit_retail_audit" id="survey_file_edit_retail_audit">
				    <label>Survey Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:5px" id="survey_edit_file_previous_name_div_retail_audit">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="survey_edit_file_previous_name_retail_audit"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="survey_edit_file_previous_name_remove_button_retail_audit">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div-->
                           <div id="survey_question_main_div_edit_retail_audit">
			      <input type="hidden" name="total_survey_question_created_edit_retail_audit" id="total_survey_question_created_edit_retail_audit" value = "1">
			      <div id="survey_question_div_edit_retail_audit">
				 
				 
			      </div>
			      <a class = "add_new_question_edit_retail_audit" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Question</a>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_survey_edit_retail_audit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- edit audit survey end ----->
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/html5imageupload.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/ckeditor/ckeditor.js"></script>
      <script>
         CKEDITOR.replace( 'editor1' );
	 CKEDITOR.replace( 'contestifi_term_condition' );
      </script>
      <script>
         $('.dropzone').html5imageupload();
         $(document).ready(function() {
	    
	    var location_type = $("#location_type").val();
	    if(location_type == '16'){
	       $("#content_builder_tab_li").hide();
		$("#contestifi_content_builder_tab_li").hide();
		$("#network_tab").hide();
		$("#content_builder_tab_coupon").hide();
		$("#content_builder_tab_termconditon").hide();
		$("#content_builder_tab_sms").hide();
	    }
	    if (location_type == '8'){
	       $("#content_builder_tab_sms").hide();
	    }
	    if (location_type == '10' || location_type == '11' || location_type == '12' || location_type == '13' || location_type == '14' || location_type == '18'){
	       $("#content_builder_tab_coupon").hide();
	       $("#content_builder_tab_termconditon").hide();
	       $("#content_builder_tab_sms").hide();
	    }
	    if (location_type == '12')
	    {
	       $("#contestifi_content_builder_tab_li").show();
	    }
	    else
	    {
	       $("#contestifi_content_builder_tab_li").hide();
	    }
            $(".loading").hide();
	    $('.tab_menu').css({'cursor' : 'not-allowed'});
            $('.tab_menu').attr('disabled','disabled');
                 
            /*$('.crsl-items').carousel({
               visible: 1,
               itemMinWidth: 180,
               itemEqualHeight: 370,
               itemMargin: 9,
            });*/
	    
	    
               
                       
         });
	 $(document).on('click','.viewras',function(){
		  $(".loading").show();
		  var locuid=$(this).data('locuid');
		  var locmacid=$(this).data('macid');
		  
		   $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/raspberry_ping_sync_info",
   	    data: {locuid:locuid,locmacid:locmacid},
		dataType: 'json',
		success: function(data){
			$('.pinglogs').html(data.pinghtml);
			$('.synclogs').html(data.synchtml);
   		$('#raspberrymodal').modal('show');
		$(".loading").hide();
   	    }
          });
		  
		  
	  })
</script>
      <!-- script to get the geolocation and set lat and long -->
      <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0wqDpyD8TYvX-6KVfGpLxPNXqpVbxJFc&libraries=places" type="text/javascript"></script>	
      <script type="text/javascript">
   function initialize() {
      var myLocation = new google.maps.LatLng( '20.5937', '78.9629');
                                           var mapOptions = {
                                               center: myLocation,
                                               zoom: 10
                                           };
                                           var marker = new google.maps.Marker({
                                               position: myLocation,
                                               title: 'Property Location'
                                           });
                                           var map = new google.maps.Map(document.getElementById('map1'),mapOptions);
                                           marker.setMap(map);
   	    
       var input = document.getElementById('geolocation');
       //var options = {componentRestrictions: {country: 'in'}};
       //var autocomplete = new google.maps.places.Autocomplete(input,options);
       var autocomplete = new google.maps.places.Autocomplete(input);
       google.maps.event.addListener(autocomplete, 'place_changed', function () {
           var place = autocomplete.getPlace();
           document.getElementById('lat').value = place.geometry.location.lat().toFixed(6);
           document.getElementById('long').value = place.geometry.location.lng().toFixed(6);
           document.getElementById('placeid').value = place.place_id;
           $("#placeid").attr('class','mui--is-dirty valid mui--is-not-empty');
           $("#lat").attr('class','mui--is-dirty valid mui--is-not-empty');
           $("#long").attr('class','mui--is-dirty valid mui--is-not-empty');
    $("#placeidview").html(place.place_id);
    $("#latview").html(place.geometry.location.lat().toFixed(6));
    $("#longview").html(place.geometry.location.lng().toFixed(6));
    var address_1 = '';
    var address_2 = '';
    var postal_code = '';
    var state_name = '';
    var city_name = '';
    
    for (var i = 0; i < place.address_components.length; i++) {
	for (var j = 0; j < place.address_components[i].types.length; j++) {
	    if (place.address_components[i].types[j] == "sublocality_level_1") {
		address_1 = place.address_components[i].long_name;
	    }
	    if (place.address_components[i].types[j] == "locality") {
		address_2 = place.address_components[i].long_name;
	    }
	    if (place.address_components[i].types[j] == "postal_code") {
		postal_code = place.address_components[i].long_name;
	    }
	    if (place.address_components[i].types[j] == "administrative_area_level_1") {
		state_name = place.address_components[i].long_name;
	    }
	    if (place.address_components[i].types[j] == "administrative_area_level_2") {
		city_name = place.address_components[i].long_name;
	    }
	}
    }
    if (address_1 == '')
    {
	document.getElementById('address1').value = address_2;
	document.getElementById('address2').value = '';
    }
    else
    {
	document.getElementById('address1').value = address_1;
	document.getElementById('address2').value = address_2;
    }
    document.getElementById('pin').value = postal_code;
    $('select#state option').removeAttr("selected");
    $("#state option").filter(function() {
	return this.text.toLowerCase() == state_name.toLowerCase(); 
    }).attr('selected', true);
    var state_id = $("#state").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url()?>location/city_list",
      data: "state_id="+state_id,
      success: function(data){
	  $("#city").html(data);
	$("#city option").filter(function() {
	    return this.text.toLowerCase() == city_name.toLowerCase(); 
	}).attr('selected', true);
	  $("#city_popup").html(data);
      }
    });
    
    var myLocation = new google.maps.LatLng(place.geometry.location.lat().toFixed(6), place.geometry.location.lng().toFixed(6));
                                           var mapOptions = {
                                               center: myLocation,
                                               zoom: 10
                                           };
                                           var marker = new google.maps.Marker({
                                               position: myLocation,
                                               title: 'Property Location'
                                           });
                                           var map = new google.maps.Map(document.getElementById('map1'),mapOptions);
                                           marker.setMap(map);
       });
   }
   google.maps.event.addDomListener(window, 'load', initialize);
   
   $("#state").change(function(){
          $(".loading").show();
          var state_id = $("#state").val();
          $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/city_list",
   	    data: "state_id="+state_id,
   	    success: function(data){
   	     $(".loading").hide();
   		$("#city").html(data);
   		$("#city_popup").html(data);
   	    }
          });
   });
   $("#city").change(function(){
         /* $(".loading").show();
          var city_id = $("#city").val();
          $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/zone_list",
   	    data: "city_id="+city_id,
   	    success: function(data){
   	     $(".loading").hide();
   		$("#zone").html(data);
   	    }
          });*/
   });
   // add zone
   function add_zone_model(){
      $("#add_zone").modal("show");
   }
    // add city
   function add_city_model(){
      $("#add_city").modal("show");
   }
   function add_zone(){
      var city_id = $("#city_popup").val();
      var zone_name = $("#zone_name_popup").val();
      $(".loading").show();
       $.ajax({
           type: "POST",
           url: "<?php echo base_url()?>location/add_zone",
           data: "city_id="+city_id+"&zone_name="+zone_name,
           success: function(data){
           
           $.ajax({
                type: "POST",
                url: "<?php echo base_url()?>location/zone_list",
                data: "city_id="+city_id,
                success: function(data){
                 $(".loading").hide();
                 $("#zone_name_popup").val('');
                  $("#add_zone").modal("hide");
                    $("#zone").html(data);
                }
           });
           }
            });
   }
   function add_city(){
      var state_id = $("#state_popup").val();
      var city_name = $("#city_name_popup").val();
      $(".loading").show();
       $.ajax({
           type: "POST",
           url: "<?php echo base_url()?>location/add_city",
           data: "state_id="+state_id+"&city_name="+city_name,
           success: function(data){
           
           $.ajax({
                type: "POST",
                url: "<?php echo base_url()?>location/city_list",
                data: "state_id="+state_id,
                success: function(data){
                 $(".loading").hide();
                 $("#city_name_popup").val('');
                  $("#add_city").modal("hide");
                    $("#city").html(data);
                }
           });
           }
            });
   }
   var Clicked_ButtonValue;
   $(document).on('click', 'button[type="submit"]', function(e){
   //$('button[type="submit"]').click(function(e){
       Clicked_ButtonValue = $(this).val();
   });
    function add_location() {
         var hashtags = $("#hashtags").val();
       $(".loading").show();
      var enable_channel = 0;
      if ($('input[name="enable_channel"]').is(':checked')) {
         enable_channel = $("input[name='enable_channel']:checked").val();
      }
      var enable_nas_down_notification = 0;
      if ($('input[name="enable_nas_down_notification"]').is(':checked')) {
         enable_nas_down_notification = $("input[name='enable_nas_down_notification']:checked").val();
      }
       $("#add_location_error").html("");
       var created_location_id = $("#created_location_id").val();
       var location_type = $("input[name='location_type']").val();
       var location_name = $("#location_name").val();
       var geo_address = $("#geolocation").val();
       var placeid =$("#placeid").val();
       var lat =$("#lat").val();
       var long =$("#long").val();
       var address1 =$("#address1").val();
       var address2 =$("#address2").val();
       var pin =$("#pin").val();
       var state =$("#state").val();
       var city =$("#city").val();
       //var zone =$("#zone").val();
       var zone = '0';
       var location_id =$("#location_id").val();
       var contact_person_name =$("#contact_person_name").val();
       var email_id =$("#email_id").val();
       var mobile_no = $("#mobile_no").val();
       if (location_name == '' || geo_address == '' || address1 == '' || address2 =='' || pin == '' || state == '' || city == '' || zone =='' || contact_person_name == '' || email_id == '' || mobile_no == '') {
          $("#add_location_error").html("Please fill all fields");
          $(".loading").hide();
       }
       else{
          if (lat == ''|| long == '' || placeid == '') {
   	  $("#add_location_error").html("please select geo location properly");
   	  $(".loading").hide();
          }else{
	    var form_data = new FormData();
	    form_data.append('enable_nas_down_notification',enable_nas_down_notification);
	  form_data.append('enable_channel',enable_channel);
	 form_data.append('created_location_id',created_location_id);
	 form_data.append('location_type',location_type);
	 form_data.append('location_name',location_name);
	 form_data.append('geo_address',geo_address);
	 form_data.append('placeid',placeid);
	 form_data.append('lat',lat);
	 form_data.append('long',long);
	 form_data.append('address1',address1);
	 form_data.append('address2',address2);
	 form_data.append('pin',pin);
	 form_data.append('state',state);
	 form_data.append('city',city);
	 form_data.append('zone',zone);
	 form_data.append('location_id',location_id);
	 form_data.append('contact_person_name',contact_person_name);
	 form_data.append('email_id',email_id);
	 form_data.append('mobile_no',mobile_no);
	 form_data.append('hashtags',hashtags);
   	  $.ajax({
   	     type: "POST",
   	     url: "<?php echo base_url()?>location/add_location",
	     data: form_data,
	     processData: false,
    contentType: false,
   	     /*data: "enable_channel="+enable_channel+"&created_location_id="+created_location_id+"&location_type="+location_type+"&location_name="+location_name+"&geo_address="+geo_address+"&placeid="+placeid+"&lat="+lat+"&long="+long+"&address1="+address1+"&address2="+address2+"&pin="+pin+"&state="+state+"&city="+city+"&zone="+zone+"&location_id="+location_id+"&contact_person_name="+contact_person_name+"&email_id="+email_id+"&mobile_no="+mobile_no,*/
   	     success: function(data){
   		$(".loading").hide();
   		if(isNaN(data)){
   		   $("#add_location_error").html("You don't have sufficient balance");
   		}else{
   		   //set value in hidden field End
   		   $("#created_location_id").val(data);
   		   $("#location_name_hidden").val(location_name);
   		   $("#location_id_hidden").val(location_id);
   		   $("#email_hidden").val(email_id);
   		   $("#mobile_hidden").val(mobile_no);
   		   //set value in hidden field End
   		   if (Clicked_ButtonValue == 'exit') {
   		       window.location = "<?php  echo base_url()?>location";
   		   }else{
   		      // go to next tab
   		      //$("#location_detail_tab_checked").show();
   		      $('.tab_menu').css({'cursor' : 'pointer'});
   		      $('.tab_menu').removeAttr('disabled');
   		      //$('.location_name_view').html(location_name);
   		      //$('.location_id_view').html("("+location_id+")");
   		      //$('.email_view').html(email_id);
   		      //$('.mobile_number_view').html(mobile_no);
   		      // hide location type radio
   		      //$("#location_type_div").hide();
                      $(".data_limit_div").addClass("disabled");
                        (function() {
                           $(".os-range").slider({
                              range: "min",
                              max: 600,
                              value:5,
                              min: 5,
                           step: 5,
                              slide: function(e, ui) {
                                $(".os-currentVal").html(ui.value);
                              }
                           });
                        }).call(this);
   		      if (location_type == '1') {
			$(".data_limit_div").removeClass("disabled");
			$('.timelimit_checkbox').attr("disabled", true);
   			 $("#crsl_items_hotel_location").remove();
   			 $("#cp_public_location").show();
   			 $("#cp_hotel_location").hide();
                         $("#publicspaces_slider_image_div").show();
			$("#publicspace_social_login_div").show();
			$("#publicspace_loginwith_div").show();
			$("#slider_background_color").show();
			$("#pin_login_div_publicwifi").show();
                        
   		      }else{
                        
   			 if (location_type == '2') {
			   $(".data_limit_div").removeClass("disabled");
   			    $(".cp_hotel_slider1").show();
   			    $(".cp_hotel_slider2").show();
   			    $(".cp_hotel_slider3").show();
   			    $(".cp_hotel_slider4").show();
   			    $("#hotel_ssid_div").show();//show hotel ssid div
                            $("#other_location_plan_div").show();
			    $("#is_white_theme_div").show();
   			 }else if (location_type == '3') {
			   $(".data_limit_div").removeClass("disabled");
   			    $(".cp_hospital_slider1").show();
   			    $(".cp_hospital_slider2").show();
   			    $(".cp_hospital_slider3").show();
   			    $(".cp_hospital_slider4").show();
   			    $("#hospital_ssid_div").show();//show hospital ssid div and cp type of that location
                            $("#other_location_plan_div").show();
   			 }else if (location_type == '4') {
			   $(".data_limit_div").removeClass("disabled");
   			    $(".cp_institutional_slider1").show();
   			    $(".cp_institutional_slider2").show();
   			    $(".cp_institutional_slider3").show();
   			    $(".cp_institutional_slider4").show();
   			    $("#institutional_user_type_div").show();//show department type
   			    //$("#institutional_ssid_div").show();//show hotel/clg ssid div
                            $("#other_location_plan_div").show();
   			 }else if (location_type == '5') {
			   $(".data_limit_div").removeClass("disabled");
   			    $(".cp_enterprise_slider1").show();
   			    $(".cp_enterprise_slider2").show();
   			    $(".cp_enterprise_slider3").show();
   			    $(".cp_enterprise_slider4").show();
   			    //$("#enterprise_user_type_div").show();
   			    $("#institutional_user_type_div").show();// show department type
   			    $("#enterprise_ssid_div").show();//show hospital ssid div and cp type of that location
                            $("#other_location_plan_div").show();
   			 }else if (location_type == '6') {
   			    $(".cp_cafe_slider1").show();
   			    $(".cp_cafe_slider2").show();
   			    $(".cp_cafe_slider3").show();
   			    $(".cp_cafe_slider4").show();
   			    //$("#caffe_ssid_div").show();//show cafe ssid div
                            $("#cafe_plan_div").show();
                            $("#social_login_div").show();
                            $("#pin_login_div").show();
                            $("#loginwith_div").show();
			    $("#offer_redemption_mode_div").show();
			    $("#is_white_theme_div").show();
   			 }
                         else if (location_type == '7') {
   			    $(".cp_retail_slider1").show();
   			    $(".cp_retail_slider2").show();
   			    $(".cp_retail_slider3").show();
   			    $(".cp_retail_slider4").show();
   			    //$("#caffe_ssid_div").show();//show cafe ssid div
                            $("#cafe_plan_div").show();
                            $("#social_login_div").show();
                            $("#disablewifi_div").show();
                            $("#loginwith_div").show();
			    $("#offer_redemption_mode_div").show();
   			 }
			 else if (location_type == '8') {
   			    $(".cp_retail_slider1").show();
   			    $(".cp_retail_slider2").show();
   			    $(".cp_retail_slider3").show();
   			    $(".cp_retail_slider4").show();
   			    //$("#caffe_ssid_div").show();//show cafe ssid div
                            $("#custom_location_div").show();
			    $("#slider_image_div").show();
                            $(".hide_for_custom_location").hide();
                            $("#disablewifi_div").show();
   			 }
			 else if (location_type == '9') {
			   $(".slider1_brand_logo").hide();
   			    $(".cp_purple_slider1").show();
   			    $(".cp_purple_slider2").show();
   			    $(".cp_purple_slider3").show();
   			    $(".cp_purple_slider4").show();
   			    //$("#caffe_ssid_div").show();//show cafe ssid div
                            $("#cafe_plan_div").show();
                            $("#social_login_div").show();
                            $("#pin_login_div").show();
                            $("#loginwith_div").show();
			    $("#slider_image_div").show();
			    $("#offer_redemption_mode_div").show();
   			 }
			 else if (location_type == '10') {
			    $(".right_side_logo").hide();
			   $(".slider1_brand_logo").hide();
   			    $(".cp_offline_slider1").show();
   			    $(".cp_offline_slider2").show();
   			    $(".cp_offline_slider3").show();
   			    $(".cp_offline_slider4").show();
                            $("#default_offer_image_div").hide();
                            $(".hide_for_custom_location").hide();
                            $("#offline_location_div").show();
                            $('.crsl-item-logo-hotel').hide();
   			 }
			 else if (location_type == '11') {
			    $(".right_side_logo").hide();
			   $(".slider1_brand_logo").hide();
   			    $(".cp_offline_vm_slider1").show();
   			    $(".cp_offline_vm_slider2").show();
   			    $(".cp_offline_vm_slider3").show();
   			    $(".cp_offline_vm_slider4").show();
                            $("#default_offer_image_div").hide();
                            $(".hide_for_custom_location").hide();
                            $("#offline_location_div").show();
			    $(".offline_not_in_visitor_management").hide();
                            $('.crsl-item-logo-hotel').hide();
			    $("#vm_cp_org_dpt_emp_main_div").show();
			    
			    $("#location_logo2_div").show();
			    $("#offline_vm_banner_image_div").show();
			    $("#offline_banner_image_div").hide();
   			 }
			 else if (location_type == '12') {
			   $(".right_side_logo").hide();
   			    $(".cp_contestifi_slider1").show();
   			    $(".cp_contestifi_slider2").show();
   			    $(".cp_contestifi_slider3").show();
   			    $(".cp_contestifi_slider4").show();
				$("#default_offer_image_div").hide();
				$("#offline_banner_image_div").hide();
				$("#offline_location_div").show();
				$(".offline_not_in_visitor_management").hide();
				$(".hide_for_custom_location").show();
				$("#is_signup_enable_div").show();
				$("#loginwith_div").show();
				$('.crsl-item-logo-hotel').hide();
				$("#contestifi_contest_header_text_div").show();
   			 }
			  else if (location_type == '13') {
			    $(".right_side_logo").hide();
   			    $(".cp_retail_audit_slider1").show();
   			    $(".cp_retail_audit_slider2").show();
   			    $(".cp_retail_audit_slider3").show();
   			    $(".cp_retail_audit_slider4").show();
   			    //$("#caffe_ssid_div").show();//show cafe ssid div
                            $("#retail_audit_div").show();
                            $(".hide_for_custom_location").hide();
			    $("#default_offer_image_div").hide();
   			 }
			 else if (location_type == '14') {
			    $(".right_side_logo").hide();
			   $(".slider1_brand_logo").hide();
   			    $(".cp_event_module_slider1").show();
   			    $(".cp_event_module_slider2").show();
   			    $(".cp_event_module_slider3").show();
   			    $(".cp_event_module_slider4").show();
                            $("#default_offer_image_div").hide();
                            $(".hide_for_custom_location").hide();
			    $(".offline_not_in_visitor_management").hide();
                            $('.crsl-item-logo-hotel').hide();
			    $("#offline_vm_banner_image_div").show();
			    $("#offline_banner_image_div").hide();
			    
			    $("#offline_event_module_turn_feature").show();
   			 }
			 else if (location_type == '16') {
			    $(".right_side_logo").hide();
			   $(".slider1_brand_logo").hide();
   			    $(".cp_offline_slider1").show();
   			    $(".cp_offline_slider2").show();
   			    $(".cp_offline_slider3").show();
   			    $(".cp_offline_slider4").show();
			    
                            $("#default_offer_image_div").hide();
                            $(".hide_for_custom_location").hide();
			    $(".offline_not_in_visitor_management").hide();
                            $('.crsl-item-logo-hotel').hide();
			    $('#other_all_location_theme_color_div').hide();
			    $("#lms_vm_cp_org_dpt_emp_main_div").show();
   			 } else if (location_type == '18') {
			    $(".right_side_logo").hide();
			   $(".slider1_brand_logo").hide();
   			    $(".cp_offline_vm_slider1").show();
   			    $(".cp_offline_vm_slider2").show();
   			    $(".cp_offline_vm_slider3").show();
   			    $(".cp_offline_vm_slider4").show();
                            $("#default_offer_image_div").hide();
                            $(".hide_for_custom_location").hide();
                            $("#offline_location_div").show();
			    $(".offline_not_in_visitor_management").hide();
                            $('.crsl-item-logo-hotel').hide();
			    $("#vm_cp_org_dpt_emp_main_div").show();
			    
			    $("#location_logo2_div").show();
			    $("#offline_vm_banner_image_div").show();
			    $("#offline_banner_image_div").hide();
   			 }
   			 $("#crsl_items_public_location").remove();
   			 $("#cp_hotel_location").show();
   			 $("#cp_public_location").hide();
   		      }
   		      mui.tabs.activate('captive_portal');
   		       $('.crsl-items').carousel({
   			 visible: 1,
   			 itemMinWidth: 180,
   			 itemEqualHeight: 370,
   			 itemMargin: 9
   		      });
   		   }
   		}
   		
   		$(".loading").hide();
   	     }
          });
          }
          
       }
     }
       


   // on cp type change show and hide div
   $("input[name='captive_potal_type']").change(function() {
      var cp_type = $("input[name='captive_potal_type']:checked").val();
         if (cp_type == 'cp1') {
	    $(".time_limit_div").removeClass("disabled");
	    $(".data_limit_div").removeClass("disabled");
	    $('.datalimit_checkbox').prop('checked', false);
	    $('.datalimit_checkbox').attr("disabled", false);
	    $('.timelimit_checkbox').prop('checked', true);
	    $('.timelimit_checkbox').attr("disabled", true);
         (function() {
                        $(".os-range").slider({
                           range: "min",
                           max: 600,
                           value:5,
                           min: 5,
                           step: 5,
                           slide: function(e, ui) {
                             $(".os-currentVal").html(ui.value);
                           }
                        });
                     }).call(this);
            //$(".cp_type_slider").html("CAPTIVE PORTAL 1 SAMPLE");
            $(".cp1_slider1").show();
            $(".cp2_slider1").hide();
            $(".cp3_slider1").hide();
            
            $(".cp1_slider2").show();
            $(".cp2_slider2").hide();
            $(".cp3_slider2").hide();
            
            $(".cp1_slider3").show();
            $(".cp2_slider3").hide();
            $(".cp3_slider3").hide();
            
            $(".cp1_slider4").show();
            $(".cp2_slider4").hide();
            $(".cp3_slider4").hide();
            $(".cp2_div").hide();
            $(".cp3_div").hide();
            $(".cp1_div").show();
	    $(".hide_data_from_cp3").show();
	    
	    //slider images show & other
	    $("#publicspaces_slider_image_div").show();
	    $("#publicspace_social_login_div").show();
	    $("#publicspace_loginwith_div").show();
	    
	    $("#slider_background_color").show();
	    $("#div_for_cp1_plantype").show();
	    $("#pin_login_div_publicwifi").show();
         }
	 
         else if (cp_type == 'cp2') {
            $(".time_limit_div").addClass("disabled");
	    $('.time_limit_div').prop('checked', false);
         $(".data_limit_div").removeClass("disabled");
	 $('.datalimit_checkbox').prop('checked', true);
	 $('.datalimit_checkbox').attr("disabled", true);
         (function() {
                        $(".os-range_hybrid").slider({
                           range: "min",
                           max: 600,
                           value:5,
                           min: 5,
                           step: 5,
                           slide: function(e, ui) {
                             $(".os-currentVal_hybrid").html(ui.value);
                           }
                        });
                     }).call(this);
             //$(".cp_type_slider").html("CAPTIVE PORTAL 2 SAMPLE");
            
            $(".cp1_slider1").hide();
            $(".cp2_slider1").show();
            $(".cp3_slider1").hide();
            
            $(".cp1_slider2").hide();
            $(".cp2_slider2").show();
            $(".cp3_slider2").hide();
            
            $(".cp1_slider3").hide();
            $(".cp2_slider3").show();
            $(".cp3_slider3").hide();
            
            $(".cp1_slider4").hide();
            $(".cp2_slider4").show();
            $(".cp3_slider4").hide();
            $(".cp1_div").hide();
            $(".cp3_div").hide();
            $(".cp2_div").show();
	    $(".hide_data_from_cp3").show();
	    
	    //slider images hide & other
	    $("#publicspaces_slider_image_div").hide();
	    $("#publicspace_social_login_div").hide();
	    $("#publicspace_loginwith_div").hide();
	    
	    $("#slider_background_color").hide();
	    $("#pin_login_div_publicwifi").hide();
         }
         else if (cp_type == 'cp3') {
             //$(".cp_type_slider").html("CAPTIVE PORTAL 3 SAMPLE");
            $(".time_limit_div_hybrid").removeClass("disabled");
            $(".data_limit_div_hybrid").removeClass("disabled");
	    $('.datalimit_checkbox_hybrid').prop('checked', true);
            $('.timelimit_checkbox_hybrid').prop('checked', true);
	    $('.datalimit_checkbox_hybrid').attr("disabled", true);
	    $('.timelimit_checkbox_hybrid').attr("disabled", true);
            $(".cp1_slider1").hide();
            $(".cp2_slider1").hide();
            $(".cp3_slider1").show();
            
            $(".cp1_slider2").hide();
            $(".cp2_slider2").hide();
            $(".cp3_slider2").show();
            
            $(".cp1_slider3").hide();
            $(".cp2_slider3").hide();
            $(".cp3_slider3").show();
            
            $(".cp1_slider4").hide();
            $(".cp2_slider4").hide();
            $(".cp3_slider4").show();
            $(".cp2_div").hide();
            $(".cp1_div").hide();
            $(".cp3_div").show();
	    $(".hide_data_from_cp3").hide();
	    
	     //slider images hide
	    $("#publicspaces_slider_image_div").hide();
	    $("#publicspace_social_login_div").hide();
	    $("#publicspace_loginwith_div").hide();
	    
	    $("#slider_background_color").hide();
	    $("#pin_login_div_publicwifi").hide();
         }
	 else if (cp_type == 'cp4') {
            $(".time_limit_div").removeClass("disabled");
         $(".data_limit_div").addClass("disabled");
         (function() {
                        $(".os-range").slider({
                           range: "min",
                           max: 600,
                           value:5,
                           min: 5,
                           step: 5,
                           slide: function(e, ui) {
                             $(".os-currentVal").html(ui.value);
                           }
                        });
                     }).call(this);
            //$(".cp_type_slider").html("CAPTIVE PORTAL 1 SAMPLE");
            $(".cp1_slider1").show();
            $(".cp2_slider1").hide();
            $(".cp3_slider1").hide();
            
            $(".cp1_slider2").show();
            $(".cp2_slider2").hide();
            $(".cp3_slider2").hide();
            
            $(".cp1_slider3").show();
            $(".cp2_slider3").hide();
            $(".cp3_slider3").hide();
            
            $(".cp1_slider4").show();
            $(".cp2_slider4").hide();
            $(".cp3_slider4").hide();
            $(".cp2_div").hide();
            $(".cp3_div").hide();
            $(".cp1_div").show();
	    
	    
	    $(".hide_data_from_cp3").hide();
	    $(".cp1_plan_div").hide();
	    $(".cp3_plan_div").show();
	    
	    //slider images show & other
	    $("#publicspaces_slider_image_div").show();
	    $("#publicspace_social_login_div").show();
	    $("#publicspace_loginwith_div").show();
	    
	    $("#slider_background_color").show();
	    $("#pin_login_div_publicwifi").hide();
         }
   });
   
   function add_captive_portal() {
	var is_pinenable = 0;
      var slider_background_color = '';
      var is_socialloginenable = 0;
      var login_with_mobile_email = 0;
   $("#add_captive_portal_error").html("");
   var is_otpdisabled = '1';
   if ($('input[name="is_otpdisabled_public_location"]').is(':checked')) {
         is_otpdisabled = $("input[name='is_otpdisabled_public_location']:checked").val();
   }
   var main_ssid = '';
   if ($('input[name="public_wifi_ssid"]').val() == '') {
	 $("#add_captive_portal_error").html("Please enter SSID");
	    return false;
   }else{
      main_ssid = $('input[name="public_wifi_ssid"]').val();
   }
   var cp_type = $("input[name='captive_potal_type']:checked").val();
   var cp1_plan =0;
   if (cp_type == 'cp1')
   {
	if ($('input[name="is_pinenable_publicwifi"]').is(':checked')) {
	    is_pinenable = $("input[name='is_pinenable_publicwifi']:checked").val();
	}
      var slider_background_color = $('input[name="slider_background_color"]').val();
      if ( $("#cp1_no_of_daily_session").val() == '') {
         $("#add_captive_portal_error").html("Please fill all fields");
         return false;
      }
      if ($('input[name="is_socialloginenable"]').is(':checked')) {
         is_socialloginenable = $("input[name='is_socialloginenable']:checked").val();
      }
      var login_with_mobile_email = $("input[name='login_with_mobile_email']:checked").val();
      
      if ($("#cp1_plan").val() == '')
	 {
	    $("#add_captive_portal_error").html("Please fill all fields");
	    return false;
	 }
	 cp1_plan = $("#cp1_plan").val();
	 save_time_plan("yes");
	 
     
   }
   else if (cp_type == 'cp2') {
      if ($("#cp2_plan").val() == '' || $("#cp2_signip_data").val() == '' || $("#cp2_daily_data").val() == '') {
         $("#add_captive_portal_error").html("Please fill all fields");
         return false;
      }
      save_data_plan("yes");
   }
   else if (cp_type == 'cp3') {
      //if ($("#cp3_hybrid_plan").val() == '' || $("#cp3_data_plan").val() == '' || $("#cp3_signip_data").val() == '' || $("#cp3_daily_data").val() == '') {
      if ($("#cp3_hybrid_plan").val() == ''  || $("#cp3_signip_data").val() == '' || $("#cp3_daily_data").val() == '') {
         $("#add_captive_portal_error").html("Please fill all fields");
         return false;
      }
      save_hybrid_data_plan("yes");
      var delayInMilliseconds = 5000; //1 second
      setTimeout(function() {
         save_hybrid_plan("yes");
      }, delayInMilliseconds);
   }
   else if (cp_type == 'cp4') {
      cp1_plan = $("#cp3_hybrid_plan").val();
      var slider_background_color = $('input[name="slider_background_color"]').val();
      if ($("#cp3_hybrid_plan").val() == '' || $("#cp1_no_of_daily_session").val() == '') {
         $("#add_captive_portal_error").html("Please fill all fields");
         return false;
      }
      if ($('input[name="is_socialloginenable"]').is(':checked')) {
         is_socialloginenable = $("input[name='is_socialloginenable']:checked").val();
      }
      var login_with_mobile_email = $("input[name='login_with_mobile_email']:checked").val();
      save_hybrid_data_plan("yes");
      var delayInMilliseconds = 5000; //1 second
      setTimeout(function() {
         save_hybrid_plan("yes");
      }, delayInMilliseconds);
   }
   var icon_color = $('input[name="icon_color"]:visible').val();
   var image_name = '';
   var image_src = '';
   image_src = $('input[name="thumb_values"]').val();
   if (typeof image_src != "undefined") {
      image_name = $('input[name="thumb_values"]').next().next().attr('name');
      image_src = $('input[name="thumb_values"]').val();
   }else{
      image_src = '';
   }
   
   
   var background_image_name = '';
   var background_image_src = '';
   background_image_src = $('input[name="thumb_background_values"]').val();
   if (typeof background_image_src != "undefined") {
      background_image_name = $('input[name="thumb_background_values"]').next().next().attr('name');
      thumb_background_values = $('input[name="thumb_background_values"]').val();
   }else{
      background_image_src = '';
   }
   
   var slider1_image_name = '';
   var slider1_image_src = '';
   slider1_image_src = $('input[name="thumb_slider1_values"]').val();
   if (typeof slider1_image_src != "undefined") {
      slider1_image_name = $('input[name="thumb_slider1_values"]').next().next().attr('name');
      thumb_slider1_values = $('input[name="thumb_slider1_values"]').val();
   }else{
      slider1_image_src = '';
   }
  
  
   var slider2_image_name = '';
   var slider2_image_src = '';
   slider2_image_src = $('input[name="thumb_slider2_values"]').val();
   if (typeof slider2_image_src != "undefined") {
      slider2_image_name = $('input[name="thumb_slider2_values"]').next().next().attr('name');
      thumb_slider2_values = $('input[name="thumb_slider2_values"]').val();
   }else{
      slider2_image_src = '';
   }
   
   var form_data = new FormData(); 
   //form_data.append("file", file_data);
   form_data.append('image_name',image_name);
   form_data.append('image_src',image_src);
   form_data.append('background_image_name',background_image_name);
   form_data.append('background_image_src',background_image_src);
   form_data.append('slider1_image_name',slider1_image_name);
   form_data.append('slider1_image_src',slider1_image_src);
   form_data.append('slider2_image_name',slider2_image_name);
   form_data.append('slider2_image_src',slider2_image_src);
   form_data.append("location_uid", $("#location_id_hidden").val());
   form_data.append("locationid", $("#created_location_id").val());
   form_data.append("cp_type", $("input[name='captive_potal_type']:checked").val());
   form_data.append("cp1_plan", cp1_plan);
   form_data.append("cp1_no_of_daily_session", $("#cp1_no_of_daily_session").val());
   form_data.append("cp2_plan", $("#cp2_plan").val());
   form_data.append("cp2_signip_data", $("#cp2_signip_data").val());
   form_data.append("cp2_daily_data", $("#cp2_daily_data").val());
   form_data.append("cp3_hybrid_plan", $("#cp3_hybrid_plan").val());
   //form_data.append("cp3_data_plan", $("#cp3_data_plan").val());
   form_data.append("cp3_data_plan", '0');
   form_data.append("cp3_signip_data", $("#cp3_signip_data").val());
   form_data.append("cp3_daily_data", $("#cp3_daily_data").val());
   form_data.append("is_otpdisabled", is_otpdisabled);
   form_data.append("main_ssid", main_ssid);
   form_data.append("icon_color", icon_color);
   form_data.append("is_socialloginenable", is_socialloginenable);
   form_data.append("login_with_mobile_email", login_with_mobile_email);
   form_data.append("slider_background_color", slider_background_color);
   form_data.append("is_pinenable", is_pinenable);
   //form_data.append("cp1_plan_type", cp1_plan_type);
   $(".loading").show();
   $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_captive_portal",
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,   
        success: function(data){
   	$(".loading").hide();
   	if (Clicked_ButtonValue == 'exit') {
   	    window.location = "<?php  echo base_url()?>location";
   	}else{
   	   // go to next tab
   	    //$("#captive_portal_tab_checked").show();
             mui.tabs.activate('nas_setup');
   	   var location_uid =  $("#location_id_hidden").val();
   	   nas_setup_list(location_uid);
   	  	get_location_offer_image_path();
		get_location_cp_path();
   	}
   	
        }
         });
  
   }
   
   $(document).on('click', ".institutional_add_user_button", function(){
         $("#cp_enterprise_user_listing").append('<div class="row"> '+
                                       '<div class="col-sm-4 col-xs-4"><div class="row"><div class="mui-textfield mui-textfield--float-label">'+
                                       '<input data-id="" class = "cp_institution_user_list mui--is-empty mui--is-untouched mui--is-pristine" type="text" name="cp_institution_user_list[]" value="" >'+
                                       '</div></div></div>'+
                                       '<div class="col-sm-3 col-xs-3">'+
                                       '<p class="mui-btn mui-btn--small mui-btn--danger remove_hotel_plan" style="margin-top:2px">Remove</p>'+
                                       '</div></div>');
   });
   // hotel plan on remove button remove div
   $(document).on('click', ".remove_hotel_plan", function(){
      $(this).parent().parent().remove();
   });
   function cp_hotel_add_plan() {
      var is_valid = 1;
      var plan_id = $("#cp_hotel_plan").val();
      var plan_name = $("#cp_hotel_plan option:selected").text();
      if (plan_id == '') {
         return false;
      }
      $(".hotel_plan_listing").each(function(){
         if($(this).data('planid') == plan_id){
            is_valid = 0;
            return false;
         }
      });
      if (is_valid == '1') {
         $("#cp_hotel_added_plan_listing").append('<div class="col-sm-12 col-xs-12 hotel_plan_listing" '+
                                     'data-planid='+plan_id+'>'+
                                     '<div class="col-sm-6 col-xs-6">'+
                                     '<legend style="margin-top:5px; font-weight:400">'+plan_name+'</legend><input type="hidden" name="cp_home_selected_plan_id[]" value="'+plan_id+'"></div>'+
                          
                                     '<div class="col-sm-6 col-xs-6">'+
                                     '<p class="mui-btn mui-btn--small mui-btn--danger remove_hotel_plan" style="margin-top:2px">remove</p>'+
                                     '</div></div>');
      }
   
   }
    //hotel cp on add plan click append plan in list
       // submit hotel cp form
   function add_hotel_captive_portal() {
	var tatdays = 3;
	var is_preaudit_disable = '0';
	if ($('input[name="is_preaudit_disable"]').is(':checked')) {
	    is_preaudit_disable = $("input[name='is_preaudit_disable']:checked").val();
	}
	var audit_ticket_email_send = $("#audit_ticket_email_send").val();
	var event_module_signup_user_name_require = '0';
	var event_model_signup_user_organisation_require = '0';
	var event_module_signup_nick_name = '0';
	var event_module_signup_email = '0';
	var event_module_signup_phone = '0';
	var is_live_chat_enable = 0;
	var is_discuss_enable = 0;
	var is_information_enable = 0;
	var is_poll_enable = 0;
	var is_download_enable = 0;
	var is_white_theme = 0;
      var user_name_required = '0';
      var user_email_required = '0';
      var user_age_required = '0';
      var user_gender_required = '0';
      
      var contestifi_contest_header = '';
      // for offline location
      var is_offline_registration = 0;
      var synk_frequency = 1;
      var offline_cp_type = 0;
      var offline_cp_source_folder_path = '';
      var offline_cp_landing_page_path = '';
      var custom_captive_url = '';
      var custom_location_plan = '0';
      var custon_cp_signip_data = '0';
      var custon_cp_daily_data = '0';
      var custon_cp_no_of_daily_session = '0';
      var is_socialloginenable = 0;
      var is_pinenable = 0;
      var is_wifidisabled = '1';
      var num_of_session_per_day = 0;
      var login_with_mobile_email = 0;
      var offer_redemption_mode = 0;
      var location_type = $("#location_type").val();
      $("#add_hotel_captive_portal_error").html("");
      if (location_type != '6' && location_type != '7' && location_type != '8'  && location_type != '9' && location_type != '10' && location_type != '11' && location_type != '12' && location_type != '13' && location_type != '14' && location_type != '16') {
	 var cp_home_selected_plan_id = $("input[name='cp_home_selected_plan_id[]']").map(function(){return $(this).val();}).get();
	 if (cp_home_selected_plan_id == '') {
	    $("#add_hotel_captive_portal_error").html("Please add atlist one plan");
	    return false;
	 }
      }
      
      var image_name = '';
      var image_src = '';
      image_src = $('input[name="thumb_values"]').val();
      if (typeof image_src != "undefined") {
	 image_name = $('input[name="thumb_values"]').next().next().attr('name');
	 image_src = $('input[name="thumb_values"]').val();
      }else{
	 image_src = '';
      }
   
   
      var background_image_name = '';
      var background_image_src = '';
      background_image_src = $('input[name="thumb_background_values"]').val();
      if (typeof background_image_src != "undefined") {
	 background_image_name = $('input[name="thumb_background_values"]').next().next().attr('name');
	 thumb_background_values = $('input[name="thumb_background_values"]').val();
      }else{
	 background_image_src = '';
      }
   
      var slider1_image_name = '';
      var slider1_image_src = '';
      slider1_image_src = $('input[name="thumb_slider1_values"]').val();
      if (typeof slider1_image_src != "undefined") {
	 slider1_image_name = $('input[name="thumb_slider1_values"]').next().next().attr('name');
	 thumb_slider1_values = $('input[name="thumb_slider1_values"]').val();
      }else{
	 slider1_image_src = '';
      }
  
  
   var slider2_image_name = '';
   var slider2_image_src = '';
   slider2_image_src = $('input[name="thumb_slider2_values"]').val();
   if (typeof slider2_image_src != "undefined") {
      slider2_image_name = $('input[name="thumb_slider2_values"]').next().next().attr('name');
      thumb_slider2_values = $('input[name="thumb_slider2_values"]').val();
   }else{
      slider2_image_src = '';
   }
   
   var enterprise_user_type = 0;
   var main_ssid = '';
   var guest_ssid = '';
   var cp_cafe_plan_id = '';
   var location_portal = '';
   var is_otpdisabled = '1';
   var icon_color = $('input[name="icon_color"]:visible').val();
   if ($('input[name="is_otpdisabled"]').is(':checked')) {
         is_otpdisabled = $("input[name='is_otpdisabled']:checked").val();
   }
   var institutional_user_list = $("input[name='cp_institution_user_list[]']").map(function(){
      //return $(this).val();
      return $(this).data("id")+"~~~~"+$(this).val();
      }).get();
   if ($('input[name="hotel_guest_wifi_ssid"]').val() == '') {
	 $("#add_hotel_captive_portal_error").html("Please enter SSID");
	 return false;
   }else{
	 var check_valid = validate_ssid($('input[name="hotel_guest_wifi_ssid"]').val());
     
      if (check_valid == 0)
      {
	 $("#add_hotel_captive_portal_error").html("Invalid ssid. Only '-' ie Hyphen and '_' ie Underscore special character are allowed. And No space are allowed.");
	 return false
      }
      else
      {
	 main_ssid = $('input[name="hotel_guest_wifi_ssid"]').val();
      }
   }
    
   if (location_type == '2') {
      location_portal = 'cp_hotel';
      is_white_theme = $("input[name='is_white_theme']:checked").val();
      var hotel_guest_wifi = 0;
      var hotel_visitor_wifi = 0;
      
      if ($('input[name="hotel_guest_wifi"]').is(':checked')) {
         hotel_guest_wifi = $('input[name="hotel_guest_wifi"]').val();
         
      }
      if ($('input[name="hotel_visitor_wifi"]').is(':checked')) {
         hotel_visitor_wifi = $('input[name="hotel_visitor_wifi"]').val();
      }
      if (hotel_guest_wifi == '0' && hotel_visitor_wifi == '0') {
         $("#add_hotel_captive_portal_error").html("Please select atlist one wifi type");
         return false;
      }else{
         if (hotel_guest_wifi == '1' && hotel_visitor_wifi == '1') {
	    enterprise_user_type = '3';
         }else if (hotel_guest_wifi) {
	    enterprise_user_type = '1';
         }else{
	    enterprise_user_type = '2';
         }
      }
   }else if (location_type == '3') {
      location_portal = 'cp_hospital';
      var hospital_patient_wifi = 0;
      var hospital_guest_wifi = 0;
      if ($('input[name="hospital_patient_wifi"]').is(':checked')) {
         hospital_patient_wifi = $('input[name="hospital_patient_wifi"]').val();
      }
      if ($('input[name="hospital_guest_wifi"]').is(':checked')) {
         hospital_guest_wifi = $('input[name="hospital_guest_wifi"]').val();
      }
      if (hospital_guest_wifi == '0' && hospital_patient_wifi == '0') {
         $("#add_hotel_captive_portal_error").html("Please select atlist one wifi type");
         return false;
      }else{
         if (hospital_guest_wifi == '1' && hospital_patient_wifi == '1') {
	    enterprise_user_type = '3'
         }else if (hospital_patient_wifi) {
	    enterprise_user_type = '1'
         }else{
	    enterprise_user_type = '2'
         }
      }
   }else if (location_type == '4') {
      location_portal = 'cp_institutional';
      if (institutional_user_list == '') {
	    $("#add_hotel_captive_portal_error").html("Please Add atlist one department type");
	    return false;
      }
   }
   else if (location_type == '5') {
      location_portal = 'cp_enterprise';
      var enterprise_employee_wifi = 0;
      var enterprise_guest_wifi = 0;
      if ($('input[name="enterprise_employee_wifi"]').is(':checked')) {
         enterprise_employee_wifi = $('input[name="enterprise_employee_wifi"]').val();
         
      }
      if ($('input[name="enterprise_guest_wifi"]').is(':checked')) {
         enterprise_guest_wifi = $('input[name="enterprise_guest_wifi"]').val();
      }
      if (enterprise_employee_wifi == '0' && enterprise_guest_wifi == '0') {
         $("#add_hotel_captive_portal_error").html("Please select atlist one wifi type");
         return false;
      }else{
         if (enterprise_employee_wifi == '1' && enterprise_guest_wifi == '1') {
	    enterprise_user_type = '3'
         }else if (enterprise_employee_wifi) {
	    enterprise_user_type = '1'
         }else{
	    enterprise_user_type = '2'
         }
      }
      if (institutional_user_list == '') {
	    $("#add_hotel_captive_portal_error").html("Please Add atlist one department type");
	    return false;
      }
   }
   else if (location_type == '6') {
    is_white_theme = $("input[name='is_white_theme']:checked").val();
      if ($("#cp1_no_of_daily_session_other").val() == '') {
         $("#add_hotel_captive_portal_error").html("Please Enter Number of session");
	 return false;
      }else{
         num_of_session_per_day = $("#cp1_no_of_daily_session_other").val();
      }
      location_portal = 'cp_cafe';
      
	 if ($('#cp_cafe_plan').val() == '') {
	    $("#add_hotel_captive_portal_error").html("Please Select plan");
	       return false;
	 }else{
	    cp_cafe_plan_id = $('#cp_cafe_plan').val();
	 }
	 save_time_plan_other_location_cafe("yes");
      
      if ($('input[name="is_socialloginenable"]').is(':checked')) {
         is_socialloginenable = $("input[name='is_socialloginenable']:checked").val();
      }
      if ($('input[name="is_pinenable"]').is(':checked')) {
         is_pinenable = $("input[name='is_pinenable']:checked").val();
      }
      var login_with_mobile_email = $("input[name='login_with_mobile_email_notpublic']:checked").val();
      
      offer_redemption_mode = $("input[name='offer_redemption_mode']:checked").val();
   }
   else if (location_type == '7') {
      if ($("#cp1_no_of_daily_session_other").val() == '') {
         $("#add_hotel_captive_portal_error").html("Please Enter Number of session");
	 return false;
      }else{
         num_of_session_per_day = $("#cp1_no_of_daily_session_other").val();
      }
      location_portal = 'cp_retail';
    
	 if ($('#cp_cafe_plan').val() == '') {
	    $("#add_hotel_captive_portal_error").html("Please Select plan");
	       return false;
	 }else{
	    cp_cafe_plan_id = $('#cp_cafe_plan').val();
	 }
	 save_time_plan_other_location_cafe("yes");
      
      
      
      if ($('input[name="is_socialloginenable"]').is(':checked')) {
         is_socialloginenable = $("input[name='is_socialloginenable']:checked").val();
      }
      if ($('input[name="is_wifidisabled"]').is(':checked')) {
         is_wifidisabled = $("input[name='is_wifidisabled']:checked").val();
      }
      var login_with_mobile_email = $("input[name='login_with_mobile_email_notpublic']:checked").val();
      offer_redemption_mode = $("input[name='offer_redemption_mode']:checked").val();
      
      
   }
   
   else if (location_type == '8') {
      location_portal = 'cp_custom';
      if ($("#custom_captive_url").val() == '') {
         $("#add_hotel_captive_portal_error").html("Please Captive portal url");
	 return false;
      }else{
         custom_captive_url = $("#custom_captive_url").val();
      }
      if ($('#custom_location_plan').val() == '') {
	 $("#add_hotel_captive_portal_error").html("Please Select plan");
	    return false;
      }else{
	 custom_location_plan = $('#custom_location_plan').val();
      }
      
      custon_cp_signip_data = $('#custon_cp_signip_data').val();
      custon_cp_daily_data = $('#custon_cp_daily_data').val();
      custon_cp_no_of_daily_session = $('#custon_cp_no_of_daily_session').val();
      save_custom_location_plan("yes");
   }
   
   else if (location_type == '9') {
      if ($("#cp1_no_of_daily_session_other").val() == '') {
         $("#add_hotel_captive_portal_error").html("Please Enter Number of session");
	 return false;
      }else{
         num_of_session_per_day = $("#cp1_no_of_daily_session_other").val();
      }
      location_portal = 'cp_purple';
      
	 if ($('#cp_cafe_plan').val() == '') {
	    $("#add_hotel_captive_portal_error").html("Please Select plan");
	       return false;
	 }else{
	    cp_cafe_plan_id = $('#cp_cafe_plan').val();
	 }
	 save_time_plan_other_location_cafe("yes");
      
      if ($('input[name="is_socialloginenable"]').is(':checked')) {
         is_socialloginenable = $("input[name='is_socialloginenable']:checked").val();
      }
      if ($('input[name="is_pinenable"]').is(':checked')) {
         is_pinenable = $("input[name='is_pinenable']:checked").val();
      }
      var login_with_mobile_email = $("input[name='login_with_mobile_email_notpublic']:checked").val();
      
      offer_redemption_mode = $("input[name='offer_redemption_mode']:checked").val();
   }
   else if (location_type == '10') {
      location_portal = 'cp_offline';
      synk_frequency = $("input[name='offline_sync_frequency']:checked").val();
      offline_cp_type = $("input[name='offline_cp_content_type']:checked").val();
      offline_cp_source_folder_path = $("#cp_source_folder_path").val();
      offline_cp_landing_page_path = $("#cp_landing_path_path").val();
      if (offline_cp_type == '2') {
	 if (offline_cp_source_folder_path == '' || offline_cp_landing_page_path == '') {
	    $("#add_hotel_captive_portal_error").html("Please Captive portal url");
	    return false;
	 }
	 
      }
      if (offline_cp_type == '2') {
	 is_offline_registration = 0
      }else{
	 if ($('input[name="enable_offline_dynamic_registration"]').is(':checked')) {
	    is_offline_registration = $("input[name='enable_offline_dynamic_registration']:checked").val();
	 }
      }
      
      var background_image_name = '';
      var background_image_src = '';
      background_image_src = $('input[name="thumb_background_offline_values"]').val();
      if (typeof background_image_src != "undefined") {
	 background_image_name = $('input[name="thumb_background_offline_values"]').next().next().attr('name');
	 thumb_background_values = $('input[name="thumb_background_offline_values"]').val();
      }else{
	 background_image_src = '';
      }
      
   }
   else if (location_type == '11' || location_type == '18') {
      // for logo2 (logo 2 save in slider 1 column)
      slider1_image_src = $('input[name="thumb2_values"]').val();
      if (typeof slider1_image_src != "undefined") {
	 slider1_image_name = $('input[name="thumb2_values"]').next().next().attr('name');
	 thumb_slider1_values = $('input[name="thumb2_values"]').val();
      }else{
	 slider1_image_src = '';
      }
      
       if (location_type==18) {
        location_portal = 'cp_eventguest';
      }else{
         location_portal = 'cp_visitor';
      }
      synk_frequency = $("input[name='offline_sync_frequency']:checked").val();
      offline_cp_type = '1';// dynamic
      offline_cp_source_folder_path = '';//static
      offline_cp_landing_page_path = '';//static
      if (offline_cp_type == '2') {
	 if (offline_cp_source_folder_path == '' || offline_cp_landing_page_path == '') {
	    $("#add_hotel_captive_portal_error").html("Please Captive portal url");
	    return false;
	 }
	 
      }
      is_offline_registration = 0
      
      
      
      var background_image_name = '';
      var background_image_src = '';
      background_image_src = $('input[name="thumb_background_offline_vm_values"]').val();
      if (typeof background_image_src != "undefined") {
	 background_image_name = $('input[name="thumb_background_offline_vm_values"]').next().next().attr('name');
	 thumb_background_values = $('input[name="thumb_background_offline_vm_values"]').val();
      }else{
	 background_image_src = '';
      }
      
   }
   else if (location_type == '12') {
      
      location_portal = 'cp_contestifi';
      synk_frequency = $("input[name='offline_sync_frequency']:checked").val();
      offline_cp_type = '1';// dynamic
      offline_cp_source_folder_path = '';//static
      offline_cp_landing_page_path = '';//static
      
      if ($('input[name="is_signup_enable"]').is(':checked')) {
	    is_offline_registration = $("input[name='is_signup_enable']:checked").val();
      }
      contestifi_contest_header = $('input[name="contestifi_header_text"]').val();
      if (contestifi_contest_header == '')
      {
	  $("#add_hotel_captive_portal_error").html("Please Enter Header Text");
	    return false;
      }
      var login_with_mobile_email = $("input[name='login_with_mobile_email_notpublic']:checked").val();
      if ($('input[name="signup_user_name_require"]').is(':checked')) {
         user_name_required = $("input[name='signup_user_name_require']:checked").val();
      }
      if ($('input[name="signup_user_email_mobile_require"]').is(':checked')) {
         user_email_required = $("input[name='signup_user_email_mobile_require']:checked").val();
      }
      if ($('input[name="signup_user_age_require"]').is(':checked')) {
         user_age_required = $("input[name='signup_user_age_require']:checked").val();
      }
      if ($('input[name="signup_user_gender_require"]').is(':checked')) {
         user_gender_required = $("input[name='signup_user_gender_require']:checked").val();
      }
   }
    else if (location_type == '13')
   {
    tatdays = $("#tatdays").val();
	location_portal = 'cp_retail_audit';
	 var background_image_name = '';
	  var background_image_src = '';
	  background_image_src = $('input[name="thumb_background_offline_values"]').val();
	  if (typeof background_image_src != "undefined") {
	     background_image_name = $('input[name="thumb_background_offline_values"]').next().next().attr('name');
	     thumb_background_values = $('input[name="thumb_background_offline_values"]').val();
	  }else{
	     background_image_src = '';
	  }
   }
    else if (location_type == '14') {
	location_portal = 'cp_event_module';
	offline_cp_type = '1';// dynamic
	offline_cp_source_folder_path = '';//static
	offline_cp_landing_page_path = '';//static
	var background_image_name = '';
	var background_image_src = '';
	background_image_src = $('input[name="thumb_background_offline_vm_values"]').val();
	if (typeof background_image_src != "undefined")
	{
	    background_image_name = $('input[name="thumb_background_offline_vm_values"]').next().next().attr('name');
	    thumb_background_values = $('input[name="thumb_background_offline_vm_values"]').val();
	}
	else
	{
	    background_image_src = '';
	}
	if ($('input[name="is_live_chat_enable"]').is(':checked')) {
	    is_live_chat_enable = $("input[name='is_live_chat_enable']:checked").val();
	}
	if ($('input[name="is_discuss_enable"]').is(':checked')) {
	    is_discuss_enable = $("input[name='is_discuss_enable']:checked").val();
	}
	if ($('input[name="is_information_enable"]').is(':checked')) {
	    is_information_enable = $("input[name='is_information_enable']:checked").val();
	}
	if ($('input[name="is_poll_enable"]').is(':checked')) {
	    is_poll_enable = $("input[name='is_poll_enable']:checked").val();
	}
	if ($('input[name="is_download_enable"]').is(':checked')) {
	    is_download_enable = $("input[name='is_download_enable']:checked").val();
	}
	
	if ($('input[name="event_module_signup_user_name_require"]').is(':checked')) {
	    event_module_signup_user_name_require = $("input[name='event_module_signup_user_name_require']:checked").val();
	}
	if ($('input[name="event_model_signup_user_organisation_require"]').is(':checked')) {
	    event_model_signup_user_organisation_require = $("input[name='event_model_signup_user_organisation_require']:checked").val();
	}
	if ($('input[name="event_module_signup_nick_name"]').is(':checked')) {
	    event_module_signup_nick_name = $("input[name='event_module_signup_nick_name']:checked").val();
	}
	if ($('input[name="event_module_signup_email"]').is(':checked')) {
	    event_module_signup_email = $("input[name='event_module_signup_email']:checked").val();
	}
	if ($('input[name="event_module_signup_phone"]').is(':checked')) {
	    event_module_signup_phone = $("input[name='event_module_signup_phone']:checked").val();
	}
    }
    else if (location_type == '16') {
	location_portal = 'cp_lms';
	var background_image_name = '';
	  var background_image_src = '';
	  background_image_src = $('input[name="thumb_background_offline_values"]').val();
	  if (typeof background_image_src != "undefined") {
	     background_image_name = $('input[name="thumb_background_offline_values"]').next().next().attr('name');
	     thumb_background_values = $('input[name="thumb_background_offline_values"]').val();
	  }else{
	     background_image_src = '';
	  }
    }
   
   
   $("#add_hotel_captive_portal_error").html("");
   //var plan_type = $("input[name='cp_hotel_plan_type']:checked").val();
   var plan_type = '';
   //var file_data = $("#exampleInputFile").prop("files")[0];
   var form_data = new FormData();
   //form_data.append("file", file_data);
   form_data.append('image_name',image_name);
   form_data.append('image_src',image_src);
    form_data.append('background_image_name',background_image_name);
   form_data.append('background_image_src',background_image_src);
   form_data.append('slider1_image_name',slider1_image_name);
   form_data.append('slider1_image_src',slider1_image_src);
   form_data.append('slider2_image_name',slider2_image_name);
   form_data.append('slider2_image_src',slider2_image_src);
   form_data.append("location_uid", $("#location_id_hidden").val());
   form_data.append("locationid", $("#created_location_id").val());
   form_data.append("cp_type", location_portal);
   form_data.append("cp_home_selected_plan_id", cp_home_selected_plan_id);
   form_data.append("cp_hotel_plan_type", plan_type);
   form_data.append("cp_enterprise_user_type", enterprise_user_type);
   form_data.append("institutional_user_list", institutional_user_list);
   form_data.append("main_ssid", main_ssid);
   form_data.append("guest_ssid", guest_ssid);
   form_data.append("cp_cafe_plan_id", cp_cafe_plan_id);
   form_data.append("is_otpdisabled", is_otpdisabled);
   form_data.append("num_of_session_per_day", num_of_session_per_day);
   form_data.append("icon_color", icon_color);
   form_data.append("is_socialloginenable", is_socialloginenable);
   form_data.append("is_pinenable", is_pinenable);
   form_data.append("is_wifidisabled", is_wifidisabled);
   form_data.append("login_with_mobile_email", login_with_mobile_email);
   form_data.append("offer_redemption_mode", offer_redemption_mode);
   
   form_data.append("custom_captive_url", custom_captive_url);
   form_data.append("custom_location_plan", custom_location_plan);
   form_data.append("custon_cp_signip_data", custon_cp_signip_data);
   form_data.append("custon_cp_daily_data", custon_cp_daily_data);
   form_data.append("custon_cp_no_of_daily_session", custon_cp_no_of_daily_session);
   
   form_data.append("synk_frequency", synk_frequency);
   form_data.append("offline_cp_type", offline_cp_type);
   form_data.append("offline_cp_source_folder_path", offline_cp_source_folder_path);
   form_data.append("offline_cp_landing_page_path", offline_cp_landing_page_path);
   form_data.append("is_offline_registration", is_offline_registration);
   form_data.append("contestifi_contest_header", contestifi_contest_header);
   form_data.append("user_name_required", user_name_required);
   form_data.append("user_email_required", user_email_required);
   form_data.append("user_age_required", user_age_required);
   form_data.append("user_gender_required", user_gender_required);
   form_data.append("is_white_theme", is_white_theme);
   form_data.append("is_live_chat_enable", is_live_chat_enable);
   form_data.append("is_discuss_enable", is_discuss_enable);
   form_data.append("is_information_enable", is_information_enable);
   form_data.append("is_poll_enable", is_poll_enable);
   form_data.append("is_download_enable", is_download_enable);
   form_data.append("event_module_signup_user_name_require", event_module_signup_user_name_require);
   form_data.append("event_model_signup_user_organisation_require", event_model_signup_user_organisation_require);
   form_data.append("event_module_signup_nick_name", event_module_signup_nick_name);
   form_data.append("event_module_signup_email", event_module_signup_email);
   form_data.append("event_module_signup_phone", event_module_signup_phone);
   form_data.append("is_preaudit_disable", is_preaudit_disable);
   form_data.append("audit_ticket_email_send", audit_ticket_email_send);
   form_data.append("tatdays", tatdays);
   $(".loading").show();
   $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_hotel_captive_portal",
         cache: false,
        contentType: false,
        processData: false,
        data: form_data,   
	 success: function(data){
	    $(".loading").hide();
	    if (Clicked_ButtonValue == 'exit') {
		window.location = "<?php  echo base_url()?>location";
	    }else{
	       if (offline_cp_type == '1' && location_portal != 'cp_contestifi') {
		  $("#content_builder_tab_li").show();
		  content_builder();
		  mui.tabs.activate('content_builder');
	       }else{
		    if (location_portal == 'cp_retail_audit') {
			$("#content_builder_tab_li").show();
			content_builder();
			mui.tabs.activate('content_builder');
		    }
		    else
		    {
			    $("#content_builder_tab_li").hide();
			 if (location_portal == 'cp_contestifi')
			 {
			    contestifi_content_builder();
			    $("#contestifi_content_builder_tab_li").show();
			    mui.tabs.activate('contestifi_content_builder');
			 }
			 else if (location_portal == 'cp_lms') {
			    // nothing to done
			 }
			 else
			 {
			    mui.tabs.activate('nas_setup');
			 }
		    }
		 
		  
	       }
	       // go to next tab
		//$("#captive_portal_tab_checked").show();
	       
	       var location_uid =  $("#location_id_hidden").val();
	       nas_setup_list(location_uid);
	       if (location_type == '6' || location_type == '7') {
		  save_time_plan_other_location_cafe("yes");
	       }
	       get_location_offer_image_path();
	       get_location_cp_path();
	    }
   	
	 }
         });
       }
       
   function get_location_offer_image_path() {
      var location_uid =  $("#location_id_hidden").val();
      $.ajax({
               type: "POST",
                url: "<?php echo base_url()?>location/current_image_path",
		data: "location_uid="+location_uid,
		dataType: 'json',
		success: function(data){
                  if (data.code == '1') {
                     if (data.logo_image != '') {
                        $('.location_logo_preview_show').attr('src', '');
			d = new Date();
			$('.location_logo_preview_show').attr('src', data.logo_image+"?"+d.getTime());
                     }
                     if (data.offer_image != '') {
                        $('.defult_offer_image_preview_show').attr('src', data.offer_image);
                     }
		    if (data.slider1_image != '') {
			$('#custom_slider_image1_remove_button').show();
                        $('.slider1_image_preview_show').attr('src', data.slider1_image);
                     }
		     if (data.slider2_image != '') {
			$('#custom_slider_image2_remove_button').show();
                        $('.slider2_image_preview_show').attr('src', data.slider2_image);
                     }
		    
                  }
               }
        });
   }
       
        // on nas tab click
    function nas_setup_list() {
       var location_uid =  $("#location_id_hidden").val();
       var location_type = $("#location_type").val();
        $(".loading").show();
       $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/nas_setup_list",
        data: "location_uid="+location_uid+"&location_type="+location_type,
        success: function(data){
   	$(".loading").hide();
         $("#nas_setup_data").html(data);
          access_point_list();
        }
         });
    }
        // router type chage get nas list
   $(document).on('change', "#router_type", function(){
      var router_type = $(this).val();
      $(".loading").show();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>location/nas_list",
          data: "router_type="+router_type,
          success: function(data){
          $("#nas").html(data);
          $(".loading").hide();
          }
      });
      if (router_type == '1') {
         $("#microtic_button").show();
         $("#else_router").hide();
      }else{
         $("#microtic_button").hide();
         $("#else_router").show();
      }
     // show and hide onehot router setup button
      $(".router_setup_detail_info").hide();
   });
   
   function configure_nas() {
      
      if(Clicked_ButtonValue == 'microtic'){
         $("#nas_select_erroe").html("");
         var nas = $("#nas").val();
	 var nasid = $('#nas').find(':selected').data('nasid');
         if (nas == '') {
            $("#nas_select_erroe").html("please select nas");
            return false
         }else{
            // check cp selected or not
            var location_uid =  $("#created_location_id").val();
            $(".loading").show();
            $.ajax({
               type: "POST",
               url: "<?php echo base_url()?>location/check_cp_selected_for_location",
               data: "location_uid="+location_uid,
               success: function(data){
                  if (data == '0') {
                     $("#nas_select_erroe").html("First please select Captive portal");
                  }else{
                     $("#nas_id_selected").val(nas);
		     $("#router_ip").val(nasid);
                     $("#add_location_modal").modal("show");
                  }
                  $(".loading").hide();	
               }
            });
         }
         }else if(Clicked_ButtonValue == 'exit_router_tab'){
            $(".loading").show();
            window.location = "<?php  echo base_url()?>location";
         }
      else{
         $("#nas_select_erroe").html("");
         var router_type = $("#router_type").val();
         if (router_type == '7') {
	    update_onehop_files();
	 }else{
	    create_onehop_network_view();
	 }
         /*var router_type = $("#router_type").val();
         var nasid = $("#nas").val();
         var location_uid =  $("#created_location_id").val();
         $(".loading").show();
         $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>location/update_other_router_with_nas_location",
            data: "location_uid="+location_uid+"&nasid="+nasid+"&router_type="+router_type,
            success: function(data){
               if (data == '0') {
                  $("#nas_select_erroe").html("First please select Captive portal");
               }else{
                  $("#else_router").hide();
                  $("#nas_select_erroe").html("Successfully Setup NAS");
                  nas_setup_list($("#location_id_hidden").val());
               }
               $(".loading").hide();	
            }
         });*/
      }
   }
   function start_configuration() {
      $("#router_connection_error").val("");
      var router_ip = $("#router_ip").val();
      var router_user = $("#router_user").val();
      var router_password = $("#router_password").val();
      var router_port = $("#router_port").val();
      var nasid = $("#nas_id_selected").val();
      //var location_id = $("#location_id_hidden").val();
      var location_id = $("#created_location_id").val();
      $(".loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>location/start_configuration",
         data: "router_ip="+router_ip+"&router_user="+router_user+"&router_password="+router_password+"&router_port="+router_port+"&nasid="+nasid+"&location_id="+location_id,
         success: function(data){
            $(".loading").hide();
            if (data == '') {
               $("#add_location_modal").modal("hide");
               $("#connection_type_modal").modal("show");
            }else{
               $("#router_connection_error").html("router not connect");
            }
         }
      });
   }
   function start_configuration1() {
      $("#router_connection_error1").html("");
      var router_type_wap = $("#router_type_wap").val();
      $(".loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>location/start_configuration1",
         data: "router_type_wap="+router_type_wap,
         success: function(data){
            $(".loading").hide();
            if (data == 1) {
               $("#is_router_reset_or_not_error").html("");
               $("#connection_type_modal").modal("hide");
               $("#is_router_reset_or_not").modal("show");
            }else{
               $("#router_connection_error1").html("Selected router not matched. Please select correct router type");
            }
         }
      });
   }
   function reset_wap_router() {
      $("#is_router_reset_or_not_error").html("");
      var router_type_wap = $("#router_type_wap").val();
      $(".loading").show();
      $(".loading").hide();
            $("#is_router_reset_or_not").modal("hide");
            $("#add_location_modal").modal("show");
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>location/reset_router",
         data: "router_type="+router_type_wap,
         success: function(data){
            $(".loading").hide();
            $("#is_router_reset_or_not").modal("hide");
            $("#add_location_modal").modal("show");
         }
      });
   }
   function check_wap_router_reset_porperly() {
      $("#is_router_reset_or_not_error").html("");
      var router_type_wap = $("#router_type_wap").val();
      $(".loading").show();
      $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>location/check_wap_router_reset_porperly",
            data: "router_type="+router_type_wap,
            success: function(data){
        
        if (data == 1) {
          
           start_configuration2();
        }else if(data == 2){
         $(".loading").hide();
           $("#is_router_reset_or_not_error").html("Your router is not properly reset please first reset properly");
        }else if (data == 3) {
         $(".loading").hide();
           $("#is_router_reset_or_not_error").html("Selected router not matched. Please select correct router type");
        }else if (data == 4) {
         $(".loading").hide();
           $("#is_router_reset_or_not_error").html("WAN set on wrong port set WAN on ether1");
        }else{
         $(".loading").hide();
           $("#is_router_reset_or_not_error").html("Some error occure");
        }
        
            }
      });
   }
   function start_configuration2() {
      $(".loading").show();
      $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>location/start_configuration2",
         data: "configure="+"start",
         success: function(data){
         $(".loading").hide();
             $("#is_router_reset_or_not").modal("hide");
            $("#done_configuration").modal("show");
            //get setup microtic data
            var location_uid =  $("#location_id_hidden").val();
            nas_setup_list(location_uid);
         }
      });
   }
   
   function access_point_list(){
	 
	  get_ap_location();
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
		 type: "POST",
		 url: "<?php echo base_url()?>location/location_access_point_list",
		 data: "location_uid="+location_uid,
		 success: function(data){
		  $(".loading").hide();
		  $("#access_point_list").html(data);
		 
			$('.loc').each(function(){
			   var location_uid = $(this).data('locuid');
			   var ip_address = $(this).data('macid');
			   var router_type = $(this).data('routertype');
			   var ipadd = $(this).data('ipadd');
			   var raspsat=$(this).data('raspstat');
			   //alert(ip_address);
			   //alert(router_type);
			   
			   if(router_type==6){
				 ping_onehop(ip_address, location_uid, ipadd);// onehop  
			   }else if(router_type==7){
				  if(raspsat==0) {
					   var active="<img src='"+base_url+"assets/images/red.png'> <span class='viewras' style='cursor:pointer;' data-macid='"+ip_address+"' data-routertype='"+router_type+"' data-locuid='"+location_uid+"'><i class='fa fa-eye fa-lg'></i></span>";
						$('.cla_'+ipadd).html(active); 
				  }					  
			   }else{
				   var active="<img src='"+base_url+"assets/images/red.png'>";
			      $('.cla_'+ipadd).html(active); 
			   }
			   
			});
		 
		 
		 }
		  });
   }
   
   function ping_onehop(ip_address,location_uid, ipadd) {
	 $.ajax({
                     type:"POST",
                     url: "<?php echo base_url()?>location/ping_onehop_ap_wise",
                     cache:false,
                     dataType: 'json',
                     data:"macid="+ip_address+"&location_uid="+location_uid,
                     success: function(data){
			var active="<img src='"+base_url+"assets/images/green.png'>";
			var inactive="<img src='"+base_url+"assets/images/red.png'>";
                        if (data.code == '1') {
			  $('.cla_'+ipadd).html(active);
			}else{
			  $('.cla_'+ipadd).html(inactive);
			}
			
                       
                     },
                     error: function(error){
                        //$("#loading").hide();
                     }
                  });
   }
   function get_ap_location(ap_location_type = '') {
	    // get ap location list
	  $.ajax({
		 type: "POST",
		 url: "<?php echo base_url()?>location/ap_location_list",
		 data: "ap_location_type="+ap_location_type,
		 success: function(data){
		 $(".loading").hide();
		  $("#add_ap_location").html(data);
		  $("#edit_ap_location").html(data);
		 }
	 });
   }
   function add_ap_location_model(){
	 $("#add_ap_location_modal").modal("show");
   }
   function add_new_ap_location(){
	 $("#add_ap_location_error").html("");
	 var ap_location_name = $("#ap_location_name_popup").val();
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_ap_location",
	    data: "ap_location_name="+ap_location_name,
	    success: function(data){
	       $(".loading").hide();
	       if (data == '1') {
		  get_ap_location();
		  $("#add_ap_location_modal").modal("hide");
	       }else{
		  $("#add_ap_location_error").html("AP Location Name already exist");
	       }
	       
	    }
         });
       }
   function ValidateIPaddress(ipaddress){
       if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))
       {
   return true;
       }
       else
       {
   return false;
   }
    }
   function add_location_access_point(){
      var location_uid =  $("#location_id_hidden").val();
      var access_point_ssid =  '';
      var access_point_hotspot_type =  '';
      var access_point_macid =  $("#access_point_macid").val();
     
      var access_point_ip = "0.0.0.0";
      var ap_location_type = $("#add_ap_location").val();
      if (access_point_macid == '' || ap_location_type == '') {
         $("#add_access_point_errro").html("please fill all field");
         return false;
      }
      if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(access_point_ip))
      {
         $("#add_access_point_errro").html("");
         $(".loading").show();
         $.ajax({
           type: "POST",
           url: "<?php echo base_url()?>location/add_location_access_point",
           data: "location_uid="+location_uid+"&access_point_ssid="+access_point_ssid+"&access_point_hotspot_type="+access_point_hotspot_type+"&access_point_macid="+access_point_macid+"&access_point_ip="+access_point_ip+"&ap_location_type="+ap_location_type,
           success: function(data){
            //if (data == '1') {
            if (data == '') {
               $("#access_point_ssid").val('');
               $("#access_point_macid").val('');
               access_point_list(location_uid);
            }else{
               //$("#add_access_point_errro").html("MAC ID already exist");
               $("#add_access_point_errro").html(data);
                  $(".loading").hide();
            }
           }
         });
   }else{
      $("#add_access_point_errro").html("please enter valid ip");
   }
    
       }
   function edit_access_point(ap_location_type,access_point_id, macid, hotspot_type, ssid,ip) {
	 get_ap_location(ap_location_type);
      $("#access_point_id_edit").val(access_point_id);
      //$("#access_point_ssid_edit").val(ssid);
       //$("#access_point_ip_edit").val(ip);
      //$("#voucher_data_type_edit").val(voucher_data_type);
      //$('input[name="access_point_hotspot_type_edit"][value="' + hotspot_type + '"]').prop("checked", true);
      $("#access_point_macid_edit").val(macid);
      $("#EditaccesspointModal").modal("show");
   }
    function update_location_access_point(){
   var location_uid =  $("#location_id_hidden").val();
   var access_point_id =  $("#access_point_id_edit").val();
   
   var access_point_ssid =  '';
   var access_point_hotspot_type =  '';
   /*var access_point_ssid =  $("#access_point_ssid_edit").val();
   var access_point_hotspot_type =  $("input[name='access_point_hotspot_type_edit']:checked").val();*/
   var access_point_macid =  $("#access_point_macid_edit").val();
   var access_point_ip =  $("#access_point_ip_edit").val();
   var access_point_ip = '0.0.0.0';
   var ap_location_type = $("#edit_ap_location").val();
   if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(access_point_ip))
   {
         $("#edit_access_point_errro").html("");
         $(".loading").show();
         $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_location_access_point",
        data: "location_uid="+location_uid+"&access_point_ssid="+access_point_ssid+"&access_point_hotspot_type="+access_point_hotspot_type+"&access_point_macid="+access_point_macid+"&access_point_id="+access_point_id+"&access_point_ip="+access_point_ip+"&ap_location_type="+ap_location_type,
        success: function(data){
   	if (data == '') {
	       $("#EditaccesspointModal").modal("hide");
	       access_point_list(location_uid);
	    }else{
	       $("#edit_access_point_errro").html(data);
	       $(".loading").hide();
	    }
        }
         });
   }
   else
   {
      $("#edit_access_point_errro").html("Please enter valid ip");
   }
    
       }
       
   function create_onehop_network_view(){
      var location_uid = $("#location_id_hidden").val();
      $("#create_onehop_network_status").html('');
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_get_network_detail",
   	    data: "location_uid="+location_uid,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	       $("#onehop_network_name").val(data.network_name);
	       $("#onehop_description").val(data.description);
	       $("#onehop_maxaps").val(data.maxaps);
	       $("#onehop_coa_status").val(data.coa_status);
	       $("#onehop_coa_ip").val(data.coa_ip);
	       $("#onehop_coa_secret").val(data.coa_secret);
	       $("#onehop_address").val(data.address);
	       $("#onehop_latitude").val(data.latitude);
	       $("#onehop_longitude").val(data.longitude);
	       $("#onehop_is_new_network").val(data.is_created);
	        $("#onehop_is_new_network").val(data.is_created);
	       $("#onehop_beacon_status").val(data.beacon_status);
	       $("#onehop_beacon_url").val(data.beacon_url);
	       if (data.is_created == '1') {//modify
		  $('#create_network_button').hide();
		  $("#update_network_button").show();
		  $("#create_onehop_netword").modal("show");
	       }else{
		  $('#create_network_button').show();
		  $("#update_network_button").hide();
		  ask_onehop_network_type();
		  //$("#create_onehop_netword").modal("show");
	       }
	       //$("#create_onehop_netword").modal("show");
   	    }
      });
	 
   }
   function ask_onehop_network_type() {
      $("#ask_onehop_network_type_modal").modal("show");
   }
   function ask_onehop_network_type_next() {
      var network_type_select = $("input[name='is_create_new_network']:checked").val();
      
      if (network_type_select == '1') {
	 var location_type = $("#location_type").val();
	 $(".loading").show();
	 $.ajax({
	     type: "POST",
	     url: "<?php echo base_url()?>location/get_onehop_existing_network",
	     data: "location_type="+location_type,
	     success: function(data){
	       $("#previous_network").html(data);
	       $("#select_previous_network_model").modal("show");
	     $(".loading").hide();
	    }
	});
	 
      }else{
	 $("#create_onehop_netword").modal("show");
      }
      $("#ask_onehop_network_type_modal").modal("hide");
   }
   function select_previous_network() {
      var location_uid = $("#location_id_hidden").val();
      var previous_network_id = $("#previous_network").val();
      if (previous_network_id != '') {
	 $(".loading").show();
	 $.ajax({
	     type: "POST",
	     url: "<?php echo base_url()?>location/use_existing_onehop_network",
	     data: "previous_network_id="+previous_network_id+"&location_uid="+location_uid,
	     success: function(data){
	       $("#create_onehop_network_status").html('');
	       $("#select_previous_network_model").modal("hide");
	       //nas_setup_list(location_uid);
               update_onehop_files();
	    }
	});
      }
      
   }
   function create_onehop_network(){
      var location_uid = $("#location_id_hidden").val();
      var network_name = $("#onehop_network_name").val();
      var description = $("#onehop_description").val();
      var maxaps = $("#onehop_maxaps").val();
      var coa_status = $("#onehop_coa_status").val();
      var coa_ip = $("#onehop_coa_ip").val();
      var coa_secret = $("#onehop_coa_secret").val();
      var address = $("#onehop_address").val();
      var latitude = $("#onehop_latitude").val();
      var longitude = $("#onehop_longitude").val();
      var is_created = $("#onehop_is_new_network").val();
      var service_name = 'NetworkCreate';
      var beacon_status = $("#onehop_beacon_status").val();
      if (is_created == '1') {
	 service_name = 'NetworkModify';
      }
      if (coa_status == '1') {
	 if (coa_ip == '' || coa_secret == '') {
	    $("#create_onehop_network_status").html("coa IP and coa secret can not be blank");
	    return false;
	 }
      }
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_get_network_create",
   	    data: "location_uid="+location_uid+"&network_name="+network_name+"&description="+description+"&maxaps="+maxaps+"&coa_status="+coa_status+"&coa_ip="+coa_ip+"&coa_secret="+coa_secret+"&address="+address+"&latitude="+latitude+"&longitude="+longitude+"&service_name="+service_name+"&beacon_status="+beacon_status,
	    dataType: 'json',
   	    success: function(data){
	       
	       if (data.code == '1') {
		  $("#create_onehop_network_status").html('');
		  var location_uid = $("#location_id_hidden").val();
		  $("#create_onehop_netword").modal("hide");
		  //nas_setup_list(location_uid);
                  update_onehop_files();
	       }else{
		  $(".loading").hide();
		   $("#create_onehop_network_status").html(data.msg);
	       }
	       
	       
   	    }
      });
   }
   function update_onehop_files() {
      var router_type = $("#router_type").val();
      var nasid = $("#nas").val();
      var location_uid =  $("#created_location_id").val();
      $(".loading").show();
      $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>location/update_other_router_with_nas_location",
            data: "location_uid="+location_uid+"&nasid="+nasid+"&router_type="+router_type,
            success: function(data){
               if (data == '0') {
                  $("#create_onehop_network_status").html("First please select Captive portal");
               }else{
                  nas_setup_list($("#location_id_hidden").val());
               }	
            }
         });
   }
   function add_onehop_ssid_view() {
      onehop_get_ssid_detail(0);
      $("#add_onehop_ssid").modal("show");
   }
   function onehop_get_ssid_detail(index_number){ 
      //var index_number = $(this).val();
      var location_uid = $("#location_id_hidden").val();
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_get_ssid_detail",
   	    data: "location_uid="+location_uid+"&index_number="+index_number,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	       if (data.resultCode == '1') {
		  $("#onehop_ssid_name").val(data.ssid_name);
		  $("#onehop_association").val(data.association);
		  $("#onehop_wap_mode").val(data.wap_mode);
		  $("#onehop_forwarding").val(data.forwarding);
		  $("#onehop_cp_mode").val(data.cp_mode);
		  $("#onehop_cp_url").val(data.cp_url);
		  $("#onehop_auth_server_ip").val(data.auth_server_ip);
		  $("#onehop_auth_server_port").val(data.auth_server_port);
		  $("#onehop_auth_server_secret").val(data.auth_server_secret);
		  $("#onehop_accounting").val(data.accounting);
		  $("#onehop_acc_server_ip").val(data.acc_server_ip);
		  $("#onehop_acc_server_port").val(data.acc_server_port);
		  $("#onehop_acc_server_secret").val(data.acc_server_secret);
		  $("#onehop_acc_interval").val(data.acc_interval);
		  $("#onehop_psk").val(data.psk);
		  //alert(data.wall_list.length);
		  $('.remove_wall_column').parent().remove();
		  for (var i = 0; i < data.wall_list.length; i++ ) {
		     var ip = data.wall_list[i]['ip'];
                     $("#wall_garder_ip_div").append('<span class="chip">'+ip+'<input type="hidden" name = "wall_garder_ip[]" value = "'+ip+'"><i class="close fa fa-times remove_wall_column" aria-hidden="true"></i></span>');
		  }
		  $('.remove_wall_column_domain').parent().remove();
		  for (var i = 0; i < data.wall_domain_list.length; i++ ) {
		     var domain = data.wall_domain_list[i]['domain'];
		    
                     $("#wall_garder_domain_div").append('<span class="chip">'+domain+'<input type="hidden" name = "wall_garder_domain[]" value = "'+domain+'"><i class="close fa fa-times remove_wall_column_domain" aria-hidden="true"></i></span>');
		  }
	       }else{
		  $("#onehop_ssid_name").val(data.ssid_name);
		  $("#onehop_association").val('0');
		  $("#onehop_psk").val("");
		  $("#onehop_wap_mode").val('3');
		  $("#onehop_forwarding").val('1');
		  $("#onehop_cp_mode").val('2');
		  $("#onehop_cp_url").val("");
		  $("#onehop_auth_server_ip").val(data.auth_server_ip);
		  $("#onehop_auth_server_port").val("1812");
		  $("#onehop_auth_server_secret").val("testing123");
		  $("#onehop_accounting").val('1');
		  $("#onehop_acc_server_ip").val(data.acc_server_ip);
		  $("#onehop_acc_server_port").val('1813');
		  $("#onehop_acc_server_secret").val('testing123');
		  $("#onehop_acc_interval").val('60');
		  $("#onehop_psk").val(data.psk);
		   $('.remove_wall_column').parent().remove();
		   $('.remove_wall_column_domain').parent().remove();
		   
		  
                  $('.remove_wall_column').parent().remove();
		  for (var i = 0; i < data.wall_list.length; i++ ) {
		     var ip = data.wall_list[i]['ip'];
                     $("#wall_garder_ip_div").append('<span class="chip">'+ip+'<input type="hidden" name = "wall_garder_ip[]" value = "'+ip+'"><i class="close fa fa-times remove_wall_column" aria-hidden="true"></i></span>');
		  }
		  $('.remove_wall_column_domain').parent().remove();
		  for (var i = 0; i < data.wall_domain_list.length; i++ ) {
		     var domain = data.wall_domain_list[i]['domain'];
		    
                     $("#wall_garder_domain_div").append('<span class="chip">'+domain+'<input type="hidden" name = "wall_garder_domain[]" value = "'+domain+'"><i class="close fa fa-times remove_wall_column_domain" aria-hidden="true"></i></span>');
		  }
		  
		  
	       }
	       onehop_cp_mode(2);
   	    }
      });
   }
   function onehop_cp_mode(cp_mode_type) {
	
      var created_location_id = $("#created_location_id").val();
      //var cp_mode_type = $(this).val();
      
      if (cp_mode_type == '2') {
	 //$(".loading").show();
	 $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/get_isp_url",
   	    data: "created_location_id="+created_location_id,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	       $("#onehop_cp_url").val(data.url);
   	    }
	 });
      }else{
	 $("#onehop_cp_url").val("");
      }
   }
   function validate_ip(ip) {
      if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)){
	 return 1;
      }else{
	 return 0;
      }
   }
   function add_onehop_ssid(){
      var location_uid = $("#location_id_hidden").val();
      var onehop_ssid_index = $("#onehop_ssid_index").val();
      var onehop_ssid_name = $("#onehop_ssid_name").val();
      var onehop_association = $("#onehop_association").val();
      var onehop_wap_mode = $("#onehop_wap_mode").val();
      var onehop_forwarding = $("#onehop_forwarding").val();
      var onehop_cp_mode = $("#onehop_cp_mode").val();
      var onehop_cp_url = $("#onehop_cp_url").val();
      var onehop_auth_server_ip = $("#onehop_auth_server_ip").val();
      var onehop_auth_server_port = $("#onehop_auth_server_port").val();
      var onehop_auth_server_secret = $("#onehop_auth_server_secret").val();
      var onehop_accounting = $("#onehop_accounting").val();
      var onehop_acc_server_ip = $("#onehop_acc_server_ip").val();
      var onehop_acc_server_port = $("#onehop_acc_server_port").val();
      var onehop_acc_server_secret = $("#onehop_acc_server_secret").val();
      var onehop_acc_interval = $("#onehop_acc_interval").val();
      var onehop_psk = $("#onehop_psk").val();
      var onehop_wallgarden_ip_list = $("input[name='wall_garder_ip[]']").map(function(){
	 return $(this).val();
      }).get();
      var onehop_wallgarden_domain_list = $("input[name='wall_garder_domain[]']").map(function(){
	 return $(this).val();
      }).get();
      if (onehop_cp_mode == '2') {
	 if (onehop_cp_url == '') {
	    $("#add_onehop_ssid_status").html("Please enter CP url");
	    return false;
	 }else if(onehop_wallgarden_ip_list == ''){
	    $("#add_onehop_ssid_status").html("Please enter wall garder ip");
	    return false;
	 }else if(onehop_auth_server_ip == '') {
	    $("#add_onehop_ssid_status").html("Please enter auth server ip");
	    return false;
	 }else if (onehop_auth_server_secret == '') {
	    $("#add_onehop_ssid_status").html("Please enter auth server secret");
	    return false;
	 }
      }
      if (onehop_accounting == '1') {
	 if (onehop_acc_server_ip == '') {
	    $("#add_onehop_ssid_status").html("Please enter accounting server IP");
	    return false;
	 }
	 else if (onehop_acc_server_secret == '') {
	    $("#add_onehop_ssid_status").html("Please enter accounting server secret");
	    return false;
	 }
      }
      if (onehop_association == '1') {
	 if (onehop_psk == '') {
	     $("#add_onehop_ssid_status").html("Please enter Pre Shared Key");
	    return false;
	 }else if (onehop_psk.length < 8 || onehop_psk.length > 64) {
	    $("#add_onehop_ssid_status").html("Pre Shared Key should be between 8 to 32 character");
	    return false;
	 }
	 
      }
      //validation
      if (onehop_ssid_name.length > 32) {
	 $("#add_onehop_ssid_status").html("SSID Name should be < 32 character");
	 return false;
      }
      if (onehop_auth_server_ip != '') {
	 if(validate_ip(onehop_auth_server_ip) == '0'){
	    $("#add_onehop_ssid_status").html("Please enter valid Auth Server IP");
	    return false;
	 }
      }
      if (onehop_auth_server_secret.length > 32) {
	 $("#add_onehop_ssid_status").html("Auth Server Secret should be < 32 character");
	 return false;
      }
      if (onehop_acc_server_ip != '') {
	 if(validate_ip(onehop_acc_server_ip) == '0'){
	    $("#add_onehop_ssid_status").html("Please enter valid Accounting Server IP");
	    return false;
	 }
      }
      if (onehop_acc_server_secret.length > 32) {
	 $("#add_onehop_ssid_status").html("Accounting Server Secret should be < 32 character");
	 return false;
      }
      
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_add_ssid",
   	    data: "location_uid="+location_uid+"&onehop_ssid_index="+onehop_ssid_index+"&onehop_ssid_name="+onehop_ssid_name+"&onehop_association="+onehop_association+"&onehop_wap_mode="+onehop_wap_mode+"&onehop_forwarding="+onehop_forwarding+"&onehop_cp_mode="+onehop_cp_mode+"&onehop_cp_url="+onehop_cp_url+"&onehop_auth_server_ip="+onehop_auth_server_ip+"&onehop_auth_server_port="+onehop_auth_server_port+"&onehop_auth_server_secret="+onehop_auth_server_secret+"&onehop_accounting="+onehop_accounting+"&onehop_acc_server_ip="+onehop_acc_server_ip+"&onehop_acc_server_port="+onehop_acc_server_port+"&onehop_acc_server_secret="+onehop_acc_server_secret+"&onehop_acc_interval="+onehop_acc_interval+"&onehop_psk="+onehop_psk+"&onehop_wallgarden_ip_list="+onehop_wallgarden_ip_list+"&onehop_wallgarden_domain_list="+onehop_wallgarden_domain_list,
	    dataType: 'json',
   	    success: function(data){
	       if (data.code == '1') {
		  $("#add_onehop_ssid_status").html('');
		  var location_uid = $("#location_id_hidden").val();
		  $("#add_onehop_ssid").modal("hide");
		  nas_setup_list(location_uid);
	       }else{
		  $(".loading").hide();
		  $("#add_onehop_ssid_status").html(data.msg);
	       }
	      
   	    }
      });
   }
   
   $("#add_more_wallagrder").on("click",function(){
      add_domain_white_ip();
   });
   function add_domain_white_ip() {
      var value_enter = $("#wall_garder_ip_enter").val();
      var check = validate_ip(value_enter);
      if (check == '1') {
         $("#wall_garder_ip_div").append('<span class="chip">'+value_enter+'<input type="hidden" name = "wall_garder_ip[]" value = "'+value_enter+'"><i class="close fa fa-times remove_wall_column" aria-hidden="true"></i></span>');
         var value_enter = $("#wall_garder_ip_enter").val('');
      }
      
   }
   $(document).on('click', ".remove_wall_column", function(){
      $(this).parent().remove();
   });
   
   $("#add_more_wallagrder_domain").on("click",function(){
      add_domain_white_name();
   });
   function add_domain_white_name() {
      var value_enter = $("#wall_garder_domain_enter").val();
      $("#wall_garder_domain_div").append('<span class="chip">'+value_enter+'<input type="hidden" name = "wall_garder_domain[]" value = "'+value_enter+'"><i class="close fa fa-times remove_wall_column_domain" aria-hidden="true"></i></span>');
      var value_enter = $("#wall_garder_domain_enter").val('');
   }
   $(document).on('click', ".remove_wall_column_domain", function(){
      $(this).parent().remove();
   });
   
   
   
   
   function plan_detail(plan_id) {
       $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/plan_detail",
   	    data: "plan_id="+plan_id,
	    dataType: 'json',
   	    success: function(data){
                $(".loading").hide();
		var location_type = $("#location_type").val();
               if (data.code == '1') {
                  
                  if (data.plan_type == '4') {
		      //data plan
		     if (location_type == 8) {
			$("#custom_location_dataplan").show();
			$("#custom_location_timepla").hide();
		     }
                     $(".time_limit_div").addClass("disabled");
                     $(".data_limit_div").removeClass("disabled");
                     
                     $('.download_rate').val(data.download_rate);
                     $('.upload_rate').val(data.upload_rate);
                     $('.datalimit').val(data.datalimit);
                     $('.datalimit_checkbox').prop('checked', true);
                     $('.timelimit_checkbox').prop('checked', false);
                     $('.os-currentVal').html(data.timelimit);
                     (function() {
                        $(".os-range").slider({
                           range: "min",
                           max: 600,
                           min: 5,
                           step: 5,
                           value:data.timelimit,
                           slide: function(e, ui) {
                             $(".os-currentVal").html(ui.value);
                           }
                        });
                     }).call(this);
                  }
                  else if (data.plan_type == '2') {
		     if (location_type == 8) {
			$("#custom_location_dataplan").hide();
			$("#custom_location_timepla").show();
		     }
                     $(".time_limit_div").removeClass("disabled");
                     //$(".data_limit_div").addClass("disabled");
		     $(".data_limit_div").removeClass("disabled");
                     $('.download_rate').val(data.download_rate);
                     $('.upload_rate').val(data.upload_rate);
                      $('.datalimit').val('');
                      $('.datalimit_checkbox').prop('checked', false);
                      $('.timelimit_checkbox').prop('checked', true);
		      $('.timelimit_checkbox').attr("disabled", true);
                     $('.os-currentVal').html(data.timelimit);
                      (function() {
                        $(".os-range").slider({
                           range: "min",
                           max: 600,
                           min: 5,
                           step: 5,
                           value:data.timelimit,
                           slide: function(e, ui) {
                             $(".os-currentVal").html(ui.value);
                           }
                        });
                     }).call(this);
                  }
                  else if (data.plan_type == '5') {
		     if (location_type == 8) {
			$("#custom_location_dataplan").show();
			$("#custom_location_timepla").hide();
		     }
                     $(".time_limit_div_hybrid").removeClass("disabled");
                     $(".data_limit_div_hybrid").removeClass("disabled");
                     
                     $('.download_rate_hybrid').val(data.download_rate);
                     $('.upload_rate_hybrid').val(data.upload_rate);
                      $('.datalimit_checkbox_hybrid').prop('checked', true);
                      $('.timelimit_checkbox_hybrid').prop('checked', true);
		      $('.datalimit_checkbox_hybrid').attr("disabled", true);
		      $('.timelimit_checkbox_hybrid').attr("disabled", true);
                      $('.datalimit_hybrid').val(data.datalimit);
                      $('.os-currentVal_hybrid').html(data.timelimit);
                      (function() {
                        $(".os-range_hybrid").slider({
                           range: "min",
                           max: 600,
                           min: 5,
                           step: 5,
                           value:data.timelimit,
                           slide: function(e, ui) {
                             $(".os-currentVal_hybrid").html(ui.value);
                           }
                        });
                     }).call(this);
		      
		        // for custom location
		     $(".time_limit_div").removeClass("disabled");
		     $(".data_limit_div").removeClass("disabled");
		     $('.download_rate').val(data.download_rate);
                     $('.upload_rate').val(data.upload_rate);
		     $('.datalimit').val(data.datalimit);
                     $('.datalimit_checkbox').prop('checked', true);
                     $('.timelimit_checkbox').prop('checked', true);
		     $('.timelimit_checkbox').attr("disabled", true);
		     $('.os-currentVal').html(data.timelimit);
                     (function() {
                        $(".os-range").slider({
                           range: "min",
                           min: 5,
                           max: 600,
                           step: 5,
                           value:data.timelimit,
                           slide: function(e, ui) {
                             $(".os-currentVal").html(ui.value);
                           }
                        });
                     }).call(this);
                  }
               }
	       else{
		  if (location_type == '8') {
		     $(".time_limit_div").removeClass("disabled");
                     $(".data_limit_div").removeClass("disabled");
		     $("#custom_location_dataplan").hide();
		     $("#custom_location_timepla").hide();
		  }
                  $('.download_rate').val('');
                     $('.upload_rate').val('');
                     $('.datalimit').val('');
                     $('.datalimit_checkbox').prop('checked', false);
                     $('.timelimit_checkbox').prop('checked', true);
                     $('.os-currentVal').html('5');
                     (function() {
                        $(".os-range").slider({
                           range: "min",
                           max: 600,
                           value:5,
                           min: 5,
                           step: 5,
                           slide: function(e, ui) {
                             $(".os-currentVal").html(ui.value);
                           }
                        });
                     }).call(this);
               }
	      
   	    }
      });
   }
   function change_plan(sel) {
      /*var plan_id = $(this).val();
      alert(plan_id);*/
      var plan_id = sel.value;
     plan_detail(plan_id);
   }
   function save_time_plan(click_from = "no"){
      var plan_id = $("#cp1_plan").val();
      $("#add_plan_select_box_id").val('cp1_plan');
      var datalimit = $("input[name='datalimit']:visible").val();
      var timelimit = $(".os-currentVal").html();
      var downrate = $("input[name='download_rate']:visible").val();
      var uprate = $("input[name='upload_rate']:visible").val();
      $("#edit_plan_id").val(plan_id);
      $("#edit_data_limit").val(datalimit);
      $("#edit_time_limit").val(timelimit);
      
      $("#edit_plan_downrate").val(downrate);
      $("#edit_plan_uprate").val(uprate);
      var is_data_checked = 0;
      if ( $(".datalimit_checkbox").is(':visible') && $('.datalimit_checkbox').prop('checked') ) {

        is_data_checked = '1';

      }
      else
      {
	  $("input[name='datalimit']:visible").val('');
      }
      if (is_data_checked == '1' && datalimit != '' && datalimit > 0)
      {
	 $("#edit_plan_type").val('5');
	 var plan_button_click = 'hybrid_plan';
      }
      else
      {
	 $("#edit_plan_type").val('2');
	 var plan_button_click = 'time_plan';
	 
      }
      $("#plan_button_click").val(plan_button_click);
      check_plan_already_attached(plan_id,click_from);
   }
   function save_time_plan_other_location(click_from = "no"){
      var plan_id = $("#cp_hotel_plan").val();
      $("#add_plan_select_box_id").val('cp_hotel_plan');
      var datalimit = $("input[name='datalimit']:visible").val();
      var timelimit = $(".os-currentVal").html();
      var downrate = $("input[name='download_rate']:visible").val();
      var uprate = $("input[name='upload_rate']:visible").val();
      $("#edit_plan_id").val(plan_id);
      $("#edit_data_limit").val(datalimit);
      $("#edit_time_limit").val(timelimit);
      
      $("#edit_plan_downrate").val(downrate);
      $("#edit_plan_uprate").val(uprate);
      $("#edit_plan_type").val('2');
      
      var is_data_checked = 0;
      if ( $(".datalimit_checkbox").is(':visible') && $('.datalimit_checkbox').prop('checked') ) {

        is_data_checked = '1';

      }
      else
      {
	  $("input[name='datalimit']:visible").val('');
      }
      if (is_data_checked == '1' && datalimit != '' && datalimit > 0)
      {
	 $("#edit_plan_type").val('5');
	 var plan_button_click = 'hybrid_plan';
      }
      else
      {
	 $("#edit_plan_type").val('2');
	 var plan_button_click = 'time_plan';
	 
      }
      
      $("#plan_button_click").val(plan_button_click);
      check_plan_already_attached(plan_id,click_from);
   }
   function save_time_plan_other_location_cafe(click_from = "no"){
       var plan_id = $("#cp_cafe_plan").val();
      $("#add_plan_select_box_id").val('cp_cafe_plan');
      var datalimit = $("input[name='datalimit']:visible").val();
      var timelimit = $(".os-currentVal").html();
      var downrate = $("input[name='download_rate']:visible").val();
      var uprate = $("input[name='upload_rate']:visible").val();
      $("#edit_plan_id").val(plan_id);
      $("#edit_data_limit").val(datalimit);
      $("#edit_time_limit").val(timelimit);
      
      $("#edit_plan_downrate").val(downrate);
      $("#edit_plan_uprate").val(uprate);
      $("#edit_plan_type").val('2');
      
      var is_data_checked = 0;
      if ( $(".datalimit_checkbox").is(':visible') && $('.datalimit_checkbox').prop('checked') ) {

        is_data_checked = '1';

      }
      else
      {
	 $("input[name='datalimit']:visible").val('');
      }
      if (is_data_checked == '1' && datalimit != '' && datalimit > 0)
      {
	 $("#edit_plan_type").val('5');
	 var plan_button_click = 'hybrid_plan';
      }
      else
      {
	 $("#edit_plan_type").val('2');
	 var plan_button_click = 'time_plan';
	 
      }
      $("#plan_button_click").val(plan_button_click);
      check_plan_already_attached(plan_id,click_from);
   }
   
   function save_time_plan_other_location_cafe_hybrid(click_from = "no"){
       var plan_id = $("#cp_cafe_hybrid_plan").val();
      $("#add_plan_select_box_id").val('cp_cafe_hybrid_plan');
      var datalimit = $("input[name='datalimit']:visible").val();
      var timelimit = $(".os-currentVal").html();
      var downrate = $("input[name='download_rate']:visible").val();
      var uprate = $("input[name='upload_rate']:visible").val();
      $("#edit_plan_id").val(plan_id);
      $("#edit_data_limit").val(datalimit);
      $("#edit_time_limit").val(timelimit);
      
      $("#edit_plan_downrate").val(downrate);
      $("#edit_plan_uprate").val(uprate);
      $("#edit_plan_type").val('5');
      var plan_button_click = 'hybrid_plan';
      $("#plan_button_click").val(plan_button_click);
      check_plan_already_attached(plan_id,click_from);
   }
   function save_data_plan(click_from = "no"){
      var plan_id = $("#cp2_plan").val();
      $("#add_plan_select_box_id").val('cp2_plan');
      var datalimit = $("input[name='datalimit']:visible").val();
      var timelimit = '0';
      var downrate = $("input[name='download_rate']:visible").val();
      var uprate = $("input[name='upload_rate']:visible").val();
      $("#edit_plan_id").val(plan_id);
      $("#edit_data_limit").val(datalimit);
      $("#edit_time_limit").val(timelimit);
      
      $("#edit_plan_downrate").val(downrate);
      $("#edit_plan_uprate").val(uprate);
      $("#edit_plan_type").val('4');
      var plan_button_click = 'data_plan';
      $("#plan_button_click").val(plan_button_click);
      check_plan_already_attached(plan_id,click_from);
   }
   function save_hybrid_data_plan(click_from = "no"){
      var plan_id = $("#cp3_data_plan").val();
      $("#add_plan_select_box_id").val('cp3_data_plan');
      var datalimit = $("input[name='datalimit']:visible").val();
      var timelimit = '0';
      var downrate = $("input[name='download_rate']:visible").val();
      var uprate = $("input[name='upload_rate']:visible").val();
      $("#edit_plan_id").val(plan_id);
      $("#edit_data_limit").val(datalimit);
      $("#edit_time_limit").val(timelimit);
      
      $("#edit_plan_downrate").val(downrate);
      $("#edit_plan_uprate").val(uprate);
      $("#edit_plan_type").val('4');
      var plan_button_click = 'data_plan';
      $("#plan_button_click").val(plan_button_click);
      check_plan_already_attached(plan_id,click_from);
   }
   function save_hybrid_plan(click_from = "no"){
      var plan_id = $("#cp3_hybrid_plan").val();
      $("#add_plan_select_box_id").val('cp3_hybrid_plan');
      var datalimit = $("input[name='datalimit_hybrid']:visible").val();
      var timelimit = $(".os-currentVal_hybrid").html();
      var downrate = $("input[name='download_rate_hybrid']:visible").val();
      var uprate = $("input[name='upload_rate_hybrid']:visible").val();
      
      $("#edit_plan_id").val(plan_id);
      $("#edit_data_limit").val(datalimit);
      $("#edit_time_limit").val(timelimit);
      
      $("#edit_plan_downrate").val(downrate);
      $("#edit_plan_uprate").val(uprate);
      $("#edit_plan_type").val('5');
      var plan_button_click = 'hybrid_plan';
      $("#plan_button_click").val(plan_button_click);
      check_plan_already_attached(plan_id,click_from);
   }
   function save_custom_location_plan(click_from = "no") {
       var plan_id = $("#custom_location_plan").val();
      $("#add_plan_select_box_id").val('custom_location_plan');
      var datalimit = '';
      if ($('input[name="datalimit_checkbox"]:visible').is(':checked')) {
	 datalimit = $("#custom_plan_data_limit").val();
	 if (datalimit == '') {
	    datalimit = 0;
	 }
      }
      var timelimit = '';
      if ($('input[name="timelimit_checkbox"]:visible').is(':checked')) {
	 timelimit = $(".os-currentVal").html();
      }
      var downrate = $("input[name='download_rate']:visible").val();
      var uprate = $("input[name='upload_rate']:visible").val();
      $("#edit_plan_id").val(plan_id);
      $("#edit_data_limit").val(datalimit);
      $("#edit_time_limit").val(timelimit);
      
      $("#edit_plan_downrate").val(downrate);
      $("#edit_plan_uprate").val(uprate);
      $("#edit_plan_type").val('');
      var plan_button_click = 'custom_plan';
      $("#plan_button_click").val(plan_button_click);
      check_plan_already_attached(plan_id,click_from);
   }
   function check_plan_already_attached(plan_id,click_from) {
      if (click_from == 'no') {
         $(".loading").show();
      }
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/check_plan_already_attached",
   	    data: "plan_id="+plan_id,
	    dataType: 'json',
   	    success: function(data){
               if (data.code == '1') {
                  $("#plan_name").val(data.plan_name);
                  $("#plan_desc").val(data.plan_desc);
               }
	      
	       if (click_from == 'no') {
		   $(".loading").hide();
                  $("#save_plan_model").modal("show");
               }else{
                  save_plan(click_from);
               }
   	    }
      });
   }
   function save_plan(click_from = 'no') {
      var plan_button_click = $("#plan_button_click").val();
      var plan_id = $("#edit_plan_id").val();
      var data_limit = $("#edit_data_limit").val();
      var time_limit = $("#edit_time_limit").val();
      var plan_name = $("#plan_name").val();
      var plan_desc = $("#plan_desc").val();
      var downrate = $("#edit_plan_downrate").val();
      var uprate = $("#edit_plan_uprate").val();
      var plan_type = $("#edit_plan_type").val();
      $("#save_plan_error").html("");
      if (plan_name == '' || plan_desc == '' || downrate == '' || uprate == '') {
         if (plan_name == '' || plan_desc == '') {
            $("#save_plan_error").html("Please fill all fields");
            return false;
         }
         else if (downrate == '' || uprate == '') {
            $("#save_plan_error").html("Please fill download rate and upload rate");
            return false;
         }
      }
      if (plan_button_click == 'time_plan') {
         if (time_limit == '0') {
            $("#save_plan_error").html("Please set timelimit");
            return false;
         }
      }
      else if (plan_button_click == 'data_plan') {
         if (data_limit == '0') {
            $("#save_plan_error").html("Please set datalimit");
            return false;
         }
      }
      else if (plan_button_click == 'hybrid_plan') {
         if (time_limit == '0' || data_limit == '0') {
            $("#save_plan_error").html("Please set datalimit and timelimit");
            return false;
         }
      }
      else if (plan_button_click == 'custom_plan') {
         if (time_limit == '' && data_limit == '') {
            $("#save_plan_error").html("Please set datalimit or timelimit");
            return false;
         }else{
	    if (time_limit != '' && data_limit == '') {
	       $("#custom_location_dataplan").hide();
	       $("#custom_location_timepla").show();
	    }else{
	       $("#custom_location_dataplan").show();
	       $("#custom_location_timepla").hide();
	    }
	 }
      }
      else if (plan_button_click == 'time_or_hybrid_plan')
      {
	 if (data_limit > 0)
	 {
	    plan_type = '5';
	 }
	 else
	 {
	    plan_type = '2';
	 }
	 $("#edit_plan_type").val('6');// set 6 to get all hybrid and time plan
      }
      
      if (click_from == 'no') {
         $(".loading").show();
      }
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/save_plan",
   	    data: "plan_id="+plan_id+"&data_limit="+data_limit+"&time_limit="+time_limit+"&plan_name="+plan_name+"&plan_desc="+plan_desc+"&downrate="+downrate+"&uprate="+uprate+"&plan_type="+plan_type,
	    dataType: 'json',
   	    success: function(data){
               if (click_from == 'no') {
		  $(".loading").hide();
	       }
	       
               $("#save_plan_model").modal("hide");
	       if (data.created_plan_id != '0') {
                  plan_list(data.created_plan_id)
               }
   	    }
      });
   }
   function plan_list(plan_id) {
      var select_div_id = $("#add_plan_select_box_id").val();
      var plan_type = $("#edit_plan_type").val();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/plan_list",
   	    data: "plan_type="+plan_type+"&plan_id="+plan_id,
	    
   	    success: function(data){
               $("#"+select_div_id).html(data);
   	    }
      });
      //$("#"+select_div_id).html(data);
   }
   
   function voucher_list(){
      var location_uid =  $("#location_id_hidden").val();
   $(".loading").show();
   $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/location_voucher_list",
        data: "location_uid="+location_uid,
        success: function(data){
   	$(".loading").hide();
   	$("#voucher_list").html(data);
        }
         });
       }
       
       function add_location_vouchers(){
   var location_uid =  $("#location_id_hidden").val();
   var voucher_data =  $("#voucher_data").val();
   var voucher_data_type =  $("#voucher_data_type").val();
   var voucher_cost =  $("#voucher_cost").val();
   var voucher_selling_price =  $("#voucher_selling_price").val();
    $(".loading").show();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_location_vouchers",
        data: "location_uid="+location_uid+"&voucher_data="+voucher_data+"&voucher_data_type="+voucher_data_type+"&voucher_cost="+voucher_cost+"&voucher_selling_price="+voucher_selling_price,
        success: function(data){
   	if (data == '1') {
   	   $("#voucher_data").val('');
   	   $("#voucher_cost").val('');
   	   $("#voucher_selling_price").val('');
   	   voucher_list();
   	}else{
   	   $(".loading").hide();
   	   $("#voucher_create_error").html("Can not create more than 4");
   	}
   	
        }
         });
       }
       function delete_voucher(voucher_id) {
   if (confirm("Are you sure you want to delete?")) {
      var location_uid =  $("#location_id_hidden").val();
      $(".loading").show();
       $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/delete_voucher",
        data: "voucher_id="+voucher_id,
        success: function(data){
   	voucher_list();
        }
         });
   }
   return false;
       }
       
       function edit_voucher(voucher_id, voucher_data, voucher_data_type, voucher_cost, voucher_selling_price) {
      $("#voucher_id_edit").val(voucher_id);
      $("#voucher_data_edit").val(voucher_data);
      $("#voucher_data_type_edit").val(voucher_data_type);
      $("#voucher_cost_edit").val(voucher_cost);
      $("#voucher_selling_price_edit").val(voucher_selling_price);
      $("#EditvoucherModal").modal("show");
       }
       function update_location_vouchers(){
   var location_uid =  $("#location_id_hidden").val();
    var voucher_id =  $("#voucher_id_edit").val();
   var voucher_data =  $("#voucher_data_edit").val();
   var voucher_data_type =  $("#voucher_data_type_edit").val();
   var voucher_cost =  $("#voucher_cost_edit").val();
   var voucher_selling_price =  $("#voucher_selling_price_edit").val();
    $(".loading").show();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_location_vouchers",
        data: "location_uid="+location_uid+"&voucher_data="+voucher_data+"&voucher_data_type="+voucher_data_type+"&voucher_cost="+voucher_cost+"&voucher_id="+voucher_id+"&voucher_selling_price="+voucher_selling_price,
        success: function(data){
   	$("#EditvoucherModal").modal("hide");
   	voucher_list();
        }
         });
       }
       // on cp type change show and hide div
       $("input[name='voucher_radio']").change(function() {
   var voucher_need = $("input[name='voucher_radio']:checked").val();
   if (voucher_need == '1') {
      $("#voucher_generate_div").show();
   }else{
      $("#voucher_generate_div").hide();
   }
   
       });
       
       function exit_voucher_panel(){
   var location_uid =  $("#location_id_hidden").val();
   var voucher_need = $("input[name='voucher_radio']:checked").val();
   var payment_gateway = $("input[name='payment_gateway_radio']:checked").val();
    $(".loading").show();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/update_location_voucher_status",
        data: "location_uid="+location_uid+"&voucher_need="+voucher_need+"&payment_gateway="+payment_gateway,
        success: function(data){
   	window.location = "<?php  echo base_url()?>location";
        }
         });
       }
       
       
      function terms_of_use(){
         var location_uid =  $("#location_id_hidden").val();
         $(".loading").show();
         $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>location/terms_of_use",
            data: "location_uid="+location_uid,
            success: function(data){
               $(".loading").hide();
               CKEDITOR.instances.editor1.setData(data);
            }
         });
      }
      
      function update_terms_of_use() {
     
            var location_uid =   $("#location_id_hidden").val();
            //var text_value = $("#editor1").val();
            var text_value = CKEDITOR.instances.editor1.getData();
            $(".loading").show();
            $.ajax({
               type: "POST",
               url: "<?php echo base_url()?>location/update_terms_of_use",
               //data: "location_uid="+location_uid+"&text_value="+text_value,
	       data : ({'location_uid': location_uid, 'text_value': text_value}),
               success: function(data){
                  
                  $(".loading").hide();	
               }
            });
      }
      
   function get_location_cp_path() {
      var location_uid =  $("#location_id_hidden").val();
      $.ajax({
               type: "POST",
                url: "<?php echo base_url()?>location/current_cp_path",
		data: "location_uid="+location_uid,
		dataType: 'json',
		success: function(data){
                  if (data.code == '1') {
                     var link = '<a style="font-size: 11px" href = "'+data.cp_url+'" target="_blank">'+data.cp_url+'</a><a class="mui-url_btn--os" style="font-size: 11px" href = "'+data.cp_url+'" target="_blank">CP Preview</a><a class="mui-url_btn--os" style="font-size: 11px" href = "'+data.qr_path+'" target="_blank">CP QR</a><br/>';
                     var link1='<a style="font-size: 11px" href = "'+data.eventguest_url+'" target="_blank">'+data.eventguest_url+'</a><a class="mui-url_btn--os" style="font-size: 11px" href = "'+data.eventguest_url+'" target="_blank">CP Preview</a><a class="mui-url_btn--os" style="font-size: 11px" href = "'+data.qr_pathguest+'" target="_blank">CP QR</a>';
                     $('.cp_path_url_display').html(link);
                      $('.cp_path_url_display_table').html(link1);
		  }else{
                     $('.cp_path_url_display').html("-- Add NAS Setup to generate a CP URL --");
                  }
               }
        });
   }
   
    function move_access_point(ap_location_type,access_point_id, macid, hotspot_type, router_type) {
      $("#onehop_move_macid").val(macid);
      $(".loading").show();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>location/get_location_ap_move_where",
          data: "router_type="+router_type,
          success: function(data){
	    $("#move_api_router_type").val(router_type);
	    $("#move_onehop_ap_modal").modal("show");
          $("#onehop_move_location_uid").html(data);
          $(".loading").hide();
          }
     });
   }
   function move_onehop_ap() {
      $("#move_ap_error").html("");
      var onehop_move_macid = $("#onehop_move_macid").val();
      var onehop_move_location_uid = $("#onehop_move_location_uid").val();
      var router_type = $("#move_api_router_type").val();
      if (onehop_move_macid == '' || onehop_move_location_uid == '') {
	 $("#move_ap_error").html("please select location and fill macid");
	 return false;
      }
      $(".loading").show();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>location/onehop_move_ap",
          data: "onehop_move_macid="+onehop_move_macid+"&onehop_move_location_uid="+onehop_move_location_uid+"&router_type="+router_type,
	  dataType: 'json',
          success: function(data){
	    if (data.code == '1') {
	       access_point_list();
	       $("#onehop_ap_mode_done").modal('show');
	       $("#move_onehop_ap_modal").modal('hide');
	    }else{
	       
	       $("#move_ap_error").html(data.message);
	    }
	    
	    $(".loading").hide();
          }
     });
   }
   
   function remove_slider_image(slider_type) {
      $(".loading").show();
      var location_uid =  $("#location_id_hidden").val();
      $.ajax({
          type: "POST",
          url: "<?php echo base_url()?>location/remove_slider_image",
          data: "location_uid="+location_uid+"&slider_type="+slider_type,
	  dataType: 'json',
          success: function(data){
	    if (slider_type == 'slider1') {
	       $('.slider1_image_preview_show').attr('src', '');
	       $("#custom_slider_image1_remove_button").hide();
	    }else if (slider_type == 'slider2') {
	       $('.slider2_image_preview_show').attr('src', '');
	       $("#custom_slider_image2_remove_button").hide();
	    }
	    $(".loading").hide();
          }
      });
   }
   
   $("input[name='offline_cp_content_type']").click(function(){
      var val = $('input:radio[name=offline_cp_content_type]:checked').val();
      if (val == '1') {
	 $("#dynamic_login_enable").show();
	 $("#offline_cp_type_div_static").hide();
      }else{
	 $("#dynamic_login_enable").hide();
	 $("#offline_cp_type_div_static").show();
      }
   }); 
      
</script>
      
      <script>
	 /*function content_builder() {
	    $(".loading").show();
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/content_builder_list",
	       data: "location_uid="+location_uid,
	       dataType: 'json',
	       success: function(data){
		  $(".loading").hide();
		  if (data.code == '1') {
		     if (data.logo_image != '') {
			$('.location_logo_preview_show').attr('src', '');
			d = new Date();
			$('.location_logo_preview_show').attr('src', data.logo_image+"?"+d.getTime());
		     }
			      
		  }
	       }
	    });
	 }*/
      </script>
	      <script>
	 var content_array;
	 var compliance_category;
	 // function to get all data related to main, sub category and content and also list main category
	 function content_builder(created_id= '', created_subcategory_id = '') {
	    $(".loading").show();
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/content_builder_list",
	       data: "location_uid="+location_uid,
	       dataType: 'json',
	       success: function(data){
		  content_array = data;
		  compliance_category = data.compliance_category;
		  $(".loading").hide();
		  $("#offline_content_title").val(data.offline_content_title);
		  $("#offline_content_desc").val(data.offline_content_desc);
		  if (data.main_category.length > 0) {
		     $('.main_section_content_list').html('');
		     for (var i = 0; i < data.main_category.length; i++) {
			var main_category = data.main_category[i];
			var main_class = '';
			if (created_id == '') {
			   if (i == 0) {
			      main_class = 'offline_active';
			      list_subsection(main_category.id,created_subcategory_id);
			   }
			}else{
			   if (main_category.id == created_id) {
			      main_class = 'offline_active';
			      list_subsection(main_category.id,created_subcategory_id);
			   }
			}
			var category_name = escape(main_category.category_name);
			var category_desc = escape(main_category.category_desc);
			var is_promoter = escape(main_category.is_promoter);
			var compliance_cat_id = escape(main_category.compliance_cat_id);
			var icon = escape(main_category.icon);
			var content_html = '<li class = "main_section '+main_class+'" data-is_event_module_category="'+main_category.is_event_module_category+'"  data-main_id="'+main_category.id+'" ><span>'+main_category.category_name+'</span><span class="offline-fa">'+
					  '<i onclick="edit_main_category('+main_category.id+', \''+category_name+'\', \''+category_desc+'\', \''+icon+'\', \''+is_promoter+'\', \''+compliance_cat_id+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
					  '</span>'+
					  '<span class="offline-fa"><i onclick="delete_main_category('+main_category.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
				       '</li>';
			$('.main_section_content_list').append(content_html);
		     }
		     // for main section
		     $(".offline_main_section_filled_div").show();
		     $(".offline_main_section_blank_div").hide();
		     
		  }else{
		     // for main section
		     $(".offline_main_section_blank_div").show();
		     $(".offline_main_section_filled_div").hide();
		     // for sub section
		     $(".offline_sub_section_blank_first_div").show();
		     $(".offline_sub_section_filled_div").hide();
		     $(".offline_sub_section_blank_div").hide();
		     // for content section
		     $(".offline_content_section_blank_first_div").show();
		     $(".offline_content_section_filled_div").hide();
		     $(".offline_content_section_blank_div").hide();
		  }
	       }
	    });
	 }
	 // on click of main category
	 $(document).on('click', '.main_section', function (){
	    $( ".main_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	    });
	    $( this ).addClass( "offline_active" );
	    var main_id = $(this).data('main_id'); 
	    list_subsection(main_id);
	 });
	 
	 // function to list the sub category
	 function list_subsection(main_id,created_subcategory_id = '') {
	    $('.sub_section_content_list').html('');
	    var content = '';
	    for (var i = 0; i < content_array.main_category.length; i++) {
	       var main_category = content_array.main_category[i];
	       if (main_category.id == main_id) {
		  content = main_category.sub_category;
	       }
	    }
	    if (content.length > 0) {
	       // for sub section
	       $(".offline_sub_section_filled_div").show();
	       $(".offline_sub_section_blank_first_div").hide();
	       $(".offline_sub_section_blank_div").hide();
	       for (var i = 0; i < content.length; i++) {
		  var sub_category = content[i];
		  var main_class = '';
		  var sub_category_type = sub_category.is_this_is_content;
		  if (created_subcategory_id == '') {
		     if (i == 0) {
			main_class = 'offline_active';
			list_contentsection(main_id,sub_category.id,sub_category_type);
		     }
		  }else{
		     if (sub_category.id == created_subcategory_id) {
			main_class = 'offline_active';
			list_contentsection(main_id,sub_category.id,sub_category_type);
		     }
		  }
		  
		  if (sub_category.is_this_is_content == '0') {
		     $(".sub_category_add_data").data('is_this_is_content', '0');
		     var sub_category_name = escape(sub_category.sub_category_name);
		     var sub_category_desc = escape(sub_category.sub_category_desc);
		     var icon = escape(sub_category.icon);
		     var content_html = '<li  class = "sub_section '+main_class+'"  data-main_id="'+main_id+'"  data-sub_id="'+sub_category.id+'"  data-sub_category_type="'+sub_category_type+'"><span>'+sub_category.sub_category_name+'</span><span class="offline-fa">'+
				    '<i onclick="edit_sub_category('+main_id+','+sub_category.id+', \''+sub_category_name+'\', \''+sub_category_desc+'\', \''+icon+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
				    '</span>'+
				    '<span class="offline-fa"><i onclick="delte_sub_category('+sub_category.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
				 '</li>';
		  }else{
		      var edit_icon = '';
		     $(".sub_category_add_data").data('is_this_is_content', '1');
		     var content_title = escape(sub_category.content_title);
		     var content_desc = escape(sub_category.content_desc);
		     var content_type = escape(sub_category.content_type);
		     var wiki_title = escape(sub_category.wikipedia_title_url);
		     if ((content_type == 'poll' || content_type == 'survey') && sub_category.is_synced == '1') {
			//code
		     }else{
			edit_icon = '<i onclick="edit_content('+sub_category.upload_type+','+main_id+',0,'+sub_category.id+',\''+content_title+'\',\''+sub_category.content_file+'\', \''+content_desc+'\' , \''+content_type+'\', \''+wiki_title+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>';
		     }
		     var content_html = '<li class = "sub_section '+main_class+'" data-main_id="'+main_id+'"  data-sub_id="'+sub_category.id+'" data-sub_category_type="'+sub_category_type+'">'+
					  '<div class="row">'+
					  '<div class="col-sm-12">'+    
					  '<span><strong>'+sub_category.content_title+'</strong></span><span class="offline-fa">'+edit_icon+
					  //'<i onclick="edit_content('+main_id+',0,'+sub_category.id+',\''+content_title+'\',\''+sub_category.content_file+'\', \''+content_desc+'\', \''+content_type+'\')"  class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
					  '</span>'+
					  '<span class="offline-fa"><i onclick="delete_content_on_sub_category('+sub_category.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
					  '</div>'+
					  '<div class="col-sm-12">'+
					  '<strong>'+sub_category.content_file+'</strong>'+
					  '</div>'+
					  '<div class="col-sm-12">'+
					  '<p>'+sub_category.content_desc+'</p>'+
					  '</div></div></li>';
		  }
		  
		  $('.sub_section_content_list').append(content_html);
	       }
	    }else{
	       // for sub section
	       $(".offline_sub_section_blank_div").show();
	       $(".offline_sub_section_blank_first_div").hide();
	       $(".offline_sub_section_filled_div").hide();
	       // for content section
	       $(".offline_content_section_blank_first_div").show();
	       var text = '<center><p>Add Sub Section First</p></center>';
	       $(".offline_content_section_blank_first_div").html(text);
	       $(".offline_content_section_filled_div").hide();
	       $(".offline_content_section_blank_div").hide();
	       var is_event_module_category = 0;
		$( ".main_section" ).each(function() {
		    if($( this ).hasClass( "offline_active" )){
			is_event_module_category = $(this).data('is_event_module_category');
		    }
		});
		if (is_event_module_category == '4') {
		    $(".discuss_question_add_option").hide();
		}
		else{
		    $(".discuss_question_add_option").show();
		}
	    }
	 }
	 
	 // on click of sub category
	 $(document).on('click', '.sub_section', function (){
	    $( ".sub_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	    });
	    $( this ).addClass( "offline_active" );
	    var main_id = $(this).data('main_id');
	    var sub_id = $(this).data('sub_id');
	    var sub_category_type = $(this).data('sub_category_type');
	    list_contentsection(main_id,sub_id,sub_category_type);
	 });
	  // on click of content
	 $(document).on('click', '.content_section', function (){
	    $( ".content_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	    });
	    $( this ).addClass( "offline_active" );
	   
	 });
	 // function to list the content
	 function list_contentsection(main_id,sub_id,sub_category_type) {
	    $('.content_section_content_list').html('');
	    var content = '';
	    for (var i = 0; i < content_array.main_category.length; i++) {
	       var main_category = content_array.main_category[i];
	       if (main_category.id == main_id) {
		  content = main_category.sub_category;
	       }
	    }
	    var sub_content = '';
	    if (content.length > 0) {
	       for (var i = 0; i < content.length; i++) {
		  var sub_category = content[i];
		  if (sub_category.id == sub_id) {
		     if (sub_category.is_this_is_content == '0') {
			sub_content = sub_category.content;
		     }
		  }
		  
	       }
	       if (sub_content.length > 0) {
		  for (var i = 0; i < sub_content.length; i++) {
		     // for content section
		     $(".offline_content_section_filled_div").show();
		     $(".offline_content_section_blank_first_div").hide();
		     $(".offline_content_section_blank_div").hide();
		     var content_category = sub_content[i];
		     var main_class = '';
		     if (i == 0) {
			main_class = 'offline_active';
		     }
		     var edit_icon = '';
		     var content_title = escape(content_category.content_title);
		     var content_desc = escape(content_category.content_desc);
		     var content_type = escape(content_category.content_type);
		     var content_type = escape(content_category.content_type);
		     var wiki_title = escape(content_category.wikipedia_title_url);
		     if ((content_type == 'poll' || content_type == 'survey') && sub_category.is_synced == '1') {
			//code
		     }else{
			edit_icon = '<i onclick="edit_content('+content_category.upload_type+','+main_id+','+sub_id+','+content_category.id+', \''+content_title+'\', \''+content_category.content_file+'\', \''+content_desc+'\', \''+content_type+'\', \''+wiki_title+'\')"   class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>';
		     }
		     var content_html = '<li class = "content_section '+main_class+'" data-main_id="'+main_id+'"  data-sub_id="'+sub_id+'" data-content_id="'+content_category.id+'">'+
					     '<div class="row">'+
					     '<div class="col-sm-12">'+    
					     '<span><strong>'+content_category.content_title+'</strong></span><span  class="offline-fa">'+edit_icon+
					     //'<i onclick="edit_content('+main_id+','+sub_id+','+content_category.id+', \''+content_title+'\', \''+content_category.content_file+'\', \''+content_desc+'\', \''+content_type+'\')"   class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
					     '</span>'+
					     '<span class="offline-fa"><i onclick="delete_content('+content_category.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
					     '</div>'+
					     '<div class="col-sm-12">'+
					     '<strong>'+content_category.content_file+'</strong>'+
					     '</div>'+
					     '<div class="col-sm-12">'+
					     '<p>'+content_category.content_desc+'</p>'+
					     '</div></div></li>';
		     $('.content_section_content_list').append(content_html);
		  }
	       }else{
		  
		  // for content section
		  
		  if(sub_category_type == 1){
		     $(".offline_content_section_blank_first_div").show();
		     $(".offline_content_section_blank_div").hide();
		     var text = '<center><p>No content available</p></center>';
		     $(".offline_content_section_blank_first_div").html(text);
		  }else{
		     $(".offline_content_section_blank_div").show();
		     $(".offline_content_section_blank_first_div").hide();
		     var text = '<center><p>Add Sub Section First</p></center>';
		     $(".offline_content_section_blank_first_div").html(text);
		  }
		  
		  $(".offline_content_section_filled_div").hide();
		  
	       }
	       
	    }else{
	       
	       // for content section
	       $(".offline_content_section_blank_first_div").show();
	       if(sub_category_type == 1){
		  var text = '<center><p>No content available</p></center>';
		  $(".offline_content_section_blank_first_div").html(text);
	       }else{
		  var text = '<center><p>Add Sub Section First</p></center>';
		  $(".offline_content_section_blank_first_div").html(text);
	       }
	       $(".offline_content_section_blank_div").hide();
	       $(".offline_content_section_filled_div").hide();
	       
	    }
	    
	 }
	 function open_add_main_category() {
	    $("#icon_validation").val('1');
	    $("#create_main_category_error").html("");
	    $("#add_new_main_section_form")[0].reset();
	    var location_type = $("#location_type").val();
	    if (location_type == '13')
	    {
		$("#promoter_category_div").show();
		var compliance_val = '<option value="">Select Compliance</option>';
		if (compliance_category.length > 0)
		{
		    for (var i = 0; i < compliance_category.length; i++)
		    {
			var category = compliance_category[i];
			compliance_val += '<option value="'+category.id+'">'+category.category_name+'</option>';
		    }
		}
		$("#compliance").html(compliance_val);
	    }
	    else
	    {
		$("#promoter_category_div").hide();
	    }
	    $("#main_category_modal").modal("show");
	 }
	 // function to add main category
	 function add_main_category() {
	    $("#create_main_category_error").html("");
	    var compliance_cat = $("#compliance").val();
            var location_type = $("#location_type").val();
	    var is_promoter_category = 0;
	    if ($('input[name="promoter_category"]').is(':checked')) {
		is_promoter_category = $('input[name="promoter_category"]').val();
	    }
	    else
	    {
                if (compliance_cat == '' && location_type == '13')
		{
		    $("#create_main_category_error").html("Please select compliance category");
		    return false;
		}
	    }
	    var main_category_name = $("#main_category_name").val();
	    var main_category_desc = $("#main_category_desc").val();
	    var offline_content_title = $("#offline_content_title").val();
	    var offline_content_desc = $("#offline_content_desc").val();
	    if (main_category_name == '') {
	       $("#create_main_category_error").html("Please Enter Category Name");
	       return false;
	    }else{
	       var icon_validation = $("#icon_validation").val();
	       if (icon_validation == '0') {
		  $("#create_main_category_error").html("Icon should be same hight and width and should be < 100px");
		  return false;
	       }
	       var file_data = $("#main_category_file").prop("files")[0];
	       $(".loading").show();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('main_category_id','');
	       form_data.append('main_category_name',main_category_name);
	       form_data.append('main_category_desc',main_category_desc);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append('offline_content_title',offline_content_title);
	       form_data.append("file_previous_name", '');
	       form_data.append("is_promoter_category",is_promoter_category);
	       form_data.append("compliance_cat",compliance_cat);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_main_category",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     content_builder(data);
		     $("#main_category_name").val('');
		     $("#main_category_desc").val('');
		     $("#main_category_modal").modal("hide");
		  }
	       });
	    }
	 }
	 // on click of add more sub category find which model open
	 $(document).on('click', '.sub_category_add_data', function (){
	    var is_this_is_content = $( this ).data( "is_this_is_content" );
	    if (is_this_is_content == 0) {
	       $("#icon_validation").val('1');
	       $("#create_sub_category_error").html("");
	       $("#add_new_sub_section_form")[0].reset();
	       $("#sub_category_modal").modal("show");
	    }else{
	       choose_content_type(1);
	    }
	 });
	 
	 function open_add_sub_category() {
	    $("#icon_validation").val('1');
	    $("#create_sub_category_error").html("");
	    $("#add_new_sub_section_form")[0].reset();
	    $("#sub_category_modal").modal("show");
	 }

	 // function to add sub category
	 function add_sub_category() {
	    $("#create_sub_category_error").html("");
	    var sub_category_name = $("#sub_category_name").val();
	    var sub_category_desc = $("#sub_category_desc").val();
	    if (sub_category_name == '') {
	       $("#create_sub_category_error").html("Please Enter Category Name");
	       return false;
	    }else{
	       var icon_validation = $("#icon_validation").val();
	       if (icon_validation == '0') {
		  $("#create_sub_category_error").html("Icon should be same hight and width and should be < 100px");
		  return false;
	       }
	       var category_id = '0';
	       $(".loading").show();
	       $( ".main_section" ).each(function() {
		  if($( this ).hasClass( "offline_active" )){
		     category_id = $(this).data('main_id');
		  }
	       });
	       var file_data = $("#sub_category_file").prop("files")[0];
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('sub_category_id','');
	       form_data.append('sub_category_name',sub_category_name);
	       form_data.append('sub_category_desc',sub_category_desc);
	       form_data.append("category_id", category_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_sub_category",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     content_builder(category_id,data);
		     $("#sub_category_name").val('');
		     $("#sub_category_desc").val('');
		     $("#sub_category_modal").modal("hide");
		  }
	       });
	    }
	 }
	 
	 // function to choose content type when add
	 function choose_content_type(is_main_category) {
	    $("input[name=content_type_radio]").prop('checked', false);
	    $("#choose_content_type_is_main_category").val(is_main_category);
	    var location_type = $("#location_type").val();
	    if (location_type == '13')
	    {
		$(".retail_audit_content_type").show();
		$(".not_retail_audit_content_type").hide();
		
		var radion_value = 'survey_retail_audit';
		$("input[name=content_type_radio][value="+radion_value+"]").prop('checked', true);
	    }
	    else
	    {
		$(".retail_audit_content_type").hide();
		$(".not_retail_audit_content_type").show();
		
		var is_event_module_category = 0;
		$( ".main_section" ).each(function() {
		    if($( this ).hasClass( "offline_active" )){
			is_event_module_category = $(this).data('is_event_module_category');
		    }
		});
		
		if (is_event_module_category == '2') {// only poll allow
		    $(".text_only_radio").hide();
		    $(".image_video_only_radio").hide();
		    $(".document_view_only_radio").hide();
		    $(".upload_photo_only_radio").hide();
		    $(".survey_only_radio").hide();
		    $(".wiki_only_radio").hide();
		    var radion_value = 'poll';
		    $("input[name=content_type_radio][value="+radion_value+"]").prop('checked', true);
		}
		else if (is_event_module_category == '3') {// only download allow
		    $(".text_only_radio").hide();
		    $(".image_video_only_radio").hide();
		    $(".poll_only_radio").hide();
		    $(".upload_photo_only_radio").hide();
		    $(".survey_only_radio").hide();
		    $(".wiki_only_radio").hide();
		    var radion_value = 'pdf';
		    $("input[name=content_type_radio][value="+radion_value+"]").prop('checked', true);
		}
		else if (is_event_module_category == '4') {// only download allow
		    $(".image_video_only_radio").hide();
		    $(".document_view_only_radio").hide();
		    $(".poll_only_radio").hide();
		    $(".upload_photo_only_radio").hide();
		    $(".survey_only_radio").hide();
		    $(".wiki_only_radio").hide();
		    var radion_value = 'text';
		    $("input[name=content_type_radio][value="+radion_value+"]").prop('checked', true);
		}
		else
		{
		    var radion_value = 'text';
		    $("input[name=content_type_radio][value="+radion_value+"]").prop('checked', true);
		}
		
	    }
	    $("#choose_content_type_modal").modal("show");
	    //$("#upload_content_modal_poll").modal("show");
	 }
	  // function call after choosing content type
	 function choose_content_type_next() {
	    var is_main_category = $("#choose_content_type_is_main_category").val();
	    var content_type = $("input[name='content_type_radio']:checked").val();
	    var location_type = $("#location_type").val();
	    if (location_type == '13') {
		content_type = 'survey_retail_audit';
	    }
	    $("#choose_content_type_modal").modal("hide");
	    if (content_type == 'text') {
	       $("#upload_content_modal_text_form")[0].reset();
	       $("#is_main_category_text").val(is_main_category);
	       $("#upload_content_modal_text").modal("show");
	    }
	    else if (content_type == 'image_audio_video') {
	       $("#upload_content_modal_form")[0].reset();
	       $("#is_main_category").val(is_main_category);
	       $("#upload_content_modal").modal("show");
	    }
	    else if (content_type == 'poll') {
	       $("#add_content_poll")[0].reset();
	       $("#is_main_category_poll").val(is_main_category);
	       $("#upload_content_modal_poll").modal("show");
	    }
	    else if (content_type == 'survey') {
	       $("#add_content_survey")[0].reset();
	       $("#is_main_category_survey").val(is_main_category);
	       $("#upload_content_modal_survey").modal("show");
	    }
	    else if (content_type == 'pdf') {
	       $("#upload_content_modal_pdf_form")[0].reset();
	       $("#is_main_category_pdf").val(is_main_category);
	       $("#upload_content_modal_pdf").modal("show");
	    }
	    else if (content_type == 'upload') {
	       $("#upload_content_modal_upload_form")[0].reset();
	       $("#is_main_category_upload").val(is_main_category);
	       $("#upload_content_modal_upload").modal("show");
	    }
	    else if (content_type == 'wiki') {
	       $("#upload_content_modal_wiki_form")[0].reset();
	       $("#is_main_category_wiki").val(is_main_category);
	       $("#upload_content_modal_wiki").modal("show");
	    }
	    else if (content_type == 'survey_retail_audit') {
	       $("#add_content_survey_retail_audit")[0].reset();
	       $("#is_main_category_survey_retail_audit").val(is_main_category);
	       $("#upload_content_modal_survey_retail_audit").modal("show");
	    }
	    
	    
	 }
	  // function to add sub category content
	 function add_content_text() {
	    $("#create_content_error_text").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_text").val();
	    var content_title = $("#content_title_text").val();
	    var content_desc = $("#content_desc_text").val();
	    if (content_title == '') {
	       $("#create_content_error_text").html("Please Enter Title");
	       return false;
	    }else{
	       var category_id = '0';
	       $(".loading").show();
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#content_title_text").val('');
		     $("#content_desc_text").val('');
		     $("#upload_content_modal_text").modal("hide");
		  }
	       });
	    }
	 }
	 

	 // function to add sub category content
	 function add_content() {
	    $("#create_content_error").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category").val();
	    var content_title = $("#content_title").val();
	    var content_desc = $("#content_desc").val();
	    if (content_title == '') {
	       $("#create_content_error").html("Please Enter Title");
	       return false;
	    }else{
	       var category_id = '0';
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var file_data = $("#content_file").prop("files")[0];
	       if (typeof file_data === "undefined") {
		  $("#create_content_error").html("Please Select File");
		  return false;
	       }else{
		  var file_size = $("#content_file").prop("files")[0].size;
		  file_size = ((file_size/1024)/1024);
		  if (file_size > 500) {
		     $("#create_content_error").html("File size should be max 500MB");
		     return false;
		  }
	       }
	       $(".loading").show();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  xhr: function () {
                           var xhr = new window.XMLHttpRequest();
                           xhr.upload.addEventListener("progress", function (evt) {
                               if (evt.lengthComputable) {
                                   var percentComplete = evt.loaded / evt.total;
                                   percentComplete = parseInt(percentComplete * 100);
                                     $('.progress').removeClass('hide');
                                   $('.myprogress').text(percentComplete + '%');
                                   $('.myprogress').css('width', percentComplete + '%');
                               }
                           }, false);
                           return xhr;
                       },
		  success: function(data){
		     $('.progress').addClass('hide');
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#content_title").val('');
		     $("#content_desc").val('');
		     $("#content_file").val('');
		     $("#upload_content_modal").modal("hide");
		  }
	       });
	    }
	 }
      
      
	 // function to delete content
	 function delete_content(content_id) {
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_content",
		  data: "content_id="+content_id,
		  success: function(data){
		     var main_category = $('.content_section').filter('[data-content_id="'+content_id+'"]').data('main_id');
		     var sub_category = $('.content_section').filter('[data-content_id="'+content_id+'"]').data('sub_id');
		     content_builder(main_category,sub_category);
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 // function to delete content from sub category list
	 function delete_content_on_sub_category(content_id) {
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_content",
		  data: "content_id="+content_id,
		  success: function(data){
		     var main_category = $('.sub_section').filter('[data-sub_id="'+content_id+'"]').data('main_id');
		     content_builder(main_category);
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 // function to delete sub category
	 function delte_sub_category(sub_category_id) {
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_sub_category",
		  data: "sub_category_id="+sub_category_id,
		  success: function(data){
		     var main_category = $('.sub_section').filter('[data-sub_id="'+sub_category_id+'"]').data('main_id');
		    content_builder(main_category);
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 // function to delete main category 
	 function delete_main_category(main_category_id) {
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_main_category",
		  data: "main_category_id="+main_category_id,
		  success: function(data){
		    content_builder();
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 // function to open edit main category popup
	 function edit_main_category(category_id,category_name,category_desc, icon, is_promoter,compliance_cat_id) {
	    $("#icon_validation").val('1');
	    $("#update_main_category_error").html("");
	    $("#edit_new_main_section_form")[0].reset();
	    
	    $("#main_category_id_edit").val(category_id);
	    $("#main_category_name_edit").val(unescape(category_name));
	    $("#main_category_desc_edit").val(unescape(category_desc));
	    if (icon != '') {
		  $("#maine_category_edit_file_previous_name_div").show();
	       }else{
		  $("#maine_category_edit_file_previous_name_div").hide();
	       }
	     $("#main_category_edit_file_previous_name").html(icon);
	     
	   var location_type = $("#location_type").val();
	    if (location_type == '13')
	    {
		$("#promoter_category_div_edit").show();
		var compliance_val = '<option value="">Select Compliance</option>';
		if (compliance_category.length > 0)
		{
		    for (var i = 0; i < compliance_category.length; i++)
		    {
			var category = compliance_category[i];
			var sel = '';
			if (category.id == compliance_cat_id) {
			    sel = "selected";
			}
			compliance_val += '<option value="'+category.id+'" '+sel+'>'+category.category_name+'</option>';
		    }
		}
		$("#compliance_edit").html(compliance_val);
	    }
	    else
	    {
		$("#promoter_category_div_edit").hide();
	    }
	    if (is_promoter == 1) {
		$("input[name=promoter_category_edit]").prop('checked', true);
	    }
	    else
	    {
		$("input[name=promoter_category_edit]").prop('checked', false);
	    }
	    $("#main_category_modal_edit").modal('show');
	 }
	  $(document).on('click','#main_category_edit_file_previous_name_remove_button',function(){
	    $("#main_category_edit_file_previous_name").html("");
	 });
	  $('.icon_upload_class_image').change(function() {
	 var image_id = $(this).attr('id');
	 var fr = new FileReader;
	 fr.onload = function() {
            var img = new Image;
            img.onload = function() {
               image_width = img.width;
               var image_height = img.height;
	       var imgpath=document.getElementById(image_id);
               var image_size = Math.round(imgpath.files[0].size/1024);
               if(image_height == image_width){
                  if( image_width > 100 && image_height >100){
                     $("#icon_validation").val('0');
                  }else{
                     $("#icon_validation").val('1');
                  }
               }
               else{
                  $("#icon_validation").val('0');
               }

            };
            img.onerror = function() {
               $("#icon_validation").val('0');
            };
            img.src = fr.result;
	 };
	 fr.readAsDataURL(this.files[0]);
      });
	 // function to update main category
	 function update_main_category() {
	     var compliance_cat = $("#compliance_edit").val();
            var location_type = $("#location_type").val();
	    var is_promoter_category = 0;
	    if ($('input[name="promoter_category_edit"]').is(':checked')) {
		is_promoter_category = $('input[name="promoter_category_edit"]').val();
	    }
	    else
	    {
		if (compliance_cat == '' && location_type == '13')
		{
		    $("#update_main_category_error").html("Please select compliance category");
		    return false;
		}
	    }
	    $("#update_main_category_error").html("");
	    var main_category_id_edit = $("#main_category_id_edit").val();
	    var main_category_name = $("#main_category_name_edit").val();
	    var main_category_desc = $("#main_category_desc_edit").val();
	    var offline_content_title = $("#offline_content_title").val();
	    var offline_content_desc = $("#offline_content_desc").val();
	    if (main_category_name == '') {
	       $("#update_main_category_error").html("Please Enter Category Name");
	       return false;
	    }else{
	       var icon_validation = $("#icon_validation").val();
	       if (icon_validation == '0') {
		  $("#update_main_category_error").html("Icon should be same hight and width and should be < 100px");
		  return false;
	       }
	       var file_data = $("#main_category_file_edit").prop("files")[0];
	       var file_previous_name = $("#main_category_edit_file_previous_name").html();
	       $(".loading").show();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('main_category_id',main_category_id_edit);
	       form_data.append('main_category_name',main_category_name);
	       form_data.append('main_category_desc',main_category_desc);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append('offline_content_title',offline_content_title);
	       form_data.append('offline_content_desc',offline_content_desc);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("is_promoter_category",is_promoter_category);
	       form_data.append("compliance_cat",compliance_cat);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_main_category",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     content_builder(main_category_id_edit);
		     $("#main_category_modal_edit").modal("hide");
		  }
	       });
	    }
	 }
	 // function to open edit main sub popup
	 function edit_sub_category(main_category_id,sub_category_id,sub_category_name,sub_category_desc,icon) {
	    $("#icon_validation").val('1');
	    $("#update_sub_category_error").html("");
	    $("#edit_new_sub_section_form")[0].reset();
	    
	    $("#sub_category_id_edit_main_category").val(main_category_id);
	    $("#sub_category_id_edit").val(sub_category_id);
	    $("#sub_category_name_edit").val(unescape(sub_category_name));
	    $("#sub_category_desc_edit").val(unescape(sub_category_desc));
	    if (icon != '') {
		  $("#sub_category_edit_file_previous_name_div").show();
	       }else{
		  $("#sub_category_edit_file_previous_name_div").hide();
	       }
	     $("#sub_category_edit_file_previous_name").html(icon);
	    
	    $("#sub_category_modal_edit").modal('show');
	 }
	 $(document).on('click','#sub_category_edit_file_previous_name_remove_button',function(){
	    $("#sub_category_edit_file_previous_name").html("");
	 });
	  // function to update sub category
	 function update_sub_category() {
	    $("#update_sub_category_error").html("");
	    var sub_category_id = $("#sub_category_id_edit").val();
	    var sub_category_name = $("#sub_category_name_edit").val();
	    var sub_category_desc = $("#sub_category_desc_edit").val();
	    if (sub_category_name == '') {
	       $("#update_sub_category_error").html("Please Enter Category Name");
	       return false;
	    }else{
	       var icon_validation = $("#icon_validation").val();
	       if (icon_validation == '0') {
		  $("#update_sub_category_error").html("Icon should be same hight and width and should be < 100px");
		  return false;
	       }
	       var file_data = $("#sub_category_file_edit").prop("files")[0];
	       var file_previous_name = $("#sub_category_edit_file_previous_name").html();
	       $(".loading").show();
	       
	       var category_id = $("#sub_category_id_edit_main_category").val();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('sub_category_id',sub_category_id);
	       form_data.append('sub_category_name',sub_category_name);
	       form_data.append('sub_category_desc',sub_category_desc);
	       form_data.append("category_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", file_previous_name);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_sub_category",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     content_builder(category_id,sub_category_id);
		     $("#sub_category_modal_edit").modal('hide');
		  }
	       });
	    }
	 }
	 // function to open edit content popup
	 function edit_content(upload_type,main_id,sub_id, content_id, content_title, content_file, content_desc, content_type,wiki_title) {
	    if (content_type == 'video' || content_type == 'image' || content_type == 'audio') {
	       $("#edit_image_video_content_form")[0].reset();
	       $("#edit_content_id").val(content_id);
	       $("#edit_content_id_main_category").val(main_id);
	       $("#edit_content_id_sub_category").val(sub_id);
	       $("#content_title_edit").val(unescape(content_title));
	       $("#content_desc_edit").val(unescape(content_desc));
	       if (content_file != '') {
		  $("#file_previous_name_div").show();
	       }else{
		  $("#file_previous_name_div").hide();
	       }
	       $("#file_previous_name").html(content_file);
	       $("#upload_content_modal_edit").modal('show');
	    }
	    else if (content_type == 'text') {
	       $("#edit_content_id_text").val(content_id);
	       $("#edit_content_id_main_category_text").val(main_id);
	       $("#edit_content_id_sub_category_text").val(sub_id);
	       $("#content_title_edit_text").val(unescape(content_title));
	       $("#content_desc_edit_text").val(unescape(content_desc));
	       $("#upload_content_modal_edit_text").modal('show');
	    }
	    else if (content_type == 'poll') {
	       $("#update_content_poll")[0].reset();
	       poll_content_edit_modal_open(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type);
	    }
	    else if (content_type == 'survey') {
		var location_type = $("#location_type").val();
		if (location_type == '13')
		{
		    $("#update_content_survey_retail_audit")[0].reset();
		    survey_content_edit_modal_open_retail_audit(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type);
		}
		else
		{
		    $("#update_content_survey")[0].reset();
		    survey_content_edit_modal_open(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type);
		}
	       
	    }
	    else if (content_type == 'pdf') {
	        $("#edit_pdf_content_form")[0].reset();
	       $("#edit_content_id_pdf").val(content_id);
	       $("#edit_content_id_main_category_pdf").val(main_id);
	       $("#edit_content_id_sub_category_pdf").val(sub_id);
	       $("#content_title_edit_pdf").val(unescape(content_title));
	       $("#content_desc_edit_pdf").val(unescape(content_desc));
	       if (content_file != '') {
		  $("#file_previous_name_div_pdf").show();
	       }else{
		  $("#file_previous_name_div_pdf").hide();
	       }
	       $("#file_previous_name_pdf").html(content_file);
	       $("#upload_content_modal_edit_pdf").modal('show');
	    }
	    else if (content_type == 'upload') {
	       $("#edit_content_id_upload").val(content_id);
	       $("#edit_content_id_main_category_upload").val(main_id);
	       $("#edit_content_id_sub_category_upload").val(sub_id);
	       $("#content_title_edit_upload").val(unescape(content_title));
	       $("#content_desc_edit_upload").val(unescape(content_desc));
	       $("#upload_content_modal_edit_upload").modal('show');
	       if (upload_type == '2' || upload_type == '3') {
		  $('input[name="upload_content_user_video_allow_edit"]').prop('checked', true)
	       }else{
		  $('input[name="upload_content_user_video_allow_edit"]').prop('checked', false)
	       }
	       if (upload_type == '1' || upload_type == '3') {
		  $('input[name="upload_content_user_image_allow_edit"]').prop('checked', true)
	       }else{
		  $('input[name="upload_content_user_image_allow_edit"]').prop('checked', false)
	       }
	       
	    }
	    else if (content_type == 'wiki') {
	       $("#edit_content_id_wiki").val(content_id);
	       $("#edit_content_id_main_category_wiki").val(main_id);
	       $("#edit_content_id_sub_category_wiki").val(sub_id);
	       $("#content_title_edit_wiki").val(unescape(content_title));
	       $("#wiki_title_url_edit").val(unescape(wiki_title));
	       $("#upload_content_modal_edit_wiki").modal('show');
	    }
	    
	 }
	 // function to update content
	 function update_content_text() {
	    $("#edit_content_error_text").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id_text").val();
	    var content_title = $("#content_title_edit_text").val();
	    var content_desc = $("#content_desc_edit_text").val();
	    var content_file = '';
	    var file_previous_name = '';
	    if (content_title == '') {
	       $("#edit_content_error").html("Please Enter Title");
	       return false;
	    }else{
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category_text").val();
	       sub_category = $("#edit_content_id_sub_category_text").val();
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_file", content_file);
	       form_data.append("content_id", content_id);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		    
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_edit_text").modal("hide");
		  }
	       });
	    }
	 }
	// function to update content
	 function update_content() {
	    $("#edit_content_error").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id").val();
	    var content_title = $("#content_title_edit").val();
	    var content_desc = $("#content_desc_edit").val();
	    var content_file = $("#content_file_edit").val();
	    var file_previous_name = $("#file_previous_name").html();
	    if (content_title == '') {
	       $("#edit_content_error").html("Please Enter Title");
	       return false;
	    }else{
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category").val();
	       sub_category = $("#edit_content_id_sub_category").val();
	       var file_data = $("#content_file_edit").prop("files")[0];
	       if (typeof file_data === "undefined") {
	       }else{
		  var file_size = $("#content_file_edit").prop("files")[0].size;
		  file_size = ((file_size/1024)/1024);
		  if (file_size > 500) {
		     $("#edit_content_error").html("File size should be max 500MB");
		     return false;
		  }
	       }
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_file", content_file);
	       form_data.append("content_id", content_id);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  xhr: function () {
                           var xhr = new window.XMLHttpRequest();
                           xhr.upload.addEventListener("progress", function (evt) {
                               if (evt.lengthComputable) {
                                   var percentComplete = evt.loaded / evt.total;
                                   percentComplete = parseInt(percentComplete * 100);
                                     $('.progress').removeClass('hide');
                                   $('.myprogress').text(percentComplete + '%');
                                   $('.myprogress').css('width', percentComplete + '%');
                               }
                           }, false);
                           return xhr;
                       },
		  success: function(data){
		    $('.progress').addClass('hide');
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_edit").modal("hide");
		  }
	       });
	    }
	 }
	 
	 $(document).on('click','#file_previous_name_remove_button',function(){
	    $("#file_previous_name").html("");
	 });
      
      


      function add_content_builder() {
	 $("#add_content_builder_error").html('');
	 var offline_content_title = $("#offline_content_title").val();
	 var offline_content_desc = $("#offline_content_desc").val();
	 if (offline_content_desc == '' || offline_content_title == '') {
	    $("#add_content_builder_error").html('Please fill Title and Description');
	    return false;
	 }
	 var form_data = new FormData(); 
	 form_data.append('offline_content_title',offline_content_title);
	 form_data.append('offline_content_desc',offline_content_desc);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_content_builder",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data){
	       if (Clicked_ButtonValue == 'exit') {
		  window.location = "<?php  echo base_url()?>location";
	       }else{
		  $(".loading").hide();
	       }
	    }
         });
      }
	 
	 
      
      
	 
	 $("#main_category_name").keyup(function() {
	    el = $(this);
	    if (el.val().length > 30) {
		el.val(el.val().substr(0, 30));
	    } else {
		$(".main_category_name_length").text(el.val().length);
	    }
	 });
	 $("#main_category_name_edit").keyup(function() {
	    el = $(this);
	    if (el.val().length > 30) {
		el.val(el.val().substr(0, 30));
	    } else {
		$(".main_category_name_length").text(el.val().length);
	    }
	 });
	 $("#sub_category_name").keyup(function() {
	    el = $(this);
	    if (el.val().length > 30) {
		el.val(el.val().substr(0, 30));
	    } else {
		$(".sub_category_name_length").text(el.val().length);
	    }
	 });
	 $("#sub_category_name_edit").keyup(function() {
	    el = $(this);
	    if (el.val().length > 30) {
		el.val(el.val().substr(0, 30));
	    } else {
		$(".sub_category_name_length").text(el.val().length);
	    }
	 });
	 
	
	
	 $('.modal').on('hidden.bs.modal', function (e) {
	    if($('.modal').hasClass('in')) {
	    $('body').addClass('modal-open');
	    }    
	 });
	 
	 function add_content_poll() {
	    $("#create_content_error_poll").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_poll").val();
	    var content_title = $("#content_title_poll").val();
	    var poll_querstion = $("#poll_question").val();
	    var option_one = $("#poll_option_one").val();
	    var option_two = $("#poll_option_two").val();
	    var option_three = $("#poll_option_three").val();
	    var option_four = $("#poll_option_four").val();
	    var option_five = $("#poll_option_five").val();
	    var option_six = $("#poll_option_six").val();
	    if (content_title == '' || poll_querstion == '' || option_one == '' || option_two == '') {
	       $("#create_content_error_poll").html("Please Fill mandatory fields");
	       return false;
	    }else{
	       $(".loading").show();
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       
	       var file_data = $("#pole_file").prop("files")[0];
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("poll_querstion",poll_querstion);
	       form_data.append("option_one",option_one);
	       form_data.append("option_two",option_two);
	       form_data.append("option_three",option_three);
	       form_data.append("option_four",option_four);
	       form_data.append("option_five",option_five);
	       form_data.append("option_six",option_six);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_poll",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#add_content_poll")[0].reset();
		     $("#upload_content_modal_poll").modal("hide");
		  }
	       });
	    }
	 }
	 

	
	 function poll_content_edit_modal_open(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type){
	    $("#edit_content_id_poll").val(content_id);
	       $("#edit_content_id_main_category_poll").val(main_id);
	       $("#edit_content_id_sub_category_poll").val(sub_id);
	       $("#content_title_poll_edit").val(unescape(content_title));
	       if (content_file != '') {
		  $("#poll_edit_file_previous_name_div").show();
	       }else{
		  $("#poll_edit_file_previous_name_div").hide();
	       }
	       $("#poll_edit_file_previous_name").html(content_file);
	       $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/get_content_poll_survey",
	       data: "content_id="+content_id,
	       dataType: 'json',
	       success: function(data){
		  $(".loading").hide();
		  if (data.question.length > 0) {
		     var obj = data.question[0];
		     $("#poll_question_edit").val(obj.question);
		    
		     if (obj.options.length > 0) {
			for (var i = 0; i < obj.options.length; i++) {
			   var op_obj = obj.options[i];
			   var option_id = "poll_option_one_edit";
			   if (i == '1') {
			      option_id = "poll_option_two_edit";
			   }
			   else if (i == '2') {
			      option_id = "poll_option_three_edit";
			   }
			   else if (i == '3') {
			      option_id = "poll_option_four_edit";
			   }
			   else if (i == '4') {
			      option_id = "poll_option_five_edit";
			   }
			   else if (i == '5') {
			      option_id = "poll_option_six_edit";
			   }
			   $("#"+option_id).val(op_obj.option_text);
			}
			
			
		     }
		  }
		     $("#upload_content_modal_poll_edit").modal("show");
		    
	       }
	    });

	 }
	 $(document).on('click','#poll_edit_file_previous_name_remove_button',function(){
	    $("#poll_edit_file_previous_name").html("");
	 });
      	 function update_content_poll() {
	    $("#create_content_error_poll_edit").html("");
	    var content_id = $("#edit_content_id_poll").val();
	    var main_category = $("#edit_content_id_main_category_poll").val();
	    var sub_category = $("#edit_content_id_sub_category_poll").val();
	    var content_title = $("#content_title_poll_edit").val();
	    var poll_querstion = $("#poll_question_edit").val();
	    var option_one = $("#poll_option_one_edit").val();
	    var option_two = $("#poll_option_two_edit").val();
	    var option_three = $("#poll_option_three_edit").val();
	    var option_four = $("#poll_option_four_edit").val();
	    var option_five = $("#poll_option_five_edit").val();
	    var option_six = $("#poll_option_six_edit").val();
	    if (content_title == '' || poll_querstion == '' || option_one == '' || option_two == '') {
	       $("#create_content_error_poll_edit").html("Please Fill mandatory fields");
	       return false;
	    }else{
	       $(".loading").show();
	       
	       var file_data = $("#pole_file_edit").prop("files")[0];
	       var file_previous_name = $("#poll_edit_file_previous_name").html();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       //form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", content_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("poll_querstion",poll_querstion);
	       form_data.append("option_one",option_one);
	       form_data.append("option_two",option_two);
	       form_data.append("option_three",option_three);
	       form_data.append("option_four",option_four);
	       form_data.append("option_five",option_five);
	       form_data.append("option_six",option_six);
	       form_data.append("file_previous_name",file_previous_name);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/update_content_poll",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     
		     content_builder(main_category,sub_category);
		     $("#update_content_poll")[0].reset();
		     $("#upload_content_modal_poll_edit").modal("hide");
		  }
	       });
	    }
	 }
	 

	

	 


	 


	 

	 
	// on click of new question add add new question div
	 $(document).on('click', '.add_new_question', function(){
	    var num_of_question = $("#total_survey_question_created").val();
	    num_of_question++;
	    $("#total_survey_question_created").val(num_of_question);
	    if (num_of_question > 9) {
	       num_of_question = "QUESTION "+num_of_question;
	    }else{
	       num_of_question = "QUESTION 0"+num_of_question;
	    }
	    
	    var new_div = '<div class="survey_div">'+
				    '<div class="form-group">'+
				       '<label style="color:#f00f64">'+num_of_question+'</label>'+
				       '<label class="pull-right remove_survey_question" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				       '<select class="form-control survey_type_class" name="survey_type[]">'+
					  '<option value="1">Text Entry</option>'+
					  '<option value="2">Single Choice</option>'+
					  '<option value="3">Multi Coice</option>'+
				       '</select>'+
				    '</div>'+
				    '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question[]" class = "survey_question_class"></textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				    //'</div><div class="survey_div_option"></div>'+
				 '</div>';
	    $("#survey_question_div").append(new_div);
	 });
	 
	 
	 // remove question div
	 $(document).on('click', '.remove_survey_question', function (){
	    $(this).parent().parent().remove();
	 });
	 // on change of survey type hide and show option
	 $(document).on('change', '.survey_type_class', function(){
	    var survey_type = $(this).val();
	    
	    if (survey_type != '1') {
	       var new_options = '<div class="survey_div_option"><legend style="margin-bottom:0px;font-size:14px; color:#414042">ANSWER OPTIONS  <small style="color:#999999">(Upto 6 Options)</small></legend>'+
	       '<div class="mui-textfield mui-textfield--float-label survey_option_count">'+
                  '<input type="text" name="options[]" class = "survey_option_class">'+
                  '<label>Option A<sup>*</sup></label>'+
               '</div>'+
	       '<div class="form-group survey_option_count">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options[]" class = "survey_option_class">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option B<sup>*</sup></label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-plus-square add_survey_option" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>'+
	       '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div").find('.survey_div_option').find('.survey_option_count') ).each(function() {
			total_option++;
	       });
	       if (total_option == 0) {
		  $(this).closest(".survey_div").append(new_options);
	       }
	       
	    }else{
	       $(this).closest(".survey_div").find('.survey_div_option').remove();
	    }
	 });
	 // on click add option add option
	 $(document).on('click', '.add_survey_option', function(){
	    var new_options = '<div class="form-group survey_option_count">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options[]" class = "survey_option_class">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option</label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-minus-square remove_survey_option" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_option").find('.survey_option_count') ).each(function() {
			total_option++;
		     });
	       if (total_option < 6) {
		  $(this).closest(".survey_div_option").append(new_options);
	       }
	       
	 });
	 // on click of button remove option
	 $(document).on('click', '.remove_survey_option', function(){
	    $(this).parent().parent().parent().parent().remove();
	 });
	 
	 
	 
	 function add_content_survey() {
	    $("#create_content_error_survey").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_survey").val();
	    var content_title = $("#content_title_survey").val();
	   
	    if (content_title == '') {
	       $("#create_content_error_survey").html("Please Fill mandatory fields");
	       return false;
	    }else{
	      if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var questions_array = {};
	       var i = 0;
	       var survey_error = 0;
	       $( ".survey_div" ).each(function() {
		  var question_type = 0;
		  var question= '';
		  question_type = $(this).find('.survey_type_class').val();
		  question = $(this).find('.survey_question_class').val();
		  if (question == '') {
		     survey_error = 1;
		  }
		  var options_array = {};
		  if (question_type == '2' || question_type == '3') {
		     var j = 0;
		     var total_option = 0;
		     $( $(this).find('.survey_div_option').find('.survey_option_count') ).each(function() {
			var option_val = '';
			option_val = $(this).find('.survey_option_class').val();
			if (typeof option_val === "undefined") {
			   
			}else{
			   if(option_val != ''){
			      total_option++;
			      options_array[j] = option_val;
			      j++;
			   }
			   
			}
			
		     });
		     if (question_type != 1 && total_option < 2) {
			survey_error = 1;
		     }
		  }
		  
		  
		  
		  questions_array[i] =  { question_type : question_type , question: question, options: options_array };
		  i++;
	       });
	       if (survey_error == '1') {
		  $("#create_content_error_survey").html("Please Fill mandatory fields");
		  return false;
	       }
	        $(".loading").show();
	       questions_array = JSON.stringify(questions_array); 
	       var file_data = $("#survey_file").prop("files")[0];
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("questions", questions_array);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_survey",
		  cache: false,
		  contentType: false,
		  processData: false,
		  dataType: "json",
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#add_content_survey")[0].reset();
		     $("#upload_content_modal_survey").modal("hide");
		  }
	       });
	    }
	 }
	
	
	
	 function survey_content_edit_modal_open(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type){
	    $("#edit_content_id_survey").val(content_id);
	       $("#edit_content_id_main_category_survey").val(main_id);
	       $("#edit_content_id_sub_category_survey").val(sub_id);
	       $("#content_title_survey_edit").val(unescape(content_title));
	       if (content_file != '') {
		  $("#survey_edit_file_previous_name_div").show();
	       }else{
		  $("#survey_edit_file_previous_name_div").hide();
	       }
	       $("#survey_edit_file_previous_name").html(content_file);
	       $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/get_content_poll_survey",
	       data: "content_id="+content_id,
	       dataType: 'json',
	       success: function(data){
		  $(".loading").hide();
		  $('#survey_question_div_edit').html("");
		  if (data.question.length > 0) {
		     for (var i = 0; i < data.question.length; i++) {
			var obj = data.question[i];
			var question_type = obj.survey_type;
			var question = obj.question;
			var options_array = obj.options;
			var main_div = '';
			var question_number = i+1;
			if (question_number < 10) {
			   question_number = '0'+question_number;
			}
			var level = '';
			if (i != 0) {
			   level =  '<label class="pull-right remove_survey_question_edit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>';
			}
			var text_entry = ''; var single_choice = ''; var multi_choice = '';
			if (question_type == '1') {
			   text_entry = 'selected';
			}
			else if (question_type == '2') {
			   single_choice = 'selected';
			}
			else if (question_type == '3') {
			   multi_choice = 'selected';
			}
			main_div += '<div class="survey_div_edit">';
			main_div += '<div class="form-group">'+
					  '<label style="color:#f00f64">QUESTION '+question_number+'</label>'+level+
					  '<select class="form-control survey_type_class_edit" name="survey_type_edit[]">'+
					     '<option value="1" '+text_entry+'>Text Entry</option>'+
					     '<option value="2" '+single_choice+'>Single Choice</option>'+
					     '<option value="3" '+multi_choice+'>Multi Coice</option>'+
					  '</select>'+
				       '</div>';
			main_div += '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_edit[]" class = "survey_question_class_edit">'+question+'</textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				    '</div>';
			if (options_array.length > 0) {
			   main_div += '<div class="survey_div_option_edit">';
			   main_div += '<legend style="margin-bottom:0px;font-size:14px; color:#414042">'+
					  'ANSWER OPTIONS'+
					  '<small style="color:#999999">(Upto 6 Options)</small>'+
					  '</legend>';
			   for (var j = 0; j < options_array.length; j++) {
			      var op_obj = options_array[j];
			      var option_val = op_obj.option_text;
			      if (j == 0) {
			      main_div += '<div class="mui-textfield mui-textfield--float-label survey_option_count_edit">'+
					  '<input class="survey_option_class_edit mui--is-empty mui--is-untouched mui--is-pristine" type="text" name="options_edit[]" value="'+option_val+'">'+
					  '<label>'+
					  'Option'+
					  '<sup>*</sup>'+
					  '</label>'+
					  '</div>';
			      }else if (j == 1) {
				 main_div += '<div class="form-group survey_option_count_edit">'+
				 '<div class="row">'+
				 '<div class="col-sm-10 col-xs-10 nopadding-right">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				 '<input type="text" class="survey_option_class_edit mui--is-empty mui--is-untouched mui--is-pristine" name="options_edit[]" value="'+option_val+'">'+
				 '<label style="font-weight:300;color:#a7a9ac">Option<sup>*</sup></label>'+
				 '</div>'+
				 '</div>'+
				 '<div style="padding-top:3%" class="col-sm-2 col-xs-2">'+
				 '<span class="btn-append"> <i style="font-family:FontAwesome" class="far fa-plus-square add_survey_option_edit"></i></span>'+
				' </div>'+
				 '</div>'+
				 '</div>';
			      }
			      else{
				 main_div += '<div class="form-group survey_option_count_edit">'+
				 '<div class="row">'+
				 '<div class="col-sm-10 col-xs-10 nopadding-right">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				 '<input type="text" class="survey_option_class_edit mui--is-empty mui--is-untouched mui--is-pristine" name="options_edit[]" value="'+option_val+'">'+
				 '<label style="font-weight:300;color:#a7a9ac">Option<sup>*</sup></label>'+
				 '</div>'+
				 '</div>'+
				 '<div style="padding-top:3%" class="col-sm-2 col-xs-2">'+
				 '<span class="btn-append"> <i style="font-family:FontAwesome" class="far fa-minus-square remove_survey_option_edit"></i></span>'+
				' </div>'+
				 '</div>'+
				 '</div>';
			      }
			   }
			   main_div += '</div>';
			}
			main_div += '</div>';
			$('#survey_question_div_edit').append(main_div);
		     }
		  }
		     $("#upload_content_modal_survey_edit").modal("show");
		    
	       }
	    });

	 }
	 $(document).on('click','#survey_edit_file_previous_name_remove_button',function(){
	    $("#survey_edit_file_previous_name").html("");
	 });
	 
	 function update_content_survey() {
	    $("#create_content_error_survey_edit").html("");
	    var content_id = $("#edit_content_id_survey").val();
	    var main_category = $("#edit_content_id_main_category_survey").val();
	    var sub_category = $("#edit_content_id_sub_category_survey").val();
	    var content_title = $("#content_title_survey_edit").val();
	    
	    if (content_title == '') {
	       $("#create_content_error_survey_edit").html("Please Fill mandatory fields");
	       return false;
	    }else{
	       
	       var questions_array = {};
	       var i = 0;
	       var survey_error = 0;
	       $( ".survey_div_edit" ).each(function() {
		  var question_type = 0;
		  var question= '';
		  question_type = $(this).find('.survey_type_class_edit').val();
		  question = $(this).find('.survey_question_class_edit').val();
		  if (question == '') {
		     survey_error = 1;
		  }
		  var options_array = {};
		  if (question_type == '2' || question_type == '3') {
		     var j = 0;
		     var total_option = 0;
		     $( $(this).find('.survey_div_option_edit').find('.survey_option_count_edit') ).each(function() {
			var option_val = '';
			option_val = $(this).find('.survey_option_class_edit').val();
			if (typeof option_val === "undefined") {
			   
			}else{
			   if(option_val != ''){
			      total_option++;
			      options_array[j] = option_val;
			      j++;
			   }
			   
			}
			
		     });
		     if (question_type != 1 && total_option < 2) {
			survey_error = 1;
		     }
		  }
		  
		  
		  
		  questions_array[i] =  { question_type : question_type , question: question, options: options_array };
		  i++;
	       });
	       if (survey_error == '1') {
		  $("#create_content_error_survey_edit").html("Please Fill mandatory fields");
		  return false;
	       }
	       questions_array = JSON.stringify(questions_array);
	       $(".loading").show();
	       
	       var file_data = $("#survey_file_edit").prop("files")[0];
	       var file_previous_name = $("#survey_edit_file_previous_name").html();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       //form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", content_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name",file_previous_name);
	       form_data.append("questions", questions_array);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/update_content_survey",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     
		     content_builder(main_category,sub_category);
		     $("#update_content_survey")[0].reset();
		     $("#upload_content_modal_survey_edit").modal("hide");
		  }
	       });
	    }
	 }
	 

	

	 


	 


	 

	 



	
	
	 // on click of new question add add new question div
	 $(document).on('click', '.add_new_question_edit', function(){
	   var num_of_question = $("#total_survey_question_created_edit").val();
	    num_of_question++;
	    $("#total_survey_question_created_edit").val(num_of_question);
	    if (num_of_question > 9) {
	       num_of_question = "QUESTION "+num_of_question;
	    }else{
	       num_of_question = "QUESTION 0"+num_of_question;
	    }
	    
	    var new_div = '<div class="survey_div_edit">'+
				    '<div class="form-group">'+
				       '<label style="color:#f00f64">'+num_of_question+'</label>'+
				       '<label class="pull-right remove_survey_question_edit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				       '<select class="form-control survey_type_class_edit" name="survey_type_edit[]">'+
					  '<option value="1">Text Entry</option>'+
					  '<option value="2">Single Choice</option>'+
					  '<option value="3">Multi Coice</option>'+
				       '</select>'+
				    '</div>'+
				    '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_edit[]" class = "survey_question_class_edit"></textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				    //'</div><div class="survey_div_option"></div>'+
				 '</div>';
	    $("#survey_question_div_edit").append(new_div);
	 });
	 // remove question div
	 $(document).on('click', '.remove_survey_question_edit', function (){
	    $(this).parent().parent().remove();
	 });
	 // on change of survey type hide and show option
	 $(document).on('change', '.survey_type_class_edit', function(){
	    var survey_type = $(this).val();
	    
	    if (survey_type != '1') {
	       var new_options = '<div class="survey_div_option_edit"><legend style="margin-bottom:0px;font-size:14px; color:#414042">ANSWER OPTIONS  <small style="color:#999999">(Upto 6 Options)</small></legend>'+
	       '<div class="mui-textfield mui-textfield--float-label survey_option_count_edit">'+
                  '<input type="text" name="options_edit[]" class = "survey_option_class_edit">'+
                  '<label>Option A<sup>*</sup></label>'+
               '</div>'+
	       '<div class="form-group survey_option_count_edit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_edit[]" class = "survey_option_class_edit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option B<sup>*</sup></label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-plus-square add_survey_option_edit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>'+
	       '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_edit").find('.survey_div_option_edit').find('.survey_option_count_edit') ).each(function() {
			total_option++;
	       });
	       if (total_option == 0) {
		  $(this).closest(".survey_div_edit").append(new_options);
	       }
	       
	    }else{
	       $(this).closest(".survey_div_edit").find('.survey_div_option_edit').remove();
	    }
	 });
	 // on click add option add option
	 $(document).on('click', '.add_survey_option_edit', function(){
	    var new_options = '<div class="form-group survey_option_count_edit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_edit[]" class = "survey_option_class_edit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option</label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-minus-square remove_survey_option_edit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_option_edit").find('.survey_option_count_edit') ).each(function() {
			total_option++;
		     });
	       if (total_option < 6) {
		  $(this).closest(".survey_div_option_edit").append(new_options);
	       }
	       
	 });
	 // on click of button remove option
	 $(document).on('click', '.remove_survey_option_edit', function(){
	    $(this).parent().parent().parent().parent().remove();
	 });
	 
	 
	 
	  // function to add pdf
	 function add_content_pdf() {
	    $("#create_content_error_pdf").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_pdf").val();
	    var content_title = $("#content_title_pdf").val();
	    var content_desc = $("#content_desc_pdf").val();
	    if (content_title == '') {
	       $("#create_content_error_pdf").html("Please Enter Title");
	       return false;
	    }else{
	       var category_id = '0';
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var file_data = $("#content_file_pdf").prop("files")[0];
	       if (typeof file_data === "undefined") {
		  $("#create_content_error_pdf").html("Please Select File");
		  return false;
	       }
	       $(".loading").show();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_pdf",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_pdf").modal("hide");
		  }
	       });
	    }
	 }
	
      
	 // update pdf type content
	 function update_content_pdf() {
	    $("#edit_content_error").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id_pdf").val();
	    var content_title = $("#content_title_edit_pdf").val();
	    var content_desc = $("#content_desc_edit_pdf").val();
	    var content_file = $("#content_file_edit_pdf").val();
	    var file_previous_name = $("#file_previous_name_pdf").html();
	    if (content_title == '') {
	       $("#edit_content_error_pdf").html("Please Enter Title");
	       return false;
	    }else{
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category_pdf").val();
	       sub_category = $("#edit_content_id_sub_category_pdf").val();
	       var file_data = $("#content_file_edit_pdf").prop("files")[0];
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_file", content_file);
	       form_data.append("content_id", content_id);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_pdf",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		    
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_edit_pdf").modal("hide");
		  }
	       });
	    }
	 }
     
	 
	 
	  // function to add content upload type
	 function add_content_upload() {
	    $("#create_content_error_upload").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_upload").val();
	    var content_title = $("#content_title_upload").val();
	    var content_desc = $("#content_desc_upload").val();
	    if (content_title == '') {
	       $("#create_content_error_upload").html("Please Enter Title");
	       return false;
	    }else{
	       var upload_type = 0;
	       var image_allow = 0;
	       var video_allow = 0;
	       if ($('input[name="upload_content_user_image_allow"]:visible').is(':checked')) {
		  image_allow = 1;
		  upload_type = 1;
	       }
	       if ($('input[name="upload_content_user_video_allow"]:visible').is(':checked')) {
		  video_allow = 1;
		  upload_type = 2;
	       }
	       if (image_allow == 0 && video_allow == 0) {
		  $("#create_content_error_upload").html("Please atlist one content type allow");
		  return false;
	       }
	       if (image_allow == 1 && video_allow == 1) {
		  upload_type = 3;
	       }
	       var category_id = '0';
	       $(".loading").show();
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       form_data.append("upload_type", upload_type);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_upload",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#content_title_text").val('');
		     $("#content_desc_text").val('');
		     $("#upload_content_modal_upload").modal("hide");
		  }
	       });
	    }
	 }
	 
	 function update_content_upload() {
	    $("#edit_content_error_upload").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id_upload").val();
	    var content_title = $("#content_title_edit_upload").val();
	    var content_desc = $("#content_desc_edit_upload").val();
	   var file_previous_name = '';
	    if (content_title == '') {
	       $("#edit_content_error_upload").html("Please Enter Title");
	       return false;
	    }else{
	       var upload_type = 0;
	       var image_allow = 0;
	       var video_allow = 0;
	       if ($('input[name="upload_content_user_image_allow_edit"]:visible').is(':checked')) {
		  image_allow = 1;
		  upload_type = 1;
	       }
	       if ($('input[name="upload_content_user_video_allow_edit"]:visible').is(':checked')) {
		  video_allow = 1;
		  upload_type = 2;
	       }
	       if (image_allow == 0 && video_allow == 0) {
		  $("#edit_content_error_upload").html("Please atlist one content type allow");
		  return false;
	       }
	       if (image_allow == 1 && video_allow == 1) {
		  upload_type = 3;
	       }
	       
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category_upload").val();
	       sub_category = $("#edit_content_id_sub_category_upload").val();
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", content_id);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("upload_type", upload_type);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_upload",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		    
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_edit_upload").modal("hide");
		  }
	       });
	    }
	 }
	 
	 
	 
	  // function to add sub category content
	 function add_content_wiki() {
	    $('.wikipedia_temp_data').html('');
	    $("#create_content_error_wiki").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_wiki").val();
	    var content_title = $("#content_title_wiki").val();
	    var wiki_title = $("#wiki_title_url").val();
	    if (content_title == '' || wiki_title == '') {
	       $("#create_content_error_wiki").html("Please Enter Title");
	       return false;
	    }else{
	       var category_id = '0';
	       $(".loading").show();
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       
	       
	       var search = wiki_title;
	       if (search.indexOf("http://") == 0 || search.indexOf("https://") == 0) {
		  var index = search.lastIndexOf("/") + 1;
		  search = search.substr(index);
	       }
	       $.ajax({
		  type: "GET",
		  url: "https://en.wikipedia.org/w/api.php?action=parse&format=json&prop=text|images&page="+search+"&callback=?",
		  contentType: "application/json; charset=utf-8",
		  async: false,
		  dataType: "json",
		  success: function (data, textStatus, jqXHR) {
		     
		     //alert(JSON.stringify(data));
		     //console.log(data);
		     if (typeof data.parse == "undefined") {
			$("#create_content_error_wiki").html("No data found");
			$(".loading").hide();
		     }
		     else {
			var markup = data.parse.text["*"];
			var title = data.parse.title;
			var blurb = $('<div></div>').html(markup);
			blurb.find('a').each(function() { $(this).replaceWith($(this).html()); });
			blurb.find('sup').remove();
			blurb.find('.mw-ext-cite-error').remove();
			blurb.find('.mw-editsection').remove();
			blurb.find('h2').children('span#References').remove();
			blurb.find('h2').children('span#External_links').remove();
			blurb.find('.reflist').remove();
			blurb.find('.plainlinks').remove();
			blurb.find('.navbox').remove();
			$('.wikipedia_temp_data').html(blurb);
			var modified_html = $('.wikipedia_temp_data').html();
			markup = modified_html;
			//var markup = JSON.stringify(data.parse.text["*"]);
			var title = data.parse.title;
			var pageid = data.parse.pageid;
			
			var form_data = new FormData();
			form_data.append('category_id',category_id);
			form_data.append('is_main_category',is_main_category);
			form_data.append('content_title',content_title);
			form_data.append('wikipedia_title_url',wiki_title);
			form_data.append('wikipedia_title',title);
			form_data.append('wikipedia_pageid',pageid);
			form_data.append('wikipedia_content',markup);
			form_data.append("content_id", '');
			form_data.append("location_uid", $("#location_id_hidden").val());
			form_data.append("locationid", $("#created_location_id").val());
			$.ajax({
			   type: "POST",
			   url: "<?php echo base_url()?>location/add_content_wiki",
			   cache: false,
			   contentType: false,
			   processData: false,
			   data: form_data,
			   success: function(data){
			      if (data == '1') {
				 content_builder(main_category,sub_category);
				 $("#wiki_title").val('');
				 $("#upload_content_modal_wiki").modal("hide");
			      }else{
				 $("#create_content_error_wiki").html("No data found");
				 $(".loading").hide();
			      }
			      
			   }
			});
			
		     }
	       },
	       error: function (errorMessage) {
		  $(".loading").hide();
	       }
	    });
	       
	       
	       
	       
	    }
	 }
	 
	 function update_content_wiki() {
	    $('.wikipedia_temp_data').html('');
	    $("#edit_content_error_wiki").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id_wiki").val();
	    var content_title = $("#content_title_edit_wiki").val();
	    var wiki_title = $("#wiki_title_url_edit").val();
	    if (content_title == '' || wiki_title == '') {
	       $("#edit_content_error_wiki").html("Please Enter Title");
	       return false;
	    }else{
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category_wiki").val();
	       sub_category = $("#edit_content_id_sub_category_wiki").val();
	       
	       
	       var search = wiki_title;
	       if (search.indexOf("http://") == 0 || search.indexOf("https://") == 0) {
		  var index = search.lastIndexOf("/") + 1;
		  search = search.substr(index);
	       }
	       $.ajax({
		  type: "GET",
		  url: "https://en.wikipedia.org/w/api.php?action=parse&format=json&prop=text|images&page="+search+"&callback=?",
		  contentType: "application/json; charset=utf-8",
		  async: false,
		  dataType: "json",
		  success: function (data, textStatus, jqXHR) {
		     
		     //alert(JSON.stringify(data));
		     //console.log(data);
		     if (typeof data.parse == "undefined") {
			$("#edit_content_error_wiki").html("No data found");
			$(".loading").hide();
		     }
		     else {
			var markup = data.parse.text["*"];
			var title = data.parse.title;
			var blurb = $('<div></div>').html(markup);
			blurb.find('a').each(function() { $(this).replaceWith($(this).html()); });
			blurb.find('sup').remove();
			blurb.find('.mw-ext-cite-error').remove();
			blurb.find('.mw-editsection').remove();
			blurb.find('h2').children('span#References').remove();
			blurb.find('h2').children('span#External_links').remove();
			blurb.find('.reflist').remove();
			blurb.find('.plainlinks').remove();
			blurb.find('.navbox').remove();
			$('.wikipedia_temp_data').html(blurb);
			var modified_html = $('.wikipedia_temp_data').html();
			markup = modified_html;
			//var markup = JSON.stringify(data.parse.text["*"]);
			var title = data.parse.title;
			var pageid = data.parse.pageid;
			
			var form_data = new FormData();
			form_data.append('category_id',category_id);
			form_data.append('is_main_category',is_main_category);
			form_data.append('content_title',content_title);
			form_data.append('wikipedia_title_url',wiki_title);
			form_data.append('wikipedia_title',title);
			form_data.append('wikipedia_pageid',pageid);
			form_data.append('wikipedia_content',markup);
			form_data.append("content_id", content_id);
			form_data.append("location_uid", $("#location_id_hidden").val());
			form_data.append("locationid", $("#created_location_id").val());
			$.ajax({
			   type: "POST",
			   url: "<?php echo base_url()?>location/add_content_wiki",
			   cache: false,
			   contentType: false,
			   processData: false,
			   data: form_data,
			   success: function(data){
			      if (data == '1') {
				 content_builder(main_category,sub_category);
				 $("#wiki_title").val('');
				 $("#upload_content_modal_edit_wiki").modal("hide");
			      }else{
				 $("#edit_content_error_wiki").html("No data found");
				 $(".loading").hide();
			      }
			      
			   }
			});
			
		     }
		  },
		  error: function (errorMessage) {
		     $(".loading").hide();
		  }
	       });
	       
	       
	       
	       
	    }
	 }
	 
	 
	// for audit survey
	// on click of new question add add new question div
	 $(document).on('click', '.add_new_question_retail_audit', function(){
	    var num_of_question = $("#total_survey_question_created_retail_audit").val();
	    num_of_question++;
	    $("#total_survey_question_created_retail_audit").val(num_of_question);
	    if (num_of_question > 9) {
	       num_of_question = "QUESTION "+num_of_question;
	    }else{
	       num_of_question = "QUESTION 0"+num_of_question;
	    }
	    
	    var new_div = '<div class="survey_div_retail_audit">'+
				    '<div class="form-group">'+
				       '<label style="color:#f00f64">'+num_of_question+'</label>'+
				       '<label style="padding-left:20%;">Deduct: <input type="text" class= "health_deduct_retail_audit" name="health_deduct_retail_audit[]" style="width:50px;"> % from store health</label>'+
				       '<label class="pull-right remove_survey_question_retail_audit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				       /*'<select class="form-control survey_type_class_retail_audit" name="survey_type_retail_audit[]">'+
					  '<option value="">Select Survey Type</option>'+
					  '<option value="2">Single Choice</option>'+
					  '<option value="3">Multi Coice</option>'+
				       '</select>'+*/
				    '</div>'+
				    
				    '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_retail_audit[]" class = "survey_question_class_retail_audit"></textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				       '</div>'+
				    //'</div><div class="survey_div_option"></div>'+
				    '<div class="form-group">'+
					'<div class="checkbox" style="margin-top:0px">'+
					    '<label>'+
					    '<input  class= "use_sku_survey_retail_audit" name="use_sku_survey_retail_audit[]" value="1" type="checkbox"> User SKU List'+
					    '</label>'+
				       '</div>'+
				    '</div>'+
				 '</div>';
	    $("#survey_question_div_retail_audit").append(new_div);
	 });
	$(document).on('click', '.remove_survey_question_retail_audit', function (){
	    $(this).parent().parent().remove();
	 });
	// on change of survey type hide and show option
	 $(document).on('change', '.survey_type_class_retail_audit', function(){
	    var survey_type = $(this).val();
	    if (survey_type != '1') {
	       var new_options = '<div class="survey_div_option_retail_audit"><legend style="margin-bottom:0px;font-size:14px; color:#414042">ANSWER OPTIONS  <small style="color:#999999">(Upto 6 Options)</small></legend>'+
	       '<div class="mui-textfield mui-textfield--float-label survey_option_count_retail_audit">'+
                  '<input type="text" name="options_retail_audit[]" class = "survey_option_class_retail_audit">'+
                  '<label>Option A<sup>*</sup></label>'+
               '</div>'+
	       '<div class="form-group survey_option_count_retail_audit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_retail_audit[]" class = "survey_option_class_retail_audit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option B<sup>*</sup></label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-plus-square add_survey_option_retail_audit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>'+
	       '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_retail_audit").find('.survey_div_option_retail_audit').find('.survey_option_count_retail_audit') ).each(function() {
			total_option++;
	       });
	       if (total_option == 0) {
		  $(this).closest(".survey_div_retail_audit").append(new_options);
	       }
	       
	    }else{
	       $(this).closest(".survey_div_retail_audit").find('.survey_div_option_retail_audit').remove();
	    }
	 });
	// on click add option add option
	 $(document).on('click', '.add_survey_option_retail_audit', function(){
	    var new_options = '<div class="form-group survey_option_count_retail_audit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_retail_audit[]" class = "survey_option_class_retail_audit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option</label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-minus-square remove_survey_option_retail_audit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_option_retail_audit").find('.survey_option_count_retail_audit') ).each(function() {
			total_option++;
		     });
	       if (total_option < 6) {
		  $(this).closest(".survey_div_option_retail_audit").append(new_options);
	       }
	       
	 });
	 $(document).on('click', '.remove_survey_option_retail_audit', function(){
	    $(this).parent().parent().parent().parent().remove();
	 });
	 function add_content_survey_retail_audit() {
	    $("#create_content_error_survey_retail_audit").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_survey_retail_audit").val();
	    var content_title = $("#content_title_survey_retail_audit").val();
	   
	    if (content_title == '') {
	       $("#create_content_error_survey_retail_audit").html("Please Fill mandatory fields");
	       return false;
	    }else{
	      if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var questions_array = {};
	       var i = 0;
	       var survey_error = 0;
	       $( ".survey_div_retail_audit" ).each(function() {
		  var question_type = 0;
		  var health_deduct = 0;
		  var is_sku_use = 0;
		  var question= '';
		    if ($(this).find('.use_sku_survey_retail_audit').is(':checked')) {
			is_sku_use = 1;
		    }
		  health_deduct = $(this).find('.health_deduct_retail_audit').val();
		  //question_type = $(this).find('.survey_type_class_retail_audit').val();
		  question_type = '2';
		  question = $(this).find('.survey_question_class_retail_audit').val();
		  if (question == '') {
		     survey_error = 1;
		  }
		  var options_array = {};
		  /*if (question_type == '2' || question_type == '3') {
		     var j = 0;
		     var total_option = 0;
		     $( $(this).find('.survey_div_option_retail_audit').find('.survey_option_count_retail_audit') ).each(function() {
			var option_val = '';
			option_val = $(this).find('.survey_option_class_retail_audit').val();
			if (typeof option_val === "undefined") {
			   
			}else{
			   if(option_val != ''){
			      total_option++;
			      options_array[j] = option_val;
			      j++;
			   }
			   
			}
			
		     });
		     if (question_type != 1 && total_option < 2) {
			survey_error = 1;
		     }
		  }*/
		  
		  
		  
		  questions_array[i] =  {is_sku_use: is_sku_use, health_deduct: health_deduct, question_type : question_type , question: question, options: options_array };
		  i++;
	       });
	       if (survey_error == '1') {
		  $("#create_content_error_survey_retail_audit").html("Please Fill mandatory fields");
		  return false;
	       }
	        $(".loading").show();
	       questions_array = JSON.stringify(questions_array); 
	       //var file_data = $("#survey_file_retail_audit").prop("files")[0];
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("questions", questions_array);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_survey_retail_audit",
		  cache: false,
		  contentType: false,
		  processData: false,
		  dataType: "json",
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#add_content_survey_retail_audit")[0].reset();
		     $("#upload_content_modal_survey_retail_audit").modal("hide");
		  }
	       });
	    }
	 }
	function survey_content_edit_modal_open_retail_audit(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type){
	    $("#edit_content_id_survey_retail_audit").val(content_id);
	       $("#edit_content_id_main_category_survey_retail_audit").val(main_id);
	       $("#edit_content_id_sub_category_survey_retail_audit").val(sub_id);
	       $("#content_title_survey_edit_retail_audit").val(unescape(content_title));
	       if (content_file != '') {
		  $("#survey_edit_file_previous_name_div_retail_audit").show();
	       }else{
		  $("#survey_edit_file_previous_name_div_retail_audit").hide();
	       }
	       //$("#survey_edit_file_previous_name_retail_audit").html(content_file);
	       $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/get_content_poll_survey_retail_audit",
	       data: "content_id="+content_id,
	       dataType: 'json',
	       success: function(data){
		  $(".loading").hide();
		  $('#survey_question_div_edit_retail_audit').html("");
		  if (data.question.length > 0) {
		    $("#total_survey_question_created_edit_retail_audit").val(data.question.length);
		     for (var i = 0; i < data.question.length; i++) {
			var obj = data.question[i];
			var question_type = obj.survey_type;
			var question = obj.question;
			var options_array = obj.options;
			var main_div = '';
			var question_number = i+1;
			if (question_number < 10) {
			   question_number = '0'+question_number;
			}
			var level = '';
			if (i != 0) {
			   level =  '<label class="pull-right remove_survey_question_edit_retail_audit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>';
			}
			var text_entry = ''; var single_choice = ''; var multi_choice = '';
			if (question_type == '1') {
			   text_entry = 'selected';
			}
			else if (question_type == '2') {
			   single_choice = 'selected';
			}
			else if (question_type == '3') {
			   multi_choice = 'selected';
			}
			var checked = '';
			if (obj.is_use_sku == '1') {
			    checked = "checked";
			}
			main_div += '<div class="survey_div_edit_retail_audit">';
			main_div += '<div class="form-group">'+
					  '<label style="color:#f00f64">QUESTION '+question_number+'</label>'+level+
					  '<label style="padding-left:20%;">Deduct: <input type="text" class= "health_deduct_edit_retail_audit" name="health_deduct_edit_retail_audit[]" value="'+obj.health_deduction+'" style="width:50px;"> % from store health</label>'+
					  /*'<select class="form-control survey_type_class_edit_retail_audit" name="survey_type_edit_retail_audit[]">'+
					     '<option value="2" '+single_choice+'>Single Choice</option>'+
					     '<option value="3" '+multi_choice+'>Multi Coice</option>'+
					  '</select>'+*/
				       '</div>';
			main_div += '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_edit_retail_audit[]" class = "survey_question_class_edit_retail_audit">'+question+'</textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				    '</div>';
			main_div += '<div class="form-group">'+
					'<div class="checkbox" style="margin-top:0px">'+
					    '<label>'+
					    '<input '+checked+'  class= "use_sku_survey_edit_retail_audit" name="use_sku_survey_edit_retail_audit[]" value="1" type="checkbox"> User SKU List'+
					    '</label>'+
				       '</div>'+
				    '</div>';
			/*if (options_array.length > 0) {
			   main_div += '<div class="survey_div_option_edit_retail_audit">';
			   main_div += '<legend style="margin-bottom:0px;font-size:14px; color:#414042">'+
					  'ANSWER OPTIONS'+
					  '<small style="color:#999999">(Upto 6 Options)</small>'+
					  '</legend>';
			   for (var j = 0; j < options_array.length; j++) {
			      var op_obj = options_array[j];
			      var option_val = op_obj.option_text;
			      if (j == 0) {
			      main_div += '<div class="mui-textfield mui-textfield--float-label survey_option_count_edit_retail_audit">'+
					  '<input class="survey_option_class_edit_retail_audit mui--is-empty mui--is-untouched mui--is-pristine" type="text" name="options_edit_retail_audit[]" value="'+option_val+'">'+
					  '<label>'+
					  'Option'+
					  '<sup>*</sup>'+
					  '</label>'+
					  '</div>';
			      }else if (j == 1) {
				 main_div += '<div class="form-group survey_option_count_edit_retail_audit">'+
				 '<div class="row">'+
				 '<div class="col-sm-10 col-xs-10 nopadding-right">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				 '<input type="text" class="survey_option_class_edit_retail_audit mui--is-empty mui--is-untouched mui--is-pristine" name="options_edit_retail_audit[]" value="'+option_val+'">'+
				 '<label style="font-weight:300;color:#a7a9ac">Option<sup>*</sup></label>'+
				 '</div>'+
				 '</div>'+
				 '<div style="padding-top:3%" class="col-sm-2 col-xs-2">'+
				 '<span class="btn-append"> <i style="font-family:FontAwesome" class="far fa-plus-square add_survey_option_edit_retail_audit"></i></span>'+
				' </div>'+
				 '</div>'+
				 '</div>';
			      }
			      else{
				 main_div += '<div class="form-group survey_option_count_edit_retail_audit">'+
				 '<div class="row">'+
				 '<div class="col-sm-10 col-xs-10 nopadding-right">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				 '<input type="text" class="survey_option_class_edit_retail_audit mui--is-empty mui--is-untouched mui--is-pristine" name="options_edit_retail_audit[]" value="'+option_val+'">'+
				 '<label style="font-weight:300;color:#a7a9ac">Option<sup>*</sup></label>'+
				 '</div>'+
				 '</div>'+
				 '<div style="padding-top:3%" class="col-sm-2 col-xs-2">'+
				 '<span class="btn-append"> <i style="font-family:FontAwesome" class="far fa-minus-square remove_survey_option_edit_retail_audit"></i></span>'+
				' </div>'+
				 '</div>'+
				 '</div>';
			      }
			   }
			   main_div += '</div>';
			}*/
			main_div += '</div>';
			$('#survey_question_div_edit_retail_audit').append(main_div);
		     }
		  }
		     $("#upload_content_modal_survey_edit_retail_audit").modal("show");
		    
	       }
	    });

	 }
	 $(document).on('click','#survey_edit_file_previous_name_remove_button_retail_audit',function(){
	    $("#survey_edit_file_previous_name_retail_audit").html("");
	 });
	 
	// on click of new question add add new question div
	 $(document).on('click', '.add_new_question_edit_retail_audit', function(){
	   var num_of_question = $("#total_survey_question_created_edit_retail_audit").val();
	    num_of_question++;
	    $("#total_survey_question_created_edit_retail_audit").val(num_of_question);
	    if (num_of_question > 9) {
	       num_of_question = "QUESTION "+num_of_question;
	    }else{
	       num_of_question = "QUESTION 0"+num_of_question;
	    }
	    
	    var new_div = '<div class="survey_div_edit_retail_audit">'+
				    '<div class="form-group">'+
				       '<label style="color:#f00f64">'+num_of_question+'</label>'+
				       '<label style="padding-left:20%;">Deduct: <input type="text" class= "health_deduct_edit_retail_audit" name="health_deduct_edit_retail_audit[]" style="width:50px;"> % from store health</label>'+
				       '<label class="pull-right remove_survey_question_edit_retail_audit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				       /*'<select class="form-control survey_type_class_edit_retail_audit" name="survey_type_edit_retail_audit[]">'+
					  '<option value="">Select Survey Type</option>'+
					  '<option value="2">Single Choice</option>'+
					  '<option value="3">Multi Coice</option>'+
				       '</select>'+*/
				    '</div>'+
				    
				    '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_edit_retail_audit[]" class = "survey_question_class_edit_retail_audit"></textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				       '</div>'+
				    //'</div><div class="survey_div_option"></div>'+
				    '<div class="form-group">'+
					'<div class="checkbox" style="margin-top:0px">'+
					    '<label>'+
					    '<input  class= "use_sku_survey_edit_retail_audit" name="use_sku_survey_edit_retail_audit[]" value="1" type="checkbox"> User SKU List'+
					    '</label>'+
				       '</div>'+
				    '</div>'+
				 '</div>';
	    $("#survey_question_div_edit_retail_audit").append(new_div);
	 });
	 // remove question div
	 $(document).on('click', '.remove_survey_question_edit_retail_audit', function (){
	    $(this).parent().parent().remove();
	 });
	 // on change of survey type hide and show option
	 $(document).on('change', '.survey_type_class_edit_retail_audit', function(){
	    var survey_type = $(this).val();
	    if (survey_type != '1') {
	       var new_options = '<div class="survey_div_option_edit_retail_audit"><legend style="margin-bottom:0px;font-size:14px; color:#414042">ANSWER OPTIONS  <small style="color:#999999">(Upto 6 Options)</small></legend>'+
	       '<div class="mui-textfield mui-textfield--float-label survey_option_count_edit_retail_audit">'+
                  '<input type="text" name="options_edit_retail_audit[]" class = "survey_option_class_edit_retail_audit">'+
                  '<label>Option A<sup>*</sup></label>'+
               '</div>'+
	       '<div class="form-group survey_option_count_edit_retail_audit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_edi_retail_auditt[]" class = "survey_option_class_edit_retail_audit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option B<sup>*</sup></label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-plus-square add_survey_option_edit_retail_audit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>'+
	       '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_edit_retail_audit").find('.survey_div_option_edit_retail_audit').find('.survey_option_count_edit_retail_audit') ).each(function() {
			total_option++;
	       });
	       if (total_option == 0) {
		  $(this).closest(".survey_div_edit_retail_audit").append(new_options);
	       }
	       
	    }else{
	       $(this).closest(".survey_div_edit_retail_audit").find('.survey_div_option_edit_retail_audit').remove();
	    }
	 });
	 // on click add option add option
	 $(document).on('click', '.add_survey_option_edit_retail_audit', function(){
	    var new_options = '<div class="form-group survey_option_count_edit_retail_audit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_edit_retail_audit[]" class = "survey_option_class_edit_retail_audit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option</label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-minus-square remove_survey_option_edit_retail_audit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_option_edit_retail_audit").find('.survey_option_count_edit_retail_audit') ).each(function() {
			total_option++;
		     });
	       if (total_option < 6) {
		  $(this).closest(".survey_div_option_edit_retail_audit").append(new_options);
	       }
	       
	 });
	 // on click of button remove option
	 $(document).on('click', '.remove_survey_option_edit_retail_audit', function(){
	    $(this).parent().parent().parent().parent().remove();
	 });
	
	
	 
	
	 function update_content_survey_retail_audit() {
	    $("#create_content_error_survey_edit_retail_audit").html("");
	    var content_id = $("#edit_content_id_survey_retail_audit").val();
	    var main_category = $("#edit_content_id_main_category_survey_retail_audit").val();
	    var sub_category = $("#edit_content_id_sub_category_survey_retail_audit").val();
	    var content_title = $("#content_title_survey_edit_retail_audit").val();
	    
	    if (content_title == '') {
	       $("#create_content_error_survey_edit_retail_audit").html("Please Fill mandatory fields");
	       return false;
	    }else{
	       
	       var questions_array = {};
	       var i = 0;
	       var survey_error = 0;
	       $( ".survey_div_edit_retail_audit" ).each(function() {
		  var question_type = 0;
		  var health_deduct = 0;
		  var is_sku_use = 0;
		  var question= '';
		    if ($(this).find('.use_sku_survey_edit_retail_audit').is(':checked')) {
			is_sku_use = 1;
		    }
		  health_deduct = $(this).find('.health_deduct_edit_retail_audit').val();
		  var question= '';
		  //question_type = $(this).find('.survey_type_class_edit_retail_audit').val();
		  question_type = '2';
		  question = $(this).find('.survey_question_class_edit_retail_audit').val();
		  if (question == '') {
		     survey_error = 1;
		  }
		  var options_array = {};
		  /*if (question_type == '2' || question_type == '3') {
		     var j = 0;
		     var total_option = 0;
		     $( $(this).find('.survey_div_option_edit_retail_audit').find('.survey_option_count_edit_retail_audit') ).each(function() {
			var option_val = '';
			option_val = $(this).find('.survey_option_class_edit_retail_audit').val();
			if (typeof option_val === "undefined") {
			   
			}else{
			   if(option_val != ''){
			      total_option++;
			      options_array[j] = option_val;
			      j++;
			   }
			   
			}
			
		     });
		     if (question_type != 1 && total_option < 2) {
			survey_error = 1;
		     }
		  }*/
		  
		  
		  
		  questions_array[i] =  {is_sku_use: is_sku_use, health_deduct: health_deduct, question_type : question_type , question: question, options: options_array };
		  i++;
	       });
	       if (survey_error == '1') {
		  $("#create_content_error_survey_edit_retail_audit").html("Please Fill mandatory fields");
		  return false;
	       }
	       questions_array = JSON.stringify(questions_array);
	       $(".loading").show();
	       
	       /*var file_data = $("#survey_file_edit_retail_audit").prop("files")[0];
	       var file_previous_name = $("#survey_edit_file_previous_name_retail_audit").html();*/
	       var file_data = '';
	       var file_previous_name = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       //form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", content_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name",file_previous_name);
	       form_data.append("questions", questions_array);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/update_content_survey_retail_audit",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     
		     content_builder(main_category,sub_category);
		     $("#update_content_survey_retail_audit")[0].reset();
		     $("#upload_content_modal_survey_edit_retail_audit").modal("hide");
		  }
	       });
	    }
	 }
	function add_otp_sms(){
	 $("#sms_assing_error").html("");
	 var location_uid =  $("#location_id_hidden").val();
	 var locartion_id = $("#created_location_id").val();
	 var otp_sms =  $("#otp_sms").val();
	 var sms_type = '1';
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_otp_sms",
	    data: "location_uid="+location_uid+"&locartion_id="+locartion_id+"&otp_sms="+otp_sms+"&sms_type="+sms_type,
	    success: function(data){
	       $(".loading").hide();
	       if (data == '0') {
		  $("#sms_assing_error").html("Please First setup your sms gateway");
	       }
	       else if (data == '2') {
		  $("#sms_assing_error").html("OTP Disabled in this location");
	       }
	       else{
		  $("#otp_sms").val('');
		  sms_list(location_uid);
	       }
	       
	    }
         });
      }
      function add_marketing_sms(){
	 $("#sms_assing_error").html("");
	 var location_uid =  $("#location_id_hidden").val();
	 var locartion_id = $("#created_location_id").val();
	 var otp_sms =  $("#marketing_sms").val();
	 var sms_type = '2';
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_otp_sms",
	    data: "location_uid="+location_uid+"&locartion_id="+locartion_id+"&otp_sms="+otp_sms+"&sms_type="+sms_type,
	    success: function(data){
	       $(".loading").hide();
	       if (data == '0') {
		  $("#sms_assing_error").html("Please First setup your sms gateway");
	       }
	       else if (data == '2') {
		  $("#sms_assing_error").html("OTP Disabled in this location");
	       }
	       else{
		  $("#marketing_sms").val('');
		  sms_list(location_uid);
	       }
	    }
         });
      }
      function sms_list() {
	 var location_uid =  $("#location_id_hidden").val();
	 var location_id = $("#created_location_id").val();
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/sms_list",
	    data: "location_uid="+location_uid+"&location_id="+location_id,
	    success: function(data){
	       $(".loading").hide();
	       $("#sms_list").html(data);
	    }
         });
      }

	 
      </script>
	      
	      <script>
	 $(document).ready(function() {
	    var dept_list_array;
            var location_type = $("#location_type").val();
	    if (location_type == '11'){
	       offline_organisation_list();
	       vm_offline_department_list();
	       vm_offline_employee_list();
	    }
	    
         });
	 
	 

	 $(document).on('click', '.visitor_list_of_type_odm', function(){
	    $('.visitor_list_of_type_odm').removeClass('mui-btn--danger');
	    $(this).addClass('mui-btn--danger');
	    var button_id = $(this).attr('id');
	    if (button_id == 'organisation_button') {
	       $("#organisation_listing_div").show();
	       $("#department_listing_div").hide();
	       $("#employee_listing_div").hide();
	       //offline_organisation_list();
	    }
	    else if (button_id == 'department_button') {
	       $("#organisation_listing_div").hide();
	       $("#department_listing_div").show();
	       $("#employee_listing_div").hide();
	       //vm_offline_department_list();
	    }
	    else if (button_id == 'employee_button') {
	       $("#organisation_listing_div").hide();
	       $("#department_listing_div").hide();
	       $("#employee_listing_div").show();
	    }
	 });
	 function offline_organisation_list() {
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
	       type:"POST",
               url: "<?php echo base_url()?>location/offline_organisation_list",
               cache:false,
               dataType: 'json',
               data:"location_uid="+location_uid,
	       success: function(data){
		  $(".loading").hide();
		  if(data.resultCode == '0')
		  {
		     $("#organisation_list_tabel").hide();
		  }
		  else
		  {
		     $("#organisation_list_tabel").show();
		     var organisations = data.organisations;
		     var org_list = '';
		     var org_list_in_dept = '';
		     var org_list_in_emp = '';
		     org_list_in_dept += '<option value="">Select Organisation</option>';
		     org_list_in_emp += '<option value="">Select Organisation</option>';
		     for(var i = 0; i < organisations.length; i++) {
			org_list += '<tr><td class="text-left">'+organisations[i].organisation_name+' </td> <td><i style="cursor: pointer" onclick="delete_offline_vm_organisation('+organisations[i].organisation_id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></td></tr>';
			org_list_in_dept += '<option value="'+organisations[i].organisation_id+'">'+organisations[i].organisation_name+'</option>';
			org_list_in_emp += '<option value="'+organisations[i].organisation_id+'">'+organisations[i].organisation_name+'</option>';
		     }
		     $("#organisation_list_tabel_data").html(org_list);
		     $("#vm_dept_org_name").html(org_list_in_dept);
		     $("#vm_emp_org_name").html(org_list_in_emp);
		  }
               }
	    });
	 }
	 
	 function add_vm_table() {
	    $("#add_offline_organisation_error").html("");
	    var offline_organisation_name = $("#offline_organisation_name").val();
	    
	    if (offline_organisation_name == '') {
	       $("#add_offline_organisation_error").html("Please Enter Table");
	       return false;
	    }else{
	      
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("offline_organisation_name", offline_organisation_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_offline_vm_organisation",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (data == '0')
		     {
			$(".loading").hide();
			$("#add_offline_organisation_error").html("Organisation already exist");
		     }
		     else
		     {
			offline_organisation_list();
			$("#offline_organisation_name").val('');
			
		     }
		     
		  }
	       });
	    }
	 }
	 

	 function vm_offline_department_list() {
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
	       type:"POST",
               url: "<?php echo base_url()?>location/vm_offline_department_list",
               cache:false,
               dataType: 'json',
               data:"location_uid="+location_uid,
	       success: function(data){
		  dept_list_array = data;
		  $(".loading").hide();
		  if(data.resultCode == '0')
		  {
		     $("#vm_dept_list_tabel").hide();
		  }
		  else
		  {
		     $("#vm_dept_list_tabel").show();
		     var department = data.department;
		     var dept_list = '';
		     for(var i = 0; i < department.length; i++) {
			dept_list += '<tr><td class="text-left">'+department[i].organisation_name+' </td><td class="text-left">'+department[i].department_name+' </td><td><i style="cursor: pointer" onclick="delete_offline_vm_department('+department[i].department_id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></td></tr>';
		     }
		     $("#vm_dept_list_tabel_data").html(dept_list);
		  }
               }
	    });
	 }
	 
	 function add_vm_offline_department() {
	    $("#add_offline_department_error").html("");
	    var org_id = $("#vm_dept_org_name").val();
	    var offline_department_name = $("#vm_dept_name").val();
	    
	    if (offline_department_name == '' || org_id == '') {
	       $("#add_offline_department_error").html("Please fill all fields");
	       return false;
	    }else{
	      
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("offline_department_name", offline_department_name);
	       form_data.append("org_id", org_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_offline_vm_department",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (data == '0')
		     {
			$(".loading").hide();
			$("#add_offline_department_error").html("Department already exist");
		     }
		     else
		     {
			vm_offline_department_list();
			$("#vm_dept_name").val('');
			
		     }
		     
		  }
	       });
	    }
	 }
	 


	 

	 function vm_offline_employee_list() {
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
	       type:"POST",
               url: "<?php echo base_url()?>location/vm_offline_employee_list",
               cache:false,
               dataType: 'json',
               data:"location_uid="+location_uid,
	       success: function(data){
		  $(".loading").hide();
		  if(data.resultCode == '0')
		  {
		     $("#vm_emp_list_tabel").hide();
		  }
		  else
		  {
		     $("#vm_emp_list_tabel").show();
		     var employee = data.employee;
		     var emp_list = '';
		     for(var i = 0; i < employee.length; i++) {
			emp_list += '<tr><td class="text-left">'+employee[i].employee_name+' </td><td>'+employee[i].employee_last_name+' </td><td>'+employee[i].employee_email+' </td><td>'+employee[i].employee_mobile+' </td><td>'+employee[i].department_name+'</td><td>'+employee[i].organisation_name+'</td><td><i style="cursor: pointer" onclick="delete_offline_vm_employee('+employee[i].employee_id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></td></tr>';
		     }
		     $("#vm_emp_list_tabel_data").html(emp_list);
		  }
               }
	    });
	 }
	 
	 function add_vm_offline_employee() {
	    $("#add_offline_employee_error").html("");
	    var org_id = $("#vm_emp_org_name").val();
	    var dept_id = $("#vm_emp_dept_name").val();
	    var employee_name = $("#vm_emp_name").val();
	    var employee_last_name = $("#vm_emp_last_name").val();
	    var employee_email = $("#vm_emp_email").val();
	    var employee_mobile = $("#vm_emp_mobile").val();
	    if (!validateEmail(employee_email)) {
	       $("#add_offline_employee_error").html("Enter valie email");
	       return false;
	    }
	    if (employee_name == '' || employee_email == '' || employee_mobile == '' || org_id == '' || dept_id == '') {
	       $("#add_offline_employee_error").html("Please fill all fields");
	       return false;
	    }else{
	      
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("employee_name", employee_name);
	       form_data.append("employee_last_name", employee_last_name);
	       form_data.append("employee_email", employee_email);
	       form_data.append("employee_mobile", employee_mobile);
	       form_data.append("org_id", org_id);
	       form_data.append("dept_id", dept_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("emp_unique_id", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_vm_offline_employee",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (data == '0')
		     {
			$(".loading").hide();
			$("#add_offline_employee_error").html("Employee Mobile or Email Already exist");
		     }
		     else
		     {
			vm_offline_employee_list();
			$("#vm_emp_name").val('');
			$("#vm_emp_last_name").val('');
			$("#vm_emp_email").val('');
			$("#vm_emp_mobile").val('');
			
		     }
		     
		  }
	       });
	    }
	 }
	 
	 $(document).on('change', '#vm_emp_org_name', function(){
	    var org_id = $("#vm_emp_org_name option:selected").val();
	    var dept_list = '<option value="">Select Depratment</option>';
	    if (dept_list_array.resultCode != '0') {
	       var department = dept_list_array.department;
	       for(var i = 0; i < department.length; i++) {
		  if (department[i].organisation_id == org_id) {
		  
		     dept_list += '<tr><td class="text-left">'+department[i].organisation_name+' </td><td class="text-left">'+department[i].department_name+' </td></tr>';
		     dept_list += '<option value="'+department[i].department_id+'">'+department[i].department_name+'</option>';
		     
		  }
	       }
	    }
	    $("#vm_emp_dept_name").html(dept_list);
	 });
	 
	 function validateEmail(sEmail) {
	    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	    if (filter.test(sEmail)) {
		return true;
	    }
	    else {
		return false;
	    }
	 }
	 
	 function delete_offline_vm_organisation(organisation_id) {
	    var location_id = $("#created_location_id").val();
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_offline_vm_organisation",
		  data: "organisation_id="+organisation_id+"&location_id="+location_id,
		  success: function(data){
		     offline_organisation_list();
		     vm_offline_department_list();
		     vm_offline_employee_list();
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 
	 function delete_offline_vm_department(department_id) {
	    var location_id = $("#created_location_id").val();
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_offline_vm_department",
		  data: "department_id="+department_id+"&location_id="+location_id,
		  success: function(data){
		     vm_offline_department_list();
		     vm_offline_employee_list();
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 
	 function delete_offline_vm_employee(employee_id) {
	    var location_id = $("#created_location_id").val();
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_offline_vm_employee",
		  data: "employee_id="+employee_id+"&location_id="+location_id,
		  success: function(data){
		     vm_offline_employee_list();
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	    
	 function validate_ssid(ssid)
    {
    //var re = /^\w+$/;
    var re = /^[a-zA-Z0-9_-]+$/;
    if (!re.test(ssid)) {
        
        return 0;
    }else
    {
        return 1;
    }
    return 1;
    
    }
    
   /* $(document).on('change', '.cp1_plan_type_radio', function ()
   {
      var cp1_plan_type = $("input[name='cp1_plan_type_value']:checked").val();
      if (cp1_plan_type == '2')
      {
	 $('.cp1_plan_div').hide();
	 $('.hide_data_from_cp3').hide();
	 $('.cp3_plan_div').show();
	 $(".time_limit_div").removeClass("disabled");
         $(".data_limit_div").removeClass("disabled");
	 var plan_id = $('#cp3_hybrid_plan').val();
	 plan_detail(plan_id);
      }
      else
      {
	 $(".time_limit_div").removeClass("disabled");
         $(".data_limit_div").addClass("disabled");
	 $('.cp1_plan_div').show();
	 $('.hide_data_from_cp3').show();
	 $('.cp3_plan_div').hide();
	 var plan_id = $('#cp1_plan').val();
	 plan_detail(plan_id);
      }
   });*/
   
   
   $(document).on('change', '.cp_cafe_retail_plan_type_radio', function ()
   {
      var plan_type = $("input[name='cp_cafe_retail_plan_type_value']:checked").val();
      if (plan_type == '2')
      {
	 $(".time_limit_div").removeClass("disabled");
         $(".data_limit_div").removeClass("disabled");
	 $("#cafe_retail_hybrid_plan_div").show();
	 $("#cafe_retail_time_plan_div").hide();
	 var plan_id = $('#cp_cafe_hybrid_plan').val();
	 plan_detail(plan_id);
      }
      else
      {
	 $(".time_limit_div").removeClass("disabled");
         $(".data_limit_div").addClass("disabled");
	 $("#cafe_retail_time_plan_div").show();
	 $("#cafe_retail_hybrid_plan_div").hide();
	 var plan_id = $('#cp_cafe_plan').val();
	 plan_detail(plan_id);
      }
   });


	 


	 

      </script>
		
		
	
		
	
		
		
		
   
   
      <script>
      var contestifi_array;
      function contestifi_content_builder(created_id= '', created_quiz_id = '' , created_prize_id = '')
      {
	 $(".loading").show();
	 var location_uid =  $("#location_id_hidden").val();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/contestifi_content_builder_list",
	    data: "location_uid="+location_uid,
	    dataType: 'json',
	    success: function(data)
	    {
	       contestifi_array = data;
	       $(".loading").hide();
	       if (data.contest.length > 0)
	       {
		  $('.contestifi_section_content_list').html('');
		  for (var i = 0; i < data.contest.length; i++)
		  {
		     var contest = data.contest[i];
		     var main_class = '';
		     if (created_id == '')
		     {
			if (i == 0)
			{
			   main_class = 'offline_active';
			   list_contetifi_quiz_question(contest.id,created_quiz_id);
			   list_contetifi_conetst_prize(contest.id,created_prize_id);
			}
		     }
		     else
		     {
			if (contest.id == created_id)
			{
			   main_class = 'offline_active';
			   list_contetifi_quiz_question(contest.id,created_quiz_id);
			   list_contetifi_conetst_prize(contest.id,created_prize_id);
			}
		     }
		     var content_html = '<li class = "contestifi_contest_section '+main_class+'"  data-contestifi_contest_id="'+contest.id+'" data-contestifi_contest_type="'+contest.contest_type+'"><span>'+contest.contest_name+'</span><span class="offline-fa">'+
					  '<i onclick="edit_contestifi_contest('+contest.id+')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
					  '</span>'+
					  '<span class="offline-fa"><i onclick="delete_contestifi_contest('+contest.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
				       '</li>';
		     $('.contestifi_section_content_list').append(content_html);
		  }
		  // for contest section
		  $(".contestifi_content_filled_div").show();
		  $(".contestifi_content_blank_div").hide();
	       }
	       else
	       {
		  // for contest section
		  $(".contestifi_content_blank_div").show();
		  $(".contestifi_content_filled_div").hide();
		  
		  // for quiz section
		  $(".contestifi_quiz_blank_first_div").show();
		  $(".contestifi_quiz_filled_div").hide();
		  $(".contestifi_quiz_blank_div").hide();
		  
		  // for prize section
		  $(".contestifi_prize_blank_first_div").show();
		  $(".contestifi_prize_filled_div").hide();
		  $(".contestifi_prize_blank_div").hide();
	       }
	    }
	 });
      }
      
      // function to list the contestifi quiz
      function list_contetifi_quiz_question(contest_id,contest_quiz_id = '')
      {
	 $('.contestifi_quiz_list').html('');
	 var content = '';
	 for (var i = 0; i < contestifi_array.contest.length; i++)
	 {
	    var main_contest = contestifi_array.contest[i];
	    if (main_contest.id == contest_id)
	    {
	       content = main_contest.quiz_questions;
	    }
	 }
	 if (content.length > 0)
	 {
	    // for sub section
	    $(".contestifi_quiz_filled_div").show();
	    $(".contestifi_quiz_blank_first_div").hide();
	    $(".contestifi_quiz_blank_div").hide();
	    for (var i = 0; i < content.length; i++)
	    {
	       var quiz_question = content[i];
	       var main_class = '';
	       if (contest_quiz_id == '')
	       {
		  if (i == 0)
		  {
		     main_class = 'offline_active';
		  }
	       }
	       else
	       {
		  if (quiz_question.id == contest_quiz_id)
		  {
		     main_class = 'offline_active';
		  }
	       }
	       var content_html = '<li  class = "contestifi_quiz_section '+main_class+'"  data-contestifi_contest_id="'+contest_id+'"  data-contestifi_quiz_question_id="'+quiz_question.id+'"  ><span>'+quiz_question.question+'</span><span class="offline-fa">'+
				    '<i onclick="edit_contestifi_quiz_question('+contest_id+','+quiz_question.id+')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
				    '</span>'+
				    '<span class="offline-fa"><i onclick="delte_contestifi_quiz_question('+quiz_question.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
				 '</li>';
	       $('.contestifi_quiz_list').append(content_html);
	    }
	 }
	 else
	 {
	    // for quiz section
	    $(".contestifi_quiz_blank_div").show();
	    $(".contestifi_quiz_blank_first_div").hide();
	    $(".contestifi_quiz_filled_div").hide();  
	 }
      }
      
      function list_contetifi_conetst_prize(contest_id,contest_prize_id = '')
      {
	 $('.contestifi_prize_list').html('');
	 var content = '';
	 for (var i = 0; i < contestifi_array.contest.length; i++)
	 {
	    var main_contest = contestifi_array.contest[i];
	    if (main_contest.id == contest_id)
	    {
	       content = main_contest.contest_prize;
	    }
	 }
	 if (content.length > 0)
	 {
	    // for sub section
	    $(".contestifi_prize_filled_div").show();
	    $(".contestifi_prize_blank_first_div").hide();
	    $(".contestifi_prize_blank_div").hide();
	    for (var i = 0; i < content.length; i++)
	    {
	       var quiz_question = content[i];
	       var main_class = '';
	       if (contest_prize_id == '')
	       {
		  if (i == 0)
		  {
		     main_class = 'offline_active';
		  }
	       }
	       else
	       {
		  if (quiz_question.prize_id == contest_prize_id)
		  {
		     main_class = 'offline_active';
		  }
	       }
	       var contest_prize = escape(quiz_question.contest_prize);
	       var content_html = '<li  class = "contestifi_prize_section '+main_class+'"  data-contestifi_contest_id="'+contest_id+'"  data-contestifi_contest_prize_id="'+quiz_question.id+'"  ><span>'+quiz_question.contest_prize+'</span><span class="offline-fa">'+
				    '<i onclick="edit_contestifi_contest_prize('+contest_id+','+quiz_question.prize_id+', \''+contest_prize+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
				    '</span>'+
				    '<span class="offline-fa"><i onclick="delte_contestifi_contest_prize('+quiz_question.prize_id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
				 '</li>';
	       $('.contestifi_prize_list').append(content_html);
	    }
	 }
	 else
	 {
	    // for quiz section
	    $(".contestifi_prize_blank_div").show();
	    $(".contestifi_prize_blank_first_div").hide();
	    $(".contestifi_prize_filled_div").hide();  
	 }
      }
      
      function open_add_contestifi_quiz()
      {
	 var contest_type = 1;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_type  = $(this).data('contestifi_contest_type');
	    }
	 });
	 if (contest_type == '1')
	 {
	    $("#contestifi_contest_image_contest_div").hide();
	 }
	 else
	 {
	    $("#contestifi_contest_image_contest_div").show();
	 }
	 
	 if (contest_type == '3')
	 {
	    $("#add_contestifi_survey_qustion_modal").modal("show");
	 }
	 else
	 {
	    $("#add_contestifi_quiz_qustion_modal").modal("show");
	 }
      }
      function add_contestifi_question()
      {
	 var poll_querstion = $("#quiz_question").val();
	 var option_one = $("#quiz_option_one").val();
	 var option_two = $("#quiz_option_two").val();
	 var option_three = $("#quiz_option_three").val();
	 var option_four = $("#quiz_option_four").val();
	 if ( poll_querstion == '' || option_one == '' || option_two == '') {
	       $("#create_contestifi_quiz_error").html("Please Fill mandatory fields");
	       return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 var right_ans = $("input[name='contest_quiz_right_ans']:checked").val();
	 if (right_ans == '1' && option_one == '') {
	    $("#create_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '2' && option_two == '') {
	    $("#create_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '3' && option_three == '') {
	    $("#create_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '4' && option_four == '') {
	    $("#create_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 var contest_type = 1;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_type  = $(this).data('contestifi_contest_type');
	    }
	 });
	 if (contest_type != '1')
	 {
	    var file_data = $("#contestifi_content_file").prop("files")[0];
	    if (typeof file_data === "undefined") {
	       $("#create_contestifi_quiz_error").html("Please Select File");
	       return false;
	    }else{
	       var file_size = $("#contestifi_content_file").prop("files")[0].size;
	       file_size = ((file_size/1024)/1024);
	       if (file_size > 500) {
		  $("#create_contestifi_quiz_error").html("File size should be max 500MB");
		  return false;
	       }
	    }
	 }
	 else
	 {
	     var file_data = '';
	 }
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append("file", file_data);
	 form_data.append('contest_id',contest_id);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("poll_querstion",poll_querstion);
	 form_data.append("option_one",option_one);
	 form_data.append("option_two",option_two);
	 form_data.append("option_three",option_three);
	 form_data.append("option_four",option_four);
	 form_data.append("right_ans",right_ans);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contistifi_quiz_question",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    xhr: function () {
                           var xhr = new window.XMLHttpRequest();
                           xhr.upload.addEventListener("progress", function (evt) {
                               if (evt.lengthComputable) {
                                   var percentComplete = evt.loaded / evt.total;
                                   percentComplete = parseInt(percentComplete * 100);
                                     $('.progress').removeClass('hide');
                                   $('.myprogress').text(percentComplete + '%');
                                   $('.myprogress').css('width', percentComplete + '%');
                               }
                           }, false);
                           return xhr;
                       },
	    success: function(data)
	    {
	       $('.progress').addClass('hide');
	       var quezi_question_id = data;
	       contestifi_content_builder(contest_id,quezi_question_id);
	       $("#add_contenstifi_quiz_question")[0].reset();
	       $("#add_contestifi_quiz_qustion_modal").modal("hide");
	    }
	 });
      }
      
      // on click of context
      $(document).on('click', '.contestifi_contest_section', function (){
	 $( ".contestifi_contest_section" ).each(function() {
	    $( this ).removeClass( "offline_active" );
	 });
	 $( this ).addClass( "offline_active" );
	 var contest_id = $(this).data('contestifi_contest_id'); 
	 list_contetifi_quiz_question(contest_id);
	 list_contetifi_conetst_prize(contest_id);
      });
      // on click of quiz questions
      $(document).on('click', '.contestifi_quiz_section', function (){
	 $( ".contestifi_quiz_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	 });
	 $( this ).addClass( "offline_active" );
      });
      
      $(document).on('click', '.contestifi_prize_section', function (){
	 $( ".contestifi_prize_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	 });
	 $( this ).addClass( "offline_active" );
      });
      
      function delte_contestifi_quiz_question(question_id)
      {
	 if (confirm("Are you sure you want to delete?"))
	 {
	    var locationid = $("#created_location_id").val();
	    $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/delte_contestifi_quiz_question",
	       data: "question_id="+question_id+"&locationid="+locationid,
	       success: function(data){
		  var contest_id = $('.contestifi_quiz_section').filter('[data-contestifi_quiz_question_id="'+question_id+'"]').data('contestifi_contest_id');
		  contestifi_content_builder(contest_id);
	       }
	    });
	 }
	 else
	 {
	    return false;
	 }
      }
   
      function edit_contestifi_quiz_question(contest_id,question_id)
      {
	 $(".loading").show();
	 
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/get_contestifi_quiz_question",
	    data: "question_id="+question_id,
	    dataType: 'json',
	    success: function(data)
	    {
	       if (data.question.length > 0)
	       {
		  var obj = data.question[0];
		  var contest_type = 1;
		  $( ".contestifi_contest_section" ).each(function()
		  {
		     if($( this ).hasClass( "offline_active" )){
			contest_type  = $(this).data('contestifi_contest_type');
		     }
		  });
		  if (contest_type == 3) {
		     edit_contestifi_survey_question(obj,contest_id,question_id);
		  }
		  else
		  {
		     if (contest_type == '1')
		     {
			$("#contestifi_contest_image_contest_div_edit").hide();
			$("#contestifi_content_file_edit_preview_div").hide();
		     }
		     else
		     {
			$("#contestifi_contest_image_contest_div_edit").show();
			$("#contestifi_content_file_edit_preview_div").show();
			$("#contestifi_content_file_edit_preview").html(obj.contest_file);
		     }
		     $("#edit_contestifi_quiz_question_id").val(question_id);
		     $("#quiz_question_edit").val(obj.question);
		     if (obj.options.length > 0)
		     {
			for (var i = 0; i < obj.options.length; i++)
			{
			   var op_obj = obj.options[i];
			   
			   if (i == '0') {
			      if (op_obj.is_right_option == '1') {
				 $("input[name=contest_quiz_right_ans_edit][value=1]").prop('checked', true);
			      }
			      var option_id = "quiz_option_one_edit";
			   }
			   
			   else if (i == '1') {
			      if (op_obj.is_right_option == '1') {
				 $("input[name=contest_quiz_right_ans_edit][value=2]").prop('checked', true);
			      }
			      option_id = "quiz_option_two_edit";
			   }
			   else if (i == '2') {
			      if (op_obj.is_right_option == '1') {
				 $("input[name=contest_quiz_right_ans_edit][value=3]").prop('checked', true);
			      }
			      option_id = "quiz_option_three_edit";
			   }
			   else if (i == '3') {
			      if (op_obj.is_right_option == '1') {
				 $("input[name=contest_quiz_right_ans_edit][value=4]").prop('checked', true);
			      }
			      option_id = "quiz_option_four_edit";
			   }
			   $("#"+option_id).val(op_obj.option_text);
			}
		     }
		     $("#update_contestifi_quiz_qustion_modal").modal("show");
		  }
		  
		  
		  
		  
		  
	       }
	       
	       $(".loading").hide();
	    }
	 });
      }
      function update_contestifi_question()
      {
	 $("#update_contestifi_quiz_error").html("");
	 var quiz_question_id = $("#edit_contestifi_quiz_question_id").val();
	 var poll_querstion = $("#quiz_question_edit").val();
	 var option_one = $("#quiz_option_one_edit").val();
	 var option_two = $("#quiz_option_two_edit").val();
	 var option_three = $("#quiz_option_three_edit").val();
	 var option_four = $("#quiz_option_four_edit").val();
	 if (poll_querstion == '' || option_one == '' || option_two == '')
	 {
	    $("#update_contestifi_quiz_error").html("Please Fill mandatory fields");
	    return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 var right_ans = $("input[name='contest_quiz_right_ans_edit']:checked").val();
	 if (right_ans == '1' && option_one == '') {
	    $("#update_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '2' && option_two == '') {
	    $("#update_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '3' && option_three == '') {
	    $("#update_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '4' && option_four == '') {
	    $("#update_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 
	 var contest_type = 1;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_type  = $(this).data('contestifi_contest_type');
	    }
	 });
	 if (contest_type != '1')
	 {
	    var file_data = $("#contestifi_content_file_edit").prop("files")[0];
	    if (typeof file_data === "undefined") {
	      
	    }else{
	       var file_size = $("#contestifi_content_file_edit").prop("files")[0].size;
	       file_size = ((file_size/1024)/1024);
	       if (file_size > 500) {
		  $("#create_contestifi_quiz_error").html("File size should be max 500MB");
		  return false;
	       }
	    }
	 }
	 else
	 {
	     var file_data = '';
	 }
	 
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append("file", file_data);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("poll_querstion",poll_querstion);
	 form_data.append("option_one",option_one);
	 form_data.append("option_two",option_two);
	 form_data.append("option_three",option_three);
	 form_data.append("option_four",option_four);
	 form_data.append("quiz_question_id",quiz_question_id);
	 form_data.append("contest_id",contest_id);
	 form_data.append("right_ans",right_ans);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/update_contistifi_quiz_question",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    xhr: function () {
                           var xhr = new window.XMLHttpRequest();
                           xhr.upload.addEventListener("progress", function (evt) {
                               if (evt.lengthComputable) {
                                   var percentComplete = evt.loaded / evt.total;
                                   percentComplete = parseInt(percentComplete * 100);
                                     $('.progress').removeClass('hide');
                                   $('.myprogress').text(percentComplete + '%');
                                   $('.myprogress').css('width', percentComplete + '%');
                               }
                           }, false);
                           return xhr;
                       },
	    success: function(data)
	    {
	       $('.progress').addClass('hide');
	       contestifi_content_builder(contest_id,quiz_question_id);
	       $("#update_contenstifi_quiz_question")[0].reset();
	       $("#update_contestifi_quiz_qustion_modal").modal("hide");
	    }
	 });
      }
      
      function open_add_contestifi_contest()
      {
	 $("#contestifi_contest_modal").modal("show");
      }
      
      function add_contestifi_contest()
      {
	 $("#create_contestifi_contest_error").html("");
	 var start_date = $("input[name='start_date']").val();
	 var end_date = $("input[name='end_date']").val();
	 var contest_name = $("#contestifi_contest_name").val();
	 var contest_type = $("#contestifi_contest_type").val();
	 //var bg_color = $("#contestifi_contest_bg_color").val();
	 var bg_color = '';
	 //var text_color = $("#contestifi_contest_text_color").val();
	 var text_color = '';
	 var button_color = $("#contestifi_contest_button_color").val();
	 var contest_frequency = $("input[name='contest_frequency']:checked").val();
	 var contestifi_no_of_attemption = $("#contestifi_no_of_attemption").val();
	 var contestifi_point_per_question = $("#contestifi_point_per_question").val();
	 var contestifi_negative_point = $("#contestifi_negative_point").val();
	 var contestifi_point_deduct_every_secod = $("#contestifi_point_deduct_every_secod").val();
	 var contestifi_max_time_allocation = $("#contestifi_max_time_allocation").val();
	 var contestifi_custom_frequecny = 0;
	 if (contest_type == '1')
	 {
	    if (contest_frequency == 4) {
	       contestifi_custom_frequecny = $("#contestifi_custom_frequecny").val();
	       if (contestifi_custom_frequecny == '' || contestifi_custom_frequecny == 0) {
		  $("#create_contestifi_contest_error").html("Please Enter Valid Custom Frequency");
		  return false;
	       }
	    }
	    if (start_date == '' || end_date == '' ||  contest_name == '' || contestifi_no_of_attemption == '' || contestifi_point_per_question == '') {
		  $("#create_contestifi_contest_error").html("Please Fill mandatory fields");
		  return false;
	    }
	    if (contestifi_no_of_attemption < 0 || contestifi_point_per_question < 0 || contestifi_negative_point < 0 || contestifi_point_deduct_every_secod < 0 || contestifi_max_time_allocation < 0)
	    {
	       $("#create_contestifi_contest_error").html("Please Enter valid value");
		  return false;
	    }
	    if (contestifi_max_time_allocation <= 0)
	    {
	       $("#create_contestifi_contest_error").html("Please Enter valid maximum time allocation");
		  return false;
	    }
	    var contestifi_contest_rule = $("input[name='contestifi_contet_rule[]']").map(function(){return $(this).val();}).get();
	    if (contestifi_contest_rule == '') {
	       $("#create_contestifi_contest_error").html("Please add rule");
	       return false;
	    };
	 }
	 else
	 {
	    if (contest_type == '3') {
	       contestifi_no_of_attemption = '1';
	    }
	    else{
	       if (contestifi_no_of_attemption == '') {
		  $("#create_contestifi_contest_error").html("Please Fill mandatory fields");
		  return false;
	       }
	    }
	    if (start_date == '' || end_date == '') {
		  $("#create_contestifi_contest_error").html("Please Fill mandatory fields");
		  return false;
	    }
	 }
	 var contestifi_contest_logo_image_name = '';
	 var contestifi_contest_logo_image_src = '';
	 contestifi_contest_logo_image_src = $('input[name="contestifi_logo_image_values"]').val();
	 if (typeof contestifi_contest_logo_image_src != "undefined")
	 {
	    contestifi_contest_logo_image_name = $('input[name="contestifi_logo_image_values"]').next().next().attr('name');
	    contestifi_contest_logo_image_src = $('input[name="contestifi_logo_image_values"]').val();
	 }
	 else
	 {
	    contestifi_contest_logo_image_src = '';
	    $("#create_contestifi_contest_error").html("Please Select Logo");
	    return false;
	 }
	 
	 $(".loading").show();
	 var form_data = new FormData();
	  form_data.append('contestifi_contest_logo_image_name',contestifi_contest_logo_image_name);
	 form_data.append('contestifi_contest_logo_image_src',contestifi_contest_logo_image_src);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("contest_name",contest_name);
	 form_data.append("contest_type",contest_type);
	 form_data.append("bg_color",bg_color);
	 form_data.append("contest_frequency",contest_frequency);
	 form_data.append("contestifi_custom_frequecny",contestifi_custom_frequecny);
	 form_data.append("contestifi_no_of_attemption",contestifi_no_of_attemption);
	 
	 form_data.append("contestifi_point_per_question",contestifi_point_per_question);
	 form_data.append("contestifi_negative_point",contestifi_negative_point);
	 form_data.append("contestifi_point_deduct_every_secod",contestifi_point_deduct_every_secod);
	 form_data.append("contestifi_max_time_allocation",contestifi_max_time_allocation);
	 form_data.append("contestifi_contest_rule",contestifi_contest_rule);
	 form_data.append("text_color",text_color);
	 form_data.append("button_color",button_color);
	 form_data.append("start_date",start_date);
	 form_data.append("end_date",end_date);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contestifi_contest",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       $("#contestifi_custom_frequecny_div").hide();
	       var contest_id = data;
	       contestifi_content_builder(contest_id);
	       $("#add_new_contestifi_contest")[0].reset();
	       $("#contestifi_contest_modal").modal("hide");
	    }
	 });
      }
      $(document).on('click', '.contestifi_contest_add_another_rule', function(){
	 var new_div = '<div class="col-sm-12 col-xs-12">'+
			      '<div class="col-sm-11 col-xs-11 nopadding-left">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				       '<input type="text" name="contestifi_contet_rule[]" class = "contestifi_contest_rule_class">'+
				       '<label>Rule<sup>*</sup></label>'+
				    '</div>'+
				    '</div>'+
				    '<div class="col-sm-1 col-xs-1">'+
				    '<label class="pull-right remove_contestifi_rule" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				    '</div>'+
			      '</div>';
	 $("#contestifi_contest_rule_div").append(new_div);
      });
      $(document).on('click', '.remove_contestifi_rule', function (){
	    $(this).parent().parent().remove();
      });
      $("input[name='contest_frequency']").change(function() {
	 var contest_frequency = $("input[name='contest_frequency']:checked").val();
	 if (contest_frequency == '4')
	 {
	    $("#contestifi_custom_frequecny_div").show();
	 }
	 else
	 {
	    $("#contestifi_custom_frequecny_div").hide();
	 }
      });
   
      function delete_contestifi_contest(contest_id)
      {
	 if (confirm("Are you sure you want to delete?"))
	 {
	    var locationid = $("#created_location_id").val();
	    $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/delte_contestifi_contest",
	       data: "contest_id="+contest_id+"&locationid="+locationid,
	       success: function(data){
		  contestifi_content_builder();
	       }
	    });
	 }
	 else
	 {
	    return false;
	 }
      }
   
      function edit_contestifi_contest(contest_id)
      {
	 $(".loading").show();
	 
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/get_contestifi_contest_for_edit",
	    data: "contest_id="+contest_id,
	    dataType: 'json',
	    success: function(data)
	    {
	       $("#contestifi_contest_id_edit").val(contest_id);
	       if (data.resultCode == '1')
	       {
		  var option_html = '';
		  if (data.contest_type == '2') {
		     $('.only_time_based_quiz_edit').hide();
		     $('.only_time_based_quiz_edit_no_of_attempt').show();
		     $("#contestifi_contest_rule_div_edit").hide();
		     option_html = '<option value="2">Image & Video Contest</option>';
		  }
		  else if (data.contest_type == '3') {
		     $('.only_time_based_quiz_edit').hide();
		     $('.only_time_based_quiz_edit_no_of_attempt').hide();
		     $("#contestifi_contest_rule_div_edit").hide();
		     option_html = '<option value="3">Survey</option>';
		  }
		  else{
		     $('.only_time_based_quiz_edit').show();
		     $('.only_time_based_quiz_edit_no_of_attempt').show();
		     $("#contestifi_contest_rule_div_edit").show();
		     option_html = '<option value="1">Time bound Quiz</option>';
		  }
		  $("#contestifi_contest_type_edit").html(option_html);
		  //$("#contestifi_contest_type_edit").val(data.contest_type);
		  $("#contestifi_contest_name_edit").val(data.contest_name);
		  $('#contest_logo_preview').attr('src', data.original_logo);
		  //$('#contestifi_contest_bg_color_edit').val(data.background_color);
		  //$('#contestifi_contest_text_color_edit').val(data.text_color);
		  $('#contestifi_contest_button_color_edit').val(data.button_color);
		  //$('name#contest_frequency_edit').val(data.contest_frequency);
		  if (data.contest_frequency == '1' || data.contest_frequency == '2' || data.contest_frequency == '3' || data.contest_frequency == '4')
		  {
		     $("input[name=contest_frequency_edit][value="+data.contest_frequency+"]").prop('checked', true);
		  }
		  
		  if (data.contest_frequency == '4')
		  {
		     $("#contestifi_custom_frequecny_div_edit").show();
		     $("#contestifi_custom_frequecny_edit").val(data.contestifi_custom_frequecny);
		  }
		  else
		  {
		     $("#contestifi_custom_frequecny_div_edit").hide();
		  }
		  $("#contestifi_no_of_attemption_edit").val(data.no_of_attemption);
		  $("#contestifi_point_per_question_edit").val(data.points_per_question);
		  $("#contestifi_negative_point_edit").val(data.negative_point);
		  $("#contestifi_point_deduct_every_secod_edit").val(data.point_deduct_every_second);
		  $("#contestifi_max_time_allocation_edit").val(data.max_time_allocation);
		  $(".start_date_edit").val(data.start_date);
		  $(".end_date_edit").val(data.end_date);
		  
		  for (var i = 0; i < data.contest_rule.length; i++)
		  {
		     var rule_obj = data.contest_rule[i];
		     if (i == 0)
		     {
			$("#contestifi_contest_rule_div_edit").html("");
			var new_div = '<div class="col-sm-12 col-xs-12">'+
				 '<label class="os-lable" style="font-weight:700">Contest Rules</label>'+
			      '</div>';
			$("#contestifi_contest_rule_div_edit").append(new_div);
			var new_div = '<div class="col-sm-12 col-xs-12">'+
				 '<div class="mui-textfield">'+
				       '<input type="text" class="cotenst_first_rule_edit" name="contestifi_contet_rule_edit[]" value="'+rule_obj.rule+'">'+
				       '<label>Rule<sup>*</sup></label>'+
				    '</div>'+
			      '</div>';
			$("#contestifi_contest_rule_div_edit").append(new_div);
		     }
		     else
		     {
			var new_div = '<div class="col-sm-12 col-xs-12">'+
			      '<div class="col-sm-11 col-xs-11 nopadding-left">'+
				 '<div class="mui-textfield">'+
				       '<input type="text" value="'+rule_obj.rule+'" name="contestifi_contet_rule_edit[]" class = "contestifi_contest_rule_class">'+
				       '<label>Rule<sup>*</sup></label>'+
				    '</div>'+
				    '</div>'+
				    '<div class="col-sm-1 col-xs-1">'+
				    '<label class="pull-right remove_contestifi_rule_edit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				    '</div>'+
			      '</div>';
			$("#contestifi_contest_rule_div_edit").append(new_div);
		     }
		  }
	       }
	       $("#contestifi_contest_update_modal").modal("show");
	       $(".loading").hide();
	    }
	 });
      }
      $(document).on('click', '.contestifi_contest_add_another_rule_edit', function(){
	 var new_div = '<div class="col-sm-12 col-xs-12">'+
			      '<div class="col-sm-11 col-xs-11 nopadding-left">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				       '<input type="text" name="contestifi_contet_rule_edit[]" class = "contestifi_contest_rule_class">'+
				       '<label>Rule<sup>*</sup></label>'+
				    '</div>'+
				    '</div>'+
				    '<div class="col-sm-1 col-xs-1">'+
				    '<label class="pull-right remove_contestifi_rule_edit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				    '</div>'+
			      '</div>';
	 $("#contestifi_contest_rule_div_edit").append(new_div);
      });
      $(document).on('click', '.remove_contestifi_rule_edit', function (){
	    $(this).parent().parent().remove();
      });
      $("input[name='contest_frequency_edit']").change(function() {
	 var contest_frequency = $("input[name='contest_frequency_edit']:checked").val();
	 if (contest_frequency == '4')
	 {
	    $("#contestifi_custom_frequecny_div_edit").show();
	 }
	 else
	 {
	    $("#contestifi_custom_frequecny_div_edit").hide();
	 }
      });
      
      function update_contestifi_contest()
      {
	 $("#contestifi_contest_add_another_rule_edit").html("");
	 var start_date = $("input[name='start_date_edit']").val();
	 var end_date = $("input[name='end_date_edit']").val();
	 var contest_id = $("#contestifi_contest_id_edit").val();
	 var contest_name = $("#contestifi_contest_name_edit").val();
	 var contest_type = $("#contestifi_contest_type_edit").val();
	 //var bg_color = $("#contestifi_contest_bg_color_edit").val();
	 var bg_color = '';
	 //var text_color = $("#contestifi_contest_text_color_edit").val();
	 var text_color = '';
	 var button_color = $("#contestifi_contest_button_color_edit").val();
	 var contest_frequency = $("input[name='contest_frequency_edit']:checked").val();
	 var contestifi_no_of_attemption = $("#contestifi_no_of_attemption_edit").val();
	 var contestifi_point_per_question = $("#contestifi_point_per_question_edit").val();
	 var contestifi_negative_point = $("#contestifi_negative_point_edit").val();
	 var contestifi_point_deduct_every_secod = $("#contestifi_point_deduct_every_secod_edit").val();
	 var contestifi_max_time_allocation = $("#contestifi_max_time_allocation_edit").val();
	 var contestifi_custom_frequecny = 0;
	 if (contest_type == '3') {
	    contestifi_no_of_attemption = '1';
	 }
	 if (contest_type == '1')
	 {
	 
	    if (contest_frequency == 4) {
	       contestifi_custom_frequecny = $("#contestifi_custom_frequecny_edit").val();
	       if (contestifi_custom_frequecny == '' || contestifi_custom_frequecny == 0) {
		  $("#create_contestifi_contest_error_edit").html("Please Enter Valid Custom Frequency");
		  return false;
	       }
	    }
	    if ( contest_name == '' || contestifi_no_of_attemption == '' || contestifi_point_per_question == '') {
		  $("#create_contestifi_contest_error_edit").html("Please Fill mandatory fields");
		  return false;
	    }
	    if (contestifi_no_of_attemption < 0 || contestifi_point_per_question < 0 || contestifi_negative_point < 0 || contestifi_point_deduct_every_secod < 0 || contestifi_max_time_allocation < 0)
	    {
	       $("#create_contestifi_contest_error_edit").html("Please Enter valid value");
		  return false;
	    }
	    if (contestifi_max_time_allocation <= 0)
	    {
	       $("#create_contestifi_contest_error_edit").html("Please Enter valid maximum time allocation");
		  return false;
	    }
	    var contestifi_contest_rule = $("input[name='contestifi_contet_rule_edit[]']").map(function(){return $(this).val();}).get();
	    if (contestifi_contest_rule == '') {
	       $("#create_contestifi_contest_error_edit").html("Please add rule");
	       return false;
	    }
	 }
	 var contestifi_contest_logo_image_name = '';
	 var contestifi_contest_logo_image_src = '';
	 contestifi_contest_logo_image_src = $('input[name="contestifi_logo_image_edit_values"]').val();
	 if (typeof contestifi_contest_logo_image_src != "undefined")
	 {
	    contestifi_contest_logo_image_name = $('input[name="contestifi_logo_image_edit_values"]').next().next().attr('name');
	    contestifi_contest_logo_image_src = $('input[name="contestifi_logo_image_edit_values"]').val();
	 }
	 else
	 {
	    contestifi_contest_logo_image_src = '';
	 }
	 
	 $(".loading").show();
	 var form_data = new FormData();
	  form_data.append('contestifi_contest_logo_image_name',contestifi_contest_logo_image_name);
	 form_data.append('contestifi_contest_logo_image_src',contestifi_contest_logo_image_src);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("contest_name",contest_name);
	 form_data.append("contest_type",contest_type);
	 form_data.append("bg_color",bg_color);
	 form_data.append("contest_frequency",contest_frequency);
	 form_data.append("contestifi_custom_frequecny",contestifi_custom_frequecny);
	 form_data.append("contestifi_no_of_attemption",contestifi_no_of_attemption);
	 
	 form_data.append("contestifi_point_per_question",contestifi_point_per_question);
	 form_data.append("contestifi_negative_point",contestifi_negative_point);
	 form_data.append("contestifi_point_deduct_every_secod",contestifi_point_deduct_every_secod);
	 form_data.append("contestifi_max_time_allocation",contestifi_max_time_allocation);
	 form_data.append("contestifi_contest_rule",contestifi_contest_rule);
	 form_data.append("contest_id",contest_id);
	 form_data.append("text_color",text_color);
	 form_data.append("button_color",button_color);
	 form_data.append("start_date",start_date);
	 form_data.append("end_date",end_date);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/update_contestifi_contest",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       $("#create_contestifi_contest_error_edit").html('');
	       contestifi_content_builder(contest_id);
	       $("#update_contestifi_contest")[0].reset();
	       $("#contestifi_contest_update_modal").modal("hide");
	    }
	 });
      }
   
   
      function open_add_contestifi_prize()
      {
	 $("#add_contestifi_contest_prize_modal").modal("show");
      }
      function add_contestifi_prize()
      {
	 var contestifi_contest_prize = $("#contestifi_contest_prize").val();
	 if ( contestifi_contest_prize == '') {
	       $("#create_contestifi_prize_error").html("Please Fill mandatory fields");
	       return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append('prize_id','');
	 form_data.append('contest_id',contest_id);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("contestifi_contest_prize",contestifi_contest_prize);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contistifi_contest_prize",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       var contestifi_contest_prize_id = data;
	       contestifi_content_builder(contest_id, '',contestifi_contest_prize_id);
	       $("#add_contenstifi_contest_prize")[0].reset();
	       $("#add_contestifi_contest_prize_modal").modal("hide");
	    }
	 });
      }
      
      function delte_contestifi_contest_prize(prize_id)
      {
	 if (confirm("Are you sure you want to delete?"))
	 {
	    var locationid = $("#created_location_id").val();
	    $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/delte_contestifi_contest_prize",
	       data: "prize_id="+prize_id+"&locationid="+locationid,
	       success: function(data){
		  var contest_id = $('.contestifi_prize_section').filter('[data-contestifi_contest_prize_id="'+prize_id+'"]').data('contestifi_contest_id');
		  contestifi_content_builder(contest_id);
	       }
	    });
	 }
	 else
	 {
	    return false;
	 }
      }
   
      function edit_contestifi_contest_prize(contest_id,prize_id, contest_prize)
      {
	 $("#contestifi_contest_prize_id_edit").val(prize_id);
	 $("#contestifi_contest_prize_edit").val(unescape(contest_prize));
	       $("#add_contestifi_contest_prize_modal_edit").modal("show");
	    
      }
      function update_contestifi_prize()
      {
	 $("#create_contestifi_prize_error_edit").html("");
	 var prize_id = $("#contestifi_contest_prize_id_edit").val();
	 var contestifi_contest_prize = $("#contestifi_contest_prize_edit").val();
	 if ( contestifi_contest_prize == '') {
	       $("#create_contestifi_prize_error_edit").html("Please Fill mandatory fields");
	       return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append('contest_id',contest_id);
	 form_data.append('prize_id',prize_id);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("contestifi_contest_prize",contestifi_contest_prize);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contistifi_contest_prize",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       contestifi_content_builder(contest_id, '',prize_id);
	       $("#add_contenstifi_contest_prize_edit")[0].reset();
	       $("#add_contestifi_contest_prize_modal_edit").modal("hide");
	    }
	 });
      }
      function contestifi_add_content_builder() {
	 
	 var form_data = new FormData(); 
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/contestifi_add_content_builder",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data){
	       if (Clicked_ButtonValue == 'exit') {
		  window.location = "<?php  echo base_url()?>location";
	       }else{
		  $(".loading").hide();
	       }
	    }
         });
      }
      
      $(document).ready(function(){
            $('.start_date').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       minDate: moment()
	    }).on('change', function(e, date){
	       $('.end_date').bootstrapMaterialDatePicker('setMinDate', date);
	    });
      
	    $('.end_date').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       minDate: moment()
	    }).on('change', function(e, date){
	       $('.start_date').bootstrapMaterialDatePicker('setMaxDate', date);
	      
	    });
	    
	    
	    
	    $('.start_date_edit').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       minDate: moment()
	    }).on('change', function(e, date){
	       $('.end_date_edit').bootstrapMaterialDatePicker('setMinDate', date);
	    });
      
	    $('.end_date_edit').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       minDate: moment()
	    }).on('change', function(e, date){
	       $('.start_date_edit').bootstrapMaterialDatePicker('setMaxDate', date);
	      
	    });
      });
      
      
      
      function contestifi_sign_term_condition() {
	 $(".loading").show();
	 var location_id = $("#created_location_id").val();
	 $.ajax({
               type: "POST",
                url: "<?php echo base_url()?>location/get_contestifi_signup_term_condition",
		data: "location_id="+location_id,
		dataType: 'json',
		success: function(data){
		  //$("#contestifi_term_conditon").val(data.terms);
		  CKEDITOR.instances.contestifi_term_condition.setData(data.terms);
		  $("#add_contestifi_contest_terms_modal").modal("show");
                  $(".loading").hide();
               }
	 });
      }
      function add_contestifi_terms_conditon() {
	 $(".loading").show();
	 var location_id = $("#created_location_id").val();
	 var terms = CKEDITOR.instances.contestifi_term_condition.getData();
	 //var terms = $("#contestifi_term_conditon").val();
	 $.ajax({
               type: "POST",
                url: "<?php echo base_url()?>location/add_contestifi_terms_conditon",
		data : ({'location_id': location_id, 'terms': terms}),
		dataType: 'json',
		success: function(data){
		  $("#add_contestifi_contest_terms_modal").modal("hide");
                  $(".loading").hide();
               }
	 });
      }
      
      function change_contest_type_drowdown()
      {
	 var contest_type = $("#contestifi_contest_type").val();
	 if (contest_type == '1')
	 {
	    $('.only_time_based_quiz').show();
	    $('.only_time_based_quiz_no_of_attempt').show();
	    $('#contestifi_contest_rule_div').show();
	 }
	 else
	 {
	    if (contest_type == 3)
	    {
	       $('.only_time_based_quiz_no_of_attempt').hide();
	    }
	    else
	    {
	       $('.only_time_based_quiz_no_of_attempt').show();
	    }
	    $('.only_time_based_quiz').hide();
	    $('#contestifi_contest_rule_div').hide();
	    $('#contestifi_custom_frequecny_div').hide();
	 }
      }
      
      
      function add_contestifi_survey()
      {
	 var poll_querstion = $("#survey_question").val();
	 var option_one = $("#survey_option_one").val();
	 var option_two = $("#survey_option_two").val();
	 var option_three = $("#survey_option_three").val();
	 var option_four = $("#survey_option_four").val();
	 var survey_type = $("#contestifi_contest_survey_type").val();
	 if ( poll_querstion == '' || option_one == '' || option_two == '') {
	       $("#create_contestifi_survey_error").html("Please Fill mandatory fields");
	       return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append('contest_id',contest_id);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("poll_querstion",poll_querstion);
	 form_data.append("option_one",option_one);
	 form_data.append("option_two",option_two);
	 form_data.append("option_three",option_three);
	 form_data.append("option_four",option_four);
	 form_data.append("survey_type",survey_type);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contistifi_survey_question",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       var quezi_question_id = data;
	       contestifi_content_builder(contest_id,quezi_question_id);
	       $("#add_contenstifi_survey_question")[0].reset();
	       $("#add_contestifi_survey_qustion_modal").modal("hide");
	    }
	 });
      }
      
      function edit_contestifi_survey_question(obj,contest_id,question_id) {
	 console.log(obj);
	 $("#contestifi_survey_question_id_hidden").val(question_id);
	 $("#survey_question_edit").val(obj.question);
	 $("#contestifi_contest_survey_type_edit").val(obj.survey_type);
	 if (obj.options.length > 0)
	 {
	    $("#survey_option_one_edit").val('');
	    $("#survey_option_two_edit").val('');
	    $("#survey_option_three_edit").val('');
	    $("#survey_option_four_edit").val('');
	    for (var i = 0; i < obj.options.length; i++)
	    {
	       var op_obj = obj.options[i];
	       if (i == '0') {
		  var option_id = "survey_option_one_edit";
	       }
	       else if (i == '1')
	       {
		  option_id = "survey_option_two_edit";
	       }
	       else if (i == '2')
	       {
		  option_id = "survey_option_three_edit";
	       }
	       else if (i == '3')
	       {
		  option_id = "survey_option_four_edit";
	       }
	       $("#"+option_id).val(op_obj.option_text);
	    }
	 }
	 $("#update_contestifi_survey_qustion_modal").modal("show");
      }
      function update_contestifi_survey()
      {
	 $("#create_contestifi_survey_error_edit").html("");
	 var quiz_question_id = $("#contestifi_survey_question_id_hidden").val();
	 var poll_querstion = $("#survey_question_edit").val();
	 var option_one = $("#survey_option_one_edit").val();
	 var option_two = $("#survey_option_two_edit").val();
	 var option_three = $("#survey_option_three_edit").val();
	 var option_four = $("#survey_option_four_edit").val();
	 if (poll_querstion == '' || option_one == '' || option_two == '')
	 {
	    $("#create_contestifi_survey_error_edit").html("Please Fill mandatory fields");
	    return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 
	 var survey_type = $("#contestifi_contest_survey_type_edit").val();
	 
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("poll_querstion",poll_querstion);
	 form_data.append("option_one",option_one);
	 form_data.append("option_two",option_two);
	 form_data.append("option_three",option_three);
	 form_data.append("option_four",option_four);
	 form_data.append("quiz_question_id",quiz_question_id);
	 form_data.append("contest_id",contest_id);
	 form_data.append("survey_type",survey_type);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/update_contistifi_survey_question",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       contestifi_content_builder(contest_id,quiz_question_id);
	       $("#update_contenstifi_survey_question")[0].reset();
	       $("#update_contestifi_survey_qustion_modal").modal("hide");
	    }
	 });
      }
      
      $("input[name='login_with_mobile_email_notpublic']").change(function() {
	 var login_with = $("input[name='login_with_mobile_email_notpublic']:checked").val();
	// 0:mobile; 1:email
	 if (login_with == 1) {
	    $("input[name='is_otpdisabled']").prop('checked', false);
	    $("input[name='is_otpdisabled']").parent().addClass('disabled');
	    $("#signup_require_field_email_mobile").html("User Mobile");
	 }
	 else
	 {
	    $("input[name='is_otpdisabled']").parent().removeClass('disabled');
	    $("#signup_require_field_email_mobile").html("User Email");
	 }
      });
      
      $("input[name='is_signup_enable']").change(function() {
	 var is_check = 0;
	 if ($('input[name="is_signup_enable"]').is(':checked')) {
	    is_check = $("input[name='is_signup_enable']:checked").val();
	 }
	 if (is_check == '0') {
	    $("#contestifi_signup_fields_div").hide();
	 }
	 else{
	    $("#contestifi_signup_fields_div").show();
	 }
      });
      
      
      
      
      function add_onehop_vip_ssid_view() {
	    onehop_get_vip_ssid_detail(1);
	    $("#add_onehop_vip_ssid").modal("show");
	}
	function onehop_get_vip_ssid_detail(index_number){ 
	    //var index_number = $(this).val();
	    var location_uid = $("#location_id_hidden").val();
	    $(".loading").show();
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url()?>location/onehop_get_ssid_detail",
		data: "location_uid="+location_uid+"&index_number="+index_number,
		dataType: 'json',
		success: function(data){
		    $(".loading").hide();
		    if (data.resultCode == '1') {
		       $("#onehop_ssid_name_vip").val(data.ssid_name);
		       $("#onehop_association_vip").val(data.association);
		       $("#onehop_wap_mode_vip").val(data.wap_mode);
		       $("#onehop_psk_vip").val(data.psk);
		       $("#onehop_auth_server_ip_vip").val(data.auth_server_ip);
		       $("#onehop_acc_server_ip_vip").val(data.acc_server_ip);
		    }else{
		       $("#onehop_ssid_name_vip").val();
		       $("#onehop_association_vip").val('1');
		       $("#onehop_psk_vip").val("");
		       $("#onehop_wap_mode_vip").val('3');
		       $("#onehop_auth_server_ip_vip").val(data.auth_server_ip);
		       $("#onehop_acc_server_ip_vip").val(data.acc_server_ip);
		    }
		}
	    });
	}
	
	function add_onehop_ssid_vip(){
	    var location_uid = $("#location_id_hidden").val();
	    var onehop_ssid_index = $("#onehop_ssid_index_vip").val();
	    var onehop_ssid_name = $("#onehop_ssid_name_vip").val();
	    var onehop_association = $("#onehop_association_vip").val();
	    var onehop_psk = $("#onehop_psk_vip").val();
	    var onehop_wap_mode = $("#onehop_wap_mode_vip").val();
	    var onehop_forwarding = '1';
	    var onehop_cp_mode = '0';
	    var onehop_cp_url = "";
	    var onehop_auth_server_ip = $("#onehop_auth_server_ip_vip").val();
	    var onehop_auth_server_port = '1812';
	    var onehop_auth_server_secret = 'testing123';
	    var onehop_accounting = '1';
	    var onehop_acc_server_ip = $("#onehop_acc_server_ip_vip").val();
	    var onehop_acc_server_port = "1813";
	    var onehop_acc_server_secret = "testing123";
	    var onehop_acc_interval = "60";
	    
	    var onehop_wallgarden_ip_list = "";
	    var onehop_wallgarden_domain_list = "";
     
      
	    if (onehop_association == '1') {
	       if (onehop_psk == '') {
		   $("#add_onehop_ssid_status_vip").html("Please enter Pre Shared Key");
		  return false;
	       }else if (onehop_psk.length < 8 || onehop_psk.length > 64) {
		  $("#add_onehop_ssid_status_vip").html("Pre Shared Key should be between 8 to 32 character");
		  return false;
	       }
	       
	    }
	    //validation
	    if (onehop_ssid_name.length > 32) {
	       $("#add_onehop_ssid_status_vip").html("SSID Name should be < 32 character");
	       return false;
	    }
	    var check_valid_ssid = validate_ssid(onehop_ssid_name);
	    if (check_valid_ssid == 0)
	    {
	       $("#add_onehop_ssid_status_vip").html("Invalid ssid. Only '-' ie Hyphen and '_' ie Underscore special character are allowed. And No space are allowed.");
	       return false
	    }
      
	    $(".loading").show();
	    $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/onehop_add_ssid",
		  data: "location_uid="+location_uid+"&onehop_ssid_index="+onehop_ssid_index+"&onehop_ssid_name="+onehop_ssid_name+"&onehop_association="+onehop_association+"&onehop_wap_mode="+onehop_wap_mode+"&onehop_forwarding="+onehop_forwarding+"&onehop_cp_mode="+onehop_cp_mode+"&onehop_cp_url="+onehop_cp_url+"&onehop_auth_server_ip="+onehop_auth_server_ip+"&onehop_auth_server_port="+onehop_auth_server_port+"&onehop_auth_server_secret="+onehop_auth_server_secret+"&onehop_accounting="+onehop_accounting+"&onehop_acc_server_ip="+onehop_acc_server_ip+"&onehop_acc_server_port="+onehop_acc_server_port+"&onehop_acc_server_secret="+onehop_acc_server_secret+"&onehop_acc_interval="+onehop_acc_interval+"&onehop_psk="+onehop_psk+"&onehop_wallgarden_ip_list="+onehop_wallgarden_ip_list+"&onehop_wallgarden_domain_list="+onehop_wallgarden_domain_list,
		  dataType: 'json',
		  success: function(data){
		     if (data.code == '1') {
			$("#add_onehop_ssid_status_vip").html('');
			var location_uid = $("#location_id_hidden").val();
			$("#add_onehop_vip_ssid").modal("hide");
			nas_setup_list(location_uid);
		     }else{
			$(".loading").hide();
			$("#add_onehop_ssid_status_vip").html(data.msg);
		     }
		    
		  }
	    });
	}
   </script>
      
      <script>
	$(document).ready(function() {
            var location_type = $("#location_type").val();
	    if (location_type == '13'){
	       retail_audit_sku_pin_list();
	    }
	    
        });
	function retail_audit_sku_pin_list() {
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
	       type:"POST",
               url: "<?php echo base_url()?>location/retail_audit_sku_pin_list",
               cache:false,
               dataType: 'json',
               data:"location_uid="+location_uid,
		success: function(data){
		    $(".loading").hide();
		    if(data.skuresultCode == '0')
		    {
		       $("#retail_audit_sku_table").hide();
		    }
		    else
		    {
		     $("#retail_audit_sku_table").show();
		     var sku = data.sku_list;
		     var sku_list = '';
		     for(var i = 0; i < sku.length; i++) {
			sku_list += '<tr><td class="text-left">'+sku[i].sku_name+' </td> <td><i style="cursor: pointer" onclick="delete_retail_audit_sku_name(\''+sku[i].sku_id+'\',\''+sku[i].sku_name+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>&nbsp;<i style="cursor: pointer" onclick="delete_offline_vm_organisation('+sku[i].sku_id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></td></tr>';
			 }
		     $("#retail_audit_sku_table_data").html(sku_list);
		    }
		    if(data.pinresultCode == '0')
		    {
		       $("#retail_audit_pin_list").html("");
		       $("#retail_audit_pin_list_promoter").html("");
		    }
		    else
		    {
			$("#retail_audit_pin_list").html(data.audit_pin);
			var promoter_val = '';
			if (data.promoter_pin != 0) {
			    promoter_val = data.promoter_pin;
			}
			$("#retail_audit_pin_list_promoter").html(promoter_val);
		    }
               }
	    });
	 }
	function edit_retail_audit_sku_name(sku_id, sku_name) {
	    $("#retail_audit_sku_name").val(sku_name);
	    $("#retail_audit_sku_id").val(sku_id);
	    $("#add_retail_audit_sku_button").html("Update");
	    $("#retail_audit_sku_name").parent().removeClass("mui-textfield--float-label");
	    $("#edit_retail_audit_sku_remove_button").show();
	}
	function edit_retail_audit_sku_unset() {
	    $("#retail_audit_sku_name").val('');
	    $("#retail_audit_sku_id").val('');
	    $("#add_retail_audit_sku_button").html("Add");
	    $("#retail_audit_sku_name").parent().addClass("mui-textfield--float-label");
	    $("#edit_retail_audit_sku_remove_button").hide();
	}
	function add_retail_audit_sku() {
	    $("#add_retail_audit_sku_error").html("");
	    var sku_name = $("#retail_audit_sku_name").val();
	    var sku_id = $("#retail_audit_sku_id").val();
	    if (sku_name == '') {
	       $("#add_retail_audit_sku_error").html("Please Enter SKU");
	       return false;
	    }else{
	      
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("sku_name", sku_name);
	       form_data.append("sku_id", sku_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_retail_audit_sku",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (data == '0')
		     {
			$(".loading").hide();
			$("#add_retail_audit_sku_error").html("SKU already exist");
		     }
		     else
		     {
			retail_audit_sku_pin_list();
			$("#retail_audit_sku_name").val('');
			$("#retail_audit_sku_id").val('');
			$("#add_retail_audit_sku_button").html("Add");
			$("#retail_audit_sku_name").parent().addClass("mui-textfield--float-label");
			$("#edit_retail_audit_sku_remove_button").hide();
		     }
		     
		  }
	       });
	    }
	 }
	 
	function add_retail_audit_pin() {
	    $("#add_retail_audit_pin_error").html("");
	    
	    var login_pin = $("#retail_audit_login_pin").val();
	    
	    if (login_pin == '') {
	       $("#add_retail_audit_pin_error").html("Please Enter PIN");
	       return false;
	    }else{
		if (isNaN(login_pin) || login_pin.length != '6') {
		  $("#add_retail_audit_pin_error").html("Please Enter valid 6 digit Numeric PIN");
		  return false;
		}
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("login_pin", login_pin);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_retail_audit_pin",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
			$(".loading").hide();
			if (data == 1) {
			    $("#retail_audit_login_pin").val('');
			 $("#retail_audit_pin_list").html(login_pin);
			    $("#add_retail_audit_pin_error").html("");
			}
			else
			{
			    $("#add_retail_audit_pin_error").html("Code already used in  program or store");
			}
		  }
	       });
	    }
	 }
	 function add_retail_audit_pin_promoter() {
	    $("#add_retail_audit_pin_error_promoter").html("");
	    
	    var login_pin = $("#retail_audit_login_pin_promoter").val();
	    
	    if (login_pin == '') {
	       $("#add_retail_audit_pin_error_promoter").html("Please Enter PIN");
	       return false;
	    }else{
		if (isNaN(login_pin) || login_pin.length != '6') {
		  $("#add_retail_audit_pin_error_promoter").html("Please Enter valid 6 digit Numeric PIN");
		  return false;
		}
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("login_pin", login_pin);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_retail_audit_pin_promoter",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
			$(".loading").hide();
		    if (data == 1) {
			$("#retail_audit_login_pin_promoter").val('');
			$("#retail_audit_pin_list_promoter").html(login_pin);
			$("#add_retail_audit_pin_error_promoter").html("");
		    }
		    else
		    {
			$("#add_retail_audit_pin_error_promoter").html("Code already used in  program or store");
		    }
		  }
	       });
	    }
	 }
	 function delete_retail_audit_sku_name(sku_id) {
	    var location_id = $("#created_location_id").val();
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_retail_audit_sku_name",
		  data: "sku_id="+sku_id+"&location_id="+location_id,
		  success: function(data){
		     retail_audit_sku_pin_list();
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 
	 function add_event_module_signup_extra_field() {
	    $("#add_retail_audit_sku_error").html("");
	    var field_name = $("#event_module_signup_extra_field").val();
	    if (field_name == '') {
	       $("#event_module_signup_extra_field_error").html("Please Enter field name");
	       return false;
	    }else{
	      
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("field_name", field_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_event_module_signup_extra_field",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (data == '0')
		     {
			$("#event_module_signup_extra_field_error").html("field already exist");
		     }
		     else
		     {
			$("#event_module_signup_extra_field").val('');
			$("#event_module_signup_extra_field").parent().addClass("mui-textfield--float-label");
			$("#event_module_signup_extra_field_table").show();
			var tab_data = '';
			tab_data += '<tr id="em_'+data+'">';
			tab_data += '<td>'+field_name+'</td>';
			tab_data += '<td><i style="cursor: pointer" onclick="delete_event_module_signup_field('+data+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></td>';
			tab_data += '</tr>';
			$("#event_module_signup_extra_field_table_data").append(tab_data);
		     }
		     $(".loading").hide();
		     
		  }
	       });
	    }
	 }
	 
	 function delete_event_module_signup_field(field_id) {
	    var location_id = $("#created_location_id").val();
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_event_module_signup_field",
		  data: "field_id="+field_id+"&location_id="+location_id,
		  success: function(data){
		    $(".loading").hide();
		    $("#em_"+field_id).remove();
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 $('#hashtags').keyup(function(){  
	var currpointer = document.getElementById('hashtags').selectionStart;
	var hashtagvalue = $('#hashtags').val();
	var hashidx = hashtagvalue.substring(0, currpointer).lastIndexOf('#');
	var search_val = hashtagvalue.substring(hashidx, currpointer);
	if(search_val.indexOf(" ") >= 0)
	{
        }
	else
	{
	    $('#search_filter_areas').show();
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url()?>location/hash_tag_search",
		data: "search_val="+search_val,
		success: function(data){
		    $("#search_filter_areas").html(data);
		}
	    });
        }
    });
    $('body').click(function(e) {
        if(e.target.id != 'hashtags'){
	    $('#search_filter_areas').hide();
        }
    });
    $(document).on('click', '.select_tags', function(){
	var get_value = $(this).html();
	var currpointer = document.getElementById('hashtags').selectionStart;
	var hashtagvalue = $('#hashtags').val();
	var hashidx = hashtagvalue.substring(0, currpointer).lastIndexOf('#');
	var final_value = hashtagvalue.replaceBetween(hashidx,currpointer,get_value+" ");
	var hashtagvalue = $('#hashtags').val(final_value);
    });
    String.prototype.replaceBetween = function(start, end, what) {
	return this.substring(0, start) + what + this.substring(end);
    };
    
    
    
    $(document).on('click', '.lms_visitor_list_of_type_odm', function(){
	$('.lms_visitor_list_of_type_odm').removeClass('mui-btn--danger');
	$(this).addClass('mui-btn--danger');
	var button_id = $(this).attr('id');
	
	if (button_id == 'lms_department_button') {
	   $("#lms_department_listing_div").show();
	   $("#lms_employee_listing_div").hide();
	   //vm_offline_department_list();
	}
	else if (button_id == 'lms_employee_button') {
	   $("#lms_department_listing_div").hide();
	   $("#lms_employee_listing_div").show();
	}
     });
    function lms_add_vm_offline_department() {
	    $("#lms_add_offline_department_error").html("");
	    var org_id = 0;
	    var offline_department_name = $("#lms_vm_dept_name").val();
	    
	    if (offline_department_name == '') {
	       $("#lms_add_offline_department_error").html("Please enter department name");
	       return false;
	    }else{
	      
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("offline_department_name", offline_department_name);
	       form_data.append("org_id", org_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_offline_vm_department",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (data == '0')
		     {
			$(".loading").hide();
			$("#lms_add_offline_department_error").html("Department already exist");
		     }
		     else
		     {
			lms_vm_offline_department_list();
			$("#lms_vm_dept_name").val('');
			
		     }
		     
		  }
	       });
	    }
	 }
    function lms_vm_offline_department_list() {
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
	       type:"POST",
               url: "<?php echo base_url()?>location/lms_vm_offline_department_list",
               cache:false,
               dataType: 'json',
               data:"location_uid="+location_uid,
	       success: function(data){
		  dept_list_array = data;
		  $(".loading").hide();
		  if(data.resultCode == '0')
		  {
		     $("#lms_vm_dept_list_tabel").hide();
		  }
		  else
		  {
		     $("#lms_vm_dept_list_tabel").show();
		     var department = data.department;
		     var dept_list = '';
		     var dept_list_for_emp_add = '<option value="">Select Depatment</option>';
		     for(var i = 0; i < department.length; i++) {
			dept_list += '<tr><td class="text-left">'+department[i].department_name+' </td><td><i style="cursor: pointer" onclick="delete_offline_vm_department('+department[i].department_id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></td></tr>';
			dept_list_for_emp_add += '<option value="'+department[i].department_id+'">'+department[i].department_name+'</option>';
		     }
		     $("#lms_vm_dept_list_tabel_data").html(dept_list);
		     $("#lms_vm_emp_dept_name").html(dept_list_for_emp_add);
		  }
               }
	    });
	 }
	function lms_add_vm_offline_employee() {
	    $("#lms_add_offline_employee_error").html("");
	    var org_id = 0;
	    var emp_unique_id = $("#lms_vm_emp_id").val();
	    var dept_id = $("#lms_vm_emp_dept_name").val();
	    var employee_name = $("#lms_vm_emp_name").val();
	    var employee_last_name = $("#lms_vm_emp_last_name").val();
	    var employee_email = $("#lms_vm_emp_email").val();
	    var employee_mobile = $("#lms_vm_emp_mobile").val();
	    if (!validateEmail(employee_email)) {
	       $("#lms_add_offline_employee_error").html("Enter valie email");
	       return false;
	    }
	    //alert(employee_name+"--"+employee_email+"--"+employee_mobile+"--"+dept_id)
	    if (employee_name == '' || employee_email == '' || employee_mobile == '' || dept_id == '' || emp_unique_id == '') {
	       $("#lms_add_offline_employee_error").html("Please fill all fields");
	       return false;
	    }else{
	      
	       $(".loading").show();
	       
	       var form_data = new FormData();
	       form_data.append("employee_name", employee_name);
	       form_data.append("employee_last_name", employee_last_name);
	       form_data.append("employee_email", employee_email);
	       form_data.append("employee_mobile", employee_mobile);
	       form_data.append("org_id", org_id);
	       form_data.append("dept_id", dept_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("emp_unique_id", emp_unique_id);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_vm_offline_employee",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (data == '0')
		     {
			$(".loading").hide();
			$("#lms_add_offline_employee_error").html("Employee Mobile or Email Already exist");
		     }
		     else if (data == 2)
		     {
			$(".loading").hide();
			$("#lms_add_offline_employee_error").html("Employee ID Already exist");
		     }
		     else
		     {
			lms_vm_offline_employee_list();
			$("#lms_vm_emp_id").val('');
			$("#lms_vm_emp_name").val('');
			$("#lms_vm_emp_last_name").val('');
			$("#lms_vm_emp_email").val('');
			$("#lms_vm_emp_mobile").val('');
			
		     }
		     
		  }
	       });
	    }
	 }
	function lms_vm_offline_employee_list() {
	    var location_uid =  $("#location_id_hidden").val();
	    $.ajax({
	       type:"POST",
               url: "<?php echo base_url()?>location/lms_vm_offline_employee_list",
               cache:false,
               dataType: 'json',
               data:"location_uid="+location_uid,
	       success: function(data){
		  $(".loading").hide();
		  if(data.resultCode == '0')
		  {
		     $("#lms_vm_emp_list_tabel_data").hide();
		  }
		  else
		  {
		     $("#lms_vm_emp_list_tabel_data").show();
		     var employee = data.employee;
		     var emp_list = '';
		     for(var i = 0; i < employee.length; i++) {
			emp_list += '<tr><td class="text-left">'+employee[i].employee_unique_id+' </td><td class="text-left">'+employee[i].employee_name+' </td><td>'+employee[i].employee_last_name+' </td><td>'+employee[i].employee_email+' </td><td>'+employee[i].employee_mobile+' </td><td>'+employee[i].department_name+'</td><td><i style="cursor: pointer" onclick="delete_offline_vm_employee('+employee[i].employee_id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></td></tr>';
		     }
		     $("#lms_vm_emp_list_tabel_data").html(emp_list);
		  }
               }
	    });
	 }
	 
    </script>
   </body>
</html>
