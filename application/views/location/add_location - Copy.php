<?php $this->load->view('includes/header');?>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/responsiveCarousel.min.js"></script>
<!-- Start your project here-->
<div class="loading" >
   <img src="<?php echo base_url()?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <div id="sidedrawer-brand" class="mui--appbar-line-height">
               <?php
                  $isplogo = $this->permission_model->get_isplogo();
                  if($isplogo != 0){
                     echo '<img src="'.IMAGEPATHISP."small/".$isplogo.'" class="img-responsive"/>';
                  }else{
                  
                     echo '<img src="'.base_url().'assets/images/isp_logo.png" class="img-responsive"/>';
                  }
                      ?>
            </div>
              
            <?php
            $data['navperm']=$this->plan_model->leftnav_permission();
            $this->load->view('includes/left_nav');?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">Add New Location</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                           <a href="<?php echo base_url()?>location">
                              <div class="mui-btn---small">
                                 <img src="<?php echo base_url()?>assets/images/close.svg"/>
                              </div>
                           </a>
                        </li>
                         <?php $this->load->view('includes/global_setting',$data);?>
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid" id="right-container-fluid">
               <div class="add_user">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <ul class="mui-tabs__bar">
                              <li class="mui--is-active">
                                 <a data-mui-toggle="tab" data-mui-controls="Location_details" >
                                 Location Details
                                 <span style = "display: none" id="location_detail_tab_checked"> &nbsp;&nbsp;<i class="fa fa-check" aria-hidden="true" style="color: #39b54a!important;"></i></span>
                                 </a>
                              </li>
                              <li class="disabled">
                                 <a data-mui-toggle="tab" data-mui-controls="captive_portal" class="tab_menu" >
                                 Captive Portal
                                 <span style = "display: none" id="captive_portal_tab_checked"> &nbsp;&nbsp;<i class="fa fa-check" aria-hidden="true" style="color: #39b54a!important;"></i></span>
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="nas_setup" class="tab_menu" onclick="nas_setup_list()">
                                 NAS Setup
                                 <span style = "display: none" id="nas_setup_tab_checked"> &nbsp;&nbsp;<i class="fa fa-check" aria-hidden="true" style="color: #39b54a!important;"></i></span>
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="access_points" class="access_point_tab_menu" onclick="access_point_list()">
                                 Access Points
                                 <span style = "display: none" id="access_point_tab_checked"> &nbsp;&nbsp;<i class="fa fa-check" aria-hidden="true" style="color: #39b54a!important;"></i></span>
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="Vouchers" class="tab_menu">
                                 Vouchers
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!-- location tab start -->
                        <div class="mui-tabs__pane mui--is-active" id="Location_details">
                           <form id="add_location" onsubmit="add_location(); return false;">
                              <div class="form-group">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row">
                                       <h2>Location Details</h2>
                                    </div>
                                    <div class="row">
                                       <h4>TYPE</h4>
                                    </div>
                                    <div class="row" id="location_type_div">
                                       <label class="radio-inline">
                                       <input type="radio" name="location_type"   value="1" required> Public
                                       </label>
                                       <label class="radio-inline">
                                       <input type="radio" name="location_type" value="2"> HOTEL
                                       </label>
                                       <label class="radio-inline">
                                       <input type="radio" name="location_type" value="3"> HOSPITAL
                                       </label>
                                       <label class="radio-inline">
                                       <input type="radio" name="location_type" value="4"> INSTITUTIONAL
                                       </label>
                                       <label class="radio-inline">
                                       <input type="radio" name="location_type" value="5"> ENTERPRISE
                                       </label>
                                       <label class="radio-inline">
                                       <input type="radio" name="location_type" value="6"> CAFE
                                       </label>
                                    </div>
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <!-- hidden value saved start -->
                                                      <input type="hidden" name="created_location_id" id="created_location_id">
                                                      <input type="hidden" name="location_name_hidden" id="location_name_hidden">
                                                      <input type="hidden" name="location_id_hidden" id="location_id_hidden">
                                                      <input type="hidden" name="email_hidden" id="email_hidden">
                                                      <input type="hidden" name="mobile_hidden" id="mobile_hidden">
                                                      <!-- hidden value saved end -->
                                                      <input type="text" name="location_name" id="location_name" required>
                                                      <label>Location Name <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" id="geolocation" name="geoaddr" required>
                                                      <input type="hidden"  id="placeid" name="placeid"  readonly>
                                                      <input type="hidden"  id="lat" name="lat" readonly>
                                                      <input type="hidden" id="long" name="long"  readonly>
                                                      <label>Geo Location Lookup <sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <div class="map_container">
                                       <div class="span8" style="margin-bottom: 15px; border:2px solid #6D6E71; height: 150px;" id="map1">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-4 col-xs-4">
                                          <span id="placeidview"></span>
                                       </div>
                                       <div class="col-sm-4 col-xs-4">
                                          <span id="latview"></span>
                                       </div>
                                       <div class="col-sm-4 col-xs-4">
                                          <span id="longview"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row" id="user_type_lead_enquiry">
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="address1" id="address1" required>
                                                   <label>Address Line 1 <sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="address2" id="address2" required>
                                                   <label>Address Line 2 <sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="pin" id="pin" pincode- pattern="[0-9]{1-20}" required>
                                                   <label>PIN Code <sup>*</sup></label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                <div class="mui-select">
                                                   <select name="state" id="state" required>
                                                      <option value="">All States</option>
                                                      <?php
                                                         $state_list = $this->location_model->state_list();
                                                              if($state_list->resultCode == 1){
                                                            foreach($state_list->state as $state_list_view){
                                                               echo '<option value = "'.$state_list_view->state_id.'">'.$state_list_view->state_name.'</option>';
                                                            }
                                                            
                                                         }
                                                         ?>
                                                   </select>
                                                   <label>State<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select name="city" id="city" required>
                                                      <option value="">Select City</option>
                                                   </select>
                                                   <label>City<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select id="zone" name="zone" required>
                                                      <option value="">Select Zone</option>
                                                   </select>
                                                   <label>Zone<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h5 class="create_headings" onclick="add_zone_model()">
                                                   Add New Zone
                                                </h5>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-left">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="location_id" id="location_id" value=" <?php echo $locationid->location_id?>" readonly>
                                                   <span class="title_box">(auto-generated)</span>
                                                   <label>Location ID <sup>*</sup></label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <h4>CONTACT PERSON</h4>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="contact_person_name" id="contact_person_name" required>
                                                   <label>Full Name<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="email" name="email_id" id="email_id"  required>
                                                   <label>Email ID <sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="mobile_no" id="mobile_no" pattern="[0-9]{1-20}" required>
                                                   <label>Mobile NO <sup>*</sup></label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px">
                                          <div class="row">
                                             <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 nomargin-left">
                                                <p id="add_location_error" style="color:red"></p>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value="exit"> SAVE & EXIT</button>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg out_line_btn"  value="next"> NEXT</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                        <!-- location End start -->
                        <!-- cpative portal tab start -->
                        <div class="mui-tabs__pane" id="captive_portal">
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <h2>Captive Portal</h2>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="col-sm-12 col-xs-12 mui--text-right">
                                    <p><strong><span class="location_name_view"></span> <span class="location_id_view"></span></strong></p>
                                    <p><small style="color:#808080" class="email_view"></small></p>
                                    <p><small style="color:#808080" class="mobile_number_view"></small></p>
                                 </div>
                              </div>
                           </div>
                           <div class="row" id="cp_public_location" style="display: none">
                              <div class="col-sm-6 col-xs-6">
                                 <form id="add_captive_portal" onsubmit="add_captive_portal(); return false;">
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <h4>SELECT CAPTIVE PORTAL TYPE</h4>
                                          </div>
                                          <div class="row" style="margin-bottom: 15px;">
                                             <label class="radio-inline">
                                             <input type="radio" name="captive_potal_type" value="cp1" checked> CP1 (Time Based)
                                             </label>
                                             <label class="radio-inline">
                                             <input type="radio" name="captive_potal_type"  value="cp2"> CP2 (Data Earn & Burn)
                                             </label>
                                             <label class="radio-inline">
                                             <input type="radio" name="captive_potal_type"  value="cp3"> CP3 (Hybrid)
                                             </label>
                                          </div>
                                          <div class="row"  style="margin-bottom: 15px;">
                                             <span class="cp_span">This Captivew portal lets the user surf for free for a limited time and requires them to view sponsored content
                                             Eg: Videos, Offers etc. Users can access interent multile times after consuming content for each session.</span><br/>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					   <div class="row" >
                                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="public_wifi_ssid">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
				       </div>
                                    </div>

                                    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="row" style="margin-bottom: 15px;">
					     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							 <div class="row">
							 <label class="radio-inline">
							    <input type="radio" name="is_otpdisabled_public_location"  value="0" checked> OTP Enable
							 </label>
							 </div>
					     </div>
					     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							 <div class="row">
							 <label class="radio-inline">
							    <input type="radio" name="is_otpdisabled_public_location"  value="1"> OTP Disable
							 </label>
							 </div>
					     </div>
					  </div>
				       </div>
                                    </div>

                                    <!-- cp1 fields start -->
                                    <div id="cp1_div">
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="cp1_plan" id="cp1_plan">
                                                      <?php
                                                         echo $plan_list = $this->location_model->plan_list('2');
                                                         ?>
                                                      </select>
                                                      <label>Select Time Based Plan <sup>*</sup></label>
                                                   </div>
                                                   <span class="cp_span_color">Only Time Limit based plans can be used here </span>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top:30px">
                                                   <a class="create_headings" href="<?php echo base_url()?>plan/add_plan">
                                                   Create New Plan
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp1_no_of_daily_session" id="cp1_no_of_daily_session">
                                                      <label>NO OF DAILY SESSION<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- cp1 fields END -->
                                    <!-- cp2 fields start -->
                                    <div id="cp2_div" style="display: none">
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="cp2_plan" id="cp2_plan">
                                                      <?php
                                                         echo $plan_list = $this->location_model->plan_list('4');
                                                         ?>
                                                      </select>
                                                      <label>Select Data  Plan <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top:30px">
                                                   <a class="create_headings" href="<?php echo base_url()?>plan/add_plan">
                                                   Create New Plan
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp2_signip_data" id="cp2_signip_data">
                                                      <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                      <label>Data on Signup<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp2_daily_data" id="cp2_daily_data">
                                                      <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                      <label>Daily Data Quota<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- cp2 fields END -->
                                    <!-- cp3 fields start -->
                                    <div id="cp3_div" style="display: none">
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="cp3_hybrid_plan" id="cp3_hybrid_plan">
                                                      <?php
                                                         echo $plan_list = $this->location_model->plan_list('5');
                                                         ?>
                                                      </select>
                                                      <label>Select Hybrid Plan <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top:30px">
                                                   <a class="create_headings" href="<?php echo base_url()?>plan/add_plan">
                                                   Create New Plan
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="cp3_data_plan" id="cp3_data_plan">
                                                      <?php
                                                         echo $plan_list = $this->location_model->plan_list('4');
                                                         ?>
                                                      </select>
                                                      <label>Select Data Plan <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top:30px">
                                                   <a class="create_headings" href="<?php echo base_url()?>plan/add_plan">
                                                   Create New Plan
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp3_signip_data" id="cp3_signip_data">
                                                      <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                      <label>Data on Signup<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp3_daily_data" id="cp3_daily_data">
                                                      <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                      <label>Daily Data Quota<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- cp3 fields END -->
                                    <!--div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                <legend>UPLOAD A BRAND LOGO</legend>
                                                <div class="mui-textfield mui-textfield--float-label blogoimg">
                                                   <input type="text" id="cp1_brand_image_name">
                                                   <input type="file" id="exampleInputFile" name="banner_image" class="hide"/>
                                                   <label>Logo Image <sup>*</sup></label>
                                                </div>
                                                <span class="cp_span">You can upload JPG. PNG, BMP upto 2mb in size</span>
                                             </div>
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top: 40px">
                                                <p id="but" class="mui-btn mui-btn--small mui-btn--primary" style="height:30px; padding: 0px 25px; background-color:#36465f; font-weight:600; line-height: 30px">
                                                   CHOOSE FILE
                                                   <span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span>
                                                </p>
                                             </div>
                                             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="padding-top: 40px">
                                                <p class="mui-btn mui-btn--small mui-btn--danger" type="button" onclick="unset_image()" id="unset_image_button" style="display: none; height:28px; background-color:#36465f;">Unset Image</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div-->
				    <div class="form-group" style="margin-top: 15px">
				       <input type="file" id="exampleInputFile" name="banner_image" class="hide"/>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
					     <legend>UPLOAD A BRAND LOGO</legend><br/>
					     <div class="dropzone" data-width="900" data-ajax="false" data-originalsize="false" data-height="300" style="width: 300px; height:100px;">
						<input type="file" name="thumb"  />
					     </div>
					  </div>
				       </div>
				    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px">
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                <p id="add_captive_portal_error" style="color:red"></p>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value="exit"> SAVE & EXIT</button>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg out_line_btn"  value="next"> NEXT</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                       <div class="slider_div pull-right">
                                          <div class="right_slider">
                                             <h3 class="cp_type_slider" >CAPTIVE PORTAL 1 SAMPLE</h3>
                                             <div id="crsl_items_public_location" class="crsl-items" data-navigation="navbtns">
                                                <div class="crsl-wrap">
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center   class = "cp1_slider1">
                                                            <img src="<?php echo base_url()?>assets/images/cp_1_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="display: none" class = "cp2_slider1">
                                                            <img src="<?php echo base_url()?>assets/images/cp_2_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="display: none" class = "cp3_slider1">
                                                            <img src="<?php echo base_url()?>assets/images/cp_3_img.jpg" class="img-responsive"/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-a">
                                                         <div class=" crsl-item-logo">
                                                            <img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center  class = "cp1_slider2">
                                                            <img src="<?php echo base_url()?>assets/images/cp_1_img_2.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="display: none" class = "cp2_slider2">
                                                            <img src="<?php echo base_url()?>assets/images/cp_2_img_2.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="display: none" class = "cp3_slider2">
                                                            <img src="<?php echo base_url()?>assets/images/cp_3_img_2.jpg" class="img-responsive"/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-a">
                                                         <div class=" crsl-item-logo">
                                                            <img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class = "cp1_slider3">
                                                            <img src="<?php echo base_url()?>assets/images/cp_1_img_3.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="display: none" class = "cp2_slider3">
                                                            <img src="<?php echo base_url()?>assets/images/cp_2_img_3.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="display: none" class = "cp3_slider3">
                                                            <img src="<?php echo base_url()?>assets/images/cp_3_img_3.jpg" class="img-responsive"/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-a">
                                                         <div class=" crsl-item-logo">
                                                            <img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center  class = "cp1_slider4">
                                                            <img src="<?php echo base_url()?>assets/images/cp_1_img_4.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="display: none" class = "cp2_slider4">
                                                            <img src="<?php echo base_url()?>assets/images/cp_2_img_4.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="display: none" class = "cp3_slider4">
                                                            <img src="<?php echo base_url()?>assets/images/cp_3_img_4.jpg" class="img-responsive"/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-a">
                                                         <div class=" crsl-item-logo">
                                                            <!--img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider"/-->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <nav class="slidernav">
                                                <div id="navbtns" class="clearfix">
                                                   <a href="#" class="previous pull-left">
                                                   <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                   </a>
                                                   <span class="previous_span"><strong>Setp 01:</strong> User Enter Mobile No.</span>
                                                   <a href="#" class="next pull-right">
                                                   <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                   </a>
                                                </div>
                                             </nav>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row" id="cp_hotel_location" style="display: none">
                              <div class="col-sm-6 col-xs-6">
                                 <form id="add_hotel_captive_portal"  enctype="multipart/form-data" onsubmit="add_hotel_captive_portal(); return false;">
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          
                                          <div id="hotel_ssid_div" style="display:none">
					     <div class="row" >
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="hotel_guest_wifi_ssid">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
					     <div class="row">
						<legend>Conigure your captive portal for this location </legend>
					     </div>
                                             <div class="row" >
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="hotel_guest_wifi" value="1" > <strong style="margin-left:15px">Guest WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="hotel_visitor_wifi" value="1" > <strong style="margin-left:15px">Visitor WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                          
                                          </div>
                                          <div class="row" id="institutional_ssid_div" style="display:none">
                                             <div class="col-lg-6 col-md-6 col-sm-36 col-xs-6 nomargin-left">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="institutional_wifi_ssid">
                                                   <span class="title_box"> (Max 24 Chars.)</span>
                                                   <label>SSID Name</label>
                                                </div>
                                             </div>
                                          </div>
                                          <div  id="caffe_ssid_div" style="display:none">
                                             <div class = "row">
                                                <div class="col-lg-6 col-md-6 col-sm-36 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="caffe_wifi_ssid">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
                                             
                                             
                                          </div>
                                          <div id="hospital_ssid_div" style="display:none">
					     <div class="row" >
                                               
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="hospital_patient_wifi_ssid">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
					     <div class="row">
						<legend>Conigure your captive portal for this location </legend>
					     </div>
                                             <div class="row" >
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="hospital_patient_wifi" value="1" > <strong style="margin-left:15px">Patient WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
						 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="hospital_guest_wifi" value="1" > <strong style="margin-left:15px">Guest WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="enterprise_ssid_div" style="display: none">
					     <div class="row" >
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="enterprise_employee_wifi_ssid">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
					     <div class="row">
						<legend>Conigure your captive portal for this location </legend>
					     </div>
                                             <div class="row" >
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="enterprise_employee_wifi" value="1" > <strong style="margin-left:15px">Employee WiFi </strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="enterprise_guest_wifi" value="1" > <strong style="margin-left:15px">Guest WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!--div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                <legend>UPLOAD A BRAND LOGO</legend>
                                                <div class="mui-textfield mui-textfield--float-label blogoimg">
                                                   <input type="text" id="cp_hotel_brand_image_name">
                                                   <input type="file" id="exampleInputFile_cphotel" name="banner_image_cphotel" class="hide"/>
                                                   <label>Logo Image <sup>*</sup></label>
                                                </div>
                                                <span class="cp_span">You can upload JPG. PNG, BMP upto 2mb in size</span>
                                             </div>
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top: 40px">
                                                <p id="but_cphotel" class="mui-btn mui-btn--small mui-btn--primary" style="height:30px; padding: 0px 25px; background-color:#36465f; font-weight:600; line-height: 30px">
                                                   CHOOSE FILE
                                                   <span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span>
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div-->
                                    <div class="form-group" style="margin-top: 15px">
                                       <div class="row">
						
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="row">
						      <label class="radio-inline">
							 <input type="radio" name="is_otpdisabled"  value="0" checked> OTP Enable
						      </label>
						      </div>
						   </div>
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="row">
						      <label class="radio-inline">
							 <input type="radio" name="is_otpdisabled"  value="1"> OTP Disable
						      </label>
						      </div>
						   </div>
						
					     </div>
                                    </div>
                                    
				    <div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
					     <legend>UPLOAD A BRAND LOGO</legend><br/>
					     <div class="dropzone" data-width="900" data-ajax="false" data-originalsize="false" data-height="300" style="width: 300px; height:100px;">
						<input type="file" name="thumb"  />
					     </div>
					  </div>
				       </div>
				    </div>
                                    <div class="form-group" style="display: none" id="institutional_user_type_div">
                                       <div class="col-sm-12 col-xs-12" style="margin-top: 20px;" id="cp_enterprise_user_listing">
                                          <div class="row">
                                             <div class="col-sm-5 col-xs-5">
                                                <div class="row">
                                                   <h4>Add department type </h4>
                                                </div>
                                             </div>
                                             <div class="col-sm-3 col-xs-3">
                                                <p class="mui-btn mui-btn--small mui-btn--danger institutional_add_user_button" style="margin-top:2px">Add</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="cafe_plan_div" style="display: none">
                                       <div class="form-group">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="row">
						<h4 style="text-transform: uppercase; margin-bottom:0px">Select plans to be available</h4>
						<h4 style="margin-top:0px; font-size:18px; font-weight: 300 "><small>(You can select One plans for the location)</small></h4>
					     </div>
					     <div class="row" style="margin-bottom: 15px;">
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                      <div class="row" >
                                                         <label class="radio-inline">
							 <input type="radio" name="cp_hotel_plan_type"  value="2"> Time Plans
						      </label>
                                                      </div>
						      
						   </div>
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                      <div class="row" >
                                                         <label class="radio-inline">
							 <input type="radio" name="cp_hotel_plan_type"  value="1" > Data Plans
						      </label>
                                                      </div>
						      
						   </div>
						
					     </div>
					     
					  </div>
				       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                <div class="mui-select">
                                                   <select name="cp_cafe_plan" id="cp_cafe_plan">
                                                   
                                                   </select>
                                                   <!--label>Select Plan <sup>*</sup></label-->
                                                </div>
                                             </div>
                                             
                                          </div>
                                       </div>
                                    </div>
                                    </div>
                                    <div id="other_location_plan_div" style="display: none">
                                                                           <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <h4 style="text-transform: uppercase; margin-bottom:0px">Select plans to be available</h4>
                                             <h4 style="margin-top:0px; font-size:18px; font-weight: 300 "><small>(You can select multiple plans for the location)</small></h4>
                                          </div>
                                          <div class="row" style="margin-bottom: 15px;">
                                             <label class="radio-inline">
                                             <input type="radio" name="cp_hotel_plan_type"  value="2"> Time Plans
                                             </label>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                <div class="mui-select">
                                                   <select name="cp_hotel_plan" id="cp_hotel_plan">
                                                   </select>
                                                   <!--label>Select Plan <sup>*</sup></label-->
                                                </div>
                                             </div>
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <h5 class="create_headings" onclick="cp_hotel_add_plan()">
                                                   Add Plan
                                                </h5>
                                             </div>
                                          </div>
                                       </div>
                                    </div>

                                    </div>
                                    <div class="col-sm-12 col-xs-12" style="margin-top: 20px;" id="cp_hotel_added_plan_listing">
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px">
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                <p id="add_hotel_captive_portal_error" style="color:red"></p>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value="exit"> SAVE & EXIT</button>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn"  value="save"> NEXT</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                       <div class="slider_div pull-right">
                                          <div class="right_slider">
                                             <h3>CAPTIVE PORTAL SAMPLE</h3>
                                             <div id="crsl_items_hotel_location" class="crsl-items" data-navigation="navbtns_cphotel">
                                                <div class="crsl-wrap">
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_1_img.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_hospital_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_institutional_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_cafe_slider1" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_1.jpg" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo base_url()?>assets/images/radisson_blu_logo.png" class="img-responsive brand_logo_preview_slider" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_2_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_2.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_2.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_2.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_cafe_slider2" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_2.jpg" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo base_url()?>assets/images/radisson_blu_logo.png" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_3_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_3.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_3.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_3.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_cafe_slider3" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_3.jpg" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo base_url()?>assets/images/radisson_blu_logo.png" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_4_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_4.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_4.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_4.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_cafe_slider4" style="display: none">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_4.jpg" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo base_url()?>assets/images/radisson_blu_logo.png" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <nav class="slidernav">
                                                <div id="navbtns_cphotel" class="clearfix">
                                                   <a href="#" class="previous pull-left">
                                                   <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                   </a>
                                                   <span class="previous_span"><strong>Setp 01:</strong> User Enter Mobile No.</span>
                                                   <a href="#" class="next pull-right">
                                                   <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                   </a>
                                                </div>
                                             </nav>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- captive portal tab end --> 
                        <!-- Nas portal tab start -->
                        <div class="mui-tabs__pane" id="nas_setup">
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <h2>NAS Setup</h2>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="col-sm-12 col-xs-12 mui--text-right">
                                    <p><strong><span class="location_name_view"></span> <span class="location_id_view"></span></strong></p>
                                    <p><small style="color:#808080" class="email_view"></small></p>
                                    <p><small style="color:#808080" class="mobile_number_view"></small></p>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-12 col-xs-12">
                                 <form id="configure_nas" onsubmit="configure_nas(); return false;">
                                    <div id="nas_setup_data">
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px">
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                <p id="nas_select_erroe" style="color:red">
                                                <p>
                                                   <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value="microtic" id="microtic_button" style="display: none">Start Configuration</button>
                                                   <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value = "else"  id="else_router" style="display: none">Save & Next</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                        <!-- Nas portal tab END -->
                        <!-- access point tab start -->
                        <div class="mui-tabs__pane" id="access_points">
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <h2>Add Access Point</h2>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="col-sm-12 col-xs-12 mui--text-right">
                                    <p><strong><span class="location_name_view"></span> <span class="location_id_view"></span></strong></p>
                                    <p><small style="color:#808080" class="email_view"></small></p>
                                    <p><small style="color:#808080" class="mobile_number_view"></small></p>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-12 col-xs-12">
                                 <form id="add_location_access_point" onsubmit="add_location_access_point(); return false;">
                                    <div class="form-group">
                                       <div class="row">
                                        
                                        
                                          <div class="col-sm-3 col-xs-3">
                                             <div class="mui-textfield mui-textfield--float-label" >
                                                <input type="text" name="access_point_macid" id="access_point_macid" required>
                                                <label>MAC ID <sup>*</sup></label>
                                             </div>
                                          </div>
                                          <!--div class="col-sm-3 col-xs-3">
                                             <div class="mui-textfield mui-textfield--float-label" >
                                                <input type="text" name="access_point_ip" id="access_point_ip" required>
                                                <label>IP<sup>*</sup></label>
                                             </div>
                                          </div-->
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select id="add_ap_location" name="add_ap_location" required>
                                                      <option value="">Select AP location</option>
                                                   </select>
                                                   <label>AP Location<sup>*</sup></label>
                                                </div>
                                             </div>
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h5 class="create_headings" onclick="add_ap_location_model()">
                                                   Add New AP Location
                                                </h5>
                                             </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                <p style="color:red" id="add_access_point_errro"></p>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">ADD</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>MAC ID</th>
                                                   <th>BRAND</th>
                                                   <!--th>STATUS</th-->
                                                   <!--th>IP</th-->
                                                   <th class="mui--text-right">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody id="access_point_list">
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                             <button type="button" class="mui-btn mui-btn--accent btn-lg save_btn" onclick="close_access_point_tab()">SAVE & NEXT</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- access point tab END -->
                        <!-- voucher  tab start -->
                        <div class="mui-tabs__pane" id="Vouchers">
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <h2>Create New Voucher</h2>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="col-sm-12 col-xs-12 mui--text-right">
                                    <p><strong><span class="location_name_view"></span> <span class="location_id_view"></span></strong></p>
                                    <p><small style="color:#808080" class="email_view"></small></p>
                                    <p><small style="color:#808080" class="mobile_number_view"></small></p>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <div class="row">
                                    <label class="radio-inline">
                                    <input type="radio" name="voucher_radio" value="1" checked> Voucher Use
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="voucher_radio"  value="0" > Not use
                                    </label>
                                 </div>
                                 <div id="voucher_generate_div">
                                    <form id="add_location_vouchers" onsubmit="add_location_vouchers(); return false;">
                                       <div class="row">
                                          <div class="col-sm-5 col-xs-5">
                                             <div class="mui-textfield mui-textfield--float-label">
                                                <input type="text" id="voucher_data" name="voucher_data" required>
                                                <label>Voucher Value <sup>*</sup></label>
                                             </div>
                                          </div>
                                          <div class="col-sm-2 col-xs-2">
                                             <div class="mui-select">
                                                <select id="voucher_data_type" name="voucher_data_type" required>
                                                   <option value="MB">MB</option>
                                                   <option value="GB">GB</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row" style="margin-bottom:15px;">
                                          <div class="col-sm-5 col-xs-5">
                                             <div class="mui-textfield mui-textfield--float-label">
                                                <input type="text" id="voucher_cost" name="voucher_cost" required>
                                                <label>Voucher Cost Price <sup>*</sup></label>
                                             </div>
                                          </div>
                                          <div class="col-sm-5 col-xs-5">
                                             <div class="mui-textfield mui-textfield--float-label">
                                                <input type="text" id="voucher_selling_price" name="voucher_selling_price" required>
                                                <label>Voucher Selling Price <sup>*</sup></label>
                                             </div>
                                          </div>
                                          <div class="col-sm-3 col-xs-3">
                                             <p style="color:red" id="voucher_create_error"></p>
                                             <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">ADD</button>
                                          </div>
                                       </div>
                                    </form>
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-6">
                                          <div class="table-responsive">
                                             <table class="table table-striped">
                                                <thead>
                                                   <tr class="active">
                                                      <th>&nbsp;</th>
                                                      <th>VOUCHER VALUE</th>
                                                      <th>VOUCHER COST</th>
                                                      <th>VOUCHER SELLING PRICE</th>
                                                      <th class="mui--text-right">ACTIONS</th>
                                                   </tr>
                                                </thead>
                                                <tbody id="voucher_list">
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                             <button type="button" onclick="exit_voucher_panel()"  class="mui-btn mui-btn--accent btn-lg save_btn">SAVE & EXIT</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- voucher tab END -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!---modal --->
<!-- modal  for asking user to router username and pass and ip  start-->
<div class="modal modal-container fade" id="add_location_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD NEW DEVICE</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="start_configuration(); return false;">
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">ROUTER IP</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="hidden" id="nas_id_selected" name="nas_id_selected" class="form-control">
                           <input type="text" class="form-control" placeholder="Router IP" name="router_ip" id="router_ip" required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">ROUTER USER NAME</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Router user name" name="router_user" id="router_user" required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">ROUTER PASSWORD</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="password" class="form-control" placeholder="Router password" name="router_password" id="router_password" >
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">PORT</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="number" class="form-control" placeholder="Port (Optional)" name="router_port" id="router_port" >
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           <p id="router_connection_error" style="color:red"> </p>
                           <center><button type="submit" class="btn btn-raised btn-primary btn-lg btn-block">NEXT</button></center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal  for asking user to router username and pass and ip  end-->
<!-- modal  for asking user to router type  start-->
<div class="modal modal-container fade" id="connection_type_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD NEW DEVICE</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="start_configuration1(); return false;">
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Router Type</label>
                        <div class="col-sm-6 col-xs-6">
                           <select class="form-control" name="router_type_wap" id="router_type_wap" required>
                              <option value="">Select Router type</option>
                              <option value="WAP">WAP</option>
                              <option value="WAPAC">WAP AC</option>
                              <option value="Rb850Gx2">Rb850Gx2</option>
                              <option value="Rb941-2nD">Rb941-2nD</option>
                              <option value="2011iL">2011iL-IN / 2011iL-RM</option>
                              <option value="CCR1036-8G-2S">CCR1036-8G-2S+ / CCR1036-8G-2S+-EM</option>
                              <option value="921GS-5HPacD">921GS-5HPacD</option>
                           </select>
                        </div>
                     </div>
                     <!--div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Connection Type</label>
                        <div class="col-sm-6 col-xs-6">
                           <select class="form-control"name="wan_connection_type" id="connection_type" required>
                        <option value="">Select Connection type</option>
                        <option value="DHCP">DHCP</option>
                        <option value="PPPOE">PPPOE</option>
                        <option value="STATIC">STATIC</option>
                           </select>
                        </div>
                        </div-->
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           <p id="router_connection_error1" style="color:red"> </p>
                           <center>
                              <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block">NEXT</button>
                           </center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal  for asking user to router type  end-->
<!-- modal  for asking user to router is reset or not  start-->
<div class="modal modal-container fade" id="is_router_reset_or_not" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-toggle="modal" data-target="#connection_type_modal" data-dismiss="modal" style="margin-left: 25px; float:left; font-size: 18px"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD NEW DEVICE</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                     <label class="controls-labels col-sm-12 col-xs-12">
                     <b>To begin configuring the router please ensure you have reset the router completely.</b>
                     </label>
                  </div>
                  <br /><br /><br />
                  <div class="form-group" style="margin-top: 20px; padding-bottom: 0px">
                     <center>
                        <p id="is_router_reset_or_not_error" style="color:red"> </p>
                     </center>
                     <br />
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <center><button onclick="reset_wap_router()" type="submit" class="btn btn-raised btn-primary btn-lg btn-block">RESET NOW</button></center>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <center><button onclick="check_wap_router_reset_porperly()" type="submit" class="btn btn-raised btn-primary btn-lg btn-block">NEXT</button></center>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal  for asking user to router is reset or not  end-->
<!-- modal  for start configration button  start-->
<div class="modal modal-container fade" id="is_router_start_configure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD NEW DEVICE</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                     <label class="controls-labels col-sm-12 col-xs-12">
                        <center><b>Start Configure your router.</b></center>
                     </label>
                  </div>
                  <br /><br /><br />
                  <div class="form-group" style="margin-top: 20px; padding-bottom: 0px">
                     <center>
                        <p id="is_router_start_configure_error" style="color:red"> </p>
                     </center>
                     <br />
                     <div class="col-lg-12 col-md-6 col-sm-6 col-xs-6">
                        <center><button onclick="start_configuration2()" type="submit" class="btn btn-raised btn-primary btn-lg btn-block">NEXT</button></center>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal  for start configration button  start-->
<div class="modal modal-container fade" id="done_configuration" tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">DONE Configuration</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px"></div>
      </div>
   </div>
</div>
<!-- edit voucer modal -->
<div class="modal modal-container fade"  tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static" id="EditvoucherModal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title">Edit Voucher</h4>
            </center>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="update_location_vouchers" onsubmit="update_location_vouchers(); return false;">
                     <div class="row">
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="hidden" id="voucher_id_edit" name="voucher_id_edit" required>
                              <input type="text" id="voucher_data_edit" name="voucher_data_edit" required>
                              <label>Voucher Value <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                           <div class="mui-select">
                              <select id="voucher_data_type_edit" name="voucher_data_type_edit" required>
                                 <option value="MB">MB</option>
                                 <option id="GB">GB</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="margin-bottom:15px;">
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="text" id="voucher_cost_edit" name="voucher_cost_edit" required>
                              <label>Voucher Cost Price <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="text" id="voucher_selling_price_edit" name="voucher_selling_price_edit" required>
                              <label>Voucher Selling Price <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                           <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">SAVE</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- edit ACCESS point modal -->
<div class="modal modal-container fade"  tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static" id="EditaccesspointModal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title">Edit Access Point</h4>
            </center>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="update_location_vouchers" onsubmit="update_location_access_point(); return false;">
                     <div class="row">
			<input type="hidden" id="access_point_id_edit" name="access_point_id_edit" required>
                      
                        
                        <div class="col-sm-3 col-xs-3">
                           <div class="mui-textfield ">
                              <input type="text" id="access_point_macid_edit" name="access_point_macid_edit" required>
                              <label>MAC ID  <sup>*</sup></label>
                           </div>
                        </div>
                        <!--div class="col-sm-3 col-xs-3">
                           <div class="mui-textfield ">
                              <input type="text" id="access_point_ip_edit" name="access_point_ip_edit" required>
                              <label>IP <sup>*</sup></label>
                           </div>
                        </div-->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select id="edit_ap_location" name="edit_ap_location" required>
                                                      <option value="">Select AP location</option>
                                                   </select>
                                                   <label>AP Location<sup>*</sup></label>
                                                </div>
                                             </div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h5 class="create_headings" onclick="add_ap_location_model()">
                                                   Add New AP Location
                                                </h5>
                                             </div>
                     </div>
                     <div class="row" style="margin-top:25px;">
                        <center>
                           <p style="color: red" id="edit_access_point_errro"></p>
                           <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">SAVE</button>
                        </center>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-container fade" id="add_zone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><strong>ADD ZONE</strong></h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p>Add a new zone to your location listing.</p>
            <div class="mui-textfield mui-textfield--float-label">
               <input type="text" id="zone_name_popup" name="zone_name_popup">
               <label>Zone Name</label>
            </div>
            <div class="row">
               <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                  <div class="mui-select">
                     <select id="city_popup">
                     <?php
                        echo $city_list = $this->location_model->city_list($edit_location_data->state_id,$edit_location_data->city_id);
                            ?>
                     </select>
                     <label>City</label>
                  </div>
               </div>
               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px ">
                  <!--a class="mui-btn mui-btn--small mui-btn--flat" style="color:#29ABE2 ">Add City</a-->
               </div>
            </div>
         </div>
         <div class="modal-footer" style="text-align: right">
            <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d ; color:#fff ">CANCEL</button>
            <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="add_zone()">ADD</button>
         </div>
      </div>
   </div>
</div>

<div class="modal modal-container fade" id="add_ap_location_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><strong>ADD AP LOCATION</strong></h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p>Add a new ap location  to your location listing.</p>
            <div class="mui-textfield mui-textfield--float-label">
               <input type="text" id="ap_location_name_popup" name="ap_location_name_popup">
               <label>AP Location Name</label>
            </div>
           
         </div>
         <div class="modal-footer" style="text-align: right">
	    <p style="color:red" id="add_ap_location_error"></p>
            <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d ; color:#fff ">CANCEL</button>
            <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="add_new_ap_location()">ADD</button>
         </div>
      </div>
   </div>
</div>

<!-- One hop network create start-->
<div class="modal modal-container fade" id="create_onehop_netword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">CREATE NEW NETWORK</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="create_onehop_network(); return false;">
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">ORGANIZATION</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Organization" name="onehop_organization" id="onehop_organization" value="SHOUUT" readonly="readonly" required>
			   <input type="hidden" name="onehop_address" id="onehop_address" >
			   <input type="hidden" name="onehop_latitude" id="onehop_latitude" >
			   <input type="hidden" name="onehop_longitude" id="onehop_longitude" >
			   <input type="hidden" name="onehop_is_new_network" id="onehop_is_new_network" >
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">NETWORK NAME</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Networkd name" name="onehop_network_name" id="onehop_network_name" readonly="readonly" required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">DESCRIPTION</label>
                        <div class="col-sm-6 col-xs-6">
                           <textarea class="form-control" placeholder="Description" name="onehop_description" id="onehop_description" ></textarea>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">MAXAPs</label>
                        <div class="col-sm-6 col-xs-6" style="margin-top: 15px;">
                           <input type="number" class="form-control" placeholder="Max Apx" name="onehop_maxaps" id="onehop_maxaps" readonly="readonly" value = '16'>
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">COA Status</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_coa_status" id="onehop_coa_status">
				 <!--option value=''>Select COA Status</option>
				 <option value='0'>Disable</option-->
				 <option value='1'>Enable</option>
			      </select>
			   </div>
                           
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">COA IP</label>
                        <div class="col-sm-6 col-xs-6" style="margin-top: 15px;">
                           <input type="text" class="form-control" placeholder="COA IP" name="onehop_coa_ip" id="onehop_coa_ip" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">COA SECRET</label>
                        <div class="col-sm-6 col-xs-6" style="margin-top: 15px;">
                           <input type="text" class="form-control" placeholder="Coa Secret" name="onehop_coa_secret" id="onehop_coa_secret" readonly="readonly">
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           
                           <center>
			      <p id="create_onehop_network_status" style="color:red"> </p>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" id="create_network_button">CREATE NETWORK</button>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" id="update_network_button">UPDATE NETWORK</button>
			   </center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- One hop network create end-->

<!-- One hop add ap start-->
<div class="modal modal-container fade" id="add_onehop_ap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD AP MAC</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <!--form id="add_location" onsubmit="add_onehop_ap(); return false;">
                     
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">AP MAC</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="AP MAC" name="onehop_ap_mac" id="onehop_ap_mac"  required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">AP Name</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="AP Name" name="onehop_ap_name" id="onehop_ap_name"  required>
                        </div>
                     </div>
                     
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           
                           <center>
			      <p id="add_onehop_ap_status" style="color:red"> </p>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" >ADD AP MAC</button>
			      
			   </center>
                        </div>
                     </div>
                  </form-->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- One hop add ap end-->

<!-- One hop add ssid start-->
<div class="modal modal-container fade" id="add_onehop_ssid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD SSID</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="add_onehop_ssid(); return false;">
                     
                     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">SSIDIndex</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_ssid_index" id="onehop_ssid_index" required>
				 <!--option value=''>Select Index</option-->
				 <option value='0'>Index 0</option>
				 <!--option value='1'>Index 1</option>
				 <option value='2'>Index 2</option>
				 <option value='3'>Index 3</option>
				 <option value='4'>Index 4</option>
				 <option value='5'>Index 5</option>
				 <option value='6'>Index 6</option>
				 <option value='7'>Index 7</option-->
			      </select>
			   </div>
                           
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">SSID Name</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="SSID Name" name="onehop_ssid_name" id="onehop_ssid_name"  required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">Association</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_association" id="onehop_association">
				 <option value='0'>Open(No Encription)</option>
				 <option value='1'>PSK</option>
			      </select>
			   </div>
                           
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Pre Shared Key</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Pre Shared Key" name="onehop_psk" id="onehop_psk">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">WPA Mode</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_wap_mode" id="onehop_wap_mode">
				 <option value='3'> WPA and WPA2 </option>
				 <option value='1'> WPA Only </option>
				 <option value='2'> WPA2 Only </option>
				 
			      </select>
			   </div>
			</div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">Forwarding</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_forwarding" id="onehop_forwarding">
				 <option value='1'>  NAT Mode  </option>
				 <option value='2'> Bridge Mode  </option>
				 <option value='3'> Tunnel Mode </option>
			      </select>
			   </div>
			</div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">CP Mode</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_cp_mode" id="onehop_cp_mode">
				 <option value='0'> NO Captive Portal   </option>
				 <option value='1'> Internal Captive Portal   </option>
				 <option value='2'>  External Captive Portal  </option>
			      </select>
			   </div>
			</div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">CP URL</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="CP URL" name="onehop_cp_url" id="onehop_cp_url">
                        </div>
                     </div>
		     
		     
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Walled Garden IP List</label>
                        <div class="col-sm-8 col-xs-8">
			   <div class="row">
			      <div class="col-sm-12">
				 <div class="row">
				    <div class="form-group">
				       <div class="col-sm-9 col-xs-9"  style="max-height:150px; overflow-y: auto;" id="wall_garder_ip_div">
					  <div class="input-group" style="margin:10px 0px">
					      <input type="text" class="form-control" placeholder="wall garder ip" name="wall_garder_ip[]">
					      <div class="input-group-btn">
						  <span class="btn btn-danger btn-sm" id="add_more_wallagrder">Add more</span>
					      </div>
					  </div>
					  
				       </div>
				    </div>
				 </div>
			      </div>
			   </div>
                        </div>
                     </div>
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Walled Garden Domain List</label>
                        <div class="col-sm-8 col-xs-8">
			   <div class="row">
			      <div class="col-sm-12">
				 <div class="row">
				    <div class="form-group">
				       <div class="col-sm-9 col-xs-9"  style="max-height:150px; overflow-y: auto;" id="wall_garder_domain_div">
					  <div class="input-group" style="margin:10px 0px">
					      <input type="text" class="form-control" placeholder="wall garder domain" name="wall_garder_domain[]">
					      <div class="input-group-btn">
						  <span class="btn btn-danger btn-sm" id="add_more_wallagrder_domain">Add more</span>
					      </div>
					  </div>
					  
				       </div>
				    </div>
				 </div>
			      </div>
			   </div>
                        </div>
                     </div>
		     
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Auth Server IP</label>
			
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Auth Server IP" name="onehop_auth_server_ip" id="onehop_auth_server_ip" readonly="readonly">
                        </div>
			
                     </div>
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Auth Server Port</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="number" class="form-control" placeholder="Auth Server Port" name="onehop_auth_server_port" id="onehop_auth_server_port" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Auth Server Secret</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Auth Server Secret" name="onehop_auth_server_secret" id="onehop_auth_server_secret" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">Accounting Status</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_accounting" id="onehop_accounting">
				 <!--option value='0'>  Accounting Disabled   </option-->
				 <option value='1'>  Accounting Enabled    </option>
			      </select>
			   </div>
			</div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Accounting Server IP</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Accounting Server IP" name="onehop_acc_server_ip" id="onehop_acc_server_ip" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Accounting Server Port</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="number" class="form-control" placeholder="Accounting Server port" name="onehop_acc_server_port" id="onehop_acc_server_port" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Accounting Server Secret</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Accounting Server Secret" name="onehop_acc_server_secret" id="onehop_acc_server_secret" readonly="readonly">
                        </div>
                     </div>
		     
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Accounting Interval in seconds </label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="number" class="form-control" placeholder="Accounting Interval in seconds " name="onehop_acc_interval" id="onehop_acc_interval" readonly="readonly">
                        </div>
                     </div>
		     
			
		    
		    
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           
                           <center>
			      <p id="add_onehop_ssid_status" style="color:red"> </p>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" >ADD SSID</button>
			      
			   </center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- One hop add ssid end-->


<!-- One hop add delete start-->
<div class="modal modal-container fade" id="delete_onehop_ssid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">DELETE SSID</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="delete_onehop_ssid(); return false;">
                     
                     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">SSIDIndex</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_ssid_index_delete" id="onehop_ssid_index_delete" required>
				 <option value=''>Select Index</option>
				 <option value='0'>Index 0</option>
				 <option value='1'>Index 1</option>
				 <option value='2'>Index 2</option>
				 <option value='3'>Index 3</option>
				 <option value='4'>Index 4</option>
				 <option value='5'>Index 5</option>
				 <option value='6'>Index 6</option>
				 <option value='7'>Index 7</option>
			      </select>
			   </div>
                           
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">SSID Name</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="SSID Name" name="onehop_ssid_name_delete" id="onehop_ssid_name_delete"  required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           
                           <center>
			      <p id="delete_onehop_ssid_status" style="color:red"> </p>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" >DELETE SSID</button>
			      
			   </center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<script>
   //location detail tab script
    
     
     
   
    
       
</script>

<script>
   //captive portal tab jquery
    $( '#but' ).click( function() {
             $( '#exampleInputFile' ).trigger( 'click' );
          } );
    /*$( '#but_cphotel' ).click( function() {
             $( '#exampleInputFile_cphotel' ).trigger( 'click' );
          } );*/
    var _URL = window.URL || window.webkitURL;
     $("#exampleInputFile").change(function(e) {
         var file, img;
         if ((file = this.files[0])) {
   var image_name = this.files[0].name;
   $("#cp1_brand_image_name").val(image_name);
             img = new Image();
             img.onload = function() {
                 var image_width = this.width;
                 var image_height = this.height;
   if (image_width < 600 || image_height < 200) {
     $("#add_captive_portal_error").html("Minimum Image size should be  of width 600px and height 200px");
     $('button').prop('disabled', true);
   }else{
     var  ratio = parseInt(image_width)/parseInt(image_height);
      if(ratio==3){
        $("#add_captive_portal_error").html("");
        $('button').prop('disabled', false);
      }else{
        $("#add_captive_portal_error").html("Ratio of Image to be 3:1");
                      $('button').prop('disabled', true);
      }
   }
   
      };
             img.src = _URL.createObjectURL(file);
         }
     });
     /*$("#exampleInputFile_cphotel").change(function(e) {
         var file, img;
         if ((file = this.files[0])) {
   var image_name = this.files[0].name;
   $("#cp_hotel_brand_image_name").val(image_name);
             img = new Image();
             img.onload = function() {
                 var image_width = this.width;
                 var image_height = this.height;
   if (image_width < 600 || image_height < 200) {
     $("#add_hotel_captive_portal_error").html("Minimum Image size should be  of width 600px and height 200px");
     $('button').prop('disabled', true);
   }else{
     var  ratio = parseInt(image_width)/parseInt(image_height);
      if(ratio==3){
        $("#add_hotel_captive_portal_error").html("");
        $('button').prop('disabled', false);
      }else{
        $("#add_hotel_captive_portal_error").html("Ratio of Image to be 3:1");
                      $('button').prop('disabled', true);
      }
   }
   
      };
             img.src = _URL.createObjectURL(file);
         }
     });*/
     
     //on brand image chane show in slider
     (function() {
         var URL = window.URL || window.webkitURL;
         var input = document.querySelector('#exampleInputFile');
         var preview = document.querySelector('.brand_logo_preview_slider');
         // When the file input changes, create a object URL around the file.
         input.addEventListener('change', function () {
             $('.brand_logo_preview_slider').attr('src',URL.createObjectURL(this.files[0]));
             $("#unset_image_button").show();
         });
   /*var input = document.querySelector('#exampleInputFile_cphotel');
         var preview = document.querySelector('.brand_logo_preview_slider');
         // When the file input changes, create a object URL around the file.
         input.addEventListener('change', function () {
             $('.brand_logo_preview_slider').attr('src',URL.createObjectURL(this.files[0]));
         });*/
   
     })();
     
     function unset_image(){
       var default_image_src = '<?php echo base_url()?>assets/images/default_brand_logo.png';
        $('.brand_logo_preview_slider').attr('src',default_image_src);
        $('button').prop('disabled', false);
         $("#add_captive_portal_error").html("");
   //document.getElementById("exampleInputFile").value = "";
   $("#cp1_brand_image_name").val("");
   $('#exampleInputFile').remove();
   $('.blogoimg').append('<input type="file" id="exampleInputFile" name="banner_image" class="hide"/>');
     }
     
       
      
       function voucher_list(){
   var location_uid =  $("#location_id_hidden").val();
   $(".loading").show();
   $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/location_voucher_list",
        data: "location_uid="+location_uid,
        success: function(data){
   	$(".loading").hide();
   	$("#voucher_list").html(data);
        }
         });
       }
       
       function add_location_vouchers(){
   var location_uid =  $("#location_id_hidden").val();
   var voucher_data =  $("#voucher_data").val();
   var voucher_data_type =  $("#voucher_data_type").val();
   var voucher_cost =  $("#voucher_cost").val();
   var voucher_selling_price =  $("#voucher_selling_price").val();
    $(".loading").show();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_location_vouchers",
        data: "location_uid="+location_uid+"&voucher_data="+voucher_data+"&voucher_data_type="+voucher_data_type+"&voucher_cost="+voucher_cost+"&voucher_selling_price="+voucher_selling_price,
        success: function(data){
   	if(data == '1') {
   	   $("#voucher_data").val('');
   	   $("#voucher_cost").val('');
   	   $("#voucher_selling_price").val('');
   	   voucher_list(location_uid);
   	}else{
   	   $(".loading").hide();
   	   $("#voucher_create_error").html("Can not create more than 4");
   	}
   	
        }
         });
       }
       function delete_voucher(voucher_id) {
   if (confirm("Are you sure you want to delete?")) {
      var location_uid =  $("#location_id_hidden").val();
      $(".loading").show();
       $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/delete_voucher",
        data: "voucher_id="+voucher_id,
        success: function(data){
   	voucher_list(location_uid);
        }
         });
   }
   return false;
       }
       
       function edit_voucher(voucher_id, voucher_data, voucher_data_type, voucher_cost, voucher_selling_price) {
      $("#voucher_id_edit").val(voucher_id);
      $("#voucher_data_edit").val(voucher_data);
      $("#voucher_data_type_edit").val(voucher_data_type);
      $("#voucher_cost_edit").val(voucher_cost);
      $("#voucher_selling_price_edit").val(voucher_selling_price);
      $("#EditvoucherModal").modal("show");
       }
       function update_location_vouchers(){
   var location_uid =  $("#location_id_hidden").val();
    var voucher_id =  $("#voucher_id_edit").val();
   var voucher_data =  $("#voucher_data_edit").val();
   var voucher_data_type =  $("#voucher_data_type_edit").val();
   var voucher_cost =  $("#voucher_cost_edit").val();
   var voucher_selling_price =  $("#voucher_selling_price_edit").val();
    $(".loading").show();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_location_vouchers",
        data: "location_uid="+location_uid+"&voucher_data="+voucher_data+"&voucher_data_type="+voucher_data_type+"&voucher_cost="+voucher_cost+"&voucher_id="+voucher_id+"&voucher_selling_price="+voucher_selling_price,
        success: function(data){
   	$("#EditvoucherModal").modal("hide");
   	voucher_list(location_uid);
        }
         });
       }
       // on cp type change show and hide div
       $("input[name='voucher_radio']").change(function() {
   var voucher_need = $("input[name='voucher_radio']:checked").val();
   if (voucher_need == '1') {
      $("#voucher_generate_div").show();
   }else{
      $("#voucher_generate_div").hide();
   }
   
       });
       
       function exit_voucher_panel(){
   var location_uid =  $("#location_id_hidden").val();
   var voucher_need = $("input[name='voucher_radio']:checked").val();
    $(".loading").show();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/update_location_voucher_status",
        data: "location_uid="+location_uid+"&voucher_need="+voucher_need,
        success: function(data){
   	window.location = "<?php  echo base_url()?>location";
        }
         });
       }
       
   
    
     function add_location_access_point(){
   var location_uid =  $("#location_id_hidden").val();
   var access_point_ssid =  '';
   var access_point_hotspot_type =  '';
   var access_point_macid =  $("#access_point_macid").val();
   var access_point_ip = '0.0.0.0';
   var ap_location_type = $("#add_ap_location").val();
   if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(access_point_ip))
   {
      $(".loading").show();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_location_access_point",
        data: "location_uid="+location_uid+"&access_point_ssid="+access_point_ssid+"&access_point_hotspot_type="+access_point_hotspot_type+"&access_point_macid="+access_point_macid+"&access_point_ip="+access_point_ip+"&ap_location_type="+ap_location_type,
        success: function(data){
	     //if (data == '1') {
             if (data == '') {
	    $("#access_point_ssid").val('');
	    $("#access_point_macid").val('');
	    access_point_list(location_uid);
	    }else{
	       //$("#add_access_point_errro").html("MAC ID already exist");
               $("#add_access_point_errro").html(data);
		  $(".loading").hide();
	    }
        }
         });
   }
   else
   {
      $("#add_access_point_errro").html("please enter valid ip");
   }
       }
       
        function access_point_list(){
	 get_ap_location();
   var location_uid =  $("#location_id_hidden").val();
   $(".loading").show();
   $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/location_access_point_list",
        data: "location_uid="+location_uid,
        success: function(data){
   	$(".loading").hide();
   	$("#access_point_list").html(data);
        }
         });
       }
       function delete_access_point(access_point_id) {
   if (confirm("Are you sure you want to delete?")) {
      var location_uid =  $("#location_id_hidden").val();
      $(".loading").show();
       $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/delete_access_point",
        data: "access_point_id="+access_point_id,
        success: function(data){
   	access_point_list(location_uid);
        }
         });
   }
   return false;
       }
       
        function edit_access_point(ap_location_type,access_point_id, macid, hotspot_type, ssid,ip) {
	 get_ap_location(ap_location_type);
      $("#access_point_id_edit").val(access_point_id);
      //$("#access_point_ssid_edit").val(ssid);
       //$("#access_point_ip_edit").val(ip);
      //$("#voucher_data_type_edit").val(voucher_data_type);
      //$('input[name="access_point_hotspot_type_edit"][value="' + hotspot_type + '"]').prop("checked", true);
      $("#access_point_macid_edit").val(macid);
      $("#EditaccesspointModal").modal("show");
       }
       function update_location_access_point(){
   var location_uid =  $("#location_id_hidden").val();
   var access_point_id =  $("#access_point_id_edit").val();
   
   var access_point_ssid =  $("#access_point_ssid_edit").val();
   var access_point_hotspot_type =  $("input[name='access_point_hotspot_type_edit']:checked").val();
   var access_point_macid =  $("#access_point_macid_edit").val();
   //var access_point_ip =  $("#access_point_ip_edit").val();
   var access_point_ip = '0.0.0.0';
   var ap_location_type = $("#edit_ap_location").val();
   if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(access_point_ip))
   {
         $("#edit_access_point_errro").html("");
         $(".loading").show();
         $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_location_access_point",
        data: "location_uid="+location_uid+"&access_point_ssid="+access_point_ssid+"&access_point_hotspot_type="+access_point_hotspot_type+"&access_point_macid="+access_point_macid+"&access_point_id="+access_point_id+"&access_point_ip="+access_point_ip+"&ap_location_type="+ap_location_type,
        success: function(data){
	    if (data == '') {
	       $("#EditaccesspointModal").modal("hide");
	       access_point_list(location_uid);
	    }else{
	       $("#edit_access_point_errro").html(data);
	       $(".loading").hide();
	    }
        }
         });
   }
   else
   {
      $("#edit_access_point_errro").html("Please enter valid ip");
   }
       }
       function close_access_point_tab(){
   mui.tabs.activate('Vouchers');
   $("#access_point_tab_checked").show();
   var location_uid =  $("#location_id_hidden").val();
   voucher_list(location_uid);
       }
       
       
       
       
       
       function start_configuration1() {
   $("#router_connection_error1").html("");
   var router_type_wap = $("#router_type_wap").val();
   
   //var connection_type = $("#connection_type").val();
   
   $(".loading").show();
   $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/start_configuration1",
        data: "router_type_wap="+router_type_wap,
        success: function(data){
   	$(".loading").hide();
   	if (data == 1) {
   	   $("#is_router_reset_or_not_error").html("");
   	   $("#connection_type_modal").modal("hide");
   	   $("#is_router_reset_or_not").modal("show");
   	}
   	
        }
         });
       }
       
       
       function reset_wap_router() {
   $("#is_router_reset_or_not_error").html("");
   var router_type_wap = $("#router_type_wap").val();
   $(".loading").show();
   $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>location/reset_router",
         data: "router_type="+router_type_wap,
         success: function(data){
     $(".loading").hide();
     $("#is_router_reset_or_not").modal("hide");
     $("#add_location_modal").modal("show");
     
   	
         }
   });
       }
       function check_wap_router_reset_porperly() {
   $("#is_router_reset_or_not_error").html("");
   var router_type_wap = $("#router_type_wap").val();
   $(".loading").show();
   $.ajax({
         type: "POST",
         url: "<?php echo base_url()?>location/check_wap_router_reset_porperly",
         data: "router_type="+router_type_wap,
         success: function(data){
     $(".loading").hide();
     if (data == '1') {
        $("#is_router_reset_or_not").modal("hide");
        $("#is_router_start_configure").modal("show");
     }else if(data == '2'){
        $("#is_router_reset_or_not_error").html("Your router is not properly reset please first reset properly");
     }else if (data == '3') {
        $("#is_router_reset_or_not_error").html("Selected router not matched. Please select correct router type");
     }else if (data == '4') {
        $("#is_router_reset_or_not_error").html("WAN set on wrong port set WAN on ether1");
     }else{
        $("#is_router_reset_or_not_error").html("Some error occure");
     }
     
         }
   });
       }
       function start_configuration2() {
   
   $(".loading").show();
   $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/start_configuration2",
        data: "configure="+"start",
        success: function(data){
   	$(".loading").hide();
   	   $("#is_router_start_configure").modal("hide");
   	   $("#done_configuration").modal("show");
   	   //get setup microtic data
   	   var location_uid =  $("#location_id_hidden").val();
   	   nas_setup_list(location_uid);
   	
   	
        }
         });
       }
       
       // cp_hotel plan type change set plan list
       $("input[name='cp_hotel_plan_type']").change(function() {
      $("#cp_hotel_added_plan_listing").html('');//clear plan listing
      var plan_type = $("input[name='cp_hotel_plan_type']:checked").val();
      var plan_val = 0;
      if (plan_type == '1') {
         plan_val = 4;
      }else if (plan_type == '2') {
         plan_val = 2;
      }
      $(".loading").show();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/plan_list",
        data: "plan_type="+plan_val,
        success: function(data){
   	$(".loading").hide();
   	$("#cp_hotel_plan").html(data);
   	$("#cp_cafe_plan").html(data);
        }
      });
       });
      
       
       // submit hotel cp form
       function add_hotel_captive_portal() {
         var location_type = $("input[name='location_type']:checked").val();
   $("#add_hotel_captive_portal_error").html("");
   if (location_type != '6') {
      var cp_home_selected_plan_id = $("input[name='cp_home_selected_plan_id[]']").map(function(){return $(this).val();}).get();
      if (cp_home_selected_plan_id == '') {
         $("#add_hotel_captive_portal_error").html("Please add atlist one plan");
         return false;
      }
   }
   var cp_cafe_plan_id = '';
    var main_ssid = '';
   var guest_ssid = '';
   var enterprise_user_type = 0;
   var is_otpdisabled = '1';
   if ($('input[name="is_otpdisabled"]').is(':checked')) {
         is_otpdisabled = $("input[name='is_otpdisabled']:checked").val();
         
   }
   var location_type = $("input[name='location_type']:checked").val();
   var institutional_user_list = $("input[name='cp_institution_user_list[]']").map(function(){
      //return $(this).val();
      return $(this).data("id")+"~~~~"+$(this).val();
      }).get();
   if (location_type == '2') {
      location_portal = 'cp_hotel';
      var hotel_guest_wifi = 0;
      var hotel_visitor_wifi = 0;
      if ($('input[name="hotel_guest_wifi_ssid"]').val() == '') {
	 $("#add_hotel_captive_portal_error").html("Please enter SSID");
	 return false;
      }else{
	 main_ssid = $('input[name="hotel_guest_wifi_ssid"]').val();
      }
      if ($('input[name="hotel_guest_wifi"]').is(':checked')) {
         hotel_guest_wifi = $('input[name="hotel_guest_wifi"]').val();
         
      }
      if ($('input[name="hotel_visitor_wifi"]').is(':checked')) {
         hotel_visitor_wifi = $('input[name="hotel_visitor_wifi"]').val();
         /*if ($('input[name="hotel_visitor_wifi_ssid"]').val() == '') {
	    $("#add_hotel_captive_portal_error").html("Please enter SSID");
	    return false;
         }else{
	    guest_ssid = $('input[name="hotel_visitor_wifi_ssid"]').val();
         }*/
      }
      if (hotel_guest_wifi == '0' && hotel_visitor_wifi == '0') {
         $("#add_hotel_captive_portal_error").html("Please select atlist one wifi type");
         return false;
      }else{
         if (hotel_guest_wifi == '1' && hotel_visitor_wifi == '1') {
	    enterprise_user_type = '3';
         }else if (hotel_guest_wifi) {
	    enterprise_user_type = '1';
         }else{
	    enterprise_user_type = '2';
         }
      }
   }else if (location_type == '3') {
      location_portal = 'cp_hospital';
      var hospital_patient_wifi = 0;
      var hospital_guest_wifi = 0;
      if ($('input[name="hospital_patient_wifi_ssid"]').val() == '') {
	 $("#add_hotel_captive_portal_error").html("Please enter SSID");
	 return false;
      }else{
	 main_ssid = $('input[name="hospital_patient_wifi_ssid"]').val();
      }
      if ($('input[name="hospital_patient_wifi"]').is(':checked')) {
         hospital_patient_wifi = $('input[name="hospital_patient_wifi"]').val();
      }
      if ($('input[name="hospital_guest_wifi"]').is(':checked')) {
         hospital_guest_wifi = $('input[name="hospital_guest_wifi"]').val();
         /*if ($('input[name="hospital_guest_wifi_ssid"]').val() == '') {
	    $("#add_hotel_captive_portal_error").html("Please enter SSID");
	    return false;
         }else{
	    guest_ssid = $('input[name="hospital_guest_wifi_ssid"]').val();
         }*/
      }
      if (hospital_guest_wifi == '0' && hospital_patient_wifi == '0') {
         $("#add_hotel_captive_portal_error").html("Please select atlist one wifi type");
         return false;
      }else{
         if (hospital_guest_wifi == '1' && hospital_patient_wifi == '1') {
	    enterprise_user_type = '3'
         }else if (hospital_patient_wifi) {
	    enterprise_user_type = '1'
         }else{
	    enterprise_user_type = '2'
         }
      }
   }else if (location_type == '4') {
      location_portal = 'cp_institutional';
      if ($('input[name="institutional_wifi_ssid"]').val() == '') {
	 $("#add_hotel_captive_portal_error").html("Please enter SSID");
	 return false;
      }else{
	 main_ssid = $('input[name="institutional_wifi_ssid"]').val();
      }
      if (institutional_user_list == '') {
	    $("#add_hotel_captive_portal_error").html("Please Add atlist one department type");
	    return false;
      }
   }else if (location_type == '5') {
      location_portal = 'cp_enterprise';
      var enterprise_employee_wifi = 0;
      var enterprise_guest_wifi = 0;
      if ($('input[name="enterprise_employee_wifi_ssid"]').val() == '') {
	 $("#add_hotel_captive_portal_error").html("Please enter SSID");
	 return false;
      }else{
	 main_ssid = $('input[name="enterprise_employee_wifi_ssid"]').val();
      }
      if ($('input[name="enterprise_employee_wifi"]').is(':checked')) {
         enterprise_employee_wifi = $('input[name="enterprise_employee_wifi"]').val();
         
      }
      if ($('input[name="enterprise_guest_wifi"]').is(':checked')) {
         enterprise_guest_wifi = $('input[name="enterprise_guest_wifi"]').val();
         /*if ($('input[name="enterprise_guest_wifi_ssid"]').val() == '') {
	    $("#add_hotel_captive_portal_error").html("Please enter SSID");
	    return false;
	 }else{
	    guest_ssid = $('input[name="enterprise_guest_wifi_ssid"]').val();
         }*/
      }
      if (enterprise_employee_wifi == '0' && enterprise_guest_wifi == '0') {
         $("#add_hotel_captive_portal_error").html("Please select atlist one wifi type");
         return false;
      }else{
         if (enterprise_employee_wifi == '1' && enterprise_guest_wifi == '1') {
	    enterprise_user_type = '3'
         }else if (enterprise_employee_wifi) {
	    enterprise_user_type = '1'
         }else{
	    enterprise_user_type = '2'
         }
      }
      if (institutional_user_list == '') {
	    $("#add_hotel_captive_portal_error").html("Please Add atlist one department type");
	    return false;
      }
   }
   else if (location_type == '6') {
      location_portal = 'cp_cafe';
      if ($('input[name="caffe_wifi_ssid"]').val() == '') {
	 $("#add_hotel_captive_portal_error").html("Please enter SSID");
	    return false;
      }else{
	 main_ssid = $('input[name="caffe_wifi_ssid"]').val();
      }
      if ($('#cp_cafe_plan').val() == '') {
	 $("#add_hotel_captive_portal_error").html("Please Select plan");
	    return false;
      }else{
	 cp_cafe_plan_id = $('#cp_cafe_plan').val();
      }
      
      
   }
   var image_name = '';
   var image_src = '';
   if( $('.ghost').length ){ // .hasClass() returns BOOLEAN true/false
      image_name = $(".ghost").attr('name');
      image_src = $('input[name="thumb_values"]').val();
   }
   $("#add_hotel_captive_portal_error").html("");
   var plan_type = $("input[name='cp_hotel_plan_type']:checked").val();
   //var file_data = $("#exampleInputFile_cphotel").prop("files")[0];
   var form_data = new FormData();
   //form_data.append("file", file_data);
   form_data.append('image_name',image_name);
   form_data.append('image_src',image_src);
   form_data.append("location_uid", $("#location_id_hidden").val());
   form_data.append("locationid", $("#created_location_id").val());
   form_data.append("cp_type", location_portal);
   form_data.append("cp_home_selected_plan_id", cp_home_selected_plan_id);
   form_data.append("cp_hotel_plan_type", plan_type);
   form_data.append("cp_enterprise_user_type", enterprise_user_type);
   form_data.append("institutional_user_list", institutional_user_list);
   form_data.append("main_ssid", main_ssid);
   form_data.append("guest_ssid", guest_ssid);
   form_data.append("cp_cafe_plan_id", cp_cafe_plan_id);
   form_data.append("is_otpdisabled", is_otpdisabled);
   $(".loading").show();
   $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/add_hotel_captive_portal",
         cache: false,
        contentType: false,
        processData: false,
        data: form_data,   
        success: function(data){
   	$(".loading").hide();
   	if (Clicked_ButtonValue == 'exit') {
   	    window.location = "<?php  echo base_url()?>location";
   	}else{
   	   // go to next tab
   	    $("#captive_portal_tab_checked").show();
   	   var location_uid =  $("#location_id_hidden").val();
   	   nas_setup_list(location_uid);
   	   mui.tabs.activate('nas_setup');			   
   	}
   	
        }
         });
       }
       
       
       $("input[name='cp_enterprise_user_type']").change(function() {
      var type_val = $(this).val();
      if (type_val == '2') {
         $("#institutional_user_type_div").hide();
      }else{
         $("#institutional_user_type_div").show();
      }
       });
       
       function get_ap_location(ap_location_type = '') {
	    // get ap location list
	 var city_id = $("#city").val();
	  $.ajax({
		 type: "POST",
		 url: "<?php echo base_url()?>location/ap_location_list",
		 data: "ap_location_type="+ap_location_type,
		 success: function(data){
		 $(".loading").hide();
		  $("#add_ap_location").html(data);
		  $("#edit_ap_location").html(data);
		 }
	 });
       }
       function add_ap_location_model(){
	 $("#add_ap_location_modal").modal("show");
       }
       function add_new_ap_location(){
	 $("#add_ap_location_error").html("");
	 var ap_location_name = $("#ap_location_name_popup").val();
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_ap_location",
	    data: "ap_location_name="+ap_location_name,
	    success: function(data){
	       $(".loading").hide();
	       if (data == '1') {
		  get_ap_location();
		  $("#add_ap_location_modal").modal("hide");
	       }else{
		  $("#add_ap_location_error").html("AP Location Name already exist");
	       }
	       
	    }
         });
       }
</script>

<script>
   function create_onehop_network_view(){
      var location_uid = $("#location_id_hidden").val();
      $("#create_onehop_network_status").html('');
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_get_network_detail",
   	    data: "location_uid="+location_uid,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	       $("#onehop_network_name").val(data.network_name);
	       $("#onehop_description").val(data.description);
	       $("#onehop_maxaps").val(data.maxaps);
	       $("#onehop_coa_status").val(data.coa_status);
	       $("#onehop_coa_ip").val(data.coa_ip);
	       $("#onehop_coa_secret").val(data.coa_secret);
	       $("#onehop_address").val(data.address);
	       $("#onehop_latitude").val(data.latitude);
	       $("#onehop_longitude").val(data.longitude);
	       $("#onehop_is_new_network").val(data.is_created);
	       if (data.is_created == '1') {//modify
		  $('#create_network_button').hide();
		  $("#update_network_button").show();
	       }else{
		  $('#create_network_button').show();
		  $("#update_network_button").hide();
	       }
	       $("#create_onehop_netword").modal("show");
   	    }
      });
	 
   }
   function create_onehop_network(){
      var location_uid = $("#location_id_hidden").val();
      var network_name = $("#onehop_network_name").val();
      var description = $("#onehop_description").val();
      var maxaps = $("#onehop_maxaps").val();
      var coa_status = $("#onehop_coa_status").val();
      var coa_ip = $("#onehop_coa_ip").val();
      var coa_secret = $("#onehop_coa_secret").val();
      var address = $("#onehop_address").val();
      var latitude = $("#onehop_latitude").val();
      var longitude = $("#onehop_longitude").val();
      var is_created = $("#onehop_is_new_network").val();
      var service_name = 'NetworkCreate';
      if (is_created == '1') {
	 service_name = 'NetworkModify';
      }
      if (coa_status == '1') {
	 if (coa_ip == '' || coa_secret == '') {
	    $("#create_onehop_network_status").html("coa IP and coa secret can not be blank");
	    return false;
	 }
      }
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_get_network_create",
   	    data: "location_uid="+location_uid+"&network_name="+network_name+"&description="+description+"&maxaps="+maxaps+"&coa_status="+coa_status+"&coa_ip="+coa_ip+"&coa_secret="+coa_secret+"&address="+address+"&latitude="+latitude+"&longitude="+longitude+"&service_name="+service_name,
	    dataType: 'json',
   	    success: function(data){
	       if (data.code == '1') {
		  $("#create_onehop_network_status").html('');
		  var location_uid = $("#location_id_hidden").val();
		  $("#create_onehop_netword").modal("hide");
		  nas_setup_list(location_uid);
	       }else{
		  $(".loading").hide();
		   $("#create_onehop_network_status").html(data.msg);
	       }
   	    }
      });
   }

   function add_onehop_ap_view() {
      $("#add_onehop_ap").modal("show");
   }
   function add_onehop_ap(){
      var location_uid = $("#location_id_hidden").val();
      var onehop_ap_mac = $("#onehop_ap_mac").val();
      var onehop_ap_name = $("#onehop_ap_name").val();
      if (onehop_ap_mac == '' || onehop_ap_name == '') {
	 $("#add_onehop_ap_status").html("Please fill all the fields");
	 return false;
      }
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_add_apmac",
   	    data: "location_uid="+location_uid+"&onehop_ap_mac="+onehop_ap_mac+"&onehop_ap_name="+onehop_ap_name,
	    dataType: 'json',
   	    success: function(data){
	       if (data.code == '1') {
		  $("#add_onehop_ssid_status").html('');
		  $("#add_onehop_ap_status").html("");
		  $("#add_onehop_ssid").modal("hide");
		  nas_setup_list(location_uid);
	       }else{
		  $(".loading").hide();
		 $("#add_onehop_ap_status").html(data.msg);
	       }
   	    }
      });
   }
   function add_onehop_ssid_view() {
      onehop_get_ssid_detail(0);
      $("#add_onehop_ssid").modal("show");
   }
   function validate_ip(ip) {
      if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)){
	 return 1;
      }else{
	 return 0;
      }
   }
   function add_onehop_ssid(){
      var location_uid = $("#location_id_hidden").val();
      var onehop_ssid_index = $("#onehop_ssid_index").val();
      var onehop_ssid_name = $("#onehop_ssid_name").val();
      var onehop_association = $("#onehop_association").val();
      var onehop_wap_mode = $("#onehop_wap_mode").val();
      var onehop_forwarding = $("#onehop_forwarding").val();
      var onehop_cp_mode = $("#onehop_cp_mode").val();
      var onehop_cp_url = $("#onehop_cp_url").val();
      var onehop_auth_server_ip = $("#onehop_auth_server_ip").val();
      var onehop_auth_server_port = $("#onehop_auth_server_port").val();
      var onehop_auth_server_secret = $("#onehop_auth_server_secret").val();
      var onehop_accounting = $("#onehop_accounting").val();
      var onehop_acc_server_ip = $("#onehop_acc_server_ip").val();
      var onehop_acc_server_port = $("#onehop_acc_server_port").val();
      var onehop_acc_server_secret = $("#onehop_acc_server_secret").val();
      var onehop_acc_interval = $("#onehop_acc_interval").val();
      var onehop_psk = $("#onehop_psk").val();
      var onehop_wallgarden_ip_list = $("input[name='wall_garder_ip[]']").map(function(){
	 return $(this).val();
      }).get();
     
      if (onehop_cp_mode == '2') {
	 if (onehop_cp_url == '') {
	    $("#add_onehop_ssid_status").html("Please enter CP url");
	    return false;
	 }else if(onehop_wallgarden_ip_list == ''){
	    $("#add_onehop_ssid_status").html("Please enter wall garder ip");
	    return false;
	 }else if(onehop_auth_server_ip == '') {
	    $("#add_onehop_ssid_status").html("Please enter auth server ip");
	    return false;
	 }else if (onehop_auth_server_secret == '') {
	    $("#add_onehop_ssid_status").html("Please enter auth server secret");
	    return false;
	 }
      }
      if (onehop_accounting == '1') {
	 if (onehop_acc_server_ip == '') {
	    $("#add_onehop_ssid_status").html("Please enter accounting server IP");
	    return false;
	 }
	 else if (onehop_acc_server_secret == '') {
	    $("#add_onehop_ssid_status").html("Please enter accounting server secret");
	    return false;
	 }
      }
      if (onehop_association == '1') {
	 if (onehop_psk == '') {
	     $("#add_onehop_ssid_status").html("Please enter Pre Shared Key");
	    return false;
	 }else if (onehop_psk.length < 8 || onehop_psk.length > 64) {
	    $("#add_onehop_ssid_status").html("Pre Shared Key should be between 8 to 32 character");
	    return false;
	 }
	 
      }
      //validation
      if (onehop_ssid_name.length > 32) {
	 $("#add_onehop_ssid_status").html("SSID Name should be < 32 character");
	 return false;
      }
      if (onehop_auth_server_ip != '') {
	 if(validate_ip(onehop_auth_server_ip) == '0'){
	    $("#add_onehop_ssid_status").html("Please enter valid Auth Server IP");
	    return false;
	 }
      }
      if (onehop_auth_server_secret.length > 32) {
	 $("#add_onehop_ssid_status").html("Auth Server Secret should be < 32 character");
	 return false;
      }
      if (onehop_acc_server_ip != '') {
	 if(validate_ip(onehop_acc_server_ip) == '0'){
	    $("#add_onehop_ssid_status").html("Please enter valid Accounting Server IP");
	    return false;
	 }
      }
      if (onehop_acc_server_secret.length > 32) {
	 $("#add_onehop_ssid_status").html("Accounting Server Secret should be < 32 character");
	 return false;
      }
      
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_add_ssid",
   	    data: "location_uid="+location_uid+"&onehop_ssid_index="+onehop_ssid_index+"&onehop_ssid_name="+onehop_ssid_name+"&onehop_association="+onehop_association+"&onehop_wap_mode="+onehop_wap_mode+"&onehop_forwarding="+onehop_forwarding+"&onehop_cp_mode="+onehop_cp_mode+"&onehop_cp_url="+onehop_cp_url+"&onehop_auth_server_ip="+onehop_auth_server_ip+"&onehop_auth_server_port="+onehop_auth_server_port+"&onehop_auth_server_secret="+onehop_auth_server_secret+"&onehop_accounting="+onehop_accounting+"&onehop_acc_server_ip="+onehop_acc_server_ip+"&onehop_acc_server_port="+onehop_acc_server_port+"&onehop_acc_server_secret="+onehop_acc_server_secret+"&onehop_acc_interval="+onehop_acc_interval+"&onehop_psk="+onehop_psk+"&onehop_wallgarden_ip_list="+onehop_wallgarden_ip_list,
	    dataType: 'json',
   	    success: function(data){
	       if (data.code == '1') {
		  $("#add_onehop_ssid_status").html('');
		  var location_uid = $("#location_id_hidden").val();
		  $("#add_onehop_ssid").modal("hide");
		  nas_setup_list(location_uid);
	       }else{
		  $(".loading").hide();
		  $("#add_onehop_ssid_status").html(data.msg);
	       }
   	    }
      });
   }
   //$("#onehop_ssid_index").on("change",function(){
        function onehop_get_ssid_detail(index_number){ 
      //var index_number = $(this).val();
      var location_uid = $("#location_id_hidden").val();
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_get_ssid_detail",
   	    data: "location_uid="+location_uid+"&index_number="+index_number,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	       if (data.resultCode == '1') {
		  $("#onehop_ssid_name").val(data.ssid_name);
		  $("#onehop_association").val(data.association);
		  $("#onehop_wap_mode").val(data.wap_mode);
		  $("#onehop_forwarding").val(data.forwarding);
		  $("#onehop_cp_mode").val(data.cp_mode);
		  $("#onehop_cp_url").val(data.cp_url);
		  $("#onehop_auth_server_ip").val(data.auth_server_ip);
		  $("#onehop_auth_server_port").val(data.auth_server_port);
		  $("#onehop_auth_server_secret").val(data.auth_server_secret);
		  $("#onehop_accounting").val(data.accounting);
		  $("#onehop_acc_server_ip").val(data.acc_server_ip);
		  $("#onehop_acc_server_port").val(data.acc_server_port);
		  $("#onehop_acc_server_secret").val(data.acc_server_secret);
		  $("#onehop_acc_interval").val(data.acc_interval);
		  $("#onehop_psk").val(data.psk);
		  //alert(data.wall_list.length);
		  $('.remove_wall_column').parent().parent().remove();
		  for (var i = 0; i < data.wall_list.length; i++ ) {
		     var ip = data.wall_list[i]['ip'];
		     $("#wall_garder_ip_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder ip" name = "wall_garder_ip[]" value = "'+ip+'">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column">Remove</span>'+
				      '</div></div>');
		  }
		  $('.remove_wall_column_domain').parent().parent().remove();
		  for (var i = 0; i < data.wall_domain_list.length; i++ ) {
		     var domain = data.wall_domain_list[i]['domain'];
		     $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "'+domain+'">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  }
	       }else{
		  $("#onehop_ssid_name").val(data.ssid_name);
		  $("#onehop_association").val('0');
		  $("#onehop_psk").val("");
		  $("#onehop_wap_mode").val('3');
		  $("#onehop_forwarding").val('1');
		  $("#onehop_cp_mode").val('2');
		  $("#onehop_cp_url").val("");
		  $("#onehop_auth_server_ip").val("103.20.214.108");
		  $("#onehop_auth_server_port").val("1812");
		  $("#onehop_auth_server_secret").val("testing123");
		  $("#onehop_accounting").val('1');
		  $("#onehop_acc_server_ip").val("103.20.214.108");
		  $("#onehop_acc_server_port").val('1813');
		  $("#onehop_acc_server_secret").val('testing123');
		  $("#onehop_acc_interval").val('60');
		  $("#onehop_psk").val(data.psk);
		   $('.remove_wall_column').parent().parent().remove();
		   $('.remove_wall_column_domain').parent().parent().remove();
		   
		  
		  $("#wall_garder_ip_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder ip" name = "wall_garder_ip[]" value = "103.20.213.149">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_ip_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder ip" name = "wall_garder_ip[]" value = "103.20.214.108">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column">Remove</span>'+
				      '</div></div>');
		  
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "cdn101.shouut.com">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "s3-ap-southeast-1.amazonaws.com">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "secure4.arcot.com">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "3dsecure.payseal.com">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "static2.paytm.in">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "static3.paytm.in">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "static1.paytm.in">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "static4.paytm.in">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "secure.paytm.in">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "accounts-uat.paytm.com">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]" value = "pguat.paytm.com">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
		  
	       }
	       onehop_cp_mode(2);
   	    }
      });
   }

   //});
   //$("#onehop_cp_mode").on("change",function(){
   function onehop_cp_mode(cp_mode_type) {
   
      var created_location_id = $("#created_location_id").val();
      //var cp_mode_type = $(this).val();
      
      if (cp_mode_type == '2') {
	 //$(".loading").show();
	 $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/get_isp_url",
   	    data: "created_location_id="+created_location_id,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	       $("#onehop_cp_url").val(data.url+"/login.php");
   	    }
	 });
      }else{
	 $("#onehop_cp_url").val("");
      }
      
   //});
   }
   
   $("#add_more_wallagrder").on("click",function(){
      $("#wall_garder_ip_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder ip" name = "wall_garder_ip[]">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column">Remove</span>'+
				      '</div></div>');
   });
   $(document).on('click', ".remove_wall_column", function(){
      $(this).parent().parent().remove();
   });
   $("#add_more_wallagrder_domain").on("click",function(){
      $("#wall_garder_domain_div").append('<div class="input-group col-sm-12" style="margin:10px 0px">'+
				      '<input type="text" class="form-control" placeholder="wall garder domain" name = "wall_garder_domain[]">'+
				      '<div class="input-group-btn">'+
				      '<span class="btn btn-danger btn-sm remove_wall_column_domain">Remove</span>'+
				      '</div></div>');
   });
   $(document).on('click', ".remove_wall_column_domain", function(){
      $(this).parent().parent().remove();
   });
   function delete_onehop_ssid_view() {
      $("#delete_onehop_ssid").modal("show");
   }
   $("#onehop_ssid_index_delete").on("change",function(){
      
      var index_number = $(this).val();
      var location_uid = $("#location_id_hidden").val();
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_get_ssid_detail",
   	    data: "location_uid="+location_uid+"&index_number="+index_number,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	       if (data.resultCode == '1') {
		  //alert(data.ssid_name);
		  $("#onehop_ssid_name_delete").val(data.ssid_name);
	       
	       }
   	    }
      });
   });
   function delete_onehop_ssid() {
      var location_uid = $("#location_id_hidden").val();
      var onehop_ssid_index = $("#onehop_ssid_index_delete").val();
      var onehop_ssid_name = $("#onehop_ssid_name_delete").val();
       $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_delete_ssid",
   	    data: "location_uid="+location_uid+"&onehop_ssid_index="+onehop_ssid_index+"&onehop_ssid_name="+onehop_ssid_name,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	      
		  $("#delete_onehop_ssid_status").html(data.msg);
	       
	       
   	    }
      });
   }
</script>

</body>
</html>
