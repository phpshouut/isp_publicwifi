 <?php
 $this->load->view('includes/header');
 
 ?>
 
 
     <style>
      .grey-heading{
color:#424143!important;    
cursor: pointer;    
}

.grey-heading:hover{
    color:#424143!important;    
    cursor: pointer;    
    text-decoration: underline;
}

.grey-heading-active{
    color:#424143!important;    
    cursor: pointer;    
    text-decoration: underline!important;
}


.on-green{
    color: #388E3C!important;
    cursor: pointer;
}

.on-green:hover{
    color: #388E3C!important;
    cursor: pointer;
    text-decoration: underline;
}

.on-green-active{
    color: #388E3C!important;
    cursor: pointer;    
    text-decoration: underline!important;
}
.off-red{
    color: #C62828!important;
    cursor: pointer;
}
.off-red:hover{
    color: #C62828!important;
    cursor: pointer;
    text-decoration: underline;
}
.off-red-active{
    color: #C62828!important;
    cursor: pointer;
    text-decoration: underline;
}
      #loadMore {
   padding: 5px 15px;
   text-align: center;
   color: #231f20;
    border-radius:25px; 
    margin: 15px auto;
    border: 1px solid #231f20;
    font-size: 12px;
    font-weight: 600;
    cursor: pointer;
}
#loadMore:hover {
   background-color: #231f20;
   color: #fff;
    text-decoration: none;
}
   
   .table_loader{
      width:25px;
      height:25px;
      float: left;
      margin: 0px 5px;
   }
   .paginate_button{
            width: auto!important;
            float: left;
            margin:0px!important;
            padding: 0px!important;
            color: #fff!important;
            cursor: pointer;
            font-size: 12px!important;
            font-family: 'Open Sans',sans-serif;
            font-weight: 500;
            background-color: #FFF!important;}
   .tbodys{
      height: 200px;
      overflow: auto;
   }
	    
 </style>
   <div class="loading" >
      <img src="<?php echo base_url()?>assets/images/loader.svg"/>
   </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
              <div id="sidedrawer" class="mui--no-user-select">
                  
                  <?php
		  $data['navperm']=$this->plan_model->leftnav_permission();
		  $this->load->view('includes/left_nav');?>
                  
               </div>
            <header id="header">
               <nav class="navbar navbar-default">
		  <div class="container-fluid">
		     <div class="navbar-header">
			<p class="navbar-brand">Locations</p>
                     </div>
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			   <li class="dropdown mui-btn--small mui-btn--accent" style="border-radius: 2px; margin-top: 10px;margin-right:15px; padding:0px 5px">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" 
								style="background-color:none;color: #fff; font-weight: 400; padding:2px 10px;"><i class="fa fa-plus-circle" aria-hidden="true"></i>
								ADD LOCATION

								<span class="caret"></span></a>
								<ul class="dropdown-menu" style="max-height:350px; overflow-y: auto">
									<li><a href="#">ADD NEW LOCATION</a></li>
									<li role="separator" class="divider"></li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/public">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Public Spaces
								  </a>
								  </li>
								 
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/hotel">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Hotels & Hospitality
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/hospital">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Hospitals & Healthcare
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/institutional">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Institutions & Colleges
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/enterprise">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Enterprise
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/cafe">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Cafe & Restaurants
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/retail">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Retail
								  </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/custom">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Custom Location
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/purple">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Purple Box
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/offline">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Offline
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/visitor">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Visitor Management
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/contestifi">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Contestifi
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/retail_audit">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Retail Audit
								     </a>
								  </li>
								    <li>
								     <a href="<?php echo base_url()?>location/add_location_view/event_module">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Event Module
								     </a>
								  </li>
								    <li>
								     <a href="<?php echo base_url()?>location/add_location_view/ordering">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Ordering
								     </a>
								  </li>
								    <!--<li>
								     <a href="<?php echo base_url()?>location/add_location_view/lms">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> LMS
								     </a>
								  </li>-->
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/internact">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Internal Activation
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/eventguest">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Event Guest Management
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/club">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Club
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/truck_stop">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Truck Stop
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/foodle">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Foodle
								     </a>
								  </li>
								</ul>
							  </li>
			   <?php $this->load->view('includes/global_setting',$data);?>
                        </ul>
                     </div>
                  </div>
               </nav>
            </header>

               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <ul>
                           </ul>
                        </div>
			<form method="post" action="<?php echo base_url("location/location_list/".$filtertype); ?>" id="location_filter_form">
			<input type="hidden" name="location_type_filter" id="location_type_filter" value="<?php echo $filtertype?>">
			<input type="hidden" name="online_filter" id="online_filter" value="<?php echo $offline_offline_filter?>">
			<?php
			$all_location_online_class = 'grey-heading';
			$online_class = 'on-green';
			$offline_class = 'off-red';
			
			
			
			
			if($offline_offline_filter == '1'){
			   $online_class = 'on-green-active';
			}
			elseif($offline_offline_filter == '0'){
			   $offline_class = 'off-red-active';
			}else{
			   $all_location_online_class = 'grey-heading-active';
			}
			?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="row">
			      <div class="col-sm-8 col-xs-8">
				 <?php
				    $total_location = $locations->active+$locations->inactive;
				   
				      echo '<h1>
                                           <span data-val_status= "" class="online_status_check_click '.$all_location_online_class.'">
                                           <span>'.$total_location.'</span> <small> Locations</small> 
                                           </span>
                                           <small>|</small>
                                          <span data-val_status= "1" class="online_status_check_click '.$online_class.'"> 
                                          <span>'.$locations->active.'</span>
                                          <small class="on-green">Online</small>
                                          </span><small>|</small>
                                           <span data-val_status= "0" class="online_status_check_click '.$offline_class.'">
                                           <span>'.$locations->inactive.'</span>
                                           <small class="off-red">Offline</small>
                                           </span>
                                       </h1>';
				    
				   
				 ?>
			      </div>
			      
			      <div class="col-sm-4 col-xs-4 pull-right">
				 <div class="form-group">
				    <div class="input-group">
				       <div class="input-group-addon" >
					     <i class="fa fa-search" aria-hidden="true" onclick="submit_filter_form()" style="cursor: pointer"></i>
					  
				       </div>
				       <input class="form-control" placeholder="Search Location" onblur="this.placeholder='Search Location'" onfocus="this.placeholder=''" id="search_type_filter" name="search_type_filter" type="text" value="<?php if(isset($_POST['search_type_filter'])){echo $_POST['search_type_filter'];}?>">

                                    </div>
				    <label class="search_label">You can look up by location name, ID, contact name, number</label>
				 </div>
			      </div>
			   </div>
			</div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
			      <div class="row">
				 
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="state_id_filter" id="state_id_filter" >
					  <option value="">All States</option>
					     <?php
						$state_list = $this->location_model->state_list_for_filter();
						if($state_list->resultCode == 1){
						   foreach($state_list->state as $state_list_view){
						      $sel = '';
						      if($state_list_view->state_id == $_POST['state_id_filter']){
							  $sel = 'selected="selected"';
						      }
						      echo '<option value = "'.$state_list_view->state_id.'" '.$sel.'>'.$state_list_view->state_name.'</option>';
						   }
						}
					     ?>
				       </select>
				       <label>State</label>
				    </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="city_id_filter" id="city_id_filter" >
					  <?php
					  if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
					     $state_id = $_POST['state_id_filter'];
					     $city_id = '';
					     if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
						$city_id = $_POST['city_id_filter'];
					     }
					     $city_list = $this->location_model->city_list_for_filter($state_id, $city_id);
					     echo $city_list;
					  }else{
					     echo '<option value="">All Cities</option>';
					  }
					  ?>
					  
				       </select>
				       <label>City</label>
				    </div>
				 </div>
				 
				 
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       
				       <a onclick="export_to_excel()" class="mui-btn mui-btn--small mui-btn--accent pull-right"><i class="fa fa-download fa-lg" aria-hidden="true"></i> excel</a>
				    </div>
				 </div>
				 
			      </div>
			   </div>
			   
			   
			   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			      <div class="row pull-right">
				 <div class="mui-select">
				    <a onclick="create_group_open_popup()" class="mui-btn mui-btn--small mui-btn--accent pull-right"><small>Create Group</small></a>
				 </div>
			      </div>
			   </div>
			   
			</div>
			</form>
                             <div class="col-lg-12 col-md-12 col-sm-12">
                              <div class="table-responsive">
                                 <table id="example" class="table table-striped table-bordered" >
                                    <thead>
                                       <tr class="active">
                                          <th>&nbsp;</th>
					  <th><input type="checkbox" value="1" id="check_all_location" name="location_group_check_all">&nbsp; All </th>
                                          <!--th>
                                             <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
                                                <label>
                                                <input type="checkbox"> 
                                                </label>
					  </div>
                                          </th-->
                                          <th>LOC ID</th>
                                          <th>LOCATION NAME</th>
                                          <th>STATE</th>
                                          <th>CITY</th>
                                          <th>CONTACT PERSON</th>
                                          
                                          <th>NAS IP</th>
                                          <!--th>SSID</th-->
					  <th>LAST ACTIVE</th>
                                          <th >STATUS</th>
					  <th >DEVICE MODEL</th>
					  <th >ACTION</th>
                                       </tr>
                                    </thead>
				    <tbody id="search_gridview"  style="font-size:11px"></tbody>
                                   
				    
                                 </table>
                              </div>
                           </div>
			     <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="row" style="text-align:center">
                                    <input type='hidden' id="plimit" value="" />
                                    <input type='hidden' id="poffset" value="" />
				    <input type='hidden' id="ploadmoredata" value="1" />
                                    <div class="ploadmore_loader" style="display: none">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
                                    </div>
				    <div  onclick="load_more_location()" style="display: none" class="load_more_button">
				       <span id="loadMore">Load More</span>
				    </div>
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      
      
      
      
      <!-- bulk pins model start -->
     
      <div class="modal fade" id="create_location_group_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" >
         <div class="modal-dialog" role="document" style="margin-top:5%">
            <div class="modal-content" style="min-height: 500px">
               <div class="modal-header" style="padding:10px 15px">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center><h4>Create a new group Or add to an existing group</h4></center>
               </div>
               <div class="modal-body" style="padding:10px 15px">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group" style="margin-top: 15px">
                           <div class="row">
                              <div class="col-sm-4">
                                 <label class="location_lable" style="font-weight:600;">New Group</label>
                                 <input type="text" class="form-control" name="location_group_name" id="location_group_name"  placeholder="Enter Group Name"/>
                              </div>
                           </div>
                        </div>
                     </div>
		      <div class="col-sm-12">
                        <div class="form-group" style="margin-top: 15px">
                           <div class="row">
                              <div class="col-sm-4">
                                 <input type="text" class="form-control" name="location_group_username" id="location_group_username" placeholder="Enter User Name"/>
                              </div>
			      <div class="col-sm-4">
                                 <input type="text" class="form-control" name="location_group_password" id="location_group_password" placeholder="Enter Password"/>
                              </div>
			      <div class="col-sm-4">
                                 <input type="button" onclick="create_group()" class="mui-btn mui-btn--small mui-btn--accent" value="CREATE"/>
                              </div>
                           </div>
                        </div>
                     </div>
		     <div class="col-sm-12" style="margin: 10px 0px">
			<p id="create_location_group_error" style="color: red"></p>
		     </div>
		     <div class="col-sm-12" style="margin: 10px 0px">
			<p> Portal URL: <?php echo '<a href="'.HOMEWIFIPORTAL.'groupportal" target="_blank">'.HOMEWIFIPORTAL.'groupportal</a>';?></p>
		     </div>  
                     <div class="col-sm-12" style="margin: 10px 0px">
			<label class="location_lable" style="font-weight:600;">Select Existing group</label>
                        <div class="table-responsive modal-body3">
                           <table class="table table-striped" >
                              <tr class="table_active">
                                 <th>S.N</th>
				 <th>&nbsp;</th>
                                 <th>Group Name</th>
                                 <th>Total Location </th>
                                 <th>Username</th>
				 <th>Password</th>
				 <th>Action</th>
                              </tr>
			      <tbody id="generated_group_list">
				 
			      </tbody>
                              
                           </table>
                        </div>
			<center>
			   <p style="color: red" id="add_to_existing_group_error"></p>
			 <a onclick="loc_add_to_group()" class="mui-btn mui-btn--small mui-btn--accent">Add to group</a>
			</center>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	   
      <!-- bulk pins model end -->
      
      
      
      <div class="modal fade" id="edit_location_group_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" >
         <div class="modal-dialog" role="document" style="margin-top:5%">
            <div class="modal-content" style="min-height: 500px">
               <div class="modal-header" style="padding:10px 15px">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center><h4>Update Group</h4></center>
               </div>
               <div class="modal-body" style="padding:10px 15px">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group" style="margin-top: 15px">
                           <div class="row">
                              <div class="col-sm-4">
                                 <label class="location_lable" style="font-weight:600;">Group Detail</label>
                                 <input type="text" class="form-control" name="location_group_name_edit" id="location_group_name_edit"  placeholder="Enter Group Name"/>
				 <input type="hidden" class="form-control" name="group_id_edit" id="group_id_edit" />
                              </div>
                           </div>
                        </div>
                     </div>
		      <div class="col-sm-12">
                        <div class="form-group" style="margin-top: 15px">
                           <div class="row">
                              <div class="col-sm-4">
                                 <input type="text" class="form-control" name="location_group_username_edit" id="location_group_username_edit" placeholder="Enter User Name"/>
                              </div>
			      <div class="col-sm-4">
                                 <input type="text" class="form-control" name="location_group_password_edit" id="location_group_password_edit" placeholder="Enter Password"/>
                              </div>
                           </div>
                        </div>
                     </div>
		     
		     
		     
		     <div class="col-sm-12" style="margin: 10px 0px">
			<label class="location_lable" style="font-weight:600;">Select Location To Remove From Group</label>
                        <div class="table-responsive modal-body3 tbodys">
                           <table class="table table-striped" >
                              <tr class="table_active">
                                 <th>S.No</th>
				 <th>&nbsp;</th>
                                 <th>Location Name</th>
                              </tr>
			      <tbody id="group_location_list" class="">
				 
			      </tbody>
                              
                           </table>
                        </div>
			<center>
			   <p style="color: red" id="update_location_group_error"></p>
			 <a onclick="update_group()" class="mui-btn mui-btn--small mui-btn--accent">Update</a>
			</center>
                     </div>

			   
                  </div>
               </div>
            </div>
         </div>
      </div>
	   

     
      <script>
         $(document).ready(function() {
	    
	    //hide slider
	    $(".loading").hide();
	    //sert windiw hight
	    var height = $(window).height();
            $('#main_div').css('height', height);
	    var herader_height = $("#header").height();
	    height = height - herader_height;
	    $('#right-container-fluid').css('height', height);
	    
	    $('#advance_filters').click(function() {
         //alert("Hello! I am an alert box!!");
              $('.advance_filters_div').slideToggle("400");
	    });
	    $('#search_input').keyup(function(){  
	    $('#search_filter_areas').show();
	    });
	    $('body').click(function(e) {
	    if(e.target.id != 'search_input'){
	    $('#search_filter_areas').hide();
	    }
	    
	    });
	    
	    // on filter change
	    $('#location_type_filter ,#state_id_filter').change(function(){
	       $('#online_filter').val('');
	       $('#city_id_filter').val('');
	       submit_filter_form(); 
	    });
	    $('#city_id_filter').change(function(){
	       $('#online_filter').val('');
	       submit_filter_form(); 
	    });
          
         });
	 var timer = null;
	
	$("#search_type_filter").keyup(function(){
	  clearTimeout(timer);
	     timer = setTimeout(function() {
	       submit_filter_form();
		
	     }, 1000);
	 });
	 //submit filter form
	 function submit_filter_form(){
	    $(".loading").show();
	    document.getElementById("location_filter_form").submit(); 
	 }
	
	 function status(location_id) {
	    $(".loading").show()
	     $.ajax({
		     type: "POST",
		     url: "<?php echo base_url()?>location/change_location_status",
		     data: "location_uid="+location_id,
		     success: function(data){
			$(".loading").hide();
		     }
	       });
	 }
	 
	 function delete_wifi_location(location_id) {
	    var result = confirm("Are You Sure you want to delete ?");
	    if (result) {
	       $(".loading").show()
	       $.ajax({
		     type: "POST",
		     url: "<?php echo base_url()?>location/delete_wifi_location",
		     data: "location_uid="+location_id,
		     success: function(data){
			$("#location_"+location_id).closest('tr').remove();
			$(".loading").hide();
		     }
	       });
	    }
	    
	 }
	 
	 
	 
      /*$('.loc').each(function(){ 
	 var location_type = $(this).data('location_type');
	 var location_uid = $(this).data('locuid');
	 var ip_address = $(this).data('nasip');
	 if (location_type == '10') {// offline
	    var inactive="<img src='"+base_url+"assets/images/red.png'>";
	    $('.cla_'+location_uid).html(inactive);
	 }else{
	    if (ip_address != '') {
	       if (ip_address != '104.155.209.8') {
		  ping(ip_address, location_uid);// mikrotik
	       }else{
		  ping_onehop(ip_address, location_uid);// onehop
	       }
	    }else{
	       var inactive="<img src='"+base_url+"assets/images/red.png'>";
	       $('.cla_'+location_uid).html(inactive);
	    }
	 }
	 
      });*/
      $(document).on('click', '.loc_click', function(){
	 var location_type = $(this).data('location_type');
	 var location_uid = $(this).data('locuid');
	 var ip_address = $(this).data('nasip');
	 var loder_image = "<img src='"+base_url+"assets/images/loader.svg' width='100%'>";
	 $('.cla_'+location_uid).html(loder_image);
	 if (location_type == '10') {// offline
	    var inactive="<img src='"+base_url+"assets/images/red.png'>";
	    $('.cla_'+location_uid).html(inactive);
	 }
	 else{
	    if (ip_address != '') {
	       if (ip_address != '104.155.209.8') {
		  ping(ip_address, location_uid);// mikrotik
	       }else{
		  ping_onehop(ip_address, location_uid);// onehop
	       }
	    }else{
	       var inactive="<img src='"+base_url+"assets/images/red.png'>";
	       $('.cla_'+location_uid).html(inactive);
	    }
	 }
      });
      function ping(ip_address,location_uid) {
	 $.ajax({
                     type:"POST",
                     url: "<?php echo base_url()?>location/ping_mikrotik_ip_address",
                     cache:false,
                     dataType: 'json',
                     data:"ip_address="+ip_address,
                     success: function(data){
			var active="<img src='"+base_url+"assets/images/green.png'>";
			var inactive="<img src='"+base_url+"assets/images/red.png'>";
                        if (data == '1') {
			  $('.cla_'+location_uid).html(active);
			}else{
			  $('.cla_'+location_uid).html(inactive);
			}
			
                       
                     },
                     error: function(error){
                        //$("#loading").hide();
                     }
                  });
      }
      function ping_onehop(ip_address,location_uid) {
	 $.ajax({
                     type:"POST",
                     url: "<?php echo base_url()?>location/ping_onehop_ap",
                     cache:false,
                     dataType: 'json',
                     data:"ip_address="+ip_address+"&location_uid="+location_uid,
                     success: function(data){
			var active="<img src='"+base_url+"assets/images/green.png'>";
			var inactive="<img src='"+base_url+"assets/images/red.png'>";
                        if (data.code == '1') {
			  $('.cla_'+location_uid).html(active);
			}else{
			  $('.cla_'+location_uid).html(inactive);
			}
			
                       
                     },
                     error: function(error){
                        //$("#loading").hide();
                     }
                  });
      }
      
      
      
      $(document).ready(function() {
	 var search_results = '<?php echo $location_list['search_results']?>' ;
	 var paylimit = "<?php echo $location_list['limit'] ?>";
	 var payoffset = "<?php echo $location_list['offset'] ?>";
	 var payloadmore = "<?php echo $location_list['loadmore'] ?>";
	 var total_record = "<?php echo $location_list['total_record'] ?>";
	 $('#search_gridview').html(search_results);
	 $('#plimit').val(paylimit);
	 $('#poffset').val(payoffset);
	 $('#ploadmoredata').val(payloadmore);
	 $('.ploadmore_loader').hide();
	 if (parseInt(total_record) >= parseInt(paylimit)) {
	    $(".load_more_button").show();
	 }
	 
      });
      function load_more_location(){
	 $(".load_more_button").hide();
	 var limit = $('#plimit').val();
	 var offset = $('#poffset').val();
	 var formdata = '';
	 var location_type_filter = $("#location_type_filter").val();
	 var state_id_filter = $("#state_id_filter").val();
	 var city_id_filter = $("#city_id_filter").val();
	 var search_type_filter = $("#search_type_filter").val();
	 var online_filter = $("#online_filter").val();
	 var limit = $('#plimit').val();
	 var offset = $('#poffset').val();
	 var loaded = $('#ploadmoredata').val();
	 if (loaded != '0') {
	    $('.ploadmore_loader').show();
		     $.ajax({
			url: base_url+'location/location_list_more',
			type: 'POST',
			dataType: 'json',
			data: ({'location_type_filter': location_type_filter, 'state_id_filter': state_id_filter, 'city_id_filter': city_id_filter, 'search_type_filter': search_type_filter, 'limit': limit, 'offset': offset, "online_filter": online_filter}),
			success: function(data){
			   $('#search_gridview').append(data.search_results);
			   $('#plimit').val(data.limit);
			   $('#poffset').val(data.offset);
			   $('#ploadmoredata').val(data.loadmore);
			   $('.ploadmore_loader').hide();
			   if (data.loadmore != '0') {
			      $(".load_more_button").show();
			   }
			   
			}
		     });
	 }
      }
      $(document).on('click', '.online_status_check_click', function(){
	 var status_val = $(this).data('val_status');
	 $('#online_filter').val(status_val);
	 submit_filter_form();
      });
	  
	  
	  
	  
	 function export_to_excel(){
	 var location_type_filter = $("#location_type_filter").val();
	 var state_id_filter = $("#state_id_filter").val();
	 var city_id_filter = $("#city_id_filter").val();
	 var search_type_filter = $("#search_type_filter").val();
	 var online_filter = $("#online_filter").val();
	 $('.loading').show();
		     $.ajax({
			url: base_url+'location/location_list_export_to_excel',
			type: 'POST',
			dataType: 'text',
			data: ({'location_type_filter': location_type_filter, 'state_id_filter': state_id_filter, 'city_id_filter': city_id_filter, 'search_type_filter': search_type_filter,  "online_filter": online_filter}),
			success: function(data){
			  $('.loading').hide();
			  if(data == '1'){
				  window.location = "<?php echo base_url()?>location/download_location_list_excel";
			  }
			   
			}
		     });
	 
      }
      
      
      
      
      
      
      
      
      function create_group_open_popup(){
	 $("#create_location_group_error").html("");
	 $("#add_to_existing_group_error").html('');
	 get_location_group();
	 $("#create_location_group_modal").modal("show");
      }
      
      function get_location_group(){
	 $('#generated_group_list').html('<tr><td colspan="7" style="text-align:center">Please Wait</td></tr>');
	 var location_type = $("#location_type_filter").val();
	 $.ajax({
			url: base_url+'location/get_location_group',
			type: 'POST',
			dataType: 'json',
			data: ({'location_type': location_type}),
			success: function(data){
			  $('#generated_group_list').html(data.search_results);
			}
	 });
      }
      
      function create_group(){
	 
	 $("#create_location_group_error").html("");
	 var group_name = $("#location_group_name").val();
	 var username = $("#location_group_username").val();
	 var password = $("#location_group_password").val();
	 var location_type = $("#location_type_filter").val();
	 if (group_name == '' || username == '' || password == '') {
	    $("#create_location_group_error").html("Please fill all fields");
	    return false;
	 }
	 var locations = [];
	 $.each($("input[name='location_group[]']:checked"), function(){            

            locations.push($(this).val());

         });
	  $('.loading').show();
	 $.ajax({
			url: base_url+'location/create_location_group',
			type: 'POST',
			dataType: 'json',
			data: ({'group_name': group_name,'username': username,'password': password, 'location_type':location_type,'locations': locations}),
			success: function(data){
			   $('.loading').hide();
			   if(data.resultCode == '0'){
			      $("#create_location_group_error").html(data.resultMsg);
			   }else{
			      $.each($("input[name='location_group[]']:checked"), function(){     
				  $(this).prop("checked",false);
			      });
			      $("#create_location_group_error").html("Successfully created group");
			      $("#location_group_name").val('');
			      $("#location_group_username").val('');
			      $("#location_group_password").val('');
			      get_location_group();
			   }
			}
	 });
      }
      
      function loc_add_to_group(){
	 $("#add_to_existing_group_error").html('');
	 var group_id = '';
	 if ($('input[name="group_select_radio"]').is(':checked')) {
	    group_id = $("input[name='group_select_radio']:checked").val();
	 }
	 if (group_id == '') {
	    $("#add_to_existing_group_error").html("Please Select group");
	    return false;
	 }
	 var locations = [];
	 $.each($("input[name='location_group[]']:checked"), function(){            
	    locations.push($(this).val());
	 });
	 $('.loading').show();
	 $.ajax({
			url: base_url+'location/add_location_to_group',
			type: 'POST',
			dataType: 'json',
			data: ({'locations': locations,'group_id': group_id}),
			success: function(data){
			    $.each($("input[name='location_group[]']:checked"), function(){     
				  $(this).prop("checked",false);
			      });
			   $('.loading').hide();
			   get_location_group();
			}
	 });
      }
      
      function delete_loc_group(group_id) {
	    var result = confirm("Are You Sure you want to delete ?");
	    if (result) {
	       $(".loading").show()
	       $.ajax({
		     type: "POST",
		     url: "<?php echo base_url()?>location/delete_loc_group",
		     data: "group_id="+group_id,
		     success: function(data){
			$(".loading").hide();
			get_location_group();
		     }
	       });
	    }
	    
      }
      
      function edit_loc_group(group_id, group_name, username, password){
	 get_group_location_list(group_id);
	 $("#group_id_edit").val(group_id);
	 $("#location_group_name_edit").val(group_name);
	 $("#location_group_username_edit").val(username);
	 $("#location_group_password_edit").val(password);
	 $("#update_location_group_error").html("");
	 $("#create_location_group_modal").modal("hide");
	 $("#edit_location_group_modal").modal("show");
      }
      function update_group(){
	 
	 $("#update_location_group_error").html("");
	 var group_name = $("#location_group_name_edit").val();
	 var username = $("#location_group_username_edit").val();
	 var password = $("#location_group_password_edit").val();
	 var group_id = $("#group_id_edit").val();
	 if (group_name == '' || username == '' || password == '') {
	    $("#update_location_group_error").html("Please fill all fields");
	    return false;
	 }
	 var is_loc_remove = 0;
	 var locations = [];
	 $.each($("input[name='group_location_list[]']:checked"), function(){            
	    locations.push($(this).val());
	    is_loc_remove = 1;
	 });
	  $('.loading').show();
	 $.ajax({
			url: base_url+'location/update_location_group',
			type: 'POST',
			dataType: 'json',
			data: ({'group_name': group_name,'username': username,'password': password, 'group_id':group_id,'locations':locations}),
			success: function(data){
			   $('.loading').hide();
			   if(data.resultCode == '0'){
			      $("#update_location_group_error").html(data.resultMsg);
			   }else{
			      if (is_loc_remove == 1) {
				 get_group_location_list(group_id);
			      }
			      
			      $("#update_location_group_error").html("Successfully Updated group");
			   }
			}
	 });
      }
      function get_group_location_list(group_id){
	 $('#group_location_list').html('<tr><td colspan="3" style="text-align:center">Please Wait</td></tr>');
	 
	 $.ajax({
			url: base_url+'location/get_group_location_list',
			type: 'POST',
			dataType: 'json',
			data: ({'group_id': group_id}),
			success: function(data){
			  $('#group_location_list').html(data.search_results);
			}
	 });
      }
      
      
      $('.modal').on('hidden.bs.modal', function (e) {
	    if($('.modal').hasClass('in')) {
	    $('body').addClass('modal-open');
	    }    
	 });
      
      $(document).on('click', '#check_all_location', function(){
       if ($('input[name="location_group_check_all"]').is(':checked')) {
         $.each($("input[name='location_group[]']"), function(){     
	  $(this).prop("checked",true);
	 });
       }
       else{
	$.each($("input[name='location_group[]']"), function(){     
	 $(this).prop("checked",false);
	});
       }
      });
      </script>
            
</body>
</html>
