 <?php $this->load->view('includes/header');?>
   <div class="loading" >
      <img src="<?php echo base_url()?>assets/images/loader.svg"/>
   </div>
   <div class="container-fluid">
      <div class="row">
	 <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select os-container">
	        <?php
		  $data['navperm']=$this->plan_model->leftnav_permission();
			
		     ?>
		  
               <?php $this->load->view('includes/left_nav');?>
            </div>
	    <!--header id="os-header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid nopadding-left">
                       <div class="navbar-header">
                           <span class="os-navbar-icons" id="button-os">
                           <i class="fa fa-th-large fa-lg" aria-hidden="true"></i>
							</span>
                        </div>
                        <div class="navbar-header">
                           <a class="navbar-brand os-navbar-brand" href="#">
			      
                           </a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
			      <li class="dropdown mui-btn--small mui-btn--accent" style="border-radius: 2px; margin-top: 10px;margin-right:15px; padding:0px 5px">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" 
								style="background-color:none;color: #fff; font-weight: 400; padding:2px 10px;"><i class="fa fa-plus-circle" aria-hidden="true"></i>
								ADD LOCATION

								<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="#">ADD NEW LOCATION</a></li>
									<li role="separator" class="divider"></li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/public">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Public Spaces
								  </a>
								  </li>
								 <li>
								  <a href="<?php echo base_url()?>location/add_location_view/cafe">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Cafe & Restaurants
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/hotel">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Hotels & Hospitality
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/hospital">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Hospitals & Healthcare
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/institutional">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Institutions & Colleges
								  </a>
								  </li>
								  <li>
								  <a href="<?php echo base_url()?>location/add_location_view/enterprise">
								  <i class="fa fa-plus-circle" aria-hidden="true"></i> Enterprise
								  </a>
								  </li>
								</ul>
							  </li>
                               
                              <?php //$this->load->view('includes/global_setting',$data);?>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header-->
            <header id="header">
               <nav class="navbar navbar-default">
		  <div class="container-fluid">
		     <div class="navbar-header">
			<p class="navbar-brand">Locations</p>
                     </div>
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			   <li class="dropdown mui-btn--small mui-btn--accent" style="border-radius: 2px; margin-top: 10px;margin-right:15px; padding:0px 5px">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" 
								style="background-color:none;color: #fff; font-weight: 400; padding:2px 10px;"><i class="fa fa-plus-circle" aria-hidden="true"></i>
								ADD LOCATION

								<span class="caret"></span></a>
								<ul class="dropdown-menu" style='max-height:350px; overflow-y: auto'>
								  <li><a href="#">ADD NEW LOCATION</a></li>
								  <li role="separator" class="divider"></li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/public">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Public Spaces
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/hotel">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Hotels & Hospitality
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/hospital">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Hospitals & Healthcare
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/institutional">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Institutions & Colleges
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/enterprise">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Enterprise
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/cafe">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Cafe & Restaurants
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/retail">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Retail
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/custom">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Custom Location
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/purple">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Purple Box
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/offline">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Offline
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/visitor">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Visitor Management
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/contestifi">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Contestifi
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/retail_audit">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Retail Audit
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/event_module">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Event Module
								     </a>
								  </li>
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/ordering">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Ordering
								     </a>
								  </li>
								  <!--<li>
								     <a href="<?php echo base_url()?>location/add_location_view/lms">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> LMS
								     </a>
								  </li>-->
								  <li>
								     <a href="<?php echo base_url()?>location/add_location_view/internact">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Internal Activation
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/eventguest">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Event Guest Management
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/club">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Club
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/truck_stop">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Truck Stop
								     </a>
								  </li>
								   <li>
								     <a href="<?php echo base_url()?>location/add_location_view/foodle">
									<i class="fa fa-plus-circle" aria-hidden="true"></i> Foodle
								     </a>
								  </li>
								</ul>
							  </li>
			   <?php $this->load->view('includes/global_setting',$data);?>
                        </ul>
                     </div>
                  </div>
               </nav>
            </header>
            <div id="content-wrappers">
	       <div class="mui--appbar-height"></div>
	       <div class="mui-container-fluid" id="right-container-fluid">
		  <?php
		     if($locations->public_active > 0 || $locations->public_inactive > 0 || $locations->hotel_active > 0 || $locations->hotel_inactive > 0 || $locations->hospital_active > 0 || $locations->hospital_inactive > 0 || $locations->institutional_active > 0 || $locations->institutional_inactive > 0 || $locations->enterprise_active > 0 || $locations->enterprise_inactive > 0 || $locations->caffe_active > 0 || $locations->caffe_inactive > 0 || $locations->retail_active > 0 || $locations->retail_inactive > 0|| $locations->custom_active > 0 || $locations->custom_inactive > 0 || $locations->purple_active > 0 || $locations->purple_inactive > 0 || $locations->offline_active > 0 || $locations->offline_inactive > 0|| $locations->visitor_active > 0 || $locations->visitor_inactive > 0|| $locations->contestifi_active > 0 || $locations->contestifi_inactive > 0|| $locations->retail_audit_active > 0 || $locations->retail_audit_inactive > 0 || $locations->event_module_active > 0 || $locations->event_module_inactive > 0 || $locations->ordering_active > 0 || $locations->ordering_inactive > 0 || $locations->lms_active > 0 || $locations->lms_inactive > 0 || $locations->internact_active > 0 || $locations->internact_inactive > 0 || $locations->eventguest_active > 0 || $locations->eventguest_inactive > 0 || $locations->club_active > 0 || $locations->club_inactive > 0 || $locations->truck_stop_active > 0 || $locations->truck_stop_inactive > 0 || $locations->foodle_active > 0 || $locations->foodle_inactive > 0){
		     ?>
                  <div class="right_side">
		     
                     <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <h1><?php echo $locations->total_location?> <small>Locations</small></h1>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <ul>
			      <li <?php if($locations->public_active <= 0 && $locations->public_inactive <= 0) echo 'style="display:none"'?>>
				 <a href="<?php echo base_url()?>location/location_list/public">
				    <h5>PUBLIC SPACES</h5>
				    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small><i class="fa fa-users" aria-hidden="true"></i></small>
						   
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->public_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->public_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
                              <li style="background-color:#833965 <?php if($locations->hotel_active <= 0 && $locations->hotel_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/hotel">
				    <h5>HOTEL</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-bed" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->hotel_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->hotel_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      <li style="background-color:#049966 <?php if($locations->hospital_active <= 0 && $locations->hospital_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/hospital">
				    <h5>HOSPITALS & HEALTHCARE</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-hospital-o" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->hospital_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->hospital_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      <li style="background-color:#08b1b0 <?php if($locations->institutional_active <= 0 && $locations->institutional_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/institutional">
				    <h5>INSTITUTIONALS & COLLEGES</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-university" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->institutional_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->institutional_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      <li style="background-color:#2e3192 <?php if($locations->enterprise_active <= 0 && $locations->enterprise_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/enterprise">
				    <h5>ENTERPRISE</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-building-o" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->enterprise_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->enterprise_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      <li style="background-color:#9470dc <?php if($locations->caffe_active <= 0 && $locations->caffe_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/caffe">
				    <h5>CAFE & RESTAURANTS</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-coffee" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->caffe_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->caffe_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      <li style="background-color:#942c5d  <?php if($locations->retail_active <= 0 && $locations->retail_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/retail">
				    <h5>RETAIL</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-diamond" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->retail_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->retail_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
			      <li style="background-color:#36465f  <?php if($locations->custom_active <= 0 && $locations->custom_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/custom">
				    <h5>CUSTOM LOCATION</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-cubes" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->custom_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->custom_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
			      <li style="background-color:#663399  <?php if($locations->purple_active <= 0 && $locations->purple_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/purple">
				    <h5>PURPLE BOX</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-glass" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->purple_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->purple_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
			      
			      <li style="background-color:#00a2ff  <?php if($locations->offline_active <= 0 && $locations->offline_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/offline">
				    <h5>OFFLINE</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="material-icons" style="font-size:40px">&#xE894;</i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->offline_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->offline_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
			      <li style="background-color:#00a2ff  <?php if($locations->visitor_active <= 0 && $locations->visitor_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/visitor">
				    <h5>VISITOR MANAGEMENT</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="material-icons" style="font-size:40px">&#xE894;</i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->visitor_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->visitor_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
			      <li style="background-color:#00a2ff  <?php if($locations->contestifi_active <= 0 && $locations->contestifi_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/contestifi">
				    <h5>CONTESTIFI</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="material-icons" style="font-size:40px">&#xE894;</i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->contestifi_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->contestifi_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      <li style="background-color:#942c5d  <?php if($locations->retail_audit_active <= 0 && $locations->retail_audit_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/retail_audit">
				    <h5>RETAIL AUDIT</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-diamond" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->retail_audit_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->retail_audit_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
                              <li style="background-color:#942c5d  <?php if($locations->event_module_active <= 0 && $locations->event_module_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/event_module">
				    <h5>EVENT MODULE</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-diamond" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->event_module_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->event_module_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
			      <li style="background-color:#942c5d  <?php if($locations->ordering_active <= 0 && $locations->ordering_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/ordering">
				    <h5>Ordering</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-diamond" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->ordering_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->ordering_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      			      <li style="background-color:#942c5d  <?php if($locations->lms_active <= 0 && $locations->lms_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/lms">
				    <h5>LMS</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-diamond" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->lms_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->lms_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>

			      <li style="background-color:#942c5d  <?php if($locations->internact_active <= 0 && $locations->internact_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/internact">
				    <h5>Internal Activation</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-trophy" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->internact_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->internact_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
							  
							  <li style="background-color:#942c5d  <?php if($locations->eventguest_active <= 0 && $locations->eventguest_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/eventguest">
				    <h5>Event Guest Management</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-trophy" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->eventguest_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->eventguest_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      <li style="background-color:#942c5d  <?php if($locations->club_active <= 0 && $locations->club_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/club">
				    <h5>Club</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-trophy" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->club_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->club_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      <li style="background-color:#942c5d  <?php if($locations->truck_stop_active <= 0 && $locations->truck_stop_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/truck_stop">
				    <h5>Truck Stop</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-trophy" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->truck_stop_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->truck_stop_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
			      <li style="background-color:#9470dc <?php if($locations->foodle_active <= 0 && $locations->foodle_inactive <= 0) echo ';display:none'?>">
                                 <a href="<?php echo base_url()?>location/location_list/foodle">
				    <h5>FOODLE</h5>
                                    <div class="col-sm-12 col-xs-12">
				       <div class="row">
					  <div class="col-sm-2 col-xs-2">
					     <div class="row">
						<h4>
						   <small>
						      <i class="fa fa-coffee" aria-hidden="true"></i>
						   </small> 
						</h4>
					     </div>
					  </div>
					  <div class="col-sm-8 col-xs-8">
					     <h5>ACTIVE: <?php echo $locations->foodle_active?></h5>
					     <h5>INACTIVE: <?php echo $locations->foodle_inactive?></h5>
					  </div>
					  <div class="col-sm-1 col-xs-1">
					     <h4>
						<small style="font-size:24px;"><i class="fa fa-angle-right" aria-hidden="true"></i></small>
					     </h4>
					  </div>
				       </div>
				    </div>
				 </a>
                              </li>
			      
                           </ul>
                        </div>
		     
			<div class="row">
			   <div class="serch_box">
			      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-md-offset-3" style="margin-top:3%">
				 <div class="input-group" id="adv-search">
				    <input type="text" class="serch-form-control form-control" placeholder="Search Location" onFocus="this.placeholder=''" onBlur="this.placeholder='Search Location'" style="box-shadow:none" id="search_input" name="search_input" />
				    <div class="input-group-btn">
				       <div class="btn-group" role="group">
					  <div class="dropdown dropdown-lg">
					     <button type="button" class="btn btn-primary" id="search_filter_go_button" style="border-bottom-right-radius :4px; border-top-right-radius :4px;">
						   Go
					     </button>
					     <div class="dropdown-menu dropdown-menu-right"  id="search_filter_areas" style="border-radius:0px; margin-top: 2px;">
						      
					     </div>
					  </div>
				       </div>
				    </div>
				 </div>
			      </div>
			   </div>
			</div>
		     </div>
		  </div>
		  <?php }else{
			?>
		  <div class="os_right_side">
		     <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <h2>
			      0 <small>Locations</small>
			   </h2>
			   <h6>Add your first location to get started! Select a type of public location you&#39;d like to run public Wi-fi at.</h6>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
			   <div class="row">
			      <div class="col-lg-12 col-md-12 col-sm-12">
				 <div class="os_left_add_location">
				    <h3>SELECT A LOCATION TYPE TO ADD</h3>
				    <div class="row">
				       <ul>
					  <li class="users-os-box">
					     <a href="<?php echo base_url()?>location/add_location_view/public">
					     <i class="fa fa-users fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Public Spaces</h6>
					  </li>
					  <li class="bed-os-box">
					     <a href="<?php echo base_url()?>location/add_location_view/hotel">
					     <i class="fa fa-bed fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Hotels & Hospitality</h6>
					  </li>
					  <li class="hospital-os-box">
					     <a href="<?php echo base_url()?>location/add_location_view/hospital">
					     <i class="fa fa-hospital-o fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Hospitals & Healthcare</h6>
					  </li>
					  <li class="university-os-box">
					     <a href="<?php echo base_url()?>location/add_location_view/institutional">
					     <i class="fa fa-university fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Institutions & Colleges</h6>
					  </li>
					  <li class="building-os-box">
					     <a href="<?php echo base_url()?>location/add_location_view/enterprise">
					     <i class="fa fa-building-o fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Enterprise</h6>
					  </li>
					  
					  <li class="building-os-box" style="background-color:#9470dc">
					     <a href="<?php echo base_url()?>location/add_location_view/cafe">
					     <i class="fa fa-coffee fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Cafe</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/retail">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Retail</h6>
					  </li>
					  <li style="background-color: #36465f;">
					     <a href="<?php echo base_url()?>location/add_location_view/custom">
					     <i class="fa fa-cubes fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Custom Location</h6>
					  </li>
					  <li style="background-color: #663399 ;">
					     <a href="<?php echo base_url()?>location/add_location_view/purple">
					     <i class="fa fa-glass fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Custom Location</h6>
					  </li>
					  <li style="background-color: #00a2ff ;">
					     <a href="<?php echo base_url()?>location/add_location_view/offline">
						<i class="material-icons" style="font-size:65px">&#xE894;</i>
					     </a>
					     <h6>Offline</h6>
					  </li>
					  <li style="background-color: #00a2ff ;">
					     <a href="<?php echo base_url()?>location/add_location_view/visitor">
						<i class="material-icons" style="font-size:65px">&#xE894;</i>
					     </a>
					     <h6>Visitor Management</h6>
					  </li>
					  <li style="background-color: #00a2ff ;">
					     <a href="<?php echo base_url()?>location/add_location_view/contestifi">
						<i class="material-icons" style="font-size:65px">&#xE894;</i>
					     </a>
					     <h6>Contestifi</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/retail_audit">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Retail Audit</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/event_module">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Event Module</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/ordering">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Ordering</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/lms">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>LMS</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/internact">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Internal Activation</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/eventguest">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Event Guest Management</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/club">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Club</h6>
					  </li>
					  <li style="background-color: #942c5d;">
					     <a href="<?php echo base_url()?>location/add_location_view/truck_stop">
					     <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Truck Stop</h6>
					  </li>
					  <li class="building-os-box" style="background-color:#9470dc">
					     <a href="<?php echo base_url()?>location/add_location_view/foodle">
					     <i class="fa fa-coffee fa-5x" aria-hidden="true"></i>
					     </a>
					     <h6>Foodle</h6>
					  </li>
				       </ul>
				    </div>
				 </div>
			      </div>
			   </div>
			</div>
		     </div>
		  </div>

	
			<?php
			}
			?>
	       </div>
	    </div>
	 </div>
      </div>
   </div>
      <!-- /Start your project here-->
      <!-- JQuery -->
      
      <script>
         $(document).ready(function() {
	    //hide slider
	    $(".loading").hide();
	    var height = $(window).height();
            $('#main_div').css('height', height);
	    var herader_height = $("#header").height();
	    height = height - herader_height;
	    $('#right-container-fluid').css('height', height);
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
         $('#advance_filters').click(function() {
         //alert("Hello! I am an alert box!!");
              $('.advance_filters_div').slideToggle("400");
         });
         // search box dropdown // 
         $('#search_input').keyup(function(){  
         $('#search_filter_areas').show();
         });
         $('body').click(function(e) {
         if(e.target.id != 'search_input'){
         $('#search_filter_areas').hide();
         }
         
         });
         });
         
         $(document).click(function(){
         
         $("#left_menu").toggleClass('closed');
         $("#left_menu2").toggleClass('open'); 
         });
      </script>
	  <script type="text/javascript" language="javascript">
	       $(document).ready(function () {
		
		
		// search
		  var timer = null;
		  $("#search_input").keyup(function(){
		     clearTimeout(timer);
			timer = setTimeout(function() {
			  submit_filter_form();
			   
			}, 0);
		    });
		  
	       });
	       
	       $("#search_filter_go_button").click(function(){
		  submit_filter_form();
	       });
	       
	       function submit_filter_form() {
		  var search_val = $("#search_input").val();
		     $.ajax({
			type: "POST",
			url: "<?php echo base_url()?>location/location_dashboard_search",
			data: "search_val="+search_val,
			success: function(data){
			   $("#search_filter_areas").html(data);
			}
		     });
	       }
	       function view_all() {
		  var search_val = $("#search_input").val();
		  window.location.href = '<?php echo base_url()?>location/location_list/';
	       }
	</script>
	  
</body>
</html>
