<?php $this->load->view('includes/header');?>
 <input type="hidden" name="location_id_hidden" id="location_id_hidden" value="0">
    <input type="hidden" name="created_location_id" id="created_location_id" value="0">
<script type="text/javascript" src="<?php echo base_url()?>assets/js/responsiveCarousel.min.js"></script>
<div style="display:none" class="wikipedia_temp_data"></div>
   <div class="loading" >
      <img src="<?php echo base_url()?>assets/images/loader.svg"/>
   </div>
   <div class="container-fluid">
      
      <div class="row">
         <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select os-container">
               
               <?php
	       $data['navperm']=$this->plan_model->leftnav_permission();
	       $this->load->view('includes/left_nav');?>
            </div>
             <header id="header">
               <nav class="navbar navbar-default">
		  <div class="container-fluid">
		     <div class="navbar-header">
			<p class="navbar-brand">Locations</p>
                     </div>
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			  
			   <?php $this->load->view('includes/global_setting',$data);?>
                        </ul>
                     </div>
                  </div>
               </nav>
            </header>


            <div id="content-wrapper" >
               <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="os_right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <div class="col-sm-6 col-xs-6">
                                    <h2>
				       LMS
				    </h2>
				 </div>
				 
			      </div>
                           </div> 
                           <div class="col-lg-12 col-md-12 col-sm-12">
			      <div class="mui---osbar-height"></div>
				 <div class="row">
				    <div class="col-lg-12 col-md-12 col-sm-12">
				       <ul class="mui-tabs__bar">
                                          
                                         
					  <li class="mui--is-active" id="content_builder_tab_li" >
                                             <a data-mui-toggle="tab" data-mui-controls="content_builder" onclick="content_builder()" class="tab_menu" >
                                             Content Builder
                                             </a>
                                          </li>
					  <li id="contestifi_content_builder_tab_li" >
                                             <a data-mui-toggle="tab" data-mui-controls="contestifi_content_builder" onclick="contestifi_content_builder()" class="tab_menu" >
                                             Contest Builder
                                             </a>
                                          </li>
                                          
                                       </ul>
				    </div>
				    <div class="col-lg-12 col-md-12 col-sm-12">
				       <div class="mui--osbar-height"></div>
                                       
                                       
                                       
                                       
                                 
				       
				       <!-- content builder tab start -->
				       
				       <div class="mui-tabs__pane offline mui--is-active" id="content_builder">
					  <div class="row">
					      <div class="col-lg-12 col-sm-12">
						  <div class="row">
						      <div class="col-sm-12 col-xs-12">
							  <h4>CONTENT BUILDER</h4>
							  <span class="os-otp">Please create you content structure and upload the relevant media. The CP will allow users to navigate to the content based on the heirarchy built here.</span>	
						      </div>
						  </div>
						  <div class="row">
						      <form id="add_content_builder"  enctype="multipart/form-data" onsubmit="add_content_builder(); return false;">
							  <br/>
							  <div class="row" style="margin-top:30px; margin-bottom:20px">
							      <div class="col-sm-12 col-xs-12">
								  <div class="col-sm-12 col-xs-12">
								      <div class="offline_section">
									  <div class="row">
									      <div class="col-sm-12 col-xs-12">
										  <div class="offline_list">
										      <div class="table-responsive">
											  <table class="table offline-table-bordered" style="margin-bottom: 0px;">
											      <tr>
												  <th class="col-sm-4 col-xs-4 offline_br">
												      <h5>Main Section</h5>
												      <h6>Main sections will be displayed as a list.</h6>
												  </th>
												  <th class="col-sm-4 col-xs-4 offline_br">
												      <h5>Sub Section / Content </h5>
												      <h6>You can add either sub sections or content. </h6>
												  </th>
												  <th class="col-sm-4 col-xs-4 offline_b" style="border-right:none">
												      <h5>Content</h5>
												      <h6>Here you can only add content</h6>
												  </th>
											      </tr>
											      <tbody>
												  <tr>
												      <td class="col-sm-4">
													  <div class="col-sm-12 offline-table">
													      <div class="row">
														  <div style="display: none" class="offline_main_section_filled_div" >
														     <ul class = "main_section_content_list">
														     </ul>
														     <ul>
															<li>
															   <a onclick="open_add_main_category()"> <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Section</a>		
															</li>
														     </ul>
														  </div>
														  <div style="display: none" class="offline-col offline_main_section_blank_div" onclick="open_add_main_category()">
														      <center>
															  <div class="col-sm-12">
															      <span class="mui-btn offline--outline btn-sm">
															       Add New Section
															      </span>
															  </div>
														      </center>
														  </div>
													      </div>
													  </div>
												      </td>
												      <td class="col-sm-4">
													  <div class="col-sm-12">
													      <div class="row">
													       <div style="display: none" class="offline-col offline_sub_section_blank_first_div" onclick="open_add_main_category()">
														      <center>
															<p>
															   Add Main Section First
															</p>
														      </center>
														  </div>
														  <div style="display: none" class="offline_list offline_sub_section_filled_div">
														     <ul class = "sub_section_content_list">
															
														     </ul>
														     <ul>
															  
															  <li>
															      <a class="sub_category_add_data" data-is_this_is_content="0"> 
															      <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Sub Section</a>		
															  </li>
														     </ul>
														  </div>
														  <div style="display: none" class="offline-col offline_sub_section_blank_div">
														      <center>
															  <div class="col-sm-12 discuss_question_add_option" onclick="open_add_sub_category()">
															      <span class="mui-btn offline--outline btn-sm">
															      Add New Section
															      </span>
															  </div>
															  <div class="col-sm-12 discuss_question_add_option">
															      OR
															  </div>
															  <div class="col-sm-12" onclick="choose_content_type(1)">
															      <span class="mui-btn offline--outline btn-sm" data-toggle="modal" data-target="">
															      Add Content
															      </span>
															  </div>
														      </center>
														  </div>
														  
													      </div>
													  </div>
												      </td>
												      <td class="col-sm-4" style="border-right:none">
													  <div class="col-sm-12">
													      <div class="row">
													       <div style="display: none" class="offline-col offline_content_section_blank_first_div">
														      <center>
															<p>
															   Add Sub Section First
															</p>
														      </center>
													       </div>
													       <div style="display: none" class="offline_list offline_content_section_filled_div">
														  <ul class = "content_section_content_list">
														     
														  </ul>
														  <ul>
														     <li>
															<a onclick="choose_content_type(0)">
															<i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add More Content</a>        
														     </li>
														  </ul>
													       </div>
													       <div style="display: none" class="offline-col  offline_content_section_blank_div">
														  <center>
														     <div class="col-sm-12" onclick="choose_content_type(0)">
															<span class="mui-btn offline--outline btn-sm" data-toggle="modal" data-target="#mymodalcontent">
															Add Content
															</span>
														     </div>
														  </center>
													       </div>
														  
													      </div>
													  </div>
												      </td>
												  </tr>
											      </tbody>
											  </table>
										      </div>
										  </div>
									      </div>
									  </div>
								      </div>
								  </div>
							      </div>
							  </div>
							  
						      </form>
						  </div>
					      </div>
					  </div>
				       </div>

				       <!-- content builder tab end -->
                                       
				       <!-- content builder tab start for contestifi -->
				       
				       <div class="mui-tabs__pane offline" id="contestifi_content_builder">
					  <div class="row">
					     <div class="col-lg-12 col-sm-12">
						<div class="row">
						   <div class="col-sm-12 col-xs-12">
						      <h4>CONTEST BUILDER</h4>
						    </div>
						</div>
						
						<div class="row">
						   <form id="contestifi_add_content_builder"  enctype="multipart/form-data" onsubmit="contestifi_add_content_builder(); return false;">
						      <div class="row" style="margin-top:30px; margin-bottom:20px">
							 <div class="col-sm-12 col-xs-12">
							    <div class="col-sm-12 col-xs-12">
							       <div class="offline_section">
								  <div class="row">
								     <div class="col-sm-12 col-xs-12">
									<div class="offline_list">
									   <div class="table-responsive">
									      <table class="table offline-table-bordered" style="margin-bottom: 0px;">
										 <tr>
										    <th class="col-sm-4 col-xs-4 offline_br">
											<h5>Contest</h5>
										    </th>
										    <th class="col-sm-4 col-xs-4 offline_br">
											<h5>Quiz </h5>
										    </th>
										    <th class="col-sm-4 col-xs-4 offline_br">
											<h5>Contest Prize </h5>
										    </th>
										 </tr>
										 <tbody>
										    <tr>
										       <td class="col-sm-4">
											  <div class="col-sm-12 offline-table">
											     <div class="row">
												<div style="display: none" class="contestifi_content_filled_div">
												   <ul class = "contestifi_section_content_list">
												   </ul>
												   <ul>
												      <li>
													 <a onclick="open_add_contestifi_contest()"> <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Contest</a>		
												      </li>
												   </ul>
												</div>
												<div style="display: none" class="offline-col contestifi_content_blank_div" onclick="open_add_contestifi_contest()">
												   <center>
												      <div class="col-sm-12">
													 <span class="mui-btn offline--outline btn-sm">
													       Add New Contest
													      </span>
													  </div>
												      </center>
												</div>
											     </div>
											  </div>
										       </td>
										       <td class="col-sm-4">
											  <div class="col-sm-12">
											     <div class="row">
												<div style="display: none" class="offline-col contestifi_quiz_blank_first_div" onclick="open_add_contestifi_quiz()">
												   <center>
												      <p>
													 Add Contest First
												      </p>
												   </center>
												</div>
												<div style="display: none" class="offline_list contestifi_quiz_filled_div">
												   <ul class = "contestifi_quiz_list"></ul>
												   <ul>
												      <li onclick="open_add_contestifi_quiz()">
													 <a class="contestifi_quiz_add_data"> 
													    <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Quiz
													 </a>		
												      </li>
												   </ul>
												</div>
												<div style="display: none" class="offline-col contestifi_quiz_blank_div">
												   <center>
												      <div class="col-sm-12" onclick="open_add_contestifi_quiz()">
													 <span class="mui-btn offline--outline btn-sm">
													    Add New Quiz
													 </span>
												      </div>
												   </center>
												</div>
											     </div>
											  </div>
										       </td>
										       
										       <td class="col-sm-4">
											  <div class="col-sm-12">
											     <div class="row">
												<div style="display: none" class="offline-col contestifi_prize_blank_first_div" onclick="open_add_contestifi_prize()">
												   <center>
												      <p>
													 Add Contest First
												      </p>
												   </center>
												</div>
												<div style="display: none" class="offline_list contestifi_prize_filled_div">
												   <ul class = "contestifi_prize_list"></ul>
												   <ul>
												      <li onclick="open_add_contestifi_prize()">
													 <a class="contestifi_prize_add_data"> 
													    <i class="fal fa-plus-square" style="font-family:FontAwesome"></i> Add New Prize
													 </a>		
												      </li>
												   </ul>
												</div>
												<div style="display: none" class="offline-col contestifi_prize_blank_div">
												   <center>
												      <div class="col-sm-12" onclick="open_add_contestifi_prize()">
													 <span class="mui-btn offline--outline btn-sm">
													    Add New Prize
													 </span>
												      </div>
												   </center>
												</div>
											     </div>
											  </div>
										       </td>
										    </tr>
										 </tbody>
									      </table>
									   </div>
									</div>
								     </div>
								  </div>
							       </div>
							    </div>
							 </div>
						      </div>
						     
						   </form>
						</div>
						
					     </div>
					  </div>
				       </div>

				       <!-- content builder tab end for contestifi -->
                                       
                                  
                                       
                                    </div>
                                 </div>
			   </div>
			</div>    
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
      


        
        

        






	
	
	
	<!----  Main Section Modal start ----->	
        <div class="modal fade" id="main_category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">New Main Section</h4>
                    </div>
		    <form id="add_new_main_section_form" enctype="multipart/form-data" onsubmit="add_main_category(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield mui-textfield--float-label">
			      <input type="hidden" name="icon_validation" id="icon_validation" value='1'>
                                <input type="text" name="main_category_name" id="main_category_name" maxlength="30">
                                <label>Section Name<sup>*</sup></label>
                            </div>
			    <span class="text-right pull-right" ><span class="main_category_name_length">0</span><span>/30</span></span>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="main_category_desc" id="main_category_desc" ></textarea>
                                <label>Section Description</label>
				
                            </div>
			    
			    
			    
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12 icon_upload_class_image" name="main_category_file" id="main_category_file">
				    <label>Upload Icon (Optional)</label>
				 <span><em>Preffered Size 48 * 48 px, PNG or JPG</em></span>     
			      </div>
			   </div>
			    
			     <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group os-form-group">
                                        <label class="checkbox-inline">
                                            <input name="compliance_mandatory" value="1" type="checkbox" class="openmodal"> Compliance Mandatory
                                        </label>
                                    </div>
                                </div>
                            </div>
			    
                    </div>
		    <center><p id="create_main_category_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </form>
                </div>
            </div>
        </div>
        <!----  Main Section Modal end ----->
	
	<!----  Sub Section Modal start ----->	
        <div class="modal fade" id="sub_category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">New Sub Section</h4>
                    </div>
		    <form id="add_new_sub_section_form"  enctype="multipart/form-data" onsubmit="add_sub_category(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="sub_category_name" id="sub_category_name" maxlength="30">
                                <label>Section Name<sup>*</sup></label>
                            </div>
			    <span class="text-right pull-right" ><span class="sub_category_name_length">0</span><span>/30</span></span>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="sub_category_desc" id="sub_category_desc" ></textarea>
                                <label>Section Description</label>
                            </div>
			    
			    
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12 icon_upload_class_image" name="sub_category_file" id="sub_category_file">
				    <label>Upload Icon (Optional)</label>
				 <span><em>Preffered Size 48 * 48 px, PNG or JPG</em></span>     
			      </div>
			   </div>
                    </div>
		    <center><p id="create_sub_category_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </form>
                </div>
            </div>
        </div>
        <!----  Sub Section Modal end ----->
	
	<!----  Upload content model start ----->	
        <div class="modal fade" id="upload_content_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Image, Audio Or Video</h4>
                    </div>
		      <form id = "upload_content_modal_form"  enctype="multipart/form-data" onsubmit="add_content(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category" id = "is_main_category">
                                <input type="text" name="content_title" id="content_title">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield">
				 <input style="cursor:pointer;" type="file" class="col-sm-12" name="content_file" id="content_file">
				 <span><em>Images, Videos, Audio content supported</em></span>     
			      </div>
			   </div>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="content_desc" id="content_desc"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error" style="color:red"></p></center>
		    
		    
		    <div class="mui-textfield" style="padding:0px; margin-bottom: 30px">
			<div class="progress progress_overlay hide" >
			      <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
			</div>
		     </div>
		    
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	<!----  Upload content model start ----->	
        <div class="modal fade" id="upload_content_modal_pdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Document To View</h4>
                    </div>
		      <form id="upload_content_modal_pdf_form"  enctype="multipart/form-data" onsubmit="add_content_pdf(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_pdf" id = "is_main_category_pdf">
                                <input type="text" name="content_title_pdf" id="content_title_pdf">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield">
				 <input style="cursor:pointer;" type="file" class="col-sm-12" name="content_file_pdf" id="content_file_pdf" accept="application/pdf" >
				 <span><em>Only PDF docs supported</em></span>     
			      </div>
			   </div>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="content_desc_pdf" id="content_desc_pdf"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error_pdf" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Text Content</h4>
                    </div>
		      <form id="upload_content_modal_text_form"  enctype="multipart/form-data" onsubmit="add_content_text(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_text" id = "is_main_category_text">
                                <input type="text" name="content_title_text" id="content_title_text">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			   
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="content_desc_text" id="content_desc_text"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error_text" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">User Upload Photo Or Video</h4>
                    </div>
		      <form id="upload_content_modal_upload_form"  enctype="multipart/form-data" onsubmit="add_content_upload(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_upload" id = "is_main_category_upload">
                                <input type="text" name="content_title_upload" id="content_title_upload">
                                <label> Title<sup>*</sup></label>
                            </div>
			   
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="content_desc_text" id="content_desc_upload"></textarea>
                                <label>Instructions for the user (Option)</label>
                            </div>
			    
			   <div class="form-group os-form-group" style="margin-top:10px; color:#000">
			      <label class="checkbox-inline" style="padding-left:25px">
			      <input name="upload_content_user_image_allow" value="1" checked="checked" type="checkbox" class="openmodal"> Allow user to upload Photos
			      </label>
			   </div>
			   <div class="form-group os-form-group" style="margin-top:10px; color:#000">
			      <label class="checkbox-inline" style="padding-left:25px">
			      <input name="upload_content_user_video_allow" value="1" type="checkbox" class="openmodal" checked="checked"> Allow user to upload Videos
			      </label>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_upload" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_poll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Poll</h4>
                    </div>
		      <form  id="add_content_poll"  enctype="multipart/form-data" onsubmit="add_content_poll(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_poll" id = "is_main_category_poll">
                                <input type="text" name="content_title_poll" id="content_title_poll">
                                <label>Poll Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="pole_file" id="pole_file">
				    <label>Poll Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 6 Options)</small></h2>
			   </div>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="poll_question" id="poll_question"></textarea>
                                <label>Poll Question<sup>*</sup></label>
                            </div>
			    
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_one" id="poll_option_one">
                                <label>Option A<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_two" id="poll_option_two">
                                <label>Option B<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_three" id="poll_option_three">
                                <label>Option C</label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_four" id="poll_option_four">
                                <label>Option D</label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_five" id="poll_option_five">
                                <label>Option E</label>
                            </div>
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="poll_option_six" id="poll_option_six">
                                <label>Option F</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error_poll" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_poll_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit A Poll</h4>
                    </div>
		      <form id="update_content_poll"  enctype="multipart/form-data" onsubmit="update_content_poll(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				  <input type="hidden" name = "edit_content_id_poll" id = "edit_content_id_poll">
				       <input type="hidden" name = "edit_content_id_main_category_poll" id = "edit_content_id_main_category_poll">
				 <input type="hidden" name = "edit_content_id_sub_category_poll" id = "edit_content_id_sub_category_poll">
                    
                                <input type="text" name="content_title_poll_edit" id="content_title_poll_edit">
                                <label>Poll Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="pole_file_edit" id="pole_file_edit">
				    <label>Poll Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="poll_edit_file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="poll_edit_file_previous_name"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="poll_edit_file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div>
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 6 Options)</small></h2>
			   </div>
                            <div class="mui-textfield ">
                                <textarea name="poll_question_edit" id="poll_question_edit"></textarea>
                                <label>Poll Question<sup>*</sup></label>
                            </div>
			    
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_one_edit" id="poll_option_one_edit">
                                <label>Option A<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_two_edit" id="poll_option_two_edit">
                                <label>Option B<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_three_edit" id="poll_option_three_edit">
                                <label>Option C</label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_four_edit" id="poll_option_four_edit">
                                <label>Option D</label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_five_edit" id="poll_option_five_edit">
                                <label>Option E</label>
                            </div>
			    <div class="mui-textfield ">
                                <input type="text" name="poll_option_six_edit" id="poll_option_six_edit">
                                <label>Option F</label>
                            </div>
                       
                    </div>
		    <center><p id="create_content_error_poll_edit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_survey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Survey</h4>
                    </div>
		      <form  id="add_content_survey"  enctype="multipart/form-data" onsubmit="add_content_survey(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_survey" id = "is_main_category_survey">
                                <input type="text" name="content_title_survey" id="content_title_survey">
                                <label>Survey Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="survey_file" id="survey_file">
				    <label>Survey Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			   
                           <div id="survey_question_main_div">
			      <input type="hidden" name="total_survey_question_created" id="total_survey_question_created" value = "1">
			      <div id="survey_question_div">
				 <div class="survey_div">
				    <div class="form-group">
				       <label style="color:#f00f64">QUESTION 01</label>
				       <select class="form-control survey_type_class" name="survey_type[]">
					  <option value="1">Text Entry</option>
					  <option value="2">Single Choice</option>
					  <option value="3">Multi Coice</option>
				       </select>
				    </div>
				    <div class="mui-textfield mui-textfield--float-label">
				       <textarea name="survey_question[]" class = "survey_question_class"></textarea>
				       <label>Question<sup>*</sup></label>
				    </div>
				    
				 </div>
				 
			      </div>
			      <a class = "add_new_question" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Question</a>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_survey" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_survey_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit A Survey</h4>
                    </div>
		      <form id="update_content_survey"  enctype="multipart/form-data" onsubmit="update_content_survey(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				  <input type="hidden" name = "edit_content_id_survey" id = "edit_content_id_survey">
				       <input type="hidden" name = "edit_content_id_main_category_survey" id = "edit_content_id_main_category_survey">
				 <input type="hidden" name = "edit_content_id_sub_category_survey" id = "edit_content_id_sub_category_survey">
                    
                                <input type="text" name="content_title_survey_edit" id="content_title_survey_edit">
                                <label>Survey Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="survey_file_edit" id="survey_file_edit">
				    <label>Survey Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:5px" id="survey_edit_file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="survey_edit_file_previous_name"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="survey_edit_file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div>
                           <div id="survey_question_main_div_edit">
			      <input type="hidden" name="total_survey_question_created_edit" id="total_survey_question_created_edit" value = "1">
			      <div id="survey_question_div_edit">
				 
				 
			      </div>
			      <a class = "add_new_question_edit" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Question</a>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_survey_edit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	<!----  Main Section Modal start ----->	
        <div class="modal fade" id="main_category_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Main Section</h4>
                    </div>
		    <form id="edit_new_main_section_form" enctype="multipart/form-data" onsubmit="update_main_category(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield">
				 <input type="hidden" name="main_category_id_edit" id="main_category_id_edit">
                                <input type="text" name="main_category_name_edit" id="main_category_name_edit" maxlength="30">
                                <label>Section Name<sup>*</sup></label>
                            </div>
			    <span class="text-right pull-right" ><span class="main_category_name_length">0</span><span>/30</span></span>
                            <div class="mui-textfield">
                                <textarea name="main_category_desc_edit" id="main_category_desc_edit" ></textarea>
                                <label>Section Description</label>
                            </div>
			    
			    <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12 icon_upload_class_image" name="main_category_file_edit" id="main_category_file_edit">
				    <label>Upload Icon (Optional)</label>
				 <span><em>Preffered Size 48 * 48 px, PNG or JPG</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="maine_category_edit_file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="main_category_edit_file_previous_name"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="main_category_edit_file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div>
			    
			    <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group os-form-group">
                                        <label class="checkbox-inline">
                                            <input name="compliance_mandatory_edit" value="1" type="checkbox" class="openmodal"> Compliance Mandatory
                                        </label>
                                    </div>
                                </div>
                            </div>
			     
                    </div>
		    <center><p id="update_main_category_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </form>
                </div>
            </div>
        </div>
        <!----  Main Section Modal end ----->
	<!----  Sub Section Modal start ----->	
        <div class="modal fade" id="sub_category_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Sub Section</h4>
                    </div>
		    <form id="edit_new_sub_section_form"  enctype="multipart/form-data" onsubmit="update_sub_category(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield">
			      <input type="hidden" name="sub_category_id_edit_main_category" id="sub_category_id_edit_main_category">
			      <input type="hidden" name="sub_category_id_edit" id="sub_category_id_edit">
                                <input type="text" name="sub_category_name_edit" id="sub_category_name_edit" maxlength="30">
                                <label>Section Name<sup>*</sup></label>
                            </div>
			    <span class="text-right pull-right" ><span class="sub_category_name_length">0</span><span>/30</span></span>
                            <div class="mui-textfield">
                                <textarea name="sub_category_desc_edit" id="sub_category_desc_edit" ></textarea>
                                <label>Section Description</label>
                            </div>
			    
			    
			    
			     <div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12 icon_upload_class_image" name="sub_category_file_edit" id="sub_category_file_edit">
				    <label>Upload Icon (Optional)</label>
				 <span><em>Preffered Size 48 * 48 px, PNG or JPG</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="sub_category_edit_file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="sub_category_edit_file_previous_name"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="sub_category_edit_file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div>
			    
                    </div>
		    <center><p id="update_sub_category_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </form>
                </div>
            </div>
        </div>
        <!----  Sub Section Modal end ----->
	<!----  Upload content model start ----->
	        <div class="modal fade" id="upload_content_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:8%">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Image, Audio Or Video</h4>
                    </div>
		      <form id="edit_image_video_content_form"   enctype="multipart/form-data" onsubmit="update_content(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id" id = "edit_content_id">
				       <input type="hidden" name = "edit_content_id_main_category" id = "edit_content_id_main_category">
				 <input type="hidden" name = "edit_content_id_sub_category" id = "edit_content_id_sub_category">
                                <input type="text" name="content_title_edit" id="content_title_edit">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:10px">
			      <div class="mui-textfield">
				 <input style="cursor:pointer;" type="file" class="col-sm-12" name="content_file_edit" id="content_file_edit">
				 <span><em>Images, Videos, Audio content supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:5px" id="file_previous_name_div">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-9 col-xs-9">    
                                    <label  id="file_previous_name"></label>
                                 </div>
                                    
                                 <!--div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div-->
                                    
                              </div>
                              
                           </div>
                            <div class="mui-textfield">
                                <textarea name="content_desc_edit" id="content_desc_edit"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <p id="edit_content_error" style="color:red"></p>
		    
		     <div class="mui-textfield" style="padding:0px; margin-bottom: 30px">
			<div class="progress progress_overlay hide" >
			      <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
			</div>
		     </div>
		     
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>

        <!---- Upload content model end ----->
	<!----  Upload content model start ----->	
        <div class="modal fade" id="upload_content_modal_edit_pdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Document To View</h4>
                    </div>
		      <form id="edit_pdf_content_form"  enctype="multipart/form-data" onsubmit="update_content_pdf(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id_pdf" id = "edit_content_id_pdf">
				       <input type="hidden" name = "edit_content_id_main_category_pdf" id = "edit_content_id_main_category_pdf">
				 <input type="hidden" name = "edit_content_id_sub_category_pdf" id = "edit_content_id_sub_category_pdf">
                                <input type="text" name="content_title_edit_pdf" id="content_title_edit_pdf">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-form--inline" style="margin-bottom:10px">
			      <div class="mui-textfield">
				<input accept="application/pdf"  style="cursor:pointer;" type="file" class="col-sm-12" name="content_file_edit_pdf" id="content_file_edit_pdf">
				 <span><em>Only PDF docs supported</em></span>     
			      </div>
			   </div>
			    <div class="mui-form--inline" style="margin-bottom:5px" id="file_previous_name_div_pdf">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-9 col-xs-9">    
                                    <label  id="file_previous_name_pdf"></label>
                                 </div>
                                    
                                 <!--div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="file_previous_name_remove_button">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div-->
                                    
                              </div>
                              
                           </div>
                            <div class="mui-textfield ">
                                <textarea name="content_desc_edit_pdf" id="content_desc_edit_pdf"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		    <p id="edit_content_error_pdf" style="color:red"></p>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	<!----  Upload content model start ----->
	 <div class="modal fade" id="upload_content_modal_edit_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Text Content</h4>
                    </div>
		      <form  enctype="multipart/form-data" onsubmit="update_content_text(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id_text" id = "edit_content_id_text">
				       <input type="hidden" name = "edit_content_id_main_category_text" id = "edit_content_id_main_category_text">
				 <input type="hidden" name = "edit_content_id_sub_category_text" id = "edit_content_id_sub_category_text">
                                <input type="text" name="content_title_edit_text" id="content_title_edit_text">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			   
                            <div class="mui-textfield ">
                                <textarea name="content_desc_edit_text" id="content_desc_edit_text"></textarea>
                                <label>Content Description (Option)</label>
                            </div>
                       
                    </div>
		     <center><p id="edit_content_error_text" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>

        <!---- Upload content model end ----->
	
	<!----  Upload content model start ----->
	 <div class="modal fade" id="upload_content_modal_edit_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit User Upload Photo Or video</h4>
                    </div>
		      <form  enctype="multipart/form-data" onsubmit="update_content_upload(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id_upload" id = "edit_content_id_upload">
				       <input type="hidden" name = "edit_content_id_main_category_upload" id = "edit_content_id_main_category_upload">
				 <input type="hidden" name = "edit_content_id_sub_category_upload" id = "edit_content_id_sub_category_upload">
                                <input type="text" name="content_title_edit_upload" id="content_title_edit_upload">
                                <label> Title<sup>*</sup></label>
                            </div>
			   
                            <div class="mui-textfield ">
                                <textarea name="content_desc_edit_upload" id="content_desc_edit_upload"></textarea>
                                <label>Instruction for the user (Option)</label>
                            </div>
			    
			   <div class="form-group os-form-group" style="margin-top:10px; color:#000">
			      <label class="checkbox-inline" style="padding-left:25px">
			      <input name="upload_content_user_image_allow_edit" value="1"  type="checkbox" class="openmodal"> Allow user to upload Photos
			      </label>
			   </div>
			   <div class="form-group os-form-group" style="margin-top:10px; color:#000">
			      <label class="checkbox-inline" style="padding-left:25px">
			      <input name="upload_content_user_video_allow_edit" value="1" type="checkbox" class="openmodal" > Allow user to upload Videos
			      </label>
			   </div>
                       
                    </div>
		     <center><p id="edit_content_error_upload" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>

        <!---- Upload content model end ----->
	
	<!----  Upload content type model start ----->	
        <div class="modal modal-container fade" id="choose_content_type_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Select Content Type To Add</h4>
                    </div>
		      <div class="modal-body" style="padding:5px 15px 10px">
                       
			   <div class="row"> 
			   <div class="col-sm-12 not_retail_audit_content_type text_only_radio" style="margin:5px 0px;">
			      <input type="hidden" name = "choose_content_type_is_main_category" id = "choose_content_type_is_main_category">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="text" checked="checked"> Text Only
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type image_video_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="image_audio_video"> Image, Audio or Video With Text
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type document_view_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="pdf"> Document Viewer (PDF Only)
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type upload_photo_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="upload"> Upload Photo or Video by user
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type poll_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="poll"> Create A Poll Question (Single Question)
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type survey_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="survey"> Create A Survey (Multiple Question)
			      </label> 
			   </div>
			   <div class="col-sm-12 not_retail_audit_content_type wiki_only_radio" style="margin:5px 0px;">
			      <label class="radio-inline" style="font-weight:600;color:#808080">
				 <input type="radio" name="content_type_radio"  value="wiki"> Wikipedia
			      </label> 
			   </div>
			    <div class="col-sm-12 retail_audit_content_type" style="margin:5px 0px;">
				<label class="radio-inline" style="font-weight:600;color:#808080">
				   <input type="radio" name="content_type_radio"  value="survey_retail_audit"> Add Audit Survey
				</label> 
			    </div>
			</div>
                       
                    </div>
		    
                    <div class="modal-footer" style="text-align: right; padding-top:5px;">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="button" class="mui-btn mui-btn--os-accent"  onclick="choose_content_type_next()">NEXT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
                </div>
            </div>
        </div>
        <!---- Upload content model end ----->
	
	
	
	
	<!----  Upload text content model start ----->	
        <div class="modal fade" id="upload_content_modal_wiki" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Wikipedia Content</h4>
                    </div>
		      <form id="upload_content_modal_wiki_form"  enctype="multipart/form-data" onsubmit="add_content_wiki(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_wiki" id = "is_main_category_wiki">
                                <input type="text" name="content_title_wiki" id="content_title_wiki">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			   <div class="mui-textfield mui-textfield--float-label">
				 <input type="text" name="wiki_title_url" id="wiki_title_url">
                                <label>Wikipedia Title/URL<sup>*</sup></label>
                            </div>
                            
                       
                    </div>
		    <center><p id="create_content_error_wiki" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >DONE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload text content model end ----->
	
	<!----  Upload content model start ----->
	 <div class="modal fade" id="upload_content_modal_edit_wiki" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit Wikipedia Content</h4>
			<p>
			   
			   
			</p>
                    </div>
		      <form  enctype="multipart/form-data" onsubmit="update_content_wiki(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				   <input type="hidden" name = "edit_content_id_wiki" id = "edit_content_id_wiki">
				       <input type="hidden" name = "edit_content_id_main_category_wiki" id = "edit_content_id_main_category_wiki">
				 <input type="hidden" name = "edit_content_id_sub_category_wiki" id = "edit_content_id_sub_category_wiki">
                                <input type="text" name="content_title_edit_wiki" id="content_title_edit_wiki">
                                <label>Content Title<sup>*</sup></label>
                            </div>
			    <div class="mui-textfield ">
				  <input type="text" name="wiki_title_url_edit" id="wiki_title_url_edit">
                                <label>Wikipedia Title/URL<sup>*</sup></label>
                            </div>
			   
                            
                       
                    </div>
		     <center><p id="edit_content_error_wiki" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>

        <!---- Upload content model end ----->
	
	
	
	
	<!----  Upload contestifi_contest model start ----->	
        <div class="modal fade" id="add_contestifi_quiz_qustion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Quiz</h4>
                    </div>
		      <form  id="add_contenstifi_quiz_question"  enctype="multipart/form-data" onsubmit="add_contestifi_question(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   <div id="contestifi_contest_image_contest_div" style="margin-bottom:15px" class="mui-form--inline">
			      <div class="mui-textfield">
				 <input type="file" id="contestifi_content_file" name="contestifi_content_file" class="col-sm-12 mui--is-empty mui--is-untouched mui--is-pristine" style="cursor:pointer;">
				 <span><em>Images, Videos, Audio content supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 4 Options)</small></h2>
			   </div>
                            <div class="mui-textfield mui-textfield--float-label">
                                <textarea name="quiz_question" id="quiz_question"></textarea>
                                <label>Quiz Question<sup>*</sup></label>
                            </div>
			    
			   <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="quiz_option_one" id="quiz_option_one">
				       <label>Option A<sup>*</sup></label>
				   </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans" value="1" type="radio" checked> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="quiz_option_two" id="quiz_option_two">
                                <label>Option B<sup>*</sup></label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans" value="2" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				   <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="quiz_option_three" id="quiz_option_three">
                                <label>Option C</label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans" value="3" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				   <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="quiz_option_four" id="quiz_option_four">
                                <label>Option D</label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans" value="4" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    
			    
			    
			   
                       
                    </div>
		    <center><p id="create_contestifi_quiz_error" style="color:red"></p></center>
		    <div class="mui-textfield" style="padding:0px; margin-bottom: 30px">
			<div class="progress progress_overlay hide" >
			      <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
			</div>
		     </div>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest model end ----->
	
	<!----  Upload contestifi_contest model start ----->	
        <div class="modal fade" id="update_contestifi_quiz_qustion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Update A Quiz</h4>
                    </div>
		      <form  id="update_contenstifi_quiz_question"  enctype="multipart/form-data" onsubmit="update_contestifi_question(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   <div id="contestifi_contest_image_contest_div_edit" style="margin-bottom:15px" class="mui-form--inline">
			      <div class="mui-textfield">
				 <input type="file" id="contestifi_content_file_edit" name="contestifi_content_file_edit" class="col-sm-12 mui--is-empty mui--is-untouched mui--is-pristine" style="cursor:pointer;">
				 <span><em>Images, Videos, Audio content supported</em></span>     
			      </div>
			   </div>
			   <div class="row" id="contestifi_content_file_edit_preview_div">
			      <div class="col-sm-8 col-xs-8">
				    <label>Previous File:- <span id="contestifi_content_file_edit_preview"></span></label>
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 4 Options)</small></h2>
			   </div>
                            <div class="mui-textfield ">
			      <input type="hidden" name="edit_contestifi_quiz_question_id" id="edit_contestifi_quiz_question_id">
                                <textarea name="quiz_question_edit" id="quiz_question_edit"></textarea>
                                <label>Quiz Question<sup>*</sup></label>
                            </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield ">
				    <input type="text" name="quiz_option_one_edit" id="quiz_option_one_edit">
				    <label>Option A<sup>*</sup></label>
				</div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans_edit" value="1" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield">
                                <input type="text" name="quiz_option_two_edit" id="quiz_option_two_edit">
                                <label>Option B<sup>*</sup></label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans_edit" value="2" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield">
                                <input type="text" name="quiz_option_three_edit" id="quiz_option_three_edit">
                                <label>Option C</label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans_edit" value="3" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    <div class="row">
			      
				 
				 <div class="col-sm-8 col-xs-8">
				    <div class="mui-textfield">
                                <input type="text" name="quiz_option_four_edit" id="quiz_option_four_edit">
                                <label>Option D</label>
                            </div>
				 </div>
				 
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-radio">
				    <label class="radio-inline">
					   <input name="contest_quiz_right_ans_edit" value="4" type="radio"> Right Ans
				    </label>
				    </div>
				 </div>
			      
			   </div>
			    
			    
			    
			    
			   
                       
                    </div>
		    <center><p id="update_contestifi_quiz_error" style="color:red"></p></center>
		      <div class="mui-textfield" style="padding:0px; margin-bottom: 30px">
			<div class="progress progress_overlay hide" >
			      <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
			</div>
		     </div>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest model end ----->
	
	<!----  contestifi contest section start ----->	
        <div class="modal fade" id="contestifi_contest_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">New Contest</h4>
                    </div>
		    <form  id="add_new_contestifi_contest" enctype="multipart/form-data" onsubmit="add_contestifi_contest(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   <div class="col-sm-12 col-xs-12">
			      <div class="mui-select">
				 <select onchange="change_contest_type_drowdown()" name="contestifi_contest_type" id="contestifi_contest_type">
				    <option value="1">Time bound Quiz</option>
				    <option value="2">Image & Video Contest</option>
				    <option value="3">Survey</option>
				 </select>
				 <label>Contest type<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="text" name="contestifi_contest_name" id="contestifi_contest_name">
				  <label>Contest Name<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="col-sm-6 col-xs-6">
							       <label class="os-lable">Contest Logo</label>
							       <div class="mui-textfield" style="padding-top:5px;">
								  <div class="modal-dropzone dropzone" data-width="900" data-ajax="false"  data-originalsize="false" data-height="300" style="width: 225px; height:75px;">
                                                                     <input type="file" name="contestifi_logo_image"  />
                                                                  </div>
							       </div>
				 </div>
				 
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 
				 <!--div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="color" style="border: none" name="contestifi_contest_bg_color" id="contestifi_contest_bg_color" value="#ffffff">
				       <label>BG Color <sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="color" style="border: none" name="contestifi_contest_text_color" id="contestifi_contest_text_color" value="#000000">
				       <label>Text Color <sup>*</sup></label>
				    </div>
				 </div-->
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="color" style="border: none" name="contestifi_contest_button_color" id="contestifi_contest_button_color" value="#007cb2">
				       <label>Button Color <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="col-sm-6 col-xs-6">
				    <div class="mui-textfield ">
				       <input type="text" class="start_date" name="start_date" >
				       <label>Start Date<sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-sm-6 col-xs-6">
				    <div class="mui-textfield ">
				       <input type="text" class="end_date"  name="end_date">
				       <label>End Date <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div class="row only_time_based_quiz">
			      <div class="col-sm-12 col-xs-12">
				 <div class="form-group">
				    <div class="col-sm-12 col-xs-12"><label class="os-lable" style="font-weight:700">Contest Frequency</label></div>
				    <div class="col-sm-12 col-xs-12">
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency" value="1" type="radio" checked> Daily
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency" value="2" type="radio"> Every 2 hours
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency" value="3" type="radio"> Every 4 Hours
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency" value="4" type="radio"> Custom
					   </label>
				    </div>	       
				 </div>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12" style="display:none" id="contestifi_custom_frequecny_div">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_custom_frequecny" id="contestifi_custom_frequecny">
				  <label>Custom Frequency(In hr)<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_no_of_attempt">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_no_of_attemption" id="contestifi_no_of_attemption">
				  <label>Number of attempts<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_point_per_question" id="contestifi_point_per_question">
				  <label>Point Per Question<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_negative_point" id="contestifi_negative_point" value="0">
				  <label>Negative Point<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_point_deduct_every_secod" id="contestifi_point_deduct_every_secod" value="0">
				  <label>Points deducted for every second<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
			      <div class="mui-textfield mui-textfield--float-label">
				  <input type="number" name="contestifi_max_time_allocation" id="contestifi_max_time_allocation" value="0">
				  <label>Maximum time allocation (In second)<sup>*</sup></label>
			      </div>
			   </div>
			   <div id="contestifi_contest_rule_div">
			      <div class="col-sm-12 col-xs-12">
				 <label class="os-lable" style="font-weight:700">Contest Rules</label>
			      </div>
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="contestifi_contet_rule[]">
				       <label>Rule<sup>*</sup></label>
				    </div>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz">
				 <a class = "contestifi_contest_add_another_rule" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Rule</a>
			   </div>
			   
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group os-form-group">
                                        <label class="checkbox-inline">
                                            <input name="contest_compliance_mandatory" value="1" type="checkbox" class="openmodal"> Compliance Mandatory
                                        </label>
                                    </div>
                                </div>
                           
		    <center><p id="create_contestifi_contest_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">CREATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </div>
		    </form>
                </div>
            </div>
        </div>
        <!---- contestifi contest section  end ----->
	
	
	
	<!----  contestifi contest update section start ----->	
        <div class="modal fade" id="contestifi_contest_update_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Update Contest</h4>
                    </div>
		    <form  id="update_contestifi_contest" enctype="multipart/form-data" onsubmit="update_contestifi_contest(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   <div class="col-sm-12 col-xs-12">
			      <div class="mui-select">
				 <input type="hidden" name="contestifi_contest_id_edit" id="contestifi_contest_id_edit">
				 <select name="contestifi_contest_type_edit" id="contestifi_contest_type_edit">
				    
				 </select>
				 <label>Contest type<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12">
			      <div class="mui-textfield ">
				  <input type="text" name="contestifi_contest_name_edit" id="contestifi_contest_name_edit">
				  <label>Contest Name<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="col-sm-6 col-xs-6">
							       <label class="os-lable">Contest Logo</label>
							       <div class="mui-textfield" style="padding-top:5px;">
								  <div class="modal-dropzone dropzone" data-width="900" data-ajax="false"  data-originalsize="false" data-height="300" style="width: 225px; height:75px;">
                                                                     <input type="file" name="contestifi_logo_image_edit"  />
                                                                  </div>
							       </div>
				 </div>
				 <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <label class="os-lable">&nbsp;</label>
                                       <div class="mui-textfield" style="padding-top:5px;width: 225px; height:75px; overflow: hidden;">
                                          <img onerror="this.onerror=null;this.src='<?php echo base_url()?>assets/images/default.png';" id="contest_logo_preview" class="profile-user-img img-responsive" src="" style =" width:100%;">
                                       </div>
                                 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <!--div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield">
				       <input type="color" style="border: none" name="contestifi_contest_bg_color_edit" id="contestifi_contest_bg_color_edit">
				       <label>BG Color <sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield">
				       <input type="color" style="border: none" name="contestifi_contest_text_color_edit" id="contestifi_contest_text_color_edit">
				       <label>Text Color <sup>*</sup></label>
				    </div>
				 </div-->
				 <div class="col-sm-4 col-xs-4">
				    <div class="mui-textfield">
				       <input type="color" style="border: none" name="contestifi_contest_button_color_edit" id="contestifi_contest_button_color_edit">
				       <label>Button Color <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="col-sm-6 col-xs-6">
				    <div class="mui-textfield ">
				       <input type="text" class="start_date_edit" name="start_date_edit" >
				       <label>Start Date<sup>*</sup></label>
				    </div>
				 </div>
				 <div class="col-sm-6 col-xs-6">
				    <div class="mui-textfield ">
				       <input type="text" class="end_date_edit"  name="end_date_edit">
				       <label>End Date <sup>*</sup></label>
				    </div>
				 </div>
			      </div>
			   </div>
			   <div class="row only_time_based_quiz_edit">
			      <div class="col-sm-12 col-xs-12">
				 <div class="form-group">
				    <div class="col-sm-12 col-xs-12"><label class="os-lable" style="font-weight:700">Contest Frequency</label></div>
				    <div class="col-sm-12 col-xs-12">
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency_edit" value="1" type="radio"> Daily
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency_edit" value="2" type="radio"> Every 2 hours
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency_edit" value="3" type="radio"> Every 4 Hours
					   </label>
					   <label class="radio-inline" style="margin-right:10px">
					   <input name="contest_frequency_edit" value="4" type="radio"> Custom
					   </label>
				    </div>	       
				 </div>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12" style="display:none" id="contestifi_custom_frequecny_div_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_custom_frequecny" id="contestifi_custom_frequecny_edit">
				  <label>Custom Frequency(In hr)<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit_no_of_attempt">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_no_of_attemption_edit" id="contestifi_no_of_attemption_edit">
				  <label>Number of attempts<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_point_per_question_edit" id="contestifi_point_per_question_edit">
				  <label>Point Per Question<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_negative_point_edit" id="contestifi_negative_point_edit" value="0">
				  <label>Negative Point<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_point_deduct_every_secod_edit" id="contestifi_point_deduct_every_secod_edit" value="0">
				  <label>Points deducted for every second<sup>*</sup></label>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
			      <div class="mui-textfield">
				  <input type="number" name="contestifi_max_time_allocation_edit" id="contestifi_max_time_allocation_edit" value="0">
				  <label>Maximum time allocation (In second)<sup>*</sup></label>
			      </div>
			   </div>
			   <div id="contestifi_contest_rule_div_edit">
			      <div class="col-sm-12 col-xs-12">
				 <label class="os-lable" style="font-weight:700">Contest Rules</label>
			      </div>
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-textfield">
				       <input type="text" class="cotenst_first_rule_edit" name="contestifi_contet_rule_edit[]">
				       <label>Rule<sup>*</sup></label>
				    </div>
			      </div>
			   </div>
			   <div class="col-sm-12 col-xs-12 only_time_based_quiz_edit">
				 <a class = "contestifi_contest_add_another_rule_edit" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Rule</a>
			   </div>
			   
			   
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group os-form-group">
                                        <label class="checkbox-inline">
                                            <input name="contest_compliance_mandatory_edit" value="1" type="checkbox" class="openmodal"> Compliance Mandatory
                                        </label>
                                    </div>
                                </div>
                            
		    <center><p id="create_contestifi_contest_error_edit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent">UPDATE<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		    </div>
		    </form>
                </div>
            </div>
        </div>
        <!---- contestifi contest update section  end ----->
	
	
	<!----  Upload contestifi_contest prize model start ----->	
        <div class="modal fade" id="add_contestifi_contest_prize_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" >
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Prize</h4>
                    </div>
		      <form  id="add_contenstifi_contest_prize"  enctype="multipart/form-data" onsubmit="add_contestifi_prize(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       			    
			    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="contestifi_contest_prize" id="contestifi_contest_prize">
                                <label>Contest Prize<sup>*</sup></label>
                            </div>
			    
                    </div>
		    <center><p id="create_contestifi_prize_error" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest prize model end ----->
	
	<!----  Upload contestifi_contest prize model start ----->	
        <div class="modal fade" id="add_contestifi_contest_prize_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" >
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Update Prize</h4>
                    </div>
		      <form  id="add_contenstifi_contest_prize_edit"  enctype="multipart/form-data" onsubmit="update_contestifi_prize(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       			    
			    <div class="mui-textfield">
			      <input type="hidden" name="contestifi_contest_prize_id_edit" id="contestifi_contest_prize_id_edit">
                                <input type="text" name="contestifi_contest_prize_edit" id="contestifi_contest_prize_edit">
                                <label>Contest Prize<sup>*</sup></label>
                            </div>
			    
                    </div>
		    <center><p id="create_contestifi_prize_error_edit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest prize model end ----->
	
	
	
	<!----  Upload contestifi_contest prize model start ----->	
        <div class="modal fade" id="add_contestifi_contest_terms_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog " style="margin-top:5%" role="document" >
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Add Sign up Term & Condition</h4>
                    </div>
		      <form  id="add_contenstifi_contest_terms_condition"  enctype="multipart/form-data" onsubmit="add_contestifi_terms_conditon(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       			    
			    <div class="mui-textfield">
			        <!--input type="text" name="contestifi_term_conditon" id="contestifi_term_conditon"-->
				 <textarea id="contestifi_term_condition" name="contestifi_term_condition"></textarea>
                                <label>Term Condition<sup>*</sup></label>
                            </div>
			    
                    </div>
		    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest prize model end ----->
	
	
	
	
	<!----  Upload contestifi_contest model start ----->	
        <div class="modal fade" id="add_contestifi_survey_qustion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Survey</h4>
                    </div>
		      <form  id="add_contenstifi_survey_question"  enctype="multipart/form-data" onsubmit="add_contestifi_survey(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 4 Options)</small></h2>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-select">
				    <select name="contestifi_contest_survey_type" id="contestifi_contest_survey_type">
				       <option value="1">Single Choice</option>
				       <option value="2">Multi Choice</option>
				    </select>
				    <label>Answer type<sup>*</sup></label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-textfield mui-textfield--float-label">
				     <textarea name="survey_question" id="survey_question"></textarea>
				     <label>Survey Question<sup>*</sup></label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				    <div class="mui-textfield mui-textfield--float-label">
				       <input type="text" name="survey_option_one" id="survey_option_one">
				       <label>Option A<sup>*</sup></label>
				   </div>
				 </div>
			      
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				    <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="survey_option_two" id="survey_option_two">
                                <label>Option B<sup>*</sup></label>
                            </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				   <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="survey_option_three" id="survey_option_three">
                                <label>Option C</label>
                            </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				   <div class="mui-textfield mui-textfield--float-label">
                                <input type="text" name="survey_option_four" id="survey_option_four">
                                <label>Option D</label>
                            </div>
				 </div>
				 
			      
			   </div>
			    
			    
			    
			    
			   
                       
                    </div>
		    <center><p id="create_contestifi_survey_error" style="color:red"></p></center>
		    
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest model end ----->
	
	
	<!----  Upload contestifi_contest model start ----->	
        <div class="modal fade" id="update_contestifi_survey_qustion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Survey</h4>
                    </div>
		      <form  id="update_contenstifi_survey_question"  enctype="multipart/form-data" onsubmit="update_contestifi_survey(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
			   
			   <div class="mui-form--inline" style="margin-bottom:0px">
			     <h2 style="color: #414042;font-size:16px; font-weight:400; ">ANSWER OPTIONS <small>(Upto 4 Options)</small></h2>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-select">
				    <select name="contestifi_contest_survey_type_edit" id="contestifi_contest_survey_type_edit">
				       <option value="1">Single Choice</option>
				       <option value="2">Multi Choice</option>
				    </select>
				    <label>Answer type<sup>*</sup></label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      <div class="col-sm-12 col-xs-12">
				 <div class="mui-textfield ">
				    <input type="hidden" name="contestifi_survey_question_id_hidden" id="contestifi_survey_question_id_hidden">
				     <textarea name="survey_question_edit" id="survey_question_edit"></textarea>
				     <label>Survey Question<sup>*</sup></label>
				 </div>
			      </div>
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				    <div class="mui-textfield">
				       <input type="text" name="survey_option_one_edit" id="survey_option_one_edit">
				       <label>Option A<sup>*</sup></label>
				   </div>
				 </div>
			      
			   </div>
			   <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				    <div class="mui-textfield">
                                <input type="text" name="survey_option_two_edit" id="survey_option_two_edit">
                                <label>Option B<sup>*</sup></label>
                            </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				   <div class="mui-textfield ">
                                <input type="text" name="survey_option_three_edit" id="survey_option_three_edit">
                                <label>Option C</label>
                            </div>
				 </div>
			      
			   </div>
			    <div class="row">
			      
				 
				 <div class="col-sm-12 col-xs-12">
				   <div class="mui-textfield ">
                                <input type="text" name="survey_option_four_edit" id="survey_option_four_edit">
                                <label>Option D</label>
                            </div>
				 </div>
				 
			      
			   </div>
			    
			    
			    
			    
			   
                       
                    </div>
		    <center><p id="create_contestifi_survey_error_edit" style="color:red"></p></center>
		    
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload contestifi_contest model end ----->
	
	
	
	<!----  Upload audit survey start ----->	
        <div class="modal fade" id="upload_content_modal_survey_retail_audit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Create A Survey</h4>
                    </div>
		      <form  id="add_content_survey_retail_audit"  enctype="multipart/form-data" onsubmit="add_content_survey_retail_audit(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield mui-textfield--float-label">
				 <input type="hidden" name = "is_main_category_survey_retail_audit" id = "is_main_category_survey_retail_audit">
                                <input type="text" name="content_title_survey_retail_audit" id="content_title_survey_retail_audit">
                                <label>Add Audit Element Title <sup>*</sup></label>
                            </div>
			    <!--div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="survey_file_retail_audit" id="survey_file_retail_audit">
				    <label>Survey Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div-->
			   
                           <div id="survey_question_main_div_retail_audit">
			      <input type="hidden" name="total_survey_question_created_retail_audit" id="total_survey_question_created_retail_audit" value = "1">
			      <div id="survey_question_div_retail_audit">
				 <div class="survey_div_retail_audit">
				    <div class="form-group">
				       <label style="color:#f00f64">QUESTION 01</label>
				       <label style="padding-left:20%;">Deduct: <input type="text"  class= "health_deduct_retail_audit" name="health_deduct_retail_audit[]" style="width:50px;"> % from store health</label>
				       <!--select class="form-control survey_type_class_retail_audit" name="survey_type_retail_audit[]">
					   <option value="0">Select Survey Type</option>
					  <option value="2">Single Choice</option>
					  <option value="3">Multi Coice</option>
				       </select-->
				    </div>
				    <div class="mui-textfield mui-textfield--float-label">
				       <textarea name="survey_question_retail_audit[]" class = "survey_question_class_retail_audit"></textarea>
				       <label>Question<sup>*</sup></label>
				    </div>
				    <div class="form-group">
					<div class="checkbox" style="margin-top:0px">
					    <label>
					    <input  class= "use_sku_survey_retail_audit" name="use_sku_survey_retail_audit[]" value="1" type="checkbox"> User SKU List
					    </label>
				       </div>
				    </div>
				 </div>
				 
			      </div>
			      <a class = "add_new_question_retail_audit" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Question</a>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_survey_retail_audit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- Upload audit survey end ----->
	
	<!----  edit audit survey  start ----->	
        <div class="modal fade" id="upload_content_modal_survey_edit_retail_audit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-sm" role="document" style="margin-top:0%; width:500px">
                <div class="modal-content">
                    <div class="modal-header" style="padding:10px 15px">
                        <!--				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="os-modal-title">Edit A Survey</h4>
                    </div>
		      <form id="update_content_survey_retail_audit"  enctype="multipart/form-data" onsubmit="update_content_survey_retail_audit(); return false;">
                    <div class="modal-body" style="padding:10px 15px">
                       
                            <div class="mui-textfield ">
				  <input type="hidden" name = "edit_content_id_survey_retail_audit" id = "edit_content_id_survey_retail_audit">
				       <input type="hidden" name = "edit_content_id_main_category_survey_retail_audit" id = "edit_content_id_main_category_survey_retail_audit">
				 <input type="hidden" name = "edit_content_id_sub_category_survey_retail_audit" id = "edit_content_id_sub_category_survey_retail_audit">
                    
                                <input type="text" name="content_title_survey_edit_retail_audit" id="content_title_survey_edit_retail_audit">
                                <label>Add Audit Element Title<sup>*</sup></label>
                            </div>
			    <!--div class="mui-form--inline" style="margin-bottom:15px">
			      <div class="mui-textfield" style="width:100%">
				 <input accept="image/*" style="cursor:pointer;" type="file" class="col-sm-12" name="survey_file_edit_retail_audit" id="survey_file_edit_retail_audit">
				    <label>Survey Banner Image (Optional)</label>
				 <span><em>PNG, JPG, BMP supported</em></span>     
			      </div>
			   </div>
			   <div class="mui-form--inline" style="margin-bottom:5px" id="survey_edit_file_previous_name_div_retail_audit">
			      <div class="row">    
                                 <div class="col-sm-3 col-xs-3 nopadding-right">    
                                    <label>Previous file:-</label>
                                 </div>
                                    
                                 <div class="col-sm-7 col-xs-7">    
                                    <label  id="survey_edit_file_previous_name_retail_audit"></label>
                                 </div>
                                    
                                 <div class="col-sm-2 col-xs-2 nopadding-left">    
                                    <label>
                                      <span class="mui-btn mui-btn--small mui-btn--danger" id="survey_edit_file_previous_name_remove_button_retail_audit">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </label>
                                 </div>
                                    
                              </div>
                              
                           </div-->
                           <div id="survey_question_main_div_edit_retail_audit">
			      <input type="hidden" name="total_survey_question_created_edit_retail_audit" id="total_survey_question_created_edit_retail_audit" value = "1">
			      <div id="survey_question_div_edit_retail_audit">
				 
				 
			      </div>
			      <a class = "add_new_question_edit_retail_audit" style=" font-size:14px; font-weight: 500; cursor:pointer"><i style="font-family:FontAwesome;" class="fal fa-plus-square"></i> Add Another Question</a>
			   </div>
                       
                    </div>
		    <center><p id="create_content_error_survey_edit_retail_audit" style="color:red"></p></center>
                    <div class="modal-footer" style="text-align: right">
                        <button type="button" class="mui-btn mui-btn--os-primary" data-dismiss="modal" >CANCEL<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                        <button type="submit" class="mui-btn mui-btn--os-accent" >SUBMIT<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                    </div>
		     </form>
                </div>
            </div>
        </div>
        <!---- edit audit survey end ----->
      
      <!---- edit audit survey end ----->
      
      
      
      
      <div class="modal fade" id="lms_content_location_list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" >
         <div class="modal-dialog" role="document" style="margin-top:5%">
            <div class="modal-content" style="min-height: 500px">
               <div class="modal-header" style="padding:10px 15px">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center><h4>Add Locations</h4></center>
               </div>
               <div class="modal-body" style="padding:10px 15px">
                  <div class="row">
                     
		     
		     
		     
		     <div class="col-sm-12" style="margin: 10px 0px">
			<label class="location_lable" style="font-weight:600;">Select Location To Add Or Remove From This content</label>
                        <div class="table-responsive modal-body3 tbodys">
			    <input type="hidden" name="lms_add_location_content_id" id="lms_add_location_content_id">
                           <table class="table table-striped" >
                              <tr class="table_active">
                                 <th>S.No</th>
				 <th>&nbsp;</th>
                                 <th>Location Name</th>
                              </tr>
			      <tbody id="lms_content_group_location_list" class="">
				 
			      </tbody>
                              
                           </table>
                        </div>
			<center>
			   <p style="color: red" id="lms_add_content_location_error"></p>
			 <a onclick="lms_add_content_location()" class="mui-btn mui-btn--small mui-btn--accent">add</a>
			</center>
                     </div>

			   
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      
      <div class="modal fade" id="lms_contest_location_list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" >
         <div class="modal-dialog" role="document" style="margin-top:5%">
            <div class="modal-content" style="min-height: 500px">
               <div class="modal-header" style="padding:10px 15px">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <center><h4>Add Locations</h4></center>
               </div>
               <div class="modal-body" style="padding:10px 15px">
                  <div class="row">
                     
		     
		     
		     
		     <div class="col-sm-12" style="margin: 10px 0px">
			<label class="location_lable" style="font-weight:600;">Select Location To Add Or Remove From This contest</label>
                        <div class="table-responsive modal-body3 tbodys">
			    <input type="hidden" name="lms_add_location_contest_id" id="lms_add_location_contest_id">
                           <table class="table table-striped" >
                              <tr class="table_active">
                                 <th>S.No</th>
				 <th>&nbsp;</th>
                                 <th>Location Name</th>
                              </tr>
			      <tbody id="lms_contest_group_location_list" class="">
				 
			      </tbody>
                              
                           </table>
                        </div>
			<center>
			   <p style="color: red" id="lms_add_contest_location_error"></p>
			 <a onclick="lms_add_contest_location()" class="mui-btn mui-btn--small mui-btn--accent">add</a>
			</center>
                     </div>

			   
                  </div>
               </div>
            </div>
         </div>
      </div>
	   
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/html5imageupload.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui.min.js"></script>
      
      <script>
         $(document).ready(function() {
	    $('.dropzone').html5imageupload();
            $(".loading").hide();
            content_builder();
         });
	
</script>
       
      
    <script>
	var content_array;
	var compliance_category;
	 // function to get all data related to main, sub category and content and also list main category
	function content_builder(created_id= '', created_subcategory_id = '') {
	    $(".loading").show();
	    var location_uid =  '';
	    $.ajax({
		type: "POST",
		url: "<?php echo base_url()?>location/lms_content_builder_list",
		data: "location_uid="+location_uid,
		dataType: 'json',
		success: function(data){
		    content_array = data;
		    $(".loading").hide();
		    if (data.main_category.length > 0) {
			$('.main_section_content_list').html('');
			for (var i = 0; i < data.main_category.length; i++) {
			   var main_category = data.main_category[i];
			   var main_class = '';
			   if (created_id == '') {
			      if (i == 0) {
				 main_class = 'offline_active';
				 list_subsection(main_category.id,created_subcategory_id);
			      }
			   }else{
			      if (main_category.id == created_id) {
				 main_class = 'offline_active';
				 list_subsection(main_category.id,created_subcategory_id);
			      }
			   }
			   var category_name = escape(main_category.category_name);
			   var category_desc = escape(main_category.category_desc);
			   var icon = escape(main_category.icon);
			   var content_html = '<li class = "main_section '+main_class+'" data-is_event_module_category="'+main_category.is_event_module_category+'"  data-main_id="'+main_category.id+'" ><span>'+main_category.category_name+'</span><span class="offline-fa">'+
					     '<i onclick="edit_main_category('+main_category.id+', \''+category_name+'\', \''+category_desc+'\', \''+icon+'\', \''+main_category.compliance_mandatory+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
					     '</span>'+
					     '<span class="offline-fa"><i onclick="delete_main_category('+main_category.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
					     '<span class="offline-fa"><i onclick="lms_get_content_location('+main_category.id+')" class="fa fa-map-marker fa-lg" aria-hidden="true"></i></span>'+
					  '</li>';
			   $('.main_section_content_list').append(content_html);
			}
			// for main section
			$(".offline_main_section_filled_div").show();
			$(".offline_main_section_blank_div").hide();
		     
		    }
		    else{
			// for main section
			$(".offline_main_section_blank_div").show();
			$(".offline_main_section_filled_div").hide();
			// for sub section
			$(".offline_sub_section_blank_first_div").show();
			$(".offline_sub_section_filled_div").hide();
			$(".offline_sub_section_blank_div").hide();
			// for content section
			$(".offline_content_section_blank_first_div").show();
			$(".offline_content_section_filled_div").hide();
			$(".offline_content_section_blank_div").hide();
		    }
		}
	    });
	}
	function lms_get_content_location(content_id) {
	    $("#lms_add_content_location_error").html("");
	    $("#lms_add_location_content_id").val(content_id);
	    $("#lms_content_location_list").modal("show");
	    $('#lms_content_group_location_list').html('<tr><td colspan="3" style="text-align:center">Please Wait</td></tr>');
	 
	    $.ajax({
			   url: base_url+'location/lms_get_content_location',
			   type: 'POST',
			   dataType: 'json',
			   data: ({'content_id': content_id,'is_contest' : '0'}),
			   success: function(data){
			     $('#lms_content_group_location_list').html(data.search_results);
			   }
	    });
	}
    function lms_add_content_location(){
	$("#lms_add_content_location_error").html("");
	var content_id = $("#lms_add_location_content_id").val();
	var is_contest = 0;
	var locations = [];
	$.each($("input[name='lms_content_location_list[]']:checked"), function(){            
	    locations.push($(this).val());
	    is_loc_remove = 1;
	});
	$('.loading').show();
	$.ajax({
	    url: base_url+'location/lms_add_content_location',
	    type: 'POST',
	    dataType: 'json',
	    data: ({'content_id': content_id,'is_contest': is_contest, 'locations':locations}),
	    success: function(data){
		$('.loading').hide();
		if(data.resultCode == '0'){
		    $("#lms_add_content_location_error").html(data.resultMsg);
		}
		else{
			  
		    $("#lms_add_content_location_error").html("Successfully Updated locations");
		}
	    }
	});
    }
	 // on click of main category
	 $(document).on('click', '.main_section', function (){
	    $( ".main_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	    });
	    $( this ).addClass( "offline_active" );
	    var main_id = $(this).data('main_id'); 
	    list_subsection(main_id);
	 });
	 
	 // function to list the sub category
	 function list_subsection(main_id,created_subcategory_id = '') {
	    $('.sub_section_content_list').html('');
	    var content = '';
	    for (var i = 0; i < content_array.main_category.length; i++) {
	       var main_category = content_array.main_category[i];
	       if (main_category.id == main_id) {
		  content = main_category.sub_category;
	       }
	    }
	    if (content.length > 0) {
	       // for sub section
	       $(".offline_sub_section_filled_div").show();
	       $(".offline_sub_section_blank_first_div").hide();
	       $(".offline_sub_section_blank_div").hide();
	       for (var i = 0; i < content.length; i++) {
		  var sub_category = content[i];
		  var main_class = '';
		  var sub_category_type = sub_category.is_this_is_content;
		  if (created_subcategory_id == '') {
		     if (i == 0) {
			main_class = 'offline_active';
			list_contentsection(main_id,sub_category.id,sub_category_type);
		     }
		  }else{
		     if (sub_category.id == created_subcategory_id) {
			main_class = 'offline_active';
			list_contentsection(main_id,sub_category.id,sub_category_type);
		     }
		  }
		  
		  if (sub_category.is_this_is_content == '0') {
		     $(".sub_category_add_data").data('is_this_is_content', '0');
		     var sub_category_name = escape(sub_category.sub_category_name);
		     var sub_category_desc = escape(sub_category.sub_category_desc);
		     var icon = escape(sub_category.icon);
		     var content_html = '<li class = "sub_section '+main_class+'"  data-main_id="'+main_id+'"  data-sub_id="'+sub_category.id+'"  data-sub_category_type="'+sub_category_type+'"><span>'+sub_category.sub_category_name+'</span><span class="offline-fa">'+
				    '<i onclick="edit_sub_category('+main_id+','+sub_category.id+', \''+sub_category_name+'\', \''+sub_category_desc+'\', \''+icon+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
				    '</span>'+
				    '<span class="offline-fa"><i onclick="delte_sub_category('+sub_category.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
				 '</li>';
		  }else{
		     var edit_icon = '';
		     $(".sub_category_add_data").data('is_this_is_content', '1');
		     var content_title = escape(sub_category.content_title);
		     var content_desc = escape(sub_category.content_desc);
		     var content_type = escape(sub_category.content_type);
		     var wiki_title = escape(sub_category.wikipedia_title_url);
		     if ((content_type == 'poll' || content_type == 'survey') && sub_category.is_synced == '1') {
			//code
		     }else{
			edit_icon = '<i onclick="edit_content('+sub_category.upload_type+','+main_id+',0,'+sub_category.id+',\''+content_title+'\',\''+sub_category.content_file+'\', \''+content_desc+'\' , \''+content_type+'\', \''+wiki_title+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>';
		     }
		     
		     var content_html = '<li class = "sub_section '+main_class+'" data-main_id="'+main_id+'"  data-sub_id="'+sub_category.id+'" data-sub_category_type="'+sub_category_type+'">'+
					  '<div class="row">'+
					  '<div class="col-sm-12">'+    
					  '<span><strong>'+sub_category.content_title+'</strong></span><span class="offline-fa">'+edit_icon+
					  //'<i onclick="edit_content('+main_id+',0,'+sub_category.id+',\''+content_title+'\',\''+sub_category.content_file+'\', \''+content_desc+'\' , \''+content_type+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
					  '</span>'+
					  '<span class="offline-fa"><i onclick="delete_content_on_sub_category('+sub_category.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
					  '</div>'+
					  '<div class="col-sm-12">'+
					  '<strong>'+sub_category.content_file+'</strong>'+
					  '</div>'+
					  '<div class="col-sm-12">'+
					  '<p>'+sub_category.content_desc+'</p>'+
					  '</div></div></li>';
		  }
		  
		  $('.sub_section_content_list').append(content_html);
	       }
	    }else{
	       // for sub section
	       $(".offline_sub_section_blank_div").show();
	       $(".offline_sub_section_blank_first_div").hide();
	       $(".offline_sub_section_filled_div").hide();
	       // for content section
	       $(".offline_content_section_blank_first_div").show();
	       var text = '<center><p>Add Sub Section First</p></center>';
	       $(".offline_content_section_blank_first_div").html(text);
	       $(".offline_content_section_filled_div").hide();
	       $(".offline_content_section_blank_div").hide();
	       
	       var is_event_module_category = 0;
		$( ".main_section" ).each(function() {
		    if($( this ).hasClass( "offline_active" )){
			is_event_module_category = $(this).data('is_event_module_category');
		    }
		});
		if (is_event_module_category == '4') {
		    $(".discuss_question_add_option").hide();
		}
		else{
		    $(".discuss_question_add_option").show();
		}
	    }
	 }
	 
	 // on click of sub category
	 $(document).on('click', '.sub_section', function (){
	    $( ".sub_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	    });
	    $( this ).addClass( "offline_active" );
	    var main_id = $(this).data('main_id');
	    var sub_id = $(this).data('sub_id');
	    var sub_category_type = $(this).data('sub_category_type');
	    list_contentsection(main_id,sub_id,sub_category_type);
	 });
	  // on click of content
	 $(document).on('click', '.content_section', function (){
	    $( ".content_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	    });
	    $( this ).addClass( "offline_active" );
	   
	 });
	 // function to list the content
	 function list_contentsection(main_id,sub_id,sub_category_type) {
	    $('.content_section_content_list').html('');
	    var content = '';
	    for (var i = 0; i < content_array.main_category.length; i++) {
	       var main_category = content_array.main_category[i];
	       if (main_category.id == main_id) {
		  content = main_category.sub_category;
	       }
	    }
	    var sub_content = '';
	    if (content.length > 0) {
	       for (var i = 0; i < content.length; i++) {
		  var sub_category = content[i];
		  if (sub_category.id == sub_id) {
		     if (sub_category.is_this_is_content == '0') {
			sub_content = sub_category.content;
		     }
		  }
		  
	       }
	       if (sub_content.length > 0) {
		  for (var i = 0; i < sub_content.length; i++) {
		     // for content section
		     $(".offline_content_section_filled_div").show();
		     $(".offline_content_section_blank_first_div").hide();
		     $(".offline_content_section_blank_div").hide();
		     var content_category = sub_content[i];
		     var main_class = '';
		     if (i == 0) {
			main_class = 'offline_active';
		     }
		     var edit_icon = '';
		     var content_title = escape(content_category.content_title);
		     var content_desc = escape(content_category.content_desc);
		     var content_type = escape(content_category.content_type);
		     var wiki_title = escape(content_category.wikipedia_title_url);
		      if ((content_type == 'poll' || content_type == 'survey') && sub_category.is_synced == '1') {
			//code
		     }else{
			edit_icon = '<i onclick="edit_content('+content_category.upload_type+','+main_id+','+sub_id+','+content_category.id+', \''+content_title+'\', \''+content_category.content_file+'\', \''+content_desc+'\', \''+content_type+'\', \''+wiki_title+'\')"   class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>';
		     }
		     var content_html = '<li class = "content_section '+main_class+'" data-main_id="'+main_id+'"  data-sub_id="'+sub_id+'" data-content_id="'+content_category.id+'">'+
					     '<div class="row">'+
					     '<div class="col-sm-12">'+    
					     '<span><strong>'+content_category.content_title+'</strong></span><span  class="offline-fa">'+edit_icon+
					    // '<i onclick="edit_content('+main_id+','+sub_id+','+content_category.id+', \''+content_title+'\', \''+content_category.content_file+'\', \''+content_desc+'\', \''+content_type+'\')"  class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
					     '</span>'+
					     '<span class="offline-fa"><i onclick="delete_content('+content_category.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
					     '</div>'+
					     '<div class="col-sm-12">'+
					     '<strong>'+content_category.content_file+'</strong>'+
					     '</div>'+
					     '<div class="col-sm-12">'+
					     '<p>'+content_category.content_desc+'</p>'+
					     '</div></div></li>';
		     $('.content_section_content_list').append(content_html);
		  }
	       }else{
		  
		  // for content section
		  
		  if(sub_category_type == 1){
		     $(".offline_content_section_blank_first_div").show();
		     $(".offline_content_section_blank_div").hide();
		     var text = '<center><p>No content available</p></center>';
		     $(".offline_content_section_blank_first_div").html(text);
		  }else{
		     $(".offline_content_section_blank_div").show();
		     $(".offline_content_section_blank_first_div").hide();
		     var text = '<center><p>Add Sub Section First</p></center>';
		     $(".offline_content_section_blank_first_div").html(text);
		  }
		  
		  $(".offline_content_section_filled_div").hide();
		  
	       }
	       
	    }else{
	       
	       // for content section
	       $(".offline_content_section_blank_first_div").show();
	       if(sub_category_type == 1){
		  var text = '<center><p>No content available</p></center>';
		  $(".offline_content_section_blank_first_div").html(text);
	       }else{
		  var text = '<center><p>Add Sub Section First</p></center>';
		  $(".offline_content_section_blank_first_div").html(text);
	       }
	       $(".offline_content_section_blank_div").hide();
	       $(".offline_content_section_filled_div").hide();
	       
	    }
	    
	 }
	 function open_add_main_category() {
	    $("#icon_validation").val('1');
	    $("#create_main_category_error").html("");
	    $("#add_new_main_section_form")[0].reset();
	  
	    $("#main_category_modal").modal("show");
	 }
	 // function to add main category
	 function add_main_category() {
	    $("#create_main_category_error").html("");
	    var compliance_cat = 0;	
	    var is_promoter_category = 0;
	    var compliance_mandatory = 0;
	    if ($('input[name="compliance_mandatory"]').is(':checked')) {
		compliance_mandatory = $('input[name="compliance_mandatory"]').val();
	    }
	    var main_category_name = $("#main_category_name").val();
	    var main_category_desc = $("#main_category_desc").val();
	    var offline_content_title = '';
	    var offline_content_desc = '';
	    if (main_category_name == '') {
	       $("#create_main_category_error").html("Please Enter Category Name");
	       return false;
	    }else{
	       var icon_validation = $("#icon_validation").val();
	       if (icon_validation == '0') {
		  $("#create_main_category_error").html("Icon should be same hight and width and should be < 100px");
		  return false;
	       }
	       var file_data = $("#main_category_file").prop("files")[0];
	       $(".loading").show();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('main_category_id','');
	       form_data.append('main_category_name',main_category_name);
	       form_data.append('main_category_desc',main_category_desc);
	       form_data.append("location_uid", 0);
	       form_data.append("locationid", 0);
	       form_data.append('offline_content_title',offline_content_title);
	       form_data.append('offline_content_desc',offline_content_desc);
	       form_data.append("file_previous_name", '');
	       form_data.append("is_promoter_category",is_promoter_category);
	       form_data.append("compliance_cat",compliance_cat);
	       form_data.append("is_lms",1);
	       form_data.append("compliance_mandatory",compliance_mandatory);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_main_category",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     content_builder(data);
		     $("#main_category_name").val('');
		     $("#main_category_desc").val('');
		     $("#main_category_modal").modal("hide");
		  }
	       });
	    }
	 }
	 // on click of add more sub category find which model open
	 $(document).on('click', '.sub_category_add_data', function (){
	    var is_this_is_content = $( this ).data( "is_this_is_content" );
	    if (is_this_is_content == 0) {
	       $("#icon_validation").val('1');
	       $("#create_sub_category_error").html("");
	       $("#add_new_sub_section_form")[0].reset();
	       $("#sub_category_modal").modal("show");
	    }else{
	       choose_content_type(1);
	    }
	 });
	 function open_add_sub_category() {
	    $("#icon_validation").val('1');
	    $("#create_sub_category_error").html("");
	    $("#add_new_sub_section_form")[0].reset();
	    $("#sub_category_modal").modal("show");
	 }
	 // function to add sub category
	 function add_sub_category() {
	    $("#create_sub_category_error").html("");
	    var sub_category_name = $("#sub_category_name").val();
	    var sub_category_desc = $("#sub_category_desc").val();
	    if (sub_category_name == '') {
	       $("#create_sub_category_error").html("Please Enter Category Name");
	       return false;
	    }else{
	       var icon_validation = $("#icon_validation").val();
	       if (icon_validation == '0') {
		  $("#create_sub_category_error").html("Icon should be same hight and width and should be < 100px");
		  return false;
	       }
	       var category_id = '0';
	       $(".loading").show();
	       $( ".main_section" ).each(function() {
		  if($( this ).hasClass( "offline_active" )){
		     category_id = $(this).data('main_id');
		  }
	       });
	       var file_data = $("#sub_category_file").prop("files")[0];
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('sub_category_id','');
	       form_data.append('sub_category_name',sub_category_name);
	       form_data.append('sub_category_desc',sub_category_desc);
	       form_data.append("category_id", category_id);
	       form_data.append("location_uid", 0);
	       form_data.append("locationid", 0);
	       form_data.append("file_previous_name", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_sub_category",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     content_builder(category_id,data);
		     $("#sub_category_name").val('');
		     $("#sub_category_desc").val('');
		     $("#sub_category_modal").modal("hide");
		  }
	       });
	    }
	 }
	 // function to choose content type when add
	 function choose_content_type(is_main_category) {
	    $("input[name=content_type_radio]").prop('checked', false);
	    $("#choose_content_type_is_main_category").val(is_main_category);
	    $(".retail_audit_content_type").hide();
		$(".not_retail_audit_content_type").show();
		
		
	    
	    $("#choose_content_type_modal").modal("show");
	    //$("#upload_content_modal_poll").modal("show");
	 }
	 // function call after choosing content type
	 function choose_content_type_next() {
	    var is_main_category = $("#choose_content_type_is_main_category").val();
	    var content_type = $("input[name='content_type_radio']:checked").val();
	    $("#choose_content_type_modal").modal("hide");
	    var location_type = $("#location_type").val();
	    if (location_type == '13') {
		content_type = 'survey_retail_audit';
	    }
	    if (content_type == 'text') {
	       $("#upload_content_modal_text_form")[0].reset();
	       $("#is_main_category_text").val(is_main_category);
	       $("#upload_content_modal_text").modal("show");
	    }
	    else if (content_type == 'image_audio_video') {
	       $("#upload_content_modal_form")[0].reset();
	       $("#is_main_category").val(is_main_category);
	       $("#upload_content_modal").modal("show");
	    }
	    else if (content_type == 'poll') {
	       $("#add_content_poll")[0].reset();
	       $("#is_main_category_poll").val(is_main_category);
	       $("#upload_content_modal_poll").modal("show");
	    }
	    else if (content_type == 'survey') {
	       $("#add_content_survey")[0].reset();
	       $("#is_main_category_survey").val(is_main_category);
	       $("#upload_content_modal_survey").modal("show");
	    }
	    else if (content_type == 'pdf') {
	       $("#upload_content_modal_pdf_form")[0].reset();
	       $("#is_main_category_pdf").val(is_main_category);
	       $("#upload_content_modal_pdf").modal("show");
	    }
	    else if (content_type == 'upload') {
	       $("#upload_content_modal_upload_form")[0].reset();
	       $("#is_main_category_upload").val(is_main_category);
	       $("#upload_content_modal_upload").modal("show");
	    }
	    else if (content_type == 'wiki') {
	       $("#upload_content_modal_wiki_form")[0].reset();
	       $("#is_main_category_wiki").val(is_main_category);
	       $("#upload_content_modal_wiki").modal("show");
	    }
	    else if (content_type == 'survey_retail_audit') {
	       $("#add_content_survey_retail_audit")[0].reset();
	       $("#is_main_category_survey_retail_audit").val(is_main_category);
	       $("#upload_content_modal_survey_retail_audit").modal("show");
	    }
	    //$(".add_content_form")[0].reset();
	     
	 }
	 // function to add sub category content
	 function add_content_text() {
	    $("#create_content_error_text").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_text").val();
	    var content_title = $("#content_title_text").val();
	    var content_desc = $("#content_desc_text").val();
	    if (content_title == '') {
	       $("#create_content_error_text").html("Please Enter Title");
	       return false;
	    }else{
	       var category_id = '0';
	       $(".loading").show();
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", 0);
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#content_title_text").val('');
		     $("#content_desc_text").val('');
		     $("#upload_content_modal_text").modal("hide");
		  }
	       });
	    }
	 }
	 
	 // function to add sub category content
	 function add_content() {
	    
	    $("#create_content_error").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category").val();
	    var content_title = $("#content_title").val();
	    var content_desc = $("#content_desc").val();
	    if (content_title == '') {
	       $("#create_content_error").html("Please Enter Title");
	       return false;
	    }else{
	       var category_id = '0';
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var file_data = $("#content_file").prop("files")[0];
	       if (typeof file_data === "undefined") {
		  $("#create_content_error").html("Please Select File");
		  return false;
	       }else{
		  var file_size = $("#content_file").prop("files")[0].size;
		  file_size = ((file_size/1024)/1024);
		  if (file_size > 500) {
		     $("#create_content_error").html("File size should be max 500MB");
		     return false;
		  }
	       }
	       $(".loading").show();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  xhr: function () {
                           var xhr = new window.XMLHttpRequest();
                           xhr.upload.addEventListener("progress", function (evt) {
                               if (evt.lengthComputable) {
                                   var percentComplete = evt.loaded / evt.total;
                                   percentComplete = parseInt(percentComplete * 100);
                                     $('.progress').removeClass('hide');
                                   $('.myprogress').text(percentComplete + '%');
                                   $('.myprogress').css('width', percentComplete + '%');
                               }
                           }, false);
                           return xhr;
                       },
		  success: function(data){
		     $('.progress').addClass('hide');
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#content_title").val('');
		     $("#content_desc").val('');
		     $("#content_file").val('');
		     $("#upload_content_modal").modal("hide");
		  }
	       });
	    }
	 }
      
      
	 // function to delete content
	 function delete_content(content_id) {
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_content",
		  data: "content_id="+content_id,
		  success: function(data){
		     var main_category = $('.content_section').filter('[data-content_id="'+content_id+'"]').data('main_id');
		     var sub_category = $('.content_section').filter('[data-content_id="'+content_id+'"]').data('sub_id');
		     content_builder(main_category,sub_category);
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 // function to delete content from sub category list
	 function delete_content_on_sub_category(content_id) {
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_content",
		  data: "content_id="+content_id,
		  success: function(data){
		     var main_category = $('.sub_section').filter('[data-sub_id="'+content_id+'"]').data('main_id');
		     content_builder(main_category);
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 // function to delete sub category
	 function delte_sub_category(sub_category_id) {
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_sub_category",
		  data: "sub_category_id="+sub_category_id,
		  success: function(data){
		     var main_category = $('.sub_section').filter('[data-sub_id="'+sub_category_id+'"]').data('main_id');
		    content_builder(main_category);
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 // function to delete main category 
	 function delete_main_category(main_category_id) {
	    if (confirm("Are you sure you want to delete?")) {
	       $(".loading").show();
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/delete_main_category",
		  data: "main_category_id="+main_category_id,
		  success: function(data){
		    content_builder();
		  }
	       });
	    }else{
	       return false;
	    }
	 }
	 // function to open edit main category popup
	 function edit_main_category(category_id,category_name,category_desc,icon,compliance_mandatory) {
	    $("#icon_validation").val('1');
	    $("#update_main_category_error").html("");
	    $("#edit_new_main_section_form")[0].reset();
	    
	   $("#main_category_id_edit").val(category_id);
	    $("#main_category_name_edit").val(unescape(category_name));
	    $("#main_category_desc_edit").val(unescape(category_desc));
	    if (icon != '') {
		  $("#maine_category_edit_file_previous_name_div").show();
	       }else{
		  $("#maine_category_edit_file_previous_name_div").hide();
	       }
	     $("#main_category_edit_file_previous_name").html(icon);
	     
	    if (compliance_mandatory == 1) {
		$("input[name=compliance_mandatory_edit]").prop('checked', true);
	    }
	    else
	    {
		$("input[name=compliance_mandatory_edit]").prop('checked', false);
	    }
	    
	    $("#main_category_modal_edit").modal('show');
	 }
	 $(document).on('click','#main_category_edit_file_previous_name_remove_button',function(){
	    $("#main_category_edit_file_previous_name").html("");
	 });
	 
	
	 
      $('.icon_upload_class_image').change(function() {
	 var image_id = $(this).attr('id');
	 var fr = new FileReader;
	 fr.onload = function() {
            var img = new Image;
            img.onload = function() {
               image_width = img.width;
               var image_height = img.height;
	       var imgpath=document.getElementById(image_id);
               var image_size = Math.round(imgpath.files[0].size/1024);
               if(image_height == image_width){
                  if( image_width > 100 && image_height >100){
                     $("#icon_validation").val('0');
                  }else{
                     $("#icon_validation").val('1');
                  }
               }
               else{
                  $("#icon_validation").val('0');
               }

            };
            img.onerror = function() {
               $("#icon_validation").val('0');
            };
            img.src = fr.result;
	 };
	 fr.readAsDataURL(this.files[0]);
      });
	 
	 // function to update main category
	 function update_main_category() {
	    
	    $("#update_main_category_error").html("");
	    var compliance_mandatory = 0;
	    if ($('input[name="compliance_mandatory_edit"]').is(':checked')) {
		compliance_mandatory = $('input[name="compliance_mandatory_edit"]').val();
	    }
	    var main_category_id_edit = $("#main_category_id_edit").val();
	    var main_category_name = $("#main_category_name_edit").val();
	    var main_category_desc = $("#main_category_desc_edit").val();
	    
	    if (main_category_name == '') {
	       $("#update_main_category_error").html("Please Enter Category Name");
	       return false;
	    }else{
	       var icon_validation = $("#icon_validation").val();
	       if (icon_validation == '0') {
		  $("#update_main_category_error").html("Icon should be same hight and width and should be < 100px");
		  return false;
	       }
	       var file_data = $("#main_category_file_edit").prop("files")[0];
	       
	       var file_previous_name = $("#main_category_edit_file_previous_name").html();
	       $(".loading").show();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('main_category_id',main_category_id_edit);
	       form_data.append('main_category_name',main_category_name);
	       form_data.append('main_category_desc',main_category_desc);
	       form_data.append("location_uid", 0);
	       form_data.append("locationid", 0);
	       form_data.append('offline_content_title','');
	       form_data.append('offline_content_desc','');
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("is_promoter_category",0);
	       form_data.append("compliance_cat",0);
	       form_data.append("compliance_mandatory",compliance_mandatory);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_main_category",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     content_builder(main_category_id_edit);
		     $("#main_category_modal_edit").modal("hide");
		  }
	       });
	    }
	 }
	 // function to open edit main sub popup
	 function edit_sub_category(main_category_id,sub_category_id,sub_category_name,sub_category_desc,icon) {
	    $("#icon_validation").val('1');
	    $("#update_sub_category_error").html("");
	    $("#edit_new_sub_section_form")[0].reset();
	    
	    $("#sub_category_id_edit_main_category").val(main_category_id);
	    $("#sub_category_id_edit").val(sub_category_id);
	    $("#sub_category_name_edit").val(unescape(sub_category_name));
	    $("#sub_category_desc_edit").val(unescape(sub_category_desc));
	    if (icon != '') {
		  $("#sub_category_edit_file_previous_name_div").show();
	       }else{
		  $("#sub_category_edit_file_previous_name_div").hide();
	       }
	     $("#sub_category_edit_file_previous_name").html(icon);
	    $("#sub_category_modal_edit").modal('show');
	 }
	  $(document).on('click','#sub_category_edit_file_previous_name_remove_button',function(){
	    $("#sub_category_edit_file_previous_name").html("");
	 });
	  // function to update sub category
	 function update_sub_category() {
	    $("#update_sub_category_error").html("");
	    var sub_category_id = $("#sub_category_id_edit").val();
	    var sub_category_name = $("#sub_category_name_edit").val();
	    var sub_category_desc = $("#sub_category_desc_edit").val();
	    if (sub_category_name == '') {
	       $("#update_sub_category_error").html("Please Enter Category Name");
	       return false;
	    }else{
	       var icon_validation = $("#icon_validation").val();
	       if (icon_validation == '0') {
		  $("#update_sub_category_error").html("Icon should be same hight and width and should be < 100px");
		  return false;
	       }
	       var file_data = $("#sub_category_file_edit").prop("files")[0];
	       var file_previous_name = $("#sub_category_edit_file_previous_name").html();
	       $(".loading").show();
	       var category_id = $("#sub_category_id_edit_main_category").val();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('sub_category_id',sub_category_id);
	       form_data.append('sub_category_name',sub_category_name);
	       form_data.append('sub_category_desc',sub_category_desc);
	       form_data.append("category_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", file_previous_name);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_sub_category",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     content_builder(category_id,sub_category_id);
		      $("#sub_category_modal_edit").modal('hide');
		  }
	       });
	    }
	 }
	 
	 // function to open edit content popup
	 function edit_content(upload_type,main_id,sub_id, content_id, content_title, content_file, content_desc, content_type,wiki_title) {
	    if (content_type == 'video' || content_type == 'image' || content_type == 'audio') {
	       $("#edit_image_video_content_form")[0].reset();
	       $("#edit_content_id").val(content_id);
	       $("#edit_content_id_main_category").val(main_id);
	       $("#edit_content_id_sub_category").val(sub_id);
	       $("#content_title_edit").val(unescape(content_title));
	       $("#content_desc_edit").val(unescape(content_desc));
	       if (content_file != '') {
		  $("#file_previous_name_div").show();
	       }else{
		  $("#file_previous_name_div").hide();
	       }
	       $("#file_previous_name").html(content_file);
	       $("#upload_content_modal_edit").modal('show');
	    }
	    else if (content_type == 'text') {
	       $("#edit_content_id_text").val(content_id);
	       $("#edit_content_id_main_category_text").val(main_id);
	       $("#edit_content_id_sub_category_text").val(sub_id);
	       $("#content_title_edit_text").val(unescape(content_title));
	       $("#content_desc_edit_text").val(unescape(content_desc));
	       $("#upload_content_modal_edit_text").modal('show');
	    }
	    else if (content_type == 'poll') {
	       $("#update_content_poll")[0].reset();
	       poll_content_edit_modal_open(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type);
	    }
	    else if (content_type == 'survey') {
		var location_type = $("#location_type").val();
		if (location_type == '13')
		{
		    $("#update_content_survey_retail_audit")[0].reset();
		    survey_content_edit_modal_open_retail_audit(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type);
		}
		else
		{
		    $("#update_content_survey")[0].reset();
		    survey_content_edit_modal_open(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type);
		}
	       
	    }
	    else if (content_type == 'pdf') {
	        $("#edit_pdf_content_form")[0].reset();
	       $("#edit_content_id_pdf").val(content_id);
	       $("#edit_content_id_main_category_pdf").val(main_id);
	       $("#edit_content_id_sub_category_pdf").val(sub_id);
	       $("#content_title_edit_pdf").val(unescape(content_title));
	       $("#content_desc_edit_pdf").val(unescape(content_desc));
	       if (content_file != '') {
		  $("#file_previous_name_div_pdf").show();
	       }else{
		  $("#file_previous_name_div_pdf").hide();
	       }
	       $("#file_previous_name_pdf").html(content_file);
	       $("#upload_content_modal_edit_pdf").modal('show');
	    }
	    else if (content_type == 'upload') {
	       $("#edit_content_id_upload").val(content_id);
	       $("#edit_content_id_main_category_upload").val(main_id);
	       $("#edit_content_id_sub_category_upload").val(sub_id);
	       $("#content_title_edit_upload").val(unescape(content_title));
	       $("#content_desc_edit_upload").val(unescape(content_desc));
	       $("#upload_content_modal_edit_upload").modal('show');
	       if (upload_type == '2' || upload_type == '3') {
		  $('input[name="upload_content_user_video_allow_edit"]').prop('checked', true)
	       }else{
		  $('input[name="upload_content_user_video_allow_edit"]').prop('checked', false)
	       }
	       if (upload_type == '1' || upload_type == '3') {
		  $('input[name="upload_content_user_image_allow_edit"]').prop('checked', true)
	       }else{
		  $('input[name="upload_content_user_image_allow_edit"]').prop('checked', false)
	       }
	       
	    }
	    else if (content_type == 'wiki') {
	       $("#edit_content_id_wiki").val(content_id);
	       $("#edit_content_id_main_category_wiki").val(main_id);
	       $("#edit_content_id_sub_category_wiki").val(sub_id);
	       $("#content_title_edit_wiki").val(unescape(content_title));
	       $("#wiki_title_url_edit").val(unescape(wiki_title));
	       $("#upload_content_modal_edit_wiki").modal('show');
	    }
	    
	 }
	 // function to update content
	 function update_content_text() {
	    $("#edit_content_error_text").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id_text").val();
	    var content_title = $("#content_title_edit_text").val();
	    var content_desc = $("#content_desc_edit_text").val();
	   var file_previous_name = '';
	    if (content_title == '') {
	       $("#edit_content_error").html("Please Enter Title");
	       return false;
	    }else{
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category_text").val();
	       sub_category = $("#edit_content_id_sub_category_text").val();
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", content_id);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		    
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_edit_text").modal("hide");
		  }
	       });
	    }
	 }
	 

	 // function to update content
	 function update_content() {
	    $("#edit_content_error").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id").val();
	    var content_title = $("#content_title_edit").val();
	    var content_desc = $("#content_desc_edit").val();
	    var content_file = $("#content_file_edit").val();
	    var file_previous_name = $("#file_previous_name").html();
	    if (content_title == '') {
	       $("#edit_content_error").html("Please Enter Title");
	       return false;
	    }else{
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category").val();
	       sub_category = $("#edit_content_id_sub_category").val();
	       var file_data = $("#content_file_edit").prop("files")[0];
	       if (typeof file_data === "undefined") {
	       }else{
		  var file_size = $("#content_file_edit").prop("files")[0].size;
		  file_size = ((file_size/1024)/1024);
		  if (file_size > 500) {
		     $("#edit_content_error").html("File size should be max 500MB");
		     return false;
		  }
	       }
	       
	       //$('form#aaa_dddd').find('input, textarea, button').prop('disabled', true);
	       
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_file", content_file);
	       form_data.append("content_id", content_id);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       
	       
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  xhr: function () {
                           var xhr = new window.XMLHttpRequest();
                           xhr.upload.addEventListener("progress", function (evt) {
                               if (evt.lengthComputable) {
                                   var percentComplete = evt.loaded / evt.total;
                                   percentComplete = parseInt(percentComplete * 100);
                                     $('.progress').removeClass('hide');
                                   $('.myprogress').text(percentComplete + '%');
                                   $('.myprogress').css('width', percentComplete + '%');
                               }
                           }, false);
                           return xhr;
                       },
		  success: function(data){
		    $('.progress').addClass('hide');
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_edit").modal("hide");
		  }
	       });
	    }
	 }
	 
	 $(document).on('click','#file_previous_name_remove_button',function(){
	    $("#file_previous_name").html("");
	 });
      
      


      function add_content_builder() {
	 $("#add_content_builder_error").html('');
	 var offline_content_title = $("#offline_content_title").val();
	 var offline_content_desc = $("#offline_content_desc").val();
	 if (offline_content_desc == '' || offline_content_title == '') {
	    $("#add_content_builder_error").html('Please fill Title and Description');
	    return false;
	 }
	 var form_data = new FormData(); 
	 form_data.append('offline_content_title',offline_content_title);
	 form_data.append('offline_content_desc',offline_content_desc);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_content_builder",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data){
	       if (Clicked_ButtonValue == 'exit') {
		  window.location = "<?php  echo base_url()?>location";
	       }else{
		  $(".loading").hide();
	       }
	    }
         });
      }
	 
	 
      
      
	 
	 $("#main_category_name").keyup(function() {
	    el = $(this);
	    if (el.val().length > 30) {
		el.val(el.val().substr(0, 30));
	    } else {
		$(".main_category_name_length").text(el.val().length);
	    }
	 });
	 $("#main_category_name_edit").keyup(function() {
	    el = $(this);
	    if (el.val().length > 30) {
		el.val(el.val().substr(0, 30));
	    } else {
		$(".main_category_name_length").text(el.val().length);
	    }
	 });
	 $("#sub_category_name").keyup(function() {
	    el = $(this);
	    if (el.val().length > 30) {
		el.val(el.val().substr(0, 30));
	    } else {
		$(".sub_category_name_length").text(el.val().length);
	    }
	 });
	 $("#sub_category_name_edit").keyup(function() {
	    el = $(this);
	    if (el.val().length > 30) {
		el.val(el.val().substr(0, 30));
	    } else {
		$(".sub_category_name_length").text(el.val().length);
	    }
	 });
	 
	 
	
	
	 $('.modal').on('hidden.bs.modal', function (e) {
	    if($('.modal').hasClass('in')) {
	    $('body').addClass('modal-open');
	    }    
	 });
	 
	 function add_content_poll() {
	    $("#create_content_error_poll").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_poll").val();
	    var content_title = $("#content_title_poll").val();
	    var poll_querstion = $("#poll_question").val();
	    var option_one = $("#poll_option_one").val();
	    var option_two = $("#poll_option_two").val();
	    var option_three = $("#poll_option_three").val();
	    var option_four = $("#poll_option_four").val();
	    var option_five = $("#poll_option_five").val();
	    var option_six = $("#poll_option_six").val();
	    if (content_title == '' || poll_querstion == '' || option_one == '' || option_two == '') {
	       $("#create_content_error_poll").html("Please Fill mandatory fields");
	       return false;
	    }else{
	       $(".loading").show();
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       
	       var file_data = $("#pole_file").prop("files")[0];
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("poll_querstion",poll_querstion);
	       form_data.append("option_one",option_one);
	       form_data.append("option_two",option_two);
	       form_data.append("option_three",option_three);
	       form_data.append("option_four",option_four);
	       form_data.append("option_five",option_five);
	       form_data.append("option_six",option_six);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_poll",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#add_content_poll")[0].reset();
		     $("#upload_content_modal_poll").modal("hide");
		  }
	       });
	    }
	 }
	 

	
	 function poll_content_edit_modal_open(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type){
	    $("#edit_content_id_poll").val(content_id);
	       $("#edit_content_id_main_category_poll").val(main_id);
	       $("#edit_content_id_sub_category_poll").val(sub_id);
	       $("#content_title_poll_edit").val(unescape(content_title));
	       if (content_file != '') {
		  $("#poll_edit_file_previous_name_div").show();
	       }else{
		  $("#poll_edit_file_previous_name_div").hide();
	       }
	       $("#poll_edit_file_previous_name").html(content_file);
	       $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/get_content_poll_survey",
	       data: "content_id="+content_id,
	       dataType: 'json',
	       success: function(data){
		  $(".loading").hide();
		  if (data.question.length > 0) {
		     var obj = data.question[0];
		     $("#poll_question_edit").val(obj.question);
		    
		     if (obj.options.length > 0) {
			for (var i = 0; i < obj.options.length; i++) {
			   var op_obj = obj.options[i];
			   var option_id = "poll_option_one_edit";
			   if (i == '1') {
			      option_id = "poll_option_two_edit";
			   }
			   else if (i == '2') {
			      option_id = "poll_option_three_edit";
			   }
			   else if (i == '3') {
			      option_id = "poll_option_four_edit";
			   }
			   else if (i == '4') {
			      option_id = "poll_option_five_edit";
			   }
			   else if (i == '5') {
			      option_id = "poll_option_six_edit";
			   }
			   $("#"+option_id).val(op_obj.option_text);
			}
			
			
		     }
		  }
		     $("#upload_content_modal_poll_edit").modal("show");
		    
	       }
	    });

	 }
	 $(document).on('click','#poll_edit_file_previous_name_remove_button',function(){
	    $("#poll_edit_file_previous_name").html("");
	 });
      	 function update_content_poll() {
	    $("#create_content_error_poll_edit").html("");
	    var content_id = $("#edit_content_id_poll").val();
	    var main_category = $("#edit_content_id_main_category_poll").val();
	    var sub_category = $("#edit_content_id_sub_category_poll").val();
	    var content_title = $("#content_title_poll_edit").val();
	    var poll_querstion = $("#poll_question_edit").val();
	    var option_one = $("#poll_option_one_edit").val();
	    var option_two = $("#poll_option_two_edit").val();
	    var option_three = $("#poll_option_three_edit").val();
	    var option_four = $("#poll_option_four_edit").val();
	    var option_five = $("#poll_option_five_edit").val();
	    var option_six = $("#poll_option_six_edit").val();
	    if (content_title == '' || poll_querstion == '' || option_one == '' || option_two == '') {
	       $("#create_content_error_poll_edit").html("Please Fill mandatory fields");
	       return false;
	    }else{
	       $(".loading").show();
	       
	       var file_data = $("#pole_file_edit").prop("files")[0];
	       var file_previous_name = $("#poll_edit_file_previous_name").html();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       //form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", content_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("poll_querstion",poll_querstion);
	       form_data.append("option_one",option_one);
	       form_data.append("option_two",option_two);
	       form_data.append("option_three",option_three);
	       form_data.append("option_four",option_four);
	       form_data.append("option_five",option_five);
	       form_data.append("option_six",option_six);
	       form_data.append("file_previous_name",file_previous_name);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/update_content_poll",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     
		     content_builder(main_category,sub_category);
		     $("#update_content_poll")[0].reset();
		     $("#upload_content_modal_poll_edit").modal("hide");
		  }
	       });
	    }
	 }
	 

	

	 


	 


	 

	 
	 // on click of new question add add new question div
	 $(document).on('click', '.add_new_question', function(){
	    var num_of_question = $("#total_survey_question_created").val();
	    num_of_question++;
	    $("#total_survey_question_created").val(num_of_question);
	    if (num_of_question > 9) {
	       num_of_question = "QUESTION "+num_of_question;
	    }else{
	       num_of_question = "QUESTION 0"+num_of_question;
	    }
	    
	    var new_div = '<div class="survey_div">'+
				    '<div class="form-group">'+
				       '<label style="color:#f00f64">'+num_of_question+'</label>'+
				       '<label class="pull-right remove_survey_question" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				       '<select class="form-control survey_type_class" name="survey_type[]">'+
					  '<option value="1">Text Entry</option>'+
					  '<option value="2">Single Choice</option>'+
					  '<option value="3">Multi Coice</option>'+
				       '</select>'+
				    '</div>'+
				    '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question[]" class = "survey_question_class"></textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				    //'</div><div class="survey_div_option"></div>'+
				 '</div>';
	    $("#survey_question_div").append(new_div);
	 });
	 
	 // remove question div
	 $(document).on('click', '.remove_survey_question', function (){
	    $(this).parent().parent().remove();
	 });
	  
	 // on change of survey type hide and show option
	 $(document).on('change', '.survey_type_class', function(){
	    var survey_type = $(this).val();
	    
	    if (survey_type != '1') {
	       var new_options = '<div class="survey_div_option"><legend style="margin-bottom:0px;font-size:14px; color:#414042">ANSWER OPTIONS  <small style="color:#999999">(Upto 6 Options)</small></legend>'+
	       '<div class="mui-textfield mui-textfield--float-label survey_option_count">'+
                  '<input type="text" name="options[]" class = "survey_option_class">'+
                  '<label>Option A<sup>*</sup></label>'+
               '</div>'+
	       '<div class="form-group survey_option_count">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options[]" class = "survey_option_class">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option B<sup>*</sup></label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-plus-square add_survey_option" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>'+
	       '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div").find('.survey_div_option').find('.survey_option_count') ).each(function() {
			total_option++;
	       });
	       if (total_option == 0) {
		  $(this).closest(".survey_div").append(new_options);
	       }
	       
	    }else{
	       $(this).closest(".survey_div").find('.survey_div_option').remove();
	    }
	 });
	 
	 // on click add option add option
	 $(document).on('click', '.add_survey_option', function(){
	    var new_options = '<div class="form-group survey_option_count">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options[]" class = "survey_option_class">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option</label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-minus-square remove_survey_option" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_option").find('.survey_option_count') ).each(function() {
			total_option++;
		     });
	       if (total_option < 6) {
		  $(this).closest(".survey_div_option").append(new_options);
	       }
	       
	 });
	 // on click of button remove option
	 $(document).on('click', '.remove_survey_option', function(){
	    $(this).parent().parent().parent().parent().remove();
	 });
	 
	 
	 
	 function add_content_survey() {
	    $("#create_content_error_survey").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_survey").val();
	    var content_title = $("#content_title_survey").val();
	   
	    if (content_title == '') {
	       $("#create_content_error_survey").html("Please Fill mandatory fields");
	       return false;
	    }else{
	      if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var questions_array = {};
	       var i = 0;
	       var survey_error = 0;
	       $( ".survey_div" ).each(function() {
		  var question_type = 0;
		  var question= '';
		  question_type = $(this).find('.survey_type_class').val();
		  question = $(this).find('.survey_question_class').val();
		  if (question == '') {
		     survey_error = 1;
		  }
		  var options_array = {};
		  if (question_type == '2' || question_type == '3') {
		     var j = 0;
		     var total_option = 0;
		     $( $(this).find('.survey_div_option').find('.survey_option_count') ).each(function() {
			var option_val = '';
			option_val = $(this).find('.survey_option_class').val();
			if (typeof option_val === "undefined") {
			   
			}else{
			   if(option_val != ''){
			      total_option++;
			      options_array[j] = option_val;
			      j++;
			   }
			   
			}
			
		     });
		     if (question_type != 1 && total_option < 2) {
			survey_error = 1;
		     }
		  }
		  
		  
		  
		  questions_array[i] =  { question_type : question_type , question: question, options: options_array };
		  i++;
	       });
	       if (survey_error == '1') {
		  $("#create_content_error_survey").html("Please Fill mandatory fields");
		  return false;
	       }
	        $(".loading").show();
	       questions_array = JSON.stringify(questions_array); 
	       var file_data = $("#survey_file").prop("files")[0];
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("questions", questions_array);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_survey",
		  cache: false,
		  contentType: false,
		  processData: false,
		  dataType: "json",
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#add_content_survey")[0].reset();
		     $("#upload_content_modal_survey").modal("hide");
		  }
	       });
	    }
	 }
	
	
	
	 function survey_content_edit_modal_open(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type){
	    $("#edit_content_id_survey").val(content_id);
	       $("#edit_content_id_main_category_survey").val(main_id);
	       $("#edit_content_id_sub_category_survey").val(sub_id);
	       $("#content_title_survey_edit").val(unescape(content_title));
	       if (content_file != '') {
		  $("#survey_edit_file_previous_name_div").show();
	       }else{
		  $("#survey_edit_file_previous_name_div").hide();
	       }
	       $("#survey_edit_file_previous_name").html(content_file);
	       $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/get_content_poll_survey",
	       data: "content_id="+content_id,
	       dataType: 'json',
	       success: function(data){
		  $(".loading").hide();
		  $('#survey_question_div_edit').html("");
		  if (data.question.length > 0) {
		     for (var i = 0; i < data.question.length; i++) {
			var obj = data.question[i];
			var question_type = obj.survey_type;
			var question = obj.question;
			var options_array = obj.options;
			var main_div = '';
			var question_number = i+1;
			if (question_number < 10) {
			   question_number = '0'+question_number;
			}
			var level = '';
			if (i != 0) {
			   level =  '<label class="pull-right remove_survey_question_edit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>';
			}
			var text_entry = ''; var single_choice = ''; var multi_choice = '';
			if (question_type == '1') {
			   text_entry = 'selected';
			}
			else if (question_type == '2') {
			   single_choice = 'selected';
			}
			else if (question_type == '3') {
			   multi_choice = 'selected';
			}
			main_div += '<div class="survey_div_edit">';
			main_div += '<div class="form-group">'+
					  '<label style="color:#f00f64">QUESTION '+question_number+'</label>'+level+
					  '<select class="form-control survey_type_class_edit" name="survey_type_edit[]">'+
					     '<option value="1" '+text_entry+'>Text Entry</option>'+
					     '<option value="2" '+single_choice+'>Single Choice</option>'+
					     '<option value="3" '+multi_choice+'>Multi Coice</option>'+
					  '</select>'+
				       '</div>';
			main_div += '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_edit[]" class = "survey_question_class_edit">'+question+'</textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				    '</div>';
			if (options_array.length > 0) {
			   main_div += '<div class="survey_div_option_edit">';
			   main_div += '<legend style="margin-bottom:0px;font-size:14px; color:#414042">'+
					  'ANSWER OPTIONS'+
					  '<small style="color:#999999">(Upto 6 Options)</small>'+
					  '</legend>';
			   for (var j = 0; j < options_array.length; j++) {
			      var op_obj = options_array[j];
			      var option_val = op_obj.option_text;
			      if (j == 0) {
			      main_div += '<div class="mui-textfield mui-textfield--float-label survey_option_count_edit">'+
					  '<input class="survey_option_class_edit mui--is-empty mui--is-untouched mui--is-pristine" type="text" name="options_edit[]" value="'+option_val+'">'+
					  '<label>'+
					  'Option'+
					  '<sup>*</sup>'+
					  '</label>'+
					  '</div>';
			      }else if (j == 1) {
				 main_div += '<div class="form-group survey_option_count_edit">'+
				 '<div class="row">'+
				 '<div class="col-sm-10 col-xs-10 nopadding-right">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				 '<input type="text" class="survey_option_class_edit mui--is-empty mui--is-untouched mui--is-pristine" name="options_edit[]" value="'+option_val+'">'+
				 '<label style="font-weight:300;color:#a7a9ac">Option<sup>*</sup></label>'+
				 '</div>'+
				 '</div>'+
				 '<div style="padding-top:3%" class="col-sm-2 col-xs-2">'+
				 '<span class="btn-append"> <i style="font-family:FontAwesome" class="far fa-plus-square add_survey_option_edit"></i></span>'+
				' </div>'+
				 '</div>'+
				 '</div>';
			      }
			      else{
				 main_div += '<div class="form-group survey_option_count_edit">'+
				 '<div class="row">'+
				 '<div class="col-sm-10 col-xs-10 nopadding-right">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				 '<input type="text" class="survey_option_class_edit mui--is-empty mui--is-untouched mui--is-pristine" name="options_edit[]" value="'+option_val+'">'+
				 '<label style="font-weight:300;color:#a7a9ac">Option<sup>*</sup></label>'+
				 '</div>'+
				 '</div>'+
				 '<div style="padding-top:3%" class="col-sm-2 col-xs-2">'+
				 '<span class="btn-append"> <i style="font-family:FontAwesome" class="far fa-minus-square remove_survey_option_edit"></i></span>'+
				' </div>'+
				 '</div>'+
				 '</div>';
			      }
			   }
			   main_div += '</div>';
			}
			main_div += '</div>';
			$('#survey_question_div_edit').append(main_div);
		     }
		  }
		     $("#upload_content_modal_survey_edit").modal("show");
		    
	       }
	    });

	 }
	 $(document).on('click','#survey_edit_file_previous_name_remove_button',function(){
	    $("#survey_edit_file_previous_name").html("");
	 });
	 
	 function update_content_survey() {
	    $("#create_content_error_survey_edit").html("");
	    var content_id = $("#edit_content_id_survey").val();
	    var main_category = $("#edit_content_id_main_category_survey").val();
	    var sub_category = $("#edit_content_id_sub_category_survey").val();
	    var content_title = $("#content_title_survey_edit").val();
	    
	    if (content_title == '') {
	       $("#create_content_error_survey_edit").html("Please Fill mandatory fields");
	       return false;
	    }else{
	       
	       var questions_array = {};
	       var i = 0;
	       var survey_error = 0;
	       $( ".survey_div_edit" ).each(function() {
		  var question_type = 0;
		  var question= '';
		  question_type = $(this).find('.survey_type_class_edit').val();
		  question = $(this).find('.survey_question_class_edit').val();
		  if (question == '') {
		     survey_error = 1;
		  }
		  var options_array = {};
		  if (question_type == '2' || question_type == '3') {
		     var j = 0;
		     var total_option = 0;
		     $( $(this).find('.survey_div_option_edit').find('.survey_option_count_edit') ).each(function() {
			var option_val = '';
			option_val = $(this).find('.survey_option_class_edit').val();
			if (typeof option_val === "undefined") {
			   
			}else{
			   if(option_val != ''){
			      total_option++;
			      options_array[j] = option_val;
			      j++;
			   }
			   
			}
			
		     });
		     if (question_type != 1 && total_option < 2) {
			survey_error = 1;
		     }
		  }
		  
		  
		  
		  questions_array[i] =  { question_type : question_type , question: question, options: options_array };
		  i++;
	       });
	       if (survey_error == '1') {
		  $("#create_content_error_survey_edit").html("Please Fill mandatory fields");
		  return false;
	       }
	       questions_array = JSON.stringify(questions_array);
	       $(".loading").show();
	       
	       var file_data = $("#survey_file_edit").prop("files")[0];
	       var file_previous_name = $("#survey_edit_file_previous_name").html();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       //form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", content_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name",file_previous_name);
	       form_data.append("questions", questions_array);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/update_content_survey",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     
		     content_builder(main_category,sub_category);
		     $("#update_content_survey")[0].reset();
		     $("#upload_content_modal_survey_edit").modal("hide");
		  }
	       });
	    }
	 }
	 

	

	 


	 


	 

	 



	
	
	 // on click of new question add add new question div
	 $(document).on('click', '.add_new_question_edit', function(){
	   var num_of_question = $("#total_survey_question_created_edit").val();
	    num_of_question++;
	    $("#total_survey_question_created_edit").val(num_of_question);
	    if (num_of_question > 9) {
	       num_of_question = "QUESTION "+num_of_question;
	    }else{
	       num_of_question = "QUESTION 0"+num_of_question;
	    }
	    
	    var new_div = '<div class="survey_div_edit">'+
				    '<div class="form-group">'+
				       '<label style="color:#f00f64">'+num_of_question+'</label>'+
				       '<label class="pull-right remove_survey_question_edit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				       '<select class="form-control survey_type_class_edit" name="survey_type_edit[]">'+
					  '<option value="1">Text Entry</option>'+
					  '<option value="2">Single Choice</option>'+
					  '<option value="3">Multi Coice</option>'+
				       '</select>'+
				    '</div>'+
				    '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_edit[]" class = "survey_question_class_edit"></textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				    //'</div><div class="survey_div_option"></div>'+
				 '</div>';
	    $("#survey_question_div_edit").append(new_div);
	 });
	 // remove question div
	 $(document).on('click', '.remove_survey_question_edit', function (){
	    $(this).parent().parent().remove();
	 });
	 // on change of survey type hide and show option
	 $(document).on('change', '.survey_type_class_edit', function(){
	    var survey_type = $(this).val();
	    
	    if (survey_type != '1') {
	       var new_options = '<div class="survey_div_option_edit"><legend style="margin-bottom:0px;font-size:14px; color:#414042">ANSWER OPTIONS  <small style="color:#999999">(Upto 6 Options)</small></legend>'+
	       '<div class="mui-textfield mui-textfield--float-label survey_option_count_edit">'+
                  '<input type="text" name="options_edit[]" class = "survey_option_class_edit">'+
                  '<label>Option A<sup>*</sup></label>'+
               '</div>'+
	       '<div class="form-group survey_option_count_edit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_edit[]" class = "survey_option_class_edit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option B<sup>*</sup></label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-plus-square add_survey_option_edit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>'+
	       '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_edit").find('.survey_div_option_edit').find('.survey_option_count_edit') ).each(function() {
			total_option++;
	       });
	       if (total_option == 0) {
		  $(this).closest(".survey_div_edit").append(new_options);
	       }
	       
	    }else{
	       $(this).closest(".survey_div_edit").find('.survey_div_option_edit').remove();
	    }
	 });
	 // on click add option add option
	 $(document).on('click', '.add_survey_option_edit', function(){
	    var new_options = '<div class="form-group survey_option_count_edit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_edit[]" class = "survey_option_class_edit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option</label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-minus-square remove_survey_option_edit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_option_edit").find('.survey_option_count_edit') ).each(function() {
			total_option++;
		     });
	       if (total_option < 6) {
		  $(this).closest(".survey_div_option_edit").append(new_options);
	       }
	       
	 });
	 // on click of button remove option
	 $(document).on('click', '.remove_survey_option_edit', function(){
	    $(this).parent().parent().parent().parent().remove();
	 });
	
	
	 // function to add pdf
	 function add_content_pdf() {
	    $("#create_content_error_pdf").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_pdf").val();
	    var content_title = $("#content_title_pdf").val();
	    var content_desc = $("#content_desc_pdf").val();
	    if (content_title == '') {
	       $("#create_content_error_pdf").html("Please Enter Title");
	       return false;
	    }else{
	       var category_id = '0';
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var file_data = $("#content_file_pdf").prop("files")[0];
	       if (typeof file_data === "undefined") {
		  $("#create_content_error_pdf").html("Please Select File");
		  return false;
	       }
	       $(".loading").show();
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_pdf",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_pdf").modal("hide");
		  }
	       });
	    }
	 }
	 
	 // update pdf type content
	 function update_content_pdf() {
	    $("#edit_content_error").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id_pdf").val();
	    var content_title = $("#content_title_edit_pdf").val();
	    var content_desc = $("#content_desc_edit_pdf").val();
	    var content_file = $("#content_file_edit_pdf").val();
	    var file_previous_name = $("#file_previous_name_pdf").html();
	    if (content_title == '') {
	       $("#edit_content_error_pdf").html("Please Enter Title");
	       return false;
	    }else{
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category_pdf").val();
	       sub_category = $("#edit_content_id_sub_category_pdf").val();
	       var file_data = $("#content_file_edit_pdf").prop("files")[0];
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_file", content_file);
	       form_data.append("content_id", content_id);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_pdf",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		    
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_edit_pdf").modal("hide");
		  }
	       });
	    }
	 }
	 

      
      

	
	 // function to add content upload type
	 function add_content_upload() {
	    $("#create_content_error_upload").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_upload").val();
	    var content_title = $("#content_title_upload").val();
	    var content_desc = $("#content_desc_upload").val();
	    if (content_title == '') {
	       $("#create_content_error_upload").html("Please Enter Title");
	       return false;
	    }else{
	       var upload_type = 0;
	       var image_allow = 0;
	       var video_allow = 0;
	       if ($('input[name="upload_content_user_image_allow"]:visible').is(':checked')) {
		  image_allow = 1;
		  upload_type = 1;
	       }
	       if ($('input[name="upload_content_user_video_allow"]:visible').is(':checked')) {
		  video_allow = 1;
		  upload_type = 2;
	       }
	       if (image_allow == 0 && video_allow == 0) {
		  $("#create_content_error_upload").html("Please atlist one content type allow");
		  return false;
	       }
	       if (image_allow == 1 && video_allow == 1) {
		  upload_type = 3;
	       }
	       var category_id = '0';
	       $(".loading").show();
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name", '');
	       form_data.append("upload_type", upload_type);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_upload",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#content_title_text").val('');
		     $("#content_desc_text").val('');
		     $("#upload_content_modal_upload").modal("hide");
		  }
	       });
	    }
	 }
	 
	 function update_content_upload() {
	    $("#edit_content_error_upload").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id_upload").val();
	    var content_title = $("#content_title_edit_upload").val();
	    var content_desc = $("#content_desc_edit_upload").val();
	   var file_previous_name = '';
	    if (content_title == '') {
	       $("#edit_content_error_upload").html("Please Enter Title");
	       return false;
	    }else{
	       var upload_type = 0;
	       var image_allow = 0;
	       var video_allow = 0;
	       if ($('input[name="upload_content_user_image_allow_edit"]:visible').is(':checked')) {
		  image_allow = 1;
		  upload_type = 1;
	       }
	       if ($('input[name="upload_content_user_video_allow_edit"]:visible').is(':checked')) {
		  video_allow = 1;
		  upload_type = 2;
	       }
	       if (image_allow == 0 && video_allow == 0) {
		  $("#edit_content_error_upload").html("Please atlist one content type allow");
		  return false;
	       }
	       if (image_allow == 1 && video_allow == 1) {
		  upload_type = 3;
	       }
	       
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category_upload").val();
	       sub_category = $("#edit_content_id_sub_category_upload").val();
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append('content_desc',content_desc);
	       form_data.append("content_id", content_id);
	       form_data.append("file_previous_name", file_previous_name);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("upload_type", upload_type);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_upload",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		    
		     content_builder(main_category,sub_category);
		     $("#upload_content_modal_edit_upload").modal("hide");
		  }
	       });
	    }
	 }
	 
	 
	  // function to add sub category content
	 function add_content_wiki() {
	    $('.wikipedia_temp_data').html('');
	    $("#create_content_error_wiki").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_wiki").val();
	    var content_title = $("#content_title_wiki").val();
	    var wiki_title = $("#wiki_title_url").val();
	    if (content_title == '' || wiki_title == '') {
	       $("#create_content_error_wiki").html("Please Enter Title");
	       return false;
	    }else{
	       var category_id = '0';
	       $(".loading").show();
	       if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       
	       
	       var search = wiki_title;
	       if (search.indexOf("http://") == 0 || search.indexOf("https://") == 0) {
		  var index = search.lastIndexOf("/") + 1;
		  search = search.substr(index);
	       }
	       $.ajax({
		  type: "GET",
		  url: "https://en.wikipedia.org/w/api.php?action=parse&format=json&prop=text|images&page="+search+"&callback=?",
		  contentType: "application/json; charset=utf-8",
		  async: false,
		  dataType: "json",
		  success: function (data, textStatus, jqXHR) {
		     
		     //alert(JSON.stringify(data));
		     //console.log(data);
		     if (typeof data.parse == "undefined") {
			$("#create_content_error_wiki").html("No data found");
			$(".loading").hide();
		     }
		     else {
			var markup = data.parse.text["*"];
			var title = data.parse.title;
			var blurb = $('<div></div>').html(markup);
			blurb.find('a').each(function() { $(this).replaceWith($(this).html()); });
			blurb.find('sup').remove();
			blurb.find('.mw-ext-cite-error').remove();
			blurb.find('.mw-editsection').remove();
			blurb.find('h2').children('span#References').remove();
			blurb.find('h2').children('span#External_links').remove();
			blurb.find('.reflist').remove();
			blurb.find('.plainlinks').remove();
			blurb.find('.navbox').remove();
			$('.wikipedia_temp_data').html(blurb);
			var modified_html = $('.wikipedia_temp_data').html();
			markup = modified_html;
			//var markup = JSON.stringify(data.parse.text["*"]);
			var title = data.parse.title;
			var pageid = data.parse.pageid;
			
			var form_data = new FormData();
			form_data.append('category_id',category_id);
			form_data.append('is_main_category',is_main_category);
			form_data.append('content_title',content_title);
			form_data.append('wikipedia_title_url',wiki_title);
			form_data.append('wikipedia_title',title);
			form_data.append('wikipedia_pageid',pageid);
			form_data.append('wikipedia_content',markup);
			form_data.append("content_id", '');
			form_data.append("location_uid", $("#location_id_hidden").val());
			form_data.append("locationid", $("#created_location_id").val());
			$.ajax({
			   type: "POST",
			   url: "<?php echo base_url()?>location/add_content_wiki",
			   cache: false,
			   contentType: false,
			   processData: false,
			   data: form_data,
			   success: function(data){
			      if (data == '1') {
				 content_builder(main_category,sub_category);
				 $("#wiki_title").val('');
				 $("#upload_content_modal_wiki").modal("hide");
			      }else{
				 $("#create_content_error_wiki").html("No data found");
				 $(".loading").hide();
			      }
			      
			   }
			});
			
		     }
		  },
		  error: function (errorMessage) {
		     $(".loading").hide();
		  }
	       });
	       
	       
	       
	       
	    }
	 }
	 
	 function update_content_wiki() {
	    $('.wikipedia_temp_data').html('');
	    $("#edit_content_error_wiki").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = 0;
	    var category_id = 0;
	    var content_id = $("#edit_content_id_wiki").val();
	    var content_title = $("#content_title_edit_wiki").val();
	    var wiki_title = $("#wiki_title_url_edit").val();
	    if (content_title == '' || wiki_title == '') {
	       $("#edit_content_error_wiki").html("Please Enter Title");
	       return false;
	    }else{
	       $(".loading").show();
	       main_category = $("#edit_content_id_main_category_wiki").val();
	       sub_category = $("#edit_content_id_sub_category_wiki").val();
	       
	       
	       var search = wiki_title;
	       if (search.indexOf("http://") == 0 || search.indexOf("https://") == 0) {
		  var index = search.lastIndexOf("/") + 1;
		  search = search.substr(index);
	       }
	       $.ajax({
		  type: "GET",
		  url: "https://en.wikipedia.org/w/api.php?action=parse&format=json&prop=text|images&page="+search+"&callback=?",
		  contentType: "application/json; charset=utf-8",
		  async: false,
		  dataType: "json",
		  success: function (data, textStatus, jqXHR) {
		     
		     //alert(JSON.stringify(data));
		     //console.log(data);
		     if (typeof data.parse == "undefined") {
			$("#edit_content_error_wiki").html("No data found");
			$(".loading").hide();
		     }
		     else {
			var markup = data.parse.text["*"];
			var title = data.parse.title;
			var blurb = $('<div></div>').html(markup);
			blurb.find('a').each(function() { $(this).replaceWith($(this).html()); });
			blurb.find('sup').remove();
			blurb.find('.mw-ext-cite-error').remove();
			blurb.find('.mw-editsection').remove();
			blurb.find('h2').children('span#References').remove();
			blurb.find('h2').children('span#External_links').remove();
			blurb.find('.reflist').remove();
			blurb.find('.plainlinks').remove();
			blurb.find('.navbox').remove();
			$('.wikipedia_temp_data').html(blurb);
			var modified_html = $('.wikipedia_temp_data').html();
			markup = modified_html;
			//var markup = JSON.stringify(data.parse.text["*"]);
			var title = data.parse.title;
			var pageid = data.parse.pageid;
			
			var form_data = new FormData();
			form_data.append('category_id',category_id);
			form_data.append('is_main_category',is_main_category);
			form_data.append('content_title',content_title);
			form_data.append('wikipedia_title_url',wiki_title);
			form_data.append('wikipedia_title',title);
			form_data.append('wikipedia_pageid',pageid);
			form_data.append('wikipedia_content',markup);
			form_data.append("content_id", content_id);
			form_data.append("location_uid", $("#location_id_hidden").val());
			form_data.append("locationid", $("#created_location_id").val());
			$.ajax({
			   type: "POST",
			   url: "<?php echo base_url()?>location/add_content_wiki",
			   cache: false,
			   contentType: false,
			   processData: false,
			   data: form_data,
			   success: function(data){
			      if (data == '1') {
				 content_builder(main_category,sub_category);
				 $("#wiki_title").val('');
				 $("#upload_content_modal_edit_wiki").modal("hide");
			      }else{
				 $("#edit_content_error_wiki").html("No data found");
				 $(".loading").hide();
			      }
			      
			   }
			});
			
		     }
		  },
		  error: function (errorMessage) {
		     $(".loading").hide();
		  }
	       });
	       
	       
	       
	       
	    }
	 }
	 
	 
	 
      
      
	// for audit survey
	// on click of new question add add new question div
	 $(document).on('click', '.add_new_question_retail_audit', function(){
	    var num_of_question = $("#total_survey_question_created_retail_audit").val();
	    num_of_question++;
	    $("#total_survey_question_created_retail_audit").val(num_of_question);
	    if (num_of_question > 9) {
	       num_of_question = "QUESTION "+num_of_question;
	    }else{
	       num_of_question = "QUESTION 0"+num_of_question;
	    }
	    
	    var new_div = '<div class="survey_div_retail_audit">'+
				    '<div class="form-group">'+
				       '<label style="color:#f00f64">'+num_of_question+'</label>'+
				       '<label style="padding-left:20%;">Deduct: <input type="text" class= "health_deduct_retail_audit" name="health_deduct_retail_audit[]" style="width:50px;"> % from store health</label>'+
				       '<label class="pull-right remove_survey_question_retail_audit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				       /*'<select class="form-control survey_type_class_retail_audit" name="survey_type_retail_audit[]">'+
					  '<option value="">Select Survey Type</option>'+
					  '<option value="2">Single Choice</option>'+
					  '<option value="3">Multi Coice</option>'+
				       '</select>'+*/
				    '</div>'+
				    
				    '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_retail_audit[]" class = "survey_question_class_retail_audit"></textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				       '</div>'+
				    //'</div><div class="survey_div_option"></div>'+
				    '<div class="form-group">'+
					'<div class="checkbox" style="margin-top:0px">'+
					    '<label>'+
					    '<input  class= "use_sku_survey_retail_audit" name="use_sku_survey_retail_audit[]" value="1" type="checkbox"> User SKU List'+
					    '</label>'+
				       '</div>'+
				    '</div>'+
				 '</div>';
	    $("#survey_question_div_retail_audit").append(new_div);
	 });
	$(document).on('click', '.remove_survey_question_retail_audit', function (){
	    $(this).parent().parent().remove();
	 });
	// on change of survey type hide and show option
	 $(document).on('change', '.survey_type_class_retail_audit', function(){
	    var survey_type = $(this).val();
	    if (survey_type != '1') {
	       var new_options = '<div class="survey_div_option_retail_audit"><legend style="margin-bottom:0px;font-size:14px; color:#414042">ANSWER OPTIONS  <small style="color:#999999">(Upto 6 Options)</small></legend>'+
	       '<div class="mui-textfield mui-textfield--float-label survey_option_count_retail_audit">'+
                  '<input type="text" name="options_retail_audit[]" class = "survey_option_class_retail_audit">'+
                  '<label>Option A<sup>*</sup></label>'+
               '</div>'+
	       '<div class="form-group survey_option_count_retail_audit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_retail_audit[]" class = "survey_option_class_retail_audit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option B<sup>*</sup></label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-plus-square add_survey_option_retail_audit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>'+
	       '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_retail_audit").find('.survey_div_option_retail_audit').find('.survey_option_count_retail_audit') ).each(function() {
			total_option++;
	       });
	       if (total_option == 0) {
		  $(this).closest(".survey_div_retail_audit").append(new_options);
	       }
	       
	    }else{
	       $(this).closest(".survey_div_retail_audit").find('.survey_div_option_retail_audit').remove();
	    }
	 });
	// on click add option add option
	 $(document).on('click', '.add_survey_option_retail_audit', function(){
	    var new_options = '<div class="form-group survey_option_count_retail_audit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_retail_audit[]" class = "survey_option_class_retail_audit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option</label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-minus-square remove_survey_option_retail_audit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_option_retail_audit").find('.survey_option_count_retail_audit') ).each(function() {
			total_option++;
		     });
	       if (total_option < 6) {
		  $(this).closest(".survey_div_option_retail_audit").append(new_options);
	       }
	       
	 });
	 $(document).on('click', '.remove_survey_option_retail_audit', function(){
	    $(this).parent().parent().parent().parent().remove();
	 });
	 function add_content_survey_retail_audit() {
	    $("#create_content_error_survey_retail_audit").html("");
	    var main_category = 0;
	    var sub_category = 0;
	    var is_main_category = $("#is_main_category_survey_retail_audit").val();
	    var content_title = $("#content_title_survey_retail_audit").val();
	   
	    if (content_title == '') {
	       $("#create_content_error_survey_retail_audit").html("Please Fill mandatory fields");
	       return false;
	    }else{
	      if (is_main_category == 1) {
		  $( ".main_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('main_id');
			main_category = category_id;
		     }
		  });
	       }else{
		  $( ".sub_section" ).each(function() {
		     if($( this ).hasClass( "offline_active" )){
			category_id = $(this).data('sub_id');
			sub_category = category_id;
			main_category =  $(this).data('main_id');
		     }
		  });
	       }
	       var questions_array = {};
	       var i = 0;
	       var survey_error = 0;
	       $( ".survey_div_retail_audit" ).each(function() {
		  var question_type = 0;
		  var health_deduct = 0;
		  var is_sku_use = 0;
		  var question= '';
		    if ($(this).find('.use_sku_survey_retail_audit').is(':checked')) {
			is_sku_use = 1;
		    }
		  health_deduct = $(this).find('.health_deduct_retail_audit').val();
		  //question_type = $(this).find('.survey_type_class_retail_audit').val();
		  question_type = '2';
		  question = $(this).find('.survey_question_class_retail_audit').val();
		  if (question == '') {
		     survey_error = 1;
		  }
		  var options_array = {};
		  /*if (question_type == '2' || question_type == '3') {
		     var j = 0;
		     var total_option = 0;
		     $( $(this).find('.survey_div_option_retail_audit').find('.survey_option_count_retail_audit') ).each(function() {
			var option_val = '';
			option_val = $(this).find('.survey_option_class_retail_audit').val();
			if (typeof option_val === "undefined") {
			   
			}else{
			   if(option_val != ''){
			      total_option++;
			      options_array[j] = option_val;
			      j++;
			   }
			   
			}
			
		     });
		     if (question_type != 1 && total_option < 2) {
			survey_error = 1;
		     }
		  }*/
		  
		  
		  
		  questions_array[i] =  {is_sku_use: is_sku_use, health_deduct: health_deduct, question_type : question_type , question: question, options: options_array };
		  i++;
	       });
	       if (survey_error == '1') {
		  $("#create_content_error_survey_retail_audit").html("Please Fill mandatory fields");
		  return false;
	       }
	        $(".loading").show();
	       questions_array = JSON.stringify(questions_array); 
	       //var file_data = $("#survey_file_retail_audit").prop("files")[0];
	       var file_data = '';
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       form_data.append('category_id',category_id);
	       form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", '');
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("questions", questions_array);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/add_content_survey_retail_audit",
		  cache: false,
		  contentType: false,
		  processData: false,
		  dataType: "json",
		  data: form_data,
		  success: function(data){
		     if (is_main_category == '1') {
			sub_category = data;
		     }
		     content_builder(main_category,sub_category);
		     $("#add_content_survey_retail_audit")[0].reset();
		     $("#upload_content_modal_survey_retail_audit").modal("hide");
		  }
	       });
	    }
	 }
	function survey_content_edit_modal_open_retail_audit(main_id,sub_id, content_id, content_title, content_file, content_desc, content_type){
	    $("#edit_content_id_survey_retail_audit").val(content_id);
	       $("#edit_content_id_main_category_survey_retail_audit").val(main_id);
	       $("#edit_content_id_sub_category_survey_retail_audit").val(sub_id);
	       $("#content_title_survey_edit_retail_audit").val(unescape(content_title));
	       if (content_file != '') {
		  $("#survey_edit_file_previous_name_div_retail_audit").show();
	       }else{
		  $("#survey_edit_file_previous_name_div_retail_audit").hide();
	       }
	       //$("#survey_edit_file_previous_name_retail_audit").html(content_file);
	       $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/get_content_poll_survey_retail_audit",
	       data: "content_id="+content_id,
	       dataType: 'json',
	       success: function(data){
		  $(".loading").hide();
		  $('#survey_question_div_edit_retail_audit').html("");
		  if (data.question.length > 0) {
		    $("#total_survey_question_created_edit_retail_audit").val(data.question.length);
		     for (var i = 0; i < data.question.length; i++) {
			var obj = data.question[i];
			var question_type = obj.survey_type;
			var question = obj.question;
			var options_array = obj.options;
			var main_div = '';
			var question_number = i+1;
			if (question_number < 10) {
			   question_number = '0'+question_number;
			}
			var level = '';
			if (i != 0) {
			   level =  '<label class="pull-right remove_survey_question_edit_retail_audit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>';
			}
			var text_entry = ''; var single_choice = ''; var multi_choice = '';
			if (question_type == '1') {
			   text_entry = 'selected';
			}
			else if (question_type == '2') {
			   single_choice = 'selected';
			}
			else if (question_type == '3') {
			   multi_choice = 'selected';
			}
			var checked = '';
			if (obj.is_use_sku == '1') {
			    checked = "checked";
			}
			main_div += '<div class="survey_div_edit_retail_audit">';
			main_div += '<div class="form-group">'+
					  '<label style="color:#f00f64">QUESTION '+question_number+'</label>'+level+
					  '<label style="padding-left:20%;">Deduct: <input type="text" class= "health_deduct_edit_retail_audit" name="health_deduct_edit_retail_audit[]" value="'+obj.health_deduction+'" style="width:50px;"> % from store health</label>'+
					  /*'<select class="form-control survey_type_class_edit_retail_audit" name="survey_type_edit_retail_audit[]">'+
					    '<option value="2" '+single_choice+'>Single Choice</option>'+
					     '<option value="3" '+multi_choice+'>Multi Coice</option>'+
					  '</select>'+*/
				       '</div>';
			main_div += '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_edit_retail_audit[]" class = "survey_question_class_edit_retail_audit">'+question+'</textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				    '</div>';
			main_div += '<div class="form-group">'+
					'<div class="checkbox" style="margin-top:0px">'+
					    '<label>'+
					    '<input '+checked+'  class= "use_sku_survey_edit_retail_audit" name="use_sku_survey_edit_retail_audit[]" value="1" type="checkbox"> User SKU List'+
					    '</label>'+
				       '</div>'+
				    '</div>';
			/*if (options_array.length > 0) {
			   main_div += '<div class="survey_div_option_edit_retail_audit">';
			   main_div += '<legend style="margin-bottom:0px;font-size:14px; color:#414042">'+
					  'ANSWER OPTIONS'+
					  '<small style="color:#999999">(Upto 6 Options)</small>'+
					  '</legend>';
			   for (var j = 0; j < options_array.length; j++) {
			      var op_obj = options_array[j];
			      var option_val = op_obj.option_text;
			      if (j == 0) {
			      main_div += '<div class="mui-textfield mui-textfield--float-label survey_option_count_edit_retail_audit">'+
					  '<input class="survey_option_class_edit_retail_audit mui--is-empty mui--is-untouched mui--is-pristine" type="text" name="options_edit_retail_audit[]" value="'+option_val+'">'+
					  '<label>'+
					  'Option'+
					  '<sup>*</sup>'+
					  '</label>'+
					  '</div>';
			      }else if (j == 1) {
				 main_div += '<div class="form-group survey_option_count_edit_retail_audit">'+
				 '<div class="row">'+
				 '<div class="col-sm-10 col-xs-10 nopadding-right">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				 '<input type="text" class="survey_option_class_edit_retail_audit mui--is-empty mui--is-untouched mui--is-pristine" name="options_edit_retail_audit[]" value="'+option_val+'">'+
				 '<label style="font-weight:300;color:#a7a9ac">Option<sup>*</sup></label>'+
				 '</div>'+
				 '</div>'+
				 '<div style="padding-top:3%" class="col-sm-2 col-xs-2">'+
				 '<span class="btn-append"> <i style="font-family:FontAwesome" class="far fa-plus-square add_survey_option_edit_retail_audit"></i></span>'+
				' </div>'+
				 '</div>'+
				 '</div>';
			      }
			      else{
				 main_div += '<div class="form-group survey_option_count_edit_retail_audit">'+
				 '<div class="row">'+
				 '<div class="col-sm-10 col-xs-10 nopadding-right">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				 '<input type="text" class="survey_option_class_edit_retail_audit mui--is-empty mui--is-untouched mui--is-pristine" name="options_edit_retail_audit[]" value="'+option_val+'">'+
				 '<label style="font-weight:300;color:#a7a9ac">Option<sup>*</sup></label>'+
				 '</div>'+
				 '</div>'+
				 '<div style="padding-top:3%" class="col-sm-2 col-xs-2">'+
				 '<span class="btn-append"> <i style="font-family:FontAwesome" class="far fa-minus-square remove_survey_option_edit_retail_audit"></i></span>'+
				' </div>'+
				 '</div>'+
				 '</div>';
			      }
			   }
			   main_div += '</div>';
			}*/
			main_div += '</div>';
			$('#survey_question_div_edit_retail_audit').append(main_div);
		     }
		  }
		     $("#upload_content_modal_survey_edit_retail_audit").modal("show");
		    
	       }
	    });

	 }
	 $(document).on('click','#survey_edit_file_previous_name_remove_button_retail_audit',function(){
	    $("#survey_edit_file_previous_name_retail_audit").html("");
	 });
	 
	// on click of new question add add new question div
	 $(document).on('click', '.add_new_question_edit_retail_audit', function(){
	   var num_of_question = $("#total_survey_question_created_edit_retail_audit").val();
	    num_of_question++;
	    $("#total_survey_question_created_edit_retail_audit").val(num_of_question);
	    if (num_of_question > 9) {
	       num_of_question = "QUESTION "+num_of_question;
	    }else{
	       num_of_question = "QUESTION 0"+num_of_question;
	    }
	    
	    var new_div = '<div class="survey_div_edit_retail_audit">'+
				    '<div class="form-group">'+
				       '<label style="color:#f00f64">'+num_of_question+'</label>'+
				       '<label style="padding-left:20%;">Deduct: <input type="text" class= "health_deduct_edit_retail_audit" name="health_deduct_edit_retail_audit[]" style="width:50px;"> % from store health</label>'+
				       '<label class="pull-right remove_survey_question_edit_retail_audit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				       /*'<select class="form-control survey_type_class_edit_retail_audit" name="survey_type_edit_retail_audit[]">'+
					  '<option value="">Select Survey Type</option>'+
					  '<option value="2">Single Choice</option>'+
					  '<option value="3">Multi Coice</option>'+
				       '</select>'+*/
				    '</div>'+
				    
				    '<div class="mui-textfield mui-textfield--float-label">'+
				       '<textarea name="survey_question_edit_retail_audit[]" class = "survey_question_class_edit_retail_audit"></textarea>'+
				       '<label>Question<sup>*</sup></label>'+
				       '</div>'+
				    //'</div><div class="survey_div_option"></div>'+
				    '<div class="form-group">'+
					'<div class="checkbox" style="margin-top:0px">'+
					    '<label>'+
					    '<input  class= "use_sku_survey_edit_retail_audit" name="use_sku_survey_edit_retail_audit[]" value="1" type="checkbox"> User SKU List'+
					    '</label>'+
				       '</div>'+
				    '</div>'+
				 '</div>';
	    $("#survey_question_div_edit_retail_audit").append(new_div);
	 });
	 // remove question div
	 $(document).on('click', '.remove_survey_question_edit_retail_audit', function (){
	    $(this).parent().parent().remove();
	 });
	 // on change of survey type hide and show option
	 $(document).on('change', '.survey_type_class_edit_retail_audit', function(){
	    var survey_type = $(this).val();
	    if (survey_type != '1') {
	       var new_options = '<div class="survey_div_option_edit_retail_audit"><legend style="margin-bottom:0px;font-size:14px; color:#414042">ANSWER OPTIONS  <small style="color:#999999">(Upto 6 Options)</small></legend>'+
	       '<div class="mui-textfield mui-textfield--float-label survey_option_count_edit_retail_audit">'+
                  '<input type="text" name="options_edit_retail_audit[]" class = "survey_option_class_edit_retail_audit">'+
                  '<label>Option A<sup>*</sup></label>'+
               '</div>'+
	       '<div class="form-group survey_option_count_edit_retail_audit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_edi_retail_auditt[]" class = "survey_option_class_edit_retail_audit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option B<sup>*</sup></label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-plus-square add_survey_option_edit_retail_audit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>'+
	       '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_edit_retail_audit").find('.survey_div_option_edit_retail_audit').find('.survey_option_count_edit_retail_audit') ).each(function() {
			total_option++;
	       });
	       if (total_option == 0) {
		  $(this).closest(".survey_div_edit_retail_audit").append(new_options);
	       }
	       
	    }else{
	       $(this).closest(".survey_div_edit_retail_audit").find('.survey_div_option_edit_retail_audit').remove();
	    }
	 });
	 // on click add option add option
	 $(document).on('click', '.add_survey_option_edit_retail_audit', function(){
	    var new_options = '<div class="form-group survey_option_count_edit_retail_audit">'+
                  '<div class="row">'+
                     '<div class="col-sm-10 col-xs-10 nopadding-right">'+
                        '<div class="mui-textfield mui-textfield--float-label">'+
                           '<input type="text" name="options_edit_retail_audit[]" class = "survey_option_class_edit_retail_audit">'+
                           '<label style="font-weight:300;color:#a7a9ac">Option</label>'+
                        '</div>'+
                     '</div>'+
                     '<div class="col-sm-2 col-xs-2" style="padding-top:3%">'+
                        '<span class="btn-append"> '+
                        '<i class="far fa-minus-square remove_survey_option_edit_retail_audit" style="font-family:FontAwesome"></i>'+
                        '</span> '+
                     '</div>'+
                  '</div>'+
               '</div>';
	       var total_option = 0;
	       $( $(this).closest(".survey_div_option_edit_retail_audit").find('.survey_option_count_edit_retail_audit') ).each(function() {
			total_option++;
		     });
	       if (total_option < 6) {
		  $(this).closest(".survey_div_option_edit_retail_audit").append(new_options);
	       }
	       
	 });
	 // on click of button remove option
	 $(document).on('click', '.remove_survey_option_edit_retail_audit', function(){
	    $(this).parent().parent().parent().parent().remove();
	 });
	
	
	 
	
	 function update_content_survey_retail_audit() {
	    $("#create_content_error_survey_edit_retail_audit").html("");
	    var content_id = $("#edit_content_id_survey_retail_audit").val();
	    var main_category = $("#edit_content_id_main_category_survey_retail_audit").val();
	    var sub_category = $("#edit_content_id_sub_category_survey_retail_audit").val();
	    var content_title = $("#content_title_survey_edit_retail_audit").val();
	    
	    if (content_title == '') {
	       $("#create_content_error_survey_edit_retail_audit").html("Please Fill mandatory fields");
	       return false;
	    }else{
	       
	       var questions_array = {};
	       var i = 0;
	       var survey_error = 0;
	       $( ".survey_div_edit_retail_audit" ).each(function() {
		  var question_type = 0;
		  var health_deduct = 0;
		  var is_sku_use = 0;
		  var question= '';
		    if ($(this).find('.use_sku_survey_edit_retail_audit').is(':checked')) {
			is_sku_use = 1;
		    }
		  health_deduct = $(this).find('.health_deduct_edit_retail_audit').val();
		  var question= '';
		  //question_type = $(this).find('.survey_type_class_edit_retail_audit').val();
		  question_type = '2';
		  question = $(this).find('.survey_question_class_edit_retail_audit').val();
		  if (question == '') {
		     survey_error = 1;
		  }
		  var options_array = {};
		  /*if (question_type == '2' || question_type == '3') {
		     var j = 0;
		     var total_option = 0;
		     $( $(this).find('.survey_div_option_edit_retail_audit').find('.survey_option_count_edit_retail_audit') ).each(function() {
			var option_val = '';
			option_val = $(this).find('.survey_option_class_edit_retail_audit').val();
			if (typeof option_val === "undefined") {
			   
			}else{
			   if(option_val != ''){
			      total_option++;
			      options_array[j] = option_val;
			      j++;
			   }
			   
			}
			
		     });
		     if (question_type != 1 && total_option < 2) {
			survey_error = 1;
		     }
		  }*/
		  
		  
		  
		  questions_array[i] =  {is_sku_use: is_sku_use, health_deduct: health_deduct, question_type : question_type , question: question, options: options_array };
		  i++;
	       });
	       if (survey_error == '1') {
		  $("#create_content_error_survey_edit_retail_audit").html("Please Fill mandatory fields");
		  return false;
	       }
	       questions_array = JSON.stringify(questions_array);
	       $(".loading").show();
	       
	       /*var file_data = $("#survey_file_edit_retail_audit").prop("files")[0];
	       var file_previous_name = $("#survey_edit_file_previous_name_retail_audit").html();*/
	       var file_data = "";
	       var file_previous_name = "";
	       var form_data = new FormData();
	       form_data.append("file", file_data);
	       //form_data.append('is_main_category',is_main_category);
	       form_data.append('content_title',content_title);
	       form_data.append("content_id", content_id);
	       form_data.append("location_uid", $("#location_id_hidden").val());
	       form_data.append("locationid", $("#created_location_id").val());
	       form_data.append("file_previous_name",file_previous_name);
	       form_data.append("questions", questions_array);
	       $.ajax({
		  type: "POST",
		  url: "<?php echo base_url()?>location/update_content_survey_retail_audit",
		  cache: false,
		  contentType: false,
		  processData: false,
		  data: form_data,
		  success: function(data){
		     
		     content_builder(main_category,sub_category);
		     $("#update_content_survey_retail_audit")[0].reset();
		     $("#upload_content_modal_survey_edit_retail_audit").modal("hide");
		  }
	       });
	    }
	 }
	</script>
		
		
	
   <script>
      var contestifi_array;
      function contestifi_content_builder(created_id= '', created_quiz_id = '' , created_prize_id = '')
      {
	 $(".loading").show();
	 var location_uid =  $("#location_id_hidden").val();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/lms_contestifi_content_builder_list",
	    data: "location_uid="+location_uid,
	    dataType: 'json',
	    success: function(data)
	    {
	       contestifi_array = data;
	       $(".loading").hide();
	       if (data.contest.length > 0)
	       {
		  $('.contestifi_section_content_list').html('');
		  for (var i = 0; i < data.contest.length; i++)
		  {
		     var contest = data.contest[i];
		     var main_class = '';
		     if (created_id == '')
		     {
			if (i == 0)
			{
			   main_class = 'offline_active';
			   list_contetifi_quiz_question(contest.id,created_quiz_id);
			   list_contetifi_conetst_prize(contest.id,created_prize_id);
			}
		     }
		     else
		     {
			if (contest.id == created_id)
			{
			   main_class = 'offline_active';
			   list_contetifi_quiz_question(contest.id,created_quiz_id);
			   list_contetifi_conetst_prize(contest.id,created_prize_id);
			}
		     }
		     var content_html = '<li class = "contestifi_contest_section '+main_class+'"  data-contestifi_contest_id="'+contest.id+'"  data-contestifi_contest_type="'+contest.contest_type+'"><span>'+contest.contest_name+'</span><span class="offline-fa">'+
					  '<i onclick="edit_contestifi_contest('+contest.id+')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
					  '</span>'+
					  '<span class="offline-fa"><i onclick="delete_contestifi_contest('+contest.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
					  '<span class="offline-fa"><i onclick="lms_get_contest_location('+contest.id+')" class="fa fa-map-marker fa-lg" aria-hidden="true"></i></span>'+
				       '</li>';
		     $('.contestifi_section_content_list').append(content_html);
		  }
		  // for contest section
		  $(".contestifi_content_filled_div").show();
		  $(".contestifi_content_blank_div").hide();
	       }
	       else
	       {
		  // for contest section
		  $(".contestifi_content_blank_div").show();
		  $(".contestifi_content_filled_div").hide();
		  
		  // for quiz section
		  $(".contestifi_quiz_blank_first_div").show();
		  $(".contestifi_quiz_filled_div").hide();
		  $(".contestifi_quiz_blank_div").hide();
		  
		  // for prize section
		  $(".contestifi_prize_blank_first_div").show();
		  $(".contestifi_prize_filled_div").hide();
		  $(".contestifi_prize_blank_div").hide();
	       }
	    }
	 });
      }
      function lms_get_contest_location(content_id) {
	$("#lms_add_location_contest_id").val(content_id);
	$("#lms_add_contest_location_error").html("");
	    $("#lms_contest_location_list").modal("show");
	    $('#lms_contest_group_location_list').html('<tr><td colspan="3" style="text-align:center">Please Wait</td></tr>');
	 
	    $.ajax({
			   url: base_url+'location/lms_get_content_location',
			   type: 'POST',
			   dataType: 'json',
			   data: ({'content_id': content_id, 'is_contest' : '1'}),
			   success: function(data){
			     $('#lms_contest_group_location_list').html(data.search_results);
			   }
	    });
	}
    function lms_add_contest_location(){
	$("#lms_add_contest_location_error").html("");
	var content_id = $("#lms_add_location_contest_id").val();
	var is_contest = 1;
	var locations = [];
	$.each($("input[name='lms_content_location_list[]']:checked"), function(){            
	    locations.push($(this).val());
	    is_loc_remove = 1;
	});
	$('.loading').show();
	$.ajax({
	    url: base_url+'location/lms_add_content_location',
	    type: 'POST',
	    dataType: 'json',
	    data: ({'content_id': content_id,'is_contest': is_contest, 'locations':locations}),
	    success: function(data){
		$('.loading').hide();
		if(data.resultCode == '0'){
		    $("#lms_add_contest_location_error").html(data.resultMsg);
		}
		else{
			  
		    $("#lms_add_contest_location_error").html("Successfully Updated locations");
		}
	    }
	});
    }
      
      // function to list the contestifi quiz
      function list_contetifi_quiz_question(contest_id,contest_quiz_id = '')
      {
	 $('.contestifi_quiz_list').html('');
	 var content = '';
	 for (var i = 0; i < contestifi_array.contest.length; i++)
	 {
	    var main_contest = contestifi_array.contest[i];
	    if (main_contest.id == contest_id)
	    {
	       content = main_contest.quiz_questions;
	    }
	 }
	 if (content.length > 0)
	 {
	    // for sub section
	    $(".contestifi_quiz_filled_div").show();
	    $(".contestifi_quiz_blank_first_div").hide();
	    $(".contestifi_quiz_blank_div").hide();
	    for (var i = 0; i < content.length; i++)
	    {
	       var quiz_question = content[i];
	       var main_class = '';
	       if (contest_quiz_id == '')
	       {
		  if (i == 0)
		  {
		     main_class = 'offline_active';
		  }
	       }
	       else
	       {
		  if (quiz_question.id == contest_quiz_id)
		  {
		     main_class = 'offline_active';
		  }
	       }
	       var content_html = '<li  class = "contestifi_quiz_section '+main_class+'"  data-contestifi_contest_id="'+contest_id+'"  data-contestifi_quiz_question_id="'+quiz_question.id+'"  ><span>'+quiz_question.question+'</span><span class="offline-fa">'+
				    '<i onclick="edit_contestifi_quiz_question('+contest_id+','+quiz_question.id+')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
				    '</span>'+
				    '<span class="offline-fa"><i onclick="delte_contestifi_quiz_question('+quiz_question.id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
				 '</li>';
	       $('.contestifi_quiz_list').append(content_html);
	    }
	 }
	 else
	 {
	    // for quiz section
	    $(".contestifi_quiz_blank_div").show();
	    $(".contestifi_quiz_blank_first_div").hide();
	    $(".contestifi_quiz_filled_div").hide();  
	 }
      }
      
      function list_contetifi_conetst_prize(contest_id,contest_prize_id = '')
      {
	 $('.contestifi_prize_list').html('');
	 var content = '';
	 for (var i = 0; i < contestifi_array.contest.length; i++)
	 {
	    var main_contest = contestifi_array.contest[i];
	    if (main_contest.id == contest_id)
	    {
	       content = main_contest.contest_prize;
	    }
	 }
	 if (content.length > 0)
	 {
	    // for sub section
	    $(".contestifi_prize_filled_div").show();
	    $(".contestifi_prize_blank_first_div").hide();
	    $(".contestifi_prize_blank_div").hide();
	    for (var i = 0; i < content.length; i++)
	    {
	       var quiz_question = content[i];
	       var main_class = '';
	       if (contest_prize_id == '')
	       {
		  if (i == 0)
		  {
		     main_class = 'offline_active';
		  }
	       }
	       else
	       {
		  if (quiz_question.prize_id == contest_prize_id)
		  {
		     main_class = 'offline_active';
		  }
	       }
	       var contest_prize = escape(quiz_question.contest_prize);
	       var content_html = '<li  class = "contestifi_prize_section '+main_class+'"  data-contestifi_contest_id="'+contest_id+'"  data-contestifi_contest_prize_id="'+quiz_question.id+'"  ><span>'+quiz_question.contest_prize+'</span><span class="offline-fa">'+
				    '<i onclick="edit_contestifi_contest_prize('+contest_id+','+quiz_question.prize_id+', \''+contest_prize+'\')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'+
				    '</span>'+
				    '<span class="offline-fa"><i onclick="delte_contestifi_contest_prize('+quiz_question.prize_id+')" class="fa fa-trash fa-lg" aria-hidden="true"></i></span>'+
				 '</li>';
	       $('.contestifi_prize_list').append(content_html);
	    }
	 }
	 else
	 {
	    // for quiz section
	    $(".contestifi_prize_blank_div").show();
	    $(".contestifi_prize_blank_first_div").hide();
	    $(".contestifi_prize_filled_div").hide();  
	 }
      }
      
      function open_add_contestifi_quiz()
      {
	 var contest_type = 1;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_type  = $(this).data('contestifi_contest_type');
	    }
	 });
	 if (contest_type == '1')
	 {
	    $("#contestifi_contest_image_contest_div").hide();
	 }
	 else
	 {
	    
	    $("#contestifi_contest_image_contest_div").show();
	 }
	 if (contest_type == '3')
	 {
	    $("#add_contestifi_survey_qustion_modal").modal("show");
	 }
	 else
	 {
	    $("#add_contestifi_quiz_qustion_modal").modal("show");
	 }
	 
      }
      function add_contestifi_question()
      {
	 var poll_querstion = $("#quiz_question").val();
	 var option_one = $("#quiz_option_one").val();
	 var option_two = $("#quiz_option_two").val();
	 var option_three = $("#quiz_option_three").val();
	 var option_four = $("#quiz_option_four").val();
	 if ( poll_querstion == '' || option_one == '' || option_two == '') {
	       $("#create_contestifi_quiz_error").html("Please Fill mandatory fields");
	       return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 var right_ans = $("input[name='contest_quiz_right_ans']:checked").val();
	 if (right_ans == '1' && option_one == '') {
	    $("#create_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '2' && option_two == '') {
	    $("#create_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '3' && option_three == '') {
	    $("#create_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '4' && option_four == '') {
	    $("#create_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 var contest_type = 1;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_type  = $(this).data('contestifi_contest_type');
	    }
	 });
	 if (contest_type != '1')
	 {
	    var file_data = $("#contestifi_content_file").prop("files")[0];
	    if (typeof file_data === "undefined") {
	       $("#create_contestifi_quiz_error").html("Please Select File");
	       return false;
	    }else{
	       var file_size = $("#contestifi_content_file").prop("files")[0].size;
	       file_size = ((file_size/1024)/1024);
	       if (file_size > 500) {
		  $("#create_contestifi_quiz_error").html("File size should be max 500MB");
		  return false;
	       }
	    }
	 }
	 else
	 {
	     var file_data = '';
	 }
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append("file", file_data);
	 form_data.append('contest_id',contest_id);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("poll_querstion",poll_querstion);
	 form_data.append("option_one",option_one);
	 form_data.append("option_two",option_two);
	 form_data.append("option_three",option_three);
	 form_data.append("option_four",option_four);
	 form_data.append("right_ans",right_ans);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contistifi_quiz_question",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    xhr: function () {
                           var xhr = new window.XMLHttpRequest();
                           xhr.upload.addEventListener("progress", function (evt) {
                               if (evt.lengthComputable) {
                                   var percentComplete = evt.loaded / evt.total;
                                   percentComplete = parseInt(percentComplete * 100);
                                     $('.progress').removeClass('hide');
                                   $('.myprogress').text(percentComplete + '%');
                                   $('.myprogress').css('width', percentComplete + '%');
                               }
                           }, false);
                           return xhr;
                       },
	    success: function(data)
	    {
	       $('.progress').addClass('hide');
	       var quezi_question_id = data;
	       contestifi_content_builder(contest_id,quezi_question_id);
	       $("#add_contenstifi_quiz_question")[0].reset();
	       $("#add_contestifi_quiz_qustion_modal").modal("hide");
	    }
	 });
      }
      
      // on click of context
      $(document).on('click', '.contestifi_contest_section', function (){
	 $( ".contestifi_contest_section" ).each(function() {
	    $( this ).removeClass( "offline_active" );
	 });
	 $( this ).addClass( "offline_active" );
	 var contest_id = $(this).data('contestifi_contest_id'); 
	 list_contetifi_quiz_question(contest_id);
	 list_contetifi_conetst_prize(contest_id);
      });
      // on click of quiz questions
      $(document).on('click', '.contestifi_quiz_section', function (){
	 $( ".contestifi_quiz_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	 });
	 $( this ).addClass( "offline_active" );
      });
      
      $(document).on('click', '.contestifi_prize_section', function (){
	 $( ".contestifi_prize_section" ).each(function() {
	       $( this ).removeClass( "offline_active" );
	 });
	 $( this ).addClass( "offline_active" );
      });
      function delte_contestifi_quiz_question(question_id)
      {
	 if (confirm("Are you sure you want to delete?"))
	 {
	    var locationid = $("#created_location_id").val();
	    $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/delte_contestifi_quiz_question",
	       data: "question_id="+question_id+"&locationid="+locationid,
	       success: function(data){
		  var contest_id = $('.contestifi_quiz_section').filter('[data-contestifi_quiz_question_id="'+question_id+'"]').data('contestifi_contest_id');
		  contestifi_content_builder(contest_id);
	       }
	    });
	 }
	 else
	 {
	    return false;
	 }
      }
   
      function edit_contestifi_quiz_question(contest_id,question_id)
      {
	 $(".loading").show();
	 
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/get_contestifi_quiz_question",
	    data: "question_id="+question_id,
	    dataType: 'json',
	    success: function(data)
	    {
	       if (data.question.length > 0)
	       {
		  var obj = data.question[0];
		  var contest_type = 1;
		  $( ".contestifi_contest_section" ).each(function()
		  {
		     if($( this ).hasClass( "offline_active" )){
			contest_type  = $(this).data('contestifi_contest_type');
		     }
		  });
		  if (contest_type == 3) {
		     edit_contestifi_survey_question(obj,contest_id,question_id);
		  }
		  else
		  {
		     if (contest_type == '1')
		     {
			$("#contestifi_contest_image_contest_div_edit").hide();
			$("#contestifi_content_file_edit_preview_div").hide();
		     }
		     else
		     {
			$("#contestifi_contest_image_contest_div_edit").show();
			$("#contestifi_content_file_edit_preview_div").show();
			$("#contestifi_content_file_edit_preview").html(obj.contest_file);
		     }
		     $("#edit_contestifi_quiz_question_id").val(question_id);
		     $("#quiz_question_edit").val(obj.question);
		     if (obj.options.length > 0)
		     {
			for (var i = 0; i < obj.options.length; i++)
			{
			   var op_obj = obj.options[i];
			   
			   if (i == '0') {
			      if (op_obj.is_right_option == '1') {
				 $("input[name=contest_quiz_right_ans_edit][value=1]").prop('checked', true);
			      }
			      var option_id = "quiz_option_one_edit";
			   }
			   
			   else if (i == '1') {
			      if (op_obj.is_right_option == '1') {
				 $("input[name=contest_quiz_right_ans_edit][value=2]").prop('checked', true);
			      }
			      option_id = "quiz_option_two_edit";
			   }
			   else if (i == '2') {
			      if (op_obj.is_right_option == '1') {
				 $("input[name=contest_quiz_right_ans_edit][value=3]").prop('checked', true);
			      }
			      option_id = "quiz_option_three_edit";
			   }
			   else if (i == '3') {
			      if (op_obj.is_right_option == '1') {
				 $("input[name=contest_quiz_right_ans_edit][value=4]").prop('checked', true);
			      }
			      option_id = "quiz_option_four_edit";
			   }
			   $("#"+option_id).val(op_obj.option_text);
			}
		     }
		     $("#update_contestifi_quiz_qustion_modal").modal("show");
		  }
		  
		  
		  
		  
		  
	       }
	       
	       $(".loading").hide();
	    }
	 });
      }
      function update_contestifi_question()
      {
	 $("#update_contestifi_quiz_error").html("");
	 var quiz_question_id = $("#edit_contestifi_quiz_question_id").val();
	 var poll_querstion = $("#quiz_question_edit").val();
	 var option_one = $("#quiz_option_one_edit").val();
	 var option_two = $("#quiz_option_two_edit").val();
	 var option_three = $("#quiz_option_three_edit").val();
	 var option_four = $("#quiz_option_four_edit").val();
	 if (poll_querstion == '' || option_one == '' || option_two == '')
	 {
	    $("#update_contestifi_quiz_error").html("Please Fill mandatory fields");
	    return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 var right_ans = $("input[name='contest_quiz_right_ans_edit']:checked").val();
	 if (right_ans == '1' && option_one == '') {
	    $("#update_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '2' && option_two == '') {
	    $("#update_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '3' && option_three == '') {
	    $("#update_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 else if (right_ans == '4' && option_four == '') {
	    $("#update_contestifi_quiz_error").html("Plese choose valid Right ans");
	    return false;
	 }
	 
	 var contest_type = 1;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_type  = $(this).data('contestifi_contest_type');
	    }
	 });
	 if (contest_type != '1')
	 {
	    var file_data = $("#contestifi_content_file_edit").prop("files")[0];
	    if (typeof file_data === "undefined") {
	      
	    }else{
	       var file_size = $("#contestifi_content_file_edit").prop("files")[0].size;
	       file_size = ((file_size/1024)/1024);
	       if (file_size > 500) {
		  $("#create_contestifi_quiz_error").html("File size should be max 500MB");
		  return false;
	       }
	    }
	 }
	 else
	 {
	     var file_data = '';
	 }
	 
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append("file", file_data);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("poll_querstion",poll_querstion);
	 form_data.append("option_one",option_one);
	 form_data.append("option_two",option_two);
	 form_data.append("option_three",option_three);
	 form_data.append("option_four",option_four);
	 form_data.append("quiz_question_id",quiz_question_id);
	 form_data.append("contest_id",contest_id);
	 form_data.append("right_ans",right_ans);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/update_contistifi_quiz_question",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    xhr: function () {
                           var xhr = new window.XMLHttpRequest();
                           xhr.upload.addEventListener("progress", function (evt) {
                               if (evt.lengthComputable) {
                                   var percentComplete = evt.loaded / evt.total;
                                   percentComplete = parseInt(percentComplete * 100);
                                     $('.progress').removeClass('hide');
                                   $('.myprogress').text(percentComplete + '%');
                                   $('.myprogress').css('width', percentComplete + '%');
                               }
                           }, false);
                           return xhr;
                       },
	    success: function(data)
	    {
	       $('.progress').addClass('hide');
	       contestifi_content_builder(contest_id,quiz_question_id);
	       $("#update_contenstifi_quiz_question")[0].reset();
	       $("#update_contestifi_quiz_qustion_modal").modal("hide");
	    }
	 });
      }
      
      function open_add_contestifi_contest()
      {
	 $("#contestifi_contest_modal").modal("show");
      }
      
      function add_contestifi_contest()
      {
	var compliance_mandatory = 0;
	    if ($('input[name="contest_compliance_mandatory"]').is(':checked')) {
		compliance_mandatory = $('input[name="contest_compliance_mandatory"]').val();
	    }
	 $("#create_contestifi_contest_error").html("");
	 var start_date = $("input[name='start_date']").val();
	 var end_date = $("input[name='end_date']").val();
	 var contest_name = $("#contestifi_contest_name").val();
	 var contest_type = $("#contestifi_contest_type").val();
	 //var bg_color = $("#contestifi_contest_bg_color").val();
	 var bg_color = '';
	 //var text_color = $("#contestifi_contest_text_color").val();
	 var text_color = '';
	 var button_color = $("#contestifi_contest_button_color").val();
	 var contest_frequency = $("input[name='contest_frequency']:checked").val();
	 var contestifi_no_of_attemption = $("#contestifi_no_of_attemption").val();
	 var contestifi_point_per_question = $("#contestifi_point_per_question").val();
	 var contestifi_negative_point = $("#contestifi_negative_point").val();
	 var contestifi_point_deduct_every_secod = $("#contestifi_point_deduct_every_secod").val();
	 var contestifi_max_time_allocation = $("#contestifi_max_time_allocation").val();
	 var contestifi_custom_frequecny = 0;
	 if (contest_type == '1')
	 {
	    if (contest_frequency == 4) {
	       contestifi_custom_frequecny = $("#contestifi_custom_frequecny").val();
	       if (contestifi_custom_frequecny == '' || contestifi_custom_frequecny == 0) {
		  $("#create_contestifi_contest_error").html("Please Enter Valid Custom Frequency");
		  return false;
	       }
	    }
	    if (start_date == '' || end_date == '' ||  contest_name == '' || contestifi_no_of_attemption == '' || contestifi_point_per_question == '') {
		  $("#create_contestifi_contest_error").html("Please Fill mandatory fields");
		  return false;
	    }
	    if (contestifi_no_of_attemption < 0 || contestifi_point_per_question < 0 || contestifi_negative_point < 0 || contestifi_point_deduct_every_secod < 0 || contestifi_max_time_allocation < 0)
	    {
	       $("#create_contestifi_contest_error").html("Please Enter valid value");
		  return false;
	    }
	    if (contestifi_max_time_allocation <= 0)
	    {
	       $("#create_contestifi_contest_error").html("Please Enter valid maximum time allocation");
		  return false;
	    }
	    var contestifi_contest_rule = $("input[name='contestifi_contet_rule[]']").map(function(){return $(this).val();}).get();
	    if (contestifi_contest_rule == '') {
	       $("#create_contestifi_contest_error").html("Please add rule");
	       return false;
	    };
	 }
	 else
	 {
	    if (contest_type == '3') {
	       contestifi_no_of_attemption = '1';
	    }
	    else{
	       if (contestifi_no_of_attemption == '') {
		  $("#create_contestifi_contest_error").html("Please Fill mandatory fields");
		  return false;
	       }
	    }
	    if (start_date == '' || end_date == '') {
		  $("#create_contestifi_contest_error").html("Please Fill mandatory fields");
		  return false;
	    }
	 }
	 var contestifi_contest_logo_image_name = '';
	 var contestifi_contest_logo_image_src = '';
	 contestifi_contest_logo_image_src = $('input[name="contestifi_logo_image_values"]').val();
	 if (typeof contestifi_contest_logo_image_src != "undefined")
	 {
	    contestifi_contest_logo_image_name = $('input[name="contestifi_logo_image_values"]').next().next().attr('name');
	    contestifi_contest_logo_image_src = $('input[name="contestifi_logo_image_values"]').val();
	 }
	 else
	 {
	    contestifi_contest_logo_image_src = '';
	    $("#create_contestifi_contest_error").html("Please Select Logo");
	    return false;
	 }
	 
	 $(".loading").show();
	 var form_data = new FormData();
	  form_data.append('contestifi_contest_logo_image_name',contestifi_contest_logo_image_name);
	 form_data.append('contestifi_contest_logo_image_src',contestifi_contest_logo_image_src);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("contest_name",contest_name);
	 form_data.append("contest_type",contest_type);
	 form_data.append("bg_color",bg_color);
	 form_data.append("contest_frequency",contest_frequency);
	 form_data.append("contestifi_custom_frequecny",contestifi_custom_frequecny);
	 form_data.append("contestifi_no_of_attemption",contestifi_no_of_attemption);
	 
	 form_data.append("contestifi_point_per_question",contestifi_point_per_question);
	 form_data.append("contestifi_negative_point",contestifi_negative_point);
	 form_data.append("contestifi_point_deduct_every_secod",contestifi_point_deduct_every_secod);
	 form_data.append("contestifi_max_time_allocation",contestifi_max_time_allocation);
	 form_data.append("contestifi_contest_rule",contestifi_contest_rule);
	 form_data.append("text_color",text_color);
	 form_data.append("button_color",button_color);
	 form_data.append("start_date",start_date);
	 form_data.append("end_date",end_date);
	 form_data.append("is_lms",1);
	 form_data.append("compliance_mandatory",compliance_mandatory);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contestifi_contest",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       $("#contestifi_custom_frequecny_div").hide();
	       var contest_id = data;
	       contestifi_content_builder(contest_id);
	       $("#add_new_contestifi_contest")[0].reset();
	       $("#contestifi_contest_modal").modal("hide");
	    }
	 });
      }
      $(document).on('click', '.contestifi_contest_add_another_rule', function(){
	 var new_div = '<div class="col-sm-12 col-xs-12">'+
			      '<div class="col-sm-11 col-xs-11 nopadding-left">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				       '<input type="text" name="contestifi_contet_rule[]" class = "contestifi_contest_rule_class">'+
				       '<label>Rule<sup>*</sup></label>'+
				    '</div>'+
				    '</div>'+
				    '<div class="col-sm-1 col-xs-1">'+
				    '<label class="pull-right remove_contestifi_rule" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				    '</div>'+
			      '</div>';
	 $("#contestifi_contest_rule_div").append(new_div);
      });
      $(document).on('click', '.remove_contestifi_rule', function (){
	    $(this).parent().parent().remove();
      });
      $("input[name='contest_frequency']").change(function() {
	 var contest_frequency = $("input[name='contest_frequency']:checked").val();
	 if (contest_frequency == '4')
	 {
	    $("#contestifi_custom_frequecny_div").show();
	 }
	 else
	 {
	    $("#contestifi_custom_frequecny_div").hide();
	 }
      });
   
      function delete_contestifi_contest(contest_id)
      {
	 if (confirm("Are you sure you want to delete?"))
	 {
	    var locationid = $("#created_location_id").val();
	    $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/delte_contestifi_contest",
	       data: "contest_id="+contest_id+"&locationid="+locationid,
	       success: function(data){
		  contestifi_content_builder();
	       }
	    });
	 }
	 else
	 {
	    return false;
	 }
      }
   
      function edit_contestifi_contest(contest_id)
      {
	 $(".loading").show();
	 
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/get_contestifi_contest_for_edit",
	    data: "contest_id="+contest_id,
	    dataType: 'json',
	    success: function(data)
	    {
	       $("#contestifi_contest_id_edit").val(contest_id);
	       if (data.resultCode == '1')
	       {
		  var option_html = '';
		  if (data.contest_type == '2') {
		     $('.only_time_based_quiz_edit').hide();
		     $('.only_time_based_quiz_edit_no_of_attempt').show();
		     $("#contestifi_contest_rule_div_edit").hide();
		     option_html = '<option value="2">Image & Video Contest</option>';
		  }
		  else if (data.contest_type == '3') {
		     $('.only_time_based_quiz_edit').hide();
		     $('.only_time_based_quiz_edit_no_of_attempt').hide();
		     $("#contestifi_contest_rule_div_edit").hide();
		     option_html = '<option value="3">Survey</option>';
		  }
		  else{
		     $('.only_time_based_quiz_edit').show();
		     $('.only_time_based_quiz_edit_no_of_attempt').show();
		     $("#contestifi_contest_rule_div_edit").show();
		     option_html = '<option value="1">Time bound Quiz</option>';
		  }
		  $("#contestifi_contest_type_edit").html(option_html);
		  //$("#contestifi_contest_type_edit").val(data.contest_type);
		  $("#contestifi_contest_name_edit").val(data.contest_name);
		  $('#contest_logo_preview').attr('src', data.original_logo);
		  //$('#contestifi_contest_bg_color_edit').val(data.background_color);
		  //$('#contestifi_contest_text_color_edit').val(data.text_color);
		  $('#contestifi_contest_button_color_edit').val(data.button_color);
		  //$('name#contest_frequency_edit').val(data.contest_frequency);
		  if (data.contest_frequency == '1' || data.contest_frequency == '2' || data.contest_frequency == '3' || data.contest_frequency == '4')
		  {
		     $("input[name=contest_frequency_edit][value="+data.contest_frequency+"]").prop('checked', true);
		  }
		  
		  if (data.contest_frequency == '4')
		  {
		     $("#contestifi_custom_frequecny_div_edit").show();
		     $("#contestifi_custom_frequecny_edit").val(data.contestifi_custom_frequecny);
		  }
		  else
		  {
		     $("#contestifi_custom_frequecny_div_edit").hide();
		  }
		  $("#contestifi_no_of_attemption_edit").val(data.no_of_attemption);
		  $("#contestifi_point_per_question_edit").val(data.points_per_question);
		  $("#contestifi_negative_point_edit").val(data.negative_point);
		  $("#contestifi_point_deduct_every_secod_edit").val(data.point_deduct_every_second);
		  $("#contestifi_max_time_allocation_edit").val(data.max_time_allocation);
		  $(".start_date_edit").val(data.start_date);
		  $(".end_date_edit").val(data.end_date);
		  
		  for (var i = 0; i < data.contest_rule.length; i++)
		  {
		     var rule_obj = data.contest_rule[i];
		     if (i == 0)
		     {
			$("#contestifi_contest_rule_div_edit").html("");
			var new_div = '<div class="col-sm-12 col-xs-12">'+
				 '<label class="os-lable" style="font-weight:700">Contest Rules</label>'+
			      '</div>';
			$("#contestifi_contest_rule_div_edit").append(new_div);
			var new_div = '<div class="col-sm-12 col-xs-12">'+
				 '<div class="mui-textfield">'+
				       '<input type="text" class="cotenst_first_rule_edit" name="contestifi_contet_rule_edit[]" value="'+rule_obj.rule+'">'+
				       '<label>Rule<sup>*</sup></label>'+
				    '</div>'+
			      '</div>';
			$("#contestifi_contest_rule_div_edit").append(new_div);
		     }
		     else
		     {
			var new_div = '<div class="col-sm-12 col-xs-12">'+
			      '<div class="col-sm-11 col-xs-11 nopadding-left">'+
				 '<div class="mui-textfield">'+
				       '<input type="text" value="'+rule_obj.rule+'" name="contestifi_contet_rule_edit[]" class = "contestifi_contest_rule_class">'+
				       '<label>Rule<sup>*</sup></label>'+
				    '</div>'+
				    '</div>'+
				    '<div class="col-sm-1 col-xs-1">'+
				    '<label class="pull-right remove_contestifi_rule_edit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				    '</div>'+
			      '</div>';
			$("#contestifi_contest_rule_div_edit").append(new_div);
		     }
		  }
		  
		    if (data.compliance_mandatory == 1) {
			$("input[name=contest_compliance_mandatory_edit]").prop('checked', true);
		    }
		    else
		    {
			$("input[name=contest_compliance_mandatory_edit]").prop('checked', false);
		    }
	       }
	       $("#contestifi_contest_update_modal").modal("show");
	       $(".loading").hide();
	    }
	 });
      }
      $(document).on('click', '.contestifi_contest_add_another_rule_edit', function(){
	 var new_div = '<div class="col-sm-12 col-xs-12">'+
			      '<div class="col-sm-11 col-xs-11 nopadding-left">'+
				 '<div class="mui-textfield mui-textfield--float-label">'+
				       '<input type="text" name="contestifi_contet_rule_edit[]" class = "contestifi_contest_rule_class">'+
				       '<label>Rule<sup>*</sup></label>'+
				    '</div>'+
				    '</div>'+
				    '<div class="col-sm-1 col-xs-1">'+
				    '<label class="pull-right remove_contestifi_rule_edit" style="color:#f00f64; cursor: pointer"><i class="far fa-minus-square" style="font-family:FontAwesome"></i></label>'+
				    '</div>'+
			      '</div>';
	 $("#contestifi_contest_rule_div_edit").append(new_div);
      });
      $(document).on('click', '.remove_contestifi_rule_edit', function (){
	    $(this).parent().parent().remove();
      });
      $("input[name='contest_frequency_edit']").change(function() {
	 var contest_frequency = $("input[name='contest_frequency_edit']:checked").val();
	 if (contest_frequency == '4')
	 {
	    $("#contestifi_custom_frequecny_div_edit").show();
	 }
	 else
	 {
	    $("#contestifi_custom_frequecny_div_edit").hide();
	 }
      });
      
      function update_contestifi_contest()
      {
	var compliance_mandatory = 0;
	    if ($('input[name="contest_compliance_mandatory_edit"]').is(':checked')) {
		compliance_mandatory = $('input[name="contest_compliance_mandatory_edit"]').val();
	    }
	 $("#contestifi_contest_add_another_rule_edit").html("");
	 var start_date = $("input[name='start_date_edit']").val();
	 var end_date = $("input[name='end_date_edit']").val();
	 var contest_id = $("#contestifi_contest_id_edit").val();
	 var contest_name = $("#contestifi_contest_name_edit").val();
	 var contest_type = $("#contestifi_contest_type_edit").val();
	 //var bg_color = $("#contestifi_contest_bg_color_edit").val();
	 var bg_color = '';
	 //var text_color = $("#contestifi_contest_text_color_edit").val();
	 var text_color = '';
	 var button_color = $("#contestifi_contest_button_color_edit").val();
	 var contest_frequency = $("input[name='contest_frequency_edit']:checked").val();
	 var contestifi_no_of_attemption = $("#contestifi_no_of_attemption_edit").val();
	 var contestifi_point_per_question = $("#contestifi_point_per_question_edit").val();
	 var contestifi_negative_point = $("#contestifi_negative_point_edit").val();
	 var contestifi_point_deduct_every_secod = $("#contestifi_point_deduct_every_secod_edit").val();
	 var contestifi_max_time_allocation = $("#contestifi_max_time_allocation_edit").val();
	 var contestifi_custom_frequecny = 0;
	 if (contest_type == '3') {
	    contestifi_no_of_attemption = '1';
	 }
	 if (contest_type == '1')
	 {
	 
	    if (contest_frequency == 4) {
	       contestifi_custom_frequecny = $("#contestifi_custom_frequecny_edit").val();
	       if (contestifi_custom_frequecny == '' || contestifi_custom_frequecny == 0) {
		  $("#create_contestifi_contest_error_edit").html("Please Enter Valid Custom Frequency");
		  return false;
	       }
	    }
	    if ( contest_name == '' || contestifi_no_of_attemption == '' || contestifi_point_per_question == '') {
		  $("#create_contestifi_contest_error_edit").html("Please Fill mandatory fields");
		  return false;
	    }
	    if (contestifi_no_of_attemption < 0 || contestifi_point_per_question < 0 || contestifi_negative_point < 0 || contestifi_point_deduct_every_secod < 0 || contestifi_max_time_allocation < 0)
	    {
	       $("#create_contestifi_contest_error_edit").html("Please Enter valid value");
		  return false;
	    }
	    if (contestifi_max_time_allocation <= 0)
	    {
	       $("#create_contestifi_contest_error_edit").html("Please Enter valid maximum time allocation");
		  return false;
	    }
	    var contestifi_contest_rule = $("input[name='contestifi_contet_rule_edit[]']").map(function(){return $(this).val();}).get();
	    if (contestifi_contest_rule == '') {
	       $("#create_contestifi_contest_error_edit").html("Please add rule");
	       return false;
	    }
	 }
	 var contestifi_contest_logo_image_name = '';
	 var contestifi_contest_logo_image_src = '';
	 contestifi_contest_logo_image_src = $('input[name="contestifi_logo_image_edit_values"]').val();
	 if (typeof contestifi_contest_logo_image_src != "undefined")
	 {
	    contestifi_contest_logo_image_name = $('input[name="contestifi_logo_image_edit_values"]').next().next().attr('name');
	    contestifi_contest_logo_image_src = $('input[name="contestifi_logo_image_edit_values"]').val();
	 }
	 else
	 {
	    contestifi_contest_logo_image_src = '';
	 }
	 
	 $(".loading").show();
	 var form_data = new FormData();
	  form_data.append('contestifi_contest_logo_image_name',contestifi_contest_logo_image_name);
	 form_data.append('contestifi_contest_logo_image_src',contestifi_contest_logo_image_src);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("contest_name",contest_name);
	 form_data.append("contest_type",contest_type);
	 form_data.append("bg_color",bg_color);
	 form_data.append("contest_frequency",contest_frequency);
	 form_data.append("contestifi_custom_frequecny",contestifi_custom_frequecny);
	 form_data.append("contestifi_no_of_attemption",contestifi_no_of_attemption);
	 
	 form_data.append("contestifi_point_per_question",contestifi_point_per_question);
	 form_data.append("contestifi_negative_point",contestifi_negative_point);
	 form_data.append("contestifi_point_deduct_every_secod",contestifi_point_deduct_every_secod);
	 form_data.append("contestifi_max_time_allocation",contestifi_max_time_allocation);
	 form_data.append("contestifi_contest_rule",contestifi_contest_rule);
	 form_data.append("contest_id",contest_id);
	 form_data.append("text_color",text_color);
	 form_data.append("button_color",button_color);
	 form_data.append("start_date",start_date);
	 form_data.append("end_date",end_date);
	 form_data.append("compliance_mandatory",compliance_mandatory);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/update_contestifi_contest",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       $("#create_contestifi_contest_error_edit").html('');
	       contestifi_content_builder(contest_id);
	       $("#update_contestifi_contest")[0].reset();
	       $("#contestifi_contest_update_modal").modal("hide");
	    }
	 });
      }
   
   
      function open_add_contestifi_prize()
      {
	 $("#add_contestifi_contest_prize_modal").modal("show");
      }
      function add_contestifi_prize()
      {
	 var contestifi_contest_prize = $("#contestifi_contest_prize").val();
	 if ( contestifi_contest_prize == '') {
	       $("#create_contestifi_prize_error").html("Please Fill mandatory fields");
	       return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append('prize_id','');
	 form_data.append('contest_id',contest_id);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("contestifi_contest_prize",contestifi_contest_prize);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contistifi_contest_prize",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       var contestifi_contest_prize_id = data;
	       contestifi_content_builder(contest_id, '',contestifi_contest_prize_id);
	       $("#add_contenstifi_contest_prize")[0].reset();
	       $("#add_contestifi_contest_prize_modal").modal("hide");
	    }
	 });
      }
      
      function delte_contestifi_contest_prize(prize_id)
      {
	 if (confirm("Are you sure you want to delete?"))
	 {
	    var locationid = $("#created_location_id").val();
	    $(".loading").show();
	    $.ajax({
	       type: "POST",
	       url: "<?php echo base_url()?>location/delte_contestifi_contest_prize",
	       data: "prize_id="+prize_id+"&locationid="+locationid,
	       success: function(data){
		  var contest_id = $('.contestifi_prize_section').filter('[data-contestifi_contest_prize_id="'+prize_id+'"]').data('contestifi_contest_id');
		  contestifi_content_builder(contest_id);
	       }
	    });
	 }
	 else
	 {
	    return false;
	 }
      }
   
      function edit_contestifi_contest_prize(contest_id,prize_id, contest_prize)
      {
	 $("#contestifi_contest_prize_id_edit").val(prize_id);
	 $("#contestifi_contest_prize_edit").val(unescape(contest_prize));
	       $("#add_contestifi_contest_prize_modal_edit").modal("show");
	    
      }
      function update_contestifi_prize()
      {
	 $("#create_contestifi_prize_error_edit").html("");
	 var prize_id = $("#contestifi_contest_prize_id_edit").val();
	 var contestifi_contest_prize = $("#contestifi_contest_prize_edit").val();
	 if ( contestifi_contest_prize == '') {
	       $("#create_contestifi_prize_error_edit").html("Please Fill mandatory fields");
	       return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append('contest_id',contest_id);
	 form_data.append('prize_id',prize_id);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("contestifi_contest_prize",contestifi_contest_prize);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contistifi_contest_prize",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       contestifi_content_builder(contest_id, '',prize_id);
	       $("#add_contenstifi_contest_prize_edit")[0].reset();
	       $("#add_contestifi_contest_prize_modal_edit").modal("hide");
	    }
	 });
      }
      
      function contestifi_add_content_builder() {
	 
	 var form_data = new FormData(); 
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/contestifi_add_content_builder",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data){
	       if (Clicked_ButtonValue == 'exit') {
		  window.location = "<?php  echo base_url()?>location";
	       }else{
		  $(".loading").hide();
	       }
	    }
         });
      }
	
      
      $(document).ready(function(){
            $('.start_date').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       minDate: moment()
	    }).on('change', function(e, date){
	       $('.end_date').bootstrapMaterialDatePicker('setMinDate', date);
	    });
      
	    $('.end_date').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       minDate: moment()
	    }).on('change', function(e, date){
	       $('.start_date').bootstrapMaterialDatePicker('setMaxDate', date);
	      
	    });
	    
	    
	    
	    $('.start_date_edit').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       minDate: moment()
	    }).on('change', function(e, date){
	       $('.end_date_edit').bootstrapMaterialDatePicker('setMinDate', date);
	    });
      
	    $('.end_date_edit').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       minDate: moment()
	    }).on('change', function(e, date){
	       $('.start_date_edit').bootstrapMaterialDatePicker('setMaxDate', date);
	      
	    });
      });
      
      
      function contestifi_sign_term_condition() {
	 $(".loading").show();
	 var location_id = $("#created_location_id").val();
	 $.ajax({
               type: "POST",
                url: "<?php echo base_url()?>location/get_contestifi_signup_term_condition",
		data: "location_id="+location_id,
		dataType: 'json',
		success: function(data){
		  //$("#contestifi_term_conditon").val(data.terms);
		  CKEDITOR.instances.contestifi_term_condition.setData(data.terms);
		  $("#add_contestifi_contest_terms_modal").modal("show");
                  $(".loading").hide();
               }
	 });
      }
      function add_contestifi_terms_conditon() {
	 $(".loading").show();
	 var location_id = $("#created_location_id").val();
	 var terms = CKEDITOR.instances.contestifi_term_condition.getData();
	 //var terms = $("#contestifi_term_conditon").val();
	 $.ajax({
               type: "POST",
                url: "<?php echo base_url()?>location/add_contestifi_terms_conditon",
		data : ({'location_id': location_id, 'terms': terms}),
		dataType: 'json',
		success: function(data){
		  $("#add_contestifi_contest_terms_modal").modal("hide");
                  $(".loading").hide();
               }
	 });
      }
     
      function change_contest_type_drowdown()
      {
	 var contest_type = $("#contestifi_contest_type").val();
	 if (contest_type == '1')
	 {
	    $('.only_time_based_quiz').show();
	    $('.only_time_based_quiz_no_of_attempt').show();
	    $('#contestifi_contest_rule_div').show();
	 }
	 else
	 {
	    if (contest_type == 3)
	    {
	       $('.only_time_based_quiz_no_of_attempt').hide();
	    }
	    else
	    {
	       $('.only_time_based_quiz_no_of_attempt').show();
	    }
	    $('.only_time_based_quiz').hide();
	    $('#contestifi_contest_rule_div').hide();
	    $('#contestifi_custom_frequecny_div').hide();
	 }
      }
      
      
      
      
      function add_contestifi_survey()
      {
	 var poll_querstion = $("#survey_question").val();
	 var option_one = $("#survey_option_one").val();
	 var option_two = $("#survey_option_two").val();
	 var option_three = $("#survey_option_three").val();
	 var option_four = $("#survey_option_four").val();
	 var survey_type = $("#contestifi_contest_survey_type").val();
	 if ( poll_querstion == '' || option_one == '' || option_two == '') {
	       $("#create_contestifi_survey_error").html("Please Fill mandatory fields");
	       return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append('contest_id',contest_id);
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("poll_querstion",poll_querstion);
	 form_data.append("option_one",option_one);
	 form_data.append("option_two",option_two);
	 form_data.append("option_three",option_three);
	 form_data.append("option_four",option_four);
	 form_data.append("survey_type",survey_type);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/add_contistifi_survey_question",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       var quezi_question_id = data;
	       contestifi_content_builder(contest_id,quezi_question_id);
	       $("#add_contenstifi_survey_question")[0].reset();
	       $("#add_contestifi_survey_qustion_modal").modal("hide");
	    }
	 });
      }
      
      
      function edit_contestifi_survey_question(obj,contest_id,question_id) {
	$("#contestifi_survey_question_id_hidden").val(question_id);
	 $("#survey_question_edit").val(obj.question);
	 $("#contestifi_contest_survey_type_edit").val(obj.survey_type);
	 if (obj.options.length > 0)
	 {
	     $("#survey_option_one_edit").val('');
	    $("#survey_option_two_edit").val('');
	    $("#survey_option_three_edit").val('');
	    $("#survey_option_four_edit").val('');
	    for (var i = 0; i < obj.options.length; i++)
	    {
	       var op_obj = obj.options[i];
	       if (i == '0') {
		  var option_id = "survey_option_one_edit";
	       }
	       else if (i == '1')
	       {
		  option_id = "survey_option_two_edit";
	       }
	       else if (i == '2')
	       {
		  option_id = "survey_option_three_edit";
	       }
	       else if (i == '3')
	       {
		  option_id = "survey_option_four_edit";
	       }
	       $("#"+option_id).val(op_obj.option_text);
	    }
	 }
	 $("#update_contestifi_survey_qustion_modal").modal("show");
      }
      
      
      function update_contestifi_survey()
      {
	 $("#create_contestifi_survey_error_edit").html("");
	 var quiz_question_id = $("#contestifi_survey_question_id_hidden").val();
	 var poll_querstion = $("#survey_question_edit").val();
	 var option_one = $("#survey_option_one_edit").val();
	 var option_two = $("#survey_option_two_edit").val();
	 var option_three = $("#survey_option_three_edit").val();
	 var option_four = $("#survey_option_four_edit").val();
	 if (poll_querstion == '' || option_one == '' || option_two == '')
	 {
	    $("#create_contestifi_survey_error_edit").html("Please Fill mandatory fields");
	    return false;
	 }
	 var contest_id = 0;
	 $( ".contestifi_contest_section" ).each(function()
	 {
	    if($( this ).hasClass( "offline_active" )){
	       contest_id  = $(this).data('contestifi_contest_id');
	    }
	 });
	 
	 var survey_type = $("#contestifi_contest_survey_type_edit").val();
	 
	 $(".loading").show();
	 var form_data = new FormData();
	 form_data.append("location_uid", $("#location_id_hidden").val());
	 form_data.append("locationid", $("#created_location_id").val());
	 form_data.append("poll_querstion",poll_querstion);
	 form_data.append("option_one",option_one);
	 form_data.append("option_two",option_two);
	 form_data.append("option_three",option_three);
	 form_data.append("option_four",option_four);
	 form_data.append("quiz_question_id",quiz_question_id);
	 form_data.append("contest_id",contest_id);
	 form_data.append("survey_type",survey_type);
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/update_contistifi_survey_question",
	    cache: false,
	    contentType: false,
	    processData: false,
	    data: form_data,
	    success: function(data)
	    {
	       contestifi_content_builder(contest_id,quiz_question_id);
	       $("#update_contenstifi_survey_question")[0].reset();
	       $("#update_contestifi_survey_qustion_modal").modal("hide");
	    }
	 });
      }
      
      
      
      
      
     
      
      
      
	
   </script>
	
	
    
   </body>
</html>
