<?php $this->load->view('includes/header');?>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/responsiveCarousel.min.js"></script>
<!-- Start your project here-->
<div class="loading" >
   <img src="<?php echo base_url()?>assets/images/loader.svg"/>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="wapper">
         <div id="sidedrawer" class="mui--no-user-select">
            <div id="sidedrawer-brand" class="mui--appbar-line-height">
               <?php
                  $isplogo = $this->permission_model->get_isplogo();
                  if($isplogo != 0){
                     echo '<img src="'.IMAGEPATHISP."small/".$isplogo.'" class="img-responsive"/>';
                  }else{
                  
                     echo '<img src="'.base_url().'assets/images/isp_logo.png" class="img-responsive"/>';
                  }
                      ?>
            </div>
            <?php
            $data['navperm']=$this->plan_model->leftnav_permission();
            $this->load->view('includes/left_nav');?>
         </div>
         <header id="header">
            <nav class="navbar navbar-default">
               <div class="container-fluid">
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#">Edit Location</a>
                  </div>
                  <div class="navbar-header">
                     <a class="navbar-brand" href="#" onclick="is_location_proper_setup()">Check Configuration</a>
                  </div>
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                           <a href="<?php echo base_url()?>location">
                              <div class="mui-btn---small">
                                 <img src="<?php echo base_url()?>assets/images/close.svg"/>
                              </div>
                           </a>
                        </li>
                        <?php $this->load->view('includes/global_setting',$data);?>
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <div id="content-wrapper">
            <div class="mui--appbar-height"></div>
            <div class="mui-container-fluid" id="right-container-fluid">
               <div class="add_user">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                           <ul class="mui-tabs__bar">
                              <li class="mui--is-active">
                                 <a data-mui-toggle="tab" data-mui-controls="Location_details" >
                                 Location Details
                                 <span style = "display: none" id="location_detail_tab_checked"> &nbsp;&nbsp;<i class="fa fa-check" aria-hidden="true" style="color: #39b54a!important;"></i></span>
                                 </a>
                              </li>
                              <li class="disabled">
                                 <a data-mui-toggle="tab" data-mui-controls="captive_portal" class="tab_menu" >
                                 Captive Portal
                                 <span style = "display: none" id="captive_portal_tab_checked"> &nbsp;&nbsp;<i class="fa fa-check" aria-hidden="true" style="color: #39b54a!important;"></i></span>
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="nas_setup" class="tab_menu" onclick="nas_setup_list('<?php echo $locationid?>')">
                                 NAS Setup
                                 </a>
                              </li>
                              <li >
                                 <a data-mui-toggle="tab" data-mui-controls="access_points" class="tab_menu access_point_tab_menu" <?php /*if($edit_location_data->nastype == '1') echo "style = 'cursor: not-allowed;' disabled = 'disabled'"*/?> onclick="access_point_list()">
                                 Access Points
                                 </a>
                              </li>
                              <li>
                                 <a data-mui-toggle="tab" data-mui-controls="Vouchers" class="tab_menu" onclick="voucher_list('<?php echo $locationid?>')">
                                 Vouchers
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!-- location tab start -->
                        <div class="mui-tabs__pane mui--is-active" id="Location_details">
                           <form id="add_location" onsubmit="add_location(); return false;">
                              <div class="form-group">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row">
                                       <h2>Location Details</h2>
                                    </div>
                                    <div class="row">
                                       <!--h4>TYPE</h4-->
                                    </div>
                                    <div class="row">
                                       <input type="hidden" name="location_type" id="location_type" value="<?php echo $edit_location_data->location_type?>">
                                       <!--label class="radio-inline">
                                          <input type="radio" name="location_type"   value="1" <?php if($edit_location_data->location_type == '1')echo "checked"?>> Public
                                          </label>
                                          <label class="radio-inline">
                                          <input type="radio" name="location_type" value="2" <?php if($edit_location_data->location_type == '2')echo "checked"?>> HOTEL
                                          </label-->
                                    </div>
                                    <div class="form-group">
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <!-- hidden value saved start -->
                                                      <input type="hidden" name="created_location_id" id="created_location_id" value="<?php echo $edit_location_data->id ?>">
                                                      <input type="hidden" name="location_name_hidden" id="location_name_hidden" value="<?php echo $edit_location_data->location_name?>">
                                                      <input type="hidden" name="location_id_hidden" id="location_id_hidden" value="<?php echo $locationid?>">
                                                      <input type="hidden" name="email_hidden" id="email_hidden" value="<?php echo $edit_location_data->user_email?>">
                                                      <input type="hidden" name="mobile_hidden" id="mobile_number" value="<?php echo $edit_location_data->mobile_number?>">
                                                      <!-- hidden value saved end -->
                                                      <input type="text" name="location_name" id="location_name" value="<?php echo $edit_location_data->location_name?>" required>
                                                      <label>Location Name <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" id="geolocation" name="geoaddr" value="<?php echo $edit_location_data->geo_address?>" required>
                                                      <input type="hidden"  id="placeid" name="placeid" value="<?php echo $edit_location_data->placeid?>"  readonly>
                                                      <input type="hidden"  id="lat" name="lat" value="<?php echo $edit_location_data->latitude?>" readonly>
                                                      <input type="hidden" id="long" name="long" value="<?php echo $edit_location_data->longitude?>"  readonly>
                                                      <label>Geo Location Lookup <sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <div class="map_container">
                                       <div class="span8" style="margin-bottom: 15px; border:2px solid #6D6E71; height: 150px;" id="map1">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-4 col-xs-4">
                                          <span id="placeidview"> <?php echo $edit_location_data->placeid?></span>
                                       </div>
                                       <div class="col-sm-4 col-xs-4">
                                          <span id="latview"> <?php echo $edit_location_data->latitude?></span>
                                       </div>
                                       <div class="col-sm-4 col-xs-4">
                                          <span id="longview"> <?php echo $edit_location_data->longitude?></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row" id="user_type_lead_enquiry">
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="address1" id="address1" value="<?php echo $edit_location_data->address_1?>" required>
                                                   <label>Address Line 1 <sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="address2" id="address2" value="<?php echo $edit_location_data->address_2?>" required>
                                                   <label>Address Line 2 <sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="pin" id="pin" pincode- pattern="[0-9]{1-20}" value="<?php echo $edit_location_data->pin?>" required>
                                                   <label>PIN Code <sup>*</sup></label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                <div class="mui-select">
                                                   <select name="state" id="state" required>
                                                      <option value="">All States</option>
                                                      <?php
                                                         $state_list = $this->location_model->state_list();
                                                              if($state_list->resultCode == 1){
                                                            foreach($state_list->state as $state_list_view){
                                                               $sel = '';
                                                               if($state_list_view->state_id == $edit_location_data->state_id){
                                                           $sel = "selected";
                                                               }
                                                               echo '<option value = "'.$state_list_view->state_id.'" '.$sel.'>'.$state_list_view->state_name.'</option>';
                                                            }
                                                            
                                                         }
                                                         ?>
                                                   </select>
                                                   <label>State<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select name="city" id="city" required>
                                                   <?php
                                                      echo $city_list = $this->location_model->city_list($edit_location_data->state_id,$edit_location_data->city_id);
                                                      ?>
                                                   </select>
                                                   <label>City<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select id="zone" name="zone" required>
                                                   <?php
                                                      echo $zone_list = $this->location_model->zone_list($edit_location_data->city_id,$edit_location_data->zone_id);
                                                      ?>
                                                   </select>
                                                   <label>Zone<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h5 class="create_headings" onclick="add_zone_model()">
                                                   Add New Zone
                                                </h5>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-left">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="location_id" id="location_id" value="<?php echo $edit_location_data->location_uid?>" readonly>
                                                   <span class="title_box">(auto-generated)</span>
                                                   <label>Location ID <sup>*</sup></label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <h4>CONTACT PERSON</h4>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="contact_person_name" id="contact_person_name" value="<?php echo $edit_location_data->contact_person_name?>" required>
                                                   <label>Full Name<sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="email" name="email_id" id="email_id" value="<?php echo $edit_location_data->user_email?>"  required>
                                                   <label>Email ID <sup>*</sup></label>
                                                </div>
                                             </div>
                                             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="mobile_no" id="mobile_no" pattern="[0-9]{1-20}" value="<?php echo $edit_location_data->mobile_number?>" required>
                                                   <label>Mobile NO <sup>*</sup></label>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label style="font-weight:700; margin-right:10px; color:#000">Location Portal Url:</label> 
                                                <label style="font-weight:700; margin-right:10px; margin-left:10px; color:#000">
                                                <?php
                                                   if($edit_location_data->location_type == '2'){
                                                   echo '<a href="'.HOMEWIFIPORTAL.'hotelportal" target="_blank">'.HOMEWIFIPORTAL.'hotelportal</a>';
                                                   }else if($edit_location_data->location_type == '3'){
                                                   echo '<a href="'.HOMEWIFIPORTAL.'hospitalportal" target="_blank">'.HOMEWIFIPORTAL.'hospitalportal</a>';
                                                   }else if($edit_location_data->location_type == '4'){
                                                   echo '<a href="'.HOMEWIFIPORTAL.'instituteportal" target="_blank">'.HOMEWIFIPORTAL.'instituteportal</a>';
                                                   }else if($edit_location_data->location_type == '5'){
                                                   echo '<a href="'.HOMEWIFIPORTAL.'enterpriseportal" target="_blank">'.HOMEWIFIPORTAL.'enterpriseportal</a>';
                                                   }else if($edit_location_data->location_type == '6'){
                                                   echo '<a href="'.HOMEWIFIPORTAL.'cafeportal" target="_blank">'.HOMEWIFIPORTAL.'cafeportal</a>';
                                                   }else{
                                                   echo '<a href="'.HOMEWIFIPORTAL.'locationportal" target="_blank">'.HOMEWIFIPORTAL.'locationportal</a>';
                                                   }
                                                   ?>
                                                </label> 
                                                <label style="font-weight:700; margin-right:10px; margin-left:10px; color:#000">Username</label> 
                                                <label style="font-weight:400; margin-right:10px; margin-left:10px; color:#000"><?php echo $edit_location_data->user_email?></label>
                                                <label style="font-weight:700; margin-right:10px; margin-left:10px; color:#000">Password</label> 
                                                <label style="font-weight:400; margin-right:10px; margin-left:10px; color:#000">
                                                <?php
                                                   if(isset($edit_location_data->hotel_portal_password) && $edit_location_data->hotel_portal_password != ''){
                                                      echo $edit_location_data->hotel_portal_password;
                                                   }else{
                                                      echo $edit_location_data->location_uid;
                                                   }
                                                   
                                                   ?></label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px">
                                          <div class="row">
                                             <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 nomargin-left">
                                                <p id="add_location_error" style="color:red"></p>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value="exit"> SAVE & EXIT</button>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn"  value="save"> SAVE</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                        <!-- location End start -->
                        <!-- cpative portal tab start -->
                        <div class="mui-tabs__pane" id="captive_portal">
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <h2>Captive Portal
                                 <?php
				 if(isset($edit_location_data->cp_url) && $edit_location_data->cp_url != ''){
				    ?>
				    ( <a style="font-size: 11px" href = "<?php echo $edit_location_data->cp_url ?>" target="_blank"><?php echo $edit_location_data->cp_url ?></a>)
				    <?php 
				 }
				 ?>
                                 </h2>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="col-sm-12 col-xs-12 mui--text-right">
                                    <p><strong><span class="location_name_view"></span><?php echo $edit_location_data->location_name?> <span class="location_id_view"></span></strong></p>
                                    <p><small style="color:#808080" class="email_view"><?php echo $edit_location_data->user_email?></small></p>
                                    <p><small style="color:#808080" class="mobile_number_view"><?php echo $edit_location_data->mobile_number?></small></p>
                                 </div>
                              </div>
                           </div>
                           <?php if($edit_location_data->location_type == '1'){
                              ?>
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <form id="add_captive_portal"  enctype="multipart/form-data" onsubmit="add_captive_portal(); return false;">
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <h4>SELECT CAPTIVE PORTAL TYPE</h4>
                                          </div>
                                          <div class="row" style="margin-bottom: 15px;">
                                             <?php
                                                $cp1_checked = '';
                                                $cp2_checked = '';
                                                $cp3_checked = '';
                                                
                                                if(isset($edit_location_data->cp_type) && $edit_location_data->cp_type != ''){
                                                  $cp1_checked = '';
                                                   if($edit_location_data->cp_type == 'cp1'){
						      $cp1_checked = 'checked';
                                                   }elseif($edit_location_data->cp_type == 'cp2'){
						      $cp2_checked = 'checked';
                                                   }elseif($edit_location_data->cp_type == 'cp3'){
						      $cp3_checked = 'checked';
                                                   }
                                                }
                                                ?>
                                             <label class="radio-inline">
                                             <input type="radio" name="captive_potal_type" value="cp1" <?php echo $cp1_checked?>> CP1 (Time Based)
                                             </label>
                                             <label class="radio-inline">
                                             <input type="radio" name="captive_potal_type"  value="cp2" <?php echo $cp2_checked?>> CP2 (Data Earn & Burn)
                                             </label>
                                             <label class="radio-inline">
                                             <input type="radio" name="captive_potal_type"  value="cp3" <?php echo $cp3_checked?>> CP3 (Hybrid)
                                             </label>
                                          </div>
                                          <div class="row"  style="margin-bottom: 15px;">
                                             <span class="cp_span">This Captivew portal lets the user surf for free for a limited time and requires them to view sponsored content
                                             Eg: Videos, Offers etc. Users can access interent multile times after consuming content for each session.</span><br/>
                                          </div>
                                       </div>
                                    </div>
				    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					   <div class="row" >
                                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="public_wifi_ssid" value = "<?php echo $edit_location_data->main_ssid?>">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
				       </div>
                                    </div>
				    <div class="form-group">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="row" style="margin-bottom: 15px;">
					     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							 <div class="row">
							 <label class="radio-inline">
							    <input type="radio" name="is_otpdisabled_public_location"  value="0" <?php if($edit_location_data->is_otpdisabled == '0') echo "checked"?>> OTP Enable
							 </label>
							 </div>
					     </div>
					     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							 <div class="row">
							 <label class="radio-inline">
							    <input type="radio" name="is_otpdisabled_public_location"  value="1" <?php if($edit_location_data->is_otpdisabled == '1') echo "checked"?>> OTP Disable
							 </label>
							 </div>
					     </div>
					  </div>
				       </div>
                                    </div>
                                    <!-- cp1 fields start -->
                                    <div id="cp1_div" <?php if($cp1_checked != "checked") echo 'style="display:none"'?> >
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="cp1_plan" id="cp1_plan">
                                                      <?php
                                                         echo $plan_list = $this->location_model->plan_list('2', $edit_location_data->cp1_plan);
                                                         ?>
                                                      </select>
                                                      <label>Select Time Based Plan <sup>*</sup></label>
                                                   </div>
                                                   <span class="cp_span_color">Only Time Limit based plans can be used here </span>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top:30px">
                                                   <a class="create_headings" href="<?php echo base_url()?>plan/add_plan">
                                                   Create New Plan
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp1_no_of_daily_session" id="cp1_no_of_daily_session" value="<?php echo $edit_location_data->cp1_no_of_daily_session?>">
                                                      <label>NO OF DAILY SESSION<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- cp1 fields END -->
                                    <!-- cp2 fields start -->
                                    <div id="cp2_div" <?php if($cp2_checked != "checked") echo 'style="display:none"'?> >
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="cp2_plan" id="cp2_plan">
                                                      <?php
                                                         echo $plan_list = $this->location_model->plan_list('4', $edit_location_data->cp2_plan);
                                                         ?>
                                                      </select>
                                                      <label>Select  Plan <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top:30px">
                                                   <a class="create_headings" href="<?php echo base_url()?>plan/add_plan">
                                                   Create New Plan
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp2_signip_data" id="cp2_signip_data" value="<?php echo $edit_location_data->cp2_signup_data?>">
                                                      <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                      <label>Data on Signup<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp2_daily_data" id="cp2_daily_data" value="<?php echo $edit_location_data->cp2_daily_data?>">
                                                      <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                      <label>Daily Data Quota<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- cp2 fields END -->
                                    <!-- cp3 fields start -->
                                    <div id="cp3_div" <?php if($cp3_checked != "checked") echo 'style="display:none"'?> >
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="cp3_hybrid_plan" id="cp3_hybrid_plan">
                                                      <?php
                                                         echo $plan_list = $this->location_model->plan_list('5', $edit_location_data->cp3_hybrid_plan);
                                                         ?>
                                                      </select>
                                                      <label>Select Hybrid Plan <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top:30px">
                                                   <a class="create_headings" href="<?php echo base_url()?>plan/add_plan">
                                                   Create New Plan
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="cp3_data_plan" id="cp3_data_plan">
                                                      <?php
                                                         echo $plan_list = $this->location_model->plan_list('4', $edit_location_data->cp3_data_plan);
                                                         ?>
                                                      </select>
                                                      <label>Select Data Plan <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top:30px">
                                                   <a class="create_headings" href="<?php echo base_url()?>plan/add_plan">
                                                   Create New Plan
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp3_signip_data" id="cp3_signip_data" value="<?php echo $edit_location_data->cp3_signup_data?>" >
                                                      <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                      <label>Data on Signup<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group" style="margin-top: 15px">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="cp3_daily_data" id="cp3_daily_data" value="<?php echo $edit_location_data->cp3_daily_data?>">
                                                      <span class="title_box" style="color: #231f20; font-weight: 600;">MB</span>
                                                      <label>Daily Data Quota<sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- cp3 fields END -->
                                    <!--div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                <legend>UPLOAD A BRAND LOGO</legend>
                                                <div class="mui-textfield mui-textfield--float-label blogoimg">
                                                   <input type="text" id="cp1_brand_image_name">
                                                   <input type="file" id="exampleInputFile" name="banner_image" class="hide"/>
                                                   <label>Logo Image <sup>*</sup></label>
                                                </div>
                                                <span class="cp_span">You can upload JPG. PNG, BMP upto 2mb in size</span>
                                             </div>
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top: 40px">
                                                <p id="but" class="mui-btn mui-btn--small mui-btn--primary" style="height:30px; padding: 0px 25px; background-color:#36465f; font-weight:600; line-height: 30px">
                                                   CHOOSE FILE
                                                   <span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span>
                                                </p>
                                             </div>
                                             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="padding-top: 40px">
                                                <p class="mui-btn mui-btn--small mui-btn--danger" type="button" onclick="unset_image()" id="unset_image_button" style="display: none; height:28px; background-color:#36465f;">Unset Image</p>
                                             </div>
                                          </div>
                                       </div>
                                    </div-->
				    <div class="form-group" style="margin-top: 15px">
				       <input type="file" id="exampleInputFile" name="banner_image" class="hide"/>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
					     <legend>UPLOAD A BRAND LOGO</legend><br/>
					     <div class="dropzone" data-width="900" data-ajax="false" data-resize="true" data-originalsize="false" data-height="300" style="width: 300px; height:100px;">
						<input type="file" name="thumb"  />
					     </div>
					  </div>
				       </div>
				    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px">
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                <p id="add_captive_portal_error" style="color:red"></p>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value="exit"> SAVE & EXIT</button>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn"  value="save"> SAVE</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                       <div class="slider_div pull-right">
                                          <div class="right_slider">
                                             <h3 class="cp_type_slider">
                                                <?php
                                                   // for logo
                                                   $brand_logo_display = base_url().'assets/images/default_brand_logo.png';
                                                   if(isset($edit_location_data->logo_image) && $edit_location_data->logo_image != ''){
                                                   $brand_logo_display = $edit_location_data->logo_image;
                                                   }elseif( isset($edit_location_data->original_image) && $edit_location_data->original_image != ''){
                                                   $brand_logo_display = $edit_location_data->original_image;
                                                   }
                                                   $cp1_slider_display = '';
                                                   $cp2_slider_display = '';
                                                   $cp3_slider_display = '';
                                                   if($cp2_checked == "checked"){
                                                   //echo "CAPTIVE PORTAL 2 SAMPLE";
                                                   $cp1_slider_display = 'display: none';
                                                   $cp2_slider_display = 'display: block';
                                                   $cp3_slider_display = 'display: none';
                                                   }elseif($cp3_checked == "checked"){
                                                   //echo "CAPTIVE PORTAL 3 SAMPLE";
                                                   $cp1_slider_display = 'display: none';
                                                   $cp2_slider_display = 'display: none';
                                                   $cp3_slider_display = 'display: block';
                                                   }else{
                                                  // echo "CAPTIVE PORTAL 1 SAMPLE";
                                                   $cp1_slider_display = 'display: block';
                                                   $cp2_slider_display = 'display: none';
                                                   $cp3_slider_display = 'display: none';
                                                   }
                                                   ?>
                                             </h3>
                                             <div class="crsl-items" data-navigation="navbtns">
                                                <div class="crsl-wrap">
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center  style="<?php echo $cp1_slider_display?>" class = "cp1_slider1">
                                                            <img src="<?php echo base_url()?>assets/images/cp_1_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="<?php echo $cp2_slider_display?>" class = "cp2_slider1">
                                                            <img src="<?php echo base_url()?>assets/images/cp_2_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="<?php echo $cp3_slider_display?>" class = "cp3_slider1">
                                                            <img src="<?php echo base_url()?>assets/images/cp_3_img.jpg" class="img-responsive"/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-a">
                                                         <div class=" crsl-item-logo">
                                                            <img src="<?php echo $brand_logo_display?>" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center style="<?php echo $cp1_slider_display?>" class = "cp1_slider2">
                                                            <img src="<?php echo base_url()?>assets/images/cp_1_img_2.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="<?php echo $cp2_slider_display?>" class = "cp2_slider2">
                                                            <img src="<?php echo base_url()?>assets/images/cp_2_img_2.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="<?php echo $cp3_slider_display?>" class = "cp3_slider2">
                                                            <img src="<?php echo base_url()?>assets/images/cp_3_img_2.jpg" class="img-responsive"/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-a">
                                                         <div class=" crsl-item-logo">
                                                            <img src="<?php echo $brand_logo_display?>" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center style="<?php echo $cp1_slider_display?>" class = "cp1_slider3">
                                                            <img src="<?php echo base_url()?>assets/images/cp_1_img_3.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="<?php echo $cp2_slider_display?>" class = "cp2_slider3">
                                                            <img src="<?php echo base_url()?>assets/images/cp_2_img_3.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="<?php echo $cp3_slider_display?>" class = "cp3_slider3">
                                                            <img src="<?php echo base_url()?>assets/images/cp_3_img_3.jpg" class="img-responsive"/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-a">
                                                         <div class=" crsl-item-logo">
                                                            <img src="<?php echo $brand_logo_display?>" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center style="<?php echo $cp1_slider_display?>" class = "cp1_slider4">
                                                            <img src="<?php echo base_url()?>assets/images/cp_1_img_4.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="<?php echo $cp2_slider_display?>" class = "cp2_slider4">
                                                            <img src="<?php echo base_url()?>assets/images/cp_2_img_4.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center style="<?php echo $cp3_slider_display?>" class = "cp3_slider4">
                                                            <img src="<?php echo base_url()?>assets/images/cp_3_img_4.jpg" class="img-responsive"/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-a">
                                                         <div class=" crsl-item-logo">
                                                            <!--img src="<?php echo base_url()?>assets/images/default_brand_logo.png" class="img-responsive brand_logo_preview_slider"/-->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <nav class="slidernav">
                                                <div id="navbtns" class="clearfix">
                                                   <a href="#" class="previous pull-left">
                                                   <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                   </a>
                                                   <span class="previous_span"><strong>Setp 01:</strong> User Enter Mobile No.</span>
                                                   <a href="#" class="next pull-right">
                                                   <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                   </a>
                                                </div>
                                             </nav>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php 
                              }
                              else{
                                 ?>
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <form id="add_hotel_captive_portal"  enctype="multipart/form-data" onsubmit="add_hotel_captive_portal(); return false;">
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          
                                          <div id="hotel_ssid_div" style="display: <?php if($edit_location_data->location_type == '2') echo "block"; else echo "none";?>">
                                             <div class="row" >
                                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="hotel_guest_wifi_ssid" value = "<?php echo $edit_location_data->main_ssid?>">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
					     <div class="row">
						<legend>Conigure your captive portal for this location </legend>
					     </div>
					     
					     <div class="row" >
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="hotel_guest_wifi" value="1" <?php if($edit_location_data->cp_enterprise_user_type == '3' || $edit_location_data->cp_enterprise_user_type == '1')echo "checked"?>> <strong style="margin-left:15px">Guest WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="hotel_visitor_wifi" value="1" <?php if($edit_location_data->cp_enterprise_user_type == '3' || $edit_location_data->cp_enterprise_user_type == '2')echo "checked"?>> <strong style="margin-left:15px">Visitor WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          
                                          <div class="row" id="institutional_ssid_div" style="display: <?php if($edit_location_data->location_type == '4') echo "block"; else echo "none";?>">
                                             
					     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" name="institutional_wifi_ssid" value = "<?php echo $edit_location_data->main_ssid?>">
                                                   <span class="title_box"> (Max 24 Chars.)</span>
                                                   <label>SSID Name</label>
                                                </div>
                                             </div>
                                          </div>
					  <div id="caffe_ssid_div" style="display: <?php if($edit_location_data->location_type == '6') echo "block"; else echo "none";?>">
                                             <div class = "row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
						   <div class="mui-textfield mui-textfield--float-label">
						      <input type="text" name="caffe_wifi_ssid" value = "<?php echo $edit_location_data->main_ssid?>">
						      <span class="title_box"> (Max 24 Chars.)</span>
						      <label>SSID Name</label>
						   </div>
						</div>
					     </div>
					     <!--div class="row" >
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="is_otpdisabled" value="0" <?php if($edit_location_data->is_otpdisabled == '0' )echo "checked"?>> <strong style="margin-left:15px">IS OTP Enable</strong> 
                                                      </label>
                                                   </div>
                                                </div>
					     </div-->
					     
					      
                                          </div>
			      
                                          <div id="hospital_ssid_div" style="display: <?php if($edit_location_data->location_type == '3') echo "block"; else echo "none";?>">
                                             <div class="row" >
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="hospital_patient_wifi_ssid" value="<?php echo $edit_location_data->main_ssid?>">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
					     <div class="row">
						<legend>Conigure your captive portal for this location </legend>
					     </div>
					     <div class="row" >
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="hospital_patient_wifi" value="1" <?php if($edit_location_data->cp_enterprise_user_type == '3' || $edit_location_data->cp_enterprise_user_type == '1')echo "checked"?>> <strong style="margin-left:15px">Patient WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="hospital_guest_wifi" value="1" <?php if($edit_location_data->cp_enterprise_user_type == '3' || $edit_location_data->cp_enterprise_user_type == '2')echo "checked"?>> <strong style="margin-left:15px">Guest WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="enterprise_ssid_div" style="display: <?php if($edit_location_data->location_type == '5') echo "block"; else echo "none";?>">
                                             <div class="row" >
                                              
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-left">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="enterprise_employee_wifi_ssid" value="<?php echo $edit_location_data->main_ssid?>">
                                                      <span class="title_box"> (Max 24 Chars.)</span>
                                                      <label>SSID Name</label>
                                                   </div>
                                                </div>
                                             </div>
					     <div class="row">
						<legend>Conigure your captive portal for this location </legend>
					     </div>
					     <div class="row" >
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="enterprise_employee_wifi" value="1" <?php if($edit_location_data->cp_enterprise_user_type == '3' || $edit_location_data->cp_enterprise_user_type == '1')echo "checked"?>> <strong style="margin-left:15px">Employee WiFi </strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nomargin-left" style="margin-top:15px">
                                                   <div class="checkbox">
                                                      <label>
                                                      <input type="checkbox" name="enterprise_guest_wifi" value="1" <?php if($edit_location_data->cp_enterprise_user_type == '3' || $edit_location_data->cp_enterprise_user_type == '2')echo "checked"?>> <strong style="margin-left:15px">Guest WiFi</strong> 
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
				       
                                    </div>
                                    <!--div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                <legend>UPLOAD A BRAND LOGO</legend>
                                                <div class="mui-textfield mui-textfield--float-label blogoimg">
                                                   <input type="text" id="cp1_brand_image_name">
                                                   <input type="file" id="exampleInputFile" name="banner_image" class="hide"/>
                                                   <label>Logo Image <sup>*</sup></label>
                                                </div>
                                                <span class="cp_span">You can upload JPG. PNG, BMP upto 2mb in size</span>
                                             </div>
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-top: 40px">
                                                <p id="but" class="mui-btn mui-btn--small mui-btn--primary" style="height:30px; padding: 0px 25px; background-color:#36465f; font-weight:600; line-height: 30px">
                                                   CHOOSE FILE
                                                   <span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span>
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div-->
				    <div class="form-group" style="margin-top: 15px">
				       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="row">
						
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="row">
						      <label class="radio-inline">
							 <input type="radio" name="is_otpdisabled"  value="0" <?php if($edit_location_data->is_otpdisabled == '0') echo "checked"?>> OTP Enable
						      </label>
						      </div>
						   </div>
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="row">
						      <label class="radio-inline">
							 <input type="radio" name="is_otpdisabled"  value="1" <?php if($edit_location_data->is_otpdisabled == '1') echo "checked"?>> OTP Disable
						      </label>
						      </div>
						   </div>
						
					     </div>
				       </div>
				       
				    </div>
				    <div class="form-group" style="margin-top: 15px">
				       <input type="file" id="exampleInputFile" name="banner_image" class="hide"/>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
					     <legend>UPLOAD A BRAND LOGO</legend><br/>
					     <div class="dropzone" data-width="900" data-ajax="false" data-originalsize="false" data-height="300" style="width: 300px; height:100px;">
						<input type="file" name="thumb"  />
					     </div>
					  </div>
				       </div>
				    </div>
                                    <div class="form-group" id="institutional_user_type_div" style="display: <?php if($edit_location_data->location_type == '4' || $edit_location_data->location_type == '5') echo "block"; else echo "none";?>">
                                       <div class="col-sm-12 col-xs-12" style="margin-top: 20px;" id="cp_enterprise_user_listing">
                                          <div class="row">
                                             <div class="col-sm-5 col-xs-5">
                                                <div class="row">
                                                   <h4>Add department type </h4>
                                                </div>
                                             </div>
                                             <div class="col-sm-3 col-xs-3">
                                                <p class="mui-btn mui-btn--small mui-btn--danger institutional_add_user_button" style="margin-top:2px">Add</p>
                                             </div>
                                          </div>
                                          <?php
                                             $i = 1;
                                             foreach($edit_location_data->enterprise_user_list as $enterprise_user_list){
                                                ?>
                                          <div class="row " style="margin-top:10px; margin-bottom: 10px;">
                                             <div class="col-sm-6 col-xs-6 nopadding-left">
                                                <div class="mui-textfield mui-textfield--float-label">
                                                   <input type="text" data-id="<?php echo $enterprise_user_list->id?>" class="cp_institution_user_list mui--is-empty mui--is-untouched mui--is-pristine" name="cp_institution_user_list[]" value="<?php echo $enterprise_user_list->user_type?>">
                                                </div>
                                             </div>
                                          </div>
                                          <?php
                                             }
                                             ?>
                                       </div>
                                    </div>
                                    <?php
				       if($edit_location_data->location_type == '6'){
					  ?>
					  <div class="form-group">
					  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					     <div class="row">
						<h4 style="text-transform: uppercase; margin-bottom:0px">Select plans to be available</h4>
						<h4 style="margin-top:0px; font-size:18px; font-weight: 300 "><small>(You can select One plans for the location)</small></h4>
					     </div>
					     <div class="row" style="margin-bottom: 15px;">
						
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="row" >
							 <label class="radio-inline">
							    <input type="radio" name="cp_hotel_plan_type"  value="2" <?php if($edit_location_data->cp_hotel_plan_type == '2') echo "checked"?>> Time Plans
							 </label>
						      </div>
						      
						   </div>
						   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						      <div class="row" >
							 <label class="radio-inline">
							 <input type="radio" name="cp_hotel_plan_type"  value="1" <?php if($edit_location_data->cp_hotel_plan_type == '1') echo "checked"?>> Data Plans
							 </label>
						      </div>
						      
						   </div>
						
						
					     </div>
					     
					  </div>
				       </div>
					  <div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                <div class="mui-select">
                                                   <select name="cp_cafe_plan" id="cp_cafe_plan">
                                                   <?php
                                                      if($edit_location_data->cp_hotel_plan_type == '2'){
                                                         echo $plan_list = $this->location_model->plan_list('2', $edit_location_data->cp_cafe_plan_id);
                                                      }
						      else if($edit_location_data->cp_hotel_plan_type == '1'){
                                                         echo $plan_list = $this->location_model->plan_list('4', $edit_location_data->cp_cafe_plan_id);
                                                      }
                                                       
                                                      ?>
                                                   </select>
                                                   <!--label>Select Plan <sup>*</sup></label-->
                                                </div>
                                             </div>
                                             
                                          </div>
                                       </div>
                                    </div>
					  <?php 
				       }else{
					  ?>
					  				    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <h4 style="text-transform: uppercase; margin-bottom:0px">Select plans to be available</h4>
                                             <h4 style="margin-top:0px; font-size:18px; font-weight: 300 "><small>(You can select multiple plans for the location)</small></h4>
                                          </div>
                                          <div class="row" style="margin-bottom: 15px;">
                                             <label class="radio-inline">
                                             <input type="radio" name="cp_hotel_plan_type"  value="2" <?php if($edit_location_data->cp_hotel_plan_type == '2') echo "checked"?>> Time Plans
                                             </label>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group" style="margin-top: 15px">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="padding-left:0px;">
                                                <div class="mui-select">
                                                   <select name="cp_hotel_plan" id="cp_hotel_plan">
                                                   <?php
                                                      if($edit_location_data->cp_hotel_plan_type == '2'){
                                                         echo $plan_list = $this->location_model->plan_list('2', '0');
                                                      }
                                                       
                                                      ?>
                                                   </select>
                                                   <!--label>Select Plan <sup>*</sup></label-->
                                                </div>
                                             </div>
                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <h5 class="create_headings" onclick="cp_hotel_add_plan()">
                                                   Add Plan To List
                                                </h5>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12" style="margin-top: 20px;" id="cp_hotel_added_plan_listing">
                                       <?php
                                          foreach($edit_location_data->hotel_plan_list as $hotel_plan_list1){
                                             ?>
                                       <div class="row hotel_plan_listing" data-planid="<?php echo $hotel_plan_list1->plan_id?>">
                                          <div class="col-sm-5 col-xs-5 nopadding-left">
                                             <legend style="margin-top:5px"><?php echo $hotel_plan_list1->plan_name?></legend>
                                             <input type="hidden" name="cp_home_selected_plan_id[]" value="<?php echo $hotel_plan_list1->plan_id?>">
                                          </div>
                                          <div class="col-sm-3 col-xs-3">
                                             <p class="mui-btn mui-btn--small mui-btn--danger remove_hotel_plan" style="margin-top:2px">remove</p>
                                          </div>
                                       </div>
                                       <?php 
                                          }
                                          ?>
                                    </div>

					  <?php 
				       }
				    ?>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px">
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                <p id="add_hotel_captive_portal_error" style="color:red"></p>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value="exit"> SAVE & EXIT</button>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn"  value="save"> SAVE</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                       <div class="slider_div pull-right">
                                          <?php
                                             $brand_logo_display = base_url().'assets/images/radisson_blu_logo.png';
                                             if(isset($edit_location_data->logo_image) && $edit_location_data->logo_image != ''){
                                             $brand_logo_display = $edit_location_data->logo_image;
                                             }elseif( isset($edit_location_data->original_image) && $edit_location_data->original_image != ''){
                                             $brand_logo_display = $edit_location_data->original_image;
                                             }
                                             $cp_hotel_slider_display = 'display: none';
                                             $cp_hospital_slider_display = 'display: none';
                                             $cp_institutional_slider_display = 'display: none';
                                             $cp_enterprise_slider_display = 'display: none';
					     $cp_cafe_slider_display = 'display: none';
                                             $location_type = $edit_location_data->location_type;
                                             if($location_type == '2'){
                                             $cp_hotel_slider_display = 'display: block';
                                             }elseif($location_type == '3'){
                                             $cp_hospital_slider_display = 'display: block';
                                             }elseif($location_type == '4'){
                                             $cp_institutional_slider_display = 'display: block';
                                             }elseif($location_type == '5'){
                                             $cp_enterprise_slider_display = 'display: block';
                                             }elseif($location_type == '6'){
                                             $cp_cafe_slider_display = 'display: block';
                                             }
                                             ?>
                                          <div class="right_slider">
                                             <h3>CAPTIVE PORTAL SAMPLE</h3>
                                             <div class="crsl-items" data-navigation="navbtns">
                                                <div class="crsl-wrap">
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider1" style="<?php echo $cp_hotel_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_1_img.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_hospital_slider1" style="<?php echo $cp_hospital_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_institutional_slider1" style="<?php echo $cp_institutional_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_1.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider1" style="<?php echo $cp_enterprise_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_1.jpg" class="img-responsive "/>
                                                         </center>
							 <center class="cp_cafe_slider1" style="<?php echo $cp_cafe_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_1.jpg" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo $brand_logo_display?>" class="img-responsive brand_logo_preview_slider" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider2" style="<?php echo $cp_hotel_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_2_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider2" style="<?php echo $cp_hospital_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_2.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider2" style="<?php echo $cp_institutional_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_2.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider2" style="<?php echo $cp_enterprise_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_2.jpg" class="img-responsive "/>
                                                         </center>
							 <center class="cp_cafe_slider2" style="<?php echo $cp_cafe_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_2.jpg" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo $brand_logo_display?>" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider3" style="<?php echo $cp_hotel_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_3_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider3" style="<?php echo $cp_hospital_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_3.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider3" style="<?php echo $cp_institutional_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_3.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider3" style="<?php echo $cp_enterprise_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_3.jpg" class="img-responsive "/>
                                                         </center>
							 <center class="cp_cafe_slider3" style="<?php echo $cp_cafe_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_3.jpg" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo $brand_logo_display?>" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="crsl-item">
                                                      <div class="crsl-item_img">
                                                         <center class="cp_hotel_slider4" style="<?php echo $cp_hotel_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/hotel_cp_4_img.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_hospital_slider4" style="<?php echo $cp_hospital_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/hospital_cp_4.jpg" class="img-responsive"/>
                                                         </center>
                                                         <center class="cp_institutional_slider4" style="<?php echo $cp_institutional_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/institutional_cp_4.jpg" class="img-responsive "/>
                                                         </center>
                                                         <center class="cp_enterprise_slider4" style="<?php echo $cp_enterprise_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/enterprise_cp_4.jpg" class="img-responsive "/>
                                                         </center>
							 <center class="cp_cafe_slider4" style="<?php echo $cp_cafe_slider_display?>">
                                                            <img src="<?php echo base_url()?>assets/images/cafe_4.jpg" class="img-responsive "/>
                                                         </center>
                                                      </div>
                                                      <div class="crsl-item-logo-hotel">
                                                         <div class="crsl-item-hotel">
                                                            <img src="<?php echo $brand_logo_display?>" class="img-responsive brand_logo_preview_slider"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <nav class="slidernav">
                                                <div id="navbtns" class="clearfix">
                                                   <a href="#" class="previous pull-left">
                                                   <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                   </a>
                                                   <span class="previous_span"><strong>Setp 01:</strong> User Enter Mobile No.</span>
                                                   <a href="#" class="next pull-right">
                                                   <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                   </a>
                                                </div>
                                             </nav>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php 
                              }
                              ?>
                        </div>
                        <!-- captive portal tab end --> 
                        <!-- Nas portal tab start -->
                        <div class="mui-tabs__pane" id="nas_setup">
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <h2>NAS Setup</h2>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="col-sm-12 col-xs-12 mui--text-right">
                                    <p><strong><span class="location_name_view"></span><?php echo $edit_location_data->location_name?> <span class="location_id_view"></span></strong></p>
                                    <p><small style="color:#808080" class="email_view"><?php echo $edit_location_data->user_email?></small></p>
                                    <p><small style="color:#808080" class="mobile_number_view"><?php echo $edit_location_data->mobile_number?></small></p>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-12 col-xs-12">
                                 <form id="configure_nas" onsubmit="configure_nas(); return false;">
                                    <div id="nas_setup_data">
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:15px">
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                <p id="nas_select_erroe" style="color:red">
                                                <p>
						   <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value = "exit_router_tab"  id="exit_nas_router" >Exit</button>
                                                   <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value="microtic" id="microtic_button" <?php if($edit_location_data->nastype == '1' && $edit_location_data->nasconfigure != '1'){echo "style='display:block'";}else{echo "style='display:none'";}?>>Start Configuration</button>
                                                   <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn" value = "else"  id="else_router" <?php if($edit_location_data->nastype == '2' && $edit_location_data->nasconfigure != '1'){echo "style='display:block'";}else{echo "style='display:none'";}?>>Save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                        <!-- Nas portal tab END -->
                        <!-- access point tab start -->
                        <div class="mui-tabs__pane" id="access_points">
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <h2>Add Access Point</h2>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="col-sm-12 col-xs-12 mui--text-right">
                                    <p><strong><span class="location_name_view"></span><?php echo $edit_location_data->location_name?> <span class="location_id_view"></span></strong></p>
                                    <p><small style="color:#808080" class="email_view"><?php echo $edit_location_data->user_email?></small></p>
                                    <p><small style="color:#808080" class="mobile_number_view"><?php echo $edit_location_data->mobile_number?></small></p>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-12 col-xs-12">
                                 <form id="add_location_access_point" onsubmit="add_location_access_point(); return false;">
                                    <div class="form-group">
                                       <div class="row">
                                        
                                          <div class="col-sm-3 col-xs-3">
                                             <div class="mui-textfield mui-textfield--float-label" >
                                                <input type="text" name="access_point_macid" id="access_point_macid" required>
                                                <label>MAC ID <sup>*</sup></label>
                                             </div>
                                          </div>
                                          <!--div class="col-sm-3 col-xs-3">
                                             <div class="mui-textfield mui-textfield--float-label" >
                                                <input type="text" name="access_point_ip" id="access_point_ip" required>
                                                <label>IP<sup>*</sup></label>
                                             </div>
                                          </div-->
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select id="add_ap_location" name="add_ap_location" required>
                                                      <option value="">Select AP location</option>
                                                   </select>
                                                   <label>AP Location<sup>*</sup></label>
                                                </div>
                                             </div>
					  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h5 class="create_headings" onclick="add_ap_location_model()">
                                                   Add New AP Location
                                                </h5>
                                             </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                          <div class="row">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                                <p style="color:red" id="add_access_point_errro"></p>
                                                <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">ADD</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                       <div class="table-responsive">
                                          <table class="table table-striped">
                                             <thead>
                                                <tr class="active">
                                                   <th>&nbsp;</th>
                                                   <th>MAC ID</th>
                                                   <th>BRAND</th>
                                                   <!--th>IP</th-->
                                                   <!--th>STATUS</th-->
                                                   <th class="mui--text-right">ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody id="access_point_list">
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                             <button type="button" class="mui-btn mui-btn--accent btn-lg save_btn" onclick="close_access_point_tab()">SAVE & EXIT</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- access point tab END -->
                        <!-- voucher  tab start -->
                        <div class="mui-tabs__pane" id="Vouchers">
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <h2>Create New Voucher</h2>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                 <div class="col-sm-12 col-xs-12 mui--text-right">
                                    <p><strong><span class="location_name_view"></span><?php echo $edit_location_data->location_name?> <span class="location_id_view"></span></strong></p>
                                    <p><small style="color:#808080" class="email_view"><?php echo $edit_location_data->user_email?></small></p>
                                    <p><small style="color:#808080" class="mobile_number_view"><?php echo $edit_location_data->mobile_number?></small></p>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-6 col-xs-6">
                                 <div class="row">
                                    <label class="radio-inline">
                                    <input type="radio" name="voucher_radio" value="1" <?php if($edit_location_data->is_voucher_used == '1')echo "checked"?>> Voucher Use
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="voucher_radio"  value="0" <?php if($edit_location_data->is_voucher_used == '0')echo "checked"?>> Not use
                                    </label>
                                 </div>
                                 <div id="voucher_generate_div" <?php if($edit_location_data->is_voucher_used == '0') echo "style = 'display:none'";?>>
                                    <form id="add_location_vouchers" onsubmit="add_location_vouchers(); return false;">
                                       <div class="row">
                                          <div class="col-sm-5 col-xs-5">
                                             <div class="mui-textfield mui-textfield--float-label">
                                                <input type="text" id="voucher_data" name="voucher_data" required>
                                                <label>Voucher Value <sup>*</sup></label>
                                             </div>
                                          </div>
                                          <div class="col-sm-2 col-xs-2">
                                             <div class="mui-select">
                                                <select id="voucher_data_type" name="voucher_data_type" required>
                                                   <option value="MB">MB</option>
                                                   <option value="GB">GB</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row" style="margin-bottom:15px;">
                                          <div class="col-sm-5 col-xs-5">
                                             <div class="mui-textfield mui-textfield--float-label">
                                                <input type="text" id="voucher_cost" name="voucher_cost" required>
                                                <label>Voucher Cost Price <sup>*</sup></label>
                                             </div>
                                          </div>
                                          <div class="col-sm-5 col-xs-5">
                                             <div class="mui-textfield mui-textfield--float-label">
                                                <input type="text" id="voucher_selling_price" name="voucher_selling_price" required>
                                                <label>Voucher Selling Price <sup>*</sup></label>
                                             </div>
                                          </div>
                                          <div class="col-sm-3 col-xs-3">
                                             <p style="color:red" id="voucher_create_error"></p>
                                             <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">ADD</button>
                                          </div>
                                       </div>
                                    </form>
                                    <div class="row">
                                       <div class="col-sm-12 col-xs-6">
                                          <div class="table-responsive">
                                             <table class="table table-striped">
                                                <thead>
                                                   <tr class="active">
                                                      <th>&nbsp;</th>
                                                      <th>VOUCHER VALUE</th>
                                                      <th>VOUCHER COST</th>
                                                      <th>VOUCHER SELLING PRICE</th>
                                                      <th class="mui--text-right">ACTIONS</th>
                                                   </tr>
                                                </thead>
                                                <tbody id="voucher_list">
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nomargin-left">
                                             <button type="button" onclick="exit_voucher_panel()" class="mui-btn mui-btn--accent btn-lg save_btn">SAVE & EXIT</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- voucher tab END -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!---modal --->
<!-- modal  for asking user to router username and pass and ip  start-->
<div class="modal modal-container fade" id="add_location_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD NEW DEVICE</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="start_configuration(); return false;">
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">ROUTER IP</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="hidden" id="nas_id_selected" name="nas_id_selected" class="form-control">
                           <input type="text" class="form-control" placeholder="Router IP" name="router_ip" id="router_ip" required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">ROUTER USER NAME</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Router user name" name="router_user" id="router_user" required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">ROUTER PASSWORD</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="password" class="form-control" placeholder="Router password" name="router_password" id="router_password" >
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">PORT</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="number" class="form-control" placeholder="Port (Optional)" name="router_port" id="router_port" >
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           <p id="router_connection_error" style="color:red"> </p>
                           <center><button type="submit" class="btn btn-raised btn-primary btn-lg btn-block">NEXT</button></center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal  for asking user to router username and pass and ip  end-->
<!-- modal  for asking user to router type  start-->
<div class="modal modal-container fade" id="connection_type_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD NEW DEVICE</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="start_configuration1(); return false;">
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Router Type</label>
                        <div class="col-sm-6 col-xs-6">
                           <select class="form-control" name="router_type_wap" id="router_type_wap" required>
                              <option value="">Select Router type</option>
                              <option value="WAP">WAP</option>
                              <option value="WAPAC">WAP AC</option>
                              <option value="Rb850Gx2">Rb850Gx2</option>
                              <option value="Rb941-2nD">Rb941-2nD</option>
                              <option value="2011iL">2011iL-IN / 2011iL-RM</option>
                              <option value="CCR1036-8G-2S">CCR1036-8G-2S+ / CCR1036-8G-2S+-EM</option>
                              <option value="921GS-5HPacD">921GS-5HPacD</option>
                           </select>
                        </div>
                     </div>
                     <!--div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Connection Type</label>
                        <div class="col-sm-6 col-xs-6">
                           <select class="form-control"name="wan_connection_type" id="connection_type" required>
                        <option value="">Select Connection type</option>
                        <option value="DHCP">DHCP</option>
                        <option value="PPPOE">PPPOE</option>
                        <option value="STATIC">STATIC</option>
                           </select>
                        </div>
                        </div-->
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           <p id="router_connection_error1" style="color:red"> </p>
                           <center>
                              <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block">NEXT</button>
                           </center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal  for asking user to router type  end-->
<!-- modal  for asking user to router is reset or not  start-->
<div class="modal modal-container fade" id="is_router_reset_or_not" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-toggle="modal" data-target="#connection_type_modal" data-dismiss="modal" style="margin-left: 25px; float:left; font-size: 18px"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD NEW DEVICE</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                     <label class="controls-labels col-sm-12 col-xs-12">
                     <b>To begin configuring the router please ensure you have reset the router completely.</b>
                     </label>
                  </div>
                  <br /><br /><br />
                  <div class="form-group" style="margin-top: 20px; padding-bottom: 0px">
                     <center>
                        <p id="is_router_reset_or_not_error" style="color:red"> </p>
                     </center>
                     <br />
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <center><button onclick="reset_wap_router()" type="submit" class="btn btn-raised btn-primary btn-lg btn-block">RESET NOW</button></center>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <center><button onclick="check_wap_router_reset_porperly()" type="submit" class="btn btn-raised btn-primary btn-lg btn-block">NEXT</button></center>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal  for asking user to router is reset or not  end-->
<!-- modal  for start configration button  start-->
<div class="modal modal-container fade" id="is_router_start_configure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD NEW DEVICE</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                     <label class="controls-labels col-sm-12 col-xs-12">
                        <center><b>Start Configure your router.</b></center>
                     </label>
                  </div>
                  <br /><br /><br />
                  <div class="form-group" style="margin-top: 20px; padding-bottom: 0px">
                     <center>
                        <p id="is_router_start_configure_error" style="color:red"> </p>
                     </center>
                     <br />
                     <div class="col-lg-12 col-md-6 col-sm-6 col-xs-6">
                        <center><button onclick="start_configuration2()" type="submit" class="btn btn-raised btn-primary btn-lg btn-block">NEXT</button></center>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal  for start configration button  start-->
<div class="modal modal-container fade" id="done_configuration" tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">DONE Configuration</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
         </div>
      </div>
   </div>
</div>
<!-- edit voucer modal -->
<div class="modal modal-container fade"  tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static" id="EditvoucherModal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title">Edit Voucher</h4>
            </center>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="update_location_vouchers" onsubmit="update_location_vouchers(); return false;">
                     <div class="row">
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="hidden" id="voucher_id_edit" name="voucher_id_edit" required>
                              <input type="text" id="voucher_data_edit" name="voucher_data_edit" required>
                              <label>Voucher Value <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                           <div class="mui-select">
                              <select id="voucher_data_type_edit" name="voucher_data_type_edit" required>
                                 <option value="MB">MB</option>
                                 <option id="GB">GB</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="margin-bottom:15px;">
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="text" id="voucher_cost_edit" name="voucher_cost_edit" required>
                              <label>Voucher Cost price <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-5 col-xs-5">
                           <div class="mui-textfield ">
                              <input type="text" id="voucher_selling_price_edit" name="voucher_selling_price_edit" required>
                              <label>Voucher Selling Price <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                           <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">SAVE</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- edit ACCESS point modal -->
<div class="modal modal-container fade"  tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static" id="EditaccesspointModal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title">Edit Access Point</h4>
            </center>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="update_location_vouchers" onsubmit="update_location_access_point(); return false;">
                     <div class="row">
                       
                       <input type="hidden" id="access_point_id_edit" name="access_point_id_edit" required>
                        <div class="col-sm-3 col-xs-3">
                           <div class="mui-textfield ">
                              <input type="text" id="access_point_macid_edit" name="access_point_macid_edit" required>
                              <label>MAC ID  <sup>*</sup></label>
                           </div>
                        </div>
                        <!--div class="col-sm-3 col-xs-3">
                           <div class="mui-textfield ">
                              <input type="text" id="access_point_ip_edit" name="access_point_ip_edit" required>
                              <label>IP <sup>*</sup></label>
                           </div>
                        </div-->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <div class="mui-select">
                                                   <select id="edit_ap_location" name="edit_ap_location" required>
                                                      <option value="">Select AP location</option>
                                                   </select>
                                                   <label>AP Location<sup>*</sup></label>
                                                </div>
                                             </div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <h5 class="create_headings" onclick="add_ap_location_model()">
                                                   Add New AP Location
                                                </h5>
                                             </div>
                     </div>
                     <div class="row" style="margin-top:25px;">
                        <center>
                           <p style="color: red" id="edit_access_point_errro"></p>
                           <button type="submit" class="mui-btn mui-btn--accent btn-lg save_btn">SAVE</button>
                        </center>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal modal-container fade" id="add_zone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><strong>ADD ZONE</strong></h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p>Add a new zone to your location listing.</p>
            <div class="mui-textfield mui-textfield--float-label">
               <input type="text" id="zone_name_popup" name="zone_name_popup">
               <label>Zone Name</label>
            </div>
            <div class="row">
               <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                  <div class="mui-select">
                     <select id="city_popup">
                     <?php
                        echo $city_list = $this->location_model->city_list($edit_location_data->state_id,$edit_location_data->city_id);
                            ?>
                     </select>
                     <label>City</label>
                  </div>
               </div>
               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px ">
                  <!--a class="mui-btn mui-btn--small mui-btn--flat" style="color:#29ABE2 ">Add City</a-->
               </div>
            </div>
         </div>
         <div class="modal-footer" style="text-align: right">
            <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d ; color:#fff ">CANCEL</button>
            <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="add_zone()">ADD</button>
         </div>
      </div>
   </div>
</div>

<div class="modal modal-container fade" id="add_ap_location_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><strong>ADD AP LOCATION</strong></h4>
         </div>
         <div class="modal-body" style="padding-bottom:5px">
            <p>Add a new ap location  to your location listing.</p>
            <div class="mui-textfield mui-textfield--float-label">
               <input type="text" id="ap_location_name_popup" name="ap_location_name_popup">
               <label>AP Location Name</label>
            </div>
           
         </div>
         <div class="modal-footer" style="text-align: right">
	    <p style="color:red" id="add_ap_location_error"></p>
            <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d ; color:#fff ">CANCEL</button>
            <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="add_new_ap_location()">ADD</button>
         </div>
      </div>
   </div>
</div>


<!-- modal  for check location is confiure start-->
<div class="modal modal-container fade" id="is_location_proper_setup" tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">Check Configuration</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top:0px">
	    <div class="row">
	       <div class="col-sm-10 col-xs-12 col-md-offset-1">
		 <div class="single category">
		  <ul class="list-unstyled">
			<li>
			   Is Nas Setup
			   <span class="pull-right">
				 <button class="btn-view-fund btn btn-default btn-xs" type="button">
				    <span class="glyphicon glyphicon-remove" aria-hidden="true" id="check_conf_nas_setup_no" style="display:none"></span>
				    <span class="glyphicon glyphicon-ok" aria-hidden="true" id="check_conf_nas_setup_yes" style="display:none"></span>
				 </button>
			   </span>
			</li>
			<li>
			   Is Nas And Plan Associated
			   <span class="pull-right">
				 <button class="btn-view-fund btn btn-default btn-xs" type="button">
				    <span class="glyphicon glyphicon-remove" aria-hidden="true" id="check_conf_nas_plan_association_no" style="display:none"></span>
				    <span class="glyphicon glyphicon-ok" aria-hidden="true" id="check_conf_nas_plan_association_yes" style="display:none"></span>
				 </button>
			   </span>
			</li>
			
			
		  </ul>
	       </div>
	       </div>
	    </div>
         </div>
      </div>
   </div>
</div>

<!-- One hop network create start-->
<div class="modal modal-container fade" id="create_onehop_netword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">CREATE NEW NETWORK</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="create_onehop_network(); return false;">
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">ORGANIZATION</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Organization" name="onehop_organization" id="onehop_organization" value="SHOUUT" readonly="readonly" required>
			   <input type="hidden" name="onehop_address" id="onehop_address" >
			   <input type="hidden" name="onehop_latitude" id="onehop_latitude" >
			   <input type="hidden" name="onehop_longitude" id="onehop_longitude" >
			   <input type="hidden" name="onehop_is_new_network" id="onehop_is_new_network" >
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">NETWORK NAME</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Networkd name" name="onehop_network_name" id="onehop_network_name" readonly="readonly" required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">DESCRIPTION</label>
                        <div class="col-sm-6 col-xs-6">
                           <textarea class="form-control" placeholder="Description" name="onehop_description" id="onehop_description" ></textarea>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">MAXAPs</label>
                        <div class="col-sm-6 col-xs-6" style="margin-top: 15px;">
                           <input type="number" class="form-control" placeholder="Max Apx" name="onehop_maxaps" id="onehop_maxaps"  readonly="readonly" >
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">COA Status</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_coa_status" id="onehop_coa_status">
				 <!--option value=''>Select COA Status</option>
				 <option value='0'>Disable</option-->
				 <option value='1'>Enable</option>
			      </select>
			   </div>
                           
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">COA IP</label>
                        <div class="col-sm-6 col-xs-6" style="margin-top: 15px;">
                           <input type="text" class="form-control" placeholder="COA IP" name="onehop_coa_ip" id="onehop_coa_ip" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">COA SECRET</label>
                        <div class="col-sm-6 col-xs-6" style="margin-top: 15px;">
                           <input type="text" class="form-control" placeholder="Coa Secret" name="onehop_coa_secret" id="onehop_coa_secret" readonly="readonly">
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           
                           <center>
			      <p id="create_onehop_network_status" style="color:red"> </p>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" id="create_network_button">CREATE NETWORK</button>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" id="update_network_button">UPDATE NETWORK</button>
			   </center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- One hop network create end-->

<!-- One hop add ap start-->
<div class="modal modal-container fade" id="add_onehop_ap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD AP MAC</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <!--form id="add_location" onsubmit="add_onehop_ap(); return false;">
                     
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">AP MAC</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="AP MAC" name="onehop_ap_mac" id="onehop_ap_mac"  required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">AP Name</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="AP Name" name="onehop_ap_name" id="onehop_ap_name"  required>
                        </div>
                     </div>
                     
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           
                           <center>
			      <p id="add_onehop_ap_status" style="color:red"> </p>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" >ADD AP MAC</button>
			      
			   </center>
                        </div>
                     </div>
                  </form-->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- One hop add ap end-->

<!-- One hop add ssid start-->
<div class="modal modal-container fade" id="add_onehop_ssid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">ADD SSID</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="add_onehop_ssid(); return false;">
                     
                     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">SSIDIndex</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_ssid_index" id="onehop_ssid_index" required>
				 <!--option value=''>Select Index</option-->
				 <option value='0'>Index 0</option>
				 <!--option value='1'>Index 1</option>
				 <option value='2'>Index 2</option>
				 <option value='3'>Index 3</option>
				 <option value='4'>Index 4</option>
				 <option value='5'>Index 5</option>
				 <option value='6'>Index 6</option>
				 <option value='7'>Index 7</option-->
			      </select>
			   </div>
                           
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">SSID Name</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="SSID Name" name="onehop_ssid_name" id="onehop_ssid_name" readonly="readonly"  required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">Association</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_association" id="onehop_association">
				 <option value='0'>Open(No Encription)</option>
				 <option value='1'>PSK</option>
			      </select>
			   </div>
                           
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Pre Shared Key</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Pre Shared Key" name="onehop_psk" id="onehop_psk">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">WPA Mode</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_wap_mode" id="onehop_wap_mode">
				 <option value='3'> WPA and WPA2 </option>
				 <option value='1'> WPA Only </option>
				 <option value='2'> WPA2 Only </option>
				 
			      </select>
			   </div>
			</div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">Forwarding</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_forwarding" id="onehop_forwarding">
				 <option value='1'>  NAT Mode  </option>
				 <option value='2'> Bridge Mode  </option>
				 <option value='3'> Tunnel Mode </option>
			      </select>
			   </div>
			</div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">CP Mode</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_cp_mode" id="onehop_cp_mode">
				 <!--option value='0'> NO Captive Portal   </option>
				 <option value='1'> Internal Captive Portal   </option-->
				 <option value='2'>  External Captive Portal  </option>
			      </select>
			   </div>
			</div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">CP URL</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="CP URL" name="onehop_cp_url" id="onehop_cp_url">
                        </div>
                     </div>
		     
		     
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Walled Garden IP List</label>
                        <div class="col-sm-8 col-xs-8">
			   <div class="row">
			      <div class="col-sm-12">
				 <div class="row">
				    <div class="form-group">
				       <div class="col-sm-9 col-xs-9"  style="max-height:150px; overflow-y: auto;" id="wall_garder_ip_div">
					  <div class="input-group" style="margin:10px 0px">
					      <input type="text" class="form-control" placeholder="wall garder ip" name="wall_garder_ip[]">
					      <div class="input-group-btn">
						  <span class="btn btn-danger btn-sm" id="add_more_wallagrder">Add more</span>
					      </div>
					  </div>
					  
				       </div>
				    </div>
				 </div>
			      </div>
			   </div>
                        </div>
                     </div>
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Walled Garden Domain List</label>
                        <div class="col-sm-8 col-xs-8">
			   <div class="row">
			      <div class="col-sm-12">
				 <div class="row">
				    <div class="form-group">
				       <div class="col-sm-9 col-xs-9"  style="max-height:150px; overflow-y: auto;" id="wall_garder_domain_div">
					  <div class="input-group" style="margin:10px 0px">
					      <input type="text" class="form-control" placeholder="wall garder domain" name="wall_garder_domain[]">
					      <div class="input-group-btn">
						  <span class="btn btn-danger btn-sm" id="add_more_wallagrder_domain">Add more</span>
					      </div>
					  </div>
					  
				       </div>
				    </div>
				 </div>
			      </div>
			   </div>
                        </div>
                     </div>
		     
		     
		     
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Auth Server IP</label>
			
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Auth Server IP" name="onehop_auth_server_ip" id="onehop_auth_server_ip" readonly="readonly">
                        </div>
			
                     </div>
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Auth Server Port</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="number" class="form-control" placeholder="Auth Server Port" name="onehop_auth_server_port" id="onehop_auth_server_port" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Auth Server Secret</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Auth Server Secret" name="onehop_auth_server_secret" id="onehop_auth_server_secret" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">Accounting Status</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_accounting" id="onehop_accounting">
				 <!--option value='0'>  Accounting Disabled   </option-->
				 <option value='1'>  Accounting Enabled    </option>
			      </select>
			   </div>
			</div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Accounting Server IP</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Accounting Server IP" name="onehop_acc_server_ip" id="onehop_acc_server_ip" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Accounting Server Port</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="number" class="form-control" placeholder="Accounting Server port" name="onehop_acc_server_port" id="onehop_acc_server_port" readonly="readonly">
                        </div>
                     </div>
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Accounting Server Secret</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="Accounting Server Secret" name="onehop_acc_server_secret" id="onehop_acc_server_secret" readonly="readonly">
                        </div>
                     </div>
		     
		     
		     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">Accounting Interval in seconds </label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="number" class="form-control" placeholder="Accounting Interval in seconds " name="onehop_acc_interval" id="onehop_acc_interval" readonly="readonly">
                        </div>
                     </div>
		     
			
		    
		    
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           
                           <center>
			      <p id="add_onehop_ssid_status" style="color:red"> </p>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" >ADD SSID</button>
			      
			   </center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- One hop add ssid end-->


<!-- One hop add delete start-->
<div class="modal modal-container fade" id="delete_onehop_ssid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header" style="padding-left: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 25px;"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">
               <center>
                  <h4 style="color:#414042;font-weight: bold">DELETE SSID</h4>
               </center>
            </div>
         </div>
         <div class="modal-body" style="padding-top: 0px">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <form id="add_location" onsubmit="delete_onehop_ssid(); return false;">
                     
                     <div class="form-group" style="margin-top: 15px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4" style="margin-top: 10px;">SSIDIndex</label>
                        <div class="col-sm-6 col-xs-6">
			   <div class="mui-select">
			      <select name="onehop_ssid_index_delete" id="onehop_ssid_index_delete" required>
				 <option value=''>Select Index</option>
				 <option value='0'>Index 0</option>
				 <option value='1'>Index 1</option>
				 <option value='2'>Index 2</option>
				 <option value='3'>Index 3</option>
				 <option value='4'>Index 4</option>
				 <option value='5'>Index 5</option>
				 <option value='6'>Index 6</option>
				 <option value='7'>Index 7</option>
			      </select>
			   </div>
                           
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <label class="controls-labels col-sm-4 col-xs-4">SSID Name</label>
                        <div class="col-sm-6 col-xs-6">
                           <input type="text" class="form-control" placeholder="SSID Name" name="onehop_ssid_name_delete" id="onehop_ssid_name_delete"  required>
                        </div>
                     </div>
                     <div class="form-group" style="margin-top: 5px; padding-bottom: 0px">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:25px 0px">
                           
                           <center>
			      <p id="delete_onehop_ssid_status" style="color:red"> </p>
			      <button type="submit" class="btn btn-raised btn-primary btn-lg btn-block" >DELETE SSID</button>
			      
			   </center>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- One hop delete ssid end-->

<script>
   //location detail tab script
    
     
    
     
       
       
</script>

<script>
   //captive portal tab jquery
    $( '#but' ).click( function() {
             $( '#exampleInputFile' ).trigger( 'click' );
          } );
    var _URL = window.URL || window.webkitURL;
     $("#exampleInputFile").change(function(e) {
       
         var file, img;
         if ((file = this.files[0])) {
   var image_name = this.files[0].name;
   $("#cp1_brand_image_name").val(image_name);
             img = new Image();
             img.onload = function() {
                 var image_width = this.width;
                 var image_height = this.height;
   if (image_width < 600 || image_height < 200) {
     $("#add_captive_portal_error").html("Minimum Image size should be  of width 600px and height 200px");
     $("#add_hotel_captive_portal_error").html("Minimum Image size should be  of width 600px and height 200px");
     $('button').prop('disabled', true);
   }else{
     var  ratio = parseInt(image_width)/parseInt(image_height);
      if(ratio==3){
        $("#add_captive_portal_error").html("");
        $("#add_hotel_captive_portal_error").html("");
        $('button').prop('disabled', false);
      }else{
        $("#add_captive_portal_error").html("Ratio of Image to be 3:1");
        $("#add_hotel_captive_portal_error").html("Ratio of Image to be 3:1");
                      $('button').prop('disabled', true);
      }
   }
   
      };
             img.src = _URL.createObjectURL(file);
         }
     });
     
     //on brand image chane show in slider
     (function() {
         var URL = window.URL || window.webkitURL;
         var input = document.querySelector('#exampleInputFile');
         var preview = document.querySelector('.brand_logo_preview_slider');
         // When the file input changes, create a object URL around the file.
         input.addEventListener('change', function () {
         $('.brand_logo_preview_slider').attr('src',URL.createObjectURL(this.files[0]));
             //preview.src = URL.createObjectURL(this.files[0]);
             $("#unset_image_button").show();
     
         });
        
   
     })();
     function unset_image(){
       var default_image_src = '<?php echo base_url()?>assets/images/default_brand_logo.png';
        $('.brand_logo_preview_slider').attr('src',default_image_src);
        $('button').prop('disabled', false);
         $("#add_captive_portal_error").html("");
   //document.getElementById("exampleInputFile").value = "";
   $("#cp1_brand_image_name").val("");
   $('#exampleInputFile').remove();
   $('.blogoimg').append('<input type="file" id="exampleInputFile" name="banner_image" class="hide"/>');
     }
     // on cp type change show and hide div
       
       
      
       
       
    
    
       
        
       function delete_access_point(access_point_id) {
   if (confirm("Are you sure you want to delete?")) {
      var location_uid =  $("#location_id_hidden").val();
      $(".loading").show();
       $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/delete_access_point",
        data: "access_point_id="+access_point_id,
        success: function(data){
   	access_point_list(location_uid);
        }
         });
   }
   return false;
       }
       ;
     
       function close_access_point_tab(){
   $(".loading").show();
   window.location = "<?php  echo base_url()?>location";
       }
       
      
       // cp_hotel plan type change set plan list
       $("input[name='cp_hotel_plan_type']").change(function() {
      $("#cp_hotel_added_plan_listing").html('');//clear plan listing
      var plan_type = $("input[name='cp_hotel_plan_type']:checked").val();
      var plan_val = 0;
      if (plan_type == '1') {
         plan_val = 4;
      }else if (plan_type == '2') {
         plan_val = 2;
      }
      $(".loading").show();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>location/plan_list",
        data: "plan_type="+plan_val,
        success: function(data){
	 $(".loading").hide();
	 $("#cp_hotel_plan").html(data);
	 $("#cp_cafe_plan").html(data);
        }
      });
       });
       
      
       
       
       
       $("input[name='cp_enterprise_user_type']").change(function() {
      var type_val = $(this).val();
      if (type_val == '2') {
         $("#institutional_user_type_div").hide();
      }else{
         $("#institutional_user_type_div").show();
      }
       });
       
       
       
       
       
       
       function is_location_proper_setup() {
	 var location_uid =  $("#location_id_hidden").val();
	 $(".loading").show();
	 $.ajax({
	    type: "POST",
	    url: "<?php echo base_url()?>location/is_location_proper_setup",
	    data: "location_uid="+location_uid,
	    dataType: 'json',
	    success: function(data){
	       if(data.is_nas_setup == '1'){
		  $("#check_conf_nas_setup_yes").show();
		  $("#check_conf_nas_setup_no").hide();
	       }else{
		  $("#check_conf_nas_setup_yes").hide();
		  $("#check_conf_nas_setup_no").show();
	       }
	       
	       if(data.is_nas_plan_associated == '1'){
		  $("#check_conf_nas_plan_association_yes").show();
		  $("#check_conf_nas_plan_association_no").hide();
	       }else{
		  $("#check_conf_nas_plan_association_yes").hide();
		  $("#check_conf_nas_plan_association_no").show();
	       }
	       $(".loading").hide();
	       $("#is_location_proper_setup").modal("show");
	    }
         });
	 
      }
</script>

<script>
   
   

   function add_onehop_ap_view() {
      $("#add_onehop_ap").modal("show");
   }
   function add_onehop_ap(){
      var location_uid = $("#location_id_hidden").val();
      var onehop_ap_mac = $("#onehop_ap_mac").val();
      var onehop_ap_name = $("#onehop_ap_name").val();
      if (onehop_ap_mac == '' || onehop_ap_name == '') {
	 $("#add_onehop_ap_status").html("Please fill all the fields");
	 return false;
      }
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_add_apmac",
   	    data: "location_uid="+location_uid+"&onehop_ap_mac="+onehop_ap_mac+"&onehop_ap_name="+onehop_ap_name,
	    dataType: 'json',
   	    success: function(data){
	       
	       if (data.code == '1') {
		  $("#add_onehop_ssid_status").html('');
		  $("#add_onehop_ap_status").html("");
		  $("#add_onehop_ssid").modal("hide");
		  nas_setup_list(location_uid);
	       }else{
		  $(".loading").hide();
		 $("#add_onehop_ap_status").html(data.msg);
	       }
	      
   	    }
      });
   }
   
   
   
   
   function delete_onehop_ssid_view() {
      $("#delete_onehop_ssid").modal("show");
   }
   $("#onehop_ssid_index_delete").on("change",function(){
      
      var index_number = $(this).val();
      var location_uid = $("#location_id_hidden").val();
      $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_get_ssid_detail",
   	    data: "location_uid="+location_uid+"&index_number="+index_number,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	       if (data.resultCode == '1') {
		  //alert(data.ssid_name);
		  $("#onehop_ssid_name_delete").val(data.ssid_name);
	       
	       }
   	    }
      });
   });
   function delete_onehop_ssid() {
      var location_uid = $("#location_id_hidden").val();
      var onehop_ssid_index = $("#onehop_ssid_index_delete").val();
      var onehop_ssid_name = $("#onehop_ssid_name_delete").val();
       $(".loading").show();
      $.ajax({
   	    type: "POST",
   	    url: "<?php echo base_url()?>location/onehop_delete_ssid",
   	    data: "location_uid="+location_uid+"&onehop_ssid_index="+onehop_ssid_index+"&onehop_ssid_name="+onehop_ssid_name,
	    dataType: 'json',
   	    success: function(data){
	       $(".loading").hide();
	      
		  $("#delete_onehop_ssid_status").html(data.msg);
	       
	       
   	    }
      });
   }
</script>

</body>
</html>
