 <?php
 $this->load->view('includes/header');
 
 ?>
   <style>
     
   #loadMore {
   padding: 5px 15px;
   text-align: center;
   color: #231f20;
    border-radius:25px; 
    margin: 25px auto;
    border: 1px solid #231f20;
    font-size: 12px;
    font-weight: 600;
    cursor: pointer;
   }
#loadMore:hover {
   background-color: #231f20;
   color: #fff;
    text-decoration: none;
}
   
  
 </style>
      <!-- Start your project here-->
      <div class="loading" >
	<img src="<?php echo base_url()?>assets/images/loader.svg"/>
     </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                 
                  <?php
		  $data['navperm']=$this->plan_model->leftnav_permission();
		  $this->load->view('includes/left_nav');?>  
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Legal</a>
                        </div>
			 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			   
			   <?php $this->load->view('includes/global_setting',$data);?>
                        </ul>
                     </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <h1><small>Legal Intervention Lookup</small></h1>
                           </div>
                        </div>
                        <div class="row">
                           <form method="post" action="<?php echo base_url()?>legal_intervention">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group" style="margin-top:15px;">
                                    <div class="col-sm-3 col-xs-3 nopadding-left">
                                       <div class="mui-select">
					 <?php $location_list =
					    $this->legal_intervention_model->get_location_list(isset($_POST['location'])?$_POST['location']:'');
					  ?>
                                          <select name="location" id="location">
                                            <?php echo $location_list?>
                                          </select>
                                          <label>Select Device IP / Location <sup>*</sup></label>
                                       </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-3">
                                       <div class="mui-textfield mui-textfield--float-label">
                                          <input type="tel" id="mobile" name="mobile" <?php if(isset($_POST['mobile'])) echo "value='".$_POST['mobile']."'"?>>
                                          <label>10 Digit Mobile No.<sup>*</sup></label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-12 col-xs-12" style="margin-bottom:15px;">
                                 <div class="form-group" style="margin-top:15px;">
                                    <div class="col-sm-3 col-xs-3 nopadding-left">
                                       <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                                          <div class="input-group" style="margin-top:-5px">
                                             <div class="input-group-addon" style="padding:6px 10px">
                                                <i class="fa fa-calendar"></i>
                                             </div>
                                             <input type="text" id="date-start" class="form-control date" name="selecteddate" placeholder="From*" <?php if(isset
																			($_POST['selecteddate'])) echo
																				"value='".$_POST['selecteddate']."'"?>>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-3">
                                       <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                                          <div class="input-group" style="margin-top:-5px">
                                             <div class="input-group-addon" style="padding:6px 10px">
                                                <i class="fa fa-calendar"></i>
                                             </div>
                                             <input type="text" id="date-end" class="form-control date"  name="selecteddateto" placeholder="To*" <?php if(isset
																			($_POST['selecteddateto'])) echo "value='".$_POST['selecteddateto']."'"?>>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-2">
                                       <button type="submit" class="mui-btn mui-btn--accent btn-lg btn-block">LOOKUP</button>
                                    </div>
                                    <div class="col-sm-2 col-xs-2">
                                       <!--h5 class="create_headings" id="">
                                          Clear
                                       </h5-->
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-12 col-xs-12">
                                 <div class="col-sm-6 col-xs-6">
                                    <div class="row">
                                       <h1><?php echo $total_record?> <small>Results</small></h1>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 col-xs-6">
                                    <div class="row pull-right">
                                       <a onclick="export_to_excel()" class="mui-btn mui-btn--small mui-btn--accent pull-right"><i class="fa fa-download fa-lg" aria-hidden="true"></i> excel</a>
                                    </div>
                                 </div>
                              </div>
                             </form>
			      <div class="col-sm-12 col-xs-12">
                                 <div class="table-responsive">
                                    <table class="table table-striped">
                                       <thead>
                                          <tr class="active">
                                             <th>MOBILE</th>
					     <th>NAME</th>
					     <th>EMAIL</th>
                                             <th>SESSION START</th>
                                             <th>SESSION STOP</th>
					     <th>MB USED(U/D)</th>
                                             <th>NAS IP</th>
                                             <th>MAC ID</th>
                                             <!--<th>NAS PORT ID</th>
                                             <th>NAS PORT TYPE</th>-->
                                          </tr>
                                       </thead>
                                       <tbody id="search_gridview">
					<?php
					/*foreach($user_data as $user_data1){
					  $stop_time = '';
					  if($user_data1->acctstoptime != ''){
					    $stop_time = date("d.m.Y H:i:s",strtotime($user_data1->acctstoptime));
					  }
					  echo '<tr>
                                             <td>'.$user_data1->mobile.'</td>
                                             <td>'.$user_data1->email.'</td>
                                             <td>'.date("d.m.Y H:i:s",strtotime($user_data1->acctstarttime)).'</td>
                                             <td>'.$stop_time.'</td>
					     <td>'.round($user_data1->acctinputoctets/(1024*1024), 2).'/'.round($user_data1->acctoutputoctets/(1024*1024), 2).'</td>
                                             <td>'.$user_data1->nasipaddress.'</td>
                                             <td>'.$user_data1->callingstationid.'</td>
                                             <td>'.$user_data1->nasportid.'</td>
					     <td>'.$user_data1->nasporttype.'</td>
                                          </tr>';
					}*/
					  
					?>
                                          
                                          
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           
                        </div>
			<div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                 <div class="row" style="text-align:center">
                                    <input type='hidden' id="plimit" value="" />
                                    <input type='hidden' id="poffset" value="" />
				    <input type='hidden' id="ploadmoredata" value="1" />
                                    <div class="ploadmore_loader" style="display: none">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
                                    </div>
				    <div  onclick="load_more_location()" style="display: none" class="load_more_button">
				       <span id="loadMore">Load More</span>
				    </div>
                                 </div>
                              </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <script>
         $(document).ready(function() {
	  $(".loading").hide();
           /*var height = $(window).height();
            $('#main_div').css('height', height);
          var herader_height = $("#header").height();
         height = height - herader_height;
           $('#right-container-fluid').css({'height':(windowHeight)+'px'});
           */
         });
      </script>
      
     
      <script type="text/javascript">
	$(document).ready(function()
	{
		$('#date-start').bootstrapMaterialDatePicker
		({
			weekStart: 0, format: 'DD-MM-YYYY HH:mm',
			//shortTime : true,
			maxDate: moment()
		}).on('change', function(e, date)
		{
			$('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
		});

		$('#date-end').bootstrapMaterialDatePicker
		({
			weekStart: 0, format: 'DD-MM-YYYY HH:mm',
			maxDate: moment()
		}).on('change', function(e, date)
		{
			$('#date-start').bootstrapMaterialDatePicker('setMaxDate', date);
		});
		//$.material.init()
	});
	
	
	
      
      
      $(document).ready(function() {
	
	   var search_results = '<?php echo $user_data['search_results']; ?>' ;
	    var paylimit = "<?php echo $user_data['limit'] ?>";
	    var payoffset = "<?php echo $user_data['offset'] ?>";
	    var payloadmore = "<?php echo $user_data['loadmore'] ?>";
	    var total_record = "<?php echo $user_data['total_record'] ?>";
	    $('#search_gridview').html(search_results);
	    $('#plimit').val(paylimit);
	    $('#poffset').val(payoffset);
	    $('#ploadmoredata').val(payloadmore);
	    $('.ploadmore_loader').hide();
	    if (parseInt(total_record) >= parseInt(paylimit)) {
	       $(".load_more_button").show();
	    }
	 
	 });
      
      
      function load_more_location(){
	 $(".load_more_button").hide();
	 var formdata = '';
	 var location = $("#location").val();
	  var mobile = $("#mobile").val();
	  var selecteddate = $("#date-start").val();
	  var selecteddateto = $("#date-end").val();
	  
	 var limit = $('#plimit').val();
	 var offset = $('#poffset').val();
	 var loaded = $('#ploadmoredata').val();
	 if (loaded != '0') {
	    $('.ploadmore_loader').show();
		     $.ajax({
			url: base_url+'legal_intervention/list_more',
			type: 'POST',
			dataType: 'json',
			data: ({'limit':limit,'offset':offset,'location':location,'mobile':mobile,'selecteddate':selecteddate,'selecteddateto':selecteddateto}),
			success: function(data){
			   $('#search_gridview').append(data.search_results);
			   $('#plimit').val(data.limit);
			   $('#poffset').val(data.offset);
			   $('#ploadmoredata').val(data.loadmore);
			   $('.ploadmore_loader').hide();
			   if (data.loadmore != '0') {
			      $(".load_more_button").show();
			   }
			   
			}
		     });
	 }
      }
      
       function export_to_excel(){
	 var mobile = $("#mobile").val();
	 var date_start = $("#date-start").val();
	 var date_end = $("#date-end").val();
	 var location = $("#location").val();
	 $(".loading").show();
	  $.ajax({
	     url: '<?php echo base_url()?>legal_intervention/legal_export_excel',
	     type: 'POST',
	     dataType: 'text',
	     data: ({'mobile': mobile, 'date_start': date_start, 'date_end': date_end, 'location':location}),
	     success: function(data){
	       $(".loading").hide();
	       if(data == '1'){
		       window.location = "<?php echo base_url()?>legal_intervention/legal_download_excel";
	       }
		
	     }
	  });
	 
      }
</script>
   </body>
</html>
