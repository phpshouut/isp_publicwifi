 <?php
 $this->load->view('includes/header');
 
 ?>
 <style>
     
   #loadMore {
   padding: 5px 15px;
   text-align: center;
   color: #231f20;
    border-radius:25px; 
    margin: 25px auto;
    border: 1px solid #231f20;
    font-size: 12px;
    font-weight: 600;
    cursor: pointer;
   }
#loadMore:hover {
   background-color: #231f20;
   color: #fff;
    text-decoration: none;
}
   
  
 </style>
   <div class="loading" >
      <img src="<?php echo base_url()?>assets/images/loader.svg"/>
   </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
              <div id="sidedrawer" class="mui--no-user-select">
              
             <?php
	     $data['navperm']=$this->plan_model->leftnav_permission();
	     $this->load->view('includes/left_nav');?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Wifi Session Report Location</a>
                        </div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			  <?php $this->load->view('includes/global_setting',$data);?>
                        </ul>
                     </div>
                     </div>
		      
                  </nav>
		</header>
	       

<div id="content-wrapper">
  <div class="mui--appbar-height"></div>
    <div class="mui-container-fluid" id="right-container-fluid">
      <div class="right_side">
        <div class="row">
	  <form method="post" accept-charset="utf-8" action="<?php echo base_url("sessionReport/wifi_session_failure_report"); ?>" id="location_session_filter_form">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="row" style="margin-bottom: 15px">
		   <div class="col-sm-5 col-xs-5">
                     
                  </div>
                  <div class="col-sm-2 col-xs-2">
                     <h1><small>Select Date</small></h1>
                  </div>
                  <div class="col-sm-4 col-xs-4">
                     <div class="row">
			
                        <div class="col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                              <div class="input-group" style="margin-top:-5px">
                                 <div class="input-group-addon" style="padding:6px 10px">
                                    <i class="fa fa-calendar"></i>
                                 </div>
                                 <input type="text" class="form-control date"  placeholder="Start Date*" name="start_date" value="<?php if(isset($_POST['start_date'])){echo $_POST['start_date'];}?>">
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6 col-xs-6 nopadding-right">
                           <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                              <div class="input-group" style="margin-top:-5px">
                                 <div class="input-group-addon" style="padding:6px 10px">
                                    <i class="fa fa-calendar"></i>
                                 </div>
                                 <input type="text" class="form-control date"  placeholder="End Date*"  name="end_date" value="<?php if(isset($_POST['end_date'])){echo $_POST['end_date'];}?>">
                                 <div onclick="submit_filter_form()"  class="input-group-addon btn mui-btn--accent" style="padding:6px 10px; height:30px; line-height:23px; box-shadow:none;">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                       
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="mui-row">
                  <div class="mui-col-md-4">
                     <h2>&nbsp;</h2>
                  </div>
                  <div class="mui-col-md-8">
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="today" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400">Today</button>
                     </div>
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="yesterday" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400">Yesterday</button>
                     </div>
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="lastweek" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400"> Last Week</button>
                     </div>
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="last15days" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400"> Last 15 Days</button>
                     </div>
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="last30days" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400"> Last 30 Days</button>
                     </div>
                  </div>
               </div>
            </div>
	    
	    
	    	       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="row">
				 
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="state_id_filter" id="state_id_filter" >
					  <option value="">All States</option>
					     <?php
						$state_list = $this->sessionReport_model->state_list();
						if($state_list->resultCode == 1){
						   foreach($state_list->state as $state_list_view){
						      $sel = '';
						      if($state_list_view->state_id == $_POST['state_id_filter']){
							  $sel = 'selected="selected"';
						      }
						      echo '<option value = "'.$state_list_view->state_id.'" '.$sel.'>'.$state_list_view->state_name.'</option>';
						   }
						}
					     ?>
				       </select>
				       <label>State</label>
				    </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="city_id_filter" id="city_id_filter" >
					  <?php
					  if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
					     $state_id = $_POST['state_id_filter'];
					     $city_id = '';
					     if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
						$city_id = $_POST['city_id_filter'];
					     }
					     $city_list = $this->sessionReport_model->city_list($state_id, $city_id);
					     echo $city_list;
					  }else{
					     echo '<option value="">All Cities</option>';
					  }
					  ?>
					  
				       </select>
				       <label>City</label>
				    </div>
				 </div>
			      </div>
			   
			</div>

</form>

	    
            

            <div class="col-lg-12 col-md-12 col-sm-12">
               <div class="table-responsive">
                  <table class="table table-striped">
                     <thead>
                        <tr class="active">
                           <th colspan="2" class="text-center">FAILURE SESSION RECORD</th>
                        </tr>
                     </thead>
                     <tbody>
                                                
                      
                        
                        <tr style="padding:5px 0px">
                           <td colspan="2"  style="padding:5px 0px">
                              <table class="table table-bordered table-striped">
                                 
                                 <tr>
                                                <th>Location</th>
                                                <th>Total Impression</th>
                                                <th>Total User</th>
                                                <th>New Registration</th>
                                                <th>OTP Send</th>
                                                <th>OTP Verified</th>
                                                <th>Success Net</th>
                                            </tr>
				 <tbody id="search_gridview">
					     
                                             
                                             
                                 </tbody>
				
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                 <div class="row" style="text-align:center">
                                    <input type='hidden' id="plimit" value="" />
                                    <input type='hidden' id="poffset" value="" />
				    <input type='hidden' id="ploadmoredata" value="1" />
                                    <div class="ploadmore_loader" style="display: none">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
                                    </div>
				    <div  onclick="load_more_location()" style="display: none" class="load_more_button">
				       <span id="loadMore">Load More</span>
				    </div>
                                 </div>
                              </div>
	
	</div>
      </div>
   </div>
</div>


            </div>
         </div>
      </div>
      <!-- /Start your project here-->
      <!-- div for view bssid-->
<div id="info_model" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top:4%; width:712px;height:400px!important; ">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h3 class="modal-title">User List</h3></center>
            </div>
            <div class="modal-body"  style="width:710px;height:400px!important; overflow-y:scroll; padding-bottom:10px;
         " >

                <center id="info_data">
                </center>


            </div>

        </div>

    </div>
</div>
      <script>
         $(document).ready(function() {
	    $(".loading").hide();
           var height = $(window).height();
            $('#main_div').css('height', height);
          var herader_height = $("#header").height();
         height = height - herader_height;
           $('#right-container-fluid').css({'height':(windowHeight)+'px'});
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
         $('#advance_filters').click(function() {
         //alert("Hello! I am an alert box!!");
              $('.advance_filters_div').slideToggle("400");
         });
         // search box dropdown // 
         $('#search_input').keyup(function(){  
         $('#search_filter_areas').show();
         });
         $('body').click(function(e) {
         if(e.target.id != 'search_input'){
         $('#search_filter_areas').hide();
         }
         
         });
         });
         
         $(document).click(function(){
         
         $("#left_menu").toggleClass('closed');
         $("#left_menu2").toggleClass('open'); 
         });
      </script>
		<script>
	  $(function() {
		$('#toggle-two').bootstrapToggle({
		  on: 'Enabled',
		  off: 'Disabled'
		});
	  })
	</script>
	<script type="text/javascript">
         $(document).ready(function(){
            $('.date').bootstrapMaterialDatePicker({
               format: 'DD-MM-YYYY',
               time: false,
               clearButton: true
            });
            
            $('.date-start').bootstrapMaterialDatePicker({
               weekStart: 0, format: 'DD-MM-YYYY', shortTime : true
            }).on('change', function(e, date){
                  $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
               });
            
            $('.min-date').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', minDate : new Date() });
            
            $.material.init()
         });
	 
	//submit filter form
	 function submit_filter_form(){
	    $(".loading").show();
	    document.getElementById("location_session_filter_form").submit(); 
	 }
	 
	 
	 $(document).ready(function() {
	    var search_results = "<?php echo $wifi_failure_session_data['search_results']; ?>" ;
	    var paylimit = "<?php echo $wifi_failure_session_data['limit'] ?>";
	    var payoffset = "<?php echo $wifi_failure_session_data['offset'] ?>";
	    var payloadmore = "<?php echo $wifi_failure_session_data['loadmore'] ?>";
	    var total_record = "<?php echo $wifi_failure_session_data['total_record'] ?>";
	    $('#search_gridview').html(search_results);
	    $('#plimit').val(paylimit);
	    $('#poffset').val(payoffset);
	    $('#ploadmoredata').val(payloadmore);
	    $('.ploadmore_loader').hide();
	    if (parseInt(total_record) >= parseInt(paylimit)) {
	       $(".load_more_button").show();
	    }
	 
	 });
	 
	 
      function load_more_location(){
	 $(".load_more_button").hide();
	 var formdata = '';
	 var state_id_filter = $("#state_id_filter").val();
	 var city_id_filter = $("#city_id_filter").val();
	 var start_date = $("input[name='start_date']").val();
	 var end_date = $("input[name='end_date']").val();
	 var limit = $('#plimit').val();
	 var offset = $('#poffset').val();
	 var loaded = $('#ploadmoredata').val();
	 if (loaded != '0') {
	    $('.ploadmore_loader').show();
		     $.ajax({
			url: base_url+'sessionReport/wifi_session_failure_report_load_more',
			type: 'POST',
			dataType: 'json',
			data: ({'limit':limit,'offset':offset, 'start_date':start_date, 'end_date':end_date,'state_id_filter': state_id_filter,'city_id_filter':city_id_filter}),
			success: function(data){
			   $('#search_gridview').append(data.search_results);
			   $('#plimit').val(data.limit);
			   $('#poffset').val(data.offset);
			   $('#ploadmoredata').val(data.loadmore);
			   $('.ploadmore_loader').hide();
			   if (data.loadmore != '0') {
			      $(".load_more_button").show();
			   }
			   
			}
		     });
	 }
      }
      
      $('#state_id_filter').change(function(){
	       $('#city_id_filter').val('');
	       submit_filter_form(); 
	    });
	    $('#city_id_filter').change(function(){
	       submit_filter_form(); 
	    });
	

      </script>
</body>
</html>
