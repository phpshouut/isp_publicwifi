<?php
 $this->load->view('includes/header');
 
 ?>
   <div class="loading" >
      <img src="<?php echo base_url()?>assets/images/loader.svg"/>
   </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
              <div id="sidedrawer" class="mui--no-user-select">
               
             <?php
	     $data['navperm']=$this->plan_model->leftnav_permission();
	     $this->load->view('includes/left_nav');?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Wifi Location Session Report</a>
                        </div>
			 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			   
			   <?php $this->load->view('includes/global_setting',$data);?>
                        </ul>
                     </div>
                     </div>
                  </nav>
		</header>
<div id="content-wrapper">
  <div class="mui--appbar-height"></div>
    <div class="mui-container-fluid" id="right-container-fluid">
      <div class="right_side">
        <div class="row">
	  <form method="post" accept-charset="utf-8" action="<?php echo base_url("sessionReport"); ?>" id="location_session_filter_form">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="row" style="margin-bottom: 15px">
                  <div class="col-sm-2 col-xs-2">
                     <h1><small>Select Date</small></h1>
                  </div>
                  <div class="col-sm-10 col-xs-10">
                     <div class="row">
                        <div class="col-sm-3 col-xs-3">
                           <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                              <div class="input-group" style="margin-top:-5px">
                                 <div class="input-group-addon" style="padding:6px 10px">
                                    <i class="fa fa-calendar"></i>
                                 </div>
                                 <input type="text" class="form-control start_date"  placeholder="Start Date*" name="start_date" value="<?php if(isset($_POST['start_date'])){echo $_POST['start_date'];}?>">
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-3 col-xs-3 nopadding-right">
                           <div class="mui-textfield mui-textfield--float-label" style="padding-top: 10px">
                              <div class="input-group" style="margin-top:-5px">
                                 <div class="input-group-addon" style="padding:6px 10px">
                                    <i class="fa fa-calendar"></i>
                                 </div>
                                 <input type="text" class="form-control end_date"  placeholder="End Date*"  name="end_date" value="<?php if(isset($_POST['end_date'])){echo $_POST['end_date'];}?>">
                                 <div onclick="submit_filter_form()" class="input-group-addon btn mui-btn--accent" style="padding:6px 10px; height:30px; line-height:23px; box-shadow:none;">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                       
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="mui-row">
                  <div class="mui-col-md-4">
                     <h2>&nbsp;</h2>
                  </div>
                  <div class="mui-col-md-8">
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="today" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400">Today</button>
                     </div>
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="yesterday" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400">Yesterday</button>
                     </div>
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="lastweek" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400"> Last Week</button>
                     </div>
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="last15days" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400"> Last 15 Days</button>
                     </div>
                     <div class="mui-col-md-2" style="width: auto">
                        <button onclick="submit_filter_form()" name="last30days" class="btn mui-btn--accent" style="height: 30px; line-height: 10px; box-shadow:none; font-size: 14px; font-weight: 400"> Last 30 Days</button>
                     </div>
                  </div>
               </div>
            </div>
	    
	    
	    
	    
	    	       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="row">
				 
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="state_id_filter" id="state_id_filter" >
					  <option value="">All States</option>
					     <?php
						$state_list = $this->sessionReport_model->state_list();
						if($state_list->resultCode == 1){
						   foreach($state_list->state as $state_list_view){
						      $sel = '';
						      if($state_list_view->state_id == $_POST['state_id_filter']){
							  $sel = 'selected="selected"';
						      }
						      echo '<option value = "'.$state_list_view->state_id.'" '.$sel.'>'.$state_list_view->state_name.'</option>';
						   }
						}
					     ?>
				       </select>
				       <label>State</label>
				    </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="city_id_filter" id="city_id_filter" >
					  <?php
					  if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
					     $state_id = $_POST['state_id_filter'];
					     $city_id = '';
					     if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
						$city_id = $_POST['city_id_filter'];
					     }
					     $city_list = $this->sessionReport_model->city_list($state_id, $city_id);
					     echo $city_list;
					  }else{
					     echo '<option value="">All Cities</option>';
					  }
					  ?>
					  
				       </select>
				       <label>City</label>
				    </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="locationid" id="locationid">
					  <option value="">Select Location</option>
					  <?php
					    
					     $state_id = '';
					     if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
						 $state_id = $_POST['state_id_filter'];
					     }
					     $city_id = '';
					     if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
						$city_id = $_POST['city_id_filter'];
					     }
					     $get_locations = $this->sessionReport_model->wifi_location_list($state_id, $city_id);
					     foreach($get_locations->location_list as $get_brand1){
                                                $sel = '';
                                                if($get_brand1->id == $_POST['locationid']){
                                                    $sel = 'selected="selected"';
                                                }
                                                ?>
                                                <option value="<?php echo $get_brand1->id ?>" <?php echo $sel ?>><?php
                                                    echo $get_brand1->location_name; ?></option>

                                                <?php
                                            }
                                            ?>
					
					  
				       </select>
				       <label>Location</label>
				    </div>
				 </div>
			      </div>
			   
			</div>

</form>

	    
            

            <div class="col-lg-12 col-md-12 col-sm-12">
               <div class="table-responsive">
                  <table class="table table-striped">
                     <thead>
                        <tr class="active">
                           <th colspan="2" class="text-center">SERVER SESSION RECORD</th>
                        </tr>
                     </thead>
                     <tbody>
			<?php
                                $total_windows_phone_session1 = 0;
                                $total_laptop_session1 = 0;
                                $total_android_session1 = 0;
                                $total_ios_session1 = 0;
                                $total_session = 0;
                                foreach($wifi_session_data as $data1){
                                    $total_windows_phone_session1=$total_windows_phone_session1+$data1->total_session_windows_phone;
                                    $total_laptop_session1 = $total_laptop_session1+$data1->total_session_laptop;
                                    $total_android_session1 = $total_android_session1+$data1->total_session_android;
                                    $total_ios_session1 = $total_ios_session1+$data1->total_session_iphone;
                                }
                                $total_session = $total_windows_phone_session1+$total_laptop_session1+$total_android_session1+$total_ios_session1;
				echo '<tr>
                           <td><strong>Total Session</strong></td>
                           <td class="text-center"><strong>'.$total_session.'</strong></td>
                        </tr>
			<tr>
                           <td><strong>Total Online Users</strong></td>
                           <td class="text-center"><strong>'.$wifi_total_online_user.'</strong></td>
                        </tr>
			';
                                                ?>
                        
                      
                        <tr>
                           <td colspan="2" class="text-center">
                              <strong style="font-weight:800">Location Session Record</strong>
                           </td>
                        </tr>
                        <tr style="padding:5px 0px">
                           <td colspan="2"  style="padding:5px 0px">
                              <table class="table table-bordered table-striped">
                                 <tr>
                                    <th rowspan="2">
                                       <center>Date/Location</center>
                                    </th>
                                    <th colspan="2">
                                       <center>Captive Portal Impression</center>
                                    </th>
                                    <th colspan="5">
                                       <center>Session</center>
                                    </th>
                                    <th colspan="5">
                                       <center>Users</center>
                                    </th>
                                 </tr>
                                 <tr>
                                    <th>Total</th>
				    <th>Unique Impression</th>
                                    <th>Unique</th>
                                    
                                    <th>Laptop</th>
                                    <th>Android</th>
                                    <th>IOS</th>
				    <th>Windows Phone</th>
                                    <th>Total</th>
                                    
                                    <th>Laptop</th>
                                    <th>Android</th>
                                    <th>IOS</th>
				    <th>Windows Phone</th>
                                    <th>Total</th>
                                 </tr>
				 <?php
                                            $total_windows_phone_session = 0;
                                            $total_windows_phone_user = 0;
                                            $total_laptop_session = 0;
                                            $total_laptop_user = 0;
                                            $total_android_session = 0;
                                            $total_android_user = 0;
                                            $total_ios_session = 0;
                                            $total_ios_user = 0;
                                            $total_cp_impression = 0;
					    $unique_cp_impression = 0;
                                            $total_unique_impression = 0;
                                            foreach($wifi_session_data as $data){
                                                $total_windows_phone_session = $total_windows_phone_session+$data->total_session_windows_phone;
                                                $total_windows_phone_user = $total_windows_phone_user+$data->total_user_windows_phone;
                                                $total_laptop_session = $total_laptop_session+$data->total_session_laptop;
                                                $total_laptop_user = $total_laptop_user+$data->total_user_laptop;
                                                $total_android_session = $total_android_session+$data->total_session_android;
                                                $total_android_user = $total_android_user+$data->total_user_android;
                                                $total_ios_session = $total_ios_session+$data->total_session_iphone;
                                                $total_ios_user = $total_ios_user+$data->total_user_iphone;
                                                $total_cp_impression = $total_cp_impression+$data->total_impression;
						$unique_cp_impression = $unique_cp_impression+$data->unique_impression;
                                                $total_unique_impression = $total_unique_impression+$data->total_unique_impression;
                                            ?>
					    <tr>
                                    <td>
                                       <?php echo date("d-m-Y", strtotime($data->theDate)) ?>
                                    </td>
                                    <td><?php echo $data->total_impression ?></td>
				    <td><?php echo $data->unique_impression ?></td>
                                    <td><?php echo $data->total_unique_impression ?></td>
                                    
                                    <td><?php echo $data->total_session_laptop ?></td>
                                    <td><?php echo $data->total_session_android ?></td>
                                    <td><?php echo $data->total_session_iphone ?></td>
				    <td><?php echo $data->total_session_windows_phone ?></td>
                                    <th>
                                       <?php echo($data->total_session_windows_phone+$data->total_session_laptop +$data->total_session_android+$data->total_session_iphone) ?>                                                    
                                    </th>
                                    
                                    <td>
                                        
                                                            <?php echo $data->total_user_laptop?>
                                                        
                                    </td>
                                    <td>
                                      <?php echo $data->total_user_android?>
                                                       
                                    </td>
				    <td>
                                     <?php echo $data->total_user_iphone?>
                                                        
                                    </td>
				    <td>
                                      <?php echo $data->total_user_windows_phone?>
                                                        
                                    </td>
                                    <td>
                                       <?php echo($data->total_user_windows_phone+$data->total_user_laptop +$data->total_user_android+$data->total_user_iphone) ?>
                                    </td>
                                 </tr>
					    <?php }?>
                                 
                                 <tr>
                                                <th>Total</th>
                                                <th>
                                                    <?php
                                                    echo $total_cp_impression;
						    ?>
                                                </th>
						<th>
                                                    <?php
                                                    echo $unique_cp_impression;
						    ?>
                                                </th>
                                                <th>
                                                    <?php
                                                    echo $total_unique_impression;
						    ?>
                                                </th>
                                                
                                                <th><?php echo $total_laptop_session ?></th>
                                                <th><?php echo $total_android_session ?></th>
                                                <th><?php echo $total_ios_session ?></th>
						<th>
                                                    <?php
                                                    echo $total_windows_phone_session ;
						    ?>
                                                </th>
                                                <th>
                                                    <?php echo($total_windows_phone_session+$total_laptop_session+$total_android_session+$total_ios_session)?>
                                                </th>
                                                
                                                <th><?php echo $total_laptop_user ?></th>
                                                <th><?php echo $total_android_user ?></th>
                                                <th><?php echo $total_ios_user ?></th>
						<th>
                                                    <?php
                                                    echo $total_windows_phone_user;
                                                    ?>
                                                </th>
                                                <th>
                                                    <?php echo($total_windows_phone_user+$total_laptop_user+$total_android_user+$total_ios_user)?>
                                                </th>
                                            </tr>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


            </div>
         </div>
      </div>
      <!-- /Start your project here-->
      <!-- div for view bssid-->
<div id="info_model" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top:4%; width:712px;height:400px!important; ">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center><h3 class="modal-title">User List</h3></center>
            </div>
            <div class="modal-body"  style="width:710px;height:400px!important; overflow-y:scroll; padding-bottom:10px;
         " >

                <center id="info_data">
                </center>


            </div>

        </div>

    </div>
</div>
      <script>
         $(document).ready(function() {
	     $(".loading").hide();
           var height = $(window).height();
            $('#main_div').css('height', height);
          var herader_height = $("#header").height();
         height = height - herader_height;
           $('#right-container-fluid').css({'height':(windowHeight)+'px'});
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
         $('#advance_filters').click(function() {
         //alert("Hello! I am an alert box!!");
              $('.advance_filters_div').slideToggle("400");
         });
         // search box dropdown // 
         $('#search_input').keyup(function(){  
         $('#search_filter_areas').show();
         });
         $('body').click(function(e) {
         if(e.target.id != 'search_input'){
         $('#search_filter_areas').hide();
         }
         
         });
         });
         
         $(document).click(function(){
         
         $("#left_menu").toggleClass('closed');
         $("#left_menu2").toggleClass('open'); 
         });
      </script>
		<script>
	  $(function() {
		$('#toggle-two').bootstrapToggle({
		  on: 'Enabled',
		  off: 'Disabled'
		});
	  })
	</script>
	<script type="text/javascript">
         $(document).ready(function(){
            $('.start_date').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       maxDate: moment()
	    }).on('change', function(e, date){
	       $('.end_date').bootstrapMaterialDatePicker('setMinDate', date);
	    });
      
	    $('.end_date').bootstrapMaterialDatePicker({
	       time: false,
	       clearButton: true,
	       weekStart: 0,
	       format: 'DD-MM-YYYY',
	       maxDate: moment()
	    }).on('change', function(e, date){
	       $('.start_date').bootstrapMaterialDatePicker('setMaxDate', date);
	      
	    });
            
            $.material.init()
         });
	 
	 function get_userlist_free_locationvise(visit_date,locationid,device_type){
        $('#info_data').html("Please Wait");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>sessionReport/wifi_first_free_session_user_locationvise",
            cache: false,
            data: "locationid="+locationid+"&visit_date="+visit_date+"&device_type="+device_type,
            success: function(data){
                //alert(data);
                $('#info_data').html(data);

            }
        });
    }
    
    function get_userlist_locationvise(via,visit_date, locationid){
        $('#info_data').html("Please Wait");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>sessionReport/wifi_session_user_locationvise",
            cache: false,
            data: "visit_date="+visit_date+"&locationid="+locationid+"&via="+via,
            success: function(data){
                //alert(data);
                $('#info_data').html(data);

            }
        });
    }
    
    //submit filter form
	 function submit_filter_form(){
	    $(".loading").show();
	    document.getElementById("location_session_filter_form").submit(); 
	 }
	 $("#locationid").change(function(){
	  submit_filter_form();
	  });
	 
	 
	  $('#state_id_filter').change(function(){
	       $('#city_id_filter').val('');
	       $('#locationid').val('');
	       submit_filter_form(); 
	    });
	    $('#city_id_filter').change(function(){
	     $('#locationid').val('');
	       submit_filter_form(); 
	    });
      </script>
</body>
</html>
