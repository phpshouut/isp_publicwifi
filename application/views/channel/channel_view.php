<?php
   $isp_id = ISPID;
   $api_path = '';
   $redirect_path = '';
   $channel_name = "CHANNEL";
   if($isp_id == '193'){
    $redirect_path = "https://ads360network.in/adschannel/index.html";
    $api_path = "https://ads360network.in/ads_channel_api/channel/login";
    $channel_name = "360 CHANNEL";
   }
   else{
    $redirect_path = HOMEWIFICHANNEL;
    $api_path = $this->api_url."login";
   }
 $this->load->view('includes/header');
 
 ?>
      <div class="loading" style="display: none" >
	 <img src="<?php echo base_url()?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                  
                <?php $this->load->view('includes/left_nav');?>
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">Channel</a>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="shouut_channel">
                                 <h2> <?php echo $channel_name?></h2>
                                 <h4>Reach out to the  Wi-Fi users with amazing rewards, deals and messages through <br/>the  Channel</h4>
                                 <div class="col-sm-12 col-xs-12" style="margin:25px 0px;">
                                    <center>
                                       <img src="<?php echo base_url()?>assets/images/channel_img.png" class="img-responsive"/>
                                    </center>
                                 </div>
                                 <div class="row">
				  <!--a href="<?php echo HOMEWIFICHANNEL?>index.html#/dashboard" target="_blank">
				    <button class="mui-btn mui-btn--accent">LAUNCH SHOUUT CHANNEL</button>
				  </a-->
                                    <a href="#" onclick="launch_channel()" >
				    <button class="mui-btn mui-btn--accent">LAUNCH <?php echo $channel_name?></button>
				  </a>
				    <a id="deepak"></a>
                                 </div>
                                 <div class="row"><span> Channel will open in a new tab</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
       <script>
         $(document).ready(function() {
           var height = $(window).height();
            $('#main_div').css('height', height);
          var herader_height = $("#header").height();
         height = height - herader_height;
         //alert(height);
           $('#right-container-fluid').css('height', height);
          
         });
	 function launch_channel() {
	  var username = '<?php echo $this->session->userdata['isp_session']['username'];?>';
	  var password = '<?php echo $this->session->userdata['isp_session']['password'];?>';
	  var isp_uid = '<?php echo $this->session->userdata['isp_session']['isp_uid'];?>';
	 $(".loading").show();
	    $.ajax({
			     type: "POST",
			     
			     url: "<?php echo $api_path?>",
			     dataType:"json",
		       async: false,
			      data:JSON.stringify({
				email: username,
				password: password,
				isp_uid: isp_uid
				/*email: 'ahmad@selectcitywalk.com',
				password: '123',
				isp_uid: isp_uid,
				location_uid: '12196'*/
			      }),
			      contentType: "application/x-www-form-urlencoded",
			     success: function(data){
			      $(".loading").hide();
			      /*$("#deepak").attr("target", "_blank");
			      $("#deepak").attr("href", '<?php echo HOMEWIFICHANNEL?>#/dashboard');
			     $('#deepak')[0].click();*/ 
				window.open('<?php echo $redirect_path?>#/dashboard', '_newtab');
			       
			     }
			});
	 }
      </script>
    
      
     
   </body>
</html>

