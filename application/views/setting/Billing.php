<?php $this->load->view('includes/header');?>
<!-- Start your project here-->
<div class="container-fluid">
    <div class="loading hide" >
	   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
               
                <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('includes/left_nav',$data); ?>

            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">BILLING CYCLE</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                 <?php if($this->plan_model->is_permission(BILLINGCYCLE,'ADD')){ 
                                     if(count($billing_cycle)==0){
                                     ?>   
                                <li>
                                    <a href="#">
                                        <button class="mui-btn mui-btn--small mui-btn--accent"
                                                onclick="add_billing_cycle()">
                                            + ADD BILLING CYCLE
                                        </button>
                                    </a>
                                </li>
                                 <?php
                                     }
                                     
                                     }
				      $this->view('includes/global_setting',$data);
				     ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="add_user">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="mui--appbar-height"></div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="row">
                                        <h2>Define Billing Dates</h2>
                                        <div class="form-group">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped">
                                                            <thead>
                                                            <tr class="active">
                                                                <th>&nbsp;</th>
                                                                <th>BILLING DATE</th>
                                                                <th class="mui--text-center">ACTIONS</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                              $is_editable=$this->plan_model->is_permission(BILLINGCYCLE,'EDIT');
                                                              
                                                              $disable=(!$is_editable)?"disabled":"";
                                                            foreach($billing_cycle as $billing_cycle1){
                                                                
                                                                ?>
                                                                <tr>
                                                                <td>Billing Cycle Date</td>
                                                                <td><?php echo $billing_cycle1->billing_cycle?></td>
                                                            <td class="mui--text-center">
                                                                    <a href="#"
                                                                    onclick="edit_billing_cycle('<?php echo
                                                                    $billing_cycle1->id?>','<?php echo
                                                                    $billing_cycle1->billing_cycle?>')
                                                                  ">Edit</a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            }
                                                            ?>


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                         <!--Div for tax update-->
                                                <div class="row">
                                                    <form action="" method="post" id="add_tax" autocomplete="off" onsubmit="add_tax(); return false;">
                                                         
                                                        <h2>Define Tax Rate</h2>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                            <div class="row">
                                                                
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="number" class="tax" name="tax" min="0" step="0.001" value="<?php echo isset($tax['tax'])?$tax['tax']:0;?>"required>
                                                                         <span class="title_box_left">
                                                            %
                                                            </span>
                                                                        <label> Tax<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                
                                                               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                                </div>
                                                            </div>
                                                        </div>

                                               
                                                    </form>
                                                </div>
                         
                         <!--Account Details-->
                         <div class="row"></div>
                         <div class="row"><h2>Setup Payment Gateways</h2></div>
                           <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                <ul class="mui-tabs__bar plan_mui-tabs__bar">
								  <li class="mui--is-active">
						      <a data-mui-toggle="tab" data-mui-controls="Top-Up-1">
							  Citrus
							  </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="Top-Up-2">
								  Paytm
							      </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="Top-Up-3">
								  Pay U
							      </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="Top-Up-4">
								   PaymentGateway 04
							      </a>
								  </li>
								</ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="mui--appbar-height"></div>
                          		<div class="mui-tabs__pane mui--is-active" id="Top-Up-1">
							  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                               <form action="" method="post" id="add_citrus" autocomplete="off" onsubmit="add_citrus(); return false;">
                                                            <div class="row">
                                                                
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="citruskey" name="citruskey"   value="<?php echo isset($minfo[0]->citrus_secret_key)?$minfo[0]->citrus_secret_key:'';?>" required>
                                                                   
                                                                        <label>Citrus Secret Key<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="citrusposturl" name="citrusposturl"  value="<?php echo isset($minfo[0]->citrus_post_url)?$minfo[0]->citrus_post_url:'';?>" required>
                                                                
                                                                        <label>Citrus Post Url<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="citrusvanurl" name="citrusvanurl" value="<?php echo isset($minfo[0]->citrus_vanity_url)?$minfo[0]->citrus_vanity_url:'';?>" required>
                                                                     
                                                                        <label>Citrus Vanity Url<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                             
                                                            </div>
                                                              <div class="row">  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                                </div></div>
                                                               </form>
                                                        </div>
							    </div>
                              <div class="mui-tabs__pane" id="Top-Up-2">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                               <form action="" method="post" id="add_paytm" autocomplete="off" onsubmit="add_paytm(); return false;">
                                                            <div class="row">
                                                                
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="pmrchntkey" name="pmrchntkey"   value="<?php echo isset($minfo[0]->paytm_merchant_key)?$minfo[0]->paytm_merchant_key:'';?>" required>
                                                                   
                                                                        <label>Paytm Merchant Key<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="pmrchntmid" name="pmrchntmid"  value="<?php echo isset($minfo[0]->paytm_merchant_mid)?$minfo[0]->paytm_merchant_mid:'';?>" required>
                                                                
                                                                        <label>Paytm Merchant MID<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="pmrchntweb" name="pmrchntweb" value="<?php echo isset($minfo[0]->paytm_merchant_web)?$minfo[0]->paytm_merchant_web:'';?>" required>
                                                                     
                                                                        <label>Paytm Merchant Website<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                             
                                                            </div>
                                                              <div class="row">  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                                </div></div>
                                                               </form>
                                                        </div>
                              </div>
							   <div class="mui-tabs__pane" id="Top-Up-3">
							    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                        <form action="" method="post" id="add_payu" autocomplete="off" onsubmit="add_payu(); return false;">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
							    <div class="row">
								 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="user" name="payuuser"   value="<?php echo isset($minfo[0]->payu_user)?$minfo[0]->payu_user:'';?>" required>
                                                                   
                                                                        <label>PayU User<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="password" class="pwd" name="payupwd"  value="<?php echo isset($minfo[0]->payu_pwd)?$minfo[0]->payu_pwd:'';?>" required>
                                                                
                                                                        <label>PayU Password<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="merchantid" name="payumerchantid" value="<?php echo isset($minfo[0]->payu_merchantid)?$minfo[0]->payu_merchantid:'';?>" required>
                                                                     
                                                                        <label>Merchant ID<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                             
                                                            </div>
							    </div>
							     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
							    <div class="row">
								
                                                                
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="merchantkey" name="merchantkey"   value="<?php echo isset($minfo[0]->payu_merchantkey)?$minfo[0]->payu_merchantkey:'';?>" required>
                                                                   
                                                                        <label>Merchant Key<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="merchantsalt" name="merchantsalt"  value="<?php echo isset($minfo[0]->payu_merchantsalt)?$minfo[0]->payu_merchantsalt:'';?>" required>
                                                                
                                                                        <label>Merchant Salt<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="authheader" name="authheader" value="<?php echo isset($minfo[0]->payu_authheader)?$minfo[0]->payu_authheader:'';?>" required>
                                                                     
                                                                        <label>Authorization Header<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                             
                                                            </div>
							    </div>
							    
							    
                                                              <div class="row">  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                                </div></div>
                                                               </form>
                                                        </div>
							    
							   </div>
							   <div class="mui-tabs__pane" id="Top-Up-4">PaymentGateway 4</div>
							   </div>
                           </div>
                        </div>
                          <!--Account Details-->
			  
			  
			   <!--Sms Gateway Details-->
                        
			   
			   
			   
			   
                          <!--Sms gateway Details-->
			  
			  
			    <!--Sms Gateway Details-->
                         <div class="row"></div>
                         <div class="row"><h2>Email Setup</h2></div>
                           <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                <ul class="mui-tabs__bar plan_mui-tabs__bar">
								  <li class="mui--is-active">
						      <a data-mui-toggle="tab" data-mui-controls="EMAIL-Up-1">
							  Email
							  </a>
								  </li>
								
								</ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="mui--appbar-height"></div>
                          		<div class="mui-tabs__pane mui--is-active" id="EMAIL-Up-1">
							  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                               <form action="" method="post" id="add_email" autocomplete="off" onsubmit="add_email(); return false;">
                                                            <div class="row">
                                                                
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="smtphost" name="smtphost"   value="<?php echo isset($einfo[0]->host)?$einfo[0]->host:'';?>" required>
                                                                   
                                                                        <label>SMTP HOST<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="smtport" name="smtport"  value="<?php echo isset($einfo[0]->port)?$einfo[0]->port:'';?>" required>
                                                                
                                                                        <label>SMTP PORT<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               
                                                                
                                                                
                                                             
                                                            </div>
							    
							     <div class="row">
                                                                
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="smtpuser" name="smtpuser"   value="<?php echo isset($einfo[0]->user)?$einfo[0]->user:'';?>" required>
                                                                   
                                                                        <label>USER<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="smtpwd" name="smtpwd"  value="<?php echo isset($einfo[0]->password)?$einfo[0]->password:'';?>" required>
                                                                
                                                                        <label>PASSWORD<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               
                                                                
                                                                
                                                             
                                                            </div>
                                                              <div class="row">  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                                </div></div>
                                                               </form>
                                                        </div>
							    </div>
                              
                           </div>
                        </div>
                          <!--Email gateway Details-->
                         
                         
                         
                                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Start your project here-->


<!--- Close-one Modal --->
<div class="modal fade" id="Add_billing" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Add Billing Cycle</strong></h4>
            </div>
            <form method="post" action="<?php echo base_url()?>setting/add_billing_cycle">
                <div class="modal-body" style="padding-bottom:5px">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="mui-select">
                            <select name="billing_cycle" required>
                                <option value="">Select Date</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <label>Billing Date <sup>*</sup></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="submit" class="mui-btn  mui-btn--small mui-btn--accent" id="add_billing_cycle_button">
                        ADD CYCLE</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--- End Close-one Modal --->

<!--- Close-one Modal --->

<div class="modal fade" id="Edit_Add_billing" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Edit Billing Cycle</strong></h4>
            </div>
            <form method="post" action="<?php echo base_url()?>setting/update_billing_cycle">
                <div class="modal-body" style="padding-bottom:5px">

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <input type="hidden" name="billing_cycle_id" id="billing_cycle_id">
                        <div class="mui-select">
                            <select name="billing_cycle" <?php echo $disable;?> id="billing_cycle_time" required>
                                <option value="">Select Date</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <label>Billing Date <sup>*</sup></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="submit" <?php echo $disable;?> class="mui-btn  mui-btn--small mui-btn--accent" >
                        UPDATE CYCLE</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var height = $(window).height();
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);
    });
    function add_billing_cycle(){
        $("#Add_billing").modal("show");
        /*$.ajax({
            type: "POST",
            url: "<?php echo base_url()?>setting/miscellaneous_list",
            cache: false,
            success: function(data){
                $('#plan_type_add').html(data);
                //add_billing_cycle_button
                $("#Add_billing").modal("show");
                if($('#plan_type_add > option').length == '1'){
                    $("#add_billing_cycle_button").hide();
                }
            }

        });*/
    }
    function edit_billing_cycle(id, billing_cycle){
        $("#billing_cycle_id").val(id);
        $("#billing_cycle_time").val(billing_cycle);
        $("#Edit_Add_billing").modal("show");
    }
    
    
    function add_tax()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_tax").serialize();
         $.ajax({
        url: base_url+'setting/add_tax',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
     
       function add_citrus()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_citrus").serialize();
         $.ajax({
        url: base_url+'setting/add_citrus_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
           function add_gupshup()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_gupshup").serialize();
         $.ajax({
        url: base_url+'setting/add_gupshup_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
     function add_textmsg91() {
	 $('.loading').removeClass('hide');
         var formdata = $("#add_textmsg91").serialize();
         $.ajax({
        url: base_url+'setting/add_textmsg91_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
     }
     
      function add_otpmsg91() {
	 $('.loading').removeClass('hide');
         var formdata = $("#add_otpmsg91").serialize();
         $.ajax({
        url: base_url+'setting/add_otpmsg91_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
     }
     
     
     
          function add_email()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_email").serialize();
         $.ajax({
        url: base_url+'setting/add_email_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
     
      function add_payu()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_payu").serialize();
         $.ajax({
        url: base_url+'setting/add_payu_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
      function add_paytm()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_paytm").serialize();
         $.ajax({
        url: base_url+'setting/add_paytm_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
</script>
 <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         //setting_menu_ul
$(document).ready(function(){
//class="active" style="display: block;"
$("#setting_menu_ul").css("display","block");
$("#billing_menu").addClass("menu_active");

});
      </script>
</body>
</html>
