<?php $this->load->view('includes/header');?>
<!-- Start your project here-->
<div class="container-fluid">
    <div class="loading hide" >
	   <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
    <div class="row">
        <div class="wapper">
            <div id="sidedrawer" class="mui--no-user-select">
                 
                <?php
                  $data['navperm']=$this->plan_model->leftnav_permission();
                 
                 $this->view('includes/left_nav',$data); ?>

            </div>
            <header id="header">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">SMS GATEWAY</a>
                        </div>
                         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                              <?php $this->view('includes/global_setting',$data);  ?>
                            </ul>
                         </div>
                      
                    </div>
                </nav>
            </header>
            <div id="content-wrapper">
                <div class="mui--appbar-height"></div>
                <div class="mui-container-fluid" id="right-container-fluid">
                    <div class="add_user">
                        
                        
                         <!--Div for tax update-->
                         
                         <!--Account Details-->
                         
                        
                          <!--Account Details-->
			  
			  
			   <!--Sms Gateway Details-->
                         <div class="row"></div>
                         <div class="row"><h2>Sms Text Gateways</h2></div>
                           <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                <ul class="mui-tabs__bar plan_mui-tabs__bar">
								  <li class="mui--is-active">
						      <a data-mui-toggle="tab" data-mui-controls="SMS-Up-1">
							  GUPSHUP
							  </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="SMS-Up-2">
								  Msg91
							      </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="SMS-Up-3">
								  SMS3
							      </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="SMS-Up-4">
								   SMS4
							      </a>
								  </li>
								</ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="mui--appbar-height"></div>
                          		<div class="mui-tabs__pane mui--is-active" id="SMS-Up-1">
							  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                               <form action="" method="post" id="add_gupshup" autocomplete="off" onsubmit="add_gupshup(); return false;">
                                                            <div class="row">
                                                                
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="gupuser" name="gupuser"   value="<?php echo isset($sinfo[0]->gupshup_user)?$sinfo[0]->gupshup_user:'';?>" required>
                                                                   
                                                                        <label>Gupshup Username<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="guppwd" name="guppwd"  value="<?php echo isset($sinfo[0]->gupshup_pwd)?$sinfo[0]->gupshup_pwd:'';?>" required>
                                                                
                                                                        <label>Gupshup password<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                               
                                                                
                                                                
                                                             
                                                            </div>
                                                              <div class="row">  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                                </div></div>
                                                               </form>
                                                        </div>
							    </div>
                              <div class="mui-tabs__pane" id="SMS-Up-2">
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
				    <div class="row">
                                                                    <form action="" method="post" id="add_textmsg91" autocomplete="off" onsubmit="add_textmsg91(); return false;">
                                                                <div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="authkey" name="smsauthkey"   value="<?php echo isset($sinfo[0]->msg91authkey)?$sinfo[0]->msg91authkey:'';?>" required>
                                                                   
                                                                        <label>SMS Auth Key<sup>*</sup></label>
                                                                    </div>
								           </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="scope" name="scope"   value="<?php echo isset($sinfo[0]->scope)?$sinfo[0]->scope:'';?>" required>
                                                                   
                                                                        <label>Scope<sup>*</sup></label>
                                                                    </div>
								           </div>
                                                                </div>
								    <div class="row">  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                      </div></div>
								    
                                    </form>
				    </div>
				    </div>
                              </div>
							   <div class="mui-tabs__pane" id="SMS-Up-3">
							   SMS3
							    
							   </div>
							   <div class="mui-tabs__pane" id="SMS-Up-4">SMS 4</div>
							   </div>
                           </div>
                        </div>
			   
			   
			   <div class="row"></div>
                         <div class="row"><h2>Sms OTP Gateways</h2></div>
                           <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                <ul class="mui-tabs__bar plan_mui-tabs__bar">
								  <li class="mui--is-active">
						      <a data-mui-toggle="tab" data-mui-controls="SMSO-Up-1">
							  MSG91
							  </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="SMSO-Up-2">
								  SMS2
							      </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="SMSO-Up-3">
								  SMS3
							      </a>
								  </li>
								  <li>
								  <a data-mui-toggle="tab" data-mui-controls="SMSO-Up-4">
								   SMS4
							      </a>
								  </li>
								</ul>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="mui--appbar-height"></div>
                          		<div class="mui-tabs__pane mui--is-active" id="SMSO-Up-1">
							  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">
                                                               <form action="" method="post" id="add_otpmsg91" autocomplete="off" onsubmit="add_otpmsg91(); return false;">
                                                            <div class="row">
                                                                
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="authapikey" name="authapikey"   value="<?php echo isset($otpsinfo[0]->msg91_authkey)?$otpsinfo[0]->msg91_authkey:'';?>" required>
                                                                   
                                                                        <label>AUTH Api Key<sup>*</sup></label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="mui-textfield mui-textfield--float-label" style="padding-left:20px">
                                                                        <input type="text" class="scope" name="scope"   value="<?php echo isset($otpsinfo[0]->scope)?$otpsinfo[0]->scope:'';?>" required>
                                                                   
                                                                        <label>Scope<sup>*</sup></label>
                                                                    </div>
								           </div>
                                                               
                                                               
                                                                
                                                                
                                                             
                                                            </div>
                                                              <div class="row">  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block"  value="SAVE" >
                                                                </div></div>
                                                               </form>
                                                        </div>
							    </div>
                              <div class="mui-tabs__pane" id="SMSO-Up-2">
                                
                              </div>
							   <div class="mui-tabs__pane" id="SMSO-Up-3">
							   SMS3
							    
							   </div>
							   <div class="mui-tabs__pane" id="SMSO-Up-4">SMS 4</div>
							   </div>
                           </div>
                        </div>
			   
                          <!--Sms gateway Details-->
			  
			  
			    <!--Sms Gateway Details-->
                         
                        
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Start your project here-->


<!--- Close-one Modal --->
<!--- End Close-one Modal --->

<!--- Close-one Modal --->


<script>
    $(document).ready(function() {
        var height = $(window).height();
        $('#main_div').css('height', height);
        $('#right-container-fluid').css('height', height);
    });
    function add_billing_cycle(){
        $("#Add_billing").modal("show");
        /*$.ajax({
            type: "POST",
            url: "<?php echo base_url()?>setting/miscellaneous_list",
            cache: false,
            success: function(data){
                $('#plan_type_add').html(data);
                //add_billing_cycle_button
                $("#Add_billing").modal("show");
                if($('#plan_type_add > option').length == '1'){
                    $("#add_billing_cycle_button").hide();
                }
            }

        });*/
    }
    function edit_billing_cycle(id, billing_cycle){
        $("#billing_cycle_id").val(id);
        $("#billing_cycle_time").val(billing_cycle);
        $("#Edit_Add_billing").modal("show");
    }
    
    
    function add_tax()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_tax").serialize();
         $.ajax({
        url: base_url+'setting/add_tax',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
     
       function add_citrus()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_citrus").serialize();
         $.ajax({
        url: base_url+'setting/add_citrus_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
           function add_gupshup()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_gupshup").serialize();
         $.ajax({
        url: base_url+'setting/add_gupshup_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
     function add_textmsg91() {
	 $('.loading').removeClass('hide');
         var formdata = $("#add_textmsg91").serialize();
         $.ajax({
        url: base_url+'setting/add_textmsg91_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
     }
     
      function add_otpmsg91() {
	 $('.loading').removeClass('hide');
         var formdata = $("#add_otpmsg91").serialize();
         $.ajax({
        url: base_url+'setting/add_otpmsg91_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
     }
     
     
     
          function add_email()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_email").serialize();
         $.ajax({
        url: base_url+'setting/add_email_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
     
      function add_payu()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_payu").serialize();
         $.ajax({
        url: base_url+'setting/add_payu_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
     
      function add_paytm()
     {
          $('.loading').removeClass('hide');
         var formdata = $("#add_paytm").serialize();
         $.ajax({
        url: base_url+'setting/add_paytm_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
             $('.loading').addClass('hide');
        }
    });
         //alert(formdata);
     }
</script>
 <script>
         if ($(window)) {
          $(function () {
           $('.menu').crbnMenu({
            hideActive: true
           });
          });
         }
         //setting_menu_ul
$(document).ready(function(){
//class="active" style="display: block;"
$("#setting_menu_ul").css("display","block");
$("#billing_menu").addClass("menu_active");

});
      </script>
</body>
</html>
