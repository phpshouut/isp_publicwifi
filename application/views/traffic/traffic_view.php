 <?php
 $this->load->view('includes/header');
 
 ?>
  <style>
     
   #loadMore {
   padding: 5px 15px;
   text-align: center;
   color: #231f20;
    border-radius:25px; 
    margin: 25px auto;
    border: 1px solid #231f20;
    font-size: 12px;
    font-weight: 600;
    cursor: pointer;
   }
#loadMore:hover {
   background-color: #231f20;
   color: #fff;
    text-decoration: none;
}
   
  
 </style>
      <!-- Start your project here-->
      <div class="loading" >
	<img src="<?php echo base_url()?>assets/images/loader.svg"/>
     </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <div id="sidedrawer" class="mui--no-user-select">
                
                 
                  <?php
		   $data['navperm']=$this->plan_model->leftnav_permission();
		  $this->load->view('includes/left_nav');?>  
               </div>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">User</a>
                        </div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			  <?php $this->load->view('includes/global_setting',$data);?>
                        </ul>
                     </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="data_used">
			<form action="<?php echo base_url()?>traffic" method="post" id="filter_form">
                        <div class="row">
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                              <h1>User Report</h1>
                           </div>
                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                              <div class="col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="calendar_control">
                                       <span class="calendar_icons">
                                       <img src="<?php echo base_url()?>assets/images/calendar_icons.svg" class="img-responsive"/>
                                       </span>
                                       <span class="calendar_heading">DATES FROM & TO :</span> 
                                       <span>
                                       <input class="date_range" type="text"  id="daterange" name="daterange" <?php if(isset($_POST['daterange'])) echo "value='".$_POST['daterange']."'"?>/> 
                                       </span>
                                       <span class="angle-down">
                                       <i class="fa fa-angle-down" aria-hidden="true"></i>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                              <div class="data_bar_chart" id="myChart">
                                
                              </div>
                              <div class="col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-sm-4 col-xs-4">
                                       <span class="download-img">
                                       <img src="assets/images/user_icons.svg" 
                                          class="img-responsive"/>	
                                       </span>
                                       <span class="data_heading"><?php echo $total_user->total_user?></span>
                                       <span class="data_heading">
                                          <h5>TOTAL UNIQUE USERS</h5>
                                       </span>
                                    </div>
                                    <div class="col-sm-4 col-xs-4">
                                       <span class="green_square">	
                                       </span>
                                       <span class="data_heading"><?php echo $total_user->total_newuser?></span>
                                       <span>
                                          <h5>TOTAL NEW USERS</h5>
                                       </span>
                                    </div>
                                    <div class="col-sm-4 col-xs-4">
                                       <span class="red_square">	
                                       </span>
                                       <span class="data_heading"><?php echo $total_user->total_returninguser?></span>
                                       <span style="text-align:left">
                                          <h5>TOTAL RETURNING USERS</h5>
                                       </span>
                                    </div>
                                 </div>
                              </div>
			      
			      			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			      <div class="row">
				 
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="state_id_filter" id="state_id_filter" >
					  <option value="">All States</option>
					     <?php
						$state_list = $this->data_use_model->state_list();
						if($state_list->resultCode == 1){
						   foreach($state_list->state as $state_list_view){
						      $sel = '';
						      if($state_list_view->state_id == $_POST['state_id_filter']){
							  $sel = 'selected="selected"';
						      }
						      echo '<option value = "'.$state_list_view->state_id.'" '.$sel.'>'.$state_list_view->state_name.'</option>';
						   }
						}
					     ?>
				       </select>
				       <label>State</label>
				    </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       <select name="city_id_filter" id="city_id_filter" >
					  <?php
					  if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
					     $state_id = $_POST['state_id_filter'];
					     $city_id = '';
					     if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
						$city_id = $_POST['city_id_filter'];
					     }
					     $city_list = $this->data_use_model->city_list($state_id, $city_id);
					     echo $city_list;
					  }else{
					     echo '<option value="">All Cities</option>';
					  }
					  ?>
					  
				       </select>
				       <label>City</label>
				    </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				    <div class="mui-select">
				       
				       <a onclick="export_to_excel()" class="mui-btn mui-btn--small mui-btn--accent pull-right"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <small>Download as Excel</small></a>
				    </div>
				 </div>
			      </div>
			   
			</div>


                              <div class="col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="table-responsive">
                                       <table class="table table-striped">
                                          <thead>
                                             <tr class="active">
                                                <th></th>
                                                <th>LOCATION NAME</th>
                                                <th>IP</th>
                                                <th>TOTAL USERS</th>
                                                <th>NEW USERS</th>
                                                <th>RETURNING USERS</th>
                                             </tr>
                                          </thead>
                                          <tbody id="search_gridview">
					     
                                             
                                             
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
			      <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                 <div class="row" style="text-align:center">
                                    <input type='hidden' id="plimit" value="" />
                                    <input type='hidden' id="poffset" value="" />
				    <input type='hidden' id="ploadmoredata" value="1" />
                                    <div class="ploadmore_loader" style="display: none">
                                       <img src="<?php echo base_url() ?>assets/images/loader.svg" width="5%"/>
                                    </div>
				    <div  onclick="load_more_location()" style="display: none" class="load_more_button">
				       <span id="loadMore">Load More</span>
				    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                              <div class="col-sm-12 col-xs-12 data_col">
                                 <div class="col-sm-12 col-xs-12 nopadding">
                                    <h5>REFINE & FILTER BY:
                                       <small class="pull-right"><a href="<?php echo base_url()?>traffic">Reset All</a></small>
                                    </h5>
				    <h5>
				       <button type="submit" class="btn btn-raised btn-warning btn-sm">Apply</button>
				    </h5>
                                    <div class="form-group">
								 <label class="labels">Gender</label>
								 <div class="row">
								      <div class="col-sm-12 col-xs-12">
									   <div class="radio">
									      <label>
									      <input type="radio" name="gender" id="optionsRadios1" value=""  checked <?php echo (isset($_POST['gender']) && $_POST['gender'] == '')?"checked":"";?>>
									      &nbsp;&nbsp; All Genders
									      </label>
									   </div>
									   <div class="radio">
									      <label>
									      <input type="radio" name="gender" id="optionsRadios2" value="m" <?php echo (isset($_POST['gender']) && $_POST['gender'] == 'm')?"checked":"";?>>
									      &nbsp;&nbsp; Men
									      </label>
									   </div>
									   <div class="radio">
									      <label>
									      <input type="radio" name="gender" id="optionsRadios3" value="f" <?php echo (isset($_POST['gender']) && $_POST['gender'] == 'f')?"checked":"";?>>
									      &nbsp;&nbsp; Women
									      </label>
									   </div>
								      </div>
								 </div>
				    </div>
                                    <div class="form-group" style="margin:0px">
                                       <label class="labels">Age Group</label>
                                       <div class="checkbox">
                                          <label>
                                          <input type="checkbox" name="age_group[]" value="18" <?php echo (isset($_POST['age_group']) && in_array("18",$_POST['age_group']))?"checked":"";?>>
                                          &nbsp;&nbsp; Below 18
                                          </label>
                                       </div>
                                       <div class="checkbox">
                                          <label>
                                          <input type="checkbox" name="age_group[]" value="18-30" <?php echo (isset($_POST['age_group']) && in_array("18-30",$_POST['age_group']))?"checked":"";?>>
                                          &nbsp;&nbsp; 18 - 30 Yrs
                                          </label>
                                       </div>
                                       <div class="checkbox">
                                          <label>
                                          <input type="checkbox" name="age_group[]" value="31-45" <?php echo (isset($_POST['age_group']) && in_array("31-45",$_POST['age_group']))?"checked":"";?>>
                                          &nbsp;&nbsp; 31 - 45 Yrs
                                          </label>
                                       </div>
                                       <div class="checkbox">
                                          <label>
                                          <input type="checkbox" name="age_group[]" value="45" <?php echo (isset($_POST['age_group']) && in_array("45",$_POST['age_group']))?"checked":"";?>>
                                          &nbsp;&nbsp; 45 and Above
                                          </label>
                                       </div>
				       <div class="checkbox">
                                          <label>
                                          <input type="checkbox" name="age_group[]" value="" <?php echo (isset($_POST['age_group']) && in_array("",$_POST['age_group']))?"checked":"";?>>
                                          &nbsp;&nbsp; NA
                                          </label>
                                       </div>
                                    </div>
                                    <div class="form-group" style="margin:0px">
								 <label class="labels">Time of Day Use</label>
								 <div class="checkbox">
								      <label>
									   <input type="checkbox" value="00:00-06:00" name="time_slot[]" <?php echo (isset($_POST['time_slot']) && in_array("00:00-06:00",$_POST['time_slot']))?"checked":"";?>>
									   &nbsp;&nbsp; 00:00 AM - 06:00 AM
								      </label>
								 </div>
								 <div class="checkbox">
								    <label>
								    <input type="checkbox" value="06:01-11:59" name="time_slot[]" <?php echo (isset($_POST['time_slot']) && in_array("06:01-11:59",$_POST['time_slot']))?"checked":"";?>>
								    &nbsp;&nbsp; 06:01 AM - 11:59 AM
								    </label>
								 </div>
								 <div class="checkbox">
								    <label>
								    <input type="checkbox" value="12:00-18:00" name="time_slot[]" <?php echo (isset($_POST['time_slot']) && in_array("12:00-18:00",$_POST['time_slot']))?"checked":"";?>>
								    &nbsp;&nbsp; 12:00 PM - 06:00 PM
								    </label>
								 </div>
								 <div class="checkbox">
								    <label>
								    <input type="checkbox" value="18:01-23:59" name="time_slot[]" <?php echo (isset($_POST['time_slot']) && in_array("18:01-23:59",$_POST['time_slot']))?"checked":"";?>>
								    &nbsp;&nbsp; 06:01 PM - 11:59 PM
								    </label>
								 </div>
				    </div>
                                    <!--div class="form-group" style="margin:0px">
                                       <label class="labels">Device Details</label>
                                       <div class="checkbox">
                                          <label>
                                          <input type="checkbox" value="">
                                          &nbsp;&nbsp; Mobile & Tablet
                                          </label>
                                       </div>
                                       <div class="checkbox">
                                          <label>
                                          <input type="checkbox" value="">
                                          &nbsp;&nbsp; Laptop & Computers
                                          </label>
                                       </div>
                                    </div-->
                                 </div>
                              </div>
                           </div>
                        </div>
			</form>
		     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <link href="<?php echo base_url()?>assets/css/morris.css" rel="stylesheet">
<script src="<?php echo base_url()?>assets/js/morris.min.js"></script>
<script src="<?php echo base_url()?>assets/js/raphael-min.js"></script>
<script>
	//var graphDate = [{"viewed_on": "26 Oct", "total_mb" : "39"}, {"viewed_on": "27 Oct", "total_mb" : "339"}];
	var graphDate = <?php echo json_encode($total_user->data)?>;
	console.log(graphDate);
	!function ($) {
		"use strict";
		var Dashboard1 = function () {
			this.$realData = []
		};
		//creates Stacked chart
		Dashboard1.prototype.createStackedChart = function (element, data, xkey, ykeys, labels, lineColors) {
			Morris.Bar({
				element: element,
				data: data,
				xkey: xkey,
				ykeys: ykeys,
				stacked: true,
				labels: labels,
				hideHover: 'auto',
				resize: true, //defaulted to true
				gridLineColor: '#eeeeee',
				barColors: lineColors
			});
		},
			Dashboard1.prototype.init = function () {
				//creating Stacked chart
				var $stckedData = graphDate;
				this.createStackedChart('myChart', $stckedData, 'viewed_on', ['newuser', 'returninguser'], ['New ' +
				'Users', 'Returning Users'], ['#2bb673', '#f4474a']);
			},
			//init
			$.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
	}(window.jQuery),
		//initializing
		function ($) {
			"use strict";
			$.Dashboard1.init();
		}(window.jQuery);


	
</script>
         <script>
         $(document).ready(function() {
	    $(".loading").hide();
           var height = $(window).height();
            $('#main_div').css('height', height);
          var herader_height = $("#header").height();
         height = height - herader_height;
         //alert(height);
           $('#right-container-fluid').css('height', ('auto'));
          
         });
      </script>
     
      <script type="text/javascript">
         	$(document).ready(function(){
		$('input[name="daterange"]').daterangepicker({
			locale: {
				format: 'DD.MM.YYYY'
			},
		}).on('change', function(e){
			$('#loading').show();
			$('#filter_form').submit();
		});


	});
      </script>
           
       <script>
         var options = [];
         
         $( '.dropdown-menu a' ).on( 'click', function( event ) {
         
          var $target = $( event.currentTarget ),
           val = $target.attr( 'data-value' ),
           $inp = $target.find( 'input' ),
           idx;
         
          if ( ( idx = options.indexOf( val ) ) > -1 ) {
          options.splice( idx, 1 );
          setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
          } else {
          options.push( val );
          setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
          }
         
          //$( event.target ).blur();
         
          console.log( options );
          return false;
         });
      </script>
	   <script>
	    $('#langOpt').multiselect({
		  columns: 1,
		  placeholder: 'All Locations',
		  selectAll: true
		 });
	    
	    
	    
	    $(document).ready(function() {
	    var search_results = '<?php echo $location_wise_total_user['search_results']; ?>' ;
	    var paylimit = "<?php echo $location_wise_total_user['limit'] ?>";
	    var payoffset = "<?php echo $location_wise_total_user['offset'] ?>";
	    var payloadmore = "<?php echo $location_wise_total_user['loadmore'] ?>";
	    var total_record = "<?php echo $location_wise_total_user['total_record'] ?>";
	    $('#search_gridview').html(search_results);
	    $('#plimit').val(paylimit);
	    $('#poffset').val(payoffset);
	    $('#ploadmoredata').val(payloadmore);
	    $('.ploadmore_loader').hide();
	    if (parseInt(total_record) >= parseInt(paylimit)) {
	       $(".load_more_button").show();
	    }
	 
	 });
	 
      function load_more_location(){
	 $(".load_more_button").hide();
	 var formdata = '';
	 var state_id_filter = $("#state_id_filter").val();
	 var city_id_filter = $("#city_id_filter").val();
	 var daterange = $("#daterange").val();
	 var location = '';
	 var gender =  $("input[name='gender']:checked").val();
	 var age_group = [];
	 $.each($("input[name='age_group[]']:checked"), function(){            

            age_group.push($(this).val());

         });
	 var time_slot = [];
	 $.each($("input[name='time_slot[]']:checked"), function(){            

            time_slot.push($(this).val());

         });
	 var limit = $('#plimit').val();
	 var offset = $('#poffset').val();
	 var loaded = $('#ploadmoredata').val();
	 if (loaded != '0') {
	    $('.ploadmore_loader').show();
		     $.ajax({
			url: base_url+'traffic/user_list_more',
			type: 'POST',
			dataType: 'json',
			data: ({'limit':limit,'offset':offset,'state_id_filter':state_id_filter,'city_id_filter':city_id_filter,'daterange': daterange, 'gender':gender,'age_group':age_group,'time_slot':time_slot}),
			success: function(data){
			   $('#search_gridview').append(data.search_results);
			   $('#plimit').val(data.limit);
			   $('#poffset').val(data.offset);
			   $('#ploadmoredata').val(data.loadmore);
			   $('.ploadmore_loader').hide();
			   if (data.loadmore != '0') {
			      $(".load_more_button").show();
			   }
			   
			}
		     });
	 }
      }
      
      // on filter change
	    $('#state_id_filter').change(function(){
	       $('#city_id_filter').val('');
	       submit_filter_form(); 
	    });
	    $('#city_id_filter').change(function(){
	       $('#online_filter').val('');
	       submit_filter_form(); 
	    });
	    
	    //submit filter form
	 function submit_filter_form(){
	    $(".loading").show();
	    document.getElementById("filter_form").submit(); 
	 }
	 
	 
	function export_to_excel(){
	 var state_id_filter = $("#state_id_filter").val();
	 var city_id_filter = $("#city_id_filter").val();
	 var daterange = $("#daterange").val();
	 var location = '';
	 var gender =  $("input[name='gender']:checked").val();
	 var age_group = [];
	 $.each($("input[name='age_group[]']:checked"), function(){            

            age_group.push($(this).val());

         });
	 var time_slot = [];
	 $.each($("input[name='time_slot[]']:checked"), function(){            

            time_slot.push($(this).val());

         });
	 $('.loading').show();
		     $.ajax({
			url: base_url+'traffic/user_list_export_to_excel',
			type: 'POST',
			dataType: 'text',
			data: ({'state_id_filter':state_id_filter,'city_id_filter':city_id_filter,'daterange': daterange, 'gender':gender,'age_group':age_group,'time_slot':time_slot}),
			success: function(data){
			  $('.loading').hide();
	       window.location = "<?php echo base_url()?>traffic/download_user_list_excel";
			}
		     });
	 
      }
      

	   </script>
   </body>
</html>
