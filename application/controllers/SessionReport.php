<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SessionReport extends CI_Controller {
var $api_url;
var $isp_uid;
    public function __construct() {
        parent::__construct();
	if(!isset($this->session->userdata['isp_session']['isp_uid'])){
			redirect(base_url());
		}
	$this->load->model('permission_model');
	$this->load->model('sessionReport_model');
	$this->load->model('plan_model');
	$this->isp_uid = $this->session->userdata['isp_session']['isp_uid'];
	$this->api_url = APIPATH."publicwifi/";
    }

    public function index() {
	$locationid = '';
	if(isset($_POST['locationid']) && $_POST['locationid'] != ''){
            $locationid = $_POST['locationid'];
        }
	if(isset($_POST['start_date']) && $_POST['start_date'] != ''){
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        else{
            $_POST['start_date'] = date('d-m-Y', strtotime('-7 days'));
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        if(isset($_POST['end_date']) && $_POST['end_date'] != ''){
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date));
        }
        else{
            $_POST['end_date'] = date('d-m-Y');
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date) );
        }
	// if today button click
        if (isset($_POST['today'])) {
            $_POST['start_date'] = date('d-m-Y');
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if yesterday button click
        elseif(isset($_POST['yesterday'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-1 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y', strtotime('-1 days'));
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last week button click
        elseif(isset($_POST['lastweek'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-6 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 15 days button click
        elseif(isset($_POST['last15days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-14 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 30 days button click
        elseif(isset($_POST['last30days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-29 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = 20;
	$offset = 0;
	//echo $locationid;die;
	if($locationid != ''){
	    //$data_send["wifi_free_session_count_without_location"] = $this->sessionReport_model->wifi_free_session_count_without_location($start_date, $end_date);
            //$data_send["wifi_free_session_user_count_without_location"] = $this->sessionReport_model->wifi_free_session_user_count_without_location($start_date, $end_date);
            $session_report = $this->sessionReport_model->wifi_session_report_locationvise($start_date, $end_date, $locationid);
	    $data_send["wifi_session_data"] =  $session_report->session_data;
	    $data_send["wifi_total_online_user"] =  $session_report->total_online_user;
	    $this->load->view('sessionReport/sessionReport_location',$data_send);
	}else{
	    //$data_send["wifi_free_session_count_without_location"] = $this->sessionReport_model->wifi_free_session_count_without_location($start_date, $end_date);
	    //$data_send["wifi_free_session_user_count_without_location"] = $this->sessionReport_model->wifi_free_session_user_count_without_location($start_date, $end_date);
	    $session_report = $this->sessionReport_model->wifi_total_session($start_date, $end_date, $state_id, $city_id);
	    $data_send["wifi_total_session"] = $session_report->total_session;
	    $data_send["wifi_total_online_user"] = $session_report->total_online_user;
	    $data_send["total_cp_impression"] = $session_report->total_cp_impression;
	    $data_send["unique_cp_impression"] = $session_report->unique_cp_impression;
	    $data_send["total_unique_user"] = $session_report->total_unique_user;
	    $data_send["total_android_user"] = $session_report->total_android_user;
	    $data_send["total_iso_user"] = $session_report->total_iso_user;
	    $data_send["total_windwos_phone_user"] = $session_report->total_windwos_phone_user;
	    $data_send["total_laptop_user"] = $session_report->total_laptop_user;
            $data_send["wifi_session_data_lazy_loading"] = $this->sessionReport_model->wifi_session_report($start_date, $end_date, $state_id, $city_id, $limit, $offset);
	    $this->load->view('sessionReport/sessionReport',$data_send);    
	}
	
	
    }
    
    public function session_report_list_more(){
	$locationid = '';
	if(isset($_POST['locationid']) && $_POST['locationid'] != ''){
            $locationid = $_POST['locationid'];
        }
	if(isset($_POST['start_date']) && $_POST['start_date'] != ''){
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        else{
            $_POST['start_date'] = date('d-m-Y', strtotime('-7 days'));
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        if(isset($_POST['end_date']) && $_POST['end_date'] != ''){
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date));
        }
        else{
            $_POST['end_date'] = date('d-m-Y');
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date) );
        }
	// if today button click
        if (isset($_POST['today'])) {
            $_POST['start_date'] = date('d-m-Y');
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if yesterday button click
        elseif(isset($_POST['yesterday'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-1 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y', strtotime('-1 days'));
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last week button click
        elseif(isset($_POST['lastweek'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-6 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 15 days button click
        elseif(isset($_POST['last15days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-14 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 30 days button click
        elseif(isset($_POST['last30days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-29 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = '20';
		if(isset($_POST['limit']) && $_POST['limit'] != ''){
			$limit = $_POST['limit'];
		}
	$offset = '0';
	if(isset($_POST['offset']) && $_POST['offset'] != ''){
		$offset = $_POST['offset'];
	}
	
	
	$data = $this->sessionReport_model->wifi_session_report($start_date, $end_date, $state_id, $city_id, $limit, $offset);
		echo json_encode($data);
    }
    
    public function wifi_first_free_session_user(){
        $start_date = $this->input->post('start_date');
        if($start_date != ''){
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        $end_date = $this->input->post('end_date');
        if($end_date != ''){
            $end_date = date("Y-m-d", strtotime($end_date) );
        }
        $locationid = $this->input->post('locationid');
	$device_type = $this->input->post('device_type');
        $location_user = $this->sessionReport_model->wifi_first_free_session_user( $start_date, $end_date, $locationid, $device_type);
        echo $location_user;
    }
     public function wifi_session_user(){
        $locationid = $this->input->post('locationid');
        $start_date = $this->input->post('start_date');
        if($start_date != ''){
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        $end_date = $this->input->post('end_date');
        if($end_date != ''){
            $end_date = date("Y-m-d", strtotime($end_date) );
        }
        $via = $this->input->post('via');
        $location_user = $this->sessionReport_model->wifi_session_user($locationid, $start_date, $end_date, $via);
        echo $location_user;
    }
    public function wifi_first_free_session_user_locationvise(){
        $locationid = $this->input->post('locationid');
        $visit_date = $this->input->post('visit_date');
	$device_type = $this->input->post('device_type');
        $location_user = $this->sessionReport_model->wifi_first_free_session_user_locationvise( $locationid, $visit_date,$device_type);
        echo $location_user;
    }
     public function wifi_session_user_locationvise(){
        $locationid = $this->input->post('locationid');
        $via = $this->input->post('via');
        $visit_date = $this->input->post('visit_date');
        $location_user = $this->sessionReport_model->wifi_session_user_locationvise($locationid, $via, $visit_date);
        echo $location_user;
    }
    
    
    
     public function wifi_session_failure_report(){

        if(isset($_POST['start_date']) && $_POST['start_date'] != ''){
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        else{
            $_POST['start_date'] = date('d-m-Y', strtotime('-7 days'));
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        if(isset($_POST['end_date']) && $_POST['end_date'] != ''){
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date));
        }
        else{
            $_POST['end_date'] = date('d-m-Y');
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date) );
        }
// if today button click
        if (isset($_POST['today'])) {
            $_POST['start_date'] = date('d-m-Y');
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if yesterday button click
        elseif(isset($_POST['yesterday'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-1 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y', strtotime('-1 days'));
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last week button click
        elseif(isset($_POST['lastweek'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-6 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 15 days button click
        elseif(isset($_POST['last15days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-14 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 30 days button click
        elseif(isset($_POST['last30days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-29 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //get session detail from server
        $requestData = array(
            'startdate' => $start_date,
            'enddate'=> $end_date
        );
	
	
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = 20;
	$offset = 0;
    

             $data_send["wifi_failure_session_data"] = $this->sessionReport_model->wifi_session_failure_report($start_date, $end_date, $state_id, $city_id, $limit, $offset);
            $this->load->view('sessionReport/wifi_session_failure_report',$data_send);  


    }
    
    
    
    public function wifi_session_failure_report_load_more(){

        if(isset($_POST['start_date']) && $_POST['start_date'] != ''){
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        else{
            $_POST['start_date'] = date('d-m-Y', strtotime('-7 days'));
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        if(isset($_POST['end_date']) && $_POST['end_date'] != ''){
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date));
        }
        else{
            $_POST['end_date'] = date('d-m-Y');
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date) );
        }
// if today button click
        if (isset($_POST['today'])) {
            $_POST['start_date'] = date('d-m-Y');
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if yesterday button click
        elseif(isset($_POST['yesterday'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-1 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y', strtotime('-1 days'));
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last week button click
        elseif(isset($_POST['lastweek'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-6 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 15 days button click
        elseif(isset($_POST['last15days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-14 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 30 days button click
        elseif(isset($_POST['last30days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-29 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //get session detail from server
        $requestData = array(
            'startdate' => $start_date,
            'enddate'=> $end_date
        );
	
	
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = '20';
		if(isset($_POST['limit']) && $_POST['limit'] != ''){
			$limit = $_POST['limit'];
		}
	$offset = '0';
	if(isset($_POST['offset']) && $_POST['offset'] != ''){
		$offset = $_POST['offset'];
	}
    

        $data = $this->sessionReport_model->wifi_session_failure_report($start_date, $end_date, $state_id, $city_id, $limit, $offset);
        echo json_encode($data);

    }
    
    public function session_report_export_to_excel(){
	$locationid = '';
	if(isset($_POST['locationid']) && $_POST['locationid'] != ''){
            $locationid = $_POST['locationid'];
        }
	if(isset($_POST['start_date']) && $_POST['start_date'] != ''){
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        else{
            $_POST['start_date'] = date('d-m-Y', strtotime('-7 days'));
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        if(isset($_POST['end_date']) && $_POST['end_date'] != ''){
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date));
        }
        else{
            $_POST['end_date'] = date('d-m-Y');
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date) );
        }
	// if today button click
        if (isset($_POST['today'])) {
            $_POST['start_date'] = date('d-m-Y');
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if yesterday button click
        elseif(isset($_POST['yesterday'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-1 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y', strtotime('-1 days'));
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last week button click
        elseif(isset($_POST['lastweek'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-6 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 15 days button click
        elseif(isset($_POST['last15days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-14 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 30 days button click
        elseif(isset($_POST['last30days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-29 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	
	
	
	$data = $this->sessionReport_model->session_report_export_to_excel($start_date, $end_date, $state_id, $city_id);
	echo "done";
    }
    
    public function download_session_report_excel(){
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
        header('Content-Disposition: attachment;filename="assets/session_report.xlsx"');
        header('Cache-Control: max-age=0');
        ob_clean();
        flush();
        readfile('assets/session_report.xlsx'); exit;

    }
    
    
    
    public function export_to_excel_trafic_user_detail(){
	$locationid = '';
	if(isset($_POST['locationid']) && $_POST['locationid'] != ''){
            $locationid = $_POST['locationid'];
        }
	if(isset($_POST['start_date']) && $_POST['start_date'] != ''){
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        else{
            $_POST['start_date'] = date('d-m-Y', strtotime('-7 days'));
            $start_date = $_POST['start_date'];
            $start_date = date("Y-m-d", strtotime($start_date) );
        }
        if(isset($_POST['end_date']) && $_POST['end_date'] != ''){
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date));
        }
        else{
            $_POST['end_date'] = date('d-m-Y');
            $end_date = $_POST['end_date'];
            $end_date = date("Y-m-d", strtotime($end_date) );
        }
	// if today button click
        if (isset($_POST['today'])) {
            $_POST['start_date'] = date('d-m-Y');
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if yesterday button click
        elseif(isset($_POST['yesterday'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-1 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y', strtotime('-1 days'));
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last week button click
        elseif(isset($_POST['lastweek'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-6 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 15 days button click
        elseif(isset($_POST['last15days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-14 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
        //if last 30 days button click
        elseif(isset($_POST['last30days'])) {
            $_POST['start_date'] = date('d-m-Y', strtotime('-29 days'));
            $start_date = date("Y-m-d", strtotime($_POST['start_date']) );
            $_POST['end_date'] = date('d-m-Y');
            $end_date = date("Y-m-d", strtotime($_POST['end_date']) );
        }
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	
	
	
	$data = $this->sessionReport_model->export_to_excel_trafic_user_detail($start_date, $end_date, $state_id, $city_id);
	echo "done";
    }
    
    public function download_trafic_user_detail(){
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
        header('Content-Disposition: attachment;filename="assets/traffic_user_list.xlsx"');
        header('Cache-Control: max-age=0');
        ob_clean();
        flush();
        readfile('assets/traffic_user_list.xlsx'); exit;

    }
    
}

?>
