<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Data_use extends CI_Controller {
var $api_url;
var $isp_uid;
    public function __construct() {
        parent::__construct();
	if(!isset($this->session->userdata['isp_session']['isp_uid'])){
			redirect(base_url());
		}
        $this->load->model('data_use_model');
	$this->load->model('permission_model');
	$this->load->model('plan_model');
	$this->isp_uid = $this->session->userdata['isp_session']['isp_uid'];
	$this->api_url = APIPATH."publicwifi/";
    }

    public function index() {
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
	}
	else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
	}
	$locations = array();
	if(isset($_POST['location'])){
			$locations = $_POST['location'];
	}
	$gender = '';
	if(isset($_POST['gender'])){
	    $gender = $_POST['gender'];
	}
	$age_group = array();
	if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
	}
	$time_slot = array();
	if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
	}
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = 10;
	$offset = 0;
	$data["total_mb"] = $this->data_use_model->total_mb($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id);
	$data["location_list_data"] = $this->data_use_model->location_wise_mb($from, $to, $gender, $age_group,
			$time_slot, $locations, $limit, $offset,$state_id,$city_id);
	
	//echo "<pre>";print_r($data);die;
	$this->load->view('data_use/data_use',$data);
    }
    public function location_list_more(){
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
	}
	else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
	}
	$locations = array();
	if(isset($_POST['location'])){
			$locations = $_POST['location'];
	}
	$gender = '';
	if(isset($_POST['gender'])){
	    $gender = $_POST['gender'];
	}
	$age_group = array();
	if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
	}
	$time_slot = array();
	if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
	}
	$limit = '20';
		if(isset($_POST['limit']) && $_POST['limit'] != ''){
			$limit = $_POST['limit'];
		}
	$offset = '0';
	if(isset($_POST['offset']) && $_POST['offset'] != ''){
		$offset = $_POST['offset'];
	}
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	
	$data = $this->data_use_model->location_wise_mb($from, $to, $gender, $age_group,
			$time_slot, $locations, $limit, $offset,$state_id,$city_id);
		echo json_encode($data);
    }
    
    public function data_use_export_to_excel(){
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
	}
	else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
	}
	$locations = array();
	if(isset($_POST['location'])){
			$locations = $_POST['location'];
	}
	$gender = '';
	if(isset($_POST['gender'])){
	    $gender = $_POST['gender'];
	}
	$age_group = array();
	if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
	}
	
	$time_slot = array();
	if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
	}
	
	$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	
	$data = $this->data_use_model->location_wise_mb_export_to_excel($from, $to, $gender, $age_group,
			$time_slot, $locations, $state_id,$city_id);
	echo "done";
    }
    public function download_data_use_excel(){
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
        header('Content-Disposition: attachment;filename="assets/data_report.xlsx"');
        header('Cache-Control: max-age=0');
        ob_clean();
        flush();
        readfile('assets/data_report.xlsx'); exit;

    }
    
    
}

?>
