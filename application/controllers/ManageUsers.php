<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL);
class ManageUsers extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('ManageUsers_model', 'superusers_model');
        $this->load->model('permission_model');
        $this->load->model('plan_model');
        $permission = $this->permission_model->isp_portal_permission();
        if($permission['isp_publicwifi_permission'] == '0'){
            $this->session->unset_userdata('isp_session');
            $url = base_url();
            $url = str_replace("/publicwifi/","",$url);
            redirect($url);
        }
        if(!isset($this->session->userdata['isp_session']['isp_uid'])){
            redirect(base_url());
        }
        $this->load->library('form_validation');
    }
    
    public function index(){
        $data['list_data'] = $this->superusers_model->list_team_data();
        $this->load->view('superusers/users', $data);
    }
    
    public function add_users(){
        $this->load->view('superusers/add_users');
    }
    
    public function add_team_data_todb(){
        $postdata=$this->input->post();
        // echo "<pre>"; print_R($postdata); die;
        if(!isset($postdata['user_id'])){ 
            $this->form_validation->set_rules('username', 'User Name', 'required|valid_email|callback_username_check');
        } 
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('phone', 'phone no', 'trim|required|min_length[10]|max_length[10]|numeric');
        // $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if($this->form_validation->run() == FALSE) {
            $this->load->view("superusers/add_users");
        }else {
            $this->superusers_model->add_team_data_todb();
            redirect(base_url()."manageUsers");
        }
        
    }
    public function username_check($str) {
        $userexist = $this->superusers_model->check_user_exist($str);
        if ($userexist == 0) {
            $this->form_validation->set_message('username_check', 'This {field}  already exist');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function edit_users($id=''){
        if($id != ''){
            $data = $this->superusers_model->team_data_foredit($id);
            $data['edit_userid'] = $id;
            $this->load->view('superusers/edit_users', $data);
        }
    }
    
    public function delete_user(){
        $id=$this->superusers_model->delete_user();
        echo $id;
    }
    
    public function update_userpwd(){
	$data=$this->superusers_model->update_userpwd();
        echo json_encode($data);
    }

    /**********************************************
     *	MANAGE REGIONAL USERS
     *********************************************/
    
    public function add_regionalusers(){
        $this->load->view('superusers/add_regionalusers');
    }
    public function add_regional_data_todb(){
        $postdata=$this->input->post();
        //echo "<pre>"; print_R($postdata); die;
        if(!isset($postdata['user_id'])){ 
            $this->form_validation->set_rules('username', 'User Name', 'required|valid_email|callback_regionalusername_check');
        } 
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('regional_state[]', 'State', 'required');
        if($this->form_validation->run() == FALSE) {
            $this->load->view("superusers/add_regionalusers");
        }else {
            $this->superusers_model->add_regional_data_todb();
            redirect(base_url()."manageUsers");
        }
        
    }
    
    public function regionalusername_check($str) {
	$phone = $this->input->post('phone');
        $userexist = $this->superusers_model->check_user_exist($str);
        if ($userexist == 0) {
            $this->form_validation->set_message('regionalusername_check', 'This {field}  already exist');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function regionalteam_data(){
	$data = $this->superusers_model->regionalteam_data();
	echo json_encode($data);
    }
    public function edit_regionalusers($id=''){
        if($id != ''){
            $data = $this->superusers_model->regionalteam_data_foredit($id);
            $data['edit_userid'] = $id;
            $this->load->view('superusers/edit_regionalusers', $data);
        }
    }
    
    public function getallstatelist(){
	$get_allstates = $this->superusers_model->get_allstates();
	echo $get_allstates;
    }
}

?>