<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Traffic extends CI_Controller {
var $api_url;
var $isp_uid;
    public function __construct() {
        parent::__construct();
	if(!isset($this->session->userdata['isp_session']['isp_uid'])){
			redirect(base_url());
		}
        $this->load->model('data_use_model');
	$this->load->model('permission_model');
	$this->load->model('traffic_model');
	$this->load->model('plan_model');
	$this->isp_uid = $this->session->userdata['isp_session']['isp_uid'];
	$this->api_url = APIPATH."publicwifi/";
    }

    public function index() {
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
		}
		else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
		}

		$locations = array();
		if(isset($_POST['location'])){
			$locations = $_POST['location'];
		}
		$gender = '';
		if(isset($_POST['gender'])){
			$gender = $_POST['gender'];
		}
		$age_group = array();
		if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
		}
		$time_slot = array();
		if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = 10;
	$offset = 0;
		$data["total_user"] = $this->traffic_model->total_user($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id);
		$data["location_wise_total_user"] = $this->traffic_model->location_wise_total_user($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id, $limit, $offset);
		$this->load->view('traffic/traffic_view',$data);
    }
    public function user_list_more(){
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
		}
		else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
		}

		$locations = array();
		if(isset($_POST['location'])){
			$locations = $_POST['location'];
		}
		$gender = '';
		if(isset($_POST['gender'])){
			$gender = $_POST['gender'];
		}
		$age_group = array();
		if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
		}
		$time_slot = array();
		if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = '10';
		if(isset($_POST['limit']) && $_POST['limit'] != ''){
			$limit = $_POST['limit'];
		}
	$offset = '0';
	if(isset($_POST['offset']) && $_POST['offset'] != ''){
		$offset = $_POST['offset'];
	}
	
	
	$data = $this->traffic_model->location_wise_total_user($from, $to, $gender, $age_group,
			$time_slot, $locations, $state_id,$city_id,$limit, $offset);
		echo json_encode($data);
    }
    public function traffic_report() {
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
		}
		else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
		}

		$locations = array();
		if(isset($_POST['location'])){
			$locations = $_POST['location'];
		}
		$gender = '';
		if(isset($_POST['gender'])){
			$gender = $_POST['gender'];
		}
		$age_group = array();
		if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
		}
		$time_slot = array();
		if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = 10;
	$offset = 0;
		$data["total_user"] = $this->traffic_model->total_session($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id);
		$data["location_wise_total_user"] = $this->traffic_model->location_wise_total_session($from, $to, $gender, $age_group, $time_slot, $locations,$limit, $offset,$state_id,$city_id);
		$this->load->view('traffic/traffic_report_view',$data);
    }
    public function traffic_list_more(){
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
		}
		else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
		}

		$locations = array();
		if(isset($_POST['location'])){
			$locations = $_POST['location'];
		}
		$gender = '';
		if(isset($_POST['gender'])){
			$gender = $_POST['gender'];
		}
		$age_group = array();
		if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
		}
		$time_slot = array();
		if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = '10';
		if(isset($_POST['limit']) && $_POST['limit'] != ''){
			$limit = $_POST['limit'];
		}
	$offset = '0';
	if(isset($_POST['offset']) && $_POST['offset'] != ''){
		$offset = $_POST['offset'];
	}
	
	
	$data = $this->traffic_model->location_wise_total_session($from, $to, $gender, $age_group,
			$time_slot, $locations, $limit, $offset,$state_id,$city_id);
		echo json_encode($data);
    }
    
    
     public function user_list_export_to_excel(){
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
		}
		else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
		}

		$locations = array();
		if(isset($_POST['location'])){
			$locations = $_POST['location'];
		}
		$gender = '';
		if(isset($_POST['gender'])){
			$gender = $_POST['gender'];
		}
		$age_group = array();
		if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
		}
		$time_slot = array();
		if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	$limit = '10';
		if(isset($_POST['limit']) && $_POST['limit'] != ''){
			$limit = $_POST['limit'];
		}
	$offset = '0';
	if(isset($_POST['offset']) && $_POST['offset'] != ''){
		$offset = $_POST['offset'];
	}
	
	
	$data = $this->traffic_model->user_list_export_to_excel($from, $to, $gender, $age_group,
			$time_slot, $locations, $state_id,$city_id);
	echo "done";
    }
    public function download_user_list_excel(){
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
        header('Content-Disposition: attachment;filename="assets/user_list.xlsx"');
        header('Cache-Control: max-age=0');
        ob_clean();
        flush();
        readfile('assets/user_list.xlsx'); exit;

    }
    
    
      public function traffic_list_export_to_excel(){
	if(isset($_POST['daterange']) && $_POST['daterange']!= ''){
			$date_range = explode('-',$_POST['daterange']);
			$from = $date_range['0'];
			$to = $date_range['1'];
		}
		else{
			$from = date('d.m.Y',strtotime("-6 days"));
			$to = date('d.m.Y');
			$_POST['daterange'] = $from.' - '.$to;
		}

		$locations = array();
		if(isset($_POST['location'])){
			$locations = $_POST['location'];
		}
		$gender = '';
		if(isset($_POST['gender'])){
			$gender = $_POST['gender'];
		}
		$age_group = array();
		if(isset($_POST['age_group'])){
			$age_group = $_POST['age_group'];
		}
		$time_slot = array();
		if(isset($_POST['time_slot'])){
			$time_slot = $_POST['time_slot'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
	
	
	
	$data = $this->traffic_model->traffic_list_export_to_excel($from, $to, $gender, $age_group,
			$time_slot, $locations,$state_id,$city_id);
		echo json_encode($data);
    }

    
    

    public function download_traffic_list_excel(){
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
        header('Content-Disposition: attachment;filename="assets/traffic_list.xlsx"');
        header('Cache-Control: max-age=0');
        ob_clean();
        flush();
        readfile('assets/traffic_list.xlsx'); exit;

    }

    
    
}

?>
