<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nas extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->model('nas_model');
                $this->load->model('plan_model');
                 $this->load->model('permission_model');
                if(!$this->session->has_userdata('isp_session')){
			redirect(base_url().'login'); exit;
		}
    }
    
    
    public function nas_curlhit($url,$postdata,$ip)
    {
        $postData = array(
            'mobile' => $postdata['mobileno'],
            'clientip' => $ip,
            'shortname' => $postdata['nas_name'],
            'secret'=>$postdata['nas_secret']
            
        );
        $url=$url;
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
    }
    
    public function index()
    {
       $dataarr= $this->plan_model->get_ispdetail_info();
       $datastatearr=$this->state_list('');
        // echo "<pre>"; print_R($dataarr);die;
           $data['ispdetail']=$dataarr->isp_detail;
         $data['nas_listing']=$this->nas_model->listing_nas();
          $data['state_list'] = $datastatearr->state_list;
	   $data['nastype'] = $this->nas_model->nas_type();
	$this->load->view('nas/nas_view',$data);
    }
    
       public function viewall_search_results(){
            $this->nas_model->viewall_search_results();
	}
    
    public function add_nas_data()
    {
    $postdata=$this->input->post();
      if(isset($postdata['ip']) && $postdata['ip']!='' )
        {
           // $url="http://103.206.180.82/clientsconf/index.php";
             $nasip=$postdata['ip'];
           //  $this->nas_curlhit($url,$postdata,$nasip);
             $url=CLINTCONF;
               $this->nas_curlhit($url,$postdata,$nasip);
        }
      
        
 
       
        $id=$this->nas_model->add_nas_data();
        echo json_encode($id);
    }
    public function add_nas_data_dynamic(){
	$data =$this->nas_model->add_nas_data_dynamic();
	if(count($data) > 0){
	    $url=CLINTCONF;
	    $postdata = array(
		"mobileno" => "9873834499",
		"nas_name" =>  $data->shortname,
		"nas_secret" =>  $data->secret,
			    );
	    $nasip = $data->nasip;
	    $id = $data->nasid;
	    $this->nas_curlhit($url,$postdata,$nasip);
	    echo json_encode($data);
	}else{
	  $data = array('resultCode' => '0') ;
	  echo json_encode($data);
	}
	
    }
    
       public function state_list($state_id='')
    {
        $data= $this->plan_model->state_list($state_id);
      return $data;
    }
    
 
        
      public function delete_nas()
      {
          $id=$this->nas_model->delete_nas();
          echo json_encode($id);
      }
      
      public function edit_nas()
      {
       
       $postdata=$this->input->post();
  
           $data=$this->nas_model->edit_nas(); 
           echo $data;
      }
      
        public function getzonelist() {
        $cityid = $this->input->post('cityid');
        echo $this->nas_model->zone_list($cityid);
    }
    
      public function add_zonedata()
        {
            $add_zone = $this->nas_model->add_zonedata();
           // echo $add_zone;
        }
        
        public function add_citydata(){
		$add_city = $this->nas_model->add_citydata();
		// echo $add_city;
	}
    
    public function checknas_exist()
    {
        $result=$this->nas_model->checknas_exist();
        echo json_encode($result);
    }
    
     public function checknasip_exist()
    {
       $result= $this->nas_model->checknasip_exist();
        echo json_encode($result);
    }
    
    public function status_nasip()
    {
        $postdata=$this->input->post();
        $up = $this->nas_model->ping($postdata['nasip']);
        echo $up;
    }
  
    
}

?>
