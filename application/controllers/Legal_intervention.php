<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Legal_intervention extends CI_Controller {
var $api_url;
var $isp_uid;
    public function __construct() {
        parent::__construct();
	if(!isset($this->session->userdata['isp_session']['isp_uid'])){
			redirect(base_url());
		}
        $this->load->model('legal_intervention_model');
	$this->load->model('permission_model');
	$this->load->model('plan_model');
	$this->isp_uid = $this->session->userdata['isp_session']['isp_uid'];
	$this->api_url = APIPATH."publicwifi/";
    }

    public function index() {
		$location_nasip = '';
		if(isset($_POST['location']) && $_POST['location'] != ''){
			$location_nasip = $_POST['location'];
		}
		elseif( $this->input->get('nasip')){
			$location_nasip = $this->input->get('nasip');
		}
		$mobile = '';
		if(isset($_POST['mobile']) && $_POST['mobile'] != ''){
			$mobile = $_POST['mobile'];
		}
		elseif( $this->input->get('mobile')){
			$mobile = $this->input->get('mobile');
		}
		$selecteddate = '';
		if(isset($_POST['selecteddate']) && $_POST['selecteddate'] != ''){
			$selecteddate = $_POST['selecteddate'];
		}
		else{
			if( $this->input->get('selecteddate')){
				$selecteddate = $this->input->get('selecteddate');
			}
			else{
				$selecteddate = date('d.m.Y H:i:s',strtotime("-2 days"));
				$_POST['selecteddate'] = $selecteddate;
			}

		}

		$selecteddateto = '';
		if(isset($_POST['selecteddateto']) && $_POST['selecteddateto'] != ''){
			$selecteddateto = $_POST['selecteddateto'];
		}
		else{
			if( $this->input->get('selecteddateto')){
				$selecteddateto = $this->input->get('selecteddateto');
			}
			else{
				$selecteddateto = date("d.m.Y H:i:s");
				$_POST['selecteddateto'] = $selecteddateto;
			}

		}
		$total_users_record = $this->legal_intervention_model->total_user_lazy_loading($mobile, $selecteddate,
		$selecteddateto,$location_nasip);
		$perPage = 50;
		$start = 0;
		/*$page = 1;
		if(!empty($_GET["page"])) {
			$page = $_GET["page"];
		}
		$pages  = $total_users_record/$perPage;
		$start = ($page-1)*$perPage;
		if($start < 0) $start = 0;
		$data["page"] = $page;
		$data["pages"] = $pages;*/
		$data["total_record"] = $total_users_record;
		$data["user_data"] = $this->legal_intervention_model->get_user($start, $perPage, $mobile, $selecteddate, $selecteddateto,
			$location_nasip);
		//echo "<pre>";print_r($data);die;
	$this->load->view('legal_intervention/legal_intervention_view',$data);
    }
    
    public function list_more() {
		$location_nasip = '';
		if(isset($_POST['location']) && $_POST['location'] != ''){
			$location_nasip = $_POST['location'];
		}
		
		$mobile = '';
		if(isset($_POST['mobile']) && $_POST['mobile'] != ''){
			$mobile = $_POST['mobile'];
		}
		
		$selecteddate = '';
		if(isset($_POST['selecteddate']) && $_POST['selecteddate'] != ''){
			$selecteddate = $_POST['selecteddate'];
		}
		else{
		    $selecteddate = date('d.m.Y H:i:s',strtotime("-2 days"));
		    $_POST['selecteddate'] = $selecteddate;
		}

		$selecteddateto = '';
		if(isset($_POST['selecteddateto']) && $_POST['selecteddateto'] != ''){
			$selecteddateto = $_POST['selecteddateto'];
		}
		else{
		    $selecteddateto = date("d.m.Y H:i:s");
		    $_POST['selecteddateto'] = $selecteddateto;

		}
		$perPage = '20';
		if(isset($_POST['limit']) && $_POST['limit'] != ''){
			$perPage = $_POST['limit'];
		}
		$start = '0';
		if(isset($_POST['offset']) && $_POST['offset'] != ''){
			$start = $_POST['offset'];
		}
		
		$data = $this->legal_intervention_model->get_user($start, $perPage, $mobile, $selecteddate, $selecteddateto,
			$location_nasip);
		//echo "<pre>";print_r($data);die;
	echo json_encode($data);
    }
    
    public function legal_export_excel(){
	$mobile = '';
	if(isset($_POST['mobile']) && $_POST['mobile'] != ''){
		$mobile = $_POST['mobile'];
	}
	$selecteddate = '';
	if(isset($_POST['date_start']) && $_POST['date_start'] != ''){
		$selecteddate = $_POST['date_start'];
	}
	$selecteddateto = '';
	if(isset($_POST['date_end']) && $_POST['date_end'] != ''){
		$selecteddateto = $_POST['date_end'];
	}
	$location_nasip = '';
	if(isset($_POST['location']) && $_POST['location'] != ''){
		$location_nasip = $_POST['location'];
	}
	$result = $this->legal_intervention_model->legal_export_excel($mobile, $selecteddate, $selecteddateto, $location_nasip);
	echo $result;
    }
	
    public function legal_download_excel(){
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
	    header('Content-Disposition: attachment;filename="assets/LegalIntervention.xlsx"');
	    header('Cache-Control: max-age=0');
	    ob_clean();
	    flush();
	    readfile('assets/LegalIntervention.xlsx'); exit;
    
    }
    
    
}

?>
