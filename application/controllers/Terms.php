<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		$this->load->model('login_model');
		$this->load->model('permission_model');
		
	}

	
	public function index(){
		$this->load->view('terms_view');
	}
	public function check_terms_condition_update(){
		$this->login_model->check_terms_condition_update();
		redirect(base_url().'location');
	}
	
	
	
}
