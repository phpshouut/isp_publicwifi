<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
          $this->load->library('session');
        $this->load->model('plan_model');
	$this->load->model('permission_model');
     //   $this->load->model('user_model');
     //   $this->load->model('setting_model');
       
        if (!$this->session->has_userdata('isp_session')) {
            redirect(base_url() . 'login');
            exit;
        }
    }

    public function index() {
       $data=array();
       $dataarr = $this->plan_model->listing_plan();
       $data['plan_listing']=$dataarr->listing_plan;
       $data['ispdetail']=$dataarr->ispdetail;
       $data['plan_count']=$dataarr->plan_count;
       $data['state_list']=$dataarr->state_list;
       
       
     // echo "<pre>"; print_R($data);die;
        $this->load->view('plan/plan_view', $data);
    }
    
     public function add_plan() {
      /*   if (!$this->plan_model->is_permission(PLANS, 'HIDE')) {
            redirect(base_url());
        }  
       if(!$this->plan_model->is_permission(PLANS,'ADD')){
       redirect(base_url()."plan");
       }*/
        $data=array();
        $dataarr= $this->plan_model->get_ispdetail_info();
      //  echo "<pre>"; print_R($dataarr);die;
        $datastatearr=$this->state_list('');
      //  echo "<pre>"; print_R($datastatearr); die;
       $data['ispdetail']=$dataarr->isp_detail;
        $data['tax']=$dataarr->tax;
        $data['state_list'] = $datastatearr->state_list;
         // echo "<pre>"; print_R($data); die;
      
        $this->load->view('plan/add_plan', $data);
    }
    
     public function edit_plan($id) {
        
        // echo $id; die;
        $datarr = array();
        $datarr = $this->plan_model->plan_data($id);
         $dataarr=$this->plan_model->get_ispdetail_info();
          $datastatearr=$this->state_list('');
            $data['ispdetail']=$dataarr->isp_detail;
        $data['tax']=$dataarr->tax;
         $data['deptregion_type']='region';
        $data['plan'] = $datarr->plan_data;
        $regiondat=$this->plan_model->plan_region_data($id);
        $data['plan_region'] = $regiondat->region_data;
         $data['state_list'] = $datastatearr->state_list;
//echo "<pre>"; print_R($data);die;
        $this->load->view('plan/edit_plan', $data);
    }
    
      public function user_stat($id = NULL){
		if(isset($id) && $id!=''){
                    $datarr=$this->plan_model->active_inactive_userslist($id);
                  //  echo "=====>>>".$id;
                    // $arr=$this->setting_model->get_ispdetail_info();
                        $data['ispdetail']=$datarr->ispdetail;
		//	$data = $this->user_model->user_dashboard();
                        $data['plan_count']=$datarr->plan_count;
			$data['active_inactive']['total_results'] = $datarr->total_results;
                        $data['active_inactive']['search_results'] = $datarr->search_results;
			$data['filtertype'] = $id;
                        $data['state_list'] = $datarr->state_list;
                        $data['usage_locality'] = $datarr->usage_locality;
			$this->load->view('plan/userFilter',$data);
		}else{
			redirect(base_url().'user');
		}
	}
        
        
         public function plan_stat($plantype = NULL){
		if($plantype == 'unlimited' || $plantype == 'data' || $plantype == 'fup' || $plantype == 'time'){
                    $datarr=$this->plan_model->plan_list_filter($plantype);
                     $data['plan_count']=$datarr->plan_count;
			//$data = $this->plan_model->user_dashboard();
                    //  $arr=$this->setting_model->get_ispdetail_info();
                     $data['ispdetail']=$datarr->ispdetail;
			$data['plan_listing'] = $datarr->plan_listing;
			$data['filtertype'] = $plantype;
                          $data['state_list'] = $datarr->state_list;
                       // echo ""
			$this->load->view('plan/planFilter',$data);
		}else{
			redirect(base_url().'plan');
		}
	}

    public function state_list($state_id='')
    {
        $data= $this->plan_model->state_list($state_id);
      return $data;
    }
        
    public function getcitylist() {
        $stateid = $this->input->post('stateid');
        echo $this->plan_model->getcitylist($stateid);
    }
    
       public function getzonelist() {
        $cityid = $this->input->post('cityid');
        $stateid=$this->input->post('stateid');
        echo $this->plan_model->getzonelist($cityid,'',$stateid);
    }
    
     public function viewplan_search_results() {
        $this->plan_model->viewplan_search_results();
    }
    
    public function viewplan_searchfilter(){
		 $this->plan_model->viewplan_searchfilter();
		
	}

    public function topup() {
              if (!$this->plan_model->is_permission(TOPUP, 'HIDE')) {
            redirect(base_url());
        }
         $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $data['topup_listing'] = $this->plan_model->listing_topup();
        $data['topup_count']=$this->plan_model->get_topup_usercount();
        //echo "<pre>"; print_R($data); die;
        $this->load->view('plan/topup_view', $data);
    }

    public function add_topup() {
           if (!$this->plan_model->is_permission(TOPUP, 'HIDE')) {
            redirect(base_url());
        }
         if(!$this->plan_model->is_permission(TOPUP,'ADD')){
       redirect(base_url()."plan/topup");
       }
        $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
       $data['tax']=$this->plan_model->get_tax();
       $data['region_type']=$this->plan_model->dept_region_type();
        $data['topup_type'] = $this->plan_model->misc_data(TOPUPTYPE);
      //  echo "<pre>"; print_R($data); die;
        $this->load->view('plan/add_topup', $data);
    }

    public function add_topup_data() {
        $post = $this->input->post();
        //  echo "<pre>"; print_R($_POST);die;
        $id = $this->plan_model->add_topup_detail();
        echo $id;
    }

   
    public function add_plan_data() {

        $id = $this->plan_model->add_plan_detail();
        echo $id;
    }

    public function add_plan_setting() {
        // echo "<pre>"; print_R($this->input->post());    die;
        $id = $this->plan_model->add_plan_setting();
        echo $id;
    }

    public function add_pricing() {
        // echo "<pre>"; print_R($this->input->post());    die;
        $id = $this->plan_model->add_pricing();
        echo $id;
    }
    
      public function add_region() {
        $postdata = $this->input->post();
        echo $this->plan_model->add_region();
        
    }
    
      public function check_plan_deletable() {
        $deletable = $this->plan_model->check_plan_deletable();
        echo $deletable;
    }
    
      public function delete_plan() {
        $id = $this->plan_model->delete_plan();
        echo json_encode($id);
    }
    
      public function changeplan_status()
    {
        $data=array();
        $postdata=$this->input->post();
        if($postdata['task']=="disable")
        {
            $deletable = $this->plan_model->check_plan_disable();
           
            $msg="Plan can't be disabled User assigned with plan";
        }
        else
        {
           
            $deletable = $this->plan_model->check_plan_enable();
            $msg="Plan can't be enabled Complete the full Plan";
        }
        
        if($deletable==1)
        {
          //  echo "jjj"; die;
           $data['status']=1;
             $data['msg']=$this->plan_model->status_approval($postdata['id']); 
        }
        else
        {
           // echo "aaaa";die;
            $data['status']=0;
             $data['msg']=$msg;
        }
        echo json_encode($data);
        
    }

    public function add_topuppricing() {
        // echo "<pre>"; print_R($this->input->post());    die;
        $id = $this->plan_model->add_topuppricing();
        echo $id;
    }

   

    public function edit_topup($id) {
          if (!$this->plan_model->is_permission(TOPUP, 'HIDE')) {
            redirect(base_url());
        }  
          if(!$this->plan_model->is_permission(TOPUP,'EDIT')){
                if($this->plan_model->is_permission(TOPUP,'RO'))
              {
                  $data['ro']=1;
              }
              else
              {
                  redirect(base_url()."plan/topup");
              }
       }
        if($this->plan_model->check_topup_editable($id)==0)
       {
           //$this->session->set_flashdata('topuperror',"User Already Assigned With Topup Can't Edit");
          //  redirect(base_url()."plan/topup");
             $data['ro']=1;
       }
        // echo $id; die;
        $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
        $datarr = array();
        $datarr = $this->plan_model->plan_topup($id);
        $data['topup'] = $datarr;
        $data['tax']=$this->plan_model->get_tax();
        $data['unacctn_data'] = $this->plan_model->data_unacctn_data($id);
        $data['speedtopup_data'] = $this->plan_model->data_speedtopup_data($id);
        $data['topup_region'] = $this->plan_model->topup_region_data($id);
        $this->load->view('plan/edit_topup', $data);
      
    }

    

 

  

    public function add_region_topup() {
        $postdata = $this->input->post();
        echo $this->plan_model->add_region_topup();
    }

    public function delete_region_plan() {
        echo $this->plan_model->delete_region_plan();
    }

    public function delete_region_topup() {
        echo $this->plan_model->delete_region_topup();
    }

   

    public function viewtopup_search_results() {
        $this->plan_model->viewtopup_search_results();
    }

  
    
     public function delete_topup() {
        $id = $this->plan_model->delete_topup();
        echo json_encode($id);
    }

  
    
      public function check_topup_deletable() {
        $deletable = $this->plan_model->check_topup_deletable();
        echo $deletable;
    }
    
  
    
     public function changetopup_status()
    {
        $data=array();
        $postdata=$this->input->post();
        if($postdata['task']=="disable")
        {
            $deletable = $this->plan_model->check_topup_disable();
            $msg="Top-Up can't be disabled User assigned with Top-Up";
        }
        else
        {
            $deletable = $this->plan_model->check_topup_enable();
            $msg="Top-Up can't be enabled Complete the full Top-Up";
        }
        
        if($deletable==1)
        {
           $data['status']=1;
             $data['msg']=$this->plan_model->status_approval($postdata['id']); 
        }
        else
        {
            $data['status']=0;
             $data['msg']=$msg;
        }
        echo json_encode($data);
        
    }
    
    
   
	
        
        
      
	public function active_inactive_usersfilter(){
		$id = $this->input->post('filtertype');
		$data = $this->plan_model->active_inactive_userslist($id);
		echo json_encode($data);
		
	}
        
         public function topup_stat($topuptype = NULL){
		if($topuptype == 'datatopup' || $topuptype == 'unacctncy' || $topuptype == 'speed' ){
                     $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
                     $data['topup_count']=$this->plan_model->get_topup_usercount();
			//$data = $this->plan_model->user_dashboard();
			 $data['topup_listing'] = $this->plan_model->topup_list_filter($topuptype);
			$data['filtertype'] = $topuptype;
                       // echo ""
			$this->load->view('plan/topupFilter',$data);
		}else{
			redirect(base_url().'plan/topup');
		}
	}
	public function viewtopup_searchfilter(){
		 $this->plan_model->viewtopup_searchfilter();
		
	}
        
            public function topupuser_stat($id = NULL){
              //  echo "sssss"; die;
		if(isset($id) && $id!=''){
                    
                         $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
			$data = $this->user_model->user_dashboard();
                         $data['topup_count']=$this->plan_model->get_topup_usercount();
			$data['active_inactive'] = $this->plan_model->topupfilter_userslist($id);
			$data['filtertype'] = $id;
                        //echo "<pre>"; print_R($data); die;
			$this->load->view('plan/usertopupFilter',$data);
		}else{
			redirect(base_url().'user');
		}
	}
	public function active_inactive_topupusersfilter(){
		$id = $this->input->post('filtertype');
		$data = $this->plan_model->topupfilter_userslist($id);
		echo json_encode($data);
		
	}
        
        public function check_taxfilled()
        {
          $id=  $this->plan_model->check_taxfilled();
          echo $id;
        }
		
		public function paytm_plan($id=null)
		{
			$data=array();
			if(isset($id) && $id!=''){
				
				$data=$this->plan_model->paytm_plan_detail($id);
			}else{
			$data=$this->plan_model->paytm_plan_detail();
			}
			
			$this->load->view("plan/paytm_plan_view",$data);
		}
		
		public function add_paytm_plan()
		{
			$data=$this->plan_model->add_paytm_plan();
			if($data){
				 redirect(base_url() . 'plan/paytm_plan');
			}
		}
        
        
        
                
        
        
        
    
    
    
    

}

?>
