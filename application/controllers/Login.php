<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		$this->load->model('login_model');
		$this->load->model('permission_model');
	}

	public function index(){
		if(isset($this->session->userdata['isp_session']['isp_uid'])){
			redirect(base_url()."location");
		}
		$this->load->view('Login_view');
	}
	
	public function validate_user(){
		//echo '<pre>'; print_r($_POST);
		if(empty($_POST)){
		    redirect(base_url().'login'); exit;
		}
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim');
		$this->form_validation->set_rules('password', 'Password', 'trim|callback_check_database');
		
		if ($this->form_validation->run() == FALSE){
		    $this->load->view('Login_view');
		}else{
		    $check_terms_condition_read = $this->login_model->check_terms_condition_read();
			if($check_terms_condition_read == '0'){
				redirect(base_url().'terms');
			}else{
				redirect(base_url().'location');
			}
		}
	}
	
	public function check_database($password){
	    $username = $this->input->post('username');
	    $result = $this->login_model->check_credentials($username, $password);
	 
	    if($result){
		//echo '<pre>'; print_r($result); die;
		$sess_array = array();
		foreach($result as $row){
		  $username = isset($row->username) ? $row->username : $row->email;
		  $isp_name = isset($row->isp_name) ? substr($row->isp_name, 0 , 16).' ....'  : $row->email;
		  $sess_array = array(
			'fevicon_icon' => $row->fevicon_icon,
		    'super_admin' => $row->super_admin,
		    'userid' => $row->id,
		    'isp_uid' => $row->isp_uid,
		    'isp_name' => $isp_name,
		    'username' => $username,
		    'password' => $password,
		    'userphone' => $row->phone,
                    'dept_id' => isset($row->dept_id) ? $row->dept_id : 0,
		    'publicuser' => isset($row->publicuser) ? $row->publicuser : 0
		  );
		  $this->session->set_userdata('isp_session', $sess_array);
		}
		return TRUE;
	    }else{
	      $this->form_validation->set_message('check_database', 'Username or Password is incorrect.');
	     return false;
	    }
	}
	
	public function logout(){
		$this->session->unset_userdata('isp_session');
		redirect(base_url());
	}
}
