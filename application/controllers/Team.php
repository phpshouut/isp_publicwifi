<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Team extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('team_model');
        
         $this->load->model('setting_model');
        // $this->load->model('mgmt_model');
	$this->load->model('permission_model');
         $this->load->model('plan_model');
          $this->load->library('form_validation');
          $this->load->library('email');
           $this->load->helper('string');
           if(!$this->session->has_userdata('isp_session')){
			redirect(base_url().'login'); exit;
		}
		$isp_license_data = $this->team_model->isp_license_data(); 
		if($isp_license_data != 1){
			redirect(base_url().'manageLicense'); exit;
		}
           //     $this->setting_model->check_ispmodules();
    }
    

    

    
   
    
  
  
    
      public function username_check($str) {
        $userexist = $this->team_model->check_user_exist($str);
        if ($userexist == 0) {

 
            $this->form_validation->set_message('username_check', 'This {field}  already exist');
            return FALSE;
        } else {
           
            return TRUE;
        }
    }
   
    
    
    public function my_account()
    {
         $session_data = $this->session->userdata('isp_session');
        $id=$session_data['userid'];
         $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
      $data['userdata']=$this->team_model->get_user_data($id);
    // echo "<pre>"; print_R($data); die;
      $this->load->view("setting/My_Account",$data);
    }
    
    
     public function edit_account_data()
    {
        $postdata=$this->input->post();
       
          $data['dept']=$this->team_model->get_departments();
    
         if(isset($postdata['user_id'])){ 
        
        $this->form_validation->set_rules('password', 'password', 'callback_password_check');
        } 
       
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('phone', 'phone no', 'trim|required|min_length[10]|max_length[10]|numeric');
        if ($this->form_validation->run() == FALSE) {
            $session_data = $this->session->userdata('isp_session');
        $id=$session_data['userid'];
      $data['userdata']=$this->team_model->get_user_data($id);
       $arr=$this->setting_model->get_ispdetail_info();
           $data['ispdetail']=(isset($arr[0]))?$arr[0]:array();
             $this->load->view("setting/My_Account",$data);
        } else {
             // echo "<pre>"; print_R($postdata); die;
            $userid= $this->team_model->edit_team_member();
            if($userid)
            {
                 //echo "<pre>"; print_R($postdata); die;
                redirect(base_url()."team/My_Account");
            }
           // die;
        }
    }
    
    
         public function password_check($str) {
             $postdata=$this->input->post();
           //  echo "ssss<pre>"; print_R($postdata);die;
            // echo 'ssss';die;
        $userexist = $this->team_model->check_password_correct($str);
        if ($userexist == 0) {

 
            $this->form_validation->set_message('password_check', 'Old password is Incorrect');
            return FALSE;
        } else {
           
            return TRUE;
        }
    }
    
   
  
    
  
    
    
    
}

?>