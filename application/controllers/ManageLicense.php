<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ManageLicense extends CI_Controller {

    public function __construct(){
        parent :: __construct();
	$this->load->library('Stripegateway');
        $this->load->model('Setting_model', 'setting_model');
	 $this->load->model('Plan_model', 'plan_model');
	 $this->load->model('Permission_model', 'permission_model');
        if(!$this->session->has_userdata('isp_session')){
	    redirect(base_url().'login'); exit;
	}
        if($this->session->userdata['isp_session']['super_admin'] != 1){
            redirect(base_url()); exit;
        }
	//$this->setting_model->check_ispmodules();
	
    }
    public function index(){
        $data['responseCode'] = $this->setting_model->isp_detail_info();
        $this->load->view('license/isp_license', $data);
    }
    
    public function ispbilling_details(){
	$this->setting_model->ispbilling_details();
    }
    
    public function hashmac(){
        $this->setting_model->citrus_hashmac();
    }
    
    public function paymentnotify() {

    }
    public function paymentresponse() {
        //echo '<pre>'; print_r($_POST); die;
        $postdata=$this->input->post();
        $datarr=array(); $paydata = array();

        if (isset($_POST['TxId'])) {
                // echo "sssssssssss"; die;
                set_include_path('../lib' . PATH_SEPARATOR . get_include_path());
                $secret_key = CITRUSSECRETKEY;
                $data = "";
                $flag = "true";


                $paymentPostResponse = $_POST;
                if (isset($_POST['TxId'])) {
                        $txnid = $_POST['TxId'];
                        $data .= $txnid;
                }
                if (isset($_POST['TxStatus'])) {
                        $txnstatus = $_POST['TxStatus'];
                        $data .= $txnstatus;
                }
                if (isset($_POST['amount'])) {
                        $amount = $_POST['amount'];
                        $data .= $amount;
                }
                if (isset($_POST['pgTxnNo'])) {
                        $pgtxnno = $_POST['pgTxnNo'];
                        $data .= $pgtxnno;
                }
                if (isset($_POST['issuerRefNo'])) {
                        $issuerrefno = $_POST['issuerRefNo'];
                        $data .= $issuerrefno;
                }
                if (isset($_POST['authIdCode'])) {
                        $authidcode = $_POST['authIdCode'];
                        $data .= $authidcode;
                }
                if (isset($_POST['firstName'])) {
                        $firstName = $_POST['firstName'];
                        $data .= $firstName;
                }
                if (isset($_POST['lastName'])) {
                        $lastName = $_POST['lastName'];
                        $data .= $lastName;
                }
                if (isset($_POST['pgRespCode'])) {
                        $pgrespcode = $_POST['pgRespCode'];
                        $data .= $pgrespcode;
                }
                if (isset($_POST['addressZip'])) {
                        $pincode = $_POST['addressZip'];
                        $data .= $pincode;
                }
                if (isset($_POST['signature'])) {
                        $signature = $_POST['signature'];
                }

                $respSignature = hash_hmac('sha1', $data, $secret_key);
                if ($signature != "" && strcmp($signature, $respSignature) != 0) {
                        $flag = "false";
                }

                if ($flag == "true") {
                        if ($txnstatus == "SUCCESS") {
                                $paydata['paydata'] = $this->user_model->isp_transaction_update(true);
                                $this->load->view('license/payment', $paydata);
                        } else {
                                $paydata['paydata'] = $this->user_model->isp_transaction_update();
                                $this->load->view('license/payment', $paydata);

                        }
                } else {
                        $paydata['paydata'] = $this->user_model->isp_transaction_update();
                        $this->load->view('license/payment', $paydata);
                }
        } else {

                redirect(base_url()."manageLicense");
        }
    }
    
    public function pgredirect(){
	$data = array();
	$session_data = $this->session->userdata('isp_session');;
	$isp_uid = $session_data['isp_uid'];
	
	//$data['publishable_key'] = $this->stripegateway->stripe['publishable_key'];
	$data['TXN_AMOUNT'] = $this->input->post('select_module');
	$data['CUST_ID'] = $isp_uid;
	$this->load->view('license/stripe_pgredirect', $data);
    }
    
    public function spaysuccess(){
	/*
	Array
	(
	    [amt] => 2500
	    [userid] => 129
	    [stripeToken] => tok_1BSjmbDYlmthWTFfDLl5HO88
	    [stripeTokenType] => card
	    [stripeEmail] => vikas@shouut.com
	)*/
	
	$amount = $_POST['amt']/100;
	$token  = $_POST['stripeToken'];
	$customer = array(
	    'email' => $_POST['stripeEmail'],
	    'token' => $token,
	);
	$customer_id = $this->stripegateway->create_customer($customer);
	
	$stripecharge = array('customer_id' => $customer_id,
			      'amount' => $amount,
			      'currency' => 'cad',
			      'token' => $token);
	$this->stripegateway->create_charges($stripecharge);
	
	$paydata['paydata'] = $this->user_model->isp_transaction_update(true);
	$this->load->view('license/stripe_paysuccess', $paydata);
    }
}
?>