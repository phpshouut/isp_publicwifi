<?php

class Team_model extends CI_Model{
    
  public function __construct() {
        parent::__construct();
          $this->api_url = APIPATH."planpublicwifi/";
	  $this->DB2 = $this->load->database('db2', TRUE);
    }
    
    
    public function list_team()
    {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and siu.isp_uid='".$isp_uid."'";
        
         $deptarr=$this->mgmt_model->dept_user_cond();
        $cond='';
        if(!empty($deptarr))
        {
              $paramid = '"' . implode('", "', $deptarr) . '"';
                        $cond.="AND siu.dept_id IN ({$paramid})";
        }
        $query=$this->DB2->query("SELECT siu.*,sd.`dept_name` FROM sht_isp_users siu 
LEFT JOIN `sht_department` sd ON (sd.id=siu.dept_id) WHERE siu.`super_admin`='0' AND siu.`status`='1' 
AND siu.is_deleted='0' AND siu.dept_id IS NOT NULL {$cond} {$ispcond}");
    
        return $query->result();
    }
    
    
    public function get_departments()
    {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
        
          $deptarr=$this->dept_user_cond();
        $cond='';
        if(!empty($deptarr))
        {
              $paramid = '"' . implode('", "', $deptarr) . '"';
                        $cond.="AND id IN ({$paramid})";
        }
        $query=$this->DB2->query("select `id`,`dept_name`,isp_uid from sht_department where status=1 and is_deleted=0 {$cond} {$ispcond}");
      
     //   echo$this->DB2->last_query();die;
        return $query->result();
    }
    
    
      public function check_user_exist($str)
    {
           $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
        $condarr=array('username'=>$str,"status"=>'1',"is_deleted"=>'0',"isp_uid"=>$isp_uid);
        $query=$this->DB2->get_where(SHTTEAMUSER,$condarr);
     //  echo$this->DB2->last_query();die;
        if($query->num_rows()>0)
        {
            return 0;
        }
 else {
            return 1;
        }
    }
    
    public function check_password_correct()
    {
         $sessiondata = $this->session->userdata('isp_session');
         $superadmin=$sessiondata['super_admin'];
         if($superadmin==1)
         {
            $postdata=$this->input->post();
           
           
		
		$authenQuery =$this->DB2->query("SELECT * FROM sht_isp_admin WHERE (email= '".$postdata['username']."') AND orig_pwd='".$postdata['old_pwd']."' and id= '".$postdata['user_id']."'  AND is_deleted='0'");
		//echo$this->DB2->last_query();die;
                
                $validate = $authenQuery->num_rows();
		if($validate == 1){
			 return 1;
		}
                else
                {
                    return 0;
                }
		
	   
         }
         else
         {
            $postdata=$this->input->post();
           
            $query =$this->DB2->query("SELECT salt FROM sht_isp_users WHERE (id= '".$postdata['user_id']."')");
	    $exists = $query->num_rows();
	    if($exists > 0){
		$salt = $query->row()->salt;
                $password=$postdata['old_pwd'];
		$entered_password = md5($password.$salt);
		
		$authenQuery =$this->DB2->query("SELECT * FROM sht_isp_users WHERE (username= '".$postdata['username']."') AND password='".$entered_password."' and id= '".$postdata['user_id']."' AND status='1' AND is_deleted='0'");
		//echo$this->DB2->last_query();die;
                
                $validate = $authenQuery->num_rows();
		if($validate == 1){
			 return 1;
		}
		
	    }else{
		 return 0;
	    }  
         }
          
           
      
    }
    
    
    public function add_team_member()
    {
        $postdata=$this->input->post();
       $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
        if(isset($postdata['user_id']) && $postdata['user_id']!='')
        {
            
        $tabledata=array("name"=>$postdata['firstname'],"lastname"=>$postdata['lastname']
            ,"phone"=>$postdata['phone'],"super_admin"=>"0"
            ,"status"=>"1","is_deleted"=>"0","dept_id"=>$postdata['dept'],"added_on"=>date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);    
       $this->DB2->update(SHTTEAMUSER,$tabledata,array("id"=>$postdata['user_id']));
        return $postdata['user_id'];
        }
 else {
      $salt=random_string('alnum',5);
        $password=$postdata['phone'];
      //  $this->email_credential($postdata);
        $encrypt_password=md5($password.$salt);
        $tabledata=array("username"=>$postdata['username'],"name"=>$postdata['firstname'],"lastname"=>$postdata['lastname'],"email"=>$postdata['username']
            ,"password"=>$encrypt_password,"salt"=>$salt,"orig_pwd"=>$password,"phone"=>$postdata['phone'],"super_admin"=>"0"
            ,"status"=>"1","is_deleted"=>"0","dept_id"=>$postdata['dept'],"added_on"=>date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);   
       $this->DB2->insert(SHTTEAMUSER,$tabledata,array("id"=>$postdata['user_id']));
        return$this->DB2->insert_id();
        }
 }
 
 public function update_userpwd()
 {
    $postdata=$this->input->post();
    $query=$this->db->query("select salt from sht_isp_users where id='".$postdata['teamid']."'");
    if($query->num_rows()>0)
    {
	
	$rowarr=$query->row_array();
	 $encrypt_password=md5($postdata['changepwd'].$rowarr['salt']);
	$tabledata=array("orig_pwd"=>$postdata['changepwd'],"password"=>$encrypt_password);
	$this->db->update("sht_isp_users",$tabledata,array("id"=>$postdata['teamid']));
	$data['resultcode']=1;
	$data['resultmsg']="Succes";
	$data['origpwd']=$postdata['changepwd'];
    }
    else{
	$data['resultcode']=0;
	$data['resultmsg']="Sorry Unable to update password";
    }
    return $data;
   // echo "<pre>"; print_R($postdata);
 }
   
   public function edit_team_member()
    {
        $postdata=$this->input->post();
      
     
           $salt=random_string('alnum',5);
        $password=$postdata['newpwd'];
      //  $this->email_credential($postdata);
        $encrypt_password=md5($password.$salt); 
        $tabledata=array("name"=>$postdata['firstname'],"lastname"=>$postdata['lastname']
            ,"password"=>$encrypt_password,"orig_pwd"=>$password,"salt"=>$salt,"phone"=>$postdata['phone'],"super_admin"=>"0"
            ,"status"=>"1","is_deleted"=>"0","added_on"=>date("Y-m-d H:i:s"));  
         $sessiondata = $this->session->userdata('isp_session');
         $superadmin = $sessiondata['super_admin'];
         if($superadmin==1)
         {
              $password=$postdata['newpwd'];
      //  $this->email_credential($postdata);
        $encrypt_password=md5($password); 
            $tabledata=array("isp_name"=>$postdata['firstname']
            ,"password"=>$encrypt_password,"orig_pwd"=>$password,"phone"=>$postdata['phone'],"super_admin"=>"1"
           ,"is_deleted"=>"0");   
            $this->DB2->update('sht_isp_admin',$tabledata,array("id"=>$postdata['user_id']));
	    //echo "====>>".$this->DB2->last_query(); die;
         }
         else
         {
              $this->DB2->update(SHTTEAMUSER,$tabledata,array("id"=>$postdata['user_id']));
         }
      
        return $postdata['user_id'];
      
 }
 
 public function get_user_data($id)
 {
     $condarr=array("id"=>$id);
      $sessiondata = $this->session->userdata('isp_session');
         $superadmin = $sessiondata['super_admin'];
         if($superadmin==1)
         {
            $query=$this->DB2->query('select id,email as username,isp_name as name,isp_uid ,phone from sht_isp_admin where id="'.$id.'"');
	   
         }
        else {
 $query=$this->db->get_where(SHTTEAMUSER,$condarr);
        }
    
     return $query->row_array();
 }
 
 public function get_team_user_data($id)
 {
     $condarr=array("id"=>$id);
      $sessiondata = $this->session->userdata('isp_session');
       
         
 $query=$this->db->get_where(SHTTEAMUSER,$condarr);
       
    
     return $query->row_array();
 }
    
   
      public function delete_user()
        {
            $postdata=$this->input->post();
            $tabledata=array("is_deleted"=>"1");
          $this->DB2->update(SHTTEAMUSER, $tabledata, array('id' => $postdata['userid']));
                        return  $postdata['userid'];
        }
        
        public function email_credential($postdata)
        {
               $from_email='experts@shouut.com';
   $to_email=$postdata['username'];
   $msg.="Hi '".$postdata['name']."' ,<br/><br/>you credential are<br/><br/> Username: '".$postdata['username']."' <br/>"
           . " Password: '".$postdata['password']."' <br/><br/>Thanks<br/>Shouut Decibel Team";
         $this->email->from($from_email, 'Shouut Decibel'); 
         $this->email->to($to_email);
         $this->email->subject('Email for User Credential'); 
         $this->email->message($msg); 
   
         //Send mail 
       if($this->email->send()) 
        return 1;
         else 
         return 2;
        }
	
	
	public function isp_license_data(){
		$wallet_amt = 0; $passbook_amt = 0;
		$walletQ =$this->DB2->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".ISPID."' AND is_paid='1'");
		if($walletQ->num_rows() > 0){
		    $wallet_amt = $walletQ->row()->wallet_amt;
		}
		
		$passbookQ =$this->DB2->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='".ISPID."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		if($balanceamt < 0){
			return 0;
		}else{
			return 1;
		}
	}
	
	 public function dept_user_cond()
        {
            $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $regiontype = $this->dept_region_type();
       
        $permicond = '';
        $sczcond = '';
        $deptarr=array();
        if ($regiontype == "region") {
            $dept_regionQ = $this->DB2->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "'  AND is_deleted='0' AND status='1'");
            $total_deptregion = $dept_regionQ->num_rows();
            if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
                foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid = $deptobj->city_id;
                    $zoneid = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (state_id='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "') ";
                    } else {
                        $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "' AND zone_id='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
            }
            if($permicond!='')
            {
            $sczcond .= ' AND (' . $permicond . ')';
            }
            $query=$this->DB2->query("select dept_id from sht_dept_region where status='1' {$sczcond}");
          //  echo $this->db->last_query();
            $deptarr=array();
            foreach($query->result() as $val)
            {
                $deptarr[]=$val->dept_id;
            }
           // echo "<pre>"; print_R($deptarr); die;
             
        }
        return $deptarr;
        }
	
	  public function dept_region_type() {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $region_type = '';
        if ($superadmin == 1) {
            $region_type = 'region';
        } else {
            $query = $this->DB2->get_where('sht_department', array('id' => $dept_id));
            if ($query->num_rows() > 0) {
                $rowarr = $query->row_array();
                $region_type = $rowarr['region_type'];
            }
        }
        

        return $region_type;
    }

    
}

?>
