<?php

class plan_model extends CI_Model {

    public function __construct() {
        parent::__construct();
          $this->api_url = APIPATH."planpublicwifi/";
	  $this->DB2 = $this->load->database('db2', TRUE);
    }
 public function curlhit($url,$requestData)
    {
      
       $service_url = $url;
        $curl = curl_init($service_url);
        $requestData = $requestData;
        $data_request = json_encode($requestData);
        $curl_post_data = array("requestData" => $data_request);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        return json_decode($curl_response);
      
    }
    
   

    public function listing_plan() {
        
          $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin
		);
        
        $service_url = $this->api_url."listing_plan";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
    }
    
    
     public function active_inactive_userslist($searchtype) {
         $postdata=$this->input->post();
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'searchtype'=>$searchtype,
            'search_user'=>(isset($postdata['search_user']))?$postdata['search_user']:"",
            'state'=>(isset($postdata['state']))?$postdata['state']:"",
            'city'=>(isset($postdata['city']))?$postdata['city']:"",
            'zone'=>(isset($postdata['zone']))?$postdata['zone']:"",
            'locality'=>(isset($postdata['locality']))?$postdata['locality']:""
		);
      //  echo json_encode($requestData); die;
        $service_url = $this->api_url."active_inactive_userslist";
        $data=$this->curlhit($service_url,$requestData);
      // echo "<pre>"; print_R($data); die;
        return $data;
         
         
        
    }
    
     public function get_ispdetail_info()
         {
              $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $requestData = array(
			'isp_uid' => $isp_uid
                );
         // echo json_encode($requestData); die;
             $service_url = $this->api_url."get_ispdetail_info";
            
        $data=$this->curlhit($service_url,$requestData);
        return $data;
                   
         }
         
         public function get_ispbase_info()
         {
               $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $requestData = array(
			'isp_uid' => $isp_uid
                );
         // echo json_encode($requestData); die;
             $service_url = $this->api_url."get_ispbase_info";
            
        $data=$this->curlhit($service_url,$requestData);
        return $data;
         }
         
         
          public function get_plan_usercount() {

         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin
		);
          $service_url = $this->api_url."get_plan_usercount";
        $data=$this->curlhit($service_url,$requestData);
        return $data;
        
         }
         
         
           public function getcitylist($stateid, $cityid = '') {
                 $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              "stateid"=>$stateid,
              "cityid"=>$cityid,
              "is_addable"=>(isset($postdata['is_addable']) && $postdata['is_addable']!='')?1:''
		);
         // echo json_encode($requestData); die;
          $service_url = $this->api_url."getcitylist";
        $data=$this->curlhit($service_url,$requestData);
        return $data;
               
        
    }
    
     public function getzonelist($cityid, $zoneid = '', $stateid = '') {
         
          $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              "stateid"=>$stateid,
              "cityid"=>$cityid,
             "zoneid"=> $zoneid,
              "is_addable"=>(isset($postdata['is_addable']) && $postdata['is_addable']!='')?1:''
		);
         // echo json_encode($requestData); die;
          $service_url = $this->api_url."getzonelist";
        $data=$this->curlhit($service_url,$requestData);
        return $data;
         
      
    }
    
      public function plan_list_filter($plantype) {
       $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              "plantype"=>$plantype
		);
         // echo json_encode($requestData); die;
          $service_url = $this->api_url."plan_list_filter";
        $data=$this->curlhit($service_url,$requestData);
       // echo "<pre>"; print_R($data);die;
        return $data;
    }
    
    
     public function viewplan_search_results() {
         
          $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              "search_user"=>$postdata['search_user'],
               "state"=>$postdata['state'],
               "city"=>$postdata['city'],
               "zone"=>$postdata['zone']
              
		);
        //  echo json_encode($requestData); die;
          $service_url = $this->api_url."viewplan_search_results";
        $data=$this->curlhit($service_url,$requestData);
     // echo "<pre>"; print_r($data);

        $gen='';
        $total_search = count($data);
        $i = 1;
       // $is_editable = $this->plan_model->is_permission(PLANS, 'EDIT');
       // $is_deletable = $this->plan_model->is_permission(PLANS, 'DELETE');
        foreach ($data as $sobj) {

            $plandata = "";
            if ($sobj->plantype == 1) {
                $plandata = "-";
                 $plantype="Time Plan";
            } else if ($sobj->plantype == 2) {
                  $plandata = round($this->convertTodata($sobj->datalimit . "MB"),3) . " MB";
                 $plantype="Data Plan";
            } else if ($sobj->plantype == 3) {
                $plandata = round($this->convertTodata($sobj->datalimit . "MB"),3) . " MB";
                 $plantype="Hybrid Plan";
            } 

            $status = ($sobj->enableplan == 1) ? 'active' : 'inactive';
            if ($sobj->enableplan == 1) {
                $class = 'class="inactive delete"';
                $status = '<img src="' . base_url() . 'assets/images/on2.png" rel="disable">';
                $stat1 = "Active";
                // $task="disable";
            } else {
                $class = 'class="active delete"';
                $status = '<img src="' . base_url() . 'assets/images/off2.png" rel="enable">';
                $stat1 = "Inactive";
                // $task="enable";
            }

            $gen .= '
				<tr>
					<td>' . $i . '.</td>
					
					<td><a href="' . base_url() . 'plan/edit_plan/' . $sobj->srvid . '">' . $sobj->srvname . '</a></td>
                                            <td>' . $plantype . '</td>
					<td>' . $this->convertTodata($sobj->downrate . "KB") . '</td>
					<td>' . $this->convertTodata($sobj->uprate . "KB") . '</td>
					<td>' . $plandata . '</td>
					<td>₹' . $sobj->net_total . '</td>
					<td><a href="' . base_url() . 'plan/user_stat/' . $sobj->srvid . '">' . $sobj->usercount . '</a></td>';
           
                $gen .= '<td><a ' . $class . '  onclick = "change_planstatus(\'' . $sobj->srvid . '\')" href="javascript:void(0)" id="' . $sobj->srvid . '"> ' . $status . ' </a></td>';
           
            $gen .= '<td><a href="' . base_url() . 'plan/edit_plan/' . $sobj->srvid . '">EDIT</a></td>';
            $delclass = "deletplan";

            $gen .= ' <td><a href="javascript:void(0);" class="' . $delclass . '"  rel="' . $sobj->srvid . '">Delete</a></td>';

            $gen .= '</tr>
				';
            $i++;
        }


        $data['total_results'] = $total_search;
        $data['search_results'] = $gen;

        echo json_encode($data);
    }
    
      public function viewplan_searchfilter() {
          
           $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              "search_user"=>$postdata['search_user'],
               "state"=>$postdata['state'],
               "city"=>$postdata['city'],
               "zone"=>$postdata['zone'],
              "filtertype"=>$postdata['filtertype']
              
		);
           $service_url = $this->api_url."viewplan_searchfilter";
        $data=$this->curlhit($service_url,$requestData);
          
       

        echo json_encode($data);
    }
    
    
      public function state_list($stateid = '') {
          
          
            $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
               "stateid"=>$stateid
              );
        //  echo json_encode($requestData);die;
           $service_url = $this->api_url."state_list";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
      
    }
    
     public function getstatelist($stateid = '') {
          
          
            $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
               "stateid"=>$stateid
              );
        //  echo json_encode($requestData);die;
           $service_url = $this->api_url."getstatelist";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
      
    }
    
     public function add_plan_detail() {
         
          $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              'postdata'=>$postdata
              );
          //echo json_encode($requestData);die;
           $service_url = $this->api_url."add_plan_detail";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
    }
    
    
       public function add_plan_setting() {
           
            $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              'postdata'=>$postdata
              );
          //echo json_encode($requestData);die;
           $service_url = $this->api_url."add_plan_setting";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
     
    }
    
     public function add_pricing() {
         $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              'postdata'=>$postdata
              );
          //echo json_encode($requestData);die;
           $service_url = $this->api_url."add_pricing";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
    }

     public function add_region() {
         $postdata = $this->input->post();
                $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              'postdata'=>$postdata
              );
          //echo json_encode($requestData);die;
           $service_url = $this->api_url."add_region";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
    }
    
        public function plan_data($id) {
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              'id'=>$id
              );
           $service_url = $this->api_url."plan_data";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
      
    }
    
       public function plan_region_data($id) {
           
            $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
              'id'=>$id
              );
           $service_url = $this->api_url."plan_region_data";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
       
    }

      public function check_plan_deletable() {
       
        $planid=$this->input->post('delplanid');
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=> $super_admin,
              'planid'=> $planid
              );
           $service_url = $this->api_url."check_plan_deletable";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
    }
    
     public function delete_plan() {
          $postdata = $this->input->post();
           $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=> $super_admin,
              'postdata'=> $postdata
              );
          //echo json_encode($requestData); die;
           $service_url = $this->api_url."delete_plan";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
       
       
    }
  
     public function check_plan_disable() {
         $postdata = $this->input->post();
           $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=> $super_admin,
              'postdata'=> $postdata
              );
           $service_url = $this->api_url."check_plan_disable";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
    }
    
       public function check_plan_enable() {
      $postdata = $this->input->post();
           $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=> $super_admin,
              'postdata'=> $postdata
              );
           $service_url = $this->api_url."check_plan_enable";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
    }
    
     public function check_taxfilled()
        {
         
          $postdata = $this->input->post();
           $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=> $super_admin,
              'postdata'=> $postdata
              );
           $service_url = $this->api_url."check_taxfilled";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
          
        } 
    
    public function status_approval($id) {
        
          $postdata = $this->input->post();
           $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
          $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
          $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=> $super_admin,
              'id'=> $id
              );
          //echo json_encode($requestData); die;
           $service_url = $this->api_url."status_approval";
        $data=$this->curlhit($service_url,$requestData);
       return $data;
        
        
    }

    public function convertToBytes($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number * 1024;
            case "MB":
                return $number * 1024 * 1024;
            case "GB":
                return $number * 1024 * 1024 * 1024;
            case "TB":
                return $number * 1024 * 1024 * 1024 * 1024;
            case "PB":
                return $number * 1024 * 1024 * 1024 * 1024 * 1024;
            default:
                return $from;
        }
    }

    public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number / pow(1024, 2);
            case "GB":
                return $number / pow(1024, 3);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }

    function timeToSeconds($time) {
        $timeExploded = explode(':', $time);
        if (isset($timeExploded[2])) {
            return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
        }
        return $timeExploded[0] * 3600 + $timeExploded[1] * 60;
    }



   

    public function delete_region_plan() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1);
        $this->db->update(SHTPLANREGION, $tabledata, array('id' => $postdata['id']));
       
        return $postdata['id'];
    }

   

    function sec2hms($secs) {
        $secs = round($secs);
        $secs = abs($secs);
        $hours = floor($secs / 3600) . ':';
        if ($hours == '0:')
            $hours = '';
        $minutes = substr('00' . floor(($secs / 60) % 60), -2);
        //$seconds = substr('00' . $secs % 60, -2);
        return ltrim($hours . $minutes, '0');
    }

   

   

  

  

   

  

    public function plan_enable_setting($plan_id, $form) {
        $query = $this->db->query("select id from sht_plan_activation_panel where plan_id='" . $plan_id . "'");
        if ($query->num_rows() > 0) {
            if ($form == "PLANDETAIL") {
                $tabledata = array("plan_detail" => 1);
                $this->db->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else if ($form == "PLANSETTING") {
                $tabledata = array("plan_setting" => 1);
                $this->db->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else if ($form == "PLANPRICING") {
                $tabledata = array("plan_pricing" => 1);
                $this->db->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else {
                $tabledata = array("plan_region" => 1);
                $this->db->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            }
        } else {
            if ($form == "PLANDETAIL") {
                $tabledata = array("plan_detail" => 1, 'plan_id' => $plan_id);
                $this->db->insert(PLANACTIVATIONTAB, $tabledata);
            } else if ($form == "PLANSETTING") {
                $tabledata = array("plan_setting" => 1, 'plan_id' => $plan_id);
                $this->db->insert(PLANACTIVATIONTAB, $tabledata);
            } else if ($form == "PLANPRICING") {
                $tabledata = array("plan_pricing" => 1, 'plan_id' => $plan_id);
                $this->db->insert(PLANACTIVATIONTAB, $tabledata);
            } else {
                $tabledata = array("plan_region" => 1, 'plan_id' => $plan_id);
                $this->db->insert(PLANACTIVATIONTAB, $tabledata);
            }
        }
        $conarr = array("plan_id" => $plan_id, "plan_detail" => 1, "plan_setting" => 1, "plan_pricing" => 1, "plan_region" => 1, "forced_disable" => 0);
        $query1 = $this->db->get_where(PLANACTIVATIONTAB, $conarr);
        if ($query1->num_rows() > 0) {
            $tabledata = array("enableplan" => 1);
            $this->db->update(SHTSERVICES, $tabledata, array('srvid' => $plan_id));
        }

        return $plan_id;
    }

  

   
    
     public function check_plan_editable($id) {
        $planid = $id;
        $query = $this->db->query("select id from sht_users where baseplanid='" . $planid . "' and enableuser='1'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }

 

 

    

   public function is_permission($slug, $permtype = "RO") {

        $userid = $this->session->userdata['isp_session']['userid'];
       $sessiondata = $this->session->userdata('isp_session');
      
       $query=$this->DB2->query("select dept_id from sht_isp_users where id='".$userid."'");
       $rowarr=$query->row_array();
         $isp_uid = $sessiondata['isp_uid'];
        if ($sessiondata['super_admin'] == 1) {
            return true;
        } else {
            $deptid = $rowarr['dept_id'];
            $query1 = $this->DB2->query("select id from sht_tab_menu where slug='" . $slug . "' ");
            $tabarr = $query1->row_array();
            $tabid = $tabarr['id'];
            //  echo $deptid."::".$tabid;

            $field = '';
            if ($permtype == "ADD") {
                $field = 'is_add';
            } else if ($permtype == "EDIT") {
                $field = 'is_edit';
            } else if ($permtype == "DELETE") {
                $field = 'is_delete';
            } else if ($permtype == "HIDE") {
                $field = 'is_hide';
            } else {
                $field = 'is_readonly';
            }

            $query2 = $this->DB2->query("select " . $field . " as perm from sht_isp_permission where isp_deptid='" . $deptid . "' and tabid='" . $tabid . "'");
            //  echo $this->db->last_query();
            //  die;
            if ($query2->num_rows() > 0) {
                $permarr = $query2->row_array();
                if ($permarr['perm'] == 1) {
                    if($permtype=="HIDE")
                    {
                         return false;
                    }
                    else
                    {
                         return true;
                    }
                   
                } else {
                     if($permtype=="HIDE")
                    {
                         return true;
                    }
                    else
                    {
                         return false;
                    }
                    //return false;
                }
            } else {
                return false;
            }
        }
    }

    public function leftnav_permission() {

        $userid = $this->session->userdata['isp_session']['userid'];
        $query = $this->DB2->query("select id,super_admin,dept_id from sht_isp_users where id='" . $userid . "'");
        $tabarr = array();
        $rowarr = $query->row_array();
        $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];

        $deptid = $rowarr['dept_id'];


        if ($sessiondata['super_admin'] == 1) {
            $query = $this->DB2->query("select slug from sht_tab_menu where status='1'");
            foreach ($query->result() as $val) {
                $tabarr[$val->slug] = 0;
            }
        } else {
            $query1 = $this->DB2->query("SELECT sip.is_hide,stm.slug FROM `sht_isp_permission` sip
INNER JOIN `sht_tab_menu` stm ON (stm.id=sip.tabid) WHERE isp_deptid='" . $deptid . "' ");
            foreach ($query1->result() as $val) {
                $tabarr[$val->slug] = ($sessiondata['super_admin'] == 1) ? 0 : $val->is_hide;
            }
        }
        $tabarr['PUBLICWIFI']=$this->isp_publicwifi_perm();
        //  echo $deptid."::".$tabid;
      
       
        return $tabarr;
    }
    
     public function isp_publicwifi_perm()
    {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $query=$this->DB2->query("select id from sht_isp_admin_modules where isp_uid='".$isp_uid."' and module='7' and status='1'");
      //   echo $this->db->last_query();die;
         if($query->num_rows()>0)
         {
             return 1;
         }
         else
         {
             return 0;
         }
         
    }

    public function plan_nas_association($plan_id) {
        $condarr = array();
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_regionQ = $this->db->query("select * from sht_plan_region where plan_id='" . $plan_id . "'");
         $total_deptregion= $dept_regionQ->num_rows();
         $permicond='';
         $sczcond='';
          if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
         foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid = $deptobj->city_id;
                    $zoneid = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (nasstate='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (nasstate='" . $stateid . "' AND nascity='" . $cityid . "') ";
                    } else {
                        $permicond .= " (nasstate='" . $stateid . "' AND nascity='" . $cityid . "' AND naszone='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
          }
            
            if($permicond!='')
            {
            $sczcond .= '(' . $permicond . ')';
            }
              $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
            
             $query1 = $this->db->query("select distinct(id) from nas where {$sczcond} {$ispcond}");
        

        foreach ($query1->result() as $val) {
            $query1 = $this->db->query("select srvid from sht_plannasassoc where srvid='" . $plan_id . "' and nasid='" . $val->id . "'");
            if ($query1->num_rows() == 0) {
                $tabledata = array('srvid' => $plan_id, 'nasid' => $val->id);
                $this->db->insert('sht_plannasassoc', $tabledata);
            }
        }
    }
	
	public function paytm_plan_detail($id=null){
		$get=$this->input->get();
		$sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
		 $data=array();
		 $data['plan_detail']=array();
		 $mappedloc_planarr=array();
		if($id!=''){
			$pland=$this->db->query("select srvid,srvname,price,beans from sht_services where srvid='".$id."'");
			if($pland->num_rows()>0){
				$plandata=$pland->row_array();
				$data['plan_detail']['srvid']=$plandata['srvid'];
				$data['plan_detail']['srvname']=$plandata['srvname'];
				$data['plan_detail']['price']=$plandata['price'];
				$data['plan_detail']['beans']=$plandata['beans'];
			}
			
			if(isset($get['loc_uid']) && $get['loc_uid']!=''){
				$mappedloc_planarr[$get['loc_uid']]=$get['loc_uid'];
			}else{
			$mappedlocq=$this->db->query("select location_uid from middleware_location_plan where plan_id='".$id."' and is_deleted='0'");
			//echo $this->db->last_query(); die;
			if($mappedlocq->num_rows()>0){
				foreach($mappedlocq->result() as $val){
					$mappedloc_planarr[$val->location_uid]=$val->location_uid;
				}
			}
			}
			
			
		}
		
		$planlistq=$this->db->query("select srvid,srvname from sht_services where isp_uid='".$isp_uid."' and is_deleted='0'");
		$planlistarr=array();
		if($planlistq->num_rows()>0){
			$i=0;
			foreach($planlistq->result() as $val){
				$planlistarr[$i]['srvid']=$val->srvid;
				$planlistarr[$i]['srvname']=$val->srvname;
				$i++;
			}
		}
		
		$location_list=$this->db->query("select id,location_name,location_uid from wifi_location where status='1' and isp_uid='".$isp_uid."' and status='1' and is_deleted='0'");
		$locarr=array();
		if($location_list->num_rows()>0){
			$i=0;
			foreach($location_list->result() as $locval){
				$locarr[$i]['location_uid']=$locval->location_uid;
				$locarr[$i]['location_name']=$locval->location_name;
				$selected=(isset($mappedloc_planarr[$locval->location_uid]))?1:0;
				$locarr[$i]['is_selected']=$selected;
				$i++;
			}
		}
		
		$data['plan_list']=$planlistarr;
		$data['loc_list']=$locarr;
		
		$mappedplan=$this->db->query("select ss.srvid,ss.is_deleted,ss.price_type,ss.srvname,ss.price,ss.beans,wl.location_name,wl.location_uid from sht_services ss inner join middleware_location_plan mlp on (ss.srvid=mlp.plan_id) inner join wifi_location wl on (wl.location_uid=mlp.location_uid)
		where ss.isp_uid='".$isp_uid."' and ss.is_deleted=0");
		$mappedplanarr=array();
		if($mappedplan->num_rows()>0){
			$i=0;
			foreach($mappedplan->result() as $val){
				$mappedplanarr[$i]['srvid']=$val->srvid;
				$mappedplanarr[$i]['srvname']=$val->srvname;
				$mappedplanarr[$i]['price']=$val->price;
				$mappedplanarr[$i]['price_type']=$val->price_type;
				$mappedplanarr[$i]['beans']=$val->beans;
				$mappedplanarr[$i]['location_uid']=$val->location_uid;
				$mappedplanarr[$i]['location_name']=$val->location_name;
				$i++;
			
			}
		}
		$data['mapped_list']=$mappedplanarr;
		return $data;
	}
	
	public function add_paytm_plan()
	{
		$post=$this->input->post();
		$pricetype=($post['price']>0)?1:0;
		$isfree=($post['price']>0)?0:1;
		$srvdata=array("price"=>$post['price'],"price_type"=>$pricetype,"beans"=>$post['beans']);
		$middlelocdata=array("location_uid"=>$post['location'],"plan_id"=>$post['plan'],"is_free"=>$isfree,"is_deleted"=>0,"created_on"=>date("Y-m-d H:i:s"));
		$planassocdata=array("srvid"=>$post['plan'],"nasid"=>28,"created_on"=>date("Y-m-d H:i:s"));
		
		$this->db->update("sht_services",$srvdata,array("srvid"=>$post['plan']));
		
		$query=$this->db->query("select id from sht_plannasassoc where nasid=28 and srvid='".$post['plan']."'");
		if($query->num_rows()>0){
			unset($planassocdata['created_on']);
			$this->db->update("sht_plannasassoc",$planassocdata,array("srvid"=>$post['plan'],"nasid"=>28));
		}else{
			
			$this->db->insert("sht_plannasassoc",$planassocdata);
		}
		
		$query=$this->db->query("select id from middleware_location_plan where location_uid='".$post['location']."' and plan_id='".$post['plan']."'");
		if($query->num_rows()>0){
			unset($middlelocdata['created_on']);
			$this->db->update("middleware_location_plan",$middlelocdata,array("plan_id"=>$post['plan'],"location_uid"=>$post['location']));
		}else{
			$this->db->insert("middleware_location_plan",$middlelocdata);
		}
		return true;
		
		
		//echo "<pre>"; print_R($post); die;
	}


}

?>
