<?php

class Legal_intervention_model extends CI_Model {

    public function get_location_list($locations){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_type' => '',
			'state' => '',
			'city' => '',
			'search_pattern' => ''
		);
		$service_url = $this->api_url."location_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$location_responce = json_decode($curl_response);
                $location_option = '';
		
                if($location_responce->resultCode == 1){
		     $location_option .= "<option value=''>Select Location</option>";    
                    foreach($location_responce->location_list as $location_responce1){
                      
                            $sel = "";
                            //if($location_responce1->nasip ==  $locations){
			    if($location_responce1->locationid ==  $locations){
                                $sel = "selected";
                            }
                        $location_option .= "<option value='".$location_responce1->locationid."' $sel   >".$location_responce1->location_name."</option>";
                        //$location_option .= "<option value='".$location_responce1->nasip."' $sel   >".$location_responce1->location_name."</option>";
                    }    
                }else{
                    $location_option .= "<option value=''>No location Found</option>";    
                }
                
                
                return $location_option;
                
    }
    public function total_user_lazy_loading($mobile, $selecteddate, $selecteddateto, $location_nasip){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
			'mobile' => $mobile,
			'selecteddate' => $selecteddate,
			'selecteddateto' => $selecteddateto,
			'location_nasip' => $location_nasip
		);
		$service_url = $this->api_url."legal_invention_total_user";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
               // print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
                
    }
    public function get_user($offset, $limit, $mobile, $selecteddate, $selecteddateto, $location_nasip){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
			'start' => $offset,
                        'limit' => $limit,
			'mobile' => $mobile,
			'selecteddate' => $selecteddate,
			'selecteddateto' => $selecteddateto,
			'location_nasip' => $location_nasip
		);
		$service_url = $this->api_url."legal_invention_get_user";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
               // print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		//return json_decode($curl_response);
		$result = json_decode($curl_response);
		$data= array();
		$res = '';
		$loadmore = 0;
		if(count($result) > 0){
		    $i = $offset+1;
		    foreach($result as $row){
			$stop_time = '';
			if($row->acctstoptime != ''){
			  $stop_time = date("d.m.Y H:i:s",strtotime($row->acctstoptime));
			}
			$res .= "<tr>";
			$res .= "<td>".$row->mobile."</td>";
			$res .= "<td>".$row->firstname." ".$row->lastname."</td>";
			$res .= "<td>".$row->email."</td>";
			$res .= "<td>".date("d.m.Y H:i:s",strtotime($row->acctstarttime))."</td>";
			$res .= "<td>".$stop_time."</td>";
			$res .= "<td>".round($row->acctinputoctets/(1024*1024), 2).'/'.round($row->acctoutputoctets/(1024*1024), 2)."</td>";
			$res .= "<td>".$row->nasipaddress."</td>";
			$res .= "<td>".$row->callingstationid."</td>";
			//$res .= "<td>".$row->nasportid."</td>";
			//$res .= "<td>".$row->nasporttype."</td>";
			$res .= "</tr>";
			$i++;
		    }
		    $loadmore = 1;
		}
		else{
		    $loadmore = 0;
		    $res .= '<tr><td colspan="8" style="text-align:center">No More Result Found !!</td></tr>';
		}
		$data['limit'] = $limit;
		$data['offset'] = $limit+$offset;
		$data['total_record'] = count($result);
		$data['search_results'] = $res;
		$data['loadmore'] = $loadmore;
		return $data;
                
    }
    
    public function legal_export_excel($mobile, $selecteddate, $selecteddateto, $location_nasip){
	$total_users_record = $this->total_user_lazy_loading($mobile, $selecteddate,
		$selecteddateto, $location_nasip);
	$data = array();
        $requestData = array(
	    'isp_uid' => $this->isp_uid,
	    'start' => 0,
            'limit' => $total_users_record,
	    'mobile' => $mobile,
	    'selecteddate' => $selecteddate,
	    'selecteddateto' => $selecteddateto,
	    'location_nasip' => $location_nasip
	);
	$service_url = $this->api_url."legal_invention_get_user";
	$curl = curl_init($service_url);
	$requestData = $requestData;
	$data_request = json_encode($requestData);
        //print_r($data_request);die;
	$curl_post_data = array("requestData" => $data_request);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	$curl_response = curl_exec($curl);
	curl_close($curl);
	//return json_decode($curl_response);
	$result = json_decode($curl_response);
	$this->load->library('excel');
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	$styleArray = array(
		'font'  => array(
		'bold'  => false,
		'color' => array('rgb' => '000000'),
		'name'  => 'Tahoma',
		'size' => 14
	));
	//Set sheet style
	$styleArray1 = array(
		'font'  => array(
		'bold'  => false,
		'color' => array('rgb' => '000000'),
		'name'  => 'Tahoma',
		'size' => 12
	));
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	
	$objPHPExcel->getActiveSheet()->setTitle("Locations");
	
	//Set sheet columns Heading
	$objPHPExcel->getActiveSheet()->setCellValue('A1','Mobile');
	$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->setCellValue('B1','Name');
	$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->setCellValue('C1','Email');
	$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->setCellValue('D1','Session Start');
	$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->setCellValue('E1','Session Stop');
	$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->setCellValue('F1','Mb Used(U/D)');
	$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->setCellValue('G1','Nas IP');
	$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->setCellValue('H1','MAC ID');
	$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
	//$objPHPExcel->getActiveSheet()->setCellValue('I1','NAS PORT ID');
	//$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
	
	$i = 1;
	$j = 2;// excel row number
	foreach($result as $row){
	    $stop_time = '';
	    if($row->acctstoptime != ''){
		$stop_time = date("d.m.Y H:i:s",strtotime($row->acctstoptime));
	    }
	    $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$row->mobile);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
	    $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$row->firstname." ".$row->lastname);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
	    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$row->email);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
	    $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,date("d.m.Y H:i:s",strtotime($row->acctstarttime)));
            $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
	    $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$stop_time);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
	    $objPHPExcel->getActiveSheet()->setCellValue('F'.$j,round($row->acctinputoctets/(1024*1024), 2).'/'.round($row->acctoutputoctets/(1024*1024), 2));
            $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
	    $objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$row->nasipaddress);
            $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
	    $objPHPExcel->getActiveSheet()->setCellValue('H'.$j,$row->callingstationid);
            $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);
//	    $objPHPExcel->getActiveSheet()->setCellValue('I'.$j,$row->nasportid);
//            $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
	    $i++;
	    $j++;
	}
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('assets/LegalIntervention.xlsx');
	$record_found = 1;
	return $record_found;		
    }

}

?>