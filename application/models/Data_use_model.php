<?php

class Data_use_model extends CI_Model {

    public function get_location_list($locations){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
		    
		);
		$service_url = $this->api_url."location_list_data_usages";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$location_responce = json_decode($curl_response);
                $location_option = '';
                if($location_responce->resultCode == 1){
                    foreach($location_responce->location_list as $location_responce1){
                        if(count($locations) == '0'){
                            $sel = "";
                        }
                        else{
                            $sel = "";
                            if(in_array($location_responce1->locationid, $locations)){
                                $sel = "selected";
                            }
                        }
                        $location_option .= "<option value='".$location_responce1->locationid."' $sel   >".$location_responce1->location_name."</option>";
                    }    
                }else{
                    $location_option .= "<option value=''>No location Found</option>";    
                }
                
                
                return $location_option;
                
    }
    
    public function total_mb($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'state_id' => $state_id,
			'city_id' => $city_id,
			
			
		);
		$service_url = $this->api_url."total_data_used";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
               // print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
                
    }
    public function location_wise_mb($from, $to, $gender, $age_group, $time_slot, $locations, $limit, $offset,$state_id,$city_id){
	//$offset = '5000';
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'limit' => $limit,
			'offset' => $offset,
			'state_id' => $state_id,
			'city_id' => $city_id,
		);
		$service_url = $this->api_url."location_wise_total_data_used";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		$data= array();
		
		$res = '';
		$loadmore = 0;
		if(count($result) > 0){
		    $i = $offset+1;
		    foreach($result as $row){
			$res .= "<tr>";
			$res .= "<td>".$i."</td>";
			$res .= "<td>".$row->location_name."</td>";
			$res .= "<td>".$row->nasipaddress."</td>";
			$res .= "<td>".$row->total_mb."</td>";
			$res .= "</tr>";
			$i++;
		    }
		    $loadmore = 1;
		}
		else{
		    $loadmore = 0;
		    $res .= '<tr><td colspan="3" style="text-align:center">No More Result Found !!</td></tr>';
		}
		$data['limit'] = $limit;
		$data['offset'] = $limit+$offset;
		$data['total_record'] = count($result);
		$data['search_results'] = $res;
		$data['loadmore'] = $loadmore;
		return $data;
                //return json_decode($curl_response);
		//print_r(json_decode($curl_response));die;
                
    }
    
    // get state list
	public function state_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."state_list_for_filter";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function city_list($state_id, $city_id = ''){
		$requestData = array(
			'state_id' => $state_id,
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."city_list_for_filter";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select City</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->city_id == $city_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->city_id.'">'.$responce_data1->city_name.'</option>';
		}
		return $content;
	}
	
	public function location_wise_mb_export_to_excel($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id){
	//$offset = '5000';
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'limit' => '18446744073709551615',
			'offset' => '0',
			'state_id' => $state_id,
			'city_id' => $city_id,
		);
		$service_url = $this->api_url."location_wise_total_data_used";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		
		
		$this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(0);
		$styleArray = array(
		    'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'name'  => 'Tahoma',
			'size' => 14
		));
		//Set sheet style
		$styleArray1 = array(
		    'font'  => array(
		    'bold'  => false,
		    'color' => array('rgb' => '000000'),
		    'name'  => 'Tahoma',
		    'size' => 12
		));
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		
		
		$objPHPExcel->getActiveSheet()->setTitle("Location Data Report");
		//Set sheet columns Heading
		$objPHPExcel->getActiveSheet()->setCellValue('A1','S.No');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('B1','Location');
		$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('C1','IP');
		$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('D1','Data Use');
		$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
		$i = 1;
		$j = 2;// excel row number
		foreach($result as $row){
		    $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$i);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$row->location_name);
                    $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$row->nasipaddress);
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$row->total_mb);
                    $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
		    $i++;
		    $j++;
		}
                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('assets/data_report.xlsx');
                
    }
}

?>