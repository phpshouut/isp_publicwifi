<?php

class SessionReport_model extends CI_Model {
    
    
    // get state list
	public function state_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."state_list_for_filter";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function city_list($state_id, $city_id = ''){
		$requestData = array(
			'state_id' => $state_id,
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."city_list_for_filter";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select City</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->city_id == $city_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->city_id.'">'.$responce_data1->city_name.'</option>';
		}
		return $content;
	}
    public function wifi_location_list($state_id, $city_id){
	 
        $requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_type' => '',
			'state' => $state_id,
			'city' => $city_id,
			'search_pattern' => ''
		);
		$service_url = $this->api_url."location_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
                return json_decode($curl_response);;
    }
    
    public function wifi_total_session($start_date, $end_date, $state_id, $city_id){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'state_id' => $state_id,
			'city_id' => $city_id,
			
		);
		$service_url = $this->api_url."wifi_total_session";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		//return $result->total_session;
		return $result;
		//return json_decode($curl_response);
    }
    

    public function wifi_session_report($start_date, $end_date, $state_id, $city_id, $limit, $offset){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'state_id' => $state_id,
			'city_id' => $city_id,
			'limit' => $limit,
			'offset' => $offset
			
		);
		$service_url = $this->api_url."wifi_session_report";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		$res = '';
		$loadmore = 0;
		if(count($result) > 0){
		    foreach($result as $row){
			$res .= "<tr>";
			$res .= "<td>".$row->location_name."<i class='fa fa-info-circle pull-right' aria-hidden='true' style='color: blue; cursor: pointer' data-toggle='modal' data-target='#location_info_modal' onclick='location_info(&apos;".$row->location_uid."&apos;, &apos;".$row->address."&apos;, &apos;".$row->user_name."&apos;, &apos;".$row->mobile."&apos;, &apos;".$row->user_email."&apos;)'></i></td>";
			$res .= "<td>".$row->total_cp_impression."</td>";
			$res .= "<td>".$row->unique_cp_impression."</td>";
			$res .= "<td>".$row->unique_user."</td>";
			$res .= "<td>".$row->total_session_laptop."</td>";
			$res .= "<td>".$row->total_session_android."</td>";
			$res .= "<td>".$row->total_session_iphone."</td>";
			$res .= "<td>".$row->total_session_windows_phone."</td>";
			$res .= "<th>".($row->total_session_windows_phone+$row->total_session_laptop+$row->total_session_android+$row->total_session_iphone)."</th>";
			$res .= "<td>".$row->total_user_laptop."</td>";
			$res .= "<td>".$row->total_user_android."</td>";
			$res .= "<td>".$row->total_user_iphone."</td>";
			$res .= "<td>".$row->total_user_windows_phone."</td>";
			$res .= "<th>".($row->total_user_windows_phone+$row->total_user_laptop+$row->total_user_android+$row->total_user_iphone)."</th>";
			$res .= "</tr>";
		    }
		    $loadmore = 1;
		}
		else{
		    $loadmore = 0;
		    $res .= '<tr><td colspan="14" style="text-align:center">No More Result Found !!</td></tr>';
		}
		$data['limit'] = $limit;
		$data['offset'] = $limit+$offset;
		$data['total_record'] = count($result);
		$data['search_results'] = $res;
		$data['loadmore'] = $loadmore;
		return $data;
		//return json_decode($curl_response);
    }
    
    public function wifi_first_free_session_user($start_date, $end_date, $locationid,$device_type){
	$requestData = array(
			'locationid' => $locationid,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'device_type' => $device_type,
			
		);
		$service_url = $this->api_url."wifi_first_free_session_user";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce = json_decode($curl_response);
		$html = "";
		$html .="<div class='table-responsive'><table class='table table-striped table-bordered'><tr><th>Screen Name</th><th>Mobile</th><th>Email</th><th>No Of
        Session</th><th>Gender</th><th>Age Group</th></tr>";
		if($responce->resultCode == '1'){
		    foreach($responce->users_detail as $users_detail_data){
			 $html .= "<tr><td>".$users_detail_data->screen_name."</td><td>".$users_detail_data->mobile."</td><td>".$users_detail_data->email
                        ."</td><td>".$users_detail_data->no_of_session."</td><td>".$users_detail_data->gender.
                        "</td><td>".$users_detail_data->age_group."</td></tr>";
		    }
		}else{
		    $html .= "<tr><td>No USER Found</td></tr>";
		}
		$html .= "</table></div>";
		return $html;
    }
    
    public function wifi_session_user($locationid, $start_date, $end_date, $via){
	$requestData = array(
			'locationid' => $locationid,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'via' => $via
			
		);
		$service_url = $this->api_url."wifi_session_user";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce = json_decode($curl_response);
		$html = "";
		$html .="<div class='table-responsive'><table class='table table-striped table-bordered'><tr><th>Screen Name</th><th>Mobile</th><th>Email</th><th>No Of
        Session</th><th>Gender</th><th>Age Group</th></tr>";
		if($responce->resultCode == '1'){
		    foreach($responce->users_detail as $users_detail_data){
			 $html .= "<tr><td>".$users_detail_data->screen_name."</td><td>".$users_detail_data->mobile."</td><td>".$users_detail_data->email
                        ."</td><td>".$users_detail_data->no_of_session."</td><td>".$users_detail_data->gender.
                        "</td><td>".$users_detail_data->age_group."</td></tr>";
		    }
		}else{
		    $html .= "<tr><td>No USER Found</td></tr>";
		}
		$html .= "</table></div>";
		return $html;
    }
    
    
    public function wifi_session_report_locationvise($start_date, $end_date,$locationid){
	 
        $requestData = array(
			'isp_uid' => $this->isp_uid,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'locationid' => $locationid
			
		);
		$service_url = $this->api_url."wifi_session_report_locationvise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		 return json_decode($curl_response);
    }
    public function wifi_first_free_session_user_locationvise( $locationid, $visit_date,$device_type){
	$requestData = array(
			'locationid' => $locationid,
			'visit_date' => $visit_date,
			'device_type' => $device_type
			
		);
		$service_url = $this->api_url."wifi_first_free_session_user_locationvise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce = json_decode($curl_response);
		$html = "";
		$html .="<div class='table-responsive'><table class='table table-striped table-bordered'><tr><th>Screen Name</th><th>Mobile</th><th>Email</th><th>No Of
        Session</th><th>Gender</th><th>Age Group</th></tr>";
		if($responce->resultCode == '1'){
		    foreach($responce->users_detail as $users_detail_data){
			 $html .= "<tr><td>".$users_detail_data->screen_name."</td><td>".$users_detail_data->mobile."</td><td>".$users_detail_data->email
                        ."</td><td>".$users_detail_data->no_of_session."</td><td>".$users_detail_data->gender.
                        "</td><td>".$users_detail_data->age_group."</td></tr>";
		    }
		}else{
		    $html .= "<tr><td>No USER Found</td></tr>";
		}
		$html .= "</table></div>";
		return $html;
    }
     public function wifi_session_user_locationvise($locationid, $via, $visit_date){
	$requestData = array(
			'locationid' => $locationid,
			'visit_date' => $visit_date,
			'via' => $via
			
			
		);
		$service_url = $this->api_url."wifi_session_user_locationvise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce = json_decode($curl_response);
		$html = "";
		$html .="<div class='table-responsive'><table class='table table-striped table-bordered'><tr><th>Screen Name</th><th>Mobile</th><th>Email</th><th>No Of
        Session</th><th>Gender</th><th>Age Group</th></tr>";
		if($responce->resultCode == '1'){
		    foreach($responce->users_detail as $users_detail_data){
			 $html .= "<tr><td>".$users_detail_data->screen_name."</td><td>".$users_detail_data->mobile."</td><td>".$users_detail_data->email
                        ."</td><td>".$users_detail_data->no_of_session."</td><td>".$users_detail_data->gender.
                        "</td><td>".$users_detail_data->age_group."</td></tr>";
		    }
		}else{
		    $html .= "<tr><td>No USER Found</td></tr>";
		}
		$html .= "</table></div>";
		return $html;
    }
    
   
    public function wifi_session_failure_report($start_date, $end_date, $state_id, $city_id, $limit, $offset){
	 $requestData = array(
			'isp_uid' => $this->isp_uid,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'state_id' => $state_id,
			'city_id' => $city_id,
			'limit' => $limit,
			'offset' => $offset
			
		);
		$service_url = $this->api_url."wifi_session_failure_report";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		$res = '';
		$loadmore = 0;
		if(count($result) > 0){
		    foreach($result as $row){
			$res .= "<tr>";
			
			$res .= "<td>".$row->location_name."</td>";
			$res .= "<td>".$row->total_cp_impression."</td>";
			$res .= "<td>".$row->unique_user."</td>";
			$res .= "<td>".$row->total_new_user."</td>";
			$res .= "<td>".$row->total_otp_send."</td>";
			$res .= "<td>".$row->total_otp_verified."</td>";
			$res .= "<td>".$row->total_session."</td>";
			
			$res .= "</tr>";
		    }
		    $loadmore = 1;
		}
		else{
		    $loadmore = 0;
		    $res .= '<tr><td colspan="7" style="text-align:center">No More Result Found !!</td></tr>';
		}
		$data['limit'] = $limit;
		$data['offset'] = $limit+$offset;
		$data['total_record'] = count($result);
		$data['search_results'] = $res;
		$data['loadmore'] = $loadmore;
		return $data;
    }
    
    
    public function session_report_export_to_excel($start_date, $end_date, $state_id, $city_id){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'state_id' => $state_id,
			'city_id' => $city_id,
			'limit' => '2000',
			'offset' => '0'
			
		);
		$service_url = $this->api_url."wifi_session_report";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		
	    $this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(0);
		$styleArray = array(
		    'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'name'  => 'Tahoma',
			'size' => 14
		));
		//Set sheet style
		$styleArray1 = array(
		    'font'  => array(
		    'bold'  => false,
		    'color' => array('rgb' => '000000'),
		    'name'  => 'Tahoma',
		    'size' => 12
		));
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		
		
		$objPHPExcel->getActiveSheet()->setTitle("Session Report");
		//Set sheet columns Heading
		$objPHPExcel->getActiveSheet()->setCellValue('A1','S.No');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('B1','Location');
		$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('C1','Total Impression');
		$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('D1','Unique Impression');
		$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('E1','Total Laptop Session ');
		$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('F1','Total Android Session');
		$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('G1','Total Iphone Session');
		$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('H1','Total Windows Phone Session');
		$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('I1','Total Session');
		$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('J1','Total Laptop User');
		$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('K1','Total Android User');
		$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('L1','Total Iphone User');
		$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('M1','Total Windows Phone User');
		$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('N1','Total User');
		$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($styleArray);
		$i = 1;
		$j = 2;// excel row number
		foreach($result as $row){
		    $total_session = $row->total_session_windows_phone+$row->total_session_laptop+$row->total_session_android+$row->total_session_iphone;
		    $total_user = $row->total_user_windows_phone+$row->total_user_laptop+$row->total_user_android+$row->total_user_iphone;
		    $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$i);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$row->location_name);
                    $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$row->total_cp_impression);
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$row->unique_user);
                    $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$row->total_session_laptop);
                    $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$row->total_session_android);
                    $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$row->total_session_iphone);
                    $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('H'.$j,$row->total_session_windows_phone);
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('I'.$j,$total_session);
                    $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('J'.$j,$row->total_user_laptop);
                    $objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('K'.$j,$row->total_user_android);
                    $objPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('L'.$j,$row->total_user_iphone);
                    $objPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('M'.$j,$row->total_user_windows_phone);
                    $objPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('N'.$j,$total_user);
                    $objPHPExcel->getActiveSheet()->getStyle('N'.$j)->applyFromArray($styleArray1);
		    $i++;
		    $j++;
		}
                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('assets/session_report.xlsx');
		//return json_decode($curl_response);
		//return json_decode($curl_response);
    }
	public function export_to_excel_trafic_user_detail($start_date, $end_date, $state_id, $city_id){
		$this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(0);
		$styleArray = array(
		    'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'name'  => 'Tahoma',
			'size' => 14
		));
		//Set sheet style
		$styleArray1 = array(
		    'font'  => array(
		    'bold'  => false,
		    'color' => array('rgb' => '000000'),
		    'name'  => 'Tahoma',
		    'size' => 12
		));
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setTitle("User Report");
		//Set sheet columns Heading
		$objPHPExcel->getActiveSheet()->setCellValue('A1','Location Name');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('B1','User Name');
		$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('C1','User Mobile');
		$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('D1','User Email');
		$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('E1','Session Date');
		$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
		$from = date('Y-m-d', strtotime($start_date));
		$to = date('Y-m-d', strtotime($end_date));
		$this->db->select('id, location_name,nasipaddress')->from('wifi_location');
		$this->db->where(array("isp_uid" => $this->isp_uid, "status" => '1', "is_deleted" => "0"));
		if($state_id != ''){
		    $this->db->where("state_id" , $state_id);
		}
		if($city_id != ''){
		    $this->db->where("city_id" , $city_id);
		}
		$query = $this->db->get();
		$i = 1;
		$j = 2;
		foreach($query->result() as $row){
			$this->db->select("su.firstname, su.lastname, su.email, su.mobile, wufs.session_date")->from("wifi_user_free_session as wufs");
			$this->db->where(array("wufs.location_id" => $row->id));
			$this->db->where("date(wufs.session_date) BETWEEN '$from' AND '$to'");
			$this->db->join('sht_users as su', 'wufs.userid = su.uid', 'left');
			$this->db->order_by("wufs.session_date");
			$check_user = $this->db->get();
			foreach($check_user->result() as $chech_user_row){
			    $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$row->location_name);
			    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
			    $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$chech_user_row->firstname." ".$chech_user_row->lastname);
			    $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
			    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$chech_user_row->mobile);
			    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
			    $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$chech_user_row->email);
			    $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
			    $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,date("d-m-Y", strtotime($chech_user_row->session_date)));
			    $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
			    $i++;
			    $j++;
			}
			
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('assets/traffic_user_list.xlsx');
	}
}

?>