<?php

class Nas_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->api_url = APIPATH."/naswifi/";
    }

     public function curlhit($url,$requestData)
    {
      
       $service_url = $url;
        $curl = curl_init($service_url);
        $requestData = $requestData;
        $data_request = json_encode($requestData);
        $curl_post_data = array("requestData" => $data_request);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        return json_decode($curl_response);
      
    }

    public function listing_nas() {
        
          $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin
		);
        
        $service_url = $this->api_url."listing_nas";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
        //$condarr=array('status'=>1,"is_deleted"=>0);
       
    }
    
    public function nas_type()
    {
	   $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin
		);
	//echo json_encode($requestData);die;
        
        $service_url = $this->api_url."nas_type";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
    }
    
    
    
    public function getstatename($stateid)
    {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'stateid'=>$stateid
		);
        
        $service_url = $this->api_url."getstatename";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
    }
    
   
    
     public function getcityname($stateid, $cityid)
    {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'stateid'=>$stateid,
            'cityid'=>$cityid
		);
        
        $service_url = $this->api_url."getcityname";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
    }
    
        
     public function getzonename($zoneid)
    {
         $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'zoneid'=>$zoneid,
            
		);
        
        $service_url = $this->api_url."getzonename";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
    }
    
    public function checknas_exist() {
         $postdata = $this->input->post();
          $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'postdata'=>$postdata ,
            
		);
       // echo json_encode($requestData); die;
         $service_url = $this->api_url."checknas_exist";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
        
        
    }
    
     public function checknasip_exist() {
          $postdata = $this->input->post();
          $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'postdata'=>$postdata ,
            
		);
       // echo json_encode($requestData); die;
         $service_url = $this->api_url."checknasip_exist";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
      
    }

    public function ping($host) {
         $postdata = $this->input->post();
          $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'host'=>$host ,
            
		);
         $service_url = $this->api_url."ping";
        $data=$this->curlhit($service_url,$requestData);
       
        return $data;
       
    }
    
    
       public function viewall_search_results() {
           $postdata=$this->input->post();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'postdata'=>$postdata
		);
     //   echo json_encode($requestData); die;
        $service_url = $this->api_url."viewall_search_results";
        $data=$this->curlhit($service_url,$requestData);
       
       echo json_encode($data);
       
    }

    public function add_nas_data() {
        
        $postdata=$this->input->post();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'postdata'=>$postdata
		);
     //   echo json_encode($requestData); die;
        $service_url = $this->api_url."add_nas_data";
        $data=$this->curlhit($service_url,$requestData);
    //   echo "<pre>"; print_R($data);die;
      return $data;

      
    }
    public function add_nas_data_dynamic() {
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $requestData = array(
			'isp_uid' => $isp_uid
		    );
	$service_url = $this->api_url."add_nas_data_dynamic";
        $data=$this->curlhit($service_url,$requestData);
	return $data;
    }

 

    // public function 

    public function delete_nas() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1);
        $this->db->update(SHTNAS, $tabledata, array('nasid' => $postdata['nasid']));
        return $postdata['nasid'];
    }

    public function edit_nas() {
        
         $postdata=$this->input->post();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'postdata'=>$postdata
		);
     //   echo json_encode($requestData); die;
        $service_url = $this->api_url."edit_nas";
        $data=$this->curlhit($service_url,$requestData);
       
      echo json_encode($data);
        
      
    }
    
        public function add_zonedata(){
            
            
             $postdata=$this->input->post();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'postdata'=>$postdata
		);
     //   echo json_encode($requestData); die;
        $service_url = $this->api_url."add_zonedata";
        $data=$this->curlhit($service_url,$requestData);
       
      echo json_encode($data);
           
	}
        
           public function add_citydata(){
                 $postdata=$this->input->post();
             $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];
         $dept_id= $sessiondata['dept_id'];
          $super_admin= $sessiondata['super_admin'];
        $requestData = array(
			'isp_uid' => $isp_uid,
                        'dept_id' => $dept_id,
            'super_admin'=>$super_admin,
            'postdata'=>$postdata
		);
       // echo json_encode($requestData); die;
        $service_url = $this->api_url."add_citydata";
        $data=$this->curlhit($service_url,$requestData);
      // echo "<pre>"; print_R($data); die;
      echo json_encode($data);
	}

   

    

   

 

}

?>