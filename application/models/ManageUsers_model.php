<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ManageUsers_model extends CI_Model{
    
    private $db2;
    public function __construct(){
        $this->db2 = $this->load->database('db2', TRUE);
    }
    
    public function list_team_data(){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        
        $gen = '';
        $pubusersQ = $this->db2->query("SELECT id, name, lastname, email, phone, orig_pwd FROM sht_isp_public_users WHERE isp_uid='".$isp_uid."' AND status='1' AND publicuser='0' AND is_deleted='0' ORDER BY id DESC");
        if($pubusersQ->num_rows() > 0){
            $i = 1;
            foreach($pubusersQ->result() as $puobj){
                $gen .= '<tr><td>'.$i.'</td><td>'.$puobj->name.' '.$puobj->lastname.'</td><td>'.$puobj->email.'</td><td>'.$puobj->phone.'</td><td class="pwdupd'.$puobj->id.'">'.$puobj->orig_pwd.'</td><td class="mui--text-center"><a href="javascript:void(0);" class="changepwd"  rel="'. $puobj->id.'" >Change password</a>&nbsp;&nbsp;&nbsp;<a href="'. base_url()."manageUsers/edit_users/".$puobj->id.'">Edit</a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="deletuser"  rel="'.$puobj->id.'" >Delete</a></td> </tr>';
                $i++;
            }
        }else{
            $gen = '<tr><td colspan="8">No Team Member Added</td></tr>';
        }
        
        return $gen;
    }
    
    public function getispname($isp_uid){
        $ispQ = $this->db2->query("SELECT isp_name FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
        if($ispQ->num_rows() > 0){
            return $ispQ->row()->isp_name;
        }
    }
    
    public function add_team_data_todb(){
        $postdata=$this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $isp_name = $this->getispname($isp_uid);
        
        if(isset($postdata['user_id']) && $postdata['user_id']!=''){
            
            $tabledata = array("name"=>$postdata['firstname'], "lastname"=>$postdata['lastname'], "phone"=>$postdata['phone'], "status"=>"1", "is_deleted"=>"0", "added_on"=>date("Y-m-d H:i:s"), "isp_uid"=>$isp_uid, "isp_name" => $isp_name, "publicuser" => '0');    
            $this->db2->update('sht_isp_public_users',$tabledata,array("id"=>$postdata['user_id']));
            return $postdata['user_id'];
        }else {
            $salt = random_string('alnum',5);
            $password = $postdata['phone'];
            //$this->email_credential($postdata);
            $encrypt_password = md5($password.$salt);
            $tabledata = array("username"=>$postdata['username'], "name"=>$postdata['firstname'], "lastname"=>$postdata['lastname'],"email"=>$postdata['username'], "password"=>$encrypt_password, "salt"=>$salt, "orig_pwd"=>$password, "phone"=>$postdata['phone'],"status"=>"1", "is_deleted"=>"0", "added_on"=>date("Y-m-d H:i:s"), "isp_uid"=>$isp_uid, "isp_name" => $isp_name, "publicuser" => '0');   
            $this->db2->insert('sht_isp_public_users',$tabledata);
            return $this->db2->insert_id();
        }
    }
    
    public function check_user_exist($str){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $condarr = array('username' => $str, "status" => '1', "is_deleted" => '0',"isp_uid" => $isp_uid);
        $query = $this->db2->get_where('sht_isp_public_users',$condarr);
        
        if($query->num_rows()>0){
            return 0;
        }else {
            $chkteamquery = $this->db2->get_where('sht_isp_users',$condarr);
            if($chkteamquery->num_rows()>0){
                return 0;
            }else {
                return 1;
            }
        }
    }
    
    public function team_data_foredit($user_id){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        
        $data = array();
        $pubusersQ = $this->db2->query("SELECT id, name, lastname, email, phone, orig_pwd FROM sht_isp_public_users WHERE isp_uid='".$isp_uid."' AND id='".$user_id."'");
        if($pubusersQ->num_rows() > 0){
            $data = $pubusersQ->row_array();
        }
        return $data;
    }
    
    public function update_userpwd(){
        $data = array();
        $postdata = $this->input->post();
        $query = $this->db2->query("select salt,username,name,lastname from sht_isp_public_users where id='".$postdata['teamid']."'");
        if($query->num_rows() > 0){
            $rowarr = $query->row_array();
            $encrypt_password = md5($postdata['changepwd'].$rowarr['salt']);
            $tabledata = array("orig_pwd" => $postdata['changepwd'],"password" => $encrypt_password);
            $this->db2->update("sht_isp_public_users",$tabledata,array("id"=>$postdata['teamid']));
            $data['resultcode'] = '1';
            $data['origpwd'] = $postdata['changepwd'];
        }else{
            $data['resultcode'] = '0';
        }
        return $data;
    }
    
    public function delete_user(){
        $postdata=$this->input->post();
        $tabledata = array("is_deleted"=>"1");
        $this->db2->update('sht_isp_public_users', $tabledata, array('id' => $postdata['userid']));
        return  $postdata['userid'];
    }
    
    public function get_allstates($stateid = ''){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $countryQ = $this->db2->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid = '".$isp_uid."'");
        $country_id = $countryQ->row()->country_id;
        //get state 
		$get_state_query = $this->db->query("select state_id from wifi_location where isp_uid = '$isp_uid' AND is_deleted = '0'");
		$state_ids = array();
		foreach($get_state_query->result() as $row_state){
		    $state_ids[] = $row_state->state_id;
		}
                $state_ids = '"'.implode('", "', $state_ids).'"';
        $gen = '';
        $stateQ = $this->db2->query("SELECT * FROM sht_states where  id IN ($state_ids) order by state");
        $num_rows = $stateQ->num_rows();
        if($num_rows > 0){
            foreach($stateQ->result() as $stobj){
                $sel = '';
                if($stateid == $stobj->id){
                    $sel = 'selected="selected"';
                }
                $gen .= '<option value="'.$stobj->id.'" '.$sel.'>'.$stobj->state.'</option>';
            }
        }
        return $gen;
    }
    
    public function add_regional_data_todb(){
        $postdata=$this->input->post();
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        $isp_name = $this->getispname($isp_uid);
        $regional_state = implode(',' , $postdata['regional_state']);
        
        if(isset($postdata['user_id']) && $postdata['user_id']!=''){
            
            $tabledata = array("name"=>$postdata['firstname'], "lastname"=>$postdata['lastname'], "phone"=>$postdata['phone'], "status"=>"1", "is_deleted"=>"0", "added_on"=>date("Y-m-d H:i:s"), "isp_uid"=>$isp_uid, "isp_name" => $isp_name, "assigned_region" => $regional_state, 'super_admin' => '0', 'publicuser' => '1');    
            $this->db2->update('sht_isp_public_users',$tabledata,array("id"=>$postdata['user_id']));
            return $postdata['user_id'];
        }else {
            $salt = random_string('alnum',5);
            $password = $postdata['phone'];
            //$this->email_credential($postdata);
            $encrypt_password = md5($password.$salt);
            $tabledata = array("username"=>$postdata['username'], "name"=>$postdata['firstname'], "lastname"=>$postdata['lastname'],"email"=>$postdata['username'], "password"=>$encrypt_password, "salt"=>$salt, "orig_pwd"=>$password, "phone"=>$postdata['phone'],"status"=>"1", "is_deleted"=>"0", "added_on"=>date("Y-m-d H:i:s"), "isp_uid"=>$isp_uid, "isp_name" => $isp_name, "assigned_region" => $regional_state, 'super_admin' => '0', 'publicuser' => '1');   
            $this->db2->insert('sht_isp_public_users',$tabledata);
            return $this->db2->insert_id();
        }
    }
    
    public function getstatename($stateid){
        $stateQ = $this->db2->query("SELECT * FROM sht_states WHERE id='".$stateid."'");
        $num_rows = $stateQ->num_rows();
        if($num_rows > 0){
            return $stateQ->row()->state;
        }
    }
    
    public function groupstatename($statearr){
        $gen = '';
        $statearr = explode(',' , $statearr);
        foreach($statearr as $sid){
            $stateQ = $this->db2->query("SELECT * FROM sht_states WHERE id='".$sid."'");
            $num_rows = $stateQ->num_rows();
            if($num_rows > 0){
                $gen .= $stateQ->row()->state .', ';
            }
        }
        $allstates = rtrim($gen,", ");
        return $allstates;
    }
    
    public function regionalteam_data(){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        
        $gen = '';
        $pubusersQ = $this->db2->query("SELECT id, name, lastname, email, phone, orig_pwd, assigned_region FROM sht_isp_public_users WHERE isp_uid='".$isp_uid."' AND status='1' AND is_deleted='0' AND assigned_region != '0' ORDER BY id DESC");
        if($pubusersQ->num_rows() > 0){
            $i = 1;
            foreach($pubusersQ->result() as $puobj){
                if(strpos($puobj->assigned_region , ',') > 0){
                    $statesnames = $this->groupstatename($puobj->assigned_region);
                }else{
                    $statesnames = $this->getstatename($puobj->assigned_region);
                }
                $gen .= '<tr><td>'.$i.'</td><td>'.$puobj->name.' '.$puobj->lastname.'</td><td>'.$puobj->email.'</td><td>'.$puobj->phone.'</td><td class="pwdupd'.$puobj->id.'">'.$puobj->orig_pwd.'</td><td>'.$statesnames.'</td><td class="mui--text-center"><a href="javascript:void(0);" class="changepwd"  rel="'. $puobj->id.'" >Change password</a>&nbsp;&nbsp;&nbsp;<a href="'. base_url()."manageUsers/edit_regionalusers/".$puobj->id.'">Edit</a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="deletuser"  rel="'.$puobj->id.'" >Delete</a></td> </tr>';
                $i++;
            }
        }else{
            $gen = '<tr><td colspan="8">No Team Member Added</td></tr>';
        }
        
        return $gen;
    }
    
    public function regionalteam_data_foredit($user_id){
        $sessiondata = $this->session->userdata('isp_session');
        $isp_uid = $sessiondata['isp_uid'];
        
        $data = array();
        $pubusersQ = $this->db2->query("SELECT id, name, lastname, email, phone, orig_pwd, assigned_region FROM sht_isp_public_users WHERE isp_uid='".$isp_uid."' AND id='".$user_id."'");
        if($pubusersQ->num_rows() > 0){
            $data = $pubusersQ->row_array();
        }
        return $data;
    }
}

?>