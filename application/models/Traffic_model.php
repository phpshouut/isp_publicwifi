<?php

class Traffic_model extends CI_Model {

    public function total_user($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'state_id' => $state_id,
			'city_id' => $city_id,
		);
		$service_url = $this->api_url."traffic_summary_total_user";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
                
    }
    public function location_wise_total_user($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id, $limit, $offset){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'limit' => $limit,
			'offset' => $offset,
			'state_id' => $state_id,
			'city_id' => $city_id,
		);
		$service_url = $this->api_url."traffic_summary_location_wise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		$res = '';
		$loadmore = 0;
		if(count($result) > 0){
		    $i = $offset+1;
		    foreach($result as $row){
			$res .= "<tr>";
			$res .= "<td>".$i."</td>";
			$res .= "<td>".$row->location_name."</td>";
			$res .= "<td>".$row->nasipaddress."</td>";
			$res .= "<td>".$row->totaluser."</td>";
			$res .= "<td>".$row->totalunique."</td>";
			$res .= "<td>".$row->totalrepeat."</td>";
			$res .= "</tr>";
			$i++;
		    }
		    $loadmore = 1;
		}
		else{
		    $loadmore = 0;
		    $res .= '<tr><td colspan="6" style="text-align:center">No More Result Found !!</td></tr>';
		}
		$data['limit'] = $limit;
		$data['offset'] = $limit+$offset;
		$data['total_record'] = count($result);
		$data['search_results'] = $res;
		$data['loadmore'] = $loadmore;
		return $data;
		//return json_decode($curl_response);
                
    }
     public function total_session($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'state_id' => $state_id,
			'city_id' => $city_id,
		);
		$service_url = $this->api_url."traffic_summary_total_session";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
                
    }
        public function location_wise_total_session($from, $to, $gender, $age_group, $time_slot, $locations, $limit, $offset,$state_id,$city_id){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'limit' => $limit,
			'offset' => $offset,
			'state_id' => $state_id,
			'city_id' => $city_id,
		);
		$service_url = $this->api_url."traffic_session_summary_location_wise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		 $result = json_decode($curl_response);
		$res = '';
		$loadmore = 0;
		if(count($result) > 0){
		    $i = $offset+1;
		    foreach($result as $row){
			$res .= "<tr>";
			$res .= "<td>".$i."</td>";
			$res .= "<td>".$row->location_name."</td>";
			$res .= "<td>".$row->nasipaddress."</td>";
			$res .= "<td>".$row->totaluser."</td>";
			$res .= "<td>".$row->totalunique."</td>";
			$res .= "<td>".$row->totalrepeat."</td>";
			$res .= "</tr>";
			$i++;
		    }
		    $loadmore = 1;
		}
		else{
		    $loadmore = 0;
		    $res .= '<tr><td colspan="6" style="text-align:center">No More Result Found !!</td></tr>';
		}
		$data['limit'] = $limit;
		$data['offset'] = $limit+$offset;
		$data['total_record'] = count($result);
		$data['search_results'] = $res;
		$data['loadmore'] = $loadmore;
		return $data;
		//return json_decode($curl_response);
                
    }
    
    public function user_list_export_to_excel($from, $to, $gender, $age_group, $time_slot, $locations,$state_id,$city_id){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'limit' => '2000',
			'offset' => '0',
			'state_id' => $state_id,
			'city_id' => $city_id,
		);
		$service_url = $this->api_url."traffic_summary_location_wise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
               // print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		
	    
	    $this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(0);
		$styleArray = array(
		    'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'name'  => 'Tahoma',
			'size' => 14
		));
		//Set sheet style
		$styleArray1 = array(
		    'font'  => array(
		    'bold'  => false,
		    'color' => array('rgb' => '000000'),
		    'name'  => 'Tahoma',
		    'size' => 12
		));
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		
		
		$objPHPExcel->getActiveSheet()->setTitle("Location User Report");
		//Set sheet columns Heading
		$objPHPExcel->getActiveSheet()->setCellValue('A1','S.No');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('B1','Location');
		$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('C1','IP');
		$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('D1','Total User');
		$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('E1','Unique User');
		$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('F1','Repeat User');
		$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
		$i = 1;
		$j = 2;// excel row number
		foreach($result as $row){
		    $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$i);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$row->location_name);
                    $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$row->nasipaddress);
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$row->totaluser);
                    $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$row->totalunique);
                    $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$row->totalrepeat);
                    $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
		    $i++;
		    $j++;
		}
                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('assets/user_list.xlsx');
		//return json_decode($curl_response);
                
    }
    
        public function traffic_list_export_to_excel($from, $to, $gender, $age_group, $time_slot, $locations, $state_id,$city_id){
        $requestData = array(
			'isp_uid' => $this->isp_uid,
                        'from' => $from,
			'to' => $to,
			'gender' => $gender,
			'age_group' => $age_group,
			'time_slot' => $time_slot,
                        'locations' => $locations,
			'limit' => '2000',
			'offset' => '0',
			'state_id' => $state_id,
			'city_id' => $city_id,
		);
		$service_url = $this->api_url."traffic_session_summary_location_wise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		
		$this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(0);
		$styleArray = array(
		    'font'  => array(
			'bold'  => false,
			'color' => array('rgb' => '000000'),
			'name'  => 'Tahoma',
			'size' => 14
		));
		//Set sheet style
		$styleArray1 = array(
		    'font'  => array(
		    'bold'  => false,
		    'color' => array('rgb' => '000000'),
		    'name'  => 'Tahoma',
		    'size' => 12
		));
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		
		
		$objPHPExcel->getActiveSheet()->setTitle("Location User Report");
		//Set sheet columns Heading
		$objPHPExcel->getActiveSheet()->setCellValue('A1','S.No');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('B1','Location');
		$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('C1','IP');
		$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('D1','Total Session');
		$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('E1','New Session');
		$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->setCellValue('F1','Returning Session');
		$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
		$i = 1;
		$j = 2;// excel row number
		foreach($result as $row){
		    $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$i);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$row->location_name);
                    $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$row->nasipaddress);
                    $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$row->totaluser);
                    $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$row->totalunique);
                    $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
		    $objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$row->totalrepeat);
                    $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
		    $i++;
		    $j++;
		}
                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('assets/traffic_list.xlsx');
		//return json_decode($curl_response);
                
    }
    
    
    


}

?>