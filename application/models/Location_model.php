<?php
class Location_model extends CI_Model{
	public function __construct() {
		parent::__construct();
		$this->DB2 = $this->load->database('db2', TRUE);
	}
	// get total location in dashboard active and inactive
	public function location_dashboard(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."location_count";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function location_dashboard_search($search_val){
		
		
		$requestData = array(
			'search_val' => $search_val,
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."location_dashboard_search";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		$content .= '<div class="row">';
		$content .= '<div class="search-dropdown">
                                                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px;">
                                                            <h4>'.$responce_data->totalrecord.' <small>Results</small></h4>
                                                         </div>
                                                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right: 0px;">
                                                            <h4><span>
							       <a href="#" onclick="view_all()">View All<a>
                                                               </span>
                                                            </h4>
                                                         </div>
                                                      </div>';
		if($responce_data->totalrecord > 0){
			foreach($responce_data->locations as $record){
				$content .= '<a href="'.base_url().'location/edit_location?id='.$record->location_uid.'"><div class="search-dropdown">
                                                         <h5><i class="fa fa-user" aria-hidden="true"></i> &nbsp;'.$record->location_name.' <small> ('.$record->location_uid.')</small></h5>
                                                         <samp>'.$record->contact_person_name.', '.$record->mobile_number.', '.$record->city.'</samp>
                                                      </div><a>';
			}
		}
		
		$content .= '</div>';
		return  $content;
	}
	
	// get location list based on location type
	public function location_list($type, $state_id, $city_id,$search_type_filter){
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		else{
			$type = '';
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_type' => $type,
			'state' => $state_id,
			'city' => $city_id,
			'search_pattern' => $search_type_filter
		);
		$service_url = $this->api_url."location_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}


	// get state list
	public function state_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."state_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function state_list_add_city($state_id = ''){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."state_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		//echo "<pre>";print_r($responce_data);die;
		$content = '<option value = "">Select State</option>';
		foreach($responce_data->state as $responce_data1){
			$sel = '';
			if($responce_data1->state_id == $state_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->state_id.'">'.$responce_data1->state_name.'</option>';
		}
		return $content;
	}
	// get city list
	public function city_list($state_id, $city_id = ''){
		$requestData = array(
			'state_id' => $state_id
		);
		$service_url = $this->api_url."city_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select City</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->city_id == $city_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->city_id.'">'.$responce_data1->city_name.'</option>';
		}
		return $content;
	}
	// get zone list
	public function zone_list($city_id, $zone_id = ''){
		$requestData = array(
			'city_id' => $city_id,
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."zone_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select zone</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->zone_id == $zone_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->zone_id.'">'.$responce_data1->zone_name.'</option>';
		}
		return $content;
	}
	//get the plan listing based on type
	public function plan_list($plan_type, $plan_id = ''){
		$requestData = array(
			'plan_type' => $plan_type,
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."plan_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option data-plan_type = "" value = ""> < New Plan > </option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->srvid == $plan_id){
				$sel = "selected";
			}
			$content .= '<option data-plan_type="'.$responce_data1->plan_type.'" '.$sel.' value = "'.$responce_data1->srvid.'">'.$responce_data1->srvname.'</option>';
		}
		return $content;
	}
	// add location view open time get autogenerated location id
	public function add_location_view(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."location_autogenerate_id";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	// add or update location
	public function add_location(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'created_location_id' => $this->input->post("created_location_id"),
			'location_type' => $this->input->post("location_type"),
			'location_name' => $this->input->post("location_name"),
			'geo_address' => $this->input->post("geo_address"),
			'placeid' => $this->input->post("placeid"),
			'lat' => $this->input->post("lat"),
			'long' => $this->input->post("long"),
			'address1' => $this->input->post("address1"),
			'address2' => $this->input->post("address2"),
			'pin' => $this->input->post("pin"),
			'state' => $this->input->post("state"),
			'city' => $this->input->post("city"),
			'zone' => $this->input->post("zone"),
			'location_id' => $this->input->post("location_id"),
			'contact_person_name' => $this->input->post("contact_person_name"),
			'email_id' => $this->input->post("email_id"),
			'mobile_no' => $this->input->post("mobile_no"),
			'enable_channel' => $this->input->post("enable_channel"),
			'enable_nas_down_notification' => $this->input->post("enable_nas_down_notification"),
			'hashtags' => $this->input->post("hashtags"),
			'menu_display_option' => $this->input->post("menu_display_option"),
			'enable_payrolldebit' => $this->input->post('enable_payrolldebit'),
			'return_policy_number' => $this->input->post('return_policy_number'),
			'location_display_name' => $this->input->post('location_display_name'),
			'location_price_surge' => $this->input->post('location_price_surge')
		);
		
		$created_location_id = $this->input->post("created_location_id");
		$surgefactor = $this->input->post('location_price_surge');
		$this->update_items_price_with_surgefactor($created_location_id, $surgefactor);
		
		//return $requestData;die;
		$service_url = $this->api_url."add_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	// add or update captive portal
	public function add_captive_portal(){
		$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/cp_brand_image';
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
		$base_url = 'assets/cp_brand_image/';
		$original="";
                        $small="";
                        $logo="";
			$image_name = $this->input->post("image_name");
			$image_src = $this->input->post("image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					
					$success = file_put_contents($file, $data_img);
					$requestData = array(
						'location_uid' => $this->input->post("location_uid"),
					);
					$service_url = $this->api_url."location_logo_name";
					$curl = curl_init($service_url);
					$requestData = $requestData;
					$data_request = json_encode($requestData);
					//print_r($data_request);die;
					$curl_post_data = array("requestData" => $data_request);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$curl_response = curl_exec($curl);
					curl_close($curl);
					$image_responce = json_decode($curl_response);
					if(isset($image_responce->original_name) && $image_responce->original_name != ''){
						$newname = $image_responce->original_name;
						$newname1=date("Ymdhisv").rand().".".$file_ext; 
					}else{
						$newname=date("Ymdhisv").rand().".".$file_ext;
						$newname1=date("Ymdhisv").rand().".".$file_ext; 
					}
					//rename($base_url.$filename,$base_url.'original/'.$newname);
					rename($base_url.$filename,$base_url.'original/'.$newname1);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					//$config1['source_image'] = $base_url . 'original/'.$newname;
					$config1['source_image'] = $base_url . 'original/'.$newname1;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname;
                        
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
                            
					$this->image_lib->clear();
					 $config2['image_library'] = 'gd2';
					 //$config2['source_image'] = $base_url . 'original/'.$newname;
					 //$config2['new_image'] = $base_url . 'small/'."600_".$newname;
					 
					 $config2['source_image'] = $base_url . 'original/'.$newname1;
					 $config2['new_image'] = $base_url . 'small/'."600_".$newname1;
                          
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					  $this->image_lib->initialize($config2);
					  $this->image_lib->resize();
                              
					/*$original=$newname;
					 $small="600_".$newname;
					 $logo="300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname;
			   
			$famazonname = AMAZONPATH.'isp_location_logo/original/'.$newname;
			$famazonname1 = AMAZONPATH.'isp_location_logo/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'isp_location_logo/small/600_'. $newname;*/
					
					$original=$newname1;
					 $small="600_".$newname1;
					 $logo="300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname1;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname1;
			   
			$famazonname = AMAZONPATH.'isp_location_logo/original/'.$newname1;
			$famazonname1 = AMAZONPATH.'isp_location_logo/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'isp_location_logo/small/600_'. $newname1;
	  
                          
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
			    unlink($fname);
			unlink($fname1);
			unlink($fname2);
					 
					
				}	
			}
			$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/cp_brand_image';
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
		$base_url = 'assets/retail_background_image/';
		$original_retail="";
                        $small_retail="";
                        $logo_retail="";
			$image_name = $this->input->post("background_image_name");
			$image_src = $this->input->post("background_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					
					rename($base_url.$filename,$base_url.'original/'.$newname);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = $base_url . 'original/'.$newname;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname;
                        
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
                            
					$this->image_lib->clear();
					 $config2['image_library'] = 'gd2';
					 $config2['source_image'] = $base_url . 'original/'.$newname;
					 $config2['new_image'] = $base_url . 'small/'."600_".$newname;
                          
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					  $this->image_lib->initialize($config2);
					  $this->image_lib->resize();
                              
					$original_retail = $newname;
					 $small_retail = "600_".$newname;
					 $logo_retail = "300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname;
			   
			$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
			$famazonname1 = AMAZONPATH.'retail_background_image/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'retail_background_image/small/600_'. $newname;
	  
                          
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
			    unlink($fname);
			unlink($fname1);
			unlink($fname2);
					 
					
				}	
			}
			
			$base_url = 'assets/retail_background_image/';
			$original_slider1="";
			$image_name = $this->input->post("slider1_image_name");
			$image_src = $this->input->post("slider1_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					rename($base_url.$filename,$base_url.'original/'.$newname);
					$original_slider1 = $newname;
					$fname = $base_url.'original/'.$newname;
					
					$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($fname);
				}	
			}
			
			$base_url = 'assets/retail_background_image/';
			$original_slider2="";
			$image_name = $this->input->post("slider2_image_name");
			$image_src = $this->input->post("slider2_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					rename($base_url.$filename,$base_url.'original/'.$newname);
					$original_slider2 = $newname;
					$fname = $base_url.'original/'.$newname;
					
					$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($fname);
				}	
			}
	    
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'cp_type' => $this->input->post("cp_type"),
			'cp1_plan' => $this->input->post("cp1_plan"),
			'cp1_no_of_daily_session' => $this->input->post("cp1_no_of_daily_session"),
			'cp2_plan' => $this->input->post("cp2_plan"),
			'cp2_signip_data' => $this->input->post("cp2_signip_data"),
			'cp2_daily_data' => $this->input->post("cp2_daily_data"),
			'cp3_hybrid_plan' => $this->input->post("cp3_hybrid_plan"),
			'cp3_data_plan' => $this->input->post("cp3_data_plan"),
			'cp3_signip_data' => $this->input->post("cp3_signip_data"),
			'cp3_daily_data' => $this->input->post("cp3_daily_data"),
			'image_original' => $original,
			'image_small' => $small,
			'image_logo' => $logo,
			'retail_image_original' => $original_retail,
			'retail_image_small' => $small_retail,
			'retail_image_logo' => $logo_retail,
			'is_otpdisabled' => $this->input->post("is_otpdisabled"),
			'main_ssid' => $this->input->post("main_ssid"),
			'icon_color' => $this->input->post("icon_color"),
			'original_slider1' => $original_slider1,
			'original_slider2' => $original_slider2,
			'login_with_mobile_email' => $this->input->post("login_with_mobile_email"),
			'is_socialloginenable' => $this->input->post("is_socialloginenable"),
			'slider_background_color' => $this->input->post("slider_background_color"),
			'is_pinenable' => $this->input->post("is_pinenable"),
			//'cp1_plan_type' => $this->input->post("cp1_plan_type"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_captive_portal";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function change_location_status(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."change_location_status";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	// get location date for edit view
	public function edit_location_data($locationid){
		$requestData = array(
			'locationid' => $locationid,
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."edit_location_data";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		//print_r(json_decode($curl_response));die;
		return json_decode($curl_response);
	}
	//get the nas list which are not attached with location
	public function nas_list($router_type){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'router_type' => $router_type
		);
		$service_url = $this->api_url."nas_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select NAS</option>';
		foreach($responce_data as $responce_data1){
			
			$content .= '<option data-nasid="'.$responce_data1->nasname.'"  value = "'.$responce_data1->id.'">'.$responce_data1->shortname.'</option>';
		}
		return $content;
	}
	
	// get the isp url
	public function get_isp_url($location_id = '', $for_mikrotik = ''){
		if($location_id == ''){
			if(isset($this->session->userdata['client_ap_configuration']['location_id'])){
				$location_id = $this->session->userdata['client_ap_configuration']['location_id'];
			}
		}
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_id' => $location_id,
			'for_mikrotik' => $for_mikrotik
		);
		$service_url = $this->api_url."get_isp_url";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	// get nes shortname and nasaddress
	public function get_nas_detail($nasid){
		$requestData = array(
			'nasid' => $nasid,
		);
		$service_url = $this->api_url."get_nas_detail";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
		public function location_voucher_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."location_voucher_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			$i = 1;
			foreach($responce_data->vouchers as $responce_data1){
				$content .= '<tr>';
				$content .= '<td>'.$i.'</td>';
				$content .= '<td>'.$responce_data1->voucher_data.'&nbsp'.$responce_data1->voucher_data_type.' </td>';
				$content .= '<td>Rs. '.$responce_data1->voucher_cost.' </td>';
				$content .= '<td>Rs. '.$responce_data1->voucher_selling_price.' </td>';
				$content .= ' <td class="mui--text-right">
						<a href="#" onclick="edit_voucher('.$responce_data1->id.', '.$responce_data1->voucher_data.', \''.$responce_data1->voucher_data_type.'\','.$responce_data1->voucher_cost.','.$responce_data1->voucher_selling_price.')">Edit</a> &nbsp;&nbsp;&nbsp;&nbsp;
						<a href="#" onclick="delete_voucher('.$responce_data1->id.')">Delete</a>
							 </td>';
				$content .= '</tr>';
				$i++;
			}
		}
		return $content;
		
	}
	public function add_location_vouchers(){
		$voucher_id = '';
		if($this->input->post('voucher_id') && $this->input->post('voucher_id') != ''){
			$voucher_id = $this->input->post('voucher_id');
		}
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'voucher_id' => $voucher_id,
			'voucher_data' => $this->input->post('voucher_data'),
			'voucher_data_type' => $this->input->post('voucher_data_type'),
			'voucher_cost' => $this->input->post('voucher_cost'),
			'voucher_selling_price' => $this->input->post('voucher_selling_price')
		);
		$service_url = $this->api_url."add_location_vouchers";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->resultCode;
		
	}

	public function delete_voucher($voucher_id){
		$requestData = array(
			'voucher_id' => $voucher_id
		);
		$service_url = $this->api_url."delete_voucher";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function update_location_voucher_status($locationid){
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
			'voucher_need' => $this->input->post("voucher_need"),
			'payment_gateway' => $this->input->post("payment_gateway"),
		);
		$service_url = $this->api_url."update_location_voucher_status";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->resultCode;
	}
	public function update_other_router_with_nas_location(){
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
			'nasid' => $this->input->post("nasid"),
			'isp_uid' => $this->isp_uid,
			'router_type' => $this->input->post("router_type"),
		);
		$service_url = $this->api_url."update_other_router_with_nas_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		return $responce_data->resultCode;
	}
	public function nas_setup_list(){
		$location_type = $this->input->post('location_type');
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."nas_setup_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			/*$EnGenius = ($responce_data->nastype == '2')?"selected":"";
			$Cisco = ($responce_data->nastype == '3')?"selected":"";
			$Ubiquity = ($responce_data->nastype == '4')?"selected":"";*/
			/*<option value="2" '.$EnGenius.'>EnGenius</option>
							 <option value="3" '.$Cisco.'>Cisco</option>
							 <option value="4" '.$Ubiquity.'>Ubiquity</option>*/
			$Microtik = ($responce_data->nastype == '1')?"selected":"";
			$Cambium = ($responce_data->nastype == '5')?"selected":"";
			$one_hop = ($responce_data->nastype == '6')?"selected":"";
			$RASBERRY_PI = ($responce_data->nastype == '7')?"selected":"";
			$is_mikrotik = 'style="display: none"';
			$is_onehop = 'style="display: none"';
			if($responce_data->nastype == '1'){
				$is_mikrotik = 'style="display: inline-block"';
			}else{
				$is_onehop = 'style="display: block"';
			}
			$options = '';
			if($location_type == '10' || $location_type == '11' || $location_type == '12' || $location_type == '14'){
				$options = '<option value="">Select</option> <option value="7" '.$RASBERRY_PI.'>RASBERRY PI</option>';
			}
			else{
				$options = '<option value="">Select</option>
							 <option value="1" '.$Microtik.'>Microtik</option>
							 <option value="5" '.$Cambium.'>Cambium</option>
							 <option value="6" '.$one_hop.'>One Hop</option>';
			}
			$content .='<div class="row"><div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-3">
						<label class="so-lable-network">NAS Brand <sup>*</sup></label>
						<select class="form-control" name="router_type" id="router_type" required>
							'.$options.'
						</select>
					</div>
					<div class="col-sm-3 col-xs-3">
						<label class="so-lable-network">Select NAS <sup>*</sup></label>
						<select class="form-control" name="nas" id="nas" required>
							<option data-nasid="'.$responce_data->nasname.'" value="'.$responce_data->nasid.'">'.$responce_data->shortname.'</option>
						</select>
					</div>
					<div class="col-sm-6 col-xs-6 col-pt-30">
						<p id="nas_select_erroe" style="color:red"></p>
						<button type="submit" class="mui-btn mui-btn--outline-network btn-sm" value="microtic" id="microtic_button" '.$is_mikrotik.'>Setup Router</button>
						
						<button type="submit" class="mui-btn mui-btn--outline-network btn-sm" value = "else"  id="else_router" '.$is_onehop.'>Setup Router</button>
					</div>
				</div></div>';
				if($responce_data->nastype == '1'){
					$content .= '<div class="row router_setup_detail_info" style="margin-top:20px;">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-router">
								<span>1. &#60; Router IP '.$responce_data->nasipaddress.' &#62;</span> Configured
							</label>
						</div>
						
					</div>
				</div>';
					
				}if($responce_data->nastype == '7'){
					$content .= '<div class="row router_setup_detail_info" style="margin-top:20px;">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-router">
								<span>1. &#60; Router IP '.$responce_data->nasipaddress.' &#62;</span> Configured
							</label>
						</div>
						
					</div>
				</div>';
					
				}elseif($responce_data->nastype == '6'){
					$create_network_msg = 'Configure';
					$create_vip_msg = 'Configure';
					if($responce_data->is_network_ssid_created == '1'){
						$create_network_msg = 'Re-Configure';
					}
					if($responce_data->is_network_vip_ssid_created == '1'){
						$create_vip_msg = 'Re-Configure';
					}
					$content .= '<div class="row router_setup_detail_info" style="margin-top:20px;">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-router">
								<span>1. &#60; Network Name: '.$responce_data->network_name.' &#62;</span> Created
							</label>
						</div>
						<div class="col-sm-4 col-xs-4">
							<button type="button" class="mui-btn mui-btn--outline-router btn-sm" onclick="add_onehop_ssid_view()">'.$create_network_msg.'<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
						</div>
						
					</div>
				</div>';
				$content .= '<div class="row router_setup_detail_info" style="margin-top:20px;">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-router">
								<span>2. &#60; VIP Access  &#62;</span> 
							</label>
						</div>
						<div class="col-sm-4 col-xs-4">
							<button type="button" class="mui-btn mui-btn--outline-router btn-sm" onclick="add_onehop_vip_ssid_view()">'.$create_vip_msg.'<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
						</div>
						
					</div>
				</div>';
				}
			
			$content .= '<div class="col-sm-12 col-xs-12 router_setup_detail_info" style="margin-top: 20px; margin-bottom:15px">
					<h4>ADD ACCESS POINT TO NETWORK</h4>
				</div>	';
			$content .= '<div class="row router_setup_detail_info" style="margin-top: 20px">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-network">MAC ID <sup>*</sup></label>
							<input type="text" class="form-control" name="access_point_macid" id="access_point_macid">
							
						</div>
						<div class="col-sm-6 col-xs-6">
							<div class="row">
								<div class="col-sm-7 col-xs-7">
									<label class="so-lable-network">Select AP Location <sup>*</sup></label>
									<select class="form-control" id="add_ap_location" name="add_ap_location">
										<option value="">Select AP location</option>
									</select>	
								</div>
								<div class="col-sm-5 col-xs-5">
									<h5 class="so-add_headings" onclick="add_ap_location_model()">
										Add New Location
									</h5>	
								</div>
							</div>
						</div>
						<div class="col-sm-2 col-xs-2 col-pt-30" >
						
							<button type="button" onclick="add_location_access_point()" class="mui-btn mui-btn--outline-network btn-sm">Add<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
						</div>
					</div>
					<div class="col-sm-12 col-xs-12">
					<p style="color:red" id="add_access_point_errro"></p>
					</div>
				</div>';
			$content .= '	<div class="row router_setup_detail_info">
						<div class="col-sm-12 col-xs-12">
							<div class="table-responsive">
								<div class="col-sm-12 col-xs-12">
									<table class="table table-striped">
										<thead>
											<tr class="active">
												<th>S.NO</th>
												<th>MAC ID</th>
												<th>BRAND</th>
												<th class="text-center">ACTIONS</th>
												<th class="text-center">STATUS</th>
											</tr>
										</thead>
										<tbody id="access_point_list">
										</tbody>
									</table>
								</div>
							</div>
						</div>	
					</div>';
				
		}
		else{
			/*<option value="2">EnGenius</option>
							 <option value="3">Cisco</option>
							 <option value="4">Ubiquity</option>*/
			$options = '';
			if($location_type == '10' || $location_type == '11' || $location_type == '12' || $location_type == '14'){
				$options = ' <option value="">Select</option><option value="7">RASBERRY PI</option>';
			}
			else{
				$options = '<option value="">Select</option>
							 <option value="1" >Microtik</option>
							 <option value="5">Cambium</option>
							 <option value="6">One Hop</option>';
			}
			$content .='<div class="row "><div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-3">
						<label class="so-lable-network">NAS Brand <sup>*</sup></label>
						<select class="form-control" name="router_type" id="router_type" required>
							'.$options.'
						</select>
					</div>
					<div class="col-sm-3 col-xs-3">
						<label class="so-lable-network">Select NAS <sup>*</sup></label>
						<select class="form-control" name="nas" id="nas" required>
							<option value="">Select</option>
						</select>
					</div>
					<div class="col-sm-6 col-xs-6 col-pt-30">
						<p id="nas_select_erroe" style="color:red"></p>
						<button type="submit" class="mui-btn mui-btn--outline-network btn-sm" value="microtic" id="microtic_button" style="display: none">Setup Router</button>
						<button type="submit" class="mui-btn mui-btn--outline-network btn-sm" value = "else"  id="else_router" style="display: none">Setup Router</button>
					</div>
				</div></div>';
		}
		return $content;
	}
	public function add_location_access_point(){
		$access_point_id = '';
		if($this->input->post('access_point_id') && $this->input->post('access_point_id') != ''){
			$access_point_id = $this->input->post('access_point_id');
		}
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'access_point_id' => $access_point_id,
			'access_point_ssid' => $this->input->post('access_point_ssid'),
			'access_point_hotspot_type' => $this->input->post('access_point_hotspot_type'),
			'access_point_macid' => $this->input->post('access_point_macid'),
			'access_point_ip' => $this->input->post('access_point_ip'),
			'ap_location_type' => $this->input->post('ap_location_type'),
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."add_location_access_point";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		return $responce_data->resultMsg;
		//return $responce_data->resultCode;
		
	}

	
	public function location_access_point_list($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."location_access_point_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		//echo "<pre>"; print_R($responce_data); die;
		if($responce_data->resultCode == 1){
			$i = 1;
			foreach($responce_data->vouchers as $responce_data1){
				$content .= '<tr>';
				$content .= '<td><span>'.$i.'</span></td>';
				$content .= '<td>'.$responce_data1->macid.'</td>';
				$content .= '<td>'.$responce_data1->router_name.' </td>';
				//$content .= '<td>'.$responce_data1->ip.' </td>';
				/*$content .= '<td>
                                 <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                                    <label class="onoffswitch-label" for="myonoffswitch">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                    </label>
                                 </div>
                              </td>';*/
				$content .= ' <td class="text-center">
						<a href="#" onclick="edit_access_point('.$responce_data1->ap_location_type.','.$responce_data1->id.', \''.$responce_data1->macid.'\' , \''.$responce_data1->hotspot_type.'\', \''.$responce_data1->ssid.'\', \''.$responce_data1->ip.'\')">Edit</a> &nbsp;&nbsp;
						';
						if(isset($responce_data1->router_type)){
							if($responce_data1->router_type == '6'){
								$content .= '<a class="mui-url_btn--os" onclick="move_access_point('.$responce_data1->ap_location_type.','.$responce_data1->id.', \''.$responce_data1->macid.'\' , \''.$responce_data1->hotspot_type.'\', \''.$responce_data1->router_type.'\')">Move</a>';
							}elseif($responce_data1->router_type == '7'){
								$content .= '<a class="mui-url_btn--os" onclick="move_access_point('.$responce_data1->ap_location_type.','.$responce_data1->id.', \''.$responce_data1->macid.'\' , \''.$responce_data1->hotspot_type.'\', \''.$responce_data1->router_type.'\')">Move</a>';
							}	
						}
				
						$content .='
							 </td>';
				$nsimg='<img src="'.base_url().'assets/images/loader.svg" width="15%">';
				
				$router_type = '';
				if(isset($responce_data1->router_type)){
					$router_type = $responce_data1->router_type;
					if($responce_data1->router_type != '6'){
						$nsimg='<img src="'.base_url().'assets/images/green.png">';
					}	
				}
				if($responce_data1->router_type == '7'){
					$nsimg.='&nbsp; <span class="viewras" style="cursor:pointer;" data-macid="'.$responce_data1->macid.'" data-routertype="'.$router_type.'" data-locuid="'.$responce_data1->location_uid.'"><i class="fa fa-eye"></i></span>';
				}
				$ipadd = $responce_data1->location_uid.'_'.$i;
				$content .= '<td class = " text-center loc cla_'.$ipadd.'" data-ipadd="'.$ipadd.'" data-raspstat="'.$responce_data1->raspberry_status.'"  data-macid="'.$responce_data1->macid.'" data-routertype="'.$router_type.'" data-locuid="'.$responce_data1->location_uid.'">'.$nsimg.'</td>';
				$content .= '</tr>';
				$i++;
				//<a href="#" onclick="delete_access_point('.$responce_data1->id.')">Delete</a>
			}
		}
		return $content;
		
	}
	


	public function delete_access_point($access_point_id){
		$requestData = array(
			'access_point_id' => $access_point_id
		);
		$service_url = $this->api_url."delete_access_point";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function add_zone(){
		$requestData = array(
			'city_id' => $this->input->post("city_id"),
			'zone_name' => $this->input->post("zone_name"),
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."add_zone";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function add_city(){
		$requestData = array(
			'state_id' => $this->input->post("state_id"),
			'city_name' => $this->input->post("city_name"),
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."add_city";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
		public function check_cp_selected_for_location($location_uid){
		$requestData = array(
			'location_uid' => $location_uid,
		);
		$service_url = $this->api_url."check_cp_selected_for_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->resultCode;
	}
		public function done_configuration(){
		
		$router_type = '';
		if(isset($this->session->userdata['client_ap_configuration']['router_type_wap'])){
			$router_type = $this->session->userdata['client_ap_configuration']['router_type_wap'];
		}
		
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$location_name = $this->session->userdata['client_ap_configuration']['username'];
		$location_id = $this->session->userdata['client_ap_configuration']['location_id'];
		$cp_path = $this->session->userdata['client_ap_configuration']['cp_path'];
		$subdomain_ip = '';
		if(isset($this->session->userdata['client_ap_configuration']['subdomain_ip'])){
			$subdomain_ip = $this->session->userdata['client_ap_configuration']['subdomain_ip'];
		}
		$secret = '';
		if(isset($this->session->userdata['client_ap_configuration']['secret'])){
			$secret = $this->session->userdata['client_ap_configuration']['secret'];
		}
		$main_ssid= '';
		if(isset($this->session->userdata['client_ap_configuration']['main_ssid'])){
			$main_ssid = $this->session->userdata['client_ap_configuration']['main_ssid'];
		}
		$guest_ssid = '';
		if(isset($this->session->userdata['client_ap_configuration']['guest_ssid'])){
			$guest_ssid = $this->session->userdata['client_ap_configuration']['guest_ssid'];
		}
		$is_dynamic = '';
		if(isset($this->session->userdata['client_ap_configuration']['is_dynamic'])){
			$is_dynamic = $this->session->userdata['client_ap_configuration']['is_dynamic'];
		}
		$requestData = array(
			'secret' => $secret,
			'router_type' => $router_type,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
			'location_name' => $location_name,
			'location_id' => $location_id,
			'cp_path' => $cp_path,
			'subdomain_ip' => $subdomain_ip,
			'isp_uid' => $this->isp_uid,
			'main_ssid' => $main_ssid,
			'guest_ssid' => $guest_ssid,
			'is_dynamic' => $is_dynamic,
		);
		$service_url = $this->api_url."configure_microtic_router";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$curl_response = '1';
		if($curl_response == '1'){
			$this->update_configure();
			
		}
		
		return $curl_response;
	}

	public function update_configure(){
		$location_id = $this->session->userdata['client_ap_configuration']['location_id'];
		$nasaddress = $this->session->userdata['client_ap_configuration']['nasname'];
		$nasid = $this->session->userdata['client_ap_configuration']['nasid'];
		$requestData = array(
			'location_id' => $location_id,
			'nasaddress' => $nasaddress,
			'nasid' => $nasid,
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."update_microtic_router_with_nas_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		
	}
	
	public function reset_router($router_type){
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$requestData = array(
			'router_type' => $router_type,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
		);
		$service_url = $this->api_url."reset_router";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}
	public function check_router_selected_matched($router_type){
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$requestData = array(
			'router_type' => $router_type,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
		);
		$service_url = $this->api_url."check_router_selected_matched";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}
	public function check_wap_router_reset_porperly($router_type){
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$requestData = array(
			'router_type' => $router_type,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
		);
		$service_url = $this->api_url."check_wap_router_reset_porperly";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}
	public function getExtension($str){
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }

	public function add_hotel_captive_portal(){
		$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/cp_brand_image';
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
		$base_url = 'assets/cp_brand_image/';
		$original="";
                        $small="";
                        $logo="";
			$image_name = $this->input->post("image_name");
			$image_src = $this->input->post("image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					
					$success = file_put_contents($file, $data_img);
					$requestData = array(
						'location_uid' => $this->input->post("location_uid"),
					);
					$service_url = $this->api_url."location_logo_name";
					$curl = curl_init($service_url);
					$requestData = $requestData;
					$data_request = json_encode($requestData);
					//print_r($data_request);die;
					$curl_post_data = array("requestData" => $data_request);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$curl_response = curl_exec($curl);
					curl_close($curl);
					$image_responce = json_decode($curl_response);
					if(isset($image_responce->original_name) && $image_responce->original_name != ''){
						$newname = $image_responce->original_name;
						$newname1=date("Ymdhisv").rand().".".$file_ext; 
					}else{
						$newname=date("Ymdhisv").rand().".".$file_ext;
						$newname1=date("Ymdhisv").rand().".".$file_ext; 
					}
					//rename($base_url.$filename,$base_url.'original/'.$newname);
					rename($base_url.$filename,$base_url.'original/'.$newname1);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					//$config1['source_image'] = $base_url . 'original/'.$newname;
					$config1['source_image'] = $base_url . 'original/'.$newname1;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname;
                        
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
                            
					$this->image_lib->clear();
					 $config2['image_library'] = 'gd2';
					 //$config2['source_image'] = $base_url . 'original/'.$newname;
					 //$config2['new_image'] = $base_url . 'small/'."600_".$newname;
					 $config2['source_image'] = $base_url . 'original/'.$newname1;
					 $config2['new_image'] = $base_url . 'small/'."600_".$newname1;
                          
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					  $this->image_lib->initialize($config2);
					  $this->image_lib->resize();
                              
					/*$original=$newname;
					 $small="600_".$newname;
					 $logo="300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname;
			   
			$famazonname = AMAZONPATH.'isp_location_logo/original/'.$newname;
			$famazonname1 = AMAZONPATH.'isp_location_logo/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'isp_location_logo/small/600_'. $newname;*/
	  
                          $original=$newname1;
					 $small="600_".$newname1;
					 $logo="300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname1;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname1;
			   
			$famazonname = AMAZONPATH.'isp_location_logo/original/'.$newname1;
			$famazonname1 = AMAZONPATH.'isp_location_logo/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'isp_location_logo/small/600_'. $newname1;
			
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				
				unlink($fname);
			unlink($fname1);
			unlink($fname2);	 
					
				}	
			}
			$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/retail_background_image';
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
		$base_url = 'assets/retail_background_image/';
		$original_retail="";
                        $small_retail="";
                        $logo_retail="";
			$image_name = $this->input->post("background_image_name");
			$image_src = $this->input->post("background_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					
					rename($base_url.$filename,$base_url.'original/'.$newname);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = $base_url . 'original/'.$newname;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname;
                        
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
                            
					$this->image_lib->clear();
					 $config2['image_library'] = 'gd2';
					 $config2['source_image'] = $base_url . 'original/'.$newname;
					 $config2['new_image'] = $base_url . 'small/'."600_".$newname;
                          
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					  $this->image_lib->initialize($config2);
					  $this->image_lib->resize();
                              
					$original_retail = $newname;
					 $small_retail = "600_".$newname;
					 $logo_retail = "300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname;
			   
			$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
			$famazonname1 = AMAZONPATH.'retail_background_image/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'retail_background_image/small/600_'. $newname;
	  
                          
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				
				unlink($fname);
			unlink($fname1);
			unlink($fname2);	 
					
				}	
			}
			
			$base_url = 'assets/retail_background_image/';
			$original_slider1="";
			$image_name = $this->input->post("slider1_image_name");
			$image_src = $this->input->post("slider1_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					rename($base_url.$filename,$base_url.'original/'.$newname);
					$original_slider1 = $newname;
					$fname = $base_url.'original/'.$newname;
					
					$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($fname);
				}	
			}
			
			$base_url = 'assets/retail_background_image/';
			$original_slider2="";
			$image_name = $this->input->post("slider2_image_name");
			$image_src = $this->input->post("slider2_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					rename($base_url.$filename,$base_url.'original/'.$newname);
					$original_slider2 = $newname;
					$fname = $base_url.'original/'.$newname;
					
					$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($fname);
				}	
			}
	    
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'cp_type' => $this->input->post("cp_type"),
			'cp_hotel_plan_type' => $this->input->post("cp_hotel_plan_type"),
			'cp_home_selected_plan_id' => $this->input->post("cp_home_selected_plan_id"),
			'image_original' => $original,
			'image_small' => $small,
			'image_logo' => $logo,
			'cp_enterprise_user_type' => $this->input->post("cp_enterprise_user_type"),
			'institutional_user_list' => $this->input->post("institutional_user_list"),
			'main_ssid' => $this->input->post("main_ssid"),
			'guest_ssid' => $this->input->post("guest_ssid"),
			'cp_cafe_plan_id' => $this->input->post("cp_cafe_plan_id"),
			'is_otpdisabled' => $this->input->post("is_otpdisabled"),
			'retail_image_original' => $original_retail,
			'retail_image_small' => $small_retail,
			'retail_image_logo' => $logo_retail,
			'num_of_session_per_day' => $this->input->post("num_of_session_per_day"),
			'icon_color' => $this->input->post("icon_color"),
			'is_socialloginenable' => $this->input->post("is_socialloginenable"),
			'is_pinenable' => $this->input->post("is_pinenable"),
			'is_wifidisabled' => $this->input->post("is_wifidisabled"),
			'login_with_mobile_email' => $this->input->post("login_with_mobile_email"),
			'offer_redemption_mode' => $this->input->post("offer_redemption_mode"),
			
			'custom_captive_url' => $this->input->post("custom_captive_url"),
			'custom_location_plan' => $this->input->post("custom_location_plan"),
			'custon_cp_signip_data' => $this->input->post("custon_cp_signip_data"),
			'custon_cp_daily_data' => $this->input->post("custon_cp_daily_data"),
			'custon_cp_no_of_daily_session' => $this->input->post("custon_cp_no_of_daily_session"),
			
			'original_slider1' => $original_slider1,
			'original_slider2' => $original_slider2,
			
			'synk_frequency' => $this->input->post("synk_frequency"),
			'offline_cp_type' => $this->input->post("offline_cp_type"),
			'offline_cp_source_folder_path' => $this->input->post("offline_cp_source_folder_path"),
			'offline_cp_landing_page_path' => $this->input->post("offline_cp_landing_page_path"),
			'is_offline_registration' => $this->input->post("is_offline_registration"),
			'cp_cafe_retail_plan_type' => $this->input->post("cp_cafe_retail_plan_type"),
			'contestifi_contest_header' => $this->input->post("contestifi_contest_header"),
			'user_name_required' => $this->input->post("user_name_required"),
			'user_email_required' => $this->input->post("user_email_required"),
			'user_age_required' => $this->input->post("user_age_required"),
			'user_gender_required' => $this->input->post("user_gender_required"),
			'is_white_theme' => $this->input->post("is_white_theme"),
			'is_live_chat_enable' => $this->input->post("is_live_chat_enable"),
			'is_discuss_enable' => $this->input->post("is_discuss_enable"),
			'is_information_enable' => $this->input->post("is_information_enable"),
			'is_poll_enable' => $this->input->post("is_poll_enable"),
			'is_download_enable' => $this->input->post("is_download_enable"),
			'event_module_signup_user_name_require' => $this->input->post("event_module_signup_user_name_require"),
			'event_model_signup_user_organisation_require' => $this->input->post("event_model_signup_user_organisation_require"),
			'event_module_signup_nick_name' => $this->input->post("event_module_signup_nick_name"),
			'event_module_signup_email' => $this->input->post("event_module_signup_email"),
			'event_module_signup_phone' => $this->input->post("event_module_signup_phone"),
			'is_preaudit_disable' => $this->input->post("is_preaudit_disable"),
			'audit_ticket_email_send' => $this->input->post("audit_ticket_email_send"),
			'tatdays' => $this->input->post("tatdays"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_hotel_captive_portal";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function ap_location_list($ap_location_id = ''){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."ap_location_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		$content = '<option value = "">Select AP Location</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->ap_id == $ap_location_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->ap_id.'">'.$responce_data1->ap_location_name.'</option>';
		}
		return $content;
	}
	
	public function add_ap_location(){
		$requestData = array(
			'ap_location_name' => $this->input->post("ap_location_name"),
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."add_ap_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function is_location_proper_setup($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."is_location_proper_setup";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_get_network_detail($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."onehop_get_network_detail";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_get_network_create(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'network_name' => trim($this->input->post('network_name')),
			'description' => $this->input->post('description'),
			'maxaps' => $this->input->post('maxaps'),
			'coa_status' => $this->input->post('coa_status'),
			'coa_ip' => $this->input->post('coa_ip'),
			'coa_secret' => $this->input->post('coa_secret'),
			'address' => $this->input->post('address'),
			'latitude' => $this->input->post('latitude'),
			'longitude' => $this->input->post('longitude'),
			'service_name' => $this->input->post('service_name'),
			'beacon_status' => $this->input->post('beacon_status'),
		);
		$service_url = $this->api_url."onehop_get_network_create";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_add_apmac(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'onehop_ap_mac' => $this->input->post('onehop_ap_mac'),
			'onehop_ap_name' => $this->input->post('onehop_ap_name'),
			
		);
		
		$service_url = $this->api_url."onehop_add_apmac";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_add_ssid(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'onehop_ssid_index' => $this->input->post('onehop_ssid_index'),
			'onehop_ssid_name' => $this->input->post('onehop_ssid_name'),
			'onehop_association' => $this->input->post('onehop_association'),
			'onehop_wap_mode' => $this->input->post('onehop_wap_mode'),
			'onehop_forwarding' => $this->input->post('onehop_forwarding'),
			'onehop_cp_mode' => $this->input->post('onehop_cp_mode'),
			'onehop_cp_url' => $this->input->post('onehop_cp_url'),
			'onehop_auth_server_ip' => $this->input->post('onehop_auth_server_ip'),
			'onehop_auth_server_port' => $this->input->post('onehop_auth_server_port'),
			'onehop_auth_server_secret' => $this->input->post('onehop_auth_server_secret'),
			'onehop_accounting' => $this->input->post('onehop_accounting'),
			'onehop_acc_server_ip' => $this->input->post('onehop_acc_server_ip'),
			'onehop_acc_server_port' => $this->input->post('onehop_acc_server_port'),
			'onehop_acc_server_secret' => $this->input->post('onehop_acc_server_secret'),
			'onehop_acc_interval' => $this->input->post('onehop_acc_interval'),
			'onehop_psk' => $this->input->post('onehop_psk'),
			'onehop_wallgarden_ip_list' => $this->input->post('onehop_wallgarden_ip_list'),
			'onehop_wallgarden_domain_list' => $this->input->post('onehop_wallgarden_domain_list'),
		);
		
		$service_url = $this->api_url."onehop_add_ssid";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_get_ssid_detail(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'index_number' => $this->input->post('index_number'),
			
		);
		
		$service_url = $this->api_url."onehop_get_ssid_detail";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_delete_ssid(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'onehop_ssid_index' => $this->input->post('onehop_ssid_index'),
			'onehop_ssid_name' => $this->input->post('onehop_ssid_name'),
		);
		
		$service_url = $this->api_url."onehop_delete_ssid";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function plan_detail(){
		$requestData = array(
			'plan_id' => $this->input->post('plan_id'),
			
		);
		
		$service_url = $this->api_url."plan_detail";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function check_plan_already_attached($plan_id){
		$requestData = array(
			'plan_id' => $plan_id
		);
		$service_url = $this->api_url."check_plan_already_attached";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function save_plan(){
		$requestData = array(
			'plan_id' => $this->input->post("plan_id"),
			'data_limit' => $this->input->post("data_limit"),
			'time_limit' => $this->input->post("time_limit"),
			'plan_name' => $this->input->post("plan_name"),
			'plan_desc' => $this->input->post("plan_desc"),
			'downrate' => $this->input->post("downrate"),
			'uprate' => $this->input->post("uprate"),
			'plan_type' => $this->input->post("plan_type"),
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."save_plan";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function terms_of_use($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."terms_of_use";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->content;
		
	}
	public function update_terms_of_use($location_uid,$text_value){
		$requestData = array(
			'location_uid' => $location_uid,
			'text_value' => $text_value
		);
		$service_url = $this->api_url."update_terms_of_use";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->result_msg;
		
	}
	public function current_image_path($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."current_image_path";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function current_cp_path($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."current_cp_path";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function get_location_ap_move_where($router_type){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'router_type' => $this->input->post("router_type"),
		);
		$service_url = $this->api_url."get_location_ap_move_where";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select Location</option>';
		foreach($responce_data as $responce_data1){
			
			$content .= '<option  value = "'.$responce_data1->location_uid.'">'.$responce_data1->location_name.'</option>';
		}
		return $content;
	}
	public function onehop_move_ap($onehop_move_macid,$onehop_move_location_uid){
		$requestData = array(
			'onehop_move_macid' => $onehop_move_macid,
			'onehop_move_location_uid' => $onehop_move_location_uid,
			'router_type' => $this->input->post("router_type"),
		);
		$service_url = $this->api_url."onehop_move_ap";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function ping($host){
		exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
		return $rval;
	}
	public function ping_mikrotik_ip_address($ip_address){
            $up = $this->ping($ip_address);
	    $last_sceen = date('M d H:i Y',strtotime(date('Y-m-d H:i:s')));
                if($up == '0'){//0 means server is up
			$update = $this->db->query("update wifi_location set is_location_online = '1',last_online_time = '$last_sceen' where nasipaddress = '$ip_address'");
                    return 1;
                }else{
			$update = $this->db->query("update wifi_location set is_location_online = '0' where nasipaddress = '$ip_address'");
                    return 0;
                }
	}
	public function ping_onehop_ap($location_uid){
		$requestData = array(
			'location_uid' => $location_uid,
		);
		$service_url = $this->api_url."ping_onehop_ap";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function ping_onehop_ap_wise($location_uid,$macid){
		$requestData = array(
			'location_uid' => $location_uid,
			'macid' => $macid,
		);
		$service_url = $this->api_url."ping_onehop_ap_wise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function remove_slider_image($location_uid,$slider_type){
		$requestData = array(
			'location_uid' => $location_uid,
			'slider_type' => $slider_type,
		);
		$service_url = $this->api_url."remove_slider_image";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delete_wifi_location(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."delete_wifi_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	
	
	
	public function content_builder_list(){
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
		);
		$service_url = $this->api_url."content_builder_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content_builder(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'offline_content_title' => $this->input->post("offline_content_title"),
			'offline_content_desc' => $this->input->post("offline_content_desc"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_builder";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_main_category(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$icon = '';
		
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$icon = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'main_category_name' => $this->input->post("main_category_name"),
			'main_category_desc' => $this->input->post("main_category_desc"),
			'main_category_id' => $this->input->post("main_category_id"),
			'offline_content_title' => $this->input->post("offline_content_title"),
			'offline_content_desc' => $this->input->post("offline_content_desc"),
			'icon' => $icon,
			'file_previous_name' => $this->input->post("file_previous_name"),
			'is_promoter_category' => $this->input->post("is_promoter_category"),
			'compliance_cat' => $this->input->post("compliance_cat"),
		);
		$service_url = $this->api_url."add_main_category";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_sub_category(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$icon = '';
		
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$icon = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'sub_category_name' => $this->input->post("sub_category_name"),
			'sub_category_desc' => $this->input->post("sub_category_desc"),
			'category_id' => $this->input->post("category_id"),
			'sub_category_id' => $this->input->post("sub_category_id"),
			'icon' => $icon,
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_sub_category";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'text';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			$mime = $_FILES['file']['type'];
			if(strstr($mime, "video/")){
				$file_type = "video";
			}else if(strstr($mime, "image/")){
				$file_type = "image";
			}else if(strstr($mime, "audio/")){
				$file_type = "audio";
			}
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_desc' => $this->input->post("content_desc",true),
			'content_file' => $content_file,
			'content_id' => $this->input->post("content_id"),
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function delete_content($content_id){
		$requestData = array(
			'content_id' => $content_id
		);
		$service_url = $this->api_url."delete_content";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function delete_sub_category($sub_category_id){
		$requestData = array(
			'sub_category_id' => $sub_category_id
		);
		$service_url = $this->api_url."delete_sub_category";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function delete_main_category($main_category_id){
		$requestData = array(
			'main_category_id' => $main_category_id
		);
		$service_url = $this->api_url."delete_main_category";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function get_content_poll_survey(){
		$requestData = array(
			'content_id' => $this->input->post("content_id"),
		);
		$service_url = $this->api_url."get_content_poll_survey";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content_poll(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'poll';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'option_five' => $this->input->post("option_five"),
			'option_six' => $this->input->post("option_six"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_poll";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_content_poll(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'poll';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'content_id' => $this->input->post("content_id"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'option_five' => $this->input->post("option_five"),
			'option_six' => $this->input->post("option_six"),
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_content_poll";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_content_survey(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'survey';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'questions' => json_decode($this->input->post("questions")),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_survey";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_content_survey(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'survey';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'content_id' => $this->input->post("content_id"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
			'questions' => json_decode($this->input->post("questions")),
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_content_survey";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_content_pdf(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'pdf';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_desc' => $this->input->post("content_desc",true),
			'content_file' => $content_file,
			'content_id' => $this->input->post("content_id"),
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content_upload(){
		
		$file_type = 'upload';
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_desc' => $this->input->post("content_desc",true),
			'content_file' => '',
			'content_id' => $this->input->post("content_id"),
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
			'upload_type' => $this->input->post("upload_type"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_upload";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function offline_upload_on_s3($base_url,$newname){
		$fname = $base_url.$newname;
		$famazonname = AMAZONPATH.'offline_video_image_content/'.$newname;
		$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
		$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
		//unlink($fname);
	}

	
	public function get_onehop_existing_network(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_type' => $this->input->post('location_type'),
		);
		$service_url = $this->api_url."get_onehop_existing_network";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select Network</option>';
		foreach($responce_data as $responce_data1){
			
			$content .= '<option  value = "'.$responce_data1->id.'">'.$responce_data1->network_name.'</option>';
		}
		return $content;
	}
	
	
	public function use_existing_onehop_network(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'previous_network_id' => $this->input->post('previous_network_id'),
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."use_existing_onehop_network";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_content_wiki(){
		
		$file_type = 'wiki';
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'wikipedia_title_url' => $this->input->post("wikipedia_title_url"),
			'wikipedia_title' => $this->input->post("wikipedia_title"),
			'wikipedia_pageid' => $this->input->post("wikipedia_pageid"),
			'wikipedia_content' => $this->input->post("wikipedia_content"),
			'content_id' => $this->input->post("content_id"),
			'file_type' => $file_type,
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_wiki";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function raspberry_ping_sync_info(){
		
		
		$requestData = array(
			'macid' => $this->input->post("locmacid"),
			'location_uid' => $this->input->post("locuid")
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."raspberry_ping_sync_info";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responsedata=json_decode($curl_response);
		//echo "<pre>"; print_R($responsedata); die;
		$data=array();
		$pinghtml=' <tr class="table_active">
                                 <th>S.No</th>
                                 <th>Location Macid</th>
                                 <th>Pinged On</th>
                                 <th>Status</th>
                              </tr>';
		if(count($responsedata->pingdata>0)){
			$i=1;
			
			foreach($responsedata->pingdata as $pingval){
				$pinghtml.='<tr><td>'.$i.'</td><td>'.$pingval->loc_macid.'</td><td>'.$pingval->pinged_on.'</td><td>'.$pingval->status.'</td></tr>';
				$i++;
			}
		}else{
			$pinghtml.="<tr><td>No data Found!</td></tr>";
		}
		$synchtml=' <tr class="table_active">
									 <th>S.No</th>
									 <th>Version</th>
									 <th>Synced On</th>
									 <th>Payload Size</th>
									 <th>Payload Recived</th>
									 <th>Percent Completed</th>
									 <th>Status</th>
								  </tr>';
			if(count($responsedata->syncdata>0)){
			$i=1;
			
			foreach($responsedata->syncdata as $syncval){
				$synchtml.='<tr><td>'.$i.'</td><td>'.$syncval->version.'</td><td>'.$syncval->last_communicated.'</td><td>'.$syncval->payload_size.'</td><td>'.$syncval->payload_receive.'</td><td>'.$syncval->percent.'</td><td>'.$syncval->status.'</td></tr>';
				$i++;
			}
		}else{
			$synchtml.="<tr><td>No data Found!</td></tr>";
		}
		
		$data['pinghtml']=$pinghtml;
		$data['synchtml']=$synchtml;
		
		return $data;
	}
	
	public function location_list_new($type, $state, $city,$serach_pattern,$limit,$offset,$online_filter){
		$data = array();
		$type_return = $type;
		$total_record = 0;
		$gen = '';
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		elseif($type == 'visitor'){
			$type = '11';
		}
		elseif($type == 'contestifi'){
			$type = '12';
		}
		elseif($type == 'retail_audit'){
			$type = '13';
		}
		elseif($type == 'event_module'){
			$type = '14';
		}
		elseif($type == 'ordering'){
			$type = '15';
		}
		elseif($type == 'lms'){
			$type = '16';
		}
		
		elseif($type == 'internact'){
			$type = '17';
		}elseif($type == 'eventguest'){
			$type = '18';
		}
		elseif($type == 'club'){
			$type = '19';
		}
		elseif($type == 'truck_stop'){
			$type = '20';
		}
		elseif($type == 'foodle'){
			$type = '21';
		}
		else{
			$type = '';
		}
		$isp_uid = $this->isp_uid;
		$location_type = $type;
		$locations = array();
		$online_offline_filter = '';
		if($online_filter != ''){
			$online_offline_filter = "AND is_location_online=".$online_filter."";
		}
		$location_type_where = '';
		if($location_type != ''){
			$location_type_where = "AND location_type=".$location_type."";
		}
		$state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		$serach_pattern_where = '';
		if($serach_pattern!= ''){
			$serach_pattern = strtolower($serach_pattern);
		    $serach_pattern_where = " AND (wl.state LIKE '%$serach_pattern%' OR wl.city LIKE '%$serach_pattern%' OR wl.location_name LIKE '%$serach_pattern%' OR wl.location_uid LIKE '%$serach_pattern%' OR wl.contact_person_name LIKE '%$serach_pattern%' OR wl.mobile_number LIKE '%$serach_pattern%' OR wl.pin LIKE '%$serach_pattern%'  OR wlap.macid LIKE '%$serach_pattern%')";
		}
		$pubuidstates = ''; $cwhere = '';
		$sessiondata = $this->session->userdata('isp_session');
		$publicuser = $sessiondata['publicuser'];
		if($publicuser == '1'){
			$pubuid = $sessiondata['userid'];
			$pubuid_assignstatesQ = $this->DB2->query("SELECT assigned_region FROM sht_isp_public_users WHERE id='".$pubuid."'");
			if($pubuid_assignstatesQ->num_rows() > 0){
				$pubuidstates = $pubuid_assignstatesQ->row()->assigned_region;
				
				$cwhere .= " AND wl.state_id IN ($pubuidstates) ";
			}
		}
		$query = $this->db->query("select wl.* from wifi_location as wl left join wifi_location_access_point as wlap on (wl.location_uid = wlap.location_uid) where wl.is_deleted = '0' and wl.isp_uid = '$isp_uid' $cwhere $location_type_where $state_where $city_where $serach_pattern_where $online_offline_filter group by wl.location_uid order by wl.location_name LIMIT $offset,$limit");
		if($query->num_rows() > 0){
			$location_ids = array();
			foreach($query->result() as $locrow){
				$location_ids[] = $locrow->location_uid;
			}
			$location_ids = '"'.implode('", "', $location_ids).'"';
			// get devide model
			$device_model = array();
			$get_device = $this->db->query("select location_uid, device_model_name, router_type from wifi_location_access_point where location_uid IN ($location_ids)");
			if($get_device->num_rows() > 0){
			     foreach($get_device->result() as $de_row){
				  if(array_key_exists($de_row->location_uid, $device_model)){
				       $device_model[$de_row->location_uid]['device_model_name'] = $device_model[$de_row->location_uid]['device_model_name'].', '.$de_row->device_model_name;
				  }else{
				       $device_model[$de_row->location_uid]['router_type'] = $de_row->router_type;
				       $device_model[$de_row->location_uid]['device_model_name'] = $de_row->device_model_name;
				  }
				  
			     }
			}
			$i = $offset+1;
			
			foreach($query->result() as $row){
				$is_location_online = $row->is_location_online;
				$nsimg = '<img src = "'.base_url().'assets/images/red.png">';
				if($is_location_online == '1'){
					$nsimg = '<img src = "'.base_url().'assets/images/green.png">';
				}
				$device_name = '';
				$location_uid = $row->location_uid;
				if(isset($device_model[$location_uid])){
					if($device_model[$location_uid]['router_type'] == 6){
					     $device_name = $device_model[$location_uid]['device_model_name'];
					}
					else if($device_model[$location_uid]['router_type'] == 7){
					     $device_name = 'RASBERRY PI';
					}
				}
				$gen .= "<tr>";
				$gen .= "<td>".$i."</td>";
				$gen .= '<td><input type="checkbox" name="location_group[]" value="'.$row->location_uid.'"></td>';
				$gen .= '<td><a href="'.base_url().'location/edit_location?id='.$row->location_uid.'">'.$row->location_uid.'</a></td>';
				$gen .= '<td class="col-sm-2 col-xs-2">'.addslashes($row->location_name).'</td>';
				$gen .= '<td>'.addslashes($row->state).'</td>';
				$gen .= '<td>'.addslashes($row->city).'</td>';
				$gen .= '<td>'.addslashes($row->contact_person_name).'</td>';
				
				$gen .= '<td>'.$row->nasipaddress.'</td>';
				$gen .= '<td>'.$row->last_online_time.'</td>';
				$gen .= ' <td class="col-sm-1 col-xs-1">';
					$gen .= '<span class ="table_loader cla_'.$row->location_uid.'" >'.$nsimg.'</span>';
					$gen .= '<span style="cursor: pointer" class = "loc_click" data-location_type="'.$row->location_type.'" data-locuid="'.$row->location_uid.'" data-nasip="'.$row->nasipaddress.'"><i class="fa fa-refresh" aria-hidden="true"></i></span>';
				$gen .= '</td>';
				$gen .= '<td class="col-sm-2 col-xs-2">'.$device_name.'</td>';
				$gen .= '<td id="location_'.$row->location_uid.'" onclick="delete_wifi_location('.$row->location_uid.')">';
					$gen .= '<i class="fa fa-trash fa-lg" aria-hidden="true" style="cursor:pointer"></i>';
				$gen .='</td>';
				$gen .= "</tr>";
				$i++;
				$total_record++;
			}
			$loadmore = 1;
			
		}else{
			$gen .= '<tr><td colspan="11" style="text-align:center">No More Result Found !!</td></tr>';
			$loadmore = 0;
		}
		$data['search_results'] = $gen;
		$data['location_type'] = $type_return;
		$data['limit'] = $limit;
		$data['offset'] = $limit+$offset;
		$data['loadmore'] = $loadmore;
		$data['total_record'] = $total_record;
		return $data;
	}
	
	public function location_dashboard_new(){
		
		
		$isp_uid = $this->isp_uid;
		//$isp_uid = '1111';
		$data = array();
		$public_active = 0;
		$public_inactive = 0;
		$hotel_active = 0;
		$hotel_inactive = 0;
		$hospital_active = 0;
		$hospital_inactive = 0;
		$institutional_active = 0;
		$institutional_inactive = 0;
		$enterprise_active = 0;
		$enterprise_inactive = 0;
		$caffe_active = 0;
		$caffe_inactive = 0;
		$retail_active = 0;
		$retail_inactive = 0;
		$custom_active = 0;
		$custom_inactive = 0;
		$purple_active = 0;
		$purple_inactive = 0;
		$offline_active = 0;
		$offline_inactive = 0;
		$visitor_active = 0;
		$visitor_inactive = 0;
		$contestifi_active = 0;
		$contestifi_inactive = 0;
		$retail_audit_active = 0;
		$retail_audit_inactive = 0;
		$event_module_active = 0;
		$event_module_inactive = 0;
		$ordering_active = 0;
		$ordering_inactive = 0;
		$lms_active = 0;
		$lms_inactive = 0;
		$internact_active = 0;
		$internact_inactive = 0;
		$eventguest_active=0;
		$eventguest_inactive=0;
		$club_active = 0;
		$club_inactive = 0;
		$truck_stop_active = 0;
		$truck_stop_inactive = 0;
		$foodle_active = 0;
		$foodle_inactive = 0;
		$pubuidstates = ''; $cwhere = '';
		$sessiondata = $this->session->userdata('isp_session');
		$publicuser = $sessiondata['publicuser'];
		if($publicuser == '1'){
			$pubuid = $sessiondata['userid'];
			$pubuid_assignstatesQ = $this->DB2->query("SELECT assigned_region FROM sht_isp_public_users WHERE id='".$pubuid."'");
			if($pubuid_assignstatesQ->num_rows() > 0){
				$pubuidstates = $pubuid_assignstatesQ->row()->assigned_region;
				
				$cwhere .= " AND state_id IN ($pubuidstates) ";
			}
		}
		//echo "<pre>";print_r($this->session->userdata('isp_session'));die;
		$query = $this->db->query("select is_location_online , location_type from wifi_location where is_deleted = '0' and isp_uid = '$isp_uid' $cwhere");
		foreach($query->result() as $row){
			if($row->location_type == '1'){
				//public location
				if($row->is_location_online == '1'){
					$public_active = $public_active+1;
				}else{
					$public_inactive = $public_inactive+1;
				}
			}
			elseif($row->location_type == '2'){
				//sme location
				if($row->is_location_online == '1'){
					$hotel_active = $hotel_active+1;
				}else{
					$hotel_inactive = $hotel_inactive+1;
				}
			}
			elseif($row->location_type == '3'){
				//sme location
				if($row->is_location_online == '1'){
					$hospital_active = $hospital_active+1;
				}else{
					$hospital_inactive = $hospital_inactive+1;
				}
			}
			elseif($row->location_type == '4'){
				//sme location
				if($row->is_location_online == '1'){
					$institutional_active = $institutional_active+1;
				}else{
					$institutional_inactive = $institutional_inactive+1;
				}
			}
			elseif($row->location_type == '5'){
				//sme location
				if($row->is_location_online == '1'){
					$enterprise_active = $enterprise_active+1;
				}else{
					$enterprise_inactive = $enterprise_inactive+1;
				}
			}elseif($row->location_type == '6'){
				//sme location
				if($row->is_location_online == '1'){
					$caffe_active = $caffe_active+1;
				}else{
					$caffe_inactive = $caffe_inactive+1;
				}
			}
			elseif($row->location_type == '7'){
				//sme location
				if($row->is_location_online == '1'){
					$retail_active = $retail_active+1;
				}else{
					$retail_inactive = $retail_inactive+1;
				}
			}
			elseif($row->location_type == '8'){
				//sme location
				if($row->is_location_online == '1'){
					$custom_active = $custom_active+1;
				}else{
					$custom_inactive = $custom_inactive+1;
				}
			}
			elseif($row->location_type == '9'){
				//sme location
				if($row->is_location_online == '1'){
					$purple_active = $purple_active+1;
				}else{
					$purple_inactive = $purple_inactive+1;
				}
			}
			 elseif($row->location_type == '10'){
				//sme location
				if($row->is_location_online == '1'){
					$offline_active = $offline_active+1;
				}else{
					$offline_inactive = $offline_inactive+1;
				}
			 }
			elseif($row->location_type == '11'){
				//sme location
				if($row->is_location_online == '1'){
					$visitor_active = $visitor_active+1;
				}else{
					$visitor_inactive = $visitor_inactive+1;
				}
			}
			elseif($row->location_type == '12'){
				//sme location
				if($row->is_location_online == '1'){
					$contestifi_active = $contestifi_active+1;
				}else{
					$contestifi_inactive = $contestifi_inactive+1;
				}
			}
			elseif($row->location_type == '13'){
				//sme location
				if($row->is_location_online == '1'){
					$retail_audit_active = $retail_audit_active+1;
				}else{
					$retail_audit_inactive = $retail_audit_inactive+1;
				}
			}
			elseif($row->location_type == '14'){
				//sme location
				if($row->is_location_online == '1'){
					$event_module_active = $event_module_active+1;
				}else{
					$event_module_inactive = $event_module_inactive+1;
				}
			}
			elseif($row->location_type == '15'){
				//sme location
				if($row->is_location_online == '1'){
					$ordering_active = $ordering_active+1;
				}else{
					$ordering_inactive = $ordering_inactive+1;
				}
			}
			elseif($row->location_type == '16'){
				//sme location
				if($row->is_location_online == '1'){
					$lms_active = $lms_active+1;
				}else{
					$lms_inactive = $lms_inactive+1;
				}
			}
			elseif($row->location_type == '17'){
				//sme location
				if($row->is_location_online == '1'){
					$internact_active = $internact_active+1;
				}else{
					$internact_inactive = $internact_inactive+1;
				}
			}elseif($row->location_type == '18'){
				//sme location
				if($row->is_location_online == '1'){
					$eventguest_active = $eventguest_active+1;
				}else{
					$eventguest_inactive = $eventguest_inactive+1;
				}
			}
			elseif($row->location_type == '19'){
				//sme location
				if($row->is_location_online == '1'){
					$club_active = $club_active+1;
				}else{
					$club_inactive = $club_inactive+1;
				}
			}
			elseif($row->location_type == '20'){
				//sme location
				if($row->is_location_online == '1'){
					$truck_stop_active = $truck_stop_active+1;
				}else{
					$truck_stop_inactive = $truck_stop_inactive+1;
				}
			}
			elseif($row->location_type == '21'){
				//sme location
				if($row->is_location_online == '1'){
					$foodle_active = $foodle_active+1;
				}else{
					$foodle_inactive = $foodle_inactive+1;
				}
			}
		}
		$data['public_active'] = $public_active;
		$data['public_inactive'] = $public_inactive;
		$data['hotel_active'] = $hotel_active;
		$data['hotel_inactive'] = $hotel_inactive;
		$data['hospital_active'] = $hospital_active;
		$data['hospital_inactive'] = $hospital_inactive;
		$data['institutional_active'] = $institutional_active;
		$data['institutional_inactive'] = $institutional_inactive;
		$data['enterprise_active'] = $enterprise_active;
		$data['enterprise_inactive'] = $enterprise_inactive;
		$data['caffe_active'] = $caffe_active;
		$data['caffe_inactive'] = $caffe_inactive;
		$data['retail_active'] = $retail_active;
		$data['retail_inactive'] = $retail_inactive;
		$data['custom_active'] = $custom_active;
		$data['custom_inactive'] = $custom_inactive;
		$data['purple_active'] = $purple_active;
		$data['purple_inactive'] = $purple_inactive;
		$data['offline_active'] = $offline_active;
		$data['offline_inactive'] = $offline_inactive;
		$data['visitor_active'] = $visitor_active;
		$data['visitor_inactive'] = $visitor_inactive;
		$data['contestifi_active'] = $contestifi_active;
		$data['contestifi_inactive'] = $contestifi_inactive;
		$data['retail_audit_active'] = $retail_audit_active;
		$data['retail_audit_inactive'] = $retail_audit_inactive;
		$data['event_module_active'] = $event_module_active;
		$data['event_module_inactive'] = $event_module_inactive;
		$data['ordering_active'] = $ordering_active;
		$data['ordering_inactive'] = $ordering_inactive;
		$data['ordering_active'] = $ordering_active;
		$data['ordering_inactive'] = $ordering_inactive;
		$data['ordering_active'] = $lms_active;
		$data['lms_active'] = $lms_active;
		$data['lms_inactive'] = $lms_inactive;
		$data['internact_active'] = $internact_active;
		$data['internact_inactive'] = $internact_inactive;
		$data['eventguest_active'] = $eventguest_active;
		$data['eventguest_inactive'] = $eventguest_inactive;
		$data['club_active'] = $club_active;
		$data['club_inactive'] = $club_inactive;
		$data['truck_stop_active'] = $truck_stop_active;
		$data['truck_stop_inactive'] = $truck_stop_inactive;
		$data['foodle_active'] = $foodle_active;
		$data['foodle_inactive'] = $foodle_inactive;
		$data['total_location'] = $public_active+$public_inactive+$hotel_active+$hotel_inactive+$hospital_active+$hospital_inactive+$institutional_active+$institutional_inactive+$enterprise_active+$enterprise_inactive+$caffe_active+$caffe_inactive+$retail_active+$retail_inactive+$custom_active+$custom_inactive+$purple_active+$purple_inactive+$offline_active+$offline_inactive+$visitor_active+$visitor_inactive+$contestifi_active+$contestifi_inactive+$retail_audit_active+$retail_audit_inactive+$event_module_active+$event_module_inactive+$ordering_active+$ordering_inactive+$lms_active+$lms_inactive+$internact_active+$internact_inactive+$eventguest_active+$eventguest_inactive+$club_active+$club_inactive+$truck_stop_active+$truck_stop_inactive+$foodle_active+$foodle_inactive;
		//echo "<pre>";print_r($data);die;
		return json_encode($data);
	}
	public function location_dashboard_new_listview($type, $state, $city,$serach_pattern){
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		elseif($type == 'visitor'){
			$type = '11';
		}
		elseif($type == 'contestifi'){
			$type = '12';
		}
		elseif($type == 'retail_audit'){
			$type = '13';
		}
		elseif($type == 'event_module'){
			$type = '14';
		}
		elseif($type == 'ordering'){
			$type = '15';
		}
		elseif($type == 'lms'){
			$type = '16';
		}
		elseif($type == 'internact'){
			$type = '17';
		}elseif($type == 'eventguest'){
			$type = '18';
		}
		elseif($type == 'club'){
			$type = '19';
		}
		elseif($type == 'truck_stop'){
			$type = '20';
		}
		elseif($type == 'foodle'){
			$type = '21';
		}
		else{
			$type = '';
		}
		$location_type = $type;
		$isp_uid = $this->isp_uid;
		//$isp_uid = '1111';
		$data = array();
		$active = 0;
		$inactive = 0;
		$location_type_where = '';
		if($location_type != ''){
			$location_type_where = "AND location_type=".$location_type."";
		}
		$state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		$serach_pattern_where = '';
		if($serach_pattern!= ''){
			$serach_pattern = strtolower($serach_pattern);
		    $serach_pattern_where = " AND (wl.state LIKE '%$serach_pattern%' OR wl.city LIKE '%$serach_pattern%' OR wl.location_name LIKE '%$serach_pattern%' OR wl.location_uid LIKE '%$serach_pattern%' OR wl.contact_person_name LIKE '%$serach_pattern%' OR wl.mobile_number LIKE '%$serach_pattern%' OR wl.pin LIKE '%$serach_pattern%' OR wlap.macid LIKE '%$serach_pattern%')";
		}
		$pubuidstates = ''; $cwhere = '';
		$sessiondata = $this->session->userdata('isp_session');
		$publicuser = $sessiondata['publicuser'];
		if($publicuser == '1'){
			$pubuid = $sessiondata['userid'];
			$pubuid_assignstatesQ = $this->DB2->query("SELECT assigned_region FROM sht_isp_public_users WHERE id='".$pubuid."'");
			if($pubuid_assignstatesQ->num_rows() > 0){
				$pubuidstates = $pubuid_assignstatesQ->row()->assigned_region;
				
				$cwhere .= " AND wl.state_id IN ($pubuidstates) ";
			}
		}
		//echo "<pre>";print_r($this->session->userdata('isp_session'));die;
		$query = $this->db->query("select wl.is_location_online , wl.location_type from wifi_location as wl left join wifi_location_access_point as wlap on (wl.location_uid = wlap.location_uid) where wl.is_deleted = '0' and wl.isp_uid = '$isp_uid' $cwhere $location_type_where $state_where $city_where $serach_pattern_where group by wl.location_uid");
		foreach($query->result() as $row){
				if($row->is_location_online == '1'){
					$active = $active+1;
				}else{
					$inactive = $inactive+1;
				}
			
			
		}
		$data['active'] = $active;
		$data['inactive'] = $inactive;
		
		$data['total_location'] = $active+$inactive;
		//echo "<pre>";print_r($data);die;
		return json_encode($data);
	}
	
	
	public function state_list_for_filter(){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."state_list_for_filter";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	// get city list
	public function city_list_for_filter($state_id, $city_id = ''){
		$requestData = array(
			'state_id' => $state_id,
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."city_list_for_filter";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select City</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->city_id == $city_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->city_id.'">'.$responce_data1->city_name.'</option>';
		}
		return $content;
	}


	

	
	

	public function location_list_export_to_excel($type, $state, $city,$serach_pattern,$online_filter){
		$data = array();
		$type_return = $type;
		$total_record = 0;
		$gen = '';
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		elseif($type == 'visitor'){
			$type = '11';
		}
		elseif($type == 'contestifi'){
			$type = '12';
		}
		elseif($type == 'retail_audit'){
			$type = '13';
		}
		elseif($type == 'event_module'){
			$type = '14';
		}
		elseif($type == 'ordering'){
			$type = '15';
		}
		elseif($type == 'lms'){
			$type = '16';
		}
		elseif($type == 'internact'){
			$type = '17';
		}elseif($type == 'eventguest'){
			$type = '18';
		}
		elseif($type == 'club'){
			$type = '19';
		}
		elseif($type == 'truck_stop'){
			$type = '20';
		}
		elseif($type == 'foodle'){
			$type = '21';
		}
		else{
			$type = '';
		}
		$isp_uid = $this->isp_uid;
		$location_type = $type;
		$locations = array();
		$online_offline_filter = '';
		if($online_filter != ''){
			$online_offline_filter = "AND is_location_online=".$online_filter."";
		}
		$location_type_where = '';
		if($location_type != ''){
			$location_type_where = "AND location_type=".$location_type."";
		}
		$state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		$serach_pattern_where = '';
		if($serach_pattern!= ''){
		    $serach_pattern_where = " AND (state LIKE '%$serach_pattern%' OR city LIKE '%$serach_pattern%' OR location_name LIKE '%$serach_pattern%' OR location_uid LIKE '%$serach_pattern%' OR contact_person_name LIKE '%$serach_pattern%' OR mobile_number LIKE '%$serach_pattern%')";
		}
		
		$query = $this->db->query("select * from wifi_location where is_deleted = '0' and isp_uid = '$isp_uid' $location_type_where $state_where $city_where $serach_pattern_where $online_offline_filter order by location_name");
		$record_found = 0;
		if($query->num_rows() > 0){
			$record_found = 1;
			$location_ids = array();
			foreach($query->result() as $locrow){
				$location_ids[] = $locrow->location_uid;
			}
			$location_ids = '"'.implode('", "', $location_ids).'"';
			// get devide model
			$device_model = array();
			$get_device = $this->db->query("select location_uid, device_model_name, router_type from wifi_location_access_point where location_uid IN ($location_ids)");
			if($get_device->num_rows() > 0){
			     foreach($get_device->result() as $de_row){
				  if(array_key_exists($de_row->location_uid, $device_model)){
				       $device_model[$de_row->location_uid]['device_model_name'] = $device_model[$de_row->location_uid]['device_model_name'].', '.$de_row->device_model_name;
				  }else{
				       $device_model[$de_row->location_uid]['router_type'] = $de_row->router_type;
				       $device_model[$de_row->location_uid]['device_model_name'] = $de_row->device_model_name;
				  }
				  
			     }
			}
			
			$this->load->library('excel');
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->createSheet();
				$objPHPExcel->setActiveSheetIndex(0);
				$styleArray = array(
					'font'  => array(
					'bold'  => false,
					'color' => array('rgb' => '000000'),
					'name'  => 'Tahoma',
					'size' => 14
				));
				//Set sheet style
				$styleArray1 = array(
					'font'  => array(
					'bold'  => false,
					'color' => array('rgb' => '000000'),
					'name'  => 'Tahoma',
					'size' => 12
				));
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
				
				$objPHPExcel->getActiveSheet()->setTitle("Locations");
				
				//Set sheet columns Heading
				$objPHPExcel->getActiveSheet()->setCellValue('A1','S.No');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Location ID');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Location');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('D1','State');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('E1','City');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Contect Person Name');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('G1','Nas IP');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('H1','Last Active');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('I1','Status');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('J1','Device Model');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
				$i = 1;
				$j = 2;// excel row number
			
			
			foreach($query->result() as $row){
				$is_location_online = $row->is_location_online;
				$nsimg = 'Offline';
				if($is_location_online == '1'){
					$nsimg = 'Online';
				}
				$device_name = '';
				$location_uid = $row->location_uid;
				if(isset($device_model[$location_uid])){
					if($device_model[$location_uid]['router_type'] == 6){
					     $device_name = $device_model[$location_uid]['device_model_name'];
					}
					else if($device_model[$location_uid]['router_type'] == 7){
					     $device_name = 'RASBERRY PI';
					}
				}
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$i);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$row->location_uid);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$row->location_name);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$row->state);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$row->city);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$row->contact_person_name);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$row->nasipaddress);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$j,$row->last_online_time);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$j,$nsimg);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$j,$device_name);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($styleArray1);
				$i++;
				$j++;
			}
			
			
			 $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('assets/location_list.xlsx');
			
		}
		return $record_found;
	}
	
	
	public function get_location_group($type){
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		elseif($type == 'visitor'){
			$type = '11';
		}
		elseif($type == 'contestifi'){
			$type = '12';
		}
		elseif($type == 'retail_audit'){
			$type = '13';
		}
		elseif($type == 'event_module'){
			$type = '14';
		}
		elseif($type == 'ordering'){
			$type = '15';
		}
		elseif($type == 'lms'){
			$type = '16';
		}
		elseif($type == 'internact'){
			$type = '17';
		}elseif($type == 'eventguest'){
			$type = '18';
		}
		elseif($type == 'club'){
			$type = '19';
		}
		elseif($type == 'truck_stop'){
			$type = '20';
		}
		elseif($type == 'foodle'){
			$type = '21';
		}
		else{
			$type = '';
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
                        'location_type' => $type
		);
		$service_url = $this->api_url."get_location_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		$data= array();
		
		$res = '';
		if(count($result) > 0){
		    $i = 1;
		    foreach($result as $row){
			$res .= "<tr>";
			$res .= "<td>".$i."</td>";
			$res .= '<td><input type="radio" name="group_select_radio" value="'.$row->group_id.'"></td>';
			$res .= "<td>".$row->group_name."</td>";
			$res .= "<td>".$row->locations." Locations</td>";
			$res .= "<td>".$row->username."</td>";
			$res .= "<td>".$row->password."</td>";
			$res .= '<td >';
			$res .= "<i onclick='edit_loc_group(&apos;".$row->group_id."&apos;, &apos;".$row->group_name."&apos;, &apos;".$row->username."&apos;, &apos;".$row->password."&apos;)' class='fa fa-edit fa-lg' aria-hidden='true' style='cursor:pointer'></i>"; 
					$res .= '&nbsp;&nbsp;&nbsp;<i onclick="delete_loc_group('.$row->group_id.')" class="fa fa-trash fa-lg" aria-hidden="true" style="cursor:pointer"></i>';
				$res .='</td>';
			$res .= "</tr>";
			$i++;
		    }
		}
		else{
		    $res .= '<tr><td colspan="7" style="text-align:center">No Group Found !!</td></tr>';
		}
		$data['search_results'] = $res;
		return $data;
                //return json_decode($curl_response);
		//print_r(json_decode($curl_response));die;
                
	}
	
	
	public function create_location_group(){
		$type = $this->input->post("location_type");
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		elseif($type == 'visitor'){
			$type = '11';
		}
		elseif($type == 'contestifi'){
			$type = '12';
		}
		elseif($type == 'retail_audit'){
			$type = '13';
		}
		elseif($type == 'event_module'){
			$type = '14';
		}
		elseif($type == 'ordering'){
			$type = '15';
		}
		elseif($type == 'lms'){
			$type = '16';
		}
		elseif($type == 'internact'){
			$type = '17';
		}elseif($type == 'eventguest'){
			$type = '18';
		}
		elseif($type == 'club'){
			$type = '19';
		}
		elseif($type == 'truck_stop'){
			$type = '20';
		}
		elseif($type == 'foodle'){
			$type = '21';
		}
		else{
			$type = '';
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'group_name' => $this->input->post("group_name"),
			'username' => $this->input->post("username"),
			'password' => $this->input->post("password"),
			'location_type' => $type,
			'locations' => $this->input->post("locations"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."create_location_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	
	public function add_location_to_group(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'group_id' => $this->input->post("group_id"),
			'locations' => $this->input->post("locations"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_location_to_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delete_loc_group(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'group_id' => $this->input->post("group_id")
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."delete_loc_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function update_location_group(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'group_name' => $this->input->post("group_name"),
			'username' => $this->input->post("username"),
			'password' => $this->input->post("password"),
			'group_id' => $this->input->post("group_id"),
			'locations' => $this->input->post("locations"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_location_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function get_group_location_list(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
                        'group_id' => $this->input->post("group_id"),
		);
		$service_url = $this->api_url."get_group_location_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		$data= array();
		
		$res = '';
		if(count($result) > 0){
		    $i = 1;
		    foreach($result as $row){
			$res .= "<tr>";
			$res .= "<td>".$i."</td>";
			$res .= '<td><input type="checkbox" name="group_location_list[]" value="'.$row->id.'"></td>';
			$res .= "<td>".$row->location_name."</td>";
			
			$res .= "</tr>";
			$i++;
		    }
		}
		else{
		    $res .= '<tr><td colspan="3" style="text-align:center">No Location Added !!</td></tr>';
		}
		$data['search_results'] = $res;
		return $data;
                //return json_decode($curl_response);
		//print_r(json_decode($curl_response));die;
                
	}
	
	
	public function add_otp_sms(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
			'locartion_id' => $this->input->post('locartion_id'),
			'otp_sms' => $this->input->post('otp_sms'),
			'sms_type' => $this->input->post('sms_type')
		);
		$service_url = $this->api_url."add_otp_sms";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->resultCode;
		
	}
	
	
	public function sms_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid,
			'location_id' => $this->input->post('location_id'),
		);
		$service_url = $this->api_url."sms_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			$i = 1;
			foreach($responce_data->vouchers as $responce_data1){
				$content .= '<tr>';
				$content .= '<td>'.$i.'</td>';
				$content .= '<td>'.$responce_data1->sms_type.' </td>';
				$content .= '<td>'.$responce_data1->created_on.' </td>';
				$content .= '<td>'.$responce_data1->total_sms.' </td>';
				$content .= '<td>'.$responce_data1->used_sms.' </td>';
				$content .= '<td>'.$responce_data1->sms_balance.' </td>';
				$content .= '</tr>';
				$i++;
			}
		}
		return $content;
		
	}
	
	public function offline_organisation_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."offline_organisation_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	
	
	public function add_offline_vm_organisation(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'offline_organisation_name' => $this->input->post("offline_organisation_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_offline_vm_organisation";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function vm_offline_department_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."vm_offline_department_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_offline_vm_department(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'offline_department_name' => $this->input->post("offline_department_name"),
			'org_id' => $this->input->post("org_id"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_offline_vm_department";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function vm_offline_employee_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."vm_offline_employee_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_vm_offline_employee(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'employee_name' => $this->input->post("employee_name"),
			'employee_last_name' => $this->input->post("employee_last_name"),
			'employee_email' => $this->input->post("employee_email"),
			'employee_mobile' => $this->input->post("employee_mobile"),
			'org_id' => $this->input->post("org_id"),
			'dept_id' => $this->input->post("dept_id"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_vm_offline_employee";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delete_offline_vm_organisation($organisation_id){
		$requestData = array(
			'organisation_id' => $organisation_id,
			'locationid' => $this->input->post("location_id"),
		);
		$service_url = $this->api_url."delete_offline_vm_organisation";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function delete_offline_vm_department($department_id){
		$requestData = array(
			'department_id' => $department_id,
			'locationid' => $this->input->post("location_id"),
		);
		$service_url = $this->api_url."delete_offline_vm_department";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function delete_offline_vm_employee($employee_id){
		$requestData = array(
			'employee_id' => $employee_id,
			'locationid' => $this->input->post("location_id"),
		);
		$service_url = $this->api_url."delete_offline_vm_employee";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}

	public function contestifi_content_builder_list(){
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
		);
		$service_url = $this->api_url."contestifi_content_builder_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_contistifi_quiz_question(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = '';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			$mime = $_FILES['file']['type'];
			if(strstr($mime, "video/")){
				$file_type = "video";
			}else if(strstr($mime, "image/")){
				$file_type = "image";
			}else if(strstr($mime, "audio/")){
				$file_type = "audio";
			}
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_id' => $this->input->post("contest_id"),
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'right_ans' => $this->input->post("right_ans"),
			'contest_file' => $content_file,
			'contest_file_type' => $file_type,
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_contistifi_quiz_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}

	public function delte_contestifi_quiz_question($question_id,$location_id){
		$requestData = array(
			'question_id' => $question_id,
			"location_id" => $location_id
		);
		$service_url = $this->api_url."delte_contestifi_quiz_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function get_contestifi_quiz_question(){
		$requestData = array(
			'question_id' => $this->input->post("question_id"),
		);
		$service_url = $this->api_url."get_contestifi_quiz_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_contistifi_quiz_question(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = '';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			$mime = $_FILES['file']['type'];
			if(strstr($mime, "video/")){
				$file_type = "video";
			}else if(strstr($mime, "image/")){
				$file_type = "image";
			}else if(strstr($mime, "audio/")){
				$file_type = "audio";
			}
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'quiz_question_id' => $this->input->post("quiz_question_id"),
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'contest_id' => $this->input->post("contest_id"),
			'right_ans' => $this->input->post("right_ans"),
			'contest_file' => $content_file,
			'contest_file_type' => $file_type,
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_contistifi_quiz_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}

	public function add_contestifi_contest(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$contestifi_contest_logo_image_original ="";
		$contestifi_contest_logo_image_logo ="";
		$contestifi_contest_logo_image_small ="";
		$image_name = $this->input->post("contestifi_contest_logo_image_name");
		$image_src = $this->input->post("contestifi_contest_logo_image_src");
		if($image_name != ''){
			if($image_src != ''){
				
				$file_ext = $this->getExtension($image_name);
				$explode = explode(',',$image_src);
				$image_src = $explode['1'];
				$image_src = str_replace(' ', '+', $image_src);
				$data_img = base64_decode($image_src);
				$filename = uniqid() . '.'.$file_ext;
				$file = $base_url . $filename;
				
				$success = file_put_contents($file, $data_img);
				$newname=date("Ymdhisv").rand().".".$file_ext; 
				rename($base_url.$filename,$base_url.$newname);
				
				$this->load->library('image_lib');
				$config1['image_library'] = 'gd2';
				$config1['source_image'] = $base_url .$newname;
				$config1['new_image'] = $base_url."300_".$newname;
		
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 300;
				$config1['height'] = 100;
				$this->image_lib->initialize($config1);
				$this->image_lib->resize();
		    
				$this->image_lib->clear();
				 $config2['image_library'] = 'gd2';
				 $config2['source_image'] = $base_url .$newname;
				 $config2['new_image'] = $base_url ."600_".$newname;
		  
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 600;
				$config2['height'] = 200;
				  $this->image_lib->initialize($config2);
				  $this->image_lib->resize();
		      
				$contestifi_contest_logo_image_original=$newname;
				$contestifi_contest_logo_image_logo="600_".$newname;
				$contestifi_contest_logo_image_small ="300_".$newname;
				 
				$fname = $base_url.$newname;
				$fname1 = $base_url ."300_".$newname;
				$fname2 = $base_url ."600_".$newname;
			   
				$famazonname = AMAZONPATH.'offline_video_image_content/'.$newname;
				$famazonname1 = AMAZONPATH.'offline_video_image_content/300_'. $newname;
				$famazonname2 = AMAZONPATH.'offline_video_image_content/600_'. $newname;
				$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
				$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				//unlink($fname);
				//unlink($fname1);
				//unlink($fname2);
					 
					
			}	
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_name' => $this->input->post("contest_name"),
			'contest_type' => $this->input->post("contest_type"),
			'bg_color' => $this->input->post("bg_color"),
			'contest_frequency' => $this->input->post("contest_frequency"),
			'contestifi_custom_frequecny' => $this->input->post("contestifi_custom_frequecny"),
			'contestifi_no_of_attemption' => $this->input->post("contestifi_no_of_attemption"),
			
			'contestifi_point_per_question' => $this->input->post("contestifi_point_per_question"),
			'contestifi_negative_point' => $this->input->post("contestifi_negative_point"),
			'contestifi_point_deduct_every_secod' => $this->input->post("contestifi_point_deduct_every_secod"),
			'contestifi_max_time_allocation' => $this->input->post("contestifi_max_time_allocation"),
			'contestifi_contest_logo_image_original' => $contestifi_contest_logo_image_original,
			'contestifi_contest_logo_image_logo' => $contestifi_contest_logo_image_logo,
			'contestifi_contest_logo_image_small' => $contestifi_contest_logo_image_small,
			'contestifi_contest_rule' => $this->input->post("contestifi_contest_rule"),
			'text_color' => $this->input->post("text_color"),
			'button_color' => $this->input->post("button_color"),
			'start_date' => $this->input->post("start_date"),
			'end_date' => $this->input->post("end_date"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_contestifi_contest";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delte_contestifi_contest($contest_id,$location_id){
		$requestData = array(
			'contest_id' => $contest_id,
			"location_id" => $location_id
		);
		$service_url = $this->api_url."delte_contestifi_contest";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	
	public function get_contestifi_contest_for_edit(){
		$requestData = array(
			'contest_id' => $this->input->post("contest_id"),
		);
		$service_url = $this->api_url."get_contestifi_contest_for_edit";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_contestifi_contest(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$contestifi_contest_logo_image_original ="";
		$contestifi_contest_logo_image_logo ="";
		$contestifi_contest_logo_image_small ="";
		$image_name = $this->input->post("contestifi_contest_logo_image_name");
		$image_src = $this->input->post("contestifi_contest_logo_image_src");
		if($image_name != ''){
			if($image_src != ''){
				
				$file_ext = $this->getExtension($image_name);
				$explode = explode(',',$image_src);
				$image_src = $explode['1'];
				$image_src = str_replace(' ', '+', $image_src);
				$data_img = base64_decode($image_src);
				$filename = uniqid() . '.'.$file_ext;
				$file = $base_url . $filename;
				
				$success = file_put_contents($file, $data_img);
				$newname=date("Ymdhisv").rand().".".$file_ext; 
				rename($base_url.$filename,$base_url.$newname);
				
				$this->load->library('image_lib');
				$config1['image_library'] = 'gd2';
				$config1['source_image'] = $base_url .$newname;
				$config1['new_image'] = $base_url ."300_".$newname;
		
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 300;
				$config1['height'] = 100;
				$this->image_lib->initialize($config1);
				$this->image_lib->resize();
		    
				$this->image_lib->clear();
				 $config2['image_library'] = 'gd2';
				 $config2['source_image'] = $base_url .$newname;
				 $config2['new_image'] = $base_url."600_".$newname;
		  
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 600;
				$config2['height'] = 200;
				  $this->image_lib->initialize($config2);
				  $this->image_lib->resize();
		      
				$contestifi_contest_logo_image_original=$newname;
				$contestifi_contest_logo_image_logo="600_".$newname;
				$contestifi_contest_logo_image_small ="300_".$newname;
				 
				$fname = $base_url.$newname;
				$fname1 = $base_url ."300_".$newname;
				$fname2 = $base_url."600_".$newname;
			   
				$famazonname = AMAZONPATH.'offline_video_image_content/'.$newname;
				$famazonname1 = AMAZONPATH.'offline_video_image_content/300_'. $newname;
				$famazonname2 = AMAZONPATH.'offline_video_image_content/600_'. $newname;
				$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
				$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				//unlink($fname);
				//unlink($fname1);
				//unlink($fname2);
					 
					
			}	
		}
		//$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_name' => $this->input->post("contest_name"),
			'contest_type' => $this->input->post("contest_type"),
			'bg_color' => $this->input->post("bg_color"),
			'contest_frequency' => $this->input->post("contest_frequency"),
			'contestifi_custom_frequecny' => $this->input->post("contestifi_custom_frequecny"),
			'contestifi_no_of_attemption' => $this->input->post("contestifi_no_of_attemption"),
			
			'contestifi_point_per_question' => $this->input->post("contestifi_point_per_question"),
			'contestifi_negative_point' => $this->input->post("contestifi_negative_point"),
			'contestifi_point_deduct_every_secod' => $this->input->post("contestifi_point_deduct_every_secod"),
			'contestifi_max_time_allocation' => $this->input->post("contestifi_max_time_allocation"),
			'contestifi_contest_logo_image_original' => $contestifi_contest_logo_image_original,
			'contestifi_contest_logo_image_logo' => $contestifi_contest_logo_image_logo,
			'contestifi_contest_logo_image_small' => $contestifi_contest_logo_image_small,
			'contestifi_contest_rule' => $this->input->post("contestifi_contest_rule"),
			'contest_id' => $this->input->post("contest_id"),
			'text_color' => $this->input->post("text_color"),
			'button_color' => $this->input->post("button_color"),
			'start_date' => $this->input->post("start_date"),
			'end_date' => $this->input->post("end_date"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_contestifi_contest";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_contistifi_contest_prize(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_id' => $this->input->post("contest_id"),
			'contestifi_contest_prize' => $this->input->post("contestifi_contest_prize"),
			'prize_id' => $this->input->post("prize_id"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_contistifi_contest_prize";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function delte_contestifi_contest_prize($prize_id,$location_id){
		$requestData = array(
			'prize_id' => $prize_id,
			"location_id" => $location_id
		);
		$service_url = $this->api_url."delte_contestifi_contest_prize";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	
	public function contestifi_add_content_builder(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."contestifi_add_content_builder";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function get_contestifi_signup_term_condition($location_id){
		$requestData = array(
			'location_id' => $location_id
		);
		$service_url = $this->api_url."get_contestifi_signup_term_condition";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_contestifi_terms_conditon(){
		$requestData = array(
			'location_id' => $this->input->post('location_id'),
			'terms' => $this->input->post('terms'),
		);
		$service_url = $this->api_url."add_contestifi_terms_conditon";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_contistifi_survey_question(){
		
		$content_file = '';
		$file_type = '';
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_id' => $this->input->post("contest_id"),
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'survey_type' => $this->input->post("survey_type"),
			'contest_file' => $content_file,
			'contest_file_type' => $file_type,
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_contistifi_survey_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function update_contistifi_survey_question(){
		
		$content_file = '';
		$file_type = '';
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'quiz_question_id' => $this->input->post("quiz_question_id"),
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'contest_id' => $this->input->post("contest_id"),
			'survey_type' => $this->input->post("survey_type"),
			'contest_file' => $content_file,
			'contest_file_type' => $file_type,
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_contistifi_survey_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function retail_audit_sku_pin_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."retail_audit_sku_pin_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_retail_audit_sku(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'sku_name' => $this->input->post("sku_name"),
			'sku_id' => $this->input->post("sku_id"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_retail_audit_sku";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_retail_audit_pin(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'login_pin' => $this->input->post("login_pin")
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_retail_audit_pin";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_retail_audit_pin_promoter(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'promoter_pin' => $this->input->post("login_pin")
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_retail_audit_pin";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content_survey_retail_audit(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'survey';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'questions' => json_decode($this->input->post("questions")),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_survey_retail_audit";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function get_content_poll_survey_retail_audit(){
		$requestData = array(
			'content_id' => $this->input->post("content_id"),
		);
		$service_url = $this->api_url."get_content_poll_survey_retail_audit";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_content_survey_retail_audit(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'survey';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'content_id' => $this->input->post("content_id"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
			'questions' => json_decode($this->input->post("questions")),
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_content_survey_retail_audit";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delete_retail_audit_sku_name($sku_id){
		$requestData = array(
			'sku_id' => $sku_id,
			'locationid' => $this->input->post("location_id"),
		);
		$service_url = $this->api_url."delete_retail_audit_sku_name";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function add_event_module_signup_extra_field(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'field_name' => $this->input->post("field_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_event_module_signup_extra_field";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delete_event_module_signup_field($field_id){
		$requestData = array(
			'field_id' => $field_id,
			'locationid' => $this->input->post("location_id"),
		);
		$service_url = $this->api_url."delete_event_module_signup_field";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function hash_tag_search($search_val){
		$serach_pattern = $search_val;
		$isp_uid= $this->isp_uid;
		$query = $this->db->query("select hashtags from wifi_location_hashtags where hashtags LIKE '%$serach_pattern%' ");
		$content = '';
		$content .= '<div class="row">';
		if($query->num_rows() > 0)
		{
			$i = 0;
			foreach($query->result() as $row){
				if($i ==6)
				{
					break;
				}
				else
				{
					$content .= '<div class="search-dropdown " style="padding: 5px 10px;">
                                <h5 class="select_tags" style="margin:0px; font-size:13px;font-weight:400">'.$row->hashtags.'</h5>
                                </div>';
				}
		 
		 $i++;
	     }
			
		}
		$content .= '</div>';
		
		
		return  $content;
	}



		/*******************************************************************
	 *	FOOD IQ FUNCTIONS STARTS
	 ******************************************************************/
	
	public function checkwifistatus(){
		$data = array();
		$wifistatus = $this->input->post('wifistatus');
		$location_uid = $this->input->post('location_uid');
		$is_wifi_enabled = '0';
		
		$this->db->select('is_wifi_enabled')->from('wifi_location');
		$this->db->where('location_uid', $location_uid);
		$statusQuery = $this->db->get();
		if($statusQuery->num_rows() > 0){
			$rawdata = $statusQuery->row();
			$wifistatus = $rawdata->is_wifi_enabled;
			
			if($wifistatus == '1'){
				$is_wifi_enabled = '0';
				$this->db->update('wifi_location', array('is_wifi_enabled' => '0'), array('location_uid' => $location_uid));
			}else{
				$is_wifi_enabled = '1';
				$this->db->update('wifi_location', array('is_wifi_enabled' => '1'), array('location_uid' => $location_uid));
			}
		}
		
		$data['is_wifi_enabled'] = $is_wifi_enabled;
		echo json_encode($data);
	}
	
	public function check_passiveprobe_status(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$is_psvprb_enabled = '0';
		
		$this->db->select('is_passiveprob_enabled')->from('wifi_location');
		$this->db->where('location_uid', $location_uid);
		$statusQuery = $this->db->get();
		if($statusQuery->num_rows() > 0){
			$rawdata = $statusQuery->row();
			$psvprbstatus = $rawdata->is_passiveprob_enabled;
			
			if($psvprbstatus == '1'){
				$is_psvprb_enabled = '0';
				$this->db->update('wifi_location', array('is_passiveprob_enabled' => '0'), array('location_uid' => $location_uid));
			}else{
				$is_psvprb_enabled = '1';
				$this->db->update('wifi_location', array('is_passiveprob_enabled' => '1'), array('location_uid' => $location_uid));
			}
		}
		
		$data['is_passiveprob_enabled'] = $is_psvprb_enabled;
		echo json_encode($data);
	}
	public function getall_formdetails(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$edit_productid = $this->input->post('edit_productid');
		
		$supplier_arr = array();
		$getmenusupplierQ = $this->db->query("SELECT tb2.supplier_name FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id=tb2.id) WHERE tb1.location_uid='".$location_uid."' GROUP BY tb2.supplier_name");
		if($getmenusupplierQ->num_rows() > 0){
			$i = 0;
			foreach($getmenusupplierQ->result() as $menuobj){
				$supplier_arr[$i] = $menuobj->supplier_name;
				$i++;
			}
		}
		
		//print_r($supplier_arr);
		
		$supplieropt = '<option value = "">Select Supplier</option>';
		$supplierQuery = $this->db->order_by('supplier_name', 'ASC')->get('foodiq_supplier');	
		if($supplierQuery->num_rows() > 0){
			foreach($supplierQuery->result() as $suplrobj){
				$sel = '';
				$supplier_name = $suplrobj->supplier_name;
				if(in_array($supplier_name, $supplier_arr)){
					$sel = "&#10003;";	
				}
				$supplieropt .= '<option value="'.$suplrobj->id.'">'.$suplrobj->supplier_name.' '.$sel.'</option>';
			}
		}
		$data['supplier_list'] = $supplieropt;
		
		echo json_encode($data);
	}
	public function manageproductdetails(){
		
		$postdata = $this->input->post();
		$enable_food_ordering = $this->input->post('enable_food_ordering');
		$edit_productid = $this->input->post('edit_productid');
		$location_uid = $postdata['location_uid'];
		$location_id = $postdata['locationid'];
		$supplier_id = $postdata['food_item_supplier_name'];
		//$supplier_rol = $postdata['supplier_rol'];
		
		$supplierQuery = $this->db->get_where('foodiq_supplier', array('id' => $supplier_id));
		if($supplierQuery->num_rows() > 0){
			foreach($supplierQuery->result() as $suplrobj){
				$supplier_name = $suplrobj->supplier_name;	
				$this->db->select("id, status")->from('foodiq_master_menulist');
				$this->db->where(array('isp_uid' => ISPID, 'supplier_name' => $supplier_name, 'status' => '1', 'is_deleted' => '0'));
				$menuQuery = $this->db->get();
				if($menuQuery->num_rows() > 0){
					foreach($menuQuery->result() as $uniobj){
						$productArr = array
						(
							'isp_uid' => ISPID,
							'location_id' => $location_id,
							'location_uid' => $location_uid,
							'master_menu_id' => $uniobj->id,
							'status' => $uniobj->status
						);
						
						$foodmenubarcodeQ = $this->db->query("SELECT id FROM foodiq_menulist WHERE location_uid = '".$location_uid."' AND  master_menu_id = '".$uniobj->id."'");
						if($foodmenubarcodeQ->num_rows() > 0){
							$last_insertid = $foodmenubarcodeQ->row()->id;
							$this->db->update('foodiq_menulist', $productArr, array('id' => $last_insertid));
						}
						else{
							$productArr['added_on'] = date('Y-m-d H:i:s');
							$productArr['food_item_quantity'] = '0';
							$this->db->insert('foodiq_menulist', $productArr);
							$last_insertid = $this->db->insert_id();
						}
					}
				}
			}
		}
		
		echo json_encode(true);
	}
	
	public function get_products_listing(){
		$data = array();
		$content = '';
		$location_uid = $this->input->post('location_uid');
		
		$this->db->select('tb1.food_item_quantity, tb2.*')->from('foodiq_menulist as tb1');
		$this->db->join('foodiq_master_menulist as tb2', 'tb2.id = tb1.master_menu_id', 'inner');
		$this->db->where(array('tb1.location_uid' =>  $location_uid, 'tb1.status' => '1'));
		$this->db->order_by('tb2.product_titlename', 'ASC');
		$foodmenuQ = $this->db->get();
		if($foodmenuQ->num_rows() > 0){
			$i = 1;
			foreach($foodmenuQ->result() as $fobj){
				$menuoption = '';
				$menu_display_option = $fobj->menu_display_option;
				if($menu_display_option == '1'){
					$menuoption = '<td>'.$fobj->food_item_barcode.'</td>';
				}else{
					$menuoption = '<td>Direct to Cart</td>';
				}
				
				$is_perishable = 'NO';
				if($fobj->is_perishable == '1'){
					$is_perishable = 'YES';
				}
				$content .= '<tr><td>'.$i.'</td><td>'.$fobj->menu_categoryname.'</td><td>'.$fobj->product_titlename.'</td>'.$menuoption.'<td>'.$fobj->supplier_name.'</td><td>'.$is_perishable.'</td><td>'.$fobj->food_item_quantity.'</td><td>'.$fobj->food_item_price.'</td></tr>';
				$i++;
			}
		}else{
			$content = '<tr><td colspan="6" style="text-align:center">No Products Found !!</td></tr>';
		}
		
		$data['productlist'] = $content;
		echo json_encode($data);
	}
	
	public function adddietaryoption(){
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$dietary_name = trim($this->input->post('dietary_name'));
		$this->db->insert('foodiq_dietary_preferences', array('isp_uid' => ISPID, 'location_uid' => $location_uid, 'dietary_name' => $dietary_name));
		echo json_encode(true);
	}
	public function dietaryoptionlists(){
		$data = array();
		$content = '';
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$dietaryQ = $this->db->query("select id, dietary_name from foodiq_dietary_preferences where isp_uid = ".ISPID);
		if($dietaryQ->num_rows() > 0){
			$i = 1;
			foreach($dietaryQ->result() as $fobj){
				
				$content .= '<tr><td>'.$i.'</td><td>'.$fobj->dietary_name.'</td><td><a href="javascript:void(0)" onclick="delete_dietaryoption('.$fobj->id.')"><i class="fa fa-trash"></i></a></td></tr>';
				$i++;
			}
		}else{
			$content = '<tr><td colspan="3" style="text-align:center">No Dietary Found !!</td></tr>';
		}
		
		$data['dietarylist'] = $content;
		echo json_encode($data);
	}
	
	public function delete_dietaryoption(){
		$dietid = $this->input->post('dietid');
		$this->db->delete('foodiq_dietary_preferences', array('id' => $dietid));
		echo json_encode(true);
	}

	public function addallergyoption(){
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$allergy_name = trim($this->input->post('allergy_name'));
		$this->db->insert('foodiq_allergies', array('isp_uid' => ISPID, 'location_uid' => $location_uid, 'allergy_name' => $allergy_name));
		echo json_encode(true);
	}
	public function allergyoptionlists(){
		$data = array();
		$content = '';
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$dietaryQ = $this->db->query("select id, allergy_name from foodiq_allergies where isp_uid = ".ISPID);
		if($dietaryQ->num_rows() > 0){
			$i = 1;
			foreach($dietaryQ->result() as $fobj){
				
				$content .= '<tr><td>'.$i.'</td><td>'.$fobj->allergy_name.'</td><td><a href="javascript:void(0)" onclick="delete_allergyoption('.$fobj->id.')"><i class="fa fa-trash"></i></a></td></tr>';
				$i++;
			}
		}else{
			$content = '<tr><td colspan="3" style="text-align:center">No Allergy Options Found !!</td></tr>';
		}
		
		$data['allergylist'] = $content;
		echo json_encode($data);
	}
	
	public function delete_allergyoption(){
		$allergyid = $this->input->post('allergyid');
		$this->db->delete('foodiq_allergies', array('id' => $allergyid));
		echo json_encode(true);
	}
	
	
	public function getwallet_details(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$bankdetQ = $this->db->query("SELECT * FROM foodiq_bank_details WHERE isp_uid='".ISPID."' AND location_uid='".$location_uid."'");
		if($bankdetQ->num_rows() > 0){
			$bankrawdata = $bankdetQ->row();
			$data['stripe_username'] = $bankrawdata->stripe_username;
			$data['stripe_password'] = $bankrawdata->stripe_password;
			$data['stripe_secret_key'] = $bankrawdata->stripe_secret_key;
			$data['stripe_publication_key'] = $bankrawdata->stripe_publication_key;
			$data['registration_amount'] = $bankrawdata->registration_amount;
		}else{
			$data['stripe_username'] = '';
			$data['stripe_password'] = '';
			$data['stripe_secret_key'] = '';
			$data['stripe_publication_key'] = '';
			$data['registration_amount'] = '0';
		}
		
		/*
		$slabs = ''; $slab_total_amount = 0;
		$walletdetQ = $this->db->query("SELECT id, slab_amount FROM foodiq_wallet_slabs WHERE isp_uid='".ISPID."' AND location_uid='".$location_uid."'");
		if($walletdetQ->num_rows() > 0){
			foreach($walletdetQ->result() as $walletobj){
				$slab_total_amount += $walletobj->slab_amount;
				$slabs .= '<button type="button" class="mui-btn mui-btn--outline-network btn-sm" >'.$walletobj->slab_amount.'</button> &nbsp; <a href="javascript:void(0)" onclick="deleteslab('.$walletobj->id.')"><i class="fa fa-trash"></i></a> &nbsp;  &nbsp; ';
			}
		}
		$data['slabs'] = $slabs;
		$data['slabs_count'] = $walletdetQ->num_rows();
		$data['slab_total_amount'] = $slab_total_amount;
		
		$walletlmtdetQ = $this->db->query("SELECT wallet_limit FROM foodiq_wallet_limit WHERE isp_uid='".ISPID."' AND location_uid='".$location_uid."'");
		if($walletlmtdetQ->num_rows() > 0){
			$data['wallet_limit'] = $walletlmtdetQ->row()->wallet_limit;
		}else{
			$data['wallet_limit'] = '';
		}
		*/
		echo json_encode($data);
	}
	
	public function managewalletslabs(){
		//$max_wallet_limit = $this->input->post('max_wallet_limit');
		//$finalslab_amount = $this->input->post('finalslab_amount');
		$stripe_username = $this->input->post('stripe_username');
		$stripe_password = $this->input->post('stripe_password');
		$stripe_secret_key = $this->input->post('stripe_secret_key');
		$stripe_publication_key = $this->input->post('stripe_publication_key');
		$location_uid = $this->input->post('location_uid');
		$registration_amt = $this->input->post('registration_amt');
		
		$bankdetQ = $this->db->query("SELECT id FROM foodiq_bank_details WHERE isp_uid='".ISPID."' AND location_uid='".$location_uid."'");
		if($bankdetQ->num_rows() > 0){
			$id = $bankdetQ->row()->id;
			$this->db->update('foodiq_bank_details', array('stripe_username' => $stripe_username, 'stripe_password' => $stripe_password, 'stripe_secret_key' => $stripe_secret_key, 'stripe_publication_key' => $stripe_publication_key, 'registration_amount' => $registration_amt), array('id' => $id));
		}
		else{
			$this->db->insert('foodiq_bank_details', array('isp_uid' => ISPID, 'location_uid' => $location_uid, 'stripe_username' => $stripe_username, 'stripe_password' => $stripe_password, 'stripe_secret_key' => $stripe_secret_key, 'stripe_publication_key' => $stripe_publication_key, 'registration_amount' => $registration_amt));
		}
		
		/*
		if(count($finalslab_amount) > 0){
			foreach($finalslab_amount as $slab_amt){
			$this->db->insert('foodiq_wallet_slabs', array('isp_uid' => ISPID, 'location_uid' => $location_uid, 'slab_amount' => $slab_amt, 'added_on' => date('Y-m-d H:i:s')));
			}
		}
		
		$walletlmtdetQ = $this->db->query("SELECT wallet_limit FROM foodiq_wallet_limit WHERE isp_uid='".ISPID."' AND location_uid='".$location_uid."'");
		if($walletlmtdetQ->num_rows() > 0){
			$this->db->update('foodiq_wallet_limit', array('wallet_limit' => $max_wallet_limit), array('isp_uid' => ISPID, 'location_uid' => $location_uid) );
		}else{
			$this->db->insert('foodiq_wallet_limit', array('wallet_limit' => $max_wallet_limit, 'isp_uid' => ISPID, 'location_uid' => $location_uid) );
		}
		*/
		echo json_encode(true);
	}
	
	public function deleteslab(){
		$slabid = $this->input->post('slabid');
		$this->db->delete('foodiq_wallet_slabs', array('id' => $slabid));
		echo json_encode(true);
	}
	public function managepayrollemployee(){
		$postdata = $this->input->post();
		$emp_id = $postdata['emp_id'];
		$payrolldata = array(
			'isp_uid' => ISPID,
			'location_uid' =>$postdata['location_uid'],
			'firstname' => $postdata['emp_first_name'],
			'lastname' => $postdata['emp_last_name'],
			'uid' => $postdata['emp_mobile_number'],
			'employee_id' => $postdata['emp_employee_id'],
			'company_name' => $postdata['emp_company_name'],
			'dob' => $postdata['emp_dob'],
			'email' => $postdata['emp_email']
		);
		
		if($emp_id != ''){
			$payrolldata['updated_on'] = date('Y-m-d H:i:s');
		}else{
			$payrolldata['added_on'] = date('Y-m-d H:i:s');
		}
		
		$payrollQuery = $this->db->query(" SELECT id FROM `foodiq_payroll_users` WHERE id='".$emp_id."' ");
		if($payrollQuery->num_rows() > 0){
			$this->db->update('foodiq_payroll_users', $payrolldata, array('id' => $emp_id));
		}else{
			$this->db->insert('foodiq_payroll_users', $payrolldata);
		}
		
		echo json_encode(true);
	}
	
	public function get_employee_details(){
		$data = array();
		$postdata = $this->input->post();
		$location_uid = $postdata['location_uid'];
		
		$content = '';
		$payrollQuery = $this->db->query(" SELECT firstname, lastname, uid, employee_id, payroll_debit  FROM `foodiq_payroll_users` WHERE location_uid='".$location_uid."' ");
		if($payrollQuery->num_rows() > 0){
			$i = 1;
			foreach($payrollQuery->result() as $payobj){
				$payroll_status = '';
				$payroll_debit = $payobj->payroll_debit;
				if($payroll_debit == '1'){
					$payroll_status = 'Active';
				}else{
					$payroll_status = 'Inactive';
				}
				$content .= '<tr><td>'.$i.'</td><td>'.$payobj->employee_id.'</td><td>'.$payobj->firstname.' '.$payobj->lastname.'</td><td>'.$payobj->uid.'</td><td>'.$payroll_status.'</td><td>Action</td></tr>';
				$i++;
			}
		}
		else{
			$content .= '<tr><td cospan="7">No Employee Data Found.</td></tr>';
		}
		
		$data['employee_details'] = $content;
		echo json_encode($data);
	}
	public function update_privacy_policy($location_uid,$text_value){
		$data = array();
		$query = $this->db->query("select privacy_policy from wifi_location_cptype where uid_location = '$location_uid'");
		
		if($query->num_rows() > 0){
		  $tabledata=array("privacy_policy" => $text_value);
		  $this->db->update('wifi_location_cptype', $tabledata,array("uid_location"=>$location_uid));
		  $data['result_msg'] = 'successfully updated';
		}else{
		  $tabledata=array("privacy_policy "=> $text_value, "uid_location" => $location_uid);
		  $this->db->insert('wifi_location_cptype',$tabledata);
		  $data['result_msg'] = 'successfully inserted';
		}
		
		return $data;
	}
	public function showprivacy_policy($location_uid){
		$data = array();
		$query = $this->db->query("select privacy_policy from wifi_location_cptype where uid_location = '$location_uid'");
		if($query->num_rows() > 0){
		  $row = $query->row_array();
		  $data['content'] = $row['privacy_policy'];
		}else{
		  $data['content'] = '';
		}
		
		return $data;
	}
	
	
	public function createfoodsupplier(){
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		
		$supplierArr = array();
		$supplierQuery = $this->db->query("select DISTINCT(supplier_name) as supplier_name from foodiq_menulist WHERE supplier_name != ''");
		if($supplierQuery->num_rows() > 0){
			foreach($supplierQuery->result() as $suplrobj){
				$supplierArr[] = $suplrobj->supplier_name;
			}
		}
		
		$supplier_name = trim($this->input->post('supplier_name'));
		if(in_array($supplier_name, $supplierArr)){
		     $data['code'] = 0;
		}
		else{
		     $data['code'] = 1;
		}
		return json_encode($data);
	}
	
	public function getreturn_policy(){
		$data = array();
		$policyopt = '<option value="">Select Return Policy</option>';
		$isp_uid = ISPID;
		$this->db->select('id, policy_name')->from('foodiq_return_policy');
		$this->db->where(array('isp_uid' => $isp_uid));
		$returnpolicyQ = $this->db->get();
		if($returnpolicyQ->num_rows() > 0){
			foreach($returnpolicyQ->result() as $robj){
				$policyopt .= '<option value="'.$robj->id.'">'.$robj->policy_name.'</option>';
			}
		}
		
		$data['return_policy_list'] = $policyopt;
		return $data;
	}
	
	public function update_items_price_with_surgefactor($created_location_id, $surgefactor){
		$this->db->select('location_price_surge')->from('wifi_location');
		$this->db->where('id', $created_location_id);
		$checkprevsurgeQ = $this->db->get();
		if($checkprevsurgeQ->num_rows() > 0){
			$prevsurge = $checkprevsurgeQ->row()->location_price_surge;
			if($prevsurge != $surgefactor){
				$this->db->select('id, food_item_price')->from('foodiq_menulist');
				$this->db->where(array('isp_uid' => ISPID, 'location_id' => $created_location_id));
				$checkallmenuQ = $this->db->get();
				if($checkallmenuQ->num_rows() > 0){
					foreach($checkallmenuQ->result() as $mobj){
						$item_id = $mobj->id;
						$item_price = $mobj->food_item_price;
						
						$item_price_without_surge = $item_price - ($item_price * ($prevsurge / 100) );
						$new_item_price = round( ($item_price_without_surge + ($item_price_without_surge * ($surgefactor / 100))) ,2);
						
						$this->db->update('foodiq_menulist', array('food_item_price' => $new_item_price), array('id' => $item_id));
					}
				}
			}
		}
		
		return 1;
	}
	public function edititem_pricedetails(){
		$data = array();
		$postdata = $this->input->post();
		$location_uid = $postdata['location_uid'];
		$menu_id = $postdata['menu_id'];
		$isp_uid = ISPID;
		
		$location_price_surge = '0.00';
		$this->db->select('location_price_surge')->from('wifi_location');
		$this->db->where(array('location_uid' => $location_uid));
		$checklocationsQ = $this->db->get();
		if($checklocationsQ->num_rows() > 0){
			$location_price_surge = $checklocationsQ->row()->location_price_surge;
		}
		
		
		$this->db->select('food_item_price')->from('foodiq_menulist');
		$this->db->where('id', $menu_id);
		$itemdetQ = $this->db->get();
		if($itemdetQ->num_rows() > 0){
			$data = $itemdetQ->row_array();
			$item_price = $data['food_item_price'];
			$data['base_price'] = round( ( ($item_price * 100) / (100 + $location_price_surge) ), 2);
			$data['price_surge_factor'] = $location_price_surge;
		}
		
		echo json_encode($data);
		
	}

	/*******************************************************************
	 *	FOOD IQ INVENTORY FUNCTIONS STARTS
	 ******************************************************************/
	
	public function add_storage_location(){
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$storage_name = trim($this->input->post('storage_name'));
		$check = $this->db->query("select id from foodiq_location_storage where isp_uid = '$isp_uid' AND storage_location_name = '$storage_name' AND status = '1'");
		if($check->num_rows() > 0){
		     $data['code'] = 0;
		}
		else{
		     $data['code'] = 1;
		     $insert = $this->db->query("insert into foodiq_location_storage (isp_uid, location_uid, storage_location_name, status, added_on) values('".ISPID."', '$location_uid', '$storage_name', '1', now())");
		     $location_storage_id = $this->db->insert_id();
		     
		     $this->db->query("UPDATE foodiq_location_storage SET status='0' WHERE isp_uid = '$isp_uid' AND location_uid = '$location_uid' AND id != '$location_storage_id'");
		     
		     $data['location_storage_id'] = $location_storage_id;
		}
		return json_encode($data);
	}
	
	public function add_location_refiller(){
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$storage_refiller_name = trim($this->input->post('refiller_name'));
		$storage_refiller_mobile = trim($this->input->post('refiller_mobile'));
		$storage_refiller_password = trim($this->input->post('refiller_password'));
		$storage_refiller_uid = trim($this->input->post('refiller_uid'));
		
		$check = $this->db->query("select id from foodiq_location_refiller where isp_uid = '$isp_uid' AND refiller_mobile = '$storage_refiller_mobile' AND status = '1' AND is_deleted = '0'");
		if($check->num_rows() > 0){
		     $data['code'] = 0;
		}
		else{
		     $data['code'] = 1;
		     $insert = $this->db->query("insert into foodiq_location_refiller (isp_uid, location_uid, refiller_name, refiller_mobile ,refiller_password, refiller_uid, added_on) values('".ISPID."', '$location_uid', '$storage_refiller_name', '$storage_refiller_mobile', '$storage_refiller_password', '$storage_refiller_uid', now())");
		     $refiller_id = $this->db->insert_id();
		     $data['refiller_id'] = $refiller_id;
		}
		return json_encode($data);
	}
	
	public function add_location_shelves(){
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$shelve_name = trim($this->input->post('shelve_name'));
		$check = $this->db->query("select id from foodiq_location_shelves where isp_uid = '$isp_uid' AND shelve_name = '$shelve_name' AND status = '1' AND is_deleted = '0'");
		if($check->num_rows() > 0){
		     $data['code'] = 0;
		}
		else{
		     $data['code'] = 1;
		     $insert = $this->db->query("insert into foodiq_location_shelves (isp_uid, location_uid, shelve_name, added_on) values('".ISPID."', '$location_uid', '$shelve_name', now())");
		     $shelve_id = $this->db->insert_id();
		     $data['shelve_id'] = $shelve_id;
		}
		return json_encode($data);
	}
	
	public function getinventory_settings(){
		$data = array();
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		
		$location_storage_id = ''; $location_refiller_id = ''; $location_shelve_id = '';
		$refillers_ids = array();
		$checkinventorySettingQ = $this->db->query("SELECT location_storage_id, location_refiller_uid, location_shelve_id, items_rol  FROM foodiq_inventory_settings WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."' AND status='1' AND is_deleted='0'");
		if($checkinventorySettingQ->num_rows() > 0){
			$invobj = $checkinventorySettingQ->row();
			$items_rol = $invobj->items_rol;
			$dbrefillerids = $invobj->location_refiller_uid;
			
			if(strpos($dbrefillerids, ',') !== FALSE){
				$refillers_ids = explode(',' , $dbrefillerids);
			}else{
				$refillers_ids[0] = $dbrefillerids;
			}
			
			$storagelist = "<option value = ''>Select Storage Location</option>";
			$storagelocQ = $this->db->query("SELECT id,location_uid,storage_location_name FROM foodiq_location_storage WHERE isp_uid = '$isp_uid' AND status = '1'");
			if($storagelocQ->num_rows() > 0){
				foreach($storagelocQ->result() as $storobj){
					$strsel = '';
					if($storobj->id == $invobj->location_storage_id){
						$strsel = 'selected=selected';
					}
					$storagelist .= "<option value = '".$storobj->id."' ".$strsel.">".$storobj->storage_location_name."</option>";
				}
			}
			
			$refillerlist = "<option value = ''>Select Refiller</option>";
			$refillerlocQ = $this->db->query("SELECT id,refiller_name,refiller_uid FROM foodiq_location_refiller WHERE isp_uid = '$isp_uid' AND status = '1' AND is_deleted='0'");
			if($refillerlocQ->num_rows() > 0){
				foreach($refillerlocQ->result() as $rstorobj){
					$rstrsel = '';
					if($rstorobj->id == $invobj->location_refiller_uid){
						$rstrsel = 'selected=selected';
					}
					$refillerlist .= "<option value = '".$rstorobj->id."' ".$rstrsel.">".$rstorobj->refiller_name." (".$rstorobj->refiller_uid.")</option>";
				}
			}
		}
		
		/*$shelveslocQ = $this->db->query("SELECT id,shelve_name FROM foodiq_location_shelves WHERE isp_uid = '$isp_uid' AND status = '1' AND is_deleted='0'");
		if($shelveslocQ->num_rows() > 0){
			foreach($shelveslocQ->result() as $sstorobj){
				$sstrsel = '';
				if($sstorobj->id == $invrawdata->location_shelve_id){
					$sstrsel = 'selected=selected';
				}
				$shelveslist .= "<option value = '".$sstorobj->id."' ".$sstrsel.">".$sstorobj->shelve_name."</option>";
			}
		}*/
		
		$data['storagelist'] = $storagelist;
		$data['refillerlist'] = $refillerlist;
		$data['items_rol'] = $items_rol;
		$data['refillers_ids'] = $refillers_ids;
		
		echo json_encode($data);
	}
	
	public function manage_inventory_settings(){
		$data = array();
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$storage_location = $this->input->post('storage_location');
		$refiller_id = $this->input->post('storage_refiller');
		$items_rol = $this->input->post('items_rol');
		
		/*if( strpos($storage_refiller_arr, ',') !== false ) {
			$storage_refiller_arr = explode(',' , $storage_refiller_arr);
		}else{
			$storage_refiller_arr = array($storage_refiller_arr);
		}*/
		
		$checkinventorySettingQ = $this->db->query("SELECT id FROM foodiq_inventory_settings WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."'");
		if($checkinventorySettingQ->num_rows() == 0){
			$this->db->insert('foodiq_inventory_settings', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'location_storage_id' => $storage_location, 'location_refiller_uid' => $refiller_id, 'location_shelve_id' => $location_uid, 'items_rol' => $items_rol, 'added_on' => date('Y-m-d H:i:s')));
		}else{
			$this->db->update('foodiq_inventory_settings', array( 'location_storage_id' => $storage_location, 'location_refiller_uid' => $refiller_id, 'location_shelve_id' => $location_uid, 'items_rol' => $items_rol, 'status' => '1', 'is_deleted' => '0'), array('isp_uid' => $isp_uid, 'location_uid' => $location_uid) );
		}
		
		//echo $this->db->last_query(); die;
		
		echo json_encode(true);
	}
	
	public function getinventory_actions(){
		$data = array();
		$content = '<option value="">Select Menu</option>';
		$refiller = '<option value="">Select Refiller</option>';
		$location_uid = $this->input->post('location_uid');
		$isp_uid = ISPID;
		
		$refiller_uid = '';
		$getitem_rol = $this->db->query("SELECT location_refiller_uid FROM foodiq_inventory_settings WHERE isp_uid='$isp_uid' AND location_uid='$location_uid' AND status='1' AND is_deleted='0'");
		if($getitem_rol->num_rows() > 0){
		    $refiller_uid = $getitem_rol->row()->location_refiller_uid;
		    
		    $refillerQuery = $this->db->query("SELECT id, refiller_name, refiller_mobile FROM `foodiq_location_refiller` WHERE `id` IN ( $refiller_uid )  AND status='1' AND is_deleted='0'");
		    if($refillerQuery->num_rows() > 0){
			foreach($refillerQuery->result() as $robj){
				$refiller .= '<option value="'.$robj->id.'">'.$robj->refiller_name.' ('.$robj->refiller_mobile.')</option>';
			}
		    }
		}
		$data['refiller_options'] = $refiller;
		//$foodmenuQ = $this->db->order_by('product_titlename', 'ASC')->get_where('foodiq_menulist', array('location_uid' => $location_uid, 'is_deleted' => '0'));
		
		$this->db->select('tb1.id, tb1.food_item_quantity, tb2.menu_display_option, tb2.food_item_barcode, tb2.food_carret_barcode, tb2.food_carret_quantity, tb2.is_perishable, tb2.product_titlename')->from('foodiq_menulist as tb1');
		$this->db->join('foodiq_master_menulist as tb2', 'tb1.master_menu_id=tb2.id', 'inner');
		$this->db->where(array('tb1.location_uid' => $location_uid, 'tb1.status' => '1'));
		$this->db->order_by('tb2.product_titlename', 'ASC');
		$foodmenuQ = $this->db->get();
		if($foodmenuQ->num_rows() > 0){
			$i = 1;
			foreach($foodmenuQ->result() as $fobj){
				$menuoption = '';
				$menu_display_option = $fobj->menu_display_option;
				if($menu_display_option == '1'){
					$menuoption = $fobj->food_item_barcode;
				}else{
					$menuoption = 'Direct to Cart';
				}
				//$reorderlevel = $fobj->ROL;
				$carton_barcode = $fobj->food_carret_barcode;
				$carton_qty = $fobj->food_carret_quantity;
				$qty_left = $fobj->food_item_quantity;
				$is_perishable = $fobj->is_perishable;
				$menu_id = $fobj->id;
				
				$storeqty = 0;
				$chkwarehouse = $this->db->query("SELECT quantity FROM foodiq_warehouse WHERE menu_id='".$menu_id."'");
				if($chkwarehouse->num_rows() > 0){
					$storeqty = $chkwarehouse->row()->quantity;
				}
				
				//if($qty_left < $reorderlevel){
				//	$reorderqty = ($reorderlevel - $qty_left);
				//}else{
				//	$reorderqty = $carton_qty;
				//}

				
				$content .= '<option value="'.$fobj->id.'" data-cartonbarcode="'.$carton_barcode.'" data-cartonqty="'.$carton_qty.'" data-itembarcode="'.$fobj->food_item_barcode.'" data-shelfqty="'.$qty_left.'" data-storeqty="'.$storeqty.'" data-perishable="'.$is_perishable.'">'.$fobj->product_titlename.' ('.$menuoption.')</option>';
				$i++;
			}
		}
		$checkinventorySettingQ = $this->db->query("SELECT id FROM foodiq_inventory_settings WHERE isp_uid='".ISPID."' AND location_uid='".$location_uid."'");
		if($checkinventorySettingQ->num_rows() == 0){
			$data['is_setting_enabled'] = '0';
		}else{
			$data['is_setting_enabled'] = '1';
		}
		
		
		$data['productlist'] = $content;
		echo json_encode($data);
	}
	public function get_item_inventory_details(){
		$data = array();
		$menu_id = $this->input->post('menu_id');
		$location_uid = $this->input->post('location_uid');
		
		$content = '';
		
		$this->db->select('tb1.id, tb1.food_item_quantity, tb2.menu_display_option, tb2.food_item_barcode, tb2.food_carret_barcode, tb2.food_carret_quantity, tb2.is_perishable, tb2.product_titlename')->from('foodiq_menulist as tb1');
		$this->db->join('foodiq_master_menulist as tb2', 'tb1.master_menu_id=tb2.id', 'inner');
		$this->db->where(array('tb1.id' => $menu_id, 'tb1.location_uid' => $location_uid, 'tb1.status' => '1'));
		$this->db->order_by('tb2.product_titlename', 'ASC');
		$foodmenuQ = $this->db->get();		
		if($foodmenuQ->num_rows() > 0){
			$i = 1;
			foreach($foodmenuQ->result() as $fobj){
				$menuoption = '';
				$menu_display_option = $fobj->menu_display_option;
				if($menu_display_option == '1'){
					$menuoption = $fobj->food_item_barcode;
				}else{
					$menuoption = 'Direct to Cart';
				}
				$reorderlevel = 0;
				$carton_barcode = $fobj->food_carret_barcode;
				$carton_qty = $fobj->food_carret_quantity;
				$qty_left = $fobj->food_item_quantity;
				$is_perishable = $fobj->is_perishable;
				$item_name = $fobj->product_titlename;
				
				if($carton_barcode != ''){
					$carton_barcode = $carton_barcode.' (Carton Barcode)';
				}
				
				$storeqty = 0;
				$chkwarehouse = $this->db->query("SELECT quantity FROM foodiq_warehouse WHERE menu_id='".$menu_id."' AND location_uid='$location_uid'");
				if($chkwarehouse->num_rows() > 0){
					$storeqty = $chkwarehouse->row()->quantity;
				}
				
				$content .= '<tr><td>'.$item_name.'</td><td>'.$fobj->food_item_barcode.' (Item Barcode) <br/> '.$carton_barcode.'</td><td>'.$storeqty.'</td><td>'.$qty_left.'</td></tr>';
				$i++;
			}
		}
		else{
			$content .= '<tr><td colspan="5">No Inventory Details Found.</td></tr>';
		}
		
		$data['invntry_details'] = $content;
		echo json_encode($data);
		
	}
	public function get_move_inventory_details(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$menu_id = $this->input->post('menu_id');
		
		$this->db->select('tb1.location_storage_id, tb1.location_refiller_uid, tb1.items_rol, tb2.location_uid, tb2.location_name, tb3.storage_location_name')->from('foodiq_inventory_settings as tb1');
		$this->db->join('wifi_location as tb2', 'tb2.location_uid = tb1.location_uid', 'inner');
		$this->db->join('foodiq_location_storage as tb3', 'tb3.id = tb1.location_storage_id', 'inner');
		$this->db->where(array('tb1.location_uid' =>  $location_uid, 'tb1.status' => '1', 'tb1.is_deleted' => '0'));
		$invntrylocQ = $this->db->get();
		//echo $this->db->last_query(); die;
		if($invntrylocQ->num_rows() > 0){
			$rawdata = $invntrylocQ->row();
			$refiller_id = $rawdata->location_refiller_uid;
			$storage_id = $rawdata->location_storage_id;
			$store_name = $rawdata->storage_location_name;
			$shelf_name = $rawdata->location_name;
			$data['items_rol'] = $rawdata->items_rol;
	
			$getordersfor_rolQ = $this->db->query("SELECT COALESCE(SUM(tb2.qty),0) as orderqty FROM `foodiq_order` as tb1 RIGHT JOIN `foodiq_order_details` as tb2 ON(tb1.id=tb2.order_id) WHERE DATE(tb1.added_on) >= ( CURDATE() - INTERVAL ".$rawdata->items_rol." DAY ) AND tb2.menu_id='$menu_id' ");
			$data['last_orders_qty'] = $getordersfor_rolQ->row()->orderqty;
	
			$data['movefrom_options'] = "<option value = ''>Select From</option><option value = '$storage_id' data-action='movefromstore'>".$store_name." (Store)</option><option value = '$location_uid' data-action='movefromshelve'>".$shelf_name." (Shelf)</option>";
			//<option value = '$location_uid' data-action='movefromshelve'>".$shelf_name." (Shelf)</option>
			
			$data['moveto_options'] = "<option value = ''>Select To</option><option value = '$location_uid' data-action='movetoshelve'>".$shelf_name." (Shelf)</option><option value = '$storage_id' data-action='movetostore'>".$store_name." (Store)</option>";
			//<option value = '$storage_id' data-action='movetostore'>".$store_name." (Store)</option>
			$data['refiller_id'] = $refiller_id;
			$data['location_storage_id'] = $storage_id;
		}
		
		echo json_encode($data);
	}
	public function getrefiller_assigned_locations(){
		//check also for menu_id barcode exists in another location
		$data = array();
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		$refiller_id = $this->input->post('refiller_id');
		$location_storage_id = $this->input->post('location_storage_id');
		$moveto_options = '';
		
		$where = "FIND_IN_SET('".$refiller_id."', `tb1`.`location_refiller_uid`)";
		
		$this->db->select('tb2.location_uid, tb2.location_name')->from('foodiq_inventory_settings as tb1');
		$this->db->join('wifi_location as tb2', 'tb2.location_uid = tb1.location_uid', 'inner');
		$this->db->where(array('tb1.location_uid !=' => $location_uid, 'tb1.location_storage_id' => $location_storage_id));
		$this->db->where($where);
		$chkmultilocQ = $this->db->get();
		if($chkmultilocQ->num_rows() > 0){
			foreach($chkmultilocQ->result() as $multiobj){
				$moveto_options .= "<option value = '$multiobj->location_uid' data-otherlocation='1' data-action='movetoshelve'>".$multiobj->location_name." (Shelf)</option>";
			}
		}
		$data['moveto_options'] = $moveto_options;
		
		echo json_encode($data);
	}
	public function get_return_inventory_details(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$menu_id = $this->input->post('menu_id');
		
		$suppnameQ = $this->db->query("SELECT tb2.supplier_name, tb2.is_perishable FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id=tb2.id) WHERE tb1.id='$menu_id'");
		$supp_rawdata = $suppnameQ->row();
		$supplier_name = $supp_rawdata->supplier_name;
		$is_perishable = $supp_rawdata->is_perishable;
		
		$this->db->select('tb1.location_storage_id, tb1.location_refiller_uid, tb1.items_rol, tb2.location_uid, tb2.location_name, tb3.storage_location_name')->from('foodiq_inventory_settings as tb1');
		$this->db->join('wifi_location as tb2', 'tb2.location_uid = tb1.location_uid', 'inner');
		$this->db->join('foodiq_location_storage as tb3', 'tb3.id = tb1.location_storage_id', 'inner');
		$this->db->where(array('tb1.location_uid' =>  $location_uid, 'tb1.status' => '1', 'tb1.is_deleted' => '0'));
		$invntrylocQ = $this->db->get();
		//echo $this->db->last_query(); die;
		if($invntrylocQ->num_rows() > 0){
			$rawdata = $invntrylocQ->row();
			$refiller_id = $rawdata->location_refiller_uid;
			$storage_id = $rawdata->location_storage_id;
			$store_name = $rawdata->storage_location_name;
			$shelf_name = $rawdata->location_name;

			$data['refiller_id'] = $refiller_id;
			
			if($is_perishable == 1){
				$data['returnfrom_options'] = "<option value = ''>Select From</option><option value = '$location_uid' data-action='returnfromshelve'>".$shelf_name." (Shelf)</option>";
			}else{
				$data['returnfrom_options'] = "<option value = ''>Select From</option><option value = '$storage_id' data-action='returnfromstore'>".$store_name." (Store)</option><option value = '$location_uid' data-action='returnfromshelve'>".$shelf_name." (Shelf)</option>";
			}
			
			$data['returnto_options'] = "<option value = ''>Select To</option><option value = '$supplier_name' data-action='returntosupplier'>".$supplier_name." (Supplier)</option>";
			
		}
		
		echo json_encode($data);
	}
	public function get_trash_inventory_details(){
		$data = array();
		$today_date = date('Y-m-d');		
		$location_uid = $this->input->post('location_uid');
		$menu_id = $this->input->post('menu_id');
		$menu_barcode = $this->input->post('menu_barcode');
		$is_perishable = '0';
		
		$foodmenuQ = $this->db->query("SELECT id, perishable_days, is_perishable FROM foodiq_master_menulist WHERE food_item_barcode = '$menu_barcode'");
		if($foodmenuQ->num_rows() > 0){
			foreach($foodmenuQ->result() as $fobj){
				$perishable_days = $fobj->perishable_days;
				$is_perishable = $fobj->is_perishable;
				
				$order_qty = 0;
				$orderQuery = $this->db->query("SELECT COALESCE(SUM(tb2.qty),0) as itemorderqty FROM `foodiq_order` as tb1 INNER JOIN `foodiq_order_details` as tb2 ON(tb1.id=tb2.order_id) WHERE (DATE(tb1.added_on) >= ( CURDATE() - INTERVAL ".$perishable_days." DAY )) AND tb2.menu_id='".$menu_id."'");
				if($orderQuery->num_rows() > 0){
				    $order_qty = $orderQuery->row()->itemorderqty;
				}
				
				if($is_perishable == '1'){
				    $invntryQ = $this->db->query("SELECT COALESCE(SUM(quantity),0) as storeqty, DATE( added_on ) as item_added_on  FROM foodiq_inventory_actions WHERE menu_id='".$menu_id."' AND action='addtoshelf' AND (DATE(added_on) >= ( CURDATE() - INTERVAL ".$perishable_days." DAY )) GROUP BY DATE( added_on ) LIMIT 1");
				}else{
				    $invntryQ = $this->db->query("SELECT COALESCE(SUM(quantity),0) as storeqty, DATE( added_on ) as item_added_on  FROM foodiq_inventory_actions WHERE menu_id='".$menu_id."' AND action='addinventory' AND (DATE(added_on) >= ( CURDATE() - INTERVAL ".$perishable_days." DAY )) GROUP BY DATE( added_on ) LIMIT 1");
				}
				
				if($invntryQ->num_rows() > 0){
					$laststoreqty = $invntryQ->row()->storeqty;
					
					if($order_qty > $laststoreqty){
					    $data['trashqty'] = '0';
					}
					else{
					    $data['trashqty'] = ($laststoreqty - $order_qty);
					}
				}
			}
		}
		
		$this->db->select('tb1.location_storage_id, tb1.location_refiller_uid, tb1.items_rol, tb2.location_uid, tb2.location_name, tb3.storage_location_name')->from('foodiq_inventory_settings as tb1');
		$this->db->join('wifi_location as tb2', 'tb2.location_uid = tb1.location_uid', 'inner');
		$this->db->join('foodiq_location_storage as tb3', 'tb3.id = tb1.location_storage_id', 'inner');
		$this->db->where(array('tb1.location_uid' =>  $location_uid, 'tb1.status' => '1', 'tb1.is_deleted' => '0'));
		$invntrylocQ = $this->db->get();
		//echo $this->db->last_query(); die;
		if($invntrylocQ->num_rows() > 0){
			$rawdata = $invntrylocQ->row();
			$refiller_id = $rawdata->location_refiller_uid;
			$storage_id = $rawdata->location_storage_id;
			$store_name = $rawdata->storage_location_name;
			$shelf_name = $rawdata->location_name;
	
			$data['refiller_id'] = $refiller_id;
			if($is_perishable == '0'){
				$data['trashfrom_options'] = "<option value = ''>Select From</option><option value = '$storage_id' data-action='trashfromstore'>".$store_name." (Store)</option><option value = '$location_uid' data-action='trashfromshelve'>".$shelf_name." (Shelf)</option>";
			}
			else{
				$data['trashfrom_options'] = "<option value = ''>Select From</option><option value = '$location_uid' data-action='trashfromshelve'>".$shelf_name." (Shelf)</option>";
			}
			
		}
		echo json_encode($data);
	}
	public function get_adjust_inventory_details(){
		$data = array();
		$today_date = date('Y-m-d');		
		$location_uid = $this->input->post('location_uid');
		$locationid = $this->input->post('locationid');
		$menu_id = $this->input->post('menu_id');
		
		$data['adjustfrom_options'] = "<option value = ''>Select From</option>";
		
		$this->db->select('tb2.location_name')->from('wifi_location as tb2');
		$this->db->where(array('tb2.id' =>  $locationid));
		$invntrylocQ = $this->db->get();
		//echo $this->db->last_query(); die;
		if($invntrylocQ->num_rows() > 0){
			$rawdata = $invntrylocQ->row();
			$shelf_name = $rawdata->location_name;
			
			$data['adjustfrom_options'] .= "<option value = '$location_uid' data-action='adjustfromshelve'>".$shelf_name." (Shelf)</option>";
			
		}
		$data['food_item_quantity'] = '0';
		$foodmenuQ = $this->db->query("SELECT food_item_quantity FROM foodiq_menulist WHERE id = '$menu_id' AND food_item_quantity < 0");
		if($foodmenuQ->num_rows() > 0){
			$fobj = $foodmenuQ->row();
			$data['food_item_quantity'] = abs($fobj->food_item_quantity);
		}
		echo json_encode($data);
	}
	public function manageinventory_actions(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$invntrytype = $this->input->post('invntrytype');
		$storeqty = $this->input->post('item_storeqty');
		$shelfqty = $this->input->post('item_shelfqty');
		$cartonqty = $this->input->post('item_cartonqty');
		$reorderqty = $this->input->post('item_reorderqty');
		$isp_uid = ISPID;
		
		$refiller_uid = $this->input->post('refiller_uid');;
		if($invntrytype == 'addinventory'){
			$menuid = $this->input->post('menuid');
			$menubarcode = $this->input->post('menubarcode');
			$menuqty = $this->input->post('menuqty');
			
			/*$foodmenuQ = $this->db->query("SELECT id FROM foodiq_menulist WHERE location_uid = '".$location_uid."' AND (food_carret_barcode = '".$menubarcode."' OR food_item_barcode = '".$menubarcode."') AND id='".$menuid."' AND is_deleted = '0'");
			if($foodmenuQ->num_rows() == 0){
				$data['resultCode'] = '400';
				$data['barcode_exists'] = '0';
			}else{*/
				$data['resultCode'] = '200';
				$data['barcode_exists'] = '1';
				
				$chkwarehouse = $this->db->query("SELECT id FROM foodiq_warehouse WHERE menu_id='$menuid' AND location_uid='$location_uid'");
				if($chkwarehouse->num_rows() > 0){
					$wfinalqty = '`quantity` + '.$menuqty;
					$this->db->set('quantity', $wfinalqty , FALSE);
					$this->db->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
					$this->db->update('foodiq_warehouse');
				}else{
					$this->db->insert('foodiq_warehouse', array('isp_uid' => ISPID, 'location_uid' => $location_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'added_on' => date('Y-m-d H:i:s')));
				}
				
				$this->db->select('quantity')->from('foodiq_warehouse');
				$this->db->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
				$balancestockQ = $this->db->get();
				$balance_stock_left = $balancestockQ->row()->quantity;
				
				$this->db->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => '', 'actionto_id' => '', 'actionfrom' => '', 'actionto' => '', 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => '0', 'added_on' => date('Y-m-d H:i:s')));
				
			//}
			$data['resultCode'] = '200';
		}
		else if($invntrytype == 'trashproduct'){
			$menuid = $this->input->post('menuid');
			$menuqty = $this->input->post('menuqty');
			$trashfrom = $this->input->post('trashfrom');
			$trashfrom_id = $this->input->post('trashfrom_id');
			$balance_stock_left = '0';
			$balance_shelf_left = '0';
			
			
			//trashfromshelve
			if($trashfrom == 'trashfromstore'){
				if($menuqty > $storeqty){
					$data['resultCode'] = '403';
					echo json_encode($data); die;
				}
				
				$wfinalqty = '`quantity` - '.$menuqty;
				$this->db->set('quantity', $wfinalqty , FALSE);
				$this->db->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
				$this->db->update('foodiq_warehouse');
				
				$this->db->select('quantity')->from('foodiq_warehouse');
				$this->db->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
				$balancestockQ = $this->db->get();
				$balance_stock_left = $balancestockQ->row()->quantity;
			}
			if($trashfrom == 'trashfromshelve'){
				if($menuqty > $shelfqty){
					$data['resultCode'] = '403';
					echo json_encode($data); die;
				}
				
				$finalqty = '`food_item_quantity` - '.$menuqty;
				$this->db->set('food_item_quantity', $finalqty , FALSE);
				$this->db->where('id', $menuid);
				$this->db->update('foodiq_menulist');
				
				$this->db->select('food_item_quantity')->from('foodiq_menulist');
				$this->db->where('id' , $menuid);
				$balanceshelfQ = $this->db->get();
				$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
			}
			
			
			
			$this->db->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $trashfrom_id, 'actionto_id' => '', 'actionfrom' => $trashfrom, 'actionto' => '', 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left, 'action' => $invntrytype, 'added_on' => date('Y-m-d H:i:s')));
			
			$data['resultCode'] = '200';
		}
		else if($invntrytype == 'moveproduct'){
			$menuid = $this->input->post('menuid');
			$menuqty = $this->input->post('menuqty');
			$movefrom = $this->input->post('movefrom');
			$moveto = $this->input->post('moveto');
			$movefrom_id = $this->input->post('movefromid');
			$moveto_id = $this->input->post('movetoid');
			$balance_stock_left = '0';
			$balance_shelf_left = '0';
			
			if($movefrom == 'movefromstore' && $moveto == 'movetoshelve'){
				
				if($movefrom == 'movefromstore'){
					if($menuqty > $storeqty){
						$data['resultCode'] = '403';
						echo json_encode($data); die;
					}
				}
				
				$finalqty = '`food_item_quantity` + '.$menuqty;
				$this->db->set('food_item_quantity', $finalqty , FALSE);
				$this->db->where('id', $menuid);
				$this->db->update('foodiq_menulist');
				
				$wfinalqty = '`quantity` - '.$menuqty;
				$this->db->set('quantity', $wfinalqty , FALSE);
				$this->db->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
				$this->db->update('foodiq_warehouse');
				
			}
			elseif($movefrom == 'movefromshelve' && $moveto == 'movetostore'){
				
				if($movefrom == 'movefromshelve'){
					if($menuqty > $shelfqty){
						$data['resultCode'] = '403';
						echo json_encode($data); die;
					}
				}
				
				$finalqty = '`food_item_quantity` - '.$menuqty;
				$this->db->set('food_item_quantity', $finalqty , FALSE);
				$this->db->where('id', $menuid);
				$this->db->update('foodiq_menulist');
				
				$wfinalqty = '`quantity` + '.$menuqty;
				$this->db->set('quantity', $wfinalqty , FALSE);
				$this->db->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
				$this->db->update('foodiq_warehouse');

			}
			elseif($movefrom == 'movefromshelve' && $moveto == 'movetoshelve'){
				
				if($movefrom == 'movefromshelve'){
					if($menuqty > $shelfqty){
						$data['resultCode'] = '403';
						echo json_encode($data); die;
					}
				}
				
				$finalqty = '`food_item_quantity` - '.$menuqty;
				$this->db->set('food_item_quantity', $finalqty , FALSE);
				$this->db->where(array('id' => $menuid, 'location_uid' => $location_uid));
				$this->db->update('foodiq_menulist');
				
				$nfinalqty = '`food_item_quantity` + '.$menuqty;
				$this->db->set('food_item_quantity', $nfinalqty , FALSE);
				$this->db->where('location_uid', $jsondata->moveto_id);
				$this->db->group_start();
				$this->db->where('food_item_barcode', $jsondata->barcode);
				$this->db->or_where('food_carret_barcode', $jsondata->barcode);
				$this->db->group_end();
				$this->db->update('foodiq_menulist');
				
				
				$this->db->select('id')->from('foodiq_menulist');
				$this->db->where('location_uid', $jsondata->moveto_id);
				$this->db->group_start();
				$this->db->where('food_item_barcode', $jsondata->barcode);
				$this->db->or_where('food_carret_barcode', $jsondata->barcode);
				$this->db->group_end();
				$otherlocation_menuQ = $this->db->get();
				$sec_menuid = $otherlocation_menuQ->row()->id;
				
				$this->db->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $jsondata->moveto_id, 'refiller_id' => $refiller_uid, 'menu_id' => $sec_menuid, 'quantity' => $menuqty, 'actionfrom_id' => $movefrom_id, 'actionto_id' => $moveto_id, 'actionfrom' => $movefrom, 'actionto' => $moveto, 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left , 'added_on' => date('Y-m-d H:i:s')));
				
			}
			
			
			$this->db->select('food_item_quantity')->from('foodiq_menulist');
			$this->db->where('id' , $menuid);
			$balanceshelfQ = $this->db->get();
			$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
			
			$this->db->select('quantity')->from('foodiq_warehouse');
			$this->db->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
			$balancestockQ = $this->db->get();
			$balance_stock_left = $balancestockQ->row()->quantity;
			
			$this->db->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $movefrom_id, 'actionto_id' => $moveto_id, 'actionfrom' => $movefrom, 'actionto' => $moveto, 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left , 'added_on' => date('Y-m-d H:i:s')));
			$data['resultCode'] = '200';
		}
		else if($invntrytype == 'returnproduct'){
			$menuid = $this->input->post('menuid');
			$menuqty = $this->input->post('menuqty');
			$returnfrom = $this->input->post('returnfrom');
			$returnto = $this->input->post('returnto');
			$returnfrom_id = $this->input->post('returnfromid');
			$returnto_id = $this->input->post('returntoid');
			$balance_stock_left = '0';
			$balance_shelf_left = '0';
			
			if($returnfrom == 'returnfromstore' && $returnto == 'returntosupplier'){
				
				if($returnfrom == 'returnfromstore'){
					if($menuqty > $storeqty){
						$data['resultCode'] = '403';
						echo json_encode($data); die;
					}
				}
				
				$finalqty = '`quantity` - '.$menuqty;
				$this->db->set('quantity', $finalqty , FALSE);
				$this->db->where('menu_id', $menuid);
				$this->db->update('foodiq_warehouse');
				
				$this->db->select('quantity')->from('foodiq_warehouse');
				$this->db->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
				$balancestockQ = $this->db->get();
				$balance_stock_left = $balancestockQ->row()->quantity;
				
				$this->db->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $returnfrom_id, 'actionto_id' => $returnto_id, 'actionfrom' => $returnfrom, 'actionto' => $returnto, 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left , 'added_on' => date('Y-m-d H:i:s')));
			}
			elseif($returnfrom == 'returnfromshelve' && $returnto == 'returntosupplier'){
				
				if($returnfrom == 'returnfromshelve'){
					if($menuqty > $shelfqty){
						$data['resultCode'] = '403';
						echo json_encode($data); die;
					}
				}
				
				$finalqty = '`food_item_quantity` - '.$menuqty;
				$this->db->set('food_item_quantity', $finalqty , FALSE);
				$this->db->where('id', $menuid);
				$this->db->update('foodiq_menulist');
				
				$this->db->select('food_item_quantity')->from('foodiq_menulist');
				$this->db->where('id' , $menuid);
				$balanceshelfQ = $this->db->get();
				$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
				
				
				$this->db->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $returnfrom_id, 'actionto_id' => $returnto_id, 'actionfrom' => $returnfrom, 'actionto' => $returnto, 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left , 'added_on' => date('Y-m-d H:i:s')));
			}
			$data['resultCode'] = '200';
		}
		else if($invntrytype == 'addtoshelf'){
			$menuid = $this->input->post('menuid');
			$menubarcode = $this->input->post('menubarcode');
			$menuqty = $this->input->post('menuqty');
			
			$finalqty = '`food_item_quantity` + '.$menuqty;
			$this->db->set('food_item_quantity', $finalqty , FALSE);
			$this->db->where('id', $menuid);
			$this->db->update('foodiq_menulist');
			
			$this->db->select('food_item_quantity')->from('foodiq_menulist');
			$this->db->where('id' , $menuid);
			$balanceshelfQ = $this->db->get();
			$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
			
			$this->db->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => '', 'actionto_id' => '', 'actionfrom' => '', 'actionto' => '', 'action' => $invntrytype, 'balance_stock' => '0', 'balance_shelf' => $balance_shelf_left, 'added_on' => date('Y-m-d H:i:s')));
			$data['resultCode'] = '200';
		}
		

		
		else if($invntrytype == 'adjustinventory'){
			$menuid = $this->input->post('menuid');
			$menubarcode = $this->input->post('menubarcode');
			$menuqty = $this->input->post('menuqty');
			$balance_stock_left = '0';
			$balance_shelf_left = '0';
			
			$foodmenuQ = $this->db->query("SELECT id, food_item_quantity FROM foodiq_menulist WHERE id='".$menuid."'");
			if($foodmenuQ->num_rows() > 0){
				$leftfoodqty = $foodmenuQ->row()->food_item_quantity;
				
				$finalqty = '`food_item_quantity` + '.$menuqty;
				$this->db->set('food_item_quantity', $finalqty , FALSE);
				$this->db->where('id', $menuid);
				$this->db->update('foodiq_menulist');
				
				$this->db->select('food_item_quantity')->from('foodiq_menulist');
				$this->db->where('id' , $menuid);
				$balanceshelfQ = $this->db->get();
				$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
				
				$this->db->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => '', 'actionto_id' => '', 'actionfrom' => '', 'actionto' => '', 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left, 'added_on' => date('Y-m-d H:i:s')));
			}
			$data['resultCode'] = '200';
		}
		
		echo json_encode($data);
	}
	public function showledger_report(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$menu_id = $this->input->post('menuid');
		$isp_uid = ISPID;
		
		$content = '';
		$itemsrolQ = $this->db->query("SELECT items_rol FROM foodiq_inventory_settings WHERE location_uid='".$location_uid."'");
		$item_rol = $itemsrolQ->row()->items_rol;
		
		$mastermenuQ = $this->db->query("SELECT tb1.is_perishable, tb1.perishable_days FROM foodiq_master_menulist as tb1 LEFT JOIN foodiq_menulist as tb2 ON(tb1.id=tb2.master_menu_id) WHERE tb2.id='".$menu_id."'");
		$perishable_days = $mastermenuQ->row()->perishable_days;
		
		
		
		$ledgerQ = $this->db->query("SELECT tb1.*, tb2.refiller_name, tb2.refiller_mobile FROM `foodiq_inventory_actions` as tb1 LEFT JOIN foodiq_location_refiller as tb2 ON(tb1.refiller_id=tb2.id) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.location_uid='".$location_uid."' AND tb1.menu_id='".$menu_id."' ORDER BY tb1.id DESC");
		if($ledgerQ->num_rows() > 0){
			foreach($ledgerQ->result() as $ledgobj){
				if($ledgobj->action == 'addtoshelf'){
					$content .= '<tr><td>Add to Shelf</td><td>'.$ledgobj->added_on.'</td><td>'.$ledgobj->refiller_name.' ( '.$ledgobj->refiller_mobile.' )</td><td>'.$ledgobj->quantity.'</td><td>&nbsp;</td><td>'.$ledgobj->balance_stock.'</td><td>&nbsp;</td><td>&nbsp;</td><td>'.$item_rol.'</td><td>'.$perishable_days.'</td></tr>';
				}
				elseif($ledgobj->action == 'addinventory'){
					$content .= '<tr><td>Add to Store</td><td>'.$ledgobj->added_on.'</td><td>'.$ledgobj->refiller_name.' ( '.$ledgobj->refiller_mobile.' )</td><td>'.$ledgobj->quantity.'</td><td>&nbsp;</td><td>'.$ledgobj->balance_stock.'</td><td>&nbsp;</td><td>&nbsp;</td><td>'.$item_rol.'</td><td>'.$perishable_days.'</td></tr>';
				}
				elseif($ledgobj->action == 'trashproduct'){
					$actionfrom = $ledgobj->actionfrom;
					if($actionfrom == 'trashfromshelve'){
						$actionfrom = 'Trash From Shelf';
					}
					elseif($actionfrom == 'trashfromstore'){
						$actionfrom = 'Trash From Store';
					}
					$content .= '<tr><td>'.$actionfrom.'</td><td>'.$ledgobj->added_on.'</td><td>'.$ledgobj->refiller_name.' ( '.$ledgobj->refiller_mobile.' )</td><td>&nbsp;</td><td>'.$ledgobj->quantity.'</td><td>'.$ledgobj->balance_stock.'</td><td>'.$ledgobj->quantity.'</td><td>&nbsp;</td><td>'.$item_rol.'</td><td>'.$perishable_days.'</td></tr>';
				}
				elseif($ledgobj->action == 'returnproduct'){
					$actionfrom = $ledgobj->actionfrom;
					if($actionfrom == 'returnfromshelve'){
						$actionfrom = 'Return From Shelf';
					}
					elseif($actionfrom == 'returnfromstore'){
						$actionfrom = 'Return From Store';
					}
					$content .= '<tr><td>'.$actionfrom.'</td><td>'.$ledgobj->added_on.'</td><td>'.$ledgobj->refiller_name.' ( '.$ledgobj->refiller_mobile.' )</td><td>&nbsp;</td><td>'.$ledgobj->quantity.'</td><td>'.$ledgobj->balance_stock.'</td><td>&nbsp;</td><td>'.$ledgobj->quantity.'</td><td>'.$item_rol.'</td><td>'.$perishable_days.'</td></tr>';
				}
				else{
					$content .= '<tr><td>'.$ledgobj->action.'</td><td>'.$ledgobj->added_on.'</td><td>'.$ledgobj->refiller_name.' ( '.$ledgobj->refiller_mobile.' )</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>'.$ledgobj->quantity.'</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
				}
			}
		}
		else{
			$content .= '<tr><td colspan="11">No Report Found</td></tr>';
		}
		
		$data['ledger_report'] = $content;
		
		echo json_encode($data);
	}
	public function getsupplier_listing(){
		$data = array();
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		$content = '';
		$this->db->select('tb1.id, tb1.supplier_name, tb2.supplier_rol, tb2.status, tb2.supplier_price_surge')->from('foodiq_supplier as tb1');
		$this->db->join('foodiq_inventory_supplier_settings as tb2', 'tb1.id=tb2.supplier_id', 'left');
		$this->db->where(array('tb1.status' => '1', 'tb1.isp_uid' => $isp_uid, 'tb2.location_uid' => $location_uid));
		$this->db->order_by('tb1.supplier_name', 'ASC');
		$supplierQuery = $this->db->get();
		if($supplierQuery->num_rows() > 0){
			foreach($supplierQuery->result() as $suppobj){
				$suppstatus = '';
				if($suppobj->status == '1'){
					$suppstatus = 'Active';
				}else{
					$suppstatus = 'Inactive';
				}
				$content .= '<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						    <div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							   <div class="col-sm-3 col-xs-3">
							      <label class="so-lable-food" style="display:block">'.$suppobj->supplier_name.'  ROL<sup>*</sup></label>
							      <input type="number" min="0" name="supplier_rol_'.$suppobj->id.'" value="'.$suppobj->supplier_rol.'" title="Indicate the number of days of sale that to be maintained in inventory." required>
							   </div>
							   <div class="col-sm-1 col-xs-1">
							      <label>&nbsp;</label>
							   </div>
							   <div class="col-sm-3 col-xs-3">
							      <label class="so-lable-food" style="display:block">'.$suppobj->supplier_name.'  Surge Price Factor<sup>*</sup></label>
							      <input type="number" min="0" step="0.01" name="supplier_price_surge_'.$suppobj->id.'" value="'.$suppobj->supplier_price_surge.'" title="Indicate the Surge Price Factor." required>
							   </div>
							   <div class="col-sm-1 col-xs-1">
							      <label>&nbsp;</label>
							   </div>
							   <div class="col-sm-3 col-xs-3 col-pt-30">
							      <button type="button" class="mui-btn mui-btn--outline-network btn-sm" onclick="changesupplier_status('.$suppobj->id.', &apos;' .$suppobj->supplier_name. '&apos;)">'.$suppstatus.'</button>
							   </div>
							</div>
						    </div>
						</div>
					    </div>';
			}
		}
		else{
			
			$this->db->select('tb1.id, tb1.supplier_name')->from('foodiq_supplier as tb1');
			$this->db->where(array('tb1.status' => '1', 'tb1.isp_uid' => $isp_uid));
			$this->db->order_by('tb1.supplier_name', 'ASC');
			$supplierQuery = $this->db->get();
			if($supplierQuery->num_rows() > 0){
				foreach($supplierQuery->result() as $suppobj){
					$suppstatus = '';
					if($suppobj->status == '1'){
						$suppstatus = 'Active';
					}else{
						$suppstatus = 'Inactive';
					}
					$content .= '<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							    <div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								   <div class="col-sm-3 col-xs-3">
								      <label class="so-lable-food" style="display:block">'.$suppobj->supplier_name.'  ROL<sup>*</sup></label>
								      <input type="number" min="0" name="supplier_rol_'.$suppobj->id.'" value="0" title="Indicate the number of days of sale that to be maintained in inventory." required>
								   </div>
								   <div class="col-sm-1 col-xs-1">
									<label>&nbsp;</label>
								   </div>
								   <div class="col-sm-3 col-xs-3">
									<label class="so-lable-food" style="display:block"><strong>'.$suppobj->supplier_name.'</strong> Surge Price Factor<sup>*</sup></label>
									<input type="number" min="0" step="0.01" name="supplier_price_surge_'.$suppobj->id.'" value="'.$suppobj->supplier_price_surge.'" title="Indicate the Surge Price Factor." required>
								   </div>
								   <div class="col-sm-1 col-xs-1">
								      <label>&nbsp;</label>
								   </div>
								   <div class="col-sm-3 col-xs-3 col-pt-30">
								      <button type="button" class="mui-btn mui-btn--outline-network btn-sm" onclick="changesupplier_status('.$suppobj->id.', &apos;' .$suppobj->supplier_name. '&apos;)">'.$suppstatus.'</button>
								   </div>
								</div>
							    </div>
							</div>
						    </div>';
				}
			}
			else{
				$content = '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">NO Supplier Added Yet!!</div></div>';
			}
		}
		
		$data['supplierlist'] = $content;
		echo json_encode($data);
	}
	
	public function manage_inventory_supplier(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$isp_uid = ISPID;
		$postdata = $this->input->post();
		//print_r($postdata); die;
		foreach($postdata as $key => $value){
			if($key != 'location_uid' && $key != 'locationid'){
				$keyid = explode('_', $key);
				$keyid = end($keyid);
				
				
				$this->db->select('id')->from('foodiq_inventory_supplier_settings');
				$this->db->where(array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'supplier_id' => $keyid));
				$checksuppQ = $this->db->get();
				//echo $this->db->last_query();
				if($checksuppQ->num_rows() > 0){
					$ssid = $checksuppQ->row()->id;
					
					$supplier_rol = '0'; $supplier_price_surge = '0';
					if (strpos($key, 'supplier_rol') !== false) {
						$supplier_rol = $value;
						$this->db->update('foodiq_inventory_supplier_settings', array('supplier_id' => $keyid, 'supplier_rol' => $supplier_rol), array('id' => $ssid));
					}
					elseif (strpos($key, 'supplier_price_surge') !== false) {
						$supplier_price_surge = $value;
						$this->db->update('foodiq_inventory_supplier_settings', array('supplier_id' => $keyid, 'supplier_price_surge' => $supplier_price_surge), array('id' => $ssid));
					}
					
				}else{
					$this->db->insert('foodiq_inventory_supplier_settings', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'supplier_id' => $keyid, 'supplier_rol' => $supplier_rol, 'supplier_price_surge' => $supplier_price_surge, 'added_on' => date('Y-m-d H:i:s')));
					//echo $this->db->last_query();
				}
			}
		}
		
		echo json_encode(true);
		
	}
	public function location_refiller_listing(){
		$data = array();
		$location_uid = $this->input->post('location_uid');
		$isp_uid = ISPID;
		
		$content = '';
		$supplierQuery = $this->db->get_where('foodiq_location_refiller', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid));	
		if($supplierQuery->num_rows() > 0){
			$i = 1;
			foreach($supplierQuery->result() as $suppobj){
				$content .= '<tr><td>'.$i.'</td><td>'.$suppobj->refiller_name.'</td><td>'.$suppobj->refiller_mobile.'</td><td><a href="javascript:void(0)" onclick="changerefiller_status('.$suppobj->id.')"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="editrefiller_details('.$suppobj->id.')"><i class="fa fa-edit"></i></a></td></tr>';
				$i++;
			}
		}else{
			$content = '<tr><td colspan="4">No supplier added for this location.</td></tr>';
		}
		
		$data['refillerlist'] = $content;
		echo json_encode($data);
	}
	public function getrefiller_options(){
		$data = array();
		$isp_uid = ISPID;
		$refillerlist = "<option value = ''>Select Refiller</option>";
		$refillerlocQ = $this->db->query("SELECT id,refiller_name,refiller_uid FROM foodiq_location_refiller WHERE isp_uid = '$isp_uid' AND status = '1' AND is_deleted='0'");
		if($refillerlocQ->num_rows() > 0){
			foreach($refillerlocQ->result() as $rstorobj){
				$refillerlist .= "<option value = '".$rstorobj->id."'>".$rstorobj->refiller_name." (".$rstorobj->refiller_uid.")</option>";
			}
		}
		
		$data['refillerlist'] = $refillerlist;
		echo json_encode($data);
	}
	public function changesupplier_status(){
		$supplier_id = $this->input->post('supplier_id');
		$supplier_name = $this->input->post('supplier_name');
		$location_uid = $this->input->post('location_uid');
		
		$chgstatus = '';
		$this->db->select('id, status')->from('foodiq_inventory_supplier_settings');
		$this->db->where(array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'supplier_id' => $supplier_id));
		$checkmenuQ = $this->db->get();
		if($checkmenuQ->num_rows() > 0){
			$rawdata = $checkmenuQ->row();
			$status = $rawdata->status;
			$ssid = $rawdata->id;
			
			$chgstatus = '1';
			if($status == '1'){
				$chgstatus = '0';
			}
			
			$this->db->update('foodiq_inventory_supplier_settings', array('status' => $chgstatus), array('id' => $ssid));
		}
		
		$this->db->select('tb2.id')->from('foodiq_menulist as tb1');
		$this->db->join('foodiq_master_menulist as tb2', 'tb1.master_menu_id = tb2.id', 'inner');
		$this->db->where(array('tb1.location_uid' => $location_uid, 'tb2.supplier_name' => $supplier_name));
		$checkmenuQ = $this->db->get();
		if($checkmenuQ->num_rows() > 0){
			foreach($checkmenuQ->result() as $mobj){
				$prodid = $mobj->id;
				//$this->db->update('foodiq_master_menulist', array('status' => $chgstatus), array('id' => $prodid));
				$this->db->update('foodiq_menulist', array('status' => $chgstatus), array('master_menu_id' => $prodid));
			}
		}
		
		echo json_encode(true);
	}
	/*******************************************************************
	 *	INTERNAL ACTIVATION FUNCTION STARTS
	 ******************************************************************/
	
	public function add_team_setup_details(){
		$path = FCPATH;
		if($path != ''){
			$path = $path.'assets/intern_active_background';
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			}
			// change permission 
			$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
			if( $fldrperm != 0777) {
				$output = exec("sudo chmod -R 0777 \"$path\"");
			}
		}
		$base_url = 'assets/intern_active_background/';
		$original="";
		$small="";
		$logo="";
		$image_name = $this->input->post("banner_image_name");
		$image_src = $this->input->post("banner_image_src");
		$team_name = $this->input->post('team_name');
		$call_score = $this->input->post('call_score');
		$sales_score = $this->input->post('sales_score');
		$execution_score = $this->input->post('execution_score');
		$total_score = $this->input->post('total_score');
		
		if($image_name != ''){
			if($image_src != ''){
				if (!file_exists($base_url . 'original/')) {
					mkdir($base_url . 'original/', 0777, TRUE);
				}
				$file_ext = $this->getExtension($image_name);
				$explode = explode(',',$image_src);
				$image_src = $explode['1'];
				$image_src = str_replace(' ', '+', $image_src);
				$data_img = base64_decode($image_src);
				$filename = uniqid() . '.'.$file_ext;
				$file = $base_url . $filename;
				
				$success = file_put_contents($file, $data_img);
				$newname1 = date("Ymdhisv").rand().".".$file_ext;
				
				//rename($base_url.$filename,$base_url.'original/'.$newname);
				rename($base_url.$filename,$base_url.'original/'.$newname1);
				if (!file_exists($base_url . 'logo/')) {
					mkdir($base_url . 'logo/', 0777, TRUE);
				}
				if (!file_exists($base_url . 'small/')) {
				    mkdir($base_url . 'small/', 0777, TRUE);
				}
				$this->load->library('image_lib');
				$config1['image_library'] = 'gd2';
				//$config1['source_image'] = $base_url . 'original/'.$newname;
				$config1['source_image'] = $base_url . 'original/'.$newname1;
				$config1['new_image'] = $base_url . 'logo/'."300_".$newname1;
		
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 300;
				$config1['height'] = 100;
				$this->image_lib->initialize($config1);
				$this->image_lib->resize();
		    
				$this->image_lib->clear();
				$config2['image_library'] = 'gd2';
				//$config2['source_image'] = $base_url . 'original/'.$newname;
				//$config2['new_image'] = $base_url . 'small/'."600_".$newname;
				$config2['source_image'] = $base_url . 'original/'.$newname1;
				$config2['new_image'] = $base_url . 'small/'."600_".$newname1;
		  
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 600;
				$config2['height'] = 200;
				$this->image_lib->initialize($config2);
				$this->image_lib->resize();
		      
  
				$original=$newname1;
				$small="600_".$newname1;
				$logo="300_".$newname1;
				 
				$fname = $base_url.'original/'.$newname1;
				$fname1 = $base_url . 'logo/'."300_".$newname1;
				$fname2 = $base_url . 'small/'."600_".$newname1;
		   
				$famazonname = AMAZONPATH.'intern_active_background/original/'.$newname1;
				$famazonname1 = AMAZONPATH.'intern_active_background/logo/300_'. $newname1;
				$famazonname2 = AMAZONPATH.'intern_active_background/small/600_'. $newname1;
		
				$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
				$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
			
				unlink($fname);
				unlink($fname1);
				unlink($fname2);	 
				
			}	
		}
		
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		
		$internsetup = array(
			'isp_uid' => $isp_uid,
			'location_uid' => $location_uid,
			'team_name' => $team_name,
			'call_score' => $call_score,
			'sales_score' => $sales_score,
			'execution_score' => $execution_score,
			'total_score' => $total_score,
			'added_on' => date('Y-m-d H:i:s')
		);
		
		if($original != ''){
			$internsetup['team_banner_original'] = $original;
		}
		if($small != ''){
			$internsetup['team_banner_small'] = $small;
		}
		if($logo != ''){
			$internsetup['team_banner_logo'] = $logo;
		}
		
		$internsetupQ = $this->db->query("SELECT id FROM sht_internact_teams_setup WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."'");
		if($internsetupQ->num_rows() > 0){
			$rawdata = $internsetupQ->row();
			$rcid = $rawdata->id;
			$this->db->update('sht_internact_teams_setup', $internsetup, array('id' => $rcid));
		}else{
			$this->db->insert('sht_internact_teams_setup', $internsetup);
			$rcid = $this->db->insert_id();
		}
		
		echo $rcid;
		
	}
	public function show_team_setup(){
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		$data = array();
		$internsetupQ = $this->db->query("SELECT id, team_name, team_banner_small, call_score, sales_score, execution_score, total_score FROM sht_internact_teams_setup WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."'");
		if($internsetupQ->num_rows() > 0){
			$rawdata = $internsetupQ->row();
			$data['team_id'] = $rawdata->id;
			$data['team_name'] = $rawdata->team_name;
			$data['team_banner_small'] = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/intern_active_background/small/'.$rawdata->team_banner_small;
			$data['call_score'] = $rawdata->call_score;
			$data['sales_score'] = $rawdata->sales_score;
			$data['execution_score'] = $rawdata->execution_score;
			$data['total_score'] = $rawdata->total_score;
		}
		
		echo json_encode($data);
	}
	public function manage_team_member(){
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		
		$path = FCPATH;
		if($path != ''){
			$path = $path.'assets/intern_active_background';
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			}
			// change permission 
			$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
			if( $fldrperm != 0777) {
				$output = exec("sudo chmod -R 0777 \"$path\"");
			}
		}
		$base_url = 'assets/intern_active_background/';
		$original="";
		$small="";
		$logo="";
		$image_name = $this->input->post("profile_image_name");
		$image_src = $this->input->post("profile_image_src");
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$phone = $this->input->post('mobile_number');
		$email = $this->input->post('email');
		$permission = $this->input->post('permission');
		$team_id = $this->input->post('team_id');
		$team_member_id = $this->input->post('team_member_id');
		
		$getTeamQuery = $this->db->query("SELECT id FROM sht_internact_teams_setup WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."'");
		if($getTeamQuery->num_rows() > 0){
			$team_id = $getTeamQuery->row()->id;
		}
		
		if($image_name != ''){
			if($image_src != ''){
				if (!file_exists($base_url . 'original/')) {
					mkdir($base_url . 'original/', 0777, TRUE);
				}
				$file_ext = $this->getExtension($image_name);
				$explode = explode(',',$image_src);
				$image_src = $explode['1'];
				$image_src = str_replace(' ', '+', $image_src);
				$data_img = base64_decode($image_src);
				$filename = uniqid() . '.'.$file_ext;
				$file = $base_url . $filename;
				
				$success = file_put_contents($file, $data_img);
				$newname1 = date("Ymdhisv").rand().".".$file_ext;
				
				//rename($base_url.$filename,$base_url.'original/'.$newname);
				rename($base_url.$filename,$base_url.'original/'.$newname1);
				if (!file_exists($base_url . 'logo/')) {
					mkdir($base_url . 'logo/', 0777, TRUE);
				}
				if (!file_exists($base_url . 'small/')) {
				    mkdir($base_url . 'small/', 0777, TRUE);
				}
				$this->load->library('image_lib');
				$config1['image_library'] = 'gd2';
				$config1['source_image'] = $base_url . 'original/'.$newname1;
				$config1['new_image'] = $base_url . 'logo/'."100_".$newname1;
		
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 100;
				$config1['height'] = 100;
				$this->image_lib->initialize($config1);
				$this->image_lib->resize();
				$this->image_lib->clear();
				
				$config2['image_library'] = 'gd2';
				$config2['source_image'] = $base_url . 'original/'.$newname1;
				$config2['new_image'] = $base_url . 'small/'."150_".$newname1;
		  
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 150;
				$config2['height'] = 150;
				$this->image_lib->initialize($config2);
				$this->image_lib->resize();
		      
  
				$original = $newname1;
				$small = "150_".$newname1;
				$logo = "100_".$newname1;
				 
				$fname = $base_url.'original/'.$newname1;
				$fname1 = $base_url . 'logo/'."100_".$newname1;
				$fname2 = $base_url . 'small/'."150_".$newname1;
		   
				$famazonname = AMAZONPATH.'intern_active_background/original/'.$newname1;
				$famazonname1 = AMAZONPATH.'intern_active_background/logo/100_'. $newname1;
				$famazonname2 = AMAZONPATH.'intern_active_background/small/150_'. $newname1;
		
				$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
				$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
			
				unlink($fname);
				unlink($fname1);
				unlink($fname2);	 
				
			}	
		}
		
		$internsetup = array(
			'isp_uid' => $isp_uid,
			'location_uid' => $location_uid,
			'team_id' => $team_id,
			'first_name' => trim($first_name),
			'last_name' => trim($last_name),
			'mobile' => $phone,
			'email' => trim($email),
			'permission' => $permission,
			'status' => '1',
			'added_on' => date('Y-m-d H:i:s')
		);
		
		if($original != ''){
			$internsetup['profile_image'] = $original;
		}
		if($small != ''){
			$internsetup['profile_image_small'] = $small;
		}
		if($logo != ''){
			$internsetup['profile_image_logo'] = $logo;
		}
		
		if($team_member_id != ''){
			$this->db->update('sht_internact_team_member', $internsetup, array('id' => $team_member_id));
		}else{
			$this->db->insert('sht_internact_team_member', $internsetup);
			$team_member_id = $this->db->insert_id();
		}
		
		echo $team_member_id;
	}
	public function show_team_members(){
		$data = array();
		$content = '';
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		
		$team_memberQ = $this->db->query("SELECT id, first_name, last_name, email, mobile, profile_image_logo, permission FROM sht_internact_team_member WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."' AND is_deleted='0' ORDER BY id DESC");
		if($team_memberQ->num_rows() > 0){
			$i = 1;
			foreach($team_memberQ->result() as $tmobj){
				$imagepath = '<img class="profile-user-img img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/intern_active_background/logo/'.$tmobj->profile_image_logo.'" style=" width:30%;">';
				$permission = $tmobj->permission;
				if($permission == 'read'){
					$permission = 'Read Only';
				}else{
					$permission = 'Read & Post';
				}
				$content .= '<tr><td>'.$i.'</td><td>'.$tmobj->first_name.'</td><td>'.$tmobj->last_name.'</td><td>'.$tmobj->email.'</td><td>'.$imagepath.'</td><td>'.$permission.'</td><td><a href="javascript:void(0)" onclick="edit_team_member('.$tmobj->id.')"><i class="fa fa-edit"></i></a></td></tr>';
				$i++;
			}
		}else{
			$content .= '<tr><td cospan="8">No Team Member Data Found.</td></tr>';
		}
		
		$data['team_member_details'] = $content;
		echo json_encode($data, JSON_UNESCAPED_SLASHES);
	}
	public function team_members_options(){
		$data = array();
		$content = '<option value="">Select Team Member</option>';
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		
		$team_memberQ = $this->db->query("SELECT id, first_name, last_name FROM  sht_internact_team_member WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."' ORDER BY id DESC");
		if($team_memberQ->num_rows() > 0){
			foreach($team_memberQ->result() as $tmobj){
				$content .= '<option value="'.$tmobj->id.'">'.$tmobj->first_name.' '.$tmobj->last_name.'</option>';
			}
		}
		$data['team_members'] = $content;
		echo json_encode($data, JSON_UNESCAPED_SLASHES);
	}
	public function manageteam_post(){
		
		$isp_uid = ISPID;
		$team_member_id = $this->input->post('team_member_id');
		$team_member_options = $this->input->post('team_member_options');
		$post_text = $this->input->post('post_text');
		$location_uid = $this->input->post('location_uid');
		
		$getTeamQuery = $this->db->query("SELECT id FROM sht_internact_teams_setup WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."'");
		if($getTeamQuery->num_rows() > 0){
			
			$team_id = $getTeamQuery->row()->id;
			
			$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/intern_active_background';
				if (!file_exists($path)) {
					mkdir($path, 0777, true);
				}
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
			$base_url = 'assets/intern_active_background/';
			$original = "";
			$small = "";
			$logo = "";
			$filetype = "";
			
			$post_data_name = $_FILES['post_data']['name'];
			if(isset($post_data_name) && $post_data_name != ''){
				$mimetype = $_FILES['post_data']['type'];
				if(strstr($mimetype, "video/")){
					$filetype = "video";
				}else if(strstr($mimetype, "image/")){
					$filetype = "image";
				}
				
				if($filetype == 'image'){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$tmp_post_data_name = $_FILES['post_data']['tmp_name'];
					$file_ext = $this->getExtension($post_data_name);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					move_uploaded_file($tmp_post_data_name, $file);
					
					//$success = file_put_contents($file, $data_img);
					$newname1 = date("Ymdhisv").rand().".".$file_ext;
					
					//rename($base_url.$filename,$base_url.'original/'.$newname);
					rename($base_url.$filename,$base_url.'original/'.$newname1);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = $base_url . 'original/'.$newname1;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname1;
			
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
					$this->image_lib->clear();
					
					$config2['image_library'] = 'gd2';
					$config2['source_image'] = $base_url . 'original/'.$newname1;
					$config2['new_image'] = $base_url . 'small/'."600_".$newname1;
			  
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					$this->image_lib->initialize($config2);
					$this->image_lib->resize();
			      
		
					$original = $newname1;
					$small = "600_".$newname1;
					$logo = "300_".$newname1;
					 
					$fname = $base_url.'original/'.$newname1;
					$fname1 = $base_url . 'logo/'."300_".$newname1;
					$fname2 = $base_url . 'small/'."600_".$newname1;
			   
					$famazonname = AMAZONPATH.'intern_active_background/original/'.$newname1;
					$famazonname1 = AMAZONPATH.'intern_active_background/logo/300_'. $newname1;
					$famazonname2 = AMAZONPATH.'intern_active_background/small/600_'. $newname1;
			
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					$this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
					$this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				
					unlink($fname);
					unlink($fname1);
					unlink($fname2);
				}
				elseif($filetype == 'video'){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$tmp_post_data_name = $_FILES['post_data']['tmp_name'];
					$file_ext = $this->getExtension($post_data_name);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					move_uploaded_file($tmp_post_data_name, $file);
					
					//$success = file_put_contents($file, $data_img);
					$newname1 = date("Ymdhisv").rand().".".$file_ext;
					
					//rename($base_url.$filename,$base_url.'original/'.$newname);
					rename($base_url.$filename,$base_url.'original/'.$newname1);
					
					$original = $newname1;
					
					$fname = $base_url.'original/'.$newname1;
					$famazonname = AMAZONPATH.'intern_active_background/original/'.$newname1;		
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				
					unlink($fname);
	
				}
			}
			
			
			
			$internsetup = array(
				'isp_uid' => $isp_uid,
				'location_uid' => $location_uid,
				'team_id' => $team_id,
				'team_member_id' => $team_member_options,
				'team_post_text' => $post_text,
				'media_type' => $filetype,
				'team_post_media' => $original,
				'team_post_media_small' => $small,
				'team_post_media_logo' => $logo,
				'added_on' => date('Y-m-d H:i:s')
			);
			
			
			if($team_member_id != ''){
				$this->db->update('sht_internact_team_posts', $internsetup, array('id' => $team_member_id));
			}else{
				$this->db->insert('sht_internact_team_posts', $internsetup);
				$team_member_id = $this->db->insert_id();
			}
			
			echo $team_member_id;
		}else{
			echo 'No team setup';
		}
	}
	
	public function team_posts_list(){
		$data = array();
		$content = '';
		$isp_uid = ISPID;
		$location_uid = $this->input->post('location_uid');
		
		$team_memberQ = $this->db->query("SELECT id, team_post_text, team_member_id, added_on FROM sht_internact_team_posts WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."' AND is_deleted='0' ORDER BY id DESC");
		if($team_memberQ->num_rows() > 0){
			$i = 1;
			foreach($team_memberQ->result() as $tmobj){
				//$imagepath = '<img class="profile-user-img img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/intern_active_background/logo/'.$tmobj->profile_image_logo.'" style=" width:100%;">';

				$content .= '<tr><td>'.$i.'</td><td>'.$tmobj->team_post_text.'</td><td>'.$tmobj->team_member_id.'</td><td>&nbsp;</td><td>'.$tmobj->added_on.'</td><td>Action</td></tr>';
				$i++;
			}
		}else{
			$content .= '<tr><td cospan="8">No Post Data Found.</td></tr>';
		}
		
		$data['team_post_details'] = $content;
		echo json_encode($data, JSON_UNESCAPED_SLASHES);
	}
	public function edit_team_member(){
		$data = array();
		$membrid = $this->input->post('membrid');
		$team_memberQ = $this->db->query("SELECT first_name, last_name, email, mobile, profile_image_logo, permission FROM sht_internact_team_member WHERE id='".$membrid."' ORDER BY id DESC");
		if($team_memberQ->num_rows() > 0){
			$data = $team_memberQ->row_array();
		}
		
		echo json_encode($data);
	}
	public function check_team_emailid(){
		$data = array();
		$emailid = $this->input->post('emailid');
		$team_member_id = $this->input->post('team_member_id');
		if($team_member_id != ''){
			$checkemailQ = $this->db->get_where('sht_internact_team_member', array('email' => $emailid, 'id !=' => $team_member_id));
		}else{
			$checkemailQ = $this->db->get_where('sht_internact_team_member', array('email' => $emailid));
		}
		if($checkemailQ->num_rows() > 0){
			$data['is_exists'] = '1';
		}else{
			$data['is_exists'] = '0';
		}
		
		echo json_encode($data);
	}
	
	/*******************************************************************
	 *	Event Guest management FUNCTION STARTS
	 ******************************************************************/
	public function add_offline_vm_table(){
		
		$tableData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'loc_id' => $this->input->post("locationid"),
			'table_name' => $this->input->post("offline_organisation_name"),
			"created_on"=>date("Y-m-d H:i:s"),
		);
		$this->db->insert('offline_eventguest_table',$tableData);
		
		$data['resultCode']=1;
		return $data;
		//return $requestData;die;
		
	}
	
	public function offline_table_list(){
		
		$data['resultCode']=1;
		$location_uid=$this->input->post('location_uid');
		$query=$this->db->query("select id,table_name from offline_eventguest_table where is_deleted='0' and location_uid='".$location_uid."'");
		$html='';
		$optionhtml='';
		if($query->num_rows()>0){
			foreach($query->result() as $val){
			$html.='<tr><td class="text-left">'.$val->table_name.' </td> <td><i style="cursor: pointer" onclick="delete_offline_vm_table('.$val->id.')" class="fa fa-trash fa-lg" aria-hidden="true"></i></td></tr>';
			$optionhtml.='<option value="'.$val->id.'">'.$val->table_name.'</option>';
			}
		}
		$data['html']=$html;
		$data['optionhtml']=$optionhtml;
		return $data;
	}
	
	public function delete_table_list(){
		$updateid=$this->input->post('id');
		$tabledata=array("is_deleted"=>1);
		$this->db->update("offline_eventguest_table",$tabledata,array("id"=>$updateid));
		$data['resultCode']=1;
		return $data;
	}
	
	public function add_vm_offline_guest(){
		
		$tableData = array(
			
			'loc_id' => $this->input->post("location_id"),
			'location_uid' => $this->input->post("location_uid"),
			'guest_firstname' => $this->input->post("employee_name"),
			'guest_lastname' => $this->input->post("employee_last_name"),
			'guest_organisation' => $this->input->post("employee_orgainisation"),
			'guest_designation' => $this->input->post("employee_designation"),
			'table_id' => $this->input->post("tableid"),
			'guest_prefix' =>$this->input->post("employee_prefix"),
			"created_on"=>date("Y-m-d H:i:s"),
		);
		$this->db->insert('offline_eventguest_user',$tableData);
		
		$data['resultCode']=1;
		return $data;
		//return $requestData;die;
		
	}
	
		public function vm_offline_guest_list(){
		$query=$this->db->query("select oeu.guest_prefix,oeu.id,oeu.guest_firstname,oeu.guest_lastname,oeu.guest_organisation,oeu.guest_designation,oet.table_name from offline_eventguest_user oeu
					inner join offline_eventguest_table oet on (oet.id=oeu.table_id and oet.is_deleted='0')");
		$html='';
		if($query->num_rows()>0){
			
			foreach($query->result() as $val){
				$html.='<tr><td class="text-left">'.$val->guest_prefix.' '.$val->guest_firstname.'</td><td class="text-left">'.$val->guest_organisation.'</td>
			<td class="text-left">'.$val->guest_designation.'</td><td class="text-left">'.$val->table_name.'</td>';
			}
		}
		$data['resultCode']=1;
		$data['html']=$html;
		return $data;
	}
	
	/*******************************************************************
	 *	SMARTCASH FUNCTION STARTS
	 ******************************************************************/
		public function get_smartcash_details(){
		$data = array();
		$postdata = $this->input->post();
		$location_uid = $postdata['location_uid'];
		
		$content = '';
		$this->db->select("*")->from("foodiq_smartcash");
		$this->db->where(array('location_uid' => $location_uid, 'is_deleted' => '0'));
		$this->db->order_by('id', desc);
		$payrollQuery = $this->db->get();
		if($payrollQuery->num_rows() > 0){
			$i = 1;
			foreach($payrollQuery->result() as $payobj){
				$status = 0;
				$change_status = '';
				$is_expire = 0;
				if($payobj->status == '1'){
					$status = 0;
					$change_status = 'Pause';
					$payroll_status = 'Active';
				}else{
					$status = 1;
					$change_status = 'Play';
					$payroll_status = 'Inactive';
					
				}
				$campaign_type = '';
				if($payobj->campaign_type == 1){
					$campaign_type = 'Combo';
				}
				else if($payobj->campaign_type == 2){
					$campaign_type = 'Product';
				}
				else if($payobj->campaign_type == 3){
					$campaign_type = 'Liquidation';
				}
				else{
					$campaign_type = 'Purchase Value';	
				}
				$total_days_left = '';
				if(strtotime($payobj->end_date) < strtotime(date('Y-m-d'))){
					$total_days_left = 'Expired';
					$payroll_status = 'Expired';
					$is_expire = 1;
				}
				else{
					$date1 = date_create(date('Y-m-d'));
					$date2 = date_create(date('Y-m-d',strtotime($payobj->end_date)));
					$diff = date_diff($date1,$date2);
					$total_days_left = $diff->format("%a");
				}
				if(strtotime($payobj->start_date) > strtotime(date('Y-m-d'))){
					$date1 = date_create(date('Y-m-d'));
					$date2 = date_create(date('Y-m-d',strtotime($payobj->start_date)));
					$diff = date_diff($date1,$date2);
					$days_diff = $diff->format("%a");
					$payroll_status = 'Will start in '.$days_diff. " days";
				}
				$content .= "<tr>";
				$content .= '<td>'.$payobj->campaign_name.'</td>';
				$content .= '<td>'.$campaign_type.'</td>';
				$content .= '<td>'.date('d-m-Y',strtotime($payobj->start_date)).'</td>';
				$content .= '<td>'.date('d-m-Y',strtotime($payobj->end_date)).'</td>';
				$content .= '<td>'.$total_days_left.'</td>';
				$content .= '<td>'.$payroll_status.'</td>';
				if($is_expire == 0){
					$content .= '<td><span class="mui-btn mui-btn--small mui-btn--accent " style="cursor: pointer; margin:0px; font-size:10px" onclick="active_deactive_smartcash_camp('.$payobj->id.', '.$status.')">
				'.$change_status.'
				</span></td>';
				}else{
					$content .= '<td>&nbsp;</td>';
				}
				
				$content .= '<td>
				
				<span style="margin-right:10px;cursor: pointer;">
				<i class="fa fa-trash fa-lg" aria-hidden="true" onclick="delete_smartcash_camp('.$payobj->id.')"></i>
				</span></td>';
				$content .= "</tr>";
				//<span style="margin-right:10px;cursor: pointer;">
				//<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true" ></i>
				//</span>
				$i++;
			}
		}
		else{
			$content .= '<tr><td colspan="7">No Data Found.</td></tr>';
		}
		
		$data['employee_details'] = $content;
		echo json_encode($data);
	}

	public function get_smartcash_product(){
		$data = array();
		$postdata = $this->input->post();
		$location_uid = $postdata['location_uid'];
		
		$content = '';
		$content .= '<option value="" data-product_value="0">Select Product</option>';
		$this->db->select("fm.id,fmm.product_titlename,fmm.food_item_price")->from("foodiq_menulist as fm");
		$this->db->where(array('fm.location_uid' => $location_uid, 'fmm.is_deleted' => '0', 'fmm.status' => '1'));
		$this->db->join('foodiq_master_menulist as fmm', 'fm.master_menu_id = fmm.id', 'inner');
		$this->db->order_by('fmm.product_titlename', asc);
		$payrollQuery = $this->db->get();
		//echo $this->db->last_query();
		if($payrollQuery->num_rows() > 0){
			$i = 1;
			foreach($payrollQuery->result() as $payobj){
				
				$content .= '<option value="'.$payobj->id.'" data-product_value="'.$payobj->food_item_price.'">'.$payobj->product_titlename.'</option>';
				
				$i++;
			}
		}
		
		$data['employee_details'] = $content;
		echo json_encode($data);
	}
	public function create_smartcash_campaign(){
		$return_value = 0;
		$all_ok = 1;
		
		$postdata = $this->input->post();
		if($postdata['campaign_type'] == 2){
			// check already live campaign with this product;
			$this->db->select('fs.id')->from('foodiq_smartcash as fs');
			$this->db->where(array('fs.location_uid' => $postdata['location_uid'],'fs.campaign_type' => 2, 'fs.is_deleted' => 0, 'fsi.is_deleted' => 0, 'fsi.menu_id' => $postdata['smartcash_product']));
			$this->db->where(array('DATE(fs.end_date) >=' => date('Y-m-d')));
			$this->db->join('foodiq_smartcash_item as fsi', 'fs.id = fsi.smartcash_id', 'inner');
			$check = $this->db->get();
			if($check->num_rows() > 0){
				$all_ok = 0;
				$return_value = 1;
			}
		}
		else if($postdata['campaign_type'] == 1){
			// check already live campaign with this combo;
			$this->db->select('fs.id, GROUP_CONCAT(fsi.menu_id SEPARATOR ",") AS Menu')->from('foodiq_smartcash as fs');
			$this->db->where(array('fs.location_uid' => $postdata['location_uid'],'fs.campaign_type' => 1, 'fs.is_deleted' => 0, 'fsi.is_deleted' => 0));
			$this->db->where(array('DATE(fs.end_date) >=' => date('Y-m-d')));
			$this->db->join('foodiq_smartcash_item as fsi', 'fs.id = fsi.smartcash_id', 'inner');
			$this->db->group_by("fsi.smartcash_id");
			$check = $this->db->get();
			//echo $this->db->last_query();die;
			if($check->num_rows() > 0){
				$is_find = 0;
				$Prd_data = json_decode(stripslashes($postdata['combo_product_id']));
				foreach($check->result() as $i_row){
					$menu = $i_row->Menu;
					$menu_array = array_map('trim',explode(',', $menu));
					asort($menu_array);
					asort($Prd_data);
					if($menu_array == $Prd_data){
						$is_find = 1;
						break;
					}
					//$array_difference = array_diff($Prd_data , $menu_array);
					//if(count($array_difference) == 0){
					//	$is_find = 1;
					//	break;
					//}
				}
				if($is_find == 1){
					$all_ok = 0;
					$return_value = 2;
				}
				
			}
		}
		if($all_ok == 1){
			$payrolldata = array(
				'location_uid' =>$postdata['location_uid'],
				'campaign_name' => $postdata['campaign_name'],
				'campaign_type' => $postdata['campaign_type'],
				'start_date' => date('Y-m-d H:i:s', strtotime($postdata['smartcash_start_date'])),
				'end_date' => date('Y-m-d H:i:s', strtotime($postdata['smartcash_end_date'])),
				'purchase_value' => $postdata['purchase_value'],
				'smartcash_value' => $postdata['smartcash_value'],
				'status' => 1,
				'created_on' => date('Y-m-d H:i:s'),
			);
			$this->db->insert('foodiq_smartcash', $payrolldata);
			$smartcash_id = $this->db->insert_id();
			if($postdata['campaign_type'] == 2 || $postdata['campaign_type'] == 3){
				$item_data = array(
					'smartcash_id' =>$smartcash_id,
					'menu_id' => $postdata['smartcash_product'],
					'added_on' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('foodiq_smartcash_item', $item_data);
			}
			else if($postdata['campaign_type'] == 1){
				$Prd_data = json_decode(stripslashes($postdata['combo_product_id']));
				foreach($Prd_data as $prd_id){
					$item_data = array(
						'smartcash_id' =>$smartcash_id,
						'menu_id' => $prd_id,
						'added_on' => date('Y-m-d H:i:s'),
					);
					$this->db->insert('foodiq_smartcash_item', $item_data);
				}
			}
			$return_value = 'ok';
		}
		
		
		echo json_encode($return_value);
	}
	public function delete_smartcash_camp(){
		$return_value = 1;
		$postdata = $this->input->post();
		$camp_id = $postdata['camp_id'];
		$this->db->update('foodiq_smartcash', array('is_deleted' => '1','updated_on' => date('Y-m-d H:i:s')), array('id' => $camp_id));
		echo json_encode($return_value);
	}
	public function get_liquidation_product(){
		$data = array();
		$postdata = $this->input->post();
		$location_uid = $postdata['location_uid'];
		$menu_created = array();
		$this->db->select('fs.id, fsi.menu_id')->from('foodiq_smartcash as fs');
		$this->db->where(array('fs.location_uid' => $postdata['location_uid'],'fs.campaign_type' =>3, 'fs.is_deleted' => 0, 'fsi.is_deleted' => 0));
		$this->db->where(array('DATE(fs.end_date) >=' => date('Y-m-d')));
		$this->db->join('foodiq_smartcash_item as fsi', 'fs.id = fsi.smartcash_id', 'inner');
		$check = $this->db->get();
		if($check->num_rows() > 0){
			foreach($check->result() as $c_row){
				$menu_created[] = $c_row->menu_id;
			}
		}
			
		$content = '';
		$this->db->select("fm.id,fm.product_titlename,fm.food_item_price, fmm.is_perishable, fmm.perishable_days")->from("foodiq_menulist as fm");
		$this->db->where(array('fm.location_uid' => $location_uid, 'fm.is_deleted' => '0'));
		if(count($menu_created) > 0){
			$this->db->where_not_in('fm.id', $menu_created);
		}
		$this->db->join('foodiq_master_menulist as fmm', 'fm.food_item_barcode = fmm.food_item_barcode', 'inner');
		$this->db->order_by('id', desc);
		$payrollQuery = $this->db->get();
		if($payrollQuery->num_rows() > 0){
			$i = 1;
			$content .= "<tr>";
			$content .= '<th>Product Name</th>';
			$content .= '<th>Expiry Date</th>';
			$content .= '<th>Days To Expiry</th>';
			$content .= '<th>Price</th>';
			$content .= '<th>SmartCash Price</th>';
			$content .= '<th>Action</th>';
			$content .= '</tr>';
			$start_date = date('Y-m-d');
			$end_date = date('Y-m-d', strtotime($start_date. ' + 3 days'));
			foreach($payrollQuery->result() as $payobj){
				
				//$perishable_days = $payobj->perishable_days;
				//$is_perishable = $payobj->is_perishable;
				//$create_start = date('Y-m-d', strtotime($start_date. ' - '.$perishable_days.' days'));
				//$create_end = date('Y-m-d', strtotime($end_date. ' - '.$perishable_days.' days'));
				//if($is_perishable == '1'){
				//	$invntryQ = $this->db2->query("SELECT COALESCE(SUM(quantity),0) as storeqty FROM foodiq_inventory_actions WHERE menu_id='".$payobj->id."' AND action='addtoshelf' AND date(added_on) between '$create_start' AND '$create_end'");
				//}else{
				//	$invntryQ = $this->db2->query("SELECT COALESCE(SUM(quantity),0) as storeqty FROM foodiq_inventory_actions WHERE menu_id='".$payobj->id."' AND action='addinventory' AND date(added_on) between '$create_start' AND '$create_end'");
				//}
				//if($invntryQ->num_rows() > 0){
				//	$laststoreqty = $invntryQ->row()->storeqty;
				//
				//	$order_qty = 0;
				//	$orderQuery = $this->db2->query("SELECT COALESCE(SUM(tb2.qty),0) as itemorderqty FROM `foodiq_order` as tb1 INNER JOIN `foodiq_order_details` as tb2 ON(tb1.id=tb2.order_id) WHERE date(added_on) between '$create_start' AND '$create_end' AND tb2.menu_id='".$payobj->id."'");
				//	if($orderQuery->num_rows() > 0){
				//	    $order_qty = $orderQuery->row()->itemorderqty;
				//	}
				//	if($order_qty > $laststoreqty){
				//		$data['recommend_trash_qty'] = '0';
				//	}
				//}
				$content .= "<tr>";
				$content .= '<td>'.$payobj->product_titlename.'</td>';
				$content .= '<td>'.$payobj->product_titlename.'</td>';
				$content .= '<td>'.$payobj->product_titlename.'</td>';
				$content .= '<td id="smartcash_product_price_'.$payobj->id.'">'.$payobj->food_item_price.'</td>';
				$content .= '<td><input id="smartcash_price_'.$payobj->id.'" type="text" placeholder="Enter price" style="width:80px; height:25px; border:1px solid #ccc;box-shadow:none; text-align: center; font-size:12px; padding:5px;"></td>';
				$content .= '<td><span onclick="create_liquidation_smartcash('.$payobj->id.')" class="mui-btn mui-btn--os-accent" style="height:30px;margin-right:0px;margin-bottom:5px; margin-top:0px;line-height: 25px; font-size:12px; padding:0 20px;">Launch</span></td>';
				$content .= '</tr>';
				
				$i++;
			}
		}
		else{
			$content .= '<tr><td colspan="6">No product will expire in next two days.</td></tr>';
		}
		
		$data['employee_details'] = $content;
		echo json_encode($data);
	}
	
	public function active_deactive_smartcash_camp(){
		$return_value = 1;
		$postdata = $this->input->post();
		$camp_id = $postdata['camp_id'];
		$status = $postdata['status'];
		$this->db->update('foodiq_smartcash', array('status' =>$status,'updated_on' => date('Y-m-d H:i:s')), array('id' => $camp_id));
		echo json_encode($return_value);
	}
	
	/*******************************************************************
	 *	FOODLE FUNCTION STARTS
	 ******************************************************************/
	public function foodle_loyalty_reward_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."foodle_loyalty_reward_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content .= '<option value="">Select Reward</option>';
		if($responce_data->resultCode == 1){
			foreach($responce_data->loyalty_reward as $responce_data1){
				$content .= '<option value="'.$responce_data1->id.'">'.$responce_data1->reward_name.'</option>';
			}
		}
		
		return $content;
		
	}
	public function foodle_location_loyalty_reward_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."foodle_location_loyalty_reward_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			foreach($responce_data->loyalty_reward as $responce_data1){
				$content .= '<tr>';
				$content .= '<td class="text-left">'.$responce_data1->reward_name.'</td>';
				$content .= '<td><span onclick="show_loyalty_reward_image(\''.$responce_data1->reward_image.'\')" class="reward-image-table" style="cursor:pointer;"> <img src="'.$responce_data1->reward_image.'"  class="img-responsive"   alt=""/> </span></td>';
				$content .= '<td>';
				$content .= '<span> <i  class="fa fa-pencil-square-o fa-lg" aria-hidden="true" style="cursor:pointer" onclick="location_loyalty_reward_modal_open('.$responce_data1->id.', \''.$responce_data1->reward_name.'\')"></i></span>';
				$content .= '&nbsp;&nbsp; <span><i class="fa fa-trash fa-lg" aria-hidden="true" style="cursor:pointer" onclick="location_loyalty_reward_delete('.$responce_data1->id.')"></i> </span>';
				$content .='</td>';
				$content .= '</tr>';
			}
		}
		else{
			$content .= '<td colspan="3">No reward added</td>';
		}
		return $content;
		
	}
	public function foodle_add_loyalty_rewards(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'loyalty_reward' => $this->input->post("loyalty_reward"),
			'loyalty_reward_name' => $this->input->post("loyalty_reward_name"),
			'location_loyalty_reward' => $this->input->post("location_loyalty_reward"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."foodle_add_loyalty_rewards";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	

	
	public function location_loyalty_reward_delete(){
		$requestData = array(
			'location_reward_id' => $this->input->post('location_reward_id')
		);
		$service_url = $this->api_url."location_loyalty_reward_delete";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function foodle_slab_location_loyalty_reward_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."foodle_location_loyalty_reward_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value="">Select Reward</option>';
		if($responce_data->resultCode == 1){
			foreach($responce_data->loyalty_reward as $responce_data1){
				$content .= '<option value="'.$responce_data1->id.'">'.$responce_data1->reward_name.'</option>';
			}
		}
		
		return $content;
		
	}
	public function foodle_add_loyalty_slab(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'slab_loyalty_reward_amount' => $this->input->post("slab_loyalty_reward_amount"),
			'slab_location_loyalty_reward' => $this->input->post("slab_location_loyalty_reward"),
			'foodle_slab_id' => $this->input->post("foodle_slab_id"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."foodle_add_loyalty_slab";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function foodle_location_loyalty_slab_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."foodle_location_loyalty_slab_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			foreach($responce_data->loyalty_reward as $responce_data1){
				$content .= '<tr>';
				$content .= '<td class="text-left">Rs. '.$responce_data1->slab_amount.'</td>';
				$content .= '<td class="text-left">'.$responce_data1->reward_name.'</td>';
				$content .= '<td>';
				$content .= '<span> <i  class="fa fa-pencil-square-o fa-lg" aria-hidden="true" style="cursor:pointer" onclick="location_loyalty_reward_slab_modal_open('.$responce_data1->slab_id.', \''.$responce_data1->slab_amount.'\')"></i></span>';
				$content .= '&nbsp;&nbsp; <span><i class="fa fa-trash fa-lg" aria-hidden="true" style="cursor:pointer" onclick="location_loyalty_slab_delete('.$responce_data1->slab_id.')"></i> </span>';
				$content .='</td>';
				$content .= '</tr>';
			}
		}
		else{
			$content .= '<td colspan="3">No slab added</td>';
		}
		return $content;
		
	}
	public function location_loyalty_slab_delete(){
		$requestData = array(
			'location_reward_id' => $this->input->post('location_reward_id')
		);
		$service_url = $this->api_url."location_loyalty_slab_delete";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function foodle_location_loyalty_reward_audience_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."foodle_location_loyalty_reward_audience_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			foreach($responce_data->loyalty_reward as $responce_data1){
				$content .= '<li>';
				$content .= '<a href="javascript:void(0)"> ';
				$content .= '<h4>'.$responce_data1->audience_name.'</h4>';
				$content .= '<h5>Rule: '.$responce_data1->rules.'</h5>';
				$content .= '<h3>0 users</h3>';
				$content .= '<h4>VIEW</h4>';
				$content .= '<div class="box-footer"><span style="margin:10px; cursor: pointer">
                                               <i onclick="location_loyalty_audience_edit_modal_open('.$responce_data1->id.', \''.$responce_data1->audience_name.'\', '.$responce_data1->audiance_duration.', '.$responce_data1->visit_from_condition.', '.$responce_data1->visit_from_value.', '.$responce_data1->visit_to_conditon.', '.$responce_data1->visit_to_value.', '.$responce_data1->visit_spend_between_condition.', '.$responce_data1->spend_from_condition.', '.$responce_data1->spend_from_value.', '.$responce_data1->spend_to_condition.', '.$responce_data1->spend_to_value.')" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                             </span>';
				$content .= '<span style="margin:10px; cursor: pointer">
                                               <i onclick="location_loyalty_audience_delete('.$responce_data1->id.')" class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                             </span></div>';
				$content .= '</a>';
				$content .= '</li>';
			}
		}
		else{
			$content .= 'No Audience created';	
		}
		
		return $content;
		
	}
	
	public function foodle_add_loyalty_audience(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'foodle_audience_name' => $this->input->post("foodle_audience_name"),
			'foodle_audience_time_duration' => $this->input->post("foodle_audience_time_duration"),
			'foodle_audience_visit_from_conditon' => $this->input->post("foodle_audience_visit_from_conditon"),
			'foodle_audience_visit_from_value' => $this->input->post("foodle_audience_visit_from_value"),
			'foodle_audience_visit_to_conditon' => $this->input->post("foodle_audience_visit_to_conditon"),
			'foodle_audience_visit_to_value' => $this->input->post("foodle_audience_visit_to_value"),
			'foodle_audience_spend_from_conditon' => $this->input->post("foodle_audience_spend_from_conditon"),
			'foodle_audience_spend_from_value' => $this->input->post("foodle_audience_spend_from_value"),
			'foodle_audience_spend_to_conditon' => $this->input->post("foodle_audience_spend_to_conditon"),
			'foodle_audience_spend_to_value' => $this->input->post("foodle_audience_spend_to_value"),
			'foodle_audience_visit_spend_conditon' => $this->input->post("foodle_audience_visit_spend_conditon"),
			'audience_id' => $this->input->post("audience_id"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."foodle_add_loyalty_audience";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function foodle_campaign_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."foodle_campaign_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			foreach($responce_data->loyalty_reward as $responce_data1){
				$content .= '<tr>';
				$content .= '<td>'.$responce_data1->campaign_name.'</td>';
				$content .= '<td>'.$responce_data1->reward_name.'</td>';
				$content .= '<td>'.$responce_data1->audience_name.'</td>';
				$content .= '<td>'.$responce_data1->start_date.'</td>';
				$content .= '<td>'.$responce_data1->end_date.'</td>';
				$content .= '<td>'.$responce_data1->redemption_expiryday.'</td>';
				$content .= '<td>'.$responce_data1->status.'</td>';
				$content .= '<td>';
				$content .= '<span><i class="fa fa-trash fa-lg" aria-hidden="true" style="cursor:pointer" onclick="foodle_campaign_delete('.$responce_data1->campaign_id.')"></i> </span>';
				$content .='</td>';
				$content .= '</tr>';
			}
		}
		else{
			$content .= '<tr><td colspan="7">No campaign created</td></tr>';	
		}
		
		return $content;
		
	}

	
	
	public function foodle_campaign_audience_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."foodle_location_loyalty_reward_audience_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value="">Select Audience</option>';
		if($responce_data->resultCode == 1){
			foreach($responce_data->loyalty_reward as $responce_data1){
				$content .= '<option value="'.$responce_data1->id.'">'.$responce_data1->audience_name.'</option>';
			}
		}
		
		return $content;
		
	}
	public function foodle_add_campaign(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'foodle_campaign_name' => $this->input->post("foodle_campaign_name"),
			'foodle_campaign_location_reward' => $this->input->post("foodle_campaign_location_reward"),
			'foodle_campaign_location_audience' => $this->input->post("foodle_campaign_location_audience"),
			'foodle_campaign_start_date' => $this->input->post("foodle_campaign_start_date"),
			'foodle_campaign_end_date' => $this->input->post("foodle_campaign_end_date"),
			'foodle_campaign_expire_day' => $this->input->post("foodle_campaign_expire_day"),
			'foodle_campaign_id' => $this->input->post("foodle_campaign_id"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."foodle_add_campaign";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	


	public function foodle_campaign_delete(){
		$requestData = array(
			'foodle_campaign_id' => $this->input->post('foodle_campaign_id')
		);
		$service_url = $this->api_url."foodle_campaign_delete";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	
	public function location_loyalty_audience_delete(){
		$requestData = array(
			'audience_id' => $this->input->post('audience_id')
		);
		$service_url = $this->api_url."location_loyalty_audience_delete";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}

}


?>