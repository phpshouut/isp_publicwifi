<?php
class Permission_model extends CI_Model{
    
     private $DB2;
    public function __construct(){
        $this->DB2 = $this->load->database('db2', TRUE);
    }
	public function get_isplogo(){
		$isp_uid = 0;
		if(defined('ISPID')){
			$isp_uid = ISPID;
		}
		$query = $this->DB2->query("select small_image,logo_image from sht_isp_detail where status='1' AND isp_uid='".$isp_uid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			//return $rowdata->small_image;
			if($rowdata->small_image != ''){
			      return S3IMAGEPATH."isp/isp_logo/small/".$rowdata->small_image;
			}else{
			      return S3IMAGEPATH."isp/isp_logo/logo/".$rowdata->logo_image;
			}
		}else{
			return  0;
		}
		
        }
     public function isp_portal_permission(){
	  $isp_portal_permission = 0;
	  $isp_channel_permission = 0;
	  
	  $isp_publicwifi_permission = 0;
        
        $isp_id = ISPID;
        $query = $this->DB2->query("select * from sht_isp_admin_modules where isp_uid = '$isp_id' AND status = '1'");
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                //for isp main portal permission
                if($row->module == '1'){
                    $isp_portal_permission = 1;
                }
		elseif($row->module == '8'){
		    $isp_channel_permission = 1;
		}
		elseif($row->module == '7'){
		    $isp_publicwifi_permission = 1;
		}
               
            }
        }
	$data = array();
	$data['isp_portal_permission'] = $isp_portal_permission;
	$data['isp_channel_permission'] = $isp_channel_permission;
	$data['isp_publicwifi_permission'] = $isp_publicwifi_permission;
	return $data;
       
    }
    
     public function get_isp_name(){
	  $isp_uid = 0;
		if(defined('ISPID')){
			$isp_uid = ISPID;
		}
		  $data = array();
		  $isp_name = '';
		  $fevicon_icon = '';
		  $query = $this->DB2->query("select isp_name,fevicon_icon from sht_isp_admin where isp_uid = '$isp_uid'");
		  if($query->num_rows() > 0){
			   $row = $query->row_array();
			   $isp_name = $row['isp_name'];
			   $fevicon_icon = $row['fevicon_icon'];
		  }
		  $data['isp_name'] = $isp_name;
		  $data['fevicon_icon'] = $fevicon_icon;
		  return $data;
     }
   
}


?>
