$('body').on('keyup', '.bill_amount', function(){
    var billamt = $(this).val();
    var amtdisc = $('.bill_discount').val();
    if (amtdisc != '') {
        billamt = Math.ceil(billamt - ((billamt * amtdisc) / 100));
    }
    $('.bill_total_amount').val(billamt);
});

$('body').on('change', '#install_bill_discount', function(){
    //alert('hi');
    var amtdisc = $(this).val();
    var billamt = $('#install_bill_amount').val();
    if (amtdisc != '' && billamt != '') {
        billamt = Math.ceil(billamt - ((billamt * amtdisc) / 100));
    }
    $('input[name="install_bill_total_amount"]').val(billamt);
});

$('body').on('change', '#security_bill_discount', function(){
    //alert('hi');
    var amtdisc = $(this).val();
    var billamt = $('#security_bill_amount').val();
    if (amtdisc != '' && billamt != '') {
        billamt = Math.ceil(billamt - ((billamt * amtdisc) / 100));
    }
    $('input[name="security_bill_total_amount"]').val(billamt);
});

$('body').on('change', '.advprepay_month_count', function(){
    var plan_monthly_cost = $('#plan_monthly_cost').val();
    var amtdisc = $('.advprepay_bill_discount').val();
    var month_count = $(this).val();
    var total_cost = plan_monthly_cost * month_count;
    $('input[name="advprepay_bill_amount"]').val(total_cost);
    if (amtdisc != '') {
        total_cost = Math.ceil(total_cost - ((total_cost * amtdisc) / 100));
    }
    $('input[name="advprepay_bill_total_amount"]').val(total_cost);
});

$('body').on('change', '.advprepay_bill_discount', function(){
    var plan_monthly_cost = $('#plan_monthly_cost').val();
    var month_count = $('.advprepay_month_count').val();
    var total_cost = plan_monthly_cost * month_count;
    var amtdisc = $(this).val();
    if (amtdisc != '' && total_cost != '') {
        total_cost = Math.ceil(total_cost - ((total_cost * amtdisc) / 100));
    }
    $('input[name="advprepay_bill_total_amount"]').val(total_cost);
});


/*$('body').on('change', 'input[name="bill_connection_type"]', function(){
    var val = this.value;
    if (val == 'prepaid') {
        $('.connection_type').val('prepaid');
        $('.prebilling_option').removeClass('hide');
        $('.postbilling_option').addClass('hide');
    }else if (val == 'postpaid') {
        $('.connection_type').val('postpaid');
        $('.prebilling_option').addClass('hide');
        $('.postbilling_option').removeClass('hide');
    }
});*/

$('body').on('click', '#editclick_billing_details', function(){
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        $('.loading').removeClass('hide');
        var subsid = $('.subscriber_userid').val();
        var subs_uuid = $('.subscriber_uuid').val();
        $.ajax({
            url: base_url+'user/planassoc_withuser',
            type: 'POST',
            dataType: 'json',
            data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
            success: function(data){
                $('.loading').addClass('hide');
                if (data == 0) {
                    $('#exceedlimit').addClass('hide');
                    $('#inactive_billing').removeClass('hide');
                    $('#active_billing').addClass('hide');
                }else{
                    $('#inactive_billing').addClass('hide');
                    $('#active_billing').removeClass('hide');
                    user_billing_listing();
                }
                //$('#inactive_billing').addClass('hide');
                //$('#active_billing').removeClass('hide');
            }
        });
    }
});

$('body').on('click', '#_installationModal', function(){
    $('#installation_billing_form')[0].reset();
    $('.bill_amount').val('');
    $('.bill_discount').val('');
    
    $('input').removeClass('mui--is-touched mui--is-not-empty');
    var d = new Date();
    var time = '   '+d.toLocaleTimeString();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var currdate = ((''+day).length<2 ? '0' : '')+day + '-' +
                 ((''+month).length<2 ? '0' : '')+month + '-' +
                 d.getFullYear() ;
    var seconds = d.getSeconds();
    var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
    var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
    var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
    $('input[name="install_bill_datetime"]').remove();
    $('input[name="install_bill_datetimeformat"]').remove();
    $('input[name="install_bill_number"]').val(tktdate+tkttime);
    $('#install_billdtme').append('<input name="install_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
    $('#install_billdtme').append('<input name="install_bill_datetime" type="hidden" value="'+dbtktdatetime+'">');
    $('#installationModal').modal('show');
});

$('body').on('click', '#_securityModal', function(){
    $('#security_billing_form')[0].reset();
    $('.bill_amount').val('');
    $('.bill_discount').val('');
    
    $('input').removeClass('mui--is-touched mui--is-not-empty');
    var d = new Date();
    var time = '    '+d.toLocaleTimeString();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var currdate = ((''+day).length<2 ? '0' : '')+day + '-' +
                 ((''+month).length<2 ? '0' : '')+month + '-' +
                 d.getFullYear() ;
    var seconds = d.getSeconds();
    var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
   var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
    var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
    $('input[name="security_bill_datetime"]').remove();
    $('input[name="security_bill_datetimeformat"]').remove();
    //$('input[name="security_receipt_number"]').val(tktdate+tkttime);
    $('#security_billdtme').append('<input name="security_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
    $('#security_billdtme').append('<input name="security_bill_datetime" type="hidden" value="'+dbtktdatetime+'">');
    $('#securityModal').modal('show');
});

$('body').on('click', '#_advance_prepayModal', function(){
    $('#advancedpay_billing_form')[0].reset();
    $('.adverr').addClass('hide');
    $('.bill_amount').val('');
    $('.bill_discount').val('');
    
    $subs_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/userassoc_plan_pricing',
        type: 'POST',
        dataType: 'json',
        data: 'subs_uuid='+$subs_uuid,
        async: false,
        success: function(data){
            $('#userassoc_planid').val(data.planid);
            $('#plan_monthly_cost').val(data.price);
            $('select[name="advprepay_month_count"]').val(1);
            $('input[name="advprepay_bill_total_amount"]').val(data.price);
            $('input[name="advprepay_bill_amount"]').val(data.price);
        }
    });
    var d = new Date();
    var time = '    '+d.toLocaleTimeString();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var currdate = ((''+day).length<2 ? '0' : '')+day + '-' +
                 ((''+month).length<2 ? '0' : '')+month + '-' +
                 d.getFullYear() ;
    var seconds = d.getSeconds();
    var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
    var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
    var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
    $('input[name="advprepay_bill_datetime"]').remove();
    $('input[name="advprepay_bill_datetimeformat"]').remove();
    $('input[name="advprepay_bill_number"]').val(tktdate+tkttime);
    $('#advprepay_billdtme').append('<input name="advprepay_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
    $('#advprepay_billdtme').append('<input name="advprepay_bill_datetime" type="hidden" value="'+dbtktdatetime+'">');
    $('#advance_prepayModal').modal('show');
});


$('body').on('click', '#_addtowallet', function(){
    $('#addtowallet_billing_form')[0].reset();
    var d = new Date();
    var time = '    '+d.toLocaleTimeString();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var currdate = ((''+day).length<2 ? '0' : '')+day + '-' +
                 ((''+month).length<2 ? '0' : '')+month + '-' +
                 d.getFullYear() ;
    var seconds = d.getSeconds();
    var tkttime = d.getHours() +""+ d.getMinutes() +""+ ((''+seconds).length<2 ? '0' : '')+seconds ;
    var tktdate = day +""+ month +""+ d.getFullYear().toString().substr(-2) ;
    var dbtktdatetime =  d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '')+month + '-' + ((''+day).length<2 ? '0' : '')+day + ' ' + d.getHours() +":"+ d.getMinutes() +":"+ ((''+seconds).length<2 ? '0' : '')+seconds ;;
    $('input[name="addtowallet_bill_datetime"]').remove();
    $('input[name="addtowallet_bill_datetimeformat"]').remove();
    //$('input[name="addtowallet_bill_number"]').val(tktdate+tkttime);
    $('#addtowallet_billdtme').append('<input name="addtowallet_bill_datetimeformat" readonly="readonly" class="mui--is-empty mui--is-untouched mui--is-pristine" type="text" value="'+currdate+time+'">');
    $('#addtowallet_billdtme').append('<input name="addtowallet_bill_datetime" type="hidden" value="'+dbtktdatetime+'">');
    $('#addtowalletModal').modal('show');
});

function installation_charges() {
    $('.loading').removeClass('hide');
    $('#installationModal').modal('hide');
    var formdata =  $('#installation_billing_form').serialize();
    $.ajax({
        url: base_url+'user/add_installation_charges',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $.ajax({
                url: base_url+'user/user_billing_listing',
                type: 'POST',
                dataType: 'json',
                data: 'subscriber_uuid='+data,
                async: false,
                success: function(result){
                    $('.loading').addClass('hide');
                    $('#bill_summary').html(result.bill_listing);
                }
            });
        }
    }); 
}

function security_charges() {
    $('.loading').removeClass('hide');
    $('#securityModal').modal('hide');
    var formdata =  $('#security_billing_form').serialize();
    $.ajax({
        url: base_url+'user/add_security_charges',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $.ajax({
                url: base_url+'user/user_billing_listing',
                type: 'POST',
                dataType: 'json',
                data: 'subscriber_uuid='+data,
                async: false,
                success: function(result){
                    $('.loading').addClass('hide');
                    $('#bill_summary').html(result.bill_listing);
                }
            });
        }
    }); 
}

function advance_prepaid(add_receipt = '') {
    var free_months = $('select[name="advprepay_freemonth_count"] option:selected').val();
    var discount = $('select[name="advprepay_bill_discount"] option:selected').val();

    if ((free_months != '0') && (discount != '0')) {
        $('.adverr').removeClass('hide');
        return false;
    }else{
        $('.adverr').addClass('hide');
        $('.loading').removeClass('hide');
        $('#advance_prepayModal').modal('hide');
        $('#editadvance_prepayModal').modal('hide');
        if (add_receipt != ''){
            var formdata =  $('#advancedpay_receipt_form').serialize();
        }else{
            var formdata =  $('#advancedpay_billing_form').serialize();
        }
        $.ajax({
            url: base_url+'user/advance_prepaid_charges',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            async: false,
            success: function(data){
                var subscid = data.subscid;
                $.ajax({
                    url: base_url+'user/user_billing_listing',
                    type: 'POST',
                    dataType: 'json',
                    data: 'subscriber_uuid='+subscid,
                    async: false,
                    success: function(result){
                        $('.loading').addClass('hide');
                        $('#wallet_credit_limit').html('&#8377; '+result.user_creditlimit+'.00');
                        $('#user_wallet_amt').html('&#8377;  '+result.wallet_amt);
                        $('#net_walletamt').val(result.wallet_amt);
                        $('#plan_blocked_amt').html('&#8377;  '+result.adv_totalpay);
                        $('#bill_summary').html(result.bill_listing);
                    }
                });
            }
        });
        return true;
    }
}


function user_billing_listing() {
    $('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/user_billing_listing',
        type: 'POST',
        dataType: 'json',
        data: 'subscriber_uuid='+cust_uuid,
        async: false,
        success: function(result){
            //alert(result.change_limit_upto);
            $('.loading').addClass('hide');
            $('#wallet_credit_limit').html('&#8377; '+result.user_creditlimit+'.00');
            $('#user_wallet_amt').html('&#8377;  '+result.wallet_amt);
            $('#net_walletamt').val(result.wallet_amt);
            $('input[name="update_walletLimit"]').attr("min", result.change_limit_upto);
            $('#plan_blocked_amt').html('&#8377;  '+result.adv_totalpay);
            $('#bill_summary').html(result.bill_listing);
        }
    });
}

function addtowallet(add_receipt = '') {
    $('.loading').removeClass('hide');
    $('#addtowalletModal').modal('hide');
    $('#addwallet_receiptModal').modal('hide');
    if (add_receipt != ''){
        var formdata =  $('#addwallet_receiptform').serialize();
    }else{
        var formdata =  $('#addtowallet_billing_form').serialize();
    }

    $.ajax({
        url: base_url+'user/addtowallet',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $.ajax({
                url: base_url+'user/user_billing_listing',
                type: 'POST',
                dataType: 'json',
                data: 'subscriber_uuid='+data,
                async: false,
                success: function(result){
                    $('.loading').addClass('hide');
                    $('#wallet_credit_limit').html('&#8377; '+result.user_creditlimit+'.00');
                    $('#user_wallet_amt').html('&#8377;  '+result.wallet_amt);
                    $('#net_walletamt').val(result.wallet_amt);
                    $('#bill_summary').html(result.bill_listing);
                }
            });
        }
    }); 
}


function show_updateReceiptModal(billid, billtype) {
    $.ajax({
        url: base_url+'user/getbilldetail',
        type: 'POST',
        data: 'billid='+billid,
        dataType: 'json',
        async: false,
        success: function(data){
            if (typeof data.billid != 'undefined') {
                if (billtype == 'advprepay') {
                    $('input[name="advprepay_chqddrecpt"]').val('');
                    $('.adverr').addClass('hide');
                    $('input[name="advprepay_bill_datetime"]').remove();
                    $('#dbbilldatetime').append('<input name="advprepay_bill_datetime" type="text" style="color:#ccc" readonly>');
                    $('#receipt_billid').val(billid);
                    $('input[name="advprepay_bill_datetime"]').val(data.bill_added_on);
                    $('input[name="advprepay_bill_number"]').val(data.bill_number);
                    $('select[name="advprepay_bill_payment_mode"]').val(data.paymode);
                    $('input[name="advprepay_month_count"]').val(data.advancepay_for_months);
                    $('input[name="advprepay_freemonth_count"]').val(data.number_of_free_months);
                    $('input[name="advprepay_bill_amount"]').val(data.actual_amount);
                    $('input[name="advprepay_bill_discount"]').val(data.discount);
                    $('input[name="advprepay_bill_total_amount"]').val(data.total_amount);
                    $('#planmonthly_cost').val(data.plan_cost);
                    $('#user_planid').val(data.srvid);
                    
                    $('#billmonth_count').val(data.advancepay_for_months);
                    $('#billfreemonth_count').val(data.number_of_free_months);
                    $('#bill_monthdiscount').val(data.discount);
                    $('#editadvance_prepayModal').modal('show');
                
                }else if (billtype == 'addtowallet') {
                    $('input[name="addtowallet_receipt_number"]').val('');
                    $('input[name="addtowallet_bill_datetime"]').remove();
                    $('#dbwallet_billdtme').append('<input name="addtowallet_bill_datetime" type="text" style="color:#ccc" readonly>');
                    $('input[name="addtowallet_bill_datetime"]').val(data.bill_added_on);
                    $('#walletpayment_mode').val(data.paymode);
                    $('input[name="addtowallet_payment_mode"]').val(data.paymode);
                    $('input[name="addtowallet_amount"]').val(data.actual_amount);
                    $('input[name="wallet_billid"]').val(billid);
                    $('#addwallet_receiptModal').modal('show');
                }else{
                    $('#billid_toupdate').val(billid); 
                    $('input[name="update_receipt_number"]').val('');
                    $('select[name="update_payment_mode"]').val(data.paymode);
                    $('#enterReceiptModal').modal('show');
                }
            }
        }
    });
    
}

function update_receiptform() {
    var c = confirm("Please be ensure RECEIPT NUMBER is entered correctly.");
    if (c) {
        $('.loading').removeClass('hide');
        var formdata = $('#receiptupdate_form').serialize();
        $.ajax({
            url: base_url+'user/update_receipt_number',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function(data){
                $('#enterReceiptModal').modal('hide');
                user_billing_listing();
                $('.loading').addClass('hide');
            }
        });
    }
}

function delete_billentry(billid) {
    var c = confirm("Are you sure, You want to delete this bill ?");
    if (c) {
        $('.loading').removeClass('hide');
        $.ajax({
            url: base_url+'user/delete_billentry',
            type: 'POST',
            dataType: 'json',
            data: 'billid='+billid,
            success: function(data){
                user_billing_listing();
                $('.loading').addClass('hide');
            }
        });
    }
}

function exceed_walletlimit(slug='') {
    if (slug == 'modalshow') {
        $('#exceedWalletLimitModal').modal('show');
        $('input[name="update_walletLimit"]').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
        $('input[name="update_walletLimit"]').val('');
    }
    var walletLimit = $('input[name="update_walletLimit"]').val();
    var uuid = $('.subscriber_uuid').val();

    if (walletLimit != '') {
        $.ajax({
            url: base_url+'user/update_walletLimit',
            type: 'POST',
            dataType: 'json',
            data: 'walletLimit='+walletLimit+'&uuid='+uuid,
            success: function(data){
                $('#exceedWalletLimitModal').modal('hide');
                if (data.user_suspended == '1') {
                    $('#userstatus_changealert').val(0);
                    $('#userstatus_changealert').parent().removeClass('btn-success');
                    $('#userstatus_changealert').parent().addClass('btn-danger off');
                }
                user_billing_listing();
                $('.loading').addClass('hide');
            }
        });
    }
}

function defaultbilltab_details() {
    $('.loading').removeClass('hide');
    var subsid = $('.subscriber_userid').val();
    var subs_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/planassoc_withuser',
        type: 'POST',
        dataType: 'json',
        data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
        success: function(data){
            $('.loading').addClass('hide');
            if (data == 0) {
                $('#exceedlimit').addClass('hide');
                $('#inactive_billing').removeClass('hide');
                $('#active_billing').addClass('hide');
            }else{
                $('#inactive_billing').addClass('hide');
                $('#active_billing').removeClass('hide');
                user_billing_listing();
            }
            //$('#inactive_billing').addClass('hide');
            //$('#active_billing').removeClass('hide');
        }
    });
}
