$(document).ready(function(){
        var plan_type=$('#plantype_id').val();
       // alert(plan_type);
    
      
         if(plan_type ==="TP")
          {
            $('.timeplan').css('display','block');
              $('.dataplan').css('display','none');
              $('.fupclass').css('display','none');  
               $('.timelimit').prop('required',true);
			   $('.timelimith').prop('required',false);
            $('.data_limit').prop('required',false);
            $('.data_limith').prop('required',false);
            
             
              
          }
          else if( plan_type ==="HP")
          {
            $('.timeplan').css('display','none');
              $('.dataplan').css('display','none');
              $('.fupclass').css('display','block'); 
                $('.timelimit').prop('required',false);
                $('.data_limit').prop('required',false);
                  $('.timelimith').prop('required',true);
                $('.data_limith').prop('required',true);
             
             
          }
          else if(plan_type ==="DP")
          {
                $('.timeplan').css('display','none');
              $('.dataplan').css('display','block');
              $('.fupclass').css('display','none');  
              $('.timelimit').prop('required',false);
              $('.data_limit').prop('required',true);
               $('.timelimith').prop('required',false);
                $('.data_limith').prop('required',false);
          }
    
  $('input:radio[name="plan_type_radio"]').change(function(){
     // if ($(this).val() == 'Paid')
      //    var plan_type=$('#plantype_id').val();
          if($(this).val() ==="TP")
          {
            $('.timeplan').css('display','block');
              $('.dataplan').css('display','none');
              $('.fupclass').css('display','none');  
               $('.timelimit').prop('required',true);
            $('.data_limit').prop('required',false);
              $('.data_limith').prop('required',false);
            $('.timelimith').prop('required',false);
             
              
          }
          else if( $(this).val() ==="HP")
          {
             
            $('.timeplan').css('display','none');
              $('.dataplan').css('display','none');
              $('.fupclass').css('display','block'); 
                $('.timelimit').prop('required',false);
                $('.data_limit').prop('required',false);
                  $('.timelimith').prop('required',true);
                $('.data_limith').prop('required',true);
             
             
          }
          else if($(this).val() ==="DP")
          {
                $('.timeplan').css('display','none');
              $('.dataplan').css('display','block');
              $('.fupclass').css('display','none');  
              $('.timelimit').prop('required',false);
              $('.data_limit').prop('required',true);
               $('.timelimith').prop('required',false);
                $('.data_limith').prop('required',false);
          }
          if($(this).val()==="UP")
          {
              $('.timeplan').css('display','none');
              $('.dataplan').css('display','none');
              $('.fupclass').css('display','none');
              $('.timelimit').prop('required',false);
              $('.time_calcn').prop('required',false);
              $('.data_limit').prop('required',false);
              $('.data_calcn').prop('required',false);
              $('.fup_dwnl_rate').prop('required',false);
              $('.fup_upld_rate').prop('required',false);
          }
          else if($(this).val()==="TP")
          {
            $('.timeplan').css('display','block');
              $('.dataplan').css('display','none');
              $('.fupclass').css('display','none');  
                $('.timelimit').prop('required',true);
              $('.time_calcn').prop('required',true);
              $('.data_limit').prop('required',false);
              $('.data_calcn').prop('required',false);
              $('.fup_dwnl_rate').prop('required',false);
              $('.fup_upld_rate').prop('required',false);
          }
          else if($(this).val()==="FP")
          {
            $('.timeplan').css('display','none');
              $('.dataplan').css('display','block');
              $('.fupclass').css('display','block'); 
                $('.timelimit').prop('required',false);
              $('.time_calcn').prop('required',false);
              $('.data_limit').prop('required',true);
              $('.data_calcn').prop('required',true);
              $('.fup_dwnl_rate').prop('required',true);
              $('.fup_upld_rate').prop('required',true);
          }
          else if($(this).val()==="DP")
          {
                $('.timeplan').css('display','none');
              $('.dataplan').css('display','block');
              $('.fupclass').css('display','none');  
              $('.timelimit').prop('required',false);
              $('.time_calcn').prop('required',false);
              $('.data_limit').prop('required',true);
              $('.data_calcn').prop('required',true);
              $('.fup_dwnl_rate').prop('required',false);
              $('.fup_upld_rate').prop('required',false);
          }
      
  });
  
  
  
   $('input:radio[name="plan_pricing_radio"]').change(function(){
     
          if($(this).val()==="Paid")
          {
            // alert('xxxxx');
              $('.gross_amt').prop('required',true);
              $('.tax ').prop('required',true);
               $('.tax ').removeAttr("disabled");
                $('.gross_amt ').removeAttr("disabled");
              $('.net_amount').prop('required',true);
              
          }
          else if($(this).val()==="Free")
          {
            $('.gross_amt').val("");
          //  $('.tax').val("");
            $('.net_amount').val("");
              $('.gross_amt').prop('required',false);
              $('.tax ').prop('required',false);
               $('.tax ').prop("disabled", "disabled");
                $('.gross_amt ').prop("disabled", "disabled");
              $('.net_amount').prop('required',false);
          }
         
      
  });
  
  
   $('input:radio[name="topup_type_radio"]').change(function(){
     // if ($(this).val() == 'Paid')
      //    var plan_type=$('#plantype_id').val();
     //  alert($(this).val());
          if($(this).val()==="1")
          {
              $('.data_topup').css('display','block');
              $('.unaccountancy').css('display','none');
              $('.speed_topup').css('display','none');
              $('.topup_data').prop('required',true);
              $('.data_added').prop('required',true);
              $('.unaccounting').prop('required',false);
              $('.unacctstart_time').prop('required',false);
              $('.unacctstop_time').prop('required',false);
              $('.unacct_days').prop('required',false);
              $('.dwnld_speed').prop('required',false);
              $('.upld_speed').prop('required',false);
              $('.speed_boost_start').prop('required',false);
              $('.speed_boost_stop').prop('required',false);
               $('.speedboost_days').prop('required',false);
              
          }
          else if($(this).val()==="2")
          {
             // alert('xxx');
           $('.data_topup').css('display','none');
              $('.unaccountancy').css('display','block');
              $('.speed_topup').css('display','none');
              $('.topup_data').prop('required',false);
              $('.data_added').prop('required',false);
              $('.unaccounting').prop('required',true);
              $('.unacctstart_time').prop('required',true);
              $('.unacctstop_time').prop('required',true);
              $('.unacct_days').prop('required',true);
              $('.dwnld_speed').prop('required',false);
              $('.upld_speed').prop('required',false);
              $('.speed_boost_start').prop('required',false);
              $('.speed_boost_stop').prop('required',false);
               $('.speedboost_days').prop('required',false);
          }
          else if($(this).val()==="3")
          {
            $('.data_topup').css('display','none');
              $('.unaccountancy').css('display','none');
              $('.speed_topup').css('display','block');
              $('.topup_data').prop('required',false);
              $('.data_added').prop('required',false);
              $('.unaccounting').prop('required',false);
              $('.unacctstart_time').prop('required',false);
              $('.unacctstop_time').prop('required',false);
              $('.unacct_days').prop('required',false);
              $('.dwnld_speed').prop('required',true);
              $('.upld_speed').prop('required',true);
              $('.speed_boost_start').prop('required',true);
              $('.speed_boost_stop').prop('required',true);
               $('.speedboost_days').prop('required',true);
          }
         
      
  });
  
  
$(document).on('click','.slide_usable',function(){
    
    var is_usable=$('#is_usable').val();
    if(is_usable==="1")
    {
        $('#is_usable').val('0');
    }
    else
    {
        $('#is_usable').val('1');
    }
})

$(document).on('click','.sliderburst',function(){
    
    var is_usable=$('#is_burst_enable').val();
    if(is_usable==="1")
    {
        $('#is_burst_enable').val('0');
         $('.dwnld_burst').prop('required',false);
              $('.dwnld_threshold').prop('required',false);
              $('.upld_burst').prop('required',false);
              $('.upld_threshold').prop('required',false);
              $('.burst_time').prop('required',false);
              $('.burst_priority').prop('required',false);
    }
    else
    {
        $('#is_burst_enable').val('1');
       $('.dwnld_burst').prop('required',true);
              $('.dwnld_threshold').prop('required',true);
              $('.upld_burst').prop('required',true);
              $('.upld_threshold').prop('required',true);
              $('.burst_time').prop('required',true);
              $('.burst_priority').prop('required',true);
    }
})
   
   var timer = null;
    $(document).on('keyup','.tax',function(){
         $('.btn_pricing').prop("disabled", "disabled");
          $('.btn_toppricing').prop("disabled", "disabled");
        
        var grssamt=$('.gross_amt').val();
       // alert(grssamt);
        if(isNaN(grssamt))
        {
          //  alert(' number');
            $('.amterror').html("Enter the Gross Amount");
            return false;
        }
        else
        {
           // alert('not number');
            $('.amterror').html("");
        }
          clearTimeout(timer);
         timer = setTimeout(function () {
              
                var taxrate = $('.tax').val();
                var taxamt = Math.round((taxrate / 100) * grssamt);
                var netamt=parseInt(grssamt) + parseInt(taxamt);
               // alert(netamt);
                 $('#net_amt').html(commaSeparateNumber(netamt));
                 $('#net_amount').val(netamt);
                  $('.btn_pricing').removeAttr('disabled');
                    $('.btn_toppricing').removeAttr('disabled');
         },1000);
         
        
    });
    
    var timer = null;
      $(document).on('keyup','.gross_amt',function(){
         
          $('.btn_pricing').prop("disabled", "disabled");
          $('.btn_toppricing').prop("disabled", "disabled");
        var grssamt=$('.gross_amt').val();
       // alert(grssamt);
       if(isNaN(grssamt))
        {
          //  alert(' number');
            $('.amterror').html("Enter the Gross Amount");
            return false;
        }
        else
        {
           // alert('not number');
            $('.amterror').html("");
        }
          clearTimeout(timer);
         timer = setTimeout(function () {
              
                var taxrate = $('.tax').val();
                var taxamt = Math.round((taxrate / 100) * grssamt);
                var netamt=parseInt(grssamt) + parseInt(taxamt);
              //  alert(netamt);
                 $('#net_amt').html(commaSeparateNumber(netamt));
                 $('#net_amount').val(netamt);
                 $('.btn_pricing').removeAttr('disabled');
                    $('.btn_toppricing').removeAttr('disabled');
         },1000);
         
        
    });
    
    
    
     $('input:radio[name="dwnld_speed_multiple"]').change(function(){
       //  alert('xxxxxxxx');
         $(this).parents().find('.dwnld_speed').val("");
         $(this).parents().find('.dwnld_speed').prop('required',false);
            $(this).parents().find('.dwnld_speed').prop("disabled", "disabled");
     });
     
     
      $('input:radio[name="upld_speed_multiple"]').change(function(){

         $(this).parents().find('.upld_speed').val("");
         $(this).parents().find('.upld_speed').prop('required',false);
            $(this).parents().find('.upld_speed').prop("disabled", "disabled");
     });
    
    
    $(document).on('click','#inlineCheckbox1',function(){
    if (this.checked) {
        
        $('.unacct_days').prop('checked',true);
       $('.unacct_days').prop('required',false);
    }
    else
    {
      
         $('.unacct_days').prop('checked',false);
          $('.unacct_days').prop('required',true);
    }
});

 $(document).on('click','#spinlineCheckbox1',function(){
    if (this.checked) {
        $('.speedboost_days').prop('checked',true);
         $('.speedboost_days').prop('required',false);
    }
    else
    {
         $('.speedboost_days').prop('checked',false);
         $('.speedboost_days').prop('required',true);
    }
});


$(document).on('click','.speedboost_days',function(){
    if (this.checked) {
        
         $('.speedboost_days').prop('required',false);
    }
    else
    {
         $('.speedboost_days').prop('required',true);
    }
});

$(document).on('click','.unacct_days',function(){
    if (this.checked) {
        
         $('.unacct_days').prop('required',false);
    }
    else
    {
       var checklen= $('input[class=unacct_days]:checked').length;
     //  alert(checklen);
       if(checklen===0)
       {
         $('.unacct_days').prop('required',true);
        }
        else
        {
             $('.unacct_days').prop('required',false);
        }
    }
});

 
    
});


function add_plan()
{
     var formdata = $("#add_plan").serialize();
     var is_edit=$('.is_frmedit').val();
   // alert(data);
      $('.loading').removeClass('hide');
     $.ajax({
        url: base_url+'plan/add_plan_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
         //   alert(data);
            if(is_edit==0)
            {
                $('.loading').addClass('hide');
                mui.tabs.activate('Plan-settings');
                $('.btn_setting').removeAttr('disabled');
                $('.plan_id').val(data);
                $('.tab_menu').css({'cursor': 'pointer'});
                $('.tab_menu').removeAttr('disabled'); 
            }
            else
            {
                window.location = base_url+"plan";
            }
         
           /* $('#usertype_customer').prop('checked','checked');
            $('.radio-inline').css('color', '#b3b3b3');
            $('input[type="radio"][name="user_type_radio"]').prop('disabled','disabled');
            mui.tabs.activate('KYC_details');*/
        }
    });
}




function add_plan_setting()
{
     var formdata = $("#add_plan_setting").serialize();
   // alert(data);
    var is_edit=$('.is_frmedit').val();
     $('.loading').removeClass('hide');
     $.ajax({
        url: base_url+'plan/add_plan_setting',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
         
              if(is_edit==0)
            {
                 $('.loading').addClass('hide');
             mui.tabs.activate('Pricing');
             $('.btn_pricing').removeAttr('disabled');
            }
            else
            {
                window.location = base_url+"plan";
            }
          
          
        }
    });
}


function add_pricing()
{
     var formdata = $("#add_pricing").serialize();
       var is_edit=$('.is_frmedit').val();
   // alert(data);
     $('.loading').removeClass('hide');
     $.ajax({
        url: base_url+'plan/add_pricing',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
         
            
            if (is_edit == 0)
            {
                mui.tabs.activate('add-region');
                $('.loading').addClass('hide');
                $('.btn_region').removeAttr('disabled');
            } else
            {
                window.location = base_url + "plan";
            }
         // window.location = base_url+"plan";
          
        }
    });
}

function add_topuppricing()
{
     var formdata = $("#add_pricing").serialize();
      var is_edit=$('.is_formedit').val();
   // alert(data);
     $('.loading').removeClass('hide');
     $.ajax({
        url: base_url+'plan/add_topuppricing',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            if(is_edit==0)
            {
                  $('.loading').addClass('hide');
            // mui.tabs.activate('Pricing');
            mui.tabs.activate('add-region'); 
               $('.btn_topregion').removeAttr('disabled');
   
            }else
            {
                window.location = base_url+"plan/topup"; 
            }
         
     //     window.location = base_url+"plan/topup";
          
        }
    });
}

function add_topup()
{
     var formdata = $("#add_topup").serialize();
      var is_edit=$('.is_formedit').val();
    $('.loading').removeClass('hide');
     $.ajax({
        url: base_url+'plan/add_topup_data',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
          //  alert(data);
          if(is_edit==0)
          {
               $('.loading').addClass('hide');
             mui.tabs.activate('Pricing');
             $('.btn_toppricing').removeAttr('disabled');
             //$('.limiui').removeClass('plan_mui--is-active');
            // $('.Plan-settings').addClass('plan_mui--is-active');
            $('.topup_id').val(data);
          $('.tab_menu').css({'cursor' : 'pointer'});
            $('.tab_menu').removeAttr('disabled');
          }
          else
          {
              window.location = base_url + "plan/topup";
          }
         
           
        }
    });
}


function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  
  function change_planstatus(id){
       $('#erromsg').html("Are you sure you want to Delete Plan ?" );
                $('.delyes').removeClass('hide'); 
        var id_post = id;
        var task=$('a#'+id_post).find('img').attr('rel');
      //  alert(id_post+":::"+task);
      //  return false;
      $('.loading').removeClass('hide');
        $.ajax({
            type: "POST",
            
            url: base_url+"plan/changeplan_status",
            cache: false,
            dataType: 'json',
            data: "id="+id_post+'&task='+task,
            success: function(data){
                $('.loading').addClass('hide');
                if(data.status==0)
                {
                   $('#Deleteplan_confirmation_alert').modal('show'); 
                  $('#erromsg').html(data.msg );
                $('.delyes').addClass('hide'); 
                }
                else
                {
                     $('a#'+id_post).html(data.msg);
                }
              //  $('a#'+id_post).html(data);
            }
        });
    }
    
    
     function change_topupstatus(id){
       $('#erromsg').html("Are you sure you want to Delete Top Up ?" );
                $('.delyes').removeClass('hide'); 
        var id_post = id;
        var task=$('a#'+id_post).find('img').attr('rel');
        $.ajax({
            type: "POST",
            
            url: base_url+"plan/changetopup_status",
            cache: false,
            dataType: 'json',
            data: "id="+id_post+'&task='+task,
            success: function(data){
                if(data.status==0)
                {
                   $('#Deletetopup_confirmation_alert').modal('show'); 
                  $('#erromsg').html(data.msg );
                $('.delyes').addClass('hide'); 
                }
                else
                {
                     $('a#'+id_post).html(data.msg);
                }
              //  $('a#'+id_post).html(data);
            }
        });
    }
    
    
    function check_tax_plan()
    {
         $('.loading').removeClass('hide');
        $('#taxmsg').html('');
        var id=1;
          $.ajax({
            type: "POST",
            
            url: base_url+"plan/check_taxfilled",
            cache: false,
            data: "id="+id,
            success: function(data){
              $('.loading').addClass('hide');
                if(data==0)
                {
                    $('#taxmsg').html('Please enter Tax before creating Plan');
                 $('#taxerr').modal('show');
                }
                else
                {
                     window.location = base_url+'plan/add_plan';
                }
              //  $('a#'+id_post).html(data);
            }
        });
    }
    
    function check_tax_topup()
    {
        $('#taxmsg').html('');
        $('.loading').removeClass('hide');
        var id=1;
          $.ajax({
            type: "POST",
            
            url: base_url+"plan/check_taxfilled",
            cache: false,
            data: "id="+id,
            success: function(data){
               // alert(data);
                $('.loading').addClass('hide');
                if(data==0)
                {
                    $('#taxmsg').html('Please enter Tax before creating Topup');
                 $('#taxerr').modal('show');
                }
                else
                {
                     window.location = base_url+'plan/add_topup';
                }
              //  $('a#'+id_post).html(data);
            }
        });
    }





