function setup_adduser() {
    $.ajax({
        url: base_url+'user/setupfor_adduser',
        type: 'POST',
        success: function(data){
            if (data == 0){
                alert("Please setup the Billing Cycle from Setup -> Billing");
            }else{
                window.location = base_url+'user/add';
            }
        }
    });
}

$('body').on('click', '#editclick_personal_details', function(){
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        personaldetails_listing();
    }
});
$('body').on('click', '#editclick_kyc_details', function(){
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        $('#subscriber_documents_form')[0].reset();
        $('#default_iddocdiv').removeClass('hide');
        $('.idproofdocdiv ').remove();
        $('#idprooflisting').html('');
        $('#defaultdocdiv').removeClass('hide');
        $('.subsdocdiv').remove();
        $('#kyclisting').html('');
        $('#docpreview').html('');
        $('.idproof_docnumber').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
        $('.kycdocnumber').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
        $('#user_image').html('');
        $('#user_signature').html('');
        
        $('#idproof_docnumber_arr').val('');
        $('#idproof_doctype_arr').val('');
        $('#kyc_documents_arr').val('');
        $('#kycdocnumber_arr').val('');
        
        filecount = 0; filearr = 0; filedoctype = []; kycdocnumb = []; abc = 0; sno = 0;
        idp_filecount = 0; idp_filearr = 0; idp_filedoctype = []; iddocnumb = []; idp_abc = 0; idp_sno = 0;
        
        edit_allkyc_details();
    }
});
$('body').on('click', '#editclick_plan_details', function(){
    var idclass = $(this).is('[disabled=disabled]');
    if(idclass !== true){
        $('#active_plan').css('color', '#424143');
        $('#next_cycle_plan').css('color', '#29abe2');
        $('#subscriber_plandetail_form').removeClass('hide');
        $('#nextcycle_planform').addClass('hide');
        subscriber_plan_details();
    }
});
$('body').on('change', '.ipaddrtype', function(){
    if (this.value == 0) {
        $('.addstaticip').removeClass('hide');
        $('#ipaddress').attr('required',true);
    }else{
        $('.addstaticip').addClass('hide');
        $('#ipaddress').attr('required',false);
    }
});

/********************* PLAN TABBING ************************/
$('body').on('click', '.plantabb', function(){
    var tabclick = $(this).attr('id');
    if (tabclick == 'active_plan') {
        $(this).css('color', '#424143');
        $('#next_cycle_plan').css('color', '#29abe2');
        $('#subscriber_plandetail_form').removeClass('hide');
        $('#nextcycle_planform').addClass('hide');
    }else if (tabclick == 'next_cycle_plan') {
        $(this).css('color', '#424143');
        $('#active_plan').css('color', '#29abe2');
        $('#subscriber_plandetail_form').addClass('hide');
        $('#nextcycle_planform').removeClass('hide');
        nextcycle_planlist();
    }
});

$('body').on('change', '#user_activate_check', function(){
    var user_activate_check = $(this).val();
    var subsid = $('.subscriber_userid').val();
    $('#user_activate_check').val(0);
    $('#user_activate_check').parent().removeClass('btn-success');
    $('#user_activate_check').parent().addClass('btn-danger off');
    if (subsid == '') {
        $('#user_inactiveAlert').modal('show');
    }else{
        $.ajax({
           url: base_url+'user/user_activate_check',
           type: 'POST',
           data: 'subsid='+subsid+'&is_user_activated='+user_activate_check,
           dataType: 'json',
           success: function(data){
                var otp = data.useract_otp;
                var personal_details = data.personal_details;
                var kyc_details = data.kyc_details;
                var plan_details = data.plan_details;
                var installation_details = data.installation_details;
                var security_details = data.security_details;
                
                if (otp != '0') {
                    $('#user_inactiveAlert').modal('hide');
                    $('#otp_to_activate_user').val(otp);
                    $('#activate_username').html(data.username);
                    var custphone = data.phone;
                    $('#phone_to_activate_user').val(custphone);
                    for (idx=2; idx < 7 ; idx++) {
                        custphone = custphone.substring(0, idx) + '*' + custphone.substring(idx+1);
                    }
                    $('#activate_userphone').html(custphone);
                    
                    $('#user_activate_check').parent().removeClass('btn-success');
                    $('#user_activate_check').parent().addClass('btn-danger off');
                    $('#user_activatedModal').modal('show');
                }else{
                    $('#user_activate_check').val(0);
                    $('#user_activate_check').parent().removeClass('btn-success');
                    $('#user_activate_check').parent().addClass('btn-danger off');
                    
                    if (personal_details == 0) {
                        $('#pedactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#pedactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if (kyc_details == 0) {
                        $('#kycactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#kycactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if (plan_details == 0) {
                        $('#planactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#planactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if ((installation_details == 0) || (security_details == 0)) {
                        $('#inscactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#inscactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    
                    $('#user_inactiveAlert').modal('show');
                }
           }
        });
    }
});


function getcitylist(stateid, subscriber='', cityid='') {
    $('.zonelist').empty().append('<option value="">Zone*</option>');
    $('.search_zonelist').empty().append('<option value="">All Zones</option>');
    var pin = '';
    if (subscriber != '') {
        pin = $( "#state_customer option:selected" ).attr('data-pincode');
    }
    $.ajax({
        url: base_url+'user/getcitylist',
        type: 'POST',
        dataType: 'text',
        data: 'stateid='+stateid+'&pinid='+pin+'&cityid='+cityid,
        success: function(dataa){
            if (dataa.indexOf("~~~") != -1 ) {
                var dataarr = dataa.split('~~~');
                if (subscriber != '') {
                    $('input[name="subscriber_uuid"]').val(dataarr[0]);
                    $('input[name="subscriber_username"]').val(dataarr[0]);
                }
                $('.citylist').empty().append('<option value="">City*</option>');
                $('.citylist').append(dataarr[1]);
                $('.search_citylist').empty().append('<option value="">All Cities</option>');
                $('.search_citylist').append(dataarr[1]);
                $('.search_citylist option[value="addc"]').remove();
            }else{
                $('.citylist').empty().append('<option value="">City*</option>');
                $('.citylist').append(dataa);
                $('.search_citylist').empty().append('<option value="">All Cities</option>');
                $('.search_citylist').append(dataa);
                $('.search_citylist option[value="addc"]').remove();
            }
        }
    });
}

function getzonelist(cityid, zoneid='') {
    var state = $('select[name="state"]').val();
    if (cityid == 'addc') {
        $('input[name="add_city_state"]').val(state);
        $('.city_text').val('');
        $('#add_cityModal').modal('show');
    }else{
        $.ajax({
            url: base_url+'user/getzonelist',
            type: 'POST',
            dataType: 'text',
            data: 'stateid='+state+'&cityid='+cityid+'&zoneid='+zoneid,
            success: function(data){
                $('.zonelist').empty().append('<option value="">Zone*</option>');
                $('.zonelist').append(data);
                $('.search_zonelist').empty().append('<option value="">All Zones</option>');
                $('.search_zonelist').append(data);
                $('.search_zonelist option[value="addz"]').remove();
            }
        });
    }
}

function check_toadd_newzone(zoneid, type) {
    if (type == 'leadenquiry') {
        var state = $('#state_leadenquiry option:selected').val();
        var city = $('#city_leadenquiry option:selected').val();    
    }else{
        var state = $('#state_customer option:selected').val();
        var city = $('#city_customer option:selected').val();
    }
    
    //alert(state+'@@@@'+city);
    if (zoneid == 'addz') {
        $('.zone_text').val('');
        $('input[name="state_id"]').val(state);
        $('input[name="add_zone_city"]').val(city);
        $('#add_zoneModal').modal('show');
    }
}

function add_newcity() {
    var formdata = $('#addcity_fly').serialize();
    $.ajax({
        url: base_url+'user/add_newcity',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            $('input[name="add_city_state"]').val('');
            $('#add_cityModal').modal('hide');
            var addoption = data.html;
            //alert('"' + addoption + '"');
            //$(".citylist option:last").before('"' + addoption + '"');
            $(".citylist").append('"' + addoption + '"');
            $(".zonelist").append('<option class="addzonefly" value="addz">--Add Zone--</option>');
        }
    })
}

function add_newzone() {
    var formdata = $('#addzone_fly').serialize();
    $.ajax({
        url: base_url+'user/add_newzone',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            $('input[name="state_id"]').val('');
            $('input[name="add_zone_city"]').val('');
            $('#add_zoneModal').modal('hide');
            var addoption = data.html;
            //$(".zonelist option:last").before('"' + addoption + '"');
            $(".zonelist").append('"' + addoption + '"');
        }
    })
}

function resetcityvalue() {
    $('.citylist').val('');
}
function resetzonevalue() {
    $('.zonelist').val('');
}

function add_customer(event) {
    $('.loading').removeClass('hide');
    var formdata = $('#user_type_customer').serialize();
    $.ajax({
        url: base_url+'user/add_customer',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            //alert(data);
            var subsid = data.customer_id;
            var letoc_dataid = $('input[name="letoc_dataid"]').val();
            //alert('le='+letoc_dataid);
            if ((typeof letoc_dataid != 'undefined') && (letoc_dataid != '0')) {
                window.location = base_url+'user/edit/'+subsid;
            }else{
                $('.subscriber_userid').val(data.customer_id);
                $('.tab_menu').css({'cursor' : 'pointer'});
                $('.tab_menu').removeAttr('disabled');
                $('#usertype_customer').attr('checked','checked');
                $('.radio-inline').css('color', '#b3b3b3');
                $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
                
                $('input[name="customer_uuid"]').val(data.customer_uuid);
                $('.subscriber_uuid').val(data.customer_uuid);
                $('.customer_name').html(data.customer_name);
                $('.customer_email').html(data.customer_email);
                $('.customer_mobile').html('+91 '+data.customer_mobile);
                
                $('.loading').addClass('hide');
                $('.active_addc').removeClass('hide');
                window.scrollTo(0,0);
                mui.tabs.activate('KYC_details');
            }
        }
    })
}

function add_subscriber_documents(event) {
    $('.loading').removeClass('hide');
    var fd = new FormData();
    var atleast_onefile = 0; var atleast_oneidpfile = 0;
    var totalfile = $('input[name="subscriber_documents[]"]').length;
    if (totalfile > 0) {
        atleast_onefile = $('input[name^="subscriber_documents"]')[0].files.length;
    }
    
    var total_idpfile = $('input[name="idproof_documents[]"]').length;
    if (total_idpfile > 0) {
        atleast_oneidpfile = $('input[name^="idproof_documents"]')[0].files.length;       
    }
    
    var user_profile_image = $('input[name="user_profile_image"]')[0].files[0];
    var user_signature_image = $('input[name="user_signature_image"]')[0].files[0];
    
    //alert(totalfile +'#######'+ total_idpfile);
    //alert(atleast_onefile +'@@@@@@@'+ atleast_oneidpfile);
    //alert(user_profile_image);
    if ((atleast_onefile == 0) && (atleast_oneidpfile == 0)) {
        $('.loading').addClass('hide');
        $('.subsdocdiv').addClass('hide');
        $('#defaultdocdiv').removeClass('hide');
        $('.idproofdocdiv').addClass('hide');
        $('#default_iddocdiv').removeClass('hide');
        alert('Please upload all related documents.');
    }else if (atleast_onefile == 0) {
        $('.loading').addClass('hide');
        //$('input[name^="subscriber_documents"]').remove();
        //$('[id^=abcd]').remove();
        //$('select[name="kyc_documents"]').val('');
        $('.subsdocdiv').addClass('hide');
        $('#defaultdocdiv').removeClass('hide');
        alert('Please upload atleast one KYC document.');
    }else if (atleast_oneidpfile == 0) {
        $('.loading').addClass('hide');
        //$('input[name^="idproof_documents"]').remove();
        //$('[id^=abcd]').remove();
        //$('select[name="kyc_documents"]').val('');
        $('.idproofdocdiv').addClass('hide');
        $('#default_iddocdiv').removeClass('hide');
        alert('Please upload atleast one PHOTO ID document.');
    }
    else if(typeof user_profile_image == 'undefined'){
        $('.loading').addClass('hide');
        alert('Please upload user image.');
    }else{
        //alert('ok');
        for (var j=0; j< totalfile; j++) {
            $.each($('input[name^="subscriber_documents"]')[j].files, function(l, file) {
                fd.append(j, file);
            });
        }
        
        j = j-1;
        for (var k=0; k< total_idpfile; k++) {
            $.each($('input[name^="idproof_documents"]')[k].files, function(l, ifile) {
                fd.append(j, ifile);
                j++;
            });
        }
        if (typeof user_profile_image != 'undefined') {
            fd.append(j, user_profile_image);
            j++;
        }
        if (typeof user_signature_image != 'undefined') {
            fd.append(j, user_signature_image);
        }
        
        var other_data = $('#subscriber_documents_form').serializeArray();
        $.each(other_data,function(key,input){
            fd.append(input.name,input.value);
        });
        
        $.ajax({
            url: base_url+'user/upload_documents',
            data: fd,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                //alert(data);
                $('.subscriber_userid').val(data);
                $('#usertype_customer').attr('checked','checked');
                $('.radio-inline').css('color', '#b3b3b3');
                $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
                
                //$('select[name="kyc_documents"]').val('');
                //$('input[name^="subscriber_documents"]').remove();
                $('.subsdocdiv').addClass('hide');
                $('#defaultdocdiv').removeClass('hide');
                $('.idproofdocdiv').addClass('hide');
                $('#default_iddocdiv').removeClass('hide');
                
                $('#doclisting').html('');
                $('.loading').addClass('hide');
                subscriber_plan_details();
                //mui.tabs.activate('Plan_details');
                
            }
        });
    }
}

function add_ticket_request() {
    $('.loading').removeClass('hide');
    $('#serviceRequestModal').modal('hide');
    var formdata = $('#ticket_request_form').serialize();
    $.ajax({
        url: base_url+'user/add_ticket_request',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            $('.loading').addClass('hide');
            var cust_uuid = $('input[name="subscriber_uuid"]').val();
            $.ajax({
                url: base_url+'user/fetch_allticket',
                type: 'POST',
                data: "cust_uuid="+cust_uuid,
                success: function(data){
                    $('.loading').addClass('hide');
                    $('#allticketdiv').html(data);
                }
            });
        }
    });
}

function show_edit_ticket_request(tktid='') {
    $('.loading').removeClass('hide');
    $('#edit_ticket_request_form')[0].reset();
    
    var formdata = $('#edit_ticket_request_form').serialize();
    $.ajax({
        url: base_url+'user/edit_ticket_request',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: formdata+'&tktid='+tktid,
        success: function(data){
            
            var cust_uuid = data.subscriber_uuid;
            $.ajax({
                url: base_url+'user/ticket_sorters_list',
                type: 'POST',
                async: false,
                data: 'uuid='+cust_uuid,
                success: function(data){
                   $('select[name="edticket_assignto"]').empty().append('<option value="">Assign Ticket</option>');
                   $('select[name="edticket_assignto"]').append(data);
                }
            });
            
            $('input[name="edtktid"]').val(data.tid);
            $('input[name="edticketid"]').val(data.ticket_id);
            $('input[name="edcustomer_id"]').val(data.subscriber_id);
            $('input[name="edcustomer_uuid"]').val(data.subscriber_uuid);
            $('select[name="edticket_type"]').val(data.ticket_type);
            $('select[name="edticket_priority"]').val(data.ticket_priority);
            if (data.ticket_assign_to == '0') {
                $('select[name="edticket_assignto"]').prop('disabled', false);
            }else{
                $('select[name="edticket_assignto"]').prop('disabled', true);
                $('select[name="edticket_assignto"]').val(data.ticket_assign_to);
            }
            $('textarea[name="edticket_description"]').val(data.ticket_description);
            
            var tcarr = data.ticket_comment;
            if (tcarr.length > 0) {
                $('.pre_tcomment').remove();
                for (i=0; i < tcarr.length; i++) {
                    $('#edit_tcomment_list').append("<input type='text' class='pre_tcomment' value='"+tcarr[i]+"' readonly >")
                }
            }
            //$('input[name="edticket_comment"]').val(data.ticket_comment);
            $('input[name="edticket_datetime"]').val(data.ticket_added_on);
            $('.loading').addClass('hide');            
            $('#editserviceRequestModal').modal('show');
        }
    });
}

function edit_ticket_request() {
    $('.loading').removeClass('hide');
    var formdata = $('#edit_ticket_request_form').serialize();
    tktid = $('input[name="edtktid"]').val();
    $.ajax({
        url: base_url+'user/edit_ticket_request',
        type: 'POST',
        dataType: 'json',
        async: false,
        data: formdata+'&tktid='+tktid,
        success: function(data){
            $('.loading').addClass('hide');
            $('#editserviceRequestModal').modal('hide');
            fetch_allticket();
        }
    });
}

function fetch_allticket() {
    //$('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
        url: base_url+'user/fetch_allticket',
        type: 'POST',
        data: "cust_uuid="+cust_uuid,
        success: function(data){
            $('.loading').addClass('hide');
            $('#allticketdiv').html(data);
        }
    });
}

function personaldetails_listing() {
    $('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_userid').val();
    $.ajax({
        url: base_url+'user/personaldetails_listing',
        type: 'POST',
        dataType: 'json',
        data: "sid="+cust_uuid,
        success: function(data){
            if (data.record.length != 0) {
                for (var i=0; i<data.record.length; i++) {
                    var firstname = data.record[i].firstname;
                    var lastname = data.record[i].lastname;
                    var middlename = data.record[i].middlename;
                    var email = data.record[i].email;
                    var mobile = data.record[i].mobile;
                    var dob = data.record[i].dob;
                    var flat_number = data.record[i].flat_number;
                    var address1 = data.record[i].address;
                    var address2 = data.record[i].address2;
                    var state = data.record[i].state;
                    var city = data.record[i].city;
                    var zone = data.record[i].zone;
                    var usuage_locality = data.record[i].usuage_locality;
                    var username = data.record[i].username;
                    var uid = data.record[i].uid;
                    var priority_account = data.record[i].priority_account;
                    
                    $('input[name="subscriber_fname"]').val(firstname);
                    $('input[name="subscriber_mname"]').val(middlename);
                    $('input[name="subscriber_lname"]').val(lastname);
                    $('input[name="subscriber_email"]').val(email);
                    $('input[name="subscriber_phone"]').val(mobile);
                    $('input[name="subscriber_dob"]').val(dob);
                    $('input[name="subscriber_flatno"]').val(flat_number);
                    $('input[name="subscriber_address1"]').val(address1);
                    $('input[name="subscriber_address2"]').val(address2);
                    $('select[name="state"]').val(state);
                    $('select[name="city"]').val(city);
                    $('select[name="zone"]').val(zone);
                    $('input[name="usuage_locality"]').val(usuage_locality);
                    $('input[name="subscriber_username"]').val(username);
                    $('input[name="subscriber_uuid"]').val(uid);
                    $('input[name="priority_account"]').val(priority_account);


                    
                }
            }
            $('.loading').addClass('hide');
            window.scrollTo(0,0);
            mui.tabs.activate('Personal_details');
        }
    });
}

function edit_customer() {
    $('.loading').removeClass('hide');
    var formdata = $('#edit_subscriber_form').serialize();
    $.ajax({
        url: base_url+'user/edit_personaldetails',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        success: function(data){
            //alert(data);
            if (data.customer_id != null && data.customer_uuid != '') {
                $('.subscriber_userid').val(data.customer_id);
                $('.tab_menu').css({'cursor' : 'pointer'});
                $('.tab_menu').removeAttr('disabled');
                $('#usertype_customer').attr('checked','checked');
                $('.radio-inline').css('color', '#b3b3b3');
                $('input[type="radio"][name="user_type_radio"]').attr('disabled','disabled');
                
                $('input[name="customer_uuid"]').val(data.customer_uuid);
                $('.customer_name').html(data.customer_name);
                $('.customer_email').html(data.customer_email);
                $('.customer_mobile').html('+91 '+data.customer_mobile);
            }
            $('.loading').addClass('hide');
            window.scrollTo(0,0);
            mui.tabs.activate('Personal_details');
        }
    })
}

function edit_allkyc_details() {
    $('#doclisting').html('');
    $('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_userid').val();
    $.ajax({
        url: base_url+'user/edit_allkyc_details',
        type: 'POST',
        dataType: 'json',
        data: "cust_uuid="+cust_uuid,
        success: function(data){
            var gen = '';
            if (data.length > 0) {
                $.each(data, function(index, element) {
                    //alert(index +' => '+ element['doctype'] + ' => ' + element['filename']);
                    var doctype = element['doctype'];
                    var docid = element['docid'];
                    var uuid = element['subscriber_uuid'];
                    var filename = element['filename'];
                    var document_number = element['document_number'];
                    var user_profile_image = element['user_profile_image'];
                    var user_signature_image = element['user_signature_image'];

                    if ((typeof user_profile_image != 'undefined') && (user_profile_image != '')) {
                        gen += '<div class="row" style="margin-top:20px"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>User Image</label></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>&nbsp;</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="edit_docpreview(\''+user_profile_image+'\', \''+uuid+'\')"><i class="fa fa-eye" aria-hidden="true"></i></span></div></div>';
                    }
                    if ((typeof user_signature_image != 'undefined') && (user_signature_image != '')) {
                        gen += '<div class="row" style="margin-top:20px"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>User Signature</label></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>&nbsp;</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="edit_docpreview(\''+user_signature_image+'\', \''+uuid+'\')"><i class="fa fa-eye" aria-hidden="true"></i></span></div></div>';
                    }
                    if ((typeof user_profile_image == 'undefined') || (typeof user_signature_image == 'undefined')) {
                        gen += '<div class="row" style="margin-top:20px"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>'+element['doctype']+'</label></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label>'+document_number+'</label></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="label label-default" onclick="edit_docpreview(\''+filename+'\', \''+uuid+'\')"><i class="fa fa-eye" aria-hidden="true"></i></span></div></div>';
                    //<span class="label label-default"  onclick="edit_docdelete(\''+docid+'\', \''+uuid+'\')"><i class="fa fa-trash" aria-hidden="true"></i></span>
                    }
                });
            }
            $('#uploaded_docs').html(gen);
            window.scrollTo(0,0);
            mui.tabs.activate('KYC_details');
            $('.loading').addClass('hide');
        }
    });
}

function edit_docpreview(filename, uuid) {
    var imgpath = base_url+"assets/media/documents/"+uuid+"/"+filename;
    $('#edit_documentModal').modal('show');
    $('#edit_docpreview').html("<center><img class='previewimg' src='"+imgpath+"' style='width:70%'/></center>");
}

function edit_docdelete(docid, uuid) {
    var c = confirm('Are you sure, you want to delete this ?');
    if (c) {
        $.ajax({
            url: base_url+'user/edit_docdelete',
            type: 'POST',
            dataType: 'json',
            data: "cust_uuid="+uuid+"&docid="+docid,
            success: function(data){
                edit_allkyc_details();
            }
        })
    }
}

function getplandetails(planid) {
    if (planid != '') {
        $.ajax({
            url: base_url+'user/plandetails',
            type: 'POST',
            dataType: 'json',
            data: "planid="+planid,
            success: function(data){
                //alert(data);
                $('#plan_details').removeClass('hide');
                $('.timeplan').css('display', 'none');
                $('.dataplan').css('display', 'none');
                $('.fupclass').css('display', 'none');
                
                $('.detplan_name').val(data.plan_name);
                $('.detplan_desc').val(data.plan_desc);
                $('.detplan_validity').val(data.plan_validity+' days');
                $('.detplan_type').html(data.plan_type);
                $('.detdwnld_rate').val(data.plan_dwnld_rate);
                $('.detupld_rate').val(data.plan_upld_rate);
                $('#plan_price').html('(&#8377; '+data.plan_net_amount+')');
                $('#plan_type_id').val(data.plan_type_id);
                if (data.plan_type_id == '2') {
                    $('.timeplan').css('display', 'block');
                    $('.fupclass').css('display', 'none');
                    $('.dataplan').css('display', 'none');
                    
                    $('.detplan_timelimit').val(data.plan_timelimit);
                    $('.dettime_calculated_on').html(data.plan_time_calculated_on)
                }else if (data.plan_type_id == '4') {
                    $('.timeplan').css('display', 'none');
                    $('.dataplan').css('display', 'block');
                    $('.fupclass').css('display', 'none');
                    
                    $('.detdata_limit').val(data.plan_data_limit);
                    $('#plandatalimit').val(data.plandatalimit);
                    $('#plandata_calculatedon').val(data.plandata_calculatedon);
                    $('.detdata_calcn').html(data.plan_data_calculated_on);
                }else if (data.plan_type_id == '3') {
                    $('.timeplan').css('display', 'none');
                    $('.dataplan').css('display', 'block');
                    $('.fupclass').css('display', 'block');
                    
                    $('.detdata_limit').val(data.plan_data_limit);
                    $('#plandatalimit').val(data.plandatalimit);
                    $('#plandata_calculatedon').val(data.plandata_calculatedon);
                    $('.detdata_calcn').html(data.plan_data_calculated_on);
                    $('.detfup_dwnl_rate').val(data.plan_postfup_dwnld_rate);
                    $('.detfup_upld_rate').val(data.plan_postfup_upld_rate)
                }else{
                    $('#plandatalimit').val('0');
                    $('#plandata_calculatedon').val('0');
                    
                }
                $('#plan_start_date').html(data.plan_activatedon);
                $('#plan_payment_till').html(data.renewal_date);
                $('#plan_expiry_on').html(data.expiration_date);
            }
        });
    }else{
        $('#plan_details').addClass('hide');
    }
}

function add_subscriber_plan(){
    $('.loading').removeClass('hide');
    var formdata =  $('#subscriber_plandetail_form').serialize();
    var editplan = $('.editplan').val();
    if ((typeof editplan != 'undefined') && (editplan == '1') ) {
        //$('#plan_change').modal('show');
    }else{
        //$('#plan_change').modal('hide');
    }
    $.ajax({
        url: base_url+'user/add_subscriber_plan',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $('#wallet_credit_limit').html('&#8377; '+data+'.00');
            window.scrollTo(0,0);
            
            var subsid = $('.subscriber_userid').val();
            var subs_uuid = $('.subscriber_uuid').val();
            $.ajax({
                url: base_url+'user/planassoc_withuser',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: 'subsid='+subsid+'&subs_uuid='+subs_uuid,
                success: function(data){
                    if (data == 0) {
                        $('#inactive_billing').removeClass('hide');
                        $('#active_billing').addClass('hide');
                    }else{
                        $('#inactive_billing').addClass('hide');
                        $('#active_billing').removeClass('hide');
                    }
                }
            });
            mui.tabs.activate('Billing');
            $('.loading').addClass('hide');
        }
    });   
}

function subscriber_plan_details() {
    $('.loading').removeClass('hide');
    var cust_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/subscriber_plan_details',
        type: 'POST',
        dataType: 'json',
        data: "cust_uuid="+cust_uuid,
        success: function(data){
            if (data != '') {
                $('#plan_details').addClass('hide');
                $('#assign_plan option').remove();
                $('#assign_plan').append(data.active_planlist);

                if (typeof data.plan_id != 'undefined') {
                    $('#plan_details').removeClass('hide');
                    $('.timeplan').css('display', 'none');
                    $('.dataplan').css('display', 'none');
                    $('.fupclass').css('display', 'none');
                
                    if (typeof data.next_cycle_plan != 'undefined') {
                        $('.next_cycle_plan').removeClass('hide');
                    }
                
                    $('select[name="assign_plan"]').val(data.plan_id);
                    $('.detplan_name').val(data.plan_name);
                    $('.detplan_desc').val(data.plan_desc);
                    $('.detplan_validity').val(data.plan_validity+' days');
                    $('.detplan_type').html(data.plan_type);
                    $('.detdwnld_rate').val(data.plan_dwnld_rate);
                    $('.detupld_rate').val(data.plan_upld_rate);
                    $('#plan_price').html('(&#8377; '+data.plan_net_amount+')');
                    $('#plan_type_id').val(data.plan_type_id);
                    if (data.plan_type_id == '2') {
                        $('.timeplan').css('display', 'block');
                        $('.fupclass').css('display', 'none');
                        $('.dataplan').css('display', 'none');
                        
                        $('.detplan_timelimit').val(data.plan_timelimit);
                        $('.dettime_calculated_on').html(data.plan_time_calculated_on)
                    }else if (data.plan_type_id == '4') {
                        $('.timeplan').css('display', 'none');
                        $('.dataplan').css('display', 'block');
                        $('.fupclass').css('display', 'none');
                        
                        $('.detdata_limit').val(data.plan_data_limit);
                        $('#plandatalimit').val(data.plandatalimit);
                        $('#plandata_calculatedon').val(data.plandata_calculatedon);
                        $('.detdata_calcn').html(data.plan_data_calculated_on);
                    }else if (data.plan_type_id == '3') {
                        $('.timeplan').css('display', 'none');
                        $('.dataplan').css('display', 'block');
                        $('.fupclass').css('display', 'block');
                        
                        $('.detdata_limit').val(data.plan_data_limit);
                        $('#plandatalimit').val(data.plandatalimit);
                        $('#plandata_calculatedon').val(data.plandata_calculatedon);
                        $('.detdata_calcn').html(data.plan_data_calculated_on);
                        $('.detfup_dwnl_rate').val(data.plan_postfup_dwnld_rate);
                        $('.detfup_upld_rate').val(data.plan_postfup_upld_rate)
                    }else{
                        $('#plandatalimit').val('0');
                        $('#plandata_calculatedon').val('0');
                        
                    }
                    
                    $('#plan_start_date').html(data.plan_activatedon);
                    $('#plan_payment_till').html(data.paidtill_date);
                    $('#plan_expiry_on').html(data.expiration_date);
                    $('#next_billing_date').html(data.next_bill_date);
                    
                }
                window.scrollTo(0,0);
                mui.tabs.activate('Plan_details');
                $('.loading').addClass('hide');
            }else{
                window.scrollTo(0,0);
                mui.tabs.activate('Plan_details');
                $('.loading').addClass('hide');
            }
        }
    });
}

function nextplan_details(planid) {
    if (planid != '') {
        $.ajax({
            url: base_url+'user/plandetails',
            type: 'POST',
            dataType: 'json',
            data: "planid="+planid,
            success: function(data){
                if (data != '') {
                    $('#nextplan_details').removeClass('hide');
                    $('.nxt_timeplan').css('display', 'none');
                    $('.nxt_dataplan').css('display', 'none');
                    $('.nxt_fupclass').css('display', 'none');
                    
                    if (typeof data.next_cycle_plan != 'undefined') {
                        $('.next_cycle_plan').removeClass('hide');
                    }
                    
                    $('.nxt_plan_name').html(data.plan_name);
                    $('.nxt_detplan_desc').val(data.plan_desc);
                    $('.nxt_detplan_validity').val(data.plan_validity+' days');
                    $('.nxt_detplan_type').html(data.plan_type);
                    $('.nxt_detdwnld_rate').val(data.plan_dwnld_rate);
                    $('.nxt_detupld_rate').val(data.plan_upld_rate);
                    $('#nxt_plan_price').html('(&#8377; '+data.plan_net_amount+')');
                    $('#plan_type_id').val(data.plan_type_id);
                    if (data.plan_type_id == '2') {
                        $('.nxt_timeplan').css('display', 'block');
                        $('.nxt_fupclass').css('display', 'none');
                        $('.nxt_dataplan').css('display', 'none');
                        
                        $('.nxt_detplan_timelimit').val(data.plan_timelimit);
                        $('.nxt_dettime_calculated_on').html(data.plan_time_calculated_on)
                    }else if (data.plan_type_id == '4') {
                        $('.nxt_timeplan').css('display', 'none');
                        $('.nxt_dataplan').css('display', 'block');
                        $('.nxt_fupclass').css('display', 'none');
                        
                        $('.nxt_detdata_limit').val(data.plan_data_limit);
                        $('.nxt_detdata_calcn').html(data.plan_data_calculated_on);
                    }else if (data.plan_type_id == '3') {
                        $('.nxt_timeplan').css('display', 'none');
                        $('.nxt_dataplan').css('display', 'block');
                        $('.nxt_fupclass').css('display', 'block');
                        
                        $('.nxt_detdata_limit').val(data.plan_data_limit);
                        $('.nxt_detdata_calcn').html(data.plan_data_calculated_on);
                        $('.nxt_detfup_dwnl_rate').val(data.plan_postfup_dwnld_rate);
                        $('.nxt_detfup_upld_rate').val(data.plan_postfup_upld_rate)
                    }
                    window.scrollTo(0,0);
                    mui.tabs.activate('Plan_details');
                }
            }
        });
    }else{
        $('#nextplan_details').addClass('hide');
    }
}

function active_user_confirmation() {
    $('.loading').removeClass('hide');
    var subsid = $('.subscriber_userid').val();
    var subs_uuid = $('.subscriber_uuid').val();
    var otp_to_activate_user = $('#otp_to_activate_user').val();
    var enter_otp_to_activate_user = $('#enter_otp_to_activate_user').val();
    var phone = $('#phone_to_activate_user').val();
    
    var formdata = 'subsid='+subsid+'&subs_uuid='+subs_uuid+'&send_otp='+otp_to_activate_user+'&enter_otp='+enter_otp_to_activate_user+'&phone='+phone;
    
    if (otp_to_activate_user != enter_otp_to_activate_user) {
        $('#activate_otp_err').html('Otp not matching. Please Try Again.');
        $('.loading').addClass('hide');
    }else{
        $('#user_activatedModal').modal('hide');
        $.ajax({
            url: base_url+'user/active_user_confirmation',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function(data){
                window.location = base_url+'user/edit/'+subsid;
            }
        });
    }
}

function nextcycle_planlist() {
    $('.loading').removeClass('hide');
    $('#nextplan_details').addClass('hide');
    var subs_uuid = $('.subscriber_uuid').val();
    $.ajax({
        url: base_url+'user/nextcycle_planlist',
        type: 'POST',
        dataType: 'json',
        data: 'uuid='+subs_uuid,
        success: function(data){
            $('#nextassign_plan option').remove();
            $('#nextassign_plan').append(data.active_planlist);
            if (data.nextplanid != 0) {
                nextplan_details(data.nextplanid);
            }
            $('.loading').addClass('hide');
        }
    });
}

function add_nextcycleplan() {
    $('.loading').removeClass('hide');
    var formdata =  $('#nextcycle_planform').serialize();
    $.ajax({
        url: base_url+'user/add_nextcycleplan',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $('.loading').addClass('hide');
            nextplan_details(data.nextplanid);
        }
    });   
}

function assign_ticket_member(tktid) {
    $('#assignTicketModal').modal('hide');
    $('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
       url: base_url+'user/ticket_sorters_list',
       type: 'POST',
       async: false,
       data: 'uuid='+cust_uuid,
       success: function(data){
          $('select[name="team_member"]').empty().append('<option value="">Select Team Member</option>');
          $('select[name="team_member"]').append(data);
          $('.loading').addClass('hide');
          $('#tktid_toupdate').val(tktid);
          $('#assignTicketModal').modal('show');
       }
    });
}

function updateTicketMember() {
    $('.loading').removeClass('hide');
    var formdata =  $('#assignTicketMember_form').serialize();
    $.ajax({
        url: base_url+'user/updateTicketMember',
        type: 'POST',
        dataType: 'json',
        data: formdata,
        async: false,
        success: function(data){
            $('#assignTicketModal').modal('hide');
            $('.loading').addClass('hide');
            fetch_allticket();
        }
    });
}

$('body').on('click', '#settingclick_details', function(){
    $('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
       url: base_url+'user/view_syssetting',
       type: 'POST',
       dataType: 'json',
       data: 'uuid='+cust_uuid,
       success: function(data){
          $("input[name=ipaddrtype][value=" + data.ipaddrtype + "]").attr('checked', 'checked');
            if (data.ipaddrtype == 0) {
                $('.addstaticip').removeClass('hide');
                $('#ipaddress').attr('required',true);
            }else{
                $('.addstaticip').addClass('hide');
                $('#ipaddress').attr('required',false);
            }
          
          $('input[name="ipaddress"]').val(data.ipaddress);
          if (typeof data.mac != 'undefined') {
            $('#curr_macid').html(data.mac);  
          }
          $("input[name=bindmaclock][value=" + data.maclockedstatus + "]").attr('checked', 'checked');
          $('.loading').addClass('hide');
       }
    });
});
function system_setting() {
    var staticIP = $("#ipaddress").val();
    validIP = ValidateIPaddress(staticIP);
    if (validIP) {
        $('.loading').removeClass('hide');
        var formdata  = $('#syssetting_form').serialize();
        $.ajax({
            url: base_url+'user/update_syssetting',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            success: function(data){
                if (data.ipexists == '1') {
                    $('#staticipaddr_err').html('This IP Address is alreay associated.');
                }else{
                    $("input[name=ipaddrtype][value=" + data.ipaddrtype + "]").attr('checked', 'checked');
                    if (data.ipaddrtype == 0) {
                        $('.addstaticip').removeClass('hide');
                        $('#ipaddress').attr('required',true);
                    }else{
                        $('.addstaticip').addClass('hide');
                        $('#ipaddress').attr('required',false);
                    }
                    $('input[name="ipaddress"]').val(data.ipaddress);
                    if (typeof data.mac != 'undefined') {
                      $('#curr_macid').html(data.mac);  
                    }
                    $("input[name=bindmaclock][value=" + data.maclockedstatus + "]").attr('checked', 'checked');
                }
                $('.loading').addClass('hide');
            }
        });
    }else{
        $('#staticipaddr_err').html('Please enter valid IP Address.');
    }
}

function ValidateIPaddress(ipaddress){
    if (ipaddress != '') { 
        if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))  {
            return true;  
        }else{
            return false;
        }
    }else{
        return true;
    }
}

function defaultkyctab_details() {
    $('#subscriber_documents_form')[0].reset();
    $('#default_iddocdiv').removeClass('hide');
    $('.idproofdocdiv ').remove();
    $('#idprooflisting').html('');
    $('#defaultdocdiv').removeClass('hide');
    $('.subsdocdiv').remove();
    $('#kyclisting').html('');
    $('#docpreview').html('');
    $('.idproof_docnumber').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
    $('.kycdocnumber').removeClass('mui--is-dirty mui--is-not-empty mui--is-touched');
    $('#user_image').html('');
    $('#user_signature').html('');
    
    $('#idproof_docnumber_arr').val('');
    $('#idproof_doctype_arr').val('');
    $('#kyc_documents_arr').val('');
    $('#kycdocnumber_arr').val('');
    
    filecount = 0; filearr = 0; filedoctype = []; kycdocnumb = []; abc = 0; sno = 0;
    idp_filecount = 0; idp_filearr = 0; idp_filedoctype = []; iddocnumb = []; idp_abc = 0; idp_sno = 0;
    
    edit_allkyc_details();
}

function defaultplantab_details() {
    $('#active_plan').css('color', '#424143');
    $('#next_cycle_plan').css('color', '#29abe2');
    $('#subscriber_plandetail_form').removeClass('hide');
    $('#nextcycle_planform').addClass('hide');
    subscriber_plan_details();
}

function defaultsettingtab_details() {
    $('.loading').removeClass('hide');
    var cust_uuid = $('input[name="subscriber_uuid"]').val();
    $.ajax({
       url: base_url+'user/view_syssetting',
       type: 'POST',
       dataType: 'json',
       data: 'uuid='+cust_uuid,
       success: function(data){
          $("input[name=ipaddrtype][value=" + data.ipaddrtype + "]").attr('checked', 'checked');
            if (data.ipaddrtype == 0) {
                $('.addstaticip').removeClass('hide');
                $('#ipaddress').attr('required',true);
            }else{
                $('.addstaticip').addClass('hide');
                $('#ipaddress').attr('required',false);
            }
          
          $('input[name="ipaddress"]').val(data.ipaddress);
          if (typeof data.mac != 'undefined') {
            $('#curr_macid').html(data.mac);  
          }
          $("input[name=bindmaclock][value=" + data.maclockedstatus + "]").attr('checked', 'checked');
          $('.loading').addClass('hide');
       }
    });
}

$('body').on('change', '#userstatus_changealert', function(){
    var currtstatus = $('#userstatus_changealert').val();
    var account_activated_on = $('#account_activated_on').val();
    if ((currtstatus == 1) && (account_activated_on != '')) {
        $('#userstatus_changealert').parent().removeClass('btn-danger off');
        $('#userstatus_changealert').parent().addClass('btn-success on');
        $('#userstatus_changealertModal').modal('show');
    }else if ((currtstatus == 0) && (account_activated_on != '')) {
        $('#userstatus_changealert').parent().removeClass('btn-success');
        $('#userstatus_changealert').parent().addClass('btn-danger off');
        $('#userstat_toActiveModal').modal('show');
    }else{
         var user_activate_check = $('#userstatus_changealert').val();
         var subsid = $('.subscriber_userid').val();
        $.ajax({
           url: base_url+'user/user_activate_check',
           type: 'POST',
           data: 'subsid='+subsid+'&is_user_activated='+user_activate_check,
           dataType: 'json',
           success: function(data){
                var otp = data.useract_otp;
                var personal_details = data.personal_details;
                var kyc_details = data.kyc_details;
                var plan_details = data.plan_details;
                var installation_details = data.installation_details;
                var security_details = data.security_details;
                
                if (otp != '0') {
                    $('#user_inactiveAlert').modal('hide');
                    $('#otp_to_activate_user').val(otp);
                    $('#activate_username').html(data.username);
                    var custphone = data.phone;
                    $('#phone_to_activate_user').val(custphone);
                    for (idx=2; idx < 7 ; idx++) {
                        custphone = custphone.substring(0, idx) + '*' + custphone.substring(idx+1);
                    }
                    $('#activate_userphone').html(custphone);
                    
                    $('#userstatus_changealert').parent().removeClass('btn-success');
                    $('#userstatus_changealert').parent().addClass('btn-danger off');
                    $('#user_activatedModal').modal('show');
                }else{
                    $('#userstatus_changealert').val(0);
                    $('#userstatus_changealert').parent().removeClass('btn-success');
                    $('#userstatus_changealert').parent().addClass('btn-danger off');
                    
                    if (personal_details == 0) {
                        $('#pedactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#pedactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if (kyc_details == 0) {
                        $('#kycactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#kycactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if (plan_details == 0) {
                        $('#planactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#planactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    if ((installation_details == 0) || (security_details == 0)) {
                        $('#inscactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                    }else{
                        $('#inscactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                    }
                    
                    $('#user_inactiveAlert').modal('show');
                }
           }
        });
    }
});
$('body').on('change', '.inactivate_type', function(){
    var inactivate_type = $('input[name="inactivate_type"]:checked').val();
    var suspend_days = $('select[name="suspend_days"]').val();
    if ((inactivate_type == 'suspended') && (suspend_days == '')) {
        $('select[name="suspend_days"]').attr('required', true);
    }else if(inactivate_type == 'terminate'){
        $('select[name="suspend_days"]').attr('required', false);
    }
    
});
function update_userstatus(){
    var inactivate_type = $('input[name="inactivate_type"]:checked').val();
    var suspend_days = $('select[name="suspend_days"]').val();
    var uuid = $('.subscriber_uuid').val();
    var userid = $('.subscriber_userid').val();
    
    $.ajax({
        url: base_url+'user/updateuser_profilestatus',
        type: 'POST',
        data: 'uuid='+uuid+'&inactivate_type='+inactivate_type+'&suspend_days='+suspend_days,
        success: function(){
           window.location = base_url+'user/edit/'+userid;
        }
    });
}
function resetuser_activate() {
    var uuid = $('.subscriber_uuid').val();
    var userid = $('.subscriber_userid').val();
    
    $.ajax({
        url: base_url+'user/resetuser_toactivate',
        type: 'POST',
        data: 'uuid='+uuid,
        success: function(){
           window.location = base_url+'user/edit/'+userid;
        }
    });
}

function inactiveuser_pendingdocs(subsid) {
    $.ajax({
        url: base_url+'user/user_activate_check',
        type: 'POST',
        data: 'subsid='+subsid+'&is_user_activated=0',
        dataType: 'json',
        success: function(data){
             var otp = data.useract_otp;
             var personal_details = data.personal_details;
             var kyc_details = data.kyc_details;
             var plan_details = data.plan_details;
             var installation_details = data.installation_details;
             var security_details = data.security_details;
             
             if (otp != '0') {
                 $('#user_inactiveAlert').modal('hide');
                 $('#otp_to_activate_user').val(otp);
                 $('#activate_username').html(data.username);
                 var custphone = data.phone;
                 $('#phone_to_activate_user').val(custphone);
                 for (idx=2; idx < 7 ; idx++) {
                     custphone = custphone.substring(0, idx) + '*' + custphone.substring(idx+1);
                 }
                 $('#activate_userphone').html(custphone);
                 
                 $('#user_activate_check').parent().removeClass('btn-danger off');
                 $('#user_activate_check').parent().addClass('btn-success on');
                 $('#user_activatedModal').modal('show');
             }else{
                 $('#user_activate_check').val(0);
                 $('#user_activate_check').parent().removeClass('btn-success');
                 $('#user_activate_check').parent().addClass('btn-danger off');
                 
                 if (personal_details == 0) {
                     $('#pedactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                 }else{
                     $('#pedactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                 }
                 if (kyc_details == 0) {
                     $('#kycactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                 }else{
                     $('#kycactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                 }
                 if (plan_details == 0) {
                     $('#planactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                 }else{
                     $('#planactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                 }
                 if ((installation_details == 0) || (security_details == 0)) {
                     $('#inscactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');    
                 }else{
                     $('#inscactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289');    
                 }
                 
                 $('#user_inactiveAlert').modal('show');
             }
        }
     });
}
